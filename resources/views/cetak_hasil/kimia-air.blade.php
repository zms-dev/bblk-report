<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama {
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td {
    padding: 5px;
    text-align: left;
    border: 1px solid #333;
}
</style>
<div>
    <center>FORMULIR HASIL PENGUJIAN PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL KIMIA AIR SIKLUS {{$siklus}} TAHUN {{$date}}</center></label><br>
</div>
<div class="form-group">
    <label for="nama" class="col-sm-3 control-label">Kode Peserta : {{ $register->kode_lebpes }}</label>
</div><br>
<div class="form-group">
    <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan : {{date('d/m/Y', strtotime($datas->tgl_penerimaan))}}</label>
</div><br>
<div class="form-group">
    <label for="nama" class="col-sm-3 control-label">Kualitas Bahan : {{$datas->kualitas_bahan}}</label>
</div><br>
<div class="form-group">
    <label for="nama" class="col-sm-3 control-label">Nama Penguji : {{ $datas->nama_pemeriksa }}</label>
</div><br>
<div class="form-group">
    <label for="nama" class="col-sm-3 control-label">Nomor HP : {{ $datas->nomor_hp }}</label>
</div><br>

<table class="utama">
    <thead>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Parameter</th>
            <th colspan="2"><center>Tanggal Pengujian</center></th>
            <th rowspan="2">Alat & Merek</th>
            <th rowspan="2"><center>Metode Pengujian</center></th>
            <th rowspan="2">Hasil</th>
        </tr>
        <tr>
            <th><center>Mulai</center></th>
            <th><center>Selesai</center></th>
        </tr>
    </thead>
    <tbody>
        @if(count($data))
        <?php $no = 0; ?>
        @foreach($data as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{!!$val->nama_parameter!!}</td>
            <td>{{date('d/m/Y', strtotime($val->tanggal_mulai))}}</td>
            <td>{{date('d/m/Y', strtotime($val->tanggal_selesai))}}</td>
            <td>{{ $val->Alat }}</td>
            <td><center>{!! $val->Kode !!}</center><br><center>{!! $val->metode_lain !!}</center></td>
            <td>{{ $val->Hasil }} mg/L</td>
        </tr>
        @endforeach
        @endif
        
    </tbody>
</table>
<table width="100%"  class="utama">
    <tr>
        <td width="70%"><label>Catatan :</label></td>
        <td width="30%"><label>Nama Penanggung Jawab :</label></td>
    </tr>
    <tr>
        <td>
            <div style="width: 100%; border: 1px solid #ddd; margin-right: 20px; padding: 5px">
                {{$datas->catatan}}
            </div>
        </td>
        <td>
            {{$datas->penanggung_jawab}}
        </td>
    </tr>
</table>
<br><br>
<table width="100%" border="0">
    <tr>
        <td width="70%">Mengetahui,</td>
        <td>.......................... , ...................</td>
    </tr>
    <tr>
        <td>Pimpinan Laboratorium</td>
        <td></td>
    </tr>
    <tr>
        <td>{{$perusahaan}}</td>
        <td>Penanggungjawab Pemeriksaan</td>
    </tr>
    <tr>
        <td height="50px"></td>
        <td></td>
    </tr>
    <tr>
        <td>Nama ...................................</td>
        <td>Nama ...................................</td>
    </tr>
    <tr>
        <td>NIP .......................................</td>
        <td>NIP .......................................</td>
    </tr>
</table>