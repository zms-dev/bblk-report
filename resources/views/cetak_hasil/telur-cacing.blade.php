<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
    margin-top: 100px;
}
table.utama {
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td {
    padding: 5px;
    text-align: left;
    border: 1px solid #333;
}
</style>
<div>
    <center><label>FORMULIR HASIL PEMERIKSAAN PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br> MIKKROSKOPIS TELUR CACING SIKLUS {{$siklus}} TAHUN {{$date}}</label></center><br>
</div>
<div class="form-group">
    <label for="nama" class="col-sm-3 control-label">Kode Peserta : {{$data->kode_peserta}}</label>
</div><br>
<div class="form-group">
    <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan : {{date('d-m-Y', strtotime($data->tgl_penerimaan))}}</label>
</div><b></b><br>
<div class="form-group">
    <label for="nama" class="col-sm-3 control-label">Kualitas Bahan : {{$data->kondisi}}</label>
</div><br>
<div class="form-group">
    <label for="nama" class="col-sm-3 control-label">Tanggal Pemeriksaan : {{date('d-m-Y', strtotime($data->tgl_pemeriksaan))}}</label> 
</div><br>
<div class="form-group">
    <label for="nama" class="col-sm-3 control-label">Nama Pemeriksa : {{ $data->nama_pemeriksa }}</label>
</div><br>
<div class="form-group">
    <label for="nama" class="col-sm-3 control-label">Nomor HP : {{ $data->nomor_hp }}</label>
</div><br>

<table class="utama">
    <tr>
        <th>Kode Botol</th>
        <th><center>Reagen Yang Digunakan</center></th>
        <th><center>Hasil Pemeriksaan</center></th>
    </tr>
    @if(count($data))
    <?php $no = 0; $h = "A"; ?>
    @foreach($datas as $val)
    <?php $no++; ?>
    <tr>
        <td>TCC-{!!$val->kode_botol!!}{{$h++}}</td>
        <td><center>{{ $val->reagen }}<br>{{$val->reagen_lain}}</center></td>
        <td><center>{!! $val->hasil !!}</center></td>
    </tr>
    @endforeach 
    @endif
</table>
<table width="100%" class="utama">
    <tr>
        <td width="70%"><label>Catatan :</label></td>
        <td width="30%"><label>Nama Penanggung Jawab :</label></td>
    </tr>
    <tr>
        <td>
            <div style="width: 100%; margin-right: 20px; padding: 5px">
                {{$data->catatan}}
            </div>
        </td>
        <td>
            {{$data->penanggung_jawab}}
        </td>
    </tr>
</table>
<br><br>
<table width="100%" border="0">
    <tr>
        <td width="70%">Mengetahui,</td>
        <td>.......................... , ...................</td>
    </tr>
    <tr>
        <td>Pimpinan Laboratorium</td>
        <td></td>
    </tr>
    <tr>
        <td>{{$perusahaan}}</td>
        <td>Penanggungjawab Pemeriksaan</td>
    </tr>
    <tr>
        <td height="50px"></td>
        <td></td>
    </tr>
    <tr>
        <td>Nama ...................................</td>
        <td>Nama ...................................</td>
    </tr>
    <tr>
        <td>NIP .......................................</td>
        <td>NIP .......................................</td>
    </tr>
</table>