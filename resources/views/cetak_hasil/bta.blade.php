<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
    margin-top: 100px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th,table.utama td {
    padding: 5px;
    text-align: left;
    border: 1px solid #333;
}
</style>
<div>
    <center><label>FORMULIR HASIL PEMERIKSAAN PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MIKKROSKOPIS BTA SIKLUS {{$siklus}} TAHUN {{$date}}</label></center><br>
</div>
<div>
    <label for="nama">Kode Peserta : {{ $perusahaan }}</label>
</div><br>
<div>
    <label for="nama">Tanggal Penerimaan : {{date('d-m-Y', strtotime($data->tgl_penerimaan))}}</label>
</div><br>
<div>
    <label for="nama">Kualitas Bahan : {{$data->kondisi}}</label>
</div><br>
<div>
    <label for="nama">Tanggal Pemeriksaan : {{date('d-m-Y', strtotime($data->tgl_pemeriksaan))}}</label>
</div><br>
<div>
    <label for="nama">Nama Pemeriksa : {{ $data->nama_pemeriksa }}</label>
</div><br>
<div>
    <label for="nama">Nomor HP : {{ $data->nomor_hp }}</label>
</div><br>
<div>
    <label for="nama">Jenis Lab : {{ $data->jenis_lab }}</label>
</div><br>

<table class="utama">
    <thead>
        <tr>
            <th><center>Kode Bahan Uji</center></th>
            <th><center>Hasil Pemeriksaan</center></th>
        </tr>
    </thead>
    <tbody>
        @if(count($data))
        <?php $no = 0; ?>
        @foreach($datas as $val)
        <?php $no++; ?>
        <tr>
            <td>{!!$val->kode!!}</td>
            <td>{{ $val->hasil }} @if($val->kuman != NULL): {{ $val->kuman }} BTA/100 Lapang Pandang @endif</td>
        </tr>
        @endforeach
        @endif
        
    </tbody>
</table>
<table width="100%" class="utama">
    <tr>
        <td width="70%"><label>Catatan :</label></td>
        <td width="30%"><label>Nama Penanggung Jawab :</label></td>
    </tr>
    <tr>
        <td>
            <div style="width: 100%; margin-right: 20px; padding: 5px">
                {{$data->catatan}}
            </div>
        </td>
        <td>
            {{$data->penanggung_jawab}}
        </td>
    </tr>
</table>
<br><br>
<table width="100%" border="0">
    <tr>
        <td width="70%">Mengetahui,</td>
        <td>.......................... , ...................</td>
    </tr>
    <tr>
        <td>Pimpinan Laboratorium</td>
        <td></td>
    </tr>
    <tr>
        <td>{{$perusahaan}}</td>
        <td>Penanggungjawab Pemeriksaan</td>
    </tr>
    <tr>
        <td height="50px"></td>
        <td></td>
    </tr>
    <tr>
        <td>Nama ...................................</td>
        <td>Nama ...................................</td>
    </tr>
    <tr>
        <td>NIP .......................................</td>
        <td>NIP .......................................</td>
    </tr>
</table>