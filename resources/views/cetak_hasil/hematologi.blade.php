<style type="text/css">
@page {
margin: 140px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #333;
}

#header { 
    position: fixed; 
    border-bottom:1px solid gray;
    padding-top: -130px;
}
#footer { 
    position: fixed; 
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<!-- <div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th style="vertical-align: top; padding-top: -5px">
                    <img alt="" src="http://webdesign.zamasco.com/img/logo-kemenkes.png" height="70px">
                </th>
                <td width="100%" style="padding-left: 20px">
                    <span style="font-size: 12px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 12px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL HEMATOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}</b></span><br>
                    <span style="font-size: 10px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Jakarta</b></span>
                    <div style="padding-left: 75px; margin-top: -13px; font-size: 10px">
                        <span><br/>Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560<br>Telp. (021) 4212524, 42804339 Facsimile (021) 4245516<br>Website : bblkjakarta.com ; Surat Elektronik : bblkjakarta@yahoo.co.id</span>
                    </div>
                </td>
                <th style="vertical-align: top;">
                    <img alt="" src="http://webdesign.zamasco.com/img/logo-bblk-sby.png" height="60px">
                </th>
            </tr>
            <tr>
                <th colspan="3">&nbsp;</th>
            </tr>
            <tr>
                <th colspan="3"><hr></th>
            </tr>
        </thead>
    </table>
</div> -->
<div id="footer">
    PNPME HEMATOLOGI {{$datas->kode_bahan}} TAHUN {{$date}}
</div>
<label>
    <center>
        <font size="10px">
            FORMULIR HASIL PEMERIKSAAN PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br>HEMATOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}
        </font>
    </center>
</label><br>
<table>
    <tr>
        <td>Kode Peserta </td>
        <td>: </td>
        <td>{{ $datas->kode_lab }}</td>
    </tr>
    <tr>
        <td>Kode Bahan </td>
        <td>: </td>
        <td>{{ $datas->kode_bahan }}</td>
    </tr>
    <tr>
        <td>Tanggal Penerimaan </td>
        <td>: </td>
        <td>{{date('d-m-Y', strtotime($datas->tgl_penerimaan))}}</td>
    </tr>
    <tr>
        <td>Kualitas Bahan </td>
        <td>: </td>
        <td><font style="text-transform: capitalize;">{{$datas->kualitas_bahan}}</font></td>
    </tr>
    <tr>
        <td>Tanggal Pemeriksaan </td>
        <td>: </td>
        <td>{{date('d-m-Y', strtotime($datas->tgl_pemeriksaan))}}</td>
    </tr>
</table>

<table class="table utama">
    <thead>
        <tr>
            <th>No</th>
            <th>Parameter</th>
            <th>Instrument</th>
            <th>Metode Pemeriksaan</th>
            <th>Hasil</th>
        </tr>
    </thead>
    <tbody>
        @if(count($data))
        <?php $no = 0; ?>
        @foreach($data as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{!!$val->nama_parameter!!}</td>
            <td>{{ $val->instrumen }} {{ $val->instrument_lain }}</td>
            <td>{!! $val->metode_pemeriksaan !!} {{ $val->metode_lain }}</td>
            <td>{{ $val->Hasil }}</td>
        </tr>
        @endforeach
        @endif
        
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3"><label>Catatan :</label></td>
            <td colspan="2"><label>Nama Penanggung Jawab :</label></td>
        </tr>
        <tr>
            <td colspan="3">
                {{$datas->catatan}}
            </td>
            <td colspan="2">
                {{$datas->penanggung_jawab}}
            </td>
        </tr>
    </tfoot>
</table>
<table width="100%" border="0">
    <tr>
        <td width="70%"></td>
        <td width=""></td>
        <td width=""></td>
    </tr>
    <tr>
        <td>
            <div>
            </div>
        </td>
        <td>
            Hasil dikirim tanggal<br>
            Di cetak tanggal
        </td>
        <td style="margin-left: 5px"> : {{$register->updated_at->format('d-m-Y')}}<br> : {{$tanggal}}
        </td>
    </tr>
</table>
<br><br>
<table width="100%" border="0">
    <tr>
        <td width="70%">Mengetahui,</td>
        <td>.......................... , ...................</td>
    </tr>
    <tr>
        <td>Pimpinan Laboratorium</td>
        <td></td>
    </tr>
    <tr>
        <td>{{$perusahaan}}</td>
        <td>Penanggungjawab Pemeriksaan</td>
    </tr>
    <tr>
        <td height="50px"></td>
        <td></td>
    </tr>
    <tr>
        <td>Nama ...................................</td>
        <td>Nama ...................................</td>
    </tr>
    <tr>
        <td>NIP .......................................</td>
        <td>NIP .......................................</td>
    </tr>
</table>

<script type="text/php">
  if (isset($pdf)) {
    $font = $fontMetrics->getFont("Arial", "bold");
    $pdf->page_text(515, 783, "Hal. {PAGE_NUM} dari {PAGE_COUNT}", $font, 8, array(0, 0, 0));
  }
</script>