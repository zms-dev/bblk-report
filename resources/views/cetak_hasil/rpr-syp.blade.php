<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama, table.utama td, table.utama th {    
    border: 1px solid #333;
    text-align: left;
}

table.utama {
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td {
    padding: 5px;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br>IMUNOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}<br> HASIL PEMERIKSAAN RPR</label></center><br>
                    <p>1. IDENTITAS LABORATORIUM</p>
                    <div class="form-group">
                        @foreach($data as $val)
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta : {{ $val->kode_lab }}</label>
                        @break
                        @endforeach
                    </div>
                    <p>2. BAHAN UJI :</p>
                    <table class="utama">
                            @foreach($data as $val)
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>{{$val->tgl_diterima}}</td>
                                <td>Diperiksa tanggal :</td>
                                <td>{{$val->tgl_diperiksa}}</td>
                            </tr>
                            @break
                            @endforeach
                    </table>

                    <table class="utama">
                            <tr>
                                <td>Kode Bahan Uji</td>
                                <td>Baik/Jernih </td>
                                <td>Keruh </td>
                                <td>Lain-lain</td>
                            </tr>
                            <?php  
                                $no = 0;
                                $tabunga = ['I','II','III','IV'];
                            ?>
                            @foreach($data as $val)
                            <tr>
                                <td>{{$val->no_tabung}}</td>
                                <td><input type="checkbox" name="jenis[]" value="baik/jernih" {{ ($val->jenis == 'baik/jernih') ? 'checked' : '' }}></td>
                                <td><input type="checkbox" name="jenis[]" value="keruh" {{ ($val->jenis == 'keruh') ? 'checked' : '' }}></td>
                                <td>
                                    <input type="checkbox" name="jenis[]" value="lain-lain" {{ ($val->jenis == 'lain-lain') ? 'checked' : '' }}><br>
                                    {{$val->lain}}
                                </td>
                            </tr>
                            <?php $no++; ?>
                            @endforeach
                    </table>
                    <p>3. REAGEN</p>
                    <table class="utama">
                        <tr>
                            <td width="20%">Metode Pemeriksaan </td>
                            <td width="2%">:</td>
                            @foreach($data1 as $reagen)
                            <td style="text-transform: capitalize;">{{$reagen->metode}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td width="20%">Nama Reagen</td>
                            <td width="2%">:</td>
                            @foreach($data1 as $reagen)
                            <td>@if($reagen->reagen == 'Lain - lain'){{$reagen->reagen}} :<br>{{$reagen->reagen_lain}}@else{{$reagen->reagen}}@endif</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td width="20%">Nama Produsen</td>
                            <td width="2%">:</td>
                            @foreach($data1 as $reagen)
                            <td>{{$reagen->nama_produsen}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td width="20%">Nomor Lot/Batch</td>
                            <td width="2%">:</td>
                            @foreach($data1 as $reagen)
                            <td>{{$reagen->nomor_lot}}</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td width="20%">Tanggal Kadaluarsa</td>
                            <td width="2%">:</td>
                            @foreach($data1 as $reagen)
                            <td>{{$reagen->tgl_kadaluarsa}}</td>
                            @endforeach
                        </tr>
                    </table>
                    <p>4. HASIL PEMERIKSAAN </p>
                    <table class="utama">
                        <tr>
                            <th>Kode Bahan Uji</th>
                            <th>Interpretasi hasil </th>
                            <th>Titer</th>
                        </tr>
                        <?php  
                            $no = 0;
                            $val = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
                        ?>
                        @foreach($data2 as $hasil)
                        <tr>
                            <td>{{$hasil->kode_bahan_kontrol}}</td>
                            <td>{{$hasil->interpretasi}}</td>
                            <td>{{$hasil->titer}}</td>
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                    </table>
                    @foreach($data as $val)
                    <table class="utama">
                        <tr>
                            <td>Catatan :</td>
                            <td width="30%">Petugas yang melakukan pemeriksaan :</td>
                        </tr>
                        <tr>
                            <td>{{$val->keterangan}}</td>
                            <td>{{$val->petugas_pemeriksaan}}</td>
                        </tr>
                    </table>
                    @break
                    @endforeach
                    <br><br>
                    <table width="100%" border="0">
                        <tr>
                            <td width="70%">Mengetahui,</td>
                            <td>.......................... , ...................</td>
                        </tr>
                        <tr>
                            <td>Pimpinan Laboratorium</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>{{$perusahaan}}</td>
                            <td>Penanggungjawab Pemeriksaan</td>
                        </tr>
                        <tr>
                            <td height="50px"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Nama ...................................</td>
                            <td>Nama ...................................</td>
                        </tr>
                        <tr>
                            <td>NIP .......................................</td>
                            <td>NIP .......................................</td>
                        </tr>
                    </table>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>