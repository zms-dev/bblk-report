<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
    margin-top: 100px
}
table.utama, table.utama td, table.utama th {    
    border: 1px solid #333;
    text-align: left;
}

table.utama {
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td {
    padding: 5px;
}
</style>
<center><label>FORMULIR HASIL PEMERIKSAAN PENYELENGGARAAN NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI SIKLUS {{$siklus}} TAHUN {{$date}} <br> HASIL PEMERIKSAAN ANTI HIV</label></center><br>
<p>1. IDENTITAS LABORATORIUM</p>
<div class="form-group">
    @foreach($data as $val)
    <label for="nama" class="col-sm-2 control-label">Kode Peserta : {{ $val->kode_lab }}</label>
    @break
    @endforeach
</div>
<p>2. BAHAN UJI :</p>
<table class="utama">
        @foreach($data as $val)
        <tr>
            <td>Diterima tanggal :</td>
            <td>{{date('d-m-Y', strtotime($val->tgl_diterima))}}</td>
            <td>Diperiksa tanggal :</td>
            <td>{{date('d-m-Y', strtotime($val->tgl_diperiksa))}}</td>
        </tr>
        @break
        @endforeach
</table>

<table class="utama">
        <tr>
            <td rowspan="4" width="20%"> Kondisi bahan uji saat diterima</td>
            <td>Kode bahan uji</td>
            <td>Baik/Jernih </td>
            <td>Keruh </td>
            <td>Lain-lain</td>
        </tr>
        <?php $no =1;?>
        @foreach($data as $val)
        <tr>
            <td>I-{{$no++}}-{{$val->no_tabung}}</td>
            <td><input type="checkbox" name="jenis[]" value="baik/jernih" {{ ($val->jenis == 'baik/jernih') ? 'checked' : '' }}></td>
            <td><input type="checkbox" name="jenis[]" value="keruh" {{ ($val->jenis == 'keruh') ? 'checked' : '' }}></td>
            <td><input type="checkbox" name="jenis[]" value="lain-lain" {{ ($val->jenis == 'lain-lain') ? 'checked' : '' }}></td>
        </tr>
        @endforeach
</table>
<p>3. REAGEN</p>
<table class="utama">
    <tr>
        <td>Keterangan</td>
        <td>Tes I</td>
        <td>Tes II</td>
        <td>Tes III</td>
    </tr>
    <tr>
        <td>Metode Pemeriksaan </td>
        @foreach($data1 as $reagen)
        <td style="text-transform: capitalize;">
            @if($reagen->Metode == NULL || $reagen->Metode == '' || $reagen->Metode == '-') {{$reagen->metode}} @else {{$reagen->Metode}} @endif
        </td>
        @endforeach
    </tr>
    <tr>
        <td>Nama Reagen</td>
        @foreach($data1 as $reagen)
        <td>@if($reagen->reagen == 'Lain - lain')
                {{$reagen->reagen}} :<br>{{$reagen->reagen_lain}}
            @else
                {{$reagen->reagen}}
            @endif
        </td>
        @endforeach
    </tr>
    <tr>
        <td>Nama Produsen</td>
        @foreach($data1 as $reagen)
        <td>{{$reagen->nama_produsen}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Nomor Lot/Batch</td>
        @foreach($data1 as $reagen)
        <td>{{$reagen->nomor_lot}}</td>
        @endforeach
    </tr>
    <tr>
        <td>Tanggal Kadaluarsa</td>
        @foreach($data1 as $reagen)
        <td>{{date('d-m-Y', strtotime($reagen->tgl_kadaluarsa))}}</td>
        @endforeach
    </tr>
</table>
<p>4. HASIL PEMERIKSAAN </p>
<table class="utama">
    <tr>
        @foreach($data2 as $hasil)
        @if($hasil->tabung == '1')
        <td colspan="5">Kode Bahan Uji : I-1-{{$hasil->kode_bahan_kontrol}}</td>
        @break
        @endif
        @endforeach
    </tr>
    <tr>
        <th>Tes</th>
        <th>Abs atau OD (A) (Bila dengan  EIA / Setara)</th>
        <th>Cut Off (B) (Bila dengan EIA / Setara)</th>
        <th>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</th>
        <th>Interpretasi hasil </th>
    </tr>
    <?php  
        $no = 0;
        $val = ['I','II','III','IV'];
    ?>
    @foreach($data2 as $hasil)
    @if($hasil->tabung == '1')
    <tr>
        <td>{{$val[$no]}}</td>
        <td>{{$hasil->abs_od}}</td>
        <td>{{$hasil->cut_off}}</td>
        <td>{{$hasil->sco}}</td>
        <td>{{$hasil->interpretasi}}</td>
    </tr>
    <?php $no++; ?>
    @endif
    @endforeach
</table>

<table class="utama">
    <tr>
        @foreach($data2 as $hasil)
        @if($hasil->tabung == '2')
        <td colspan="5">Kode Bahan Uji : I-2-{{$hasil->kode_bahan_kontrol}}</td>
        @break
        @endif
        @endforeach
    </tr>
    <tr>
        <th>Tes</th>
        <th>Abs atau OD (A) (Bila dengan  EIA / Setara)</th>
        <th>Cut Off (B) (Bila dengan EIA / Setara)</th>
        <th>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</th>
        <th>Interpretasi hasil </th>
    </tr>
    <?php  
        $no = 0;
        $val = ['I','II','III','IV'];
    ?>
    @foreach($data2 as $hasil)
    @if($hasil->tabung == '2')
    <tr>
        <td>{{$val[$no]}}</td>
        <td>{{$hasil->abs_od}}</td>
        <td>{{$hasil->cut_off}}</td>
        <td>{{$hasil->sco}}</td>
        <td>{{$hasil->interpretasi}}</td>
    </tr>
    <?php $no++; ?>
    @endif
    @endforeach
</table>
<table class="utama">
    <tr>
        @foreach($data2 as $hasil)
        @if($hasil->tabung == '3')
        <td colspan="5">Kode Bahan Uji : I-3-{{$hasil->kode_bahan_kontrol}}</td>
        @break
        @endif
        @endforeach
    </tr>
    <tr>
        <th>Tes</th>
        <th>Abs atau OD (A) (Bila dengan  EIA / Setara)</th>
        <th>Cut Off (B) (Bila dengan EIA / Setara)</th>
        <th>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</th>
        <th>Interpretasi hasil </th>
    </tr>
    <?php  
        $no = 0;
        $val = ['I','II','III','IV'];
    ?>
    @foreach($data2 as $hasil)
    @if($hasil->tabung == '3')
    <tr>
        <td>{{$val[$no]}}</td>
        <td>{{$hasil->abs_od}}</td>
        <td>{{$hasil->cut_off}}</td>
        <td>{{$hasil->sco}}</td>
        <td>{{$hasil->interpretasi}}</td>
    </tr>
    <?php $no++; ?>
    @endif
    @endforeach
</table>
@foreach($data as $val)
<table class="utama" style="page-break-before: always;">
    <tr>
        <td>Catatan :</td>
        <td width="30%">Petugas yang melakukan pemeriksaan :</td>
    </tr>
    <tr>
        <td>{{$val->keterangan}}</td>
        <td>{{$val->petugas_pemeriksaan}}</td>
    </tr>
</table>
@break
@endforeach
<br><br>
<table width="100%" border="0">
    <tr>
        <td width="70%">Mengetahui,</td>
        <td>.......................... , ...................</td>
    </tr>
    <tr>
        <td>Pimpinan Laboratorium</td>
        <td></td>
    </tr>
    <tr>
        <td>{{$perusahaan}}</td>
        <td>Penanggungjawab Pemeriksaan</td>
    </tr>
    <tr>
        <td height="50px"></td>
        <td></td>
    </tr>
    <tr>
        <td>Nama ...................................</td>
        <td>Nama ...................................</td>
    </tr>
    <tr>
        <td>NIP .......................................</td>
        <td>NIP .......................................</td>
    </tr>
</table>