<title>Cetak Kwitansi PKS</title>
<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
    padding: 50px
}
table, td {    
    text-align: left;
}

.inti,.inti td, .inti th {    
    border: 1px solid #333;
    text-align: left;
    border-collapse: collapse;
}
.inti td, .inti th{
    padding: 5px;
}
.header{
position: fixed;
}

.header {
top: 0;
}

table.print-friendly tr td, table.print-friendly tr th {
    page-break-inside: avoid;
}
</style>

<body style="padding-bottom: 100px;padding-left: 0;padding-right: 0;padding-top: 0;">
<table width="100%" cellpadding="0" border="0">
    <thead>
        <tr>
            <th width="50px">
                <img alt="" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
            </th>
            <th align="center">
                <span style="font-size: 32px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                <span style="font-size: 20px">DIREKTORAT JENDERAL PELAYANAN KESEHATAN <br>BALAI BESAR LABORATORIUM KESEHATAN JAKARTA</span>
                <span><br/>Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560<br>Telp. (021) 4212524, 42804339 Facsimile (021) 4245516<br>Website : bblkjakarta.com ; Surat Elektronik : bblkjakarta@yahoo.co.id</span>
            </th>
        </tr>
        <tr>
            <th colspan="2"><hr></th>
        </tr>
    </thead>
</table>
<table width="100%">
    <tbody>
        <tr>
            <td width="100%">
                <table width="100%">
                    <tr>
                        <td width="45%"></td>
                        <th>Nomor</th>
                        <th>Tanggal</th>
                    </tr>
                    <tr>
                        <th style="text-align: right;"></th>
                        <td><b>Pendaftaran :</b>&nbsp;-</td>
                        <td>-</td>
                    </tr>
                </table>
                <table style="margin-top: 100px">
                    <tr>
                        <td>Telah diterima dari</td>
                        <td> : </td>
                        <th>Laboratorium yang menggunakan Perjanjian Kerjasama</th>
                    </tr>
                    <tr>
                        <td>Untuk Pembayaran</td>
                        <td> : </td>
                        <th> Keikutsertaan Program Nasional Pemantapan Mutu Eksternal</th>
                    </tr>
                    <tr>
                        <td>Dengan Jumlah</td>
                        <td> : </td>
                        <th>Rp. {{number_format($total->jumlah_tarif)}},-</th>
                    </tr>
                    <tr>
                        <td>Terbilang</td>
                        <td> : </td>
                        <th>{{ucwords(numbToWords($total->jumlah_tarif))}} Rupiah</th>
                    </tr>
                </table>
                <table width="100%" style="margin-top: 50px">
                    <tr>
                        <td width="50%"></td>
                        <td>Bendahara Penerimaan</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Balai Besar Laboratorium Kesehatan Jakarta</td>
                    </tr>
                    <tr>
                        <td height="100px"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Sutiani Rahayu, SE</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>NIP . 197903052007012023</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr nobr="true">
            <td width="100%">
                <div style="page-break-after: always;"></div>
                <table width="100%">
                    <tr>
                        <td width="45%"></td>
                        <th>Nomor</th>
                        <th>Tanggal</th>
                    </tr>
                    <tr>
                        <th style="text-align: right;"></th>
                        <td><b>Pendaftaran :</b> -</td>
                        <td>-</td>
                    </tr>
                </table>
                <table style="margin-top: 100px;">
                    <tr>
                        <td>Kepada</td>
                        <td> :</td>
                        <th>-</th>
                    </tr>
                    <tr>
                        <td>Perihal</td>
                        <td> :</td>
                        <th>Keikutsertaan Program Nasional Pemantapan Mutu Eksternal</th>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<table class="inti print-friendly" style="margin-top: 50px" nobr="true" width="100%">
    <tr>
        <th><center>No</center></th>
        <th><center>Nama Labolatorium</center></th>
        <th><center>Bidang</center></th>
        <th><center>Jumlah (Rp)</center></th>
    </tr>
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++ ?>
    <tr nobr="true">
        <td>{{$no}}</td>
        <td>{{$val->nama_lab}}</td>
        <td>{!!str_replace('|','<br/><br/>',$val->parameter) !!}</td>
        <td style="text-align: right;">{{number_format($val->jumlah_tarif)}}</td>
    </tr>
    @endforeach
    <tr>
        <td colspan="3" style="text-align: right;">Total :</td>
        <td style="text-align: right;">{{number_format($total->jumlah_tarif)}}</td>
    </tr>

</table>