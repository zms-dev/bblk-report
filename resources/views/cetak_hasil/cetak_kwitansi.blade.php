<title>Cetak Kwitansi</title>
<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
    padding: 50px
}
table, td {    
    text-align: left;
}

.inti,.inti td, .inti th {    
    border: 1px solid #333;
    text-align: left;
    
}
.inti td, .inti th{
    padding: 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
.coba{
    font-family: "Courier New", Courier, monospace;
}

.cobain{
    text-decoration-style: dotted;
}
.font{
     font-family: "Times New Roman";
     color:  #333;
     font-size: 14px;
}

.table1, .asw {
   border: 1px solid #333;;
}

.fonta{
    text-align: center; border-top: 7px solid white;  border-left: 7px solid white; border-right: 7px solid white;
    font-size: 15px;
}

.head{
    padding: 2%; font-size: 30px;  border: 7px solid white; text-align: right; color:  #333
}

</style>
<body style="padding-bottom: 10px;padding-left: 50;padding-right: 50;padding-top: 0;">
<table width="100%" cellpadding="0" border="0" style="margin-top: -120px;">
    <thead>
        <tr>
            <th height="250px">
            </th>
            <th align="center" width="100%">
            </th>
        </tr>
        <tr>
            <th colspan="2"></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2">
                <table width="100%" cellspacing="0" cellspacing="0" style="">
                    <tr>
                        <td colspan="3" align="right" class="head" >KUITANSI</td>
                    </tr>
                    <tr>
                        <td class="font" widtd="40%" style="border-left: 7px solid white"></td>
                        <td class="font fonta">Nomor</td>
                        <td class="font fonta">Tanggal</td>
                    </tr>
                    <tr>
                        <td class="font" style="text-align: right; border-top: 1px solid white;">Invoice :&nbsp;</td>
                        <td class="coba" style=" border: 1px solid #333;">{{$input['invoice']}}</td>
                        <td class="coba" style=" border: 1px solid #333;">{{($input['tanggal'])}}</td>
                    </tr>
                    <tr>
                        <td class="font" style="text-align: right; border-top: 1px solid white; ">No. Pendaftaran :&nbsp;</td>
                        <td class="coba" style=" border: 1px solid #333;">{{$data->kode_lebpes}}</td>
                        <td class="coba" style=" border: 1px solid #333;">{{($data->registrasi_tahun)}}</td>
                    </tr>
                </table>


                <?php $imun1 = 0; $imun2 = 0; $imun12 = 0;  $bidang = explode("|",$data->bidang); ?>
                @foreach($bidang as $valb)
                    <?php 
                        if($valb == "61" || $valb == "71" || $valb == "81" || $valb == "91"){
                            $imun1++;
                        }elseif($valb == "62" || $valb == "72" || $valb == "82" || $valb == "92"){
                            $imun2++;
                        }elseif($valb == "612" || $valb == "712" || $valb == "812" || $valb == "912"){
                            $imun12++;
                        }
                    ?>
                @endforeach
                <?php
                    $imunt1 = 0;
                    $imunt2 = 0;
                    $imunt12 = 0;
                    if($imun1 == 4){
                        $imunt1 = "200000";
                    }elseif($imun2 == 4){
                        $imunt2 = "200000";
                    }elseif($imun12 == 4){
                        $imunt12 = "400000";
                    }
                ?>
                <table style="margin-top: 100px" cellspacing="0" cellspacing="0">
                    <tr>
                        <td class="font">Telah Diterima Dari</td>
                        <td>: </td>
                        <td class="coba cobain" style=" border: 1px solid #333;">{{$input['diterima']}}</td>
                    </tr>
                    <tr>
                        <td class="font">Banyaknya Uang</td>
                        <td>: </td>
                        <td class="coba cobain" style="border: 1px solid #333;">Rp. 
                            {{number_format($data->jumlah_tarif - $imunt1 - $imunt2 - $imunt12)}},-</td>
                    </tr>
                    <tr>
                        <td class="font">Untuk Pembayaran</td>
                        <td>: </td>
                        <td class="coba cobain" style="border: 1px solid #333;">{{$input['pembayaran']}}</td>
                    </tr>
                </table>
                <table width="100%" style="margin-top: 150px">
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="50%"></td>
                        <td class="font">{{$input['kasubag']}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="font">Balai Besar Laboratorium Kesehatan Jakarta</td>
                    </tr>
                    <tr>
                        <td height="100px"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="font">{{$input['nama']}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="font">NIP . {{$input['nip']}}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="page-break-after: always;"></div>
                <table width="100%" cellspacing="0" cellspacing="0" style="margin-top: -120px">
                    <tr>
                        <td colspan="3" align="right" class="head" >INVOICE</td>
                    </tr>
                    <tr>
                        <td class="font" widtd="40%" style="border-left: 7px solid white"></td>
                        <td class="font fonta">Nomor</td>
                        <td class="font fonta">Tanggal</td>
                    </tr>
                    <tr>
                        <td class="font" style="text-align: right;">Invoice :&nbsp;</td>
                        <td class="coba" style="border: 1px solid #333;">{{($input['invoice'])}}</td>
                        <td class="coba" style="border: 1px solid #333;">{{($input['tanggal'])}}</td>
                    </tr>
                    <tr>
                        <td class="font" style="text-align: right;">No. Pendaftaran :&nbsp;</td>
                        <td class="coba" style=" border: 1px solid #333;">{{$data->kode_lebpes}}</td>
                        <td class="coba" style=" border: 1px solid #333;">{{($data->registrasi_tahun)}}</td>
                    </tr>
                </table>
                <table style="margin-top: 50px;" cellspacing="0" cellspacing="0">
                     <tr>
                        <td class="font" style="border: 1px solid #333; border-right: none;">Kepada</td>
                        <td style="border: 1px solid #333; border-left: none; border-right: none;">:</td>
                        <td class="coba" style="border: 1px solid #333; border-left: none;">{{$input['kepada']}}</td>
                    </tr>
                    <tr>
                        <td class="font" style="border: 1px solid #333; border-right: none;">Perihal</td>
                        <td style="border: 1px solid #333; border-left: none; border-right: none;">:</td>
                        <td class="coba " style="border: 1px solid #333; border-left: none;">{{$input['perihal']}}</td>
                    </tr>

                  <!--   <tr>
                        <td>Kepada</td>
                        <td> :</td>
                        <th>{{$data->nama_lab}}</th>
                    </tr>
                    <tr>
                        <td>Perihal</td>
                        <td> :</td>
                        <th>Keikutsertaan Program Nasional Pemantapan Mutu Eksternal</th>
                    </tr> -->
                </table>
                <table class="table1" cellspacing="0" cellpadding="0" style="margin-top: 30px; width: 100%;  border: 1px solid  #333;">
                    <tr nobr="true" >
                        <td class="asw font" style="padding: 1%; vertical-align: top; width: 5%"><center>No</center></td>
                        <!-- <td class="asw font" style="padding: 1%; vertical-align: top; width: 20%"><center>Nama Instansi</center></td> -->
                        <td class="asw font" style="padding: 1%; vertical-align: top;"><center>Bidang</center></td>
                        <td class="asw font" style="padding: 1%; vertical-align: top;" width="50px"><center>Siklus</center></td>
                        <td class="asw font" style="padding: 1%; vertical-align: top; width: 15%"><center>Harga Satuan (Rp)</center></td>
                    </tr>
                    @php $datas = explode("|",$data->parameter); $daftarbidang = 0; @endphp
                    @foreach($datas as $val)
                        <?php $daftarbidang++; ?>
                    @endforeach
                    @if($imun1 == 4 || $imun2 == 4 || $imun12 == 4)
                        <?php $daftarbidang = $daftarbidang - 4; ?>
                    @endif
                    <tr nobr="true" valign="top">
                        <td class="asw coba">
                            <ul style="margin-top:5px;list-style-type: none; margin-left: -35px">
                            @php $datas = explode("|",$data->parameter); $parameter = 0; $number = 1; @endphp
                            @foreach($datas as $val)
                                @if($parameter == $daftarbidang)
                                <li>{{$number++}}.</li>
                                @break
                                @endif
                                <li>{{$number++}}.</li>
                                <?php $parameter++; ?>
                            @endforeach
                            </ul>
                        </td>
                        <!-- <td class="asw coba" style="padding: 5px">{{$data->nama_lab}}</td> -->
                        <!-- <td class="asw coba" style="padding-top: 5px; width: 8%"><center>@if($data->siklus == '12') 1&nbsp;&&nbsp;2 @else {{$data->siklus}} @endif</center></td> -->
                        <td class="asw coba" style="">
                            <ol style="margin-top:5px; list-style-type: none; margin-left: -30px">
                            @php $datas = explode("|",$data->parameter); $parameter = 0; @endphp
                            @foreach($datas as $val)
                                @if($imun1 == 4 || $imun2 == 4 || $imun12 == 4)
                                    @if($val == 'Anti HIV' || $val == 'Syphilis Anti TP' || $val == 'HBsAg' || $val == 'Anti HCV' )
                                    @else
                                    <li>{!! $val !!}</li>
                                    @endif
                                <?php $parameter++; ?>
                                @else
                                <li>{!! $val !!}</li>
                                @endif
                            @endforeach
                            @if($imun1 == 4 || $imun2 == 4 || $imun12 == 4)
                            <li>Anti HIV, TP, HBsAg & Anti HCV</li>
                            @else
                            @endif
                            </ol>
                            <!-- {!!str_replace('|','<br/>',$data->parameter) !!} -->
                        </td>
                        <td class="asw coba">
                            <ul style="margin-top:5px;margin-left: -20px; list-style-type: none;">
                            @php $datas = explode("|",$data->siklus); $parameter = 0; @endphp
                            @foreach($datas as $val)
                                @if($parameter == $daftarbidang)
                                <li>@if($val == 12) 1&2 @else {!! $val !!} @endif</li>
                                @break
                                @endif
                                <li>@if($val == 12) 1&2 @else {!! $val !!} @endif</li>
                                <?php $parameter++; ?>
                            @endforeach
                            </ul>
                            <!-- {!!str_replace('|','<br/>',$data->parameter) !!} -->
                        </td>
                        <td class="asw coba" style="padding: 5px">
                            <ol style="margin-top:0px; list-style-type: none; text-align: right;">
                            <?php
                                $total = 0;
                                $datas = explode("|",$data->total); 
                                $harga = 0;
                                $parameter = 0;
                            ?>
                            @foreach($datas as $val)
                                <?php 
                                    $idbidang = substr($val, -1);
                                    $nilaiuang = substr($val, 0 , -2);
                                    $nilaiuang = str_replace("-","",$nilaiuang);
                                ?>
                                @if($imun1 == 4 || $imun2 == 4 || $imun12 == 4)
                                    @if($idbidang == 6 || $idbidang == 7 || $idbidang == 8 || $idbidang == 9 )
                                    @else
                                    <li>{!! number_format($nilaiuang,0,",",",") !!}</li>
                                    @endif
                                <?php $parameter++; ?>
                                @else
                                <li>{!! number_format($nilaiuang,0,",",",") !!}</li>
                                @endif
                            @endforeach
                            @if($imun1 == 4 || $imun2 == 4 || $imun12 == 4)
                                <li>@if($imun12 == 4){!! number_format(4000000,0,",",",") !!}@else{!! number_format(2000000,0,",",",") !!}@endif</li>
                            @else
                            @endif
                            </ol>
                        </td>
                    </tr>
                    <tr>
                        <td class="aswn font" colspan="3" style="text-align: right;">Jumlah Pembayaran  (Rp) &nbsp;</td>
                        <td class="asw coba" style="text-align: right; padding-right: 5px">&nbsp;&nbsp;
                            {{number_format($data->jumlah_tarif - $imunt1 - $imunt2 - $imunt12)}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<table width="100%" style="margin-top:20px" border="">
    <tr>
        <td class="font" colspan="2">Pembayaran ditujukan ke Rekening Bank Mandiri KCP Jakarta Cut Mutia <br>
        Nomor  Rekening : 123-00-0579251-2 atas nama RPL182 BBLK JKT Dana Kelolaan BLU.
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td width="50%"></td>
        <td class="font">{{$input['kasubag']}}</td>
    </tr>
    <tr>
        <td></td>
        <td class="font">Balai Besar Laboratorium Kesehatan Jakarta</td>
    </tr>
    <tr>
        <td height="100px"></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td class="font">{{$input['nama']}}</td>
    </tr>
    <tr>
        <td></td>
        <td class="font">NIP . {{$input['nip']}}</td>
    </tr>
</table>
</body>
<!-- <div class="header">
<table>
    <tr nobr="true">
        <td>
            <img alt="" src="http://webdesign.zamasco.com/img/logo-kemenkes.png" height="120px">
        </td>
        <td style="text-align: center;">
            <font style="font-size: 32px"><b>KEMENTERIAN KESEHATAN RI</b></font><br>
            <font style="font-size: 20px">DIREKTORAT JENDERAL PELAYANAN KESEHATAN <br>BALAI BESAR LABORATORIUM KESEHATAN JAKARTA</font>
            <font>Jalan Karangmenjangan No, 18 Jakarta - 60286<br>Telepon Pelayanan : (031) 5021451 ; Faksimili : (031) 5020388<br>Website : bblksurabaya.com ; Surat elektronik : bblksub@yahoo.co.id</font>
        </td>
    </tr>
</table>
<hr>
</div> -->