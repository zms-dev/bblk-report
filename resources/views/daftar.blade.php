@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Formulir Pendaftaran</div>

                <div class="panel-body">
                    @if(Session::has('message'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('message') }}
                      </div>
                    @endif
                    <form class="form-horizontal" id="myform" action="{{url('daftar')}}" method="post" enctype="multipart/form-data"  >
                      <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Laboratorium</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="nama_lab" name="nama_lab" placeholder="Nama Laboratorium" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="pemerintah" class="col-sm-3 control-label">Jenis FasYanKes</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="pemerintah" required>
                                <option></option>
                                @if(count($badan))
                                @foreach($badan as $val)
                                <option value="{{$val->id}}">{{$val->badan_usaha}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="alamat" class="col-sm-3 control-label">Alamat</label>
                        <div class="col-sm-9">
                          <textarea class="form-control" id="alamat" name="alamat" required></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Kode Pos</label>
                        <div class="col-sm-9">
                          <input type="text" name="kode_pos" class="form-control" id="kode_pos" placeholder="Kode Pos" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="provinsi" class="col-sm-3 control-label">Provinsi</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="provinsi" id="provinsi" required>
                                <option></option>
                                @foreach($provinsi as $val)
                                <option value="{{$val->id}}">{{$val->name}}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kota" class="col-sm-3 control-label">Kota / Kabupaten</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="kota" id="kota" required>
                                <option></option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kecamatan" class="col-sm-3 control-label">Kecamatan</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="kecamatan" id="kecamatan" required>
                                <option></option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kelurahan" class="col-sm-3 control-label">Kelurahan / Desa</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="kelurahan" id="kelurahan" required>
                                <option></option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="telp" class="col-sm-3 control-label">Telp / Fax</label>
                        <div class="col-sm-9">
                          <input type="text" name="telp" class="form-control" id="telp" placeholder="Telp / Fax / Email" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                          <input type="email" name="email" class="form-control" id="email" placeholder="Email" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="penanggung" class="col-sm-3 control-label">Penanggung Jawab</label>
                        <div class="col-sm-9">
                          <input type="text" name="penanggung_jawab" class="form-control" id="penanggung" placeholder="Penanggung Jawab" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="personal" class="col-sm-3 control-label">Personal</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="personal" id="personal" placeholder="Personal" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="hp" class="col-sm-3 control-label">No. HP</label>
                        <div class="col-sm-9">
                          <input type="text" name="no_hp" class="form-control" id="hp" placeholder="No. HP" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="siklus" class="col-sm-3 control-label">Siklus</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="siklus" id="siklus" required>
                                <option></option>
                                <option value="1">Siklus 1</option>
                                <!-- <option value="2">Siklus 2</option> -->
                                <option value="12">Siklus 1 & 2</option>
                            </select>
                        </div>
                      </div>
                    <div class="table-responsive">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                              <th>No</th>
                              <th colspan="2" style="text-align: center;">Bidang Pengujian Yang Dipilih</th>
                              <th>Tarif (Rp)</th>
                          </tr>
                        </thead>
                        <tbody class="body-siklus">

                        </tbody>
                          <!-- <tr id="datasiklus">
                            <td id="nosiklus"></td>
                            <td id="paramsiklus"></td>
                            <td id="bidangsiklus"></td>
                            <td id="tarifsiklus"></td>
                          </tr> -->
                      </table>
                    </div>
                    <label>Total Pembayaran:</label><br>
                    <div class="total"></div>
                    <label>Catatan :</label>
                    <p>*) Pilih metode pembayaran anda dengan benar.</p>
                    <label for="telp" class="control-label">Metode Pembayaran</label>
                    <select id="pembayaran" class="form-control" name="pembayaran">
                      <option></option>
                      <option value="transfer">Transfer</option>
                      <option value="sptjm">SPTJM</option>
                    </select>
                    <div id="transfer" class="pembayaran" style="display:none">
                      <label for="telp" class="control-label">File</label><small> *Bukti Pembayaran Format (JPG, atau PDF) Max (5 MB)</small>
                      <input type="file" class="myFile" placeholder="Nama Laboratorium" name="file">
                    </div>
                    <div id="sptjm" class="pembayaran" style="display:none">
                      <label for="telp" class="control-label">Text</label>
                      <textarea class="form-control" name="sptjm" style="display:none;">-</textarea>
                      <label for="telp" class="control-label">File</label><small> *Bukti Pembayaran Format (JPG, atau PDF) Max (5 MB)</small>
                      <input type="file" class="myFile" placeholder="Nama Laboratorium" name="file1">
                    </div>
                    <br>
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="tombolna">
                    </div>
                    @foreach($bidang as $val)
                    <input type="hidden" name="alias[]" value="{{$val->alias}}">
                    @endforeach
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"> 
$('.myFile').bind('change', function() {
  if(this.files[0].size >= 5120000){
      alert('File bukti pembayaran melebihi 5Mb!');
      $(".tombolna").html("");
  }else{
      $(".tombolna").html("");
        var exportna = "<input type=\"submit\" id=\"Daftar\" name=\"simpan\" value=\"Kirim\" class=\"btn btn-submit\">";
        $(".tombolna").append(exportna);
  }
});


$(function() {
    $('#pembayaran').change(function(){
        $('.pembayaran').hide();
        $('#' + $(this).val()).show();
    });
});

function formatNumber (num, currency) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

$("#siklus").change(function(){
  var val = $(this).val(), i, no = 0;
  var y = document.getElementById('datasiklus')
  $(".body-siklus").html("<tr><td colspan='4'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('datasiklus').'/'}}"+val,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';javascript
      $(".body-siklus").html("");
      if(addr.Hasil != undefined){
        var no = 1;
        $.each(addr.Hasil,function(e,item){
          var tarif2 = formatNumber(item.tarif / 2);
          var tarif1 = formatNumber(item.tarif);
          var tarif3 = formatNumber(item.tarif * 2);
          if (val != '12') {
            if (item.siklus != '12') {
                if (val == '1') {
                    if (item.kuota_1 > '0') {
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                        $(".body-siklus").append(html);  
                    }else{
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                        $(".body-siklus").append(html);  
                    }
                }else{
                    if (item.kuota_2 > '0') {
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                        $(".body-siklus").append(html);  
                    }else{
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                        $(".body-siklus").append(html);  
                    }
                }
            }else{
                if (val == '1') {
                    if (item.kuota_1 > '0') {
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                        $(".body-siklus").append(html);
                    }else{
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                        $(".body-siklus").append(html);
                    }
                }else{
                    if (item.kuota_2 > '0') {
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                        $(".body-siklus").append(html);
                    }else{
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                        $(".body-siklus").append(html);
                    }
                }
            }
          }else{
            if (item.siklus != '12') {
                if (item.kuota_1 > '0') {
                    var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                    $(".body-siklus").append(html); 
                }else{
                    var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                    $(".body-siklus").append(html); 
                } 
            }else{
                if (item.kuota_1 > '0') {
                    var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif3+"</td>";
                    $(".body-siklus").append(html);
                }else{
                    var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif3+"</td>";
                    $(".body-siklus").append(html);
                }
            }
          }
          no++;
        })
        $(function() {
          $("input.10").click(enable_cb);
        });
        $(function() {
          $("input.6").click(enable_na1);
          $("input.7").click(enable_na2);
          $("input.8").click(enable_na3);
          $("input.9").click(enable_na4);
        });

        function enable_cb() {
          if (this.checked) {
            $("input.6").prop("checked", false);
            $("input.6").attr("disabled", true);
            $("input.7").prop("checked", false);
            $("input.7").attr("disabled", true);
            $("input.8").prop("checked", false);
            $("input.8").attr("disabled", true);
            $("input.9").prop("checked", false);
            $("input.9").attr("disabled", true);
          } else {
            $("input.6").removeAttr("disabled");
            $("input.7").removeAttr("disabled");
            $("input.8").removeAttr("disabled");
            $("input.9").removeAttr("disabled");
          }
        }

        $("input[type=checkbox]").click(function(){
          var waw1 = $("input.6"), waw2 = $("input.7"), waw3 = $("input.8"), waw4 = $("input.9");
          if (waw1.prop('checked') || waw2.prop('checked') || waw3.prop('checked') || waw4.prop('checked')) {
            $("input.10").attr("disabled", true);
          } else {
            $("input.10").removeAttr("disabled");
          }
          if (waw1.prop('checked') && waw2.prop('checked') && waw3.prop('checked') && waw4.prop('checked')) {
            $("input.6").prop('checked',false);
            $("input.6").attr('disabled',true);
            $("input.7").prop('checked',false);
            $("input.7").attr('disabled',true);
            $("input.8").prop('checked',false);
            $("input.8").attr('disabled',true);
            $("input.9").prop('checked',false);
            $("input.9").attr('disabled',true);
            $("input.10").prop('checked',true);
          }
          var cekbok  = $("input[type=checkbox]"),
              total   = 0;
          cekbok.each(function(){
            var e = $(this).parents('tr').find('.tarifsiklus').html().replace(/,/g,'');
            if($(this).prop('checked') == true){
              total = total + parseInt(e);
            }
          });
          $(".total").html(formatNumber(total));
        });
      }
      return false;
    }
  });
});

$("#provinsi").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('kota');
  $("#kecamatan").val("");
  $("#kelurahan").val("");
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getkota').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});

$("#kota").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('kecamatan');
  $("#kelurahan").val("");
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getkecamatan').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});


$("#kecamatan").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('kelurahan');
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getkelurahan').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});


var elements = document.getElementsByTagName("INPUT");
var select = document.getElementsByTagName("select");
var textarea = document.getElementsByTagName("textarea");
for (var i = 0; i < elements.length; i++) {
    elements[i].oninvalid = function(e) {
        e.target.setCustomValidity("");
        if (!e.target.validity.valid) {
            e.target.setCustomValidity("Tolong isi input yang kosong");
        }
    };
    elements[i].oninput = function(e) {
        e.target.setCustomValidity("");
    };
}
for (var i = 0; i < select.length; i++) {
    select[i].oninvalid = function(e) {
        e.target.setCustomValidity("");
        if (!e.target.validity.valid) {
            e.target.setCustomValidity("Tolong isi input yang kosong");
        }
    };
    select[i].oninput = function(e) {
        e.target.setCustomValidity("");
    };
}
for (var i = 0; i < textarea.length; i++) {
    textarea[i].oninvalid = function(e) {
        e.target.setCustomValidity("");
        if (!e.target.validity.valid) {
            e.target.setCustomValidity("Tolong isi input yang kosong");
        }
    };
    textarea[i].oninput = function(e) {
        e.target.setCustomValidity("");
    };
}

$('#myform').submit(function() {
  if($('.checkbidang:checkbox:checked').length == 0){
    alert('Pilih minimal 1 Bidang Pengujian');
    return false;
  }
  var $btn = $(this).button('loading')
});
</script>
@endsection