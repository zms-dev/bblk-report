@section('menudashboard')
<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dashboard<b class="caret"></b></a>
<ul class="dropdown-menu">
    <li>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Grafik<b class="caret"></b></a>
        <ul class="dropdown-menu" style="top: 0px">
            <li><a href="{{url('grafik/rekap-transaksi-parameter')}}">Rekap Transaksi berdasarkan Parameter</a></li>
            <li><a href="{{url('grafik/rekap-peserta-instansi')}}">Rekap Peserta berdasarkan Instansi</a></li>
            <li><a href="{{url('grafik/rekap-peserta-pembayaran')}}">Rekap Peserta berdasarkan Pembayaran</a></li>
        </ul>
    </li>
    <!-- <li>
        <a href="{{url('rekap-peserta-bidang')}}">Rekap Seluruh Peserta dan Bidang</a>
    </li> -->
    <li>
        <a href="{{url('rekap-peserta-provinsi')}}">Rekap Peserta berdasarkan Provinsi</a>
    </li>
    <li>
        <a href="{{url('rekap-instansi-provinsi')}}">Rekap Instansi berdasarkan Provinsi</a>
    </li>
    <li>
        <a href="{{url('rekap-jumlah-peserta-parameter')}}">Rekap Peserta Per Parameter Berdasarkan Provinsi</a>
    </li>
    <li>
        <a href="{{url('rekap-transaksi-parameter')}}">Rekap Transaksi berdasarkan Parameter</a>
    </li>
    <li>
        <a href="{{url('rekap-peserta-instansi')}}">Rekap Peserta berdasarkan Instansi</a>
    </li>
    <li>
        <a href="{{url('rekap-transaksi-peserta-parameter')}}">Rekap Tarif Peserta berdasarkan Parameter</a>
    </li>
    <li>
        <a href="{{url('rekap-peserta-parameter')}}">Rekap Peserta berdasarkan Instansi dan Parameter</a>
    </li>
</ul>
@endsection
