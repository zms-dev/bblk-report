@section('menuevaluasi')
<a href="#" class="dropdown-toggle" data-toggle="dropdown">Evaluasi<b class="caret"></b></a>
<ul class="dropdown-menu">
@if (Auth::guest())
@else
    @if(Auth::user()->penyelenggara == '1' || Auth::user()->penyelenggara == '2' || Auth::user()->penyelenggara == '11' || Auth::user()->penyelenggara == '12' || Auth::user()->penyelenggara == '13')
    <!--Hematologi-->
    <!-- <li>
        <a href="{{url('evaluasi/hematologi/rekap-hasil')}}">Rekap Hasil Per Parameter</a>
    </li> -->
    @if(Auth::user()->penyelenggara == '1')
    <li>
        <a href="{{url('evaluasi/rekap-hasil-peserta/hematologi')}}">Rekap Input Peserta</a>
    </li>
<!--     <li>
        <a href="{{url('evaluasi/laporan-peserta-parameter/hematologi')}}">Laporan Hasil Peserta Per Parameter</a>
    </li> -->
    <!-- <li>
        <a href="{{url('evaluasi/laporan-peserta-alat/hematologi')}}">Laporan Hasil Peserta Per Alat / Instrument</a>
    </li> -->
    <li>
        <a href="{{url('evaluasi/laporan-peserta-metode/hematologi')}}">Laporan Hasil Input Peserta</a>
    </li>
    <li>
        <a href="{{url('evaluasi/input-sd-median/hematologi')}}">Input Nilai SD dan Median Per Parameter</a>
    </li>
    <!-- <li>
        <a href="{{url('evaluasi/input-sd-median-alat/hematologi')}}">Input Nilai SD dan Median Per Alat / Instrument</a>
    </li> -->
    <li>
        <a href="{{url('evaluasi/input-sd-median-metode/hematologi')}}">Input Nilai SD dan Median Per Metode</a>
    </li>
    <li>
        <a href="{{url('evaluasi/input-ring/hematologi')}}">Input Nilai Rentang Parameter</a>
    </li>
    <li>
        <a href="{{url('evaluasi/input-ring-metode/hematologi')}}">Input Nilai Rentang Metode</a>
    </li>
    <li>
        <a href="{{url('evaluasi/hitung-zscore/hematologi')}}">Hitung Z-Score Parameter</a>
    </li>
    <!-- <li>
        <a href="{{url('evaluasi/hitung-zscore-alat/hematologi')}}">Hitung Z-Score Alat / Instrumen</a>
    </li> -->
    <li>
        <a href="{{url('evaluasi/hitung-zscore-metode/hematologi')}}">Hitung Z-Score Metode</a>
    </li>
    <li>
        <a href="{{url('evaluasi/hitung-zscore-cetak/hematologi')}}">Cetak Z-Score</a>
    </li>
    <li>
        <a href="{{url('evaluasi/rekap-zscore-parameter/hematologi')}}">Rekap Z-Score Seluruh Peserta Per parameter</a>
    </li>
    <!-- <li>
        <a href="{{url('evaluasi/rekap-zscore-parameter-alat/hematologi')}}">Rekap Z-Score Seluruh Peserta Per Alat</a>
    </li> -->
    <li>
        <a href="{{url('evaluasi/rekap-zscore-parameter-metode/hematologi')}}">Rekap Z-Score Seluruh Peserta Per Metode</a>
    </li>
    <!-- <li>
        <a href="{{url('evaluasi/rekapitulasi-hasil/hematologi')}}">Rekapitulasi Hasil per Parameter </a>
    </li> -->
    <li>
        <a href="{{url('grafik/hitung-zscore-semua/hematologi')}}">Grafik Z-Score</a>
    </li>
    <!-- <li>
        <a href="{{url('grafik/hitung-zscore/hematologi')}}">Grafik Z-Score per Parameter</a>
    </li>
    <li>
        <a href="{{url('grafik/hitung-zscore-alat/hematologi')}}">Grafik Z-Score per Alat / Instrumen</a>
    </li>
    <li>
        <a href="{{url('grafik/hitung-zscore-metode/hematologi')}}">Grafik Z-Score per Metode</a>
    </li> -->
    <li>
        <a href="{{url('grafik/nilai-sama/hematologi')}}">Grafik Nilai yang sama</a>
    </li>
    <!-- <li>
        <a href="{{url('grafik/nilai-sama-alat/hematologi')}}">Grafik Alat / Instrumen dengan Nilai yang sama</a>
    </li>
    <li>
        <a href="{{url('grafik/nilai-sama-metode/hematologi')}}">Grafik Metode dengan Nilai yang sama</a>
    </li> -->
    <li>
        <a href="{{url('evaluasi/upload-laporan-akhir')}}">Upload Laporan Akhir</a>
    </li>
    <li>
        <a href="{{url('hematologi/sertifikat')}}">Sertifikat</a>
    </li>
    @elseif(Auth::user()->penyelenggara == '2')
    <li>
        <a href="{{url('evaluasi/rekap-hasil-peserta/kimiaklinik')}}">Rekap Input Peserta</a>
    </li>
    <li>
        <a href="{{url('evaluasi/laporan-pelaksanaan/kimiaklinik')}}">Laporan Pelaksanaan PNPME</a>
    </li>
    <!-- <li>
        <a href="{{url('evaluasi/laporan-peserta-parameter/kimiaklinik')}}">Laporan Hasil Peserta Per Parameter</a>
    </li> -->
    <!-- <li>
        <a href="{{url('evaluasi/laporan-peserta-alat/kimiaklinik')}}">Laporan Hasil Peserta Per Alat / Instrument</a>
    </li> -->
    <li>
        <a href="{{url('evaluasi/laporan-peserta-metode/kimiaklinik')}}">Laporan Hasil Input Peserta</a>
    </li>
    <li>
        <a href="{{url('evaluasi/laporan-jumlah-kelompok/kimiaklinik')}}">Laporan Jumlah Kelompok Peserta</a>
    </li>
    <li>
        <a href="{{url('evaluasi/input-sd-median/kimiaklinik')}}">Input Nilai SD dan Median Per Parameter</a>
    </li>
    <!-- <li>
        <a href="{{url('evaluasi/input-sd-median-alat/kimiaklinik')}}">Input Nilai SD dan Median Per Alat / Instrument</a>
    </li> -->
    <li>
        <a href="{{url('evaluasi/input-sd-median-metode/kimiaklinik')}}">Input Nilai SD dan Median Per Metode</a>
    </li>
    <li>
        <a href="{{url('evaluasi/input-ring/kimiaklinik')}}">Input Rentang Per Parameter</a>
    </li>
    <!-- <li>
        <a href="{{url('evaluasi/input-ring/kimiaklinik-alat')}}">Input Nilai Range Per Alat</a>
    </li> -->
    <li>
        <a href="{{url('evaluasi/input-ring-metode/kimiaklinik')}}">Input Rentang Per Metode</a>
    </li>
    <li>
        <a href="{{url('evaluasi/hitung-zscore/kimiaklinik')}}">Hitung Z-Score</a>
    </li>
    <!-- <li>
        <a href="{{url('evaluasi/hitung-zscore-alat/kimiaklinik')}}">Hitung Z-Score Alat / Instrumen</a>
    </li> -->
    <!-- <li>
        <a href="{{url('evaluasi/hitung-zscore-metode/kimiaklinik')}}">Hitung Z-Score Metode</a>
    </li> -->
    <!-- <li>
        <a href="{{url('grafik/hitung-zscore/kimiaklinik')}}">Grafik Z-Score per Parameter</a>
    </li>
    <li>
        <a href="{{url('grafik/hitung-zscore-alat/kimiaklinik')}}">Grafik Z-Score per Alat / Instrumen</a>
    </li>
    <li>
        <a href="{{url('grafik/hitung-zscore-metode/kimiaklinik')}}">Grafik Z-Score per Metode</a>
    </li> -->
    <li>
        <a href="{{url('evaluasi/rekapitulasi-hasil/kimiaklinik')}}">Rekapitulasi Hasil per Parameter </a>
    </li>
    <!-- <li>
        <a href="{{url('grafik/nilai-sama/kimiaklinik')}}">Grafik Parameter </a>
    </li> -->
    <li>
        <a href="{{url('grafik/hitung-zscore-semua/kimiaklinik')}}">Grafik Z-Score</a>
    </li>
    <!-- <li>
        <a href="{{url('grafik/nilai-sama-alat/kimiaklinik')}}">Grafik Alat / Instrumen </a>
    </li> -->
    <li>
        <a href="{{url('grafik/nilai-sama/kimiaklinik')}}">Grafik Nilai yang sama</a>
    </li>
    <li>
        <a href="{{url('evaluasi/upload-laporan-akhir')}}">Upload Laporan Akhir</a>
    </li>
    <li>
        <a href="{{url('kimiaklinik/sertifikat')}}">Sertifikat</a>
    </li>
    @elseif(Auth::user()->penyelenggara == '11')
    <li>
        <a href="{{url('evaluasi/laporan-peserta-parameter/kimiaair')}}">Laporan Hasil Peserta Per Parameter</a>
    </li>
    <li>
        <a href="{{url('evaluasi/input-sd-median/kimiaair')}}">Input Nilai SD dan Median Per Parameter</a>
    </li>
{{--     <li>
        <a href="{{url('evaluasi/input-ring/kimiaair')}}">Input Nilai Range</a>
    </li>
 --}}
    <li>
        <a href="{{url('evaluasi/hitung-zscore/kimiaair')}}">Hitung Z-Score Parameter</a>
    </li>
    <li>
        <a href="{{url('evaluasi/rekap-zscore-parameter/kimiaair')}}">Rekap Z-Score Seluruh Peserta Per parameter</a>
    </li>
    <li>
        <a href="{{url('grafik/hitung-zscore/kimiaair')}}">Grafik Z-Score per Parameter</a>
    </li>
    <li>
        <a href="{{url('grafik/nilai-sama/kimiaair')}}">Grafik Parameter dengan Nilai Sama</a>
    </li>
    <li>
        <a href="{{url('evaluasi/rekap-nilai-peserta/kimiaair')}}">Grafik Nilai Peserta</a>
    </li>
    <li>
        <a href="{{url('evaluasi/upload-laporan-akhir')}}">Upload Laporan Akhir</a>
    </li>
    <li>
        <a href="{{url('kimkes/sertifikat')}}">Sertifikat</a>
    </li>
    @elseif(Auth::user()->penyelenggara == '12')
    <li>
        <a href="{{url('evaluasi/laporan-peserta-parameter/kimiaairterbatas')}}">Laporan Hasil Peserta Per Parameter</a>
    </li>
    <li>
        <a href="{{url('evaluasi/input-sd-median/kimiaairterbatas')}}">Input Nilai SD dan Median Per Parameter</a>
    </li>
  {{--   <li>
        <a href="{{url('evaluasi/input-ring/kimiaairterbatas')}}">Input Nilai Range</a>
    </li> --}}
    <li>
        <a href="{{url('evaluasi/hitung-zscore/kimiaairterbatas')}}">Hitung Z-Score Parameter</a>
    </li>
    <li>
        <a href="{{url('evaluasi/rekap-zscore-parameter/kimiaairterbatas')}}">Rekap Z-Score Seluruh Peserta Per parameter</a>
    </li>
    <li>
        <a href="{{url('grafik/hitung-zscore/kimiaairterbatas')}}">Grafik Z-Score per Parameter</a>
    </li>
    <li>
        <a href="{{url('grafik/nilai-sama/kimiaairterbatas')}}">Grafik Parameter dengan Nilai Sama</a>
    </li>

    <li>

        <a href="{{url('evaluasi/rekap-nilai-peserta/kimiaairterbatas')}}">Grafik Nilai Peserta</a>
    </li>

    <li>
        <a href="{{url('evaluasi/upload-laporan-akhir')}}">Upload Laporan Akhir</a>
    </li>
    <li>
        <a href="{{url('kimkest/sertifikat')}}">Sertifikat</a>
    </li>
    @elseif(Auth::user()->penyelenggara == '13')
    <li>
        <a href="{{url('evaluasi/rekap-hasil-peserta/bakteri')}}">Rekap Cetakan Peserta</a>
    </li>
    <li>
        <a href="{{url('evaluasi/input-rujukan/bakteri')}}">Input Rujukan</a>
    </li>
    <li>
        <a href="{{url('evaluasi/bakteri/penilaian-uji-kepekaan-antibiotik')}}">Penilaian Uji Kepekaan Antibiotik</a>
    </li>
    <li>
        <a href="{{url('evaluasi/bakteri/penilaian')}}">Penilaian Evaluasi Identifikasi</a>
    </li>
    <li>
        <a href="{{url('evaluasi/bakteri/evaluasi_uji_kepekaan_antibiotik')}}">Penilaian Evaluasi Uji Kepekaan Antibiotik</a>
    </li>
    <li>
        <a href="{{url('evaluasi/bakteri/grafik-keterangan-peserta')}}">Grafik Keterangan Evaluasi</a>
    </li>
    <li>
        <a href="{{url('evaluasi/upload-laporan-akhir')}}">Upload Laporan Akhir</a>
    </li>
    <li>
        <a href="{{url('bac/sertifikat')}}">Sertifikat</a>
    </li>
    @endif
    <!-- <li>
        <a href="{{url('#')}}">Grafik perbandingan dengan seluruh peserta berdasarkan z-score yang sama</a>
    </li>
    <li>
        <a href="{{url('#')}}">Grafik peserta dengan metode sama berdasarkan nilai</a>
    </li>
    <li>
        <a href="{{url('#')}}">Grafik peserta dengan metode sama berdasarkan z-score</a>
    </li>
    <li>
        <a href="{{url('#')}}">Grafik peserta dengan alat (instrument) sama berdasarkan nilai</a>
    </li>
    <li>
        <a href="{{url('#')}}">Grafik peserta dengan alat (instrument) sama berdasarkan z-score</a>
    </li> -->

    @elseif(Auth::user()->penyelenggara == '6' || Auth::user()->penyelenggara == '7' || Auth::user()->penyelenggara == '8' || Auth::user()->penyelenggara == '9')
    <!--Imunologi-->
    <li>
        <a href="{{url('admin/rujukan-imunologi')}}">Rujukan</a>
    </li>
    <li>
        <a href="{{url('evaluasi/imunologi')}}">Penilaian dan Lampiran Evaluasi</a>
    </li>
    <!-- <li>
        <a href="{{url('evaluasi/cetak-imunologi')}}">Cetak Lampiran Evaluasi</a>
    </li> -->
    <li>
        <a href="{{url('evaluasi/rekap-peserta-belum-evaluasi')}}">Rekap Input Peserta Format A</a>
    </li>
    <li>
        <a href="{{url('evaluasi/laporan-hasil-peserta')}}">Rekap Input Peserta Format B</a>
    </li>
    <li>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Grafik<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li>
                <a href="{{url('evaluasi/rekap-peserta-instansi')}}">Rekap Peserta per Instansi</a>
            </li>
            <li>
                <a href="{{url('evaluasi/rekap-persen-instansi')}}">Persentase Peserta per Instansi</a>
            </li>
            <li>
                <a href="{{url('evaluasi/rekap-reagen')}}">Rekapitulasi Reagen</a>
            </li>
            <li>
                <a href="{{url('evaluasi/rekap-reagen-persen')}}">Persentase Reagen</a>
            </li>
            <li>
                <a href="{{url('evaluasi/grafik-peserta-kirim-hasil')}}">Grafik Peserta Kirim Hasil</a>
            </li>
            <li>
                <a href="{{url('evaluasi/grafik-rujukan-instansi')}}">Grafik Rujukan per Jenis Instansi</a>
            </li>
            @if(Auth::user()->penyelenggara == '6')
            <li>
                <a href="{{url('evaluasi/grafik-rujukan-bahan-uji')}}">Grafik Rujukan per Bahan Uji</a>
            </li>
            <li>
                <a href="{{url('evaluasi/grafik-tahap-strategi')}}">Grafik Tahap Pemeriksaan Kesesuaian Strategi</a>
            </li>
            <li>
                <a href="{{url('evaluasi/grafik-jenis-reagen')}}">Grafik Jenis Reagen yang Sesuai Hasil Evaluasi</a>
            </li>
            <li>
                <a href="{{url('evaluasi/grafik-urutan-reagen')}}">Grafik Urutan Reagen yang Sesuai Hasil Evaluasi</a>
            </li>
            <li>
                <a href="{{url('evaluasi/grafik-tahap-pemeriksaan')}}">Grafik Kesesuaian Strategi</a>
            </li>
            <li>
                <a href="{{url('evaluasi/grafik-ketepatan-dan-kesesuaian')}}">Grafik Ketepatan Hasil & Kesesuaian Strategi</a>
            </li>
            @endif

            <li>
                <a href="{{url('evaluasi/grafik-rujukan-reagen')}}">Grafik Rujukan per Reagen</a>
            </li>
            <li>
                <a href="{{url('evaluasi/grafik-nilai-peserta')}}">Grafik Ketepatan Hasil Pemeriksaan</a>
            </li>
        </ul>
    </li>
    <!-- <li>
        <a href="{{url('#')}}">Laporan Hasil Peserta dengan Strategi baik</a>
    </li>
    <li>
        <a href="{{url('#')}}">Grafik Sesuai Strategi dan tidak sesuai Strategi</a>
    </li>
    <li>
        <a href="{{url('#')}}">Baik, tidak baik, tidak dapat di Nilai</a>
    </li>
    <li>
        <a href="{{url('#')}}">Upload Laporan Akhir</a>
    </li> -->
    <li>
        <a href="{{url('evaluasi/rekap-evaluasi-peserta')}}">Rekap Hasil Evaluasi </a>
    </li>
    <li>
        <a href="{{url('evaluasi/upload-laporan-akhir')}}">Upload Laporan Akhir</a>
    </li>
    <li>
        <a href="{{url('evaluasi/sertifikat')}}">Sertifikat</a>
    </li>
    @elseif(Auth::user()->penyelenggara == '4' || Auth::user()->penyelenggara == '5'  || Auth::user()->penyelenggara == '10')
    <!--Malaria-->
    @if(Auth::user()->penyelenggara == '10')
    <li>
        <a href="{{url('grafik/grafik-malaria')}}">Grafik Malaria</a>
    </li>
    <li>
        <a href="{{url('admin/rujukan-malaria')}}">Rujukan Malaria</a>
    </li>
    <li>
        <a href="{{url('evaluasi/malaria/penilaian')}}">Penilaian Malaria</a>
    </li>
    <li>
        <a href="{{url('evaluasi/malaria/rekap-grafik')}}">Grafik Instansi per Peserta</a>
    </li>
    <li>
        <a href="{{url('evaluasi/malaria/grafik-kesimpulan-evaluasi')}}">Grafik Kesimpulan Evaluasi</a>
    </li>
    <li>
        <a href="{{url('evaluasi/malaria/rekap-peserta')}}">Rekap Laporan Peserta Malaria</a>
    </li>
    <li>
        <a href="{{url('evaluasi/upload-laporan-akhir')}}">Upload Laporan Akhir</a>
    </li>
    <li>
        <a href="{{url('evaluasi-mikro/sertifikat')}}">Sertifikat</a>
    </li>
    @elseif(Auth::user()->penyelenggara == '4')
    <li>
        <a href="{{url('admin/rujukan-bta')}}">Rujukan</a>
    </li>
    <li>
        <a href="{{url('evaluasi/bta/penilaian')}}">Penilaian BTA</a>
    </li>
  
    <li>
        <a href="{{url('evaluasi/bta/rekap-grafik')}}">Grafik Peserta PNPME</a>
    </li>
    <li>
        <a href="{{url('evaluasi/bta/grafik-jawaban-peserta')}}">Grafik Jawaban Peserta</a>
    </li>
    <li>
        <a href="{{url('evaluasi/bta/grafik-kesimpulan-peserta')}}">Grafik Kesimpulan Hasil Peserta</a>
    </li>
    <li>
        <a href="{{url('evaluasi/bta/grafik-provinsi-peserta')}}">Grafik Peserta per Provinsi</a>
    </li>
    <li>
        <a href="{{url('evaluasi/bta/cetakan')}}">Rekap Cetak Evaluasi</a>
    </li>
    <li>
        <a href="{{url('evaluasi/bta/rekap-peserta')}}">Rekap Laporan Input Peserta</a>
    </li>
    <li>
        <a href="{{url('evaluasi/upload-laporan-akhir')}}">Upload Laporan Akhir</a>
    </li>
    <li>
        <a href="{{url('evaluasi-mikro/sertifikat')}}">Sertifikat</a>
    </li>
    @elseif(Auth::user()->penyelenggara == '5')
    <li>
        <a href="{{url('admin/rujukan-tc')}}">Rujukan</a>
    </li>
    <li>
        <a href="{{url('evaluasi/telur-cacing/penilaian')}}">Penilaian TC</a>
    </li>
    <li>
        <a href="{{url('evaluasi/telur-cacing/cetakan')}}">Cetakan Evaluasi</a>
    </li>
    <li>
    <li>
        <a href="{{url('grafik/grafik-tc')}}">Grafik Nilai Peserta</a>
    </li>
        <a href="{{url('evaluasi/telur-cacing/rekap-grafik')}}">Rekap dan Grafik Peserta TC</a>
    </li>
    <li>
        <a href="{{url('evaluasi/telur-cacing/rekap-peserta')}}">Rekap Laporan Input Peserta</a>
    </li>
    <li>
        <a href="{{url('evaluasi/upload-laporan-akhir')}}">Upload Laporan Akhir</a>
    </li>
    <li>
        <a href="{{url('evaluasi-mikro/sertifikat')}}">Sertifikat</a>
    </li>
    @endif

    <!-- <hr>
    <li>
        <a href="{{url('#')}}">Lampiran Evaluasi Peserta</a>
    </li>
    <li>
        <a href="{{url('#')}}">Rekap Peserta Berdasarkan Instansi</a>
    </li>
    <li>
        <a href="{{url('#')}}">Data Hasil bahan Uji</a>
    </li>
    <li>
        <a href="{{url('#')}}">Rekap Hasil Peserta</a>
    </li>
    <li>
        <a href="{{url('#')}}">Rekapitulasi Sensitifitas, Spesifisitas dan Akurasi Spesies peserta</a>
    </li>
    <li>
        <a href="{{url('#')}}">Grafik Prosentase peserta dengan Sensitifitas ≥ 70%</a>
    </li>
    <li>
        <a href="{{url('#')}}">Grafik Prosentase peserta dengan Spesifisitas ≥ 70%</a>
    </li>
    <li>
        <a href="{{url('#')}}">Grafik Prosentase peserta dengan Akurasi Spesies ≥ 70%</a>
    </li>
    <li>
        <a href="{{url('#')}}">Tabel Ketepatan Deteksi Tiap Jenis Spesies </a>
    </li>
    <li>
        <a href="{{url('#')}}">Grafik Hasil Peserta PNPME Mikroskopis Malaria </a>
    </li> -->

    @elseif(Auth::user()->penyelenggara == '3')
    <!--Urinalisa-->
    <li>
        <a href="{{url('evaluasi/rekap-hasil-peserta/urinalisa')}}">Cetak Hasil Peserta</a>
    </li>
    <li>
        <a href="{{url('/evaluasi/urinalisa/rekap-peserta')}}">Rekap Input Peserta</a>
    </li>
    <li>
        <a href="{{url('admin/rujukan-urinalisa')}}">Tabel Rujukan Parameter</a>
    </li>
    <li>
        <a href="{{url('admin/rujukan-urinalisa-metode')}}">Tabel Rujukan Metode</a>
    </li>
    <li>
        <a href="{{url('/evaluasi/urinalisasi')}}">Evaluasi Urinalisa</a>
    </li>
    <li>
        <a href="{{url('/penilaian/parameter-urinalisa')}}">Histogram hasil Seluruh Peserta</a>
    </li>
    <li>
        <a href="{{url('/penilaian/reagen-urinalisa')}}">Grafik penggunaan reagen Peserta</a>
    </li>
    <li>
        <a href="{{url('/penilaian/skoring-urinalisa')}}">Grafik Skoring Seluruh Peserta</a>
    </li>
    <!-- <li>
        <a href="{{url('/penilaian/reagen-kehamilan-urinalisa')}}">Grafik Penggunaan reagen kehamilan</a>
    </li> -->

    <li>
        <a href="{{url('evaluasi/upload-laporan-akhir')}}">Upload Laporan Akhir</a>
    </li>
    <li>
        <a href="{{url('urinalisa/sertifikat')}}">Sertifikat</a>
    </li>
    @endif
@endif
</ul>
@endsection
