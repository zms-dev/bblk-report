@section('menu')
@if (Auth::guest())
    <?php
        $daftar = DB::table('tb_hide_menu')->where('menu','=','Daftar PNPME')->first();
        // dd($register)
    ?>
    <li><a href="{{ route('login') }}">Login</a></li>
    @if(isset($daftar))
        @if($daftar->status == 'hilang')
        @else
            <li><a href="{{ route('register') }}">Daftar PNPME</a></li>
        @endif
    @endif
    <li><a href="{{ URL('tarif') }}">Tarif</a></li>
    <li><a href="{{ URL('download-sptjm') }}">Download Dokumen PNPME</a></li>
    <li><a href="{{ URL('jadwal') }}">Jadwal PNPME</a></li>
    <!-- <li><a href="{{ URL('juklak') }}">Juklak</a></li>     -->
@else
    @if(Auth::user()->role == '1')
        <li><a href="{{ URL('admin/banner') }}">Master</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Informasi <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('download_evaluasi') }}">Download Evaluasi</a></li>
                <li><a href="{{ URL('download-manual-book') }}">Download Manual Book dan Juknis</a></li>
                <li><a href="{{ URL('download-penggunaan') }}">Download Turorial Penggunaan</a></li>
                <li><a href="{{ URL('edit-data') }}">Edit Data Personal</a></li>
                <li><a href="{{ URL('download-sptjm') }}">Download Dokumen PNPME</a></li>
                <li><a href="{{ URL('tarif') }}">Tarif</a></li>
            </ul>
        </li>
        <!-- <li><a href="{{ URL('daftar') }}">Daftar</a></li> -->
        <!-- <li><a href="{{ URL('kirim-bahan') }}">Kirim Bahan</a></li> -->
        <!-- <li><a href="{{ URL('juklak') }}">Juklak</a></li> -->
        <!-- <li><a href="{{ URL('cek_transfer') }}">Cek Transfer</a></li> -->
        <li><a href="{{ URL('email-blast') }}">Email Blast</a></li>
        <!-- <li><a href="{{ URL('sptjm') }}">Download Dokumen PNPME</a></li> -->
        <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Hasil Pemeriksaan <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('hasil-pemeriksaan') }}">Input Hasil</a></li>
                <li><a href="{{ URL('cetak-hasil') }}">Cetak Hasil</a></li>
            </ul>
        </li> -->
        <li class="dropdown">
            @yield('menudashboard')
        </li>
        <li class="dropdown">
            @yield('menulaporan')
        </li>
        <!-- <li><a href="{{ URL('#') }}">Upload Evaluasi</a></li> -->
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
    @if(Auth::user()->role == '2')
        <li><a href="{{ URL('tarif') }}">Tarif</a></li>
        <li><a href="{{ URL('download_evaluasi') }}">Download Evaluasi</a></li>
        <li><a href="{{ URL('download-manual-book') }}">Download Manual Book dan Juknis</a></li>
        <li><a href="{{ URL('download-penggunaan') }}">Download Tutorial Penggunaan</a></li>
        <li><a href="{{ URL('download-sptjm') }}">Download Dokumen PNPME</a></li>
        <!-- <li><a href="{{ URL('juklak') }}">Juklak</a></li> -->
        <li class="dropdown">
            @yield('menulaporan')
        </li>
        <!-- <li class="dropdown">
            @yield('menudashboard')
        </li> -->
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
    @if(Auth::user()->role == '3')
        <?php
            $input = DB::table('tb_hide_menu')->where('menu','=','Input Hasil')->first();
            $evaluasi = DB::table('tb_hide_menu')->where('menu','=','Hasil Evaluasi')->first();
            $daftar = DB::table('tb_hide_menu')->where('menu','=','Daftar PNPME')->first();
            $register = DB::table('tb_registrasi')->where('created_by', '=', Auth::user()->id)->get();
            // dd($register)
        ?>
        <li><a href="{{ URL('tarif') }}">Tarif</a></li>
        <li><a href="{{ URL('jadwal') }}">Jadwal PNPME</a></li>
        @if(isset($daftar))
            @if($daftar->status == 'hilang')
            @else
                <li><a href="{{ URL('daftar') }}">Daftar PNPME</a></li>
            @endif
        @endif

        @if(isset($register))
        <li><a href="{{ URL('edit-data') }}">Edit Data</a></li>
        @if(isset($evaluasi))
            @if($evaluasi->status == 'muncul')
                <li><a href="{{ URL('data-evaluasi') }}">Hasil Evaluasi</a></li>
            @else
            @endif
        @endif
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Status <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('tanda-terima-bahan') }}">Tanda Terima Bahan</a></li> 
                <li><a href="{{ URL('status-pendaftaran') }}">Status Pendaftaran</a></li>
            </ul>
        </li>
        
        @if(isset($register))
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Hasil Pemeriksaan <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                @if(isset($input))
                    @if($input->status == 'hilang')
                    @else
                        <li><a href="{{ URL('hasil-pemeriksaan') }}">Input Hasil</a></li>
                    @endif
                @else
                    <li><a href="{{ URL('hasil-pemeriksaan') }}">Input Hasil</a></li>
                @endif
                <li><a href="{{ URL('edit-hasil') }}">Edit Hasil</a></li>
                <!-- <li><a href="{{ URL('cetak-sementara') }}">Cetak Hasil Sementara</a></li> -->
                <li><a href="{{ URL('cetak-hasil') }}">Cetak Hasil</a></li>
              
            </ul>
        </li>
        @endif
        @endif
<!--         <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Komentar dan Saran <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('pendapat/pendapat-responden') }}">Pendapat Responden</a></li>
                <li><a href="{{ URL('pendapat/keluh-saran') }}">Keluhan / Saran</a></li>
                <li><a href="{{ URL('pendapat/jawab/keluh-saran') }}">Jawaban Keluhan / Saran</a></li>
            </ul>
        </li> -->
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Download <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <!-- <li><a href="{{ URL('download_evaluasi') }}">Download Evaluasi</a></li> -->
                <li><a href="{{ URL('download-manual-book') }}">Download Manual Book dan Juknis</a></li>
                <li><a href="{{ URL('download-penggunaan') }}">Download Tutorial Penggunaan</a></li>
                <li><a href="{{ URL('download-sptjm') }}">Download Dokumen PNPME</a></li>
            </ul>
        </li>
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
    @if(Auth::user()->role == '4')
        <li><a href="{{ URL('download-penggunaan') }}">Tutorial Penggunaan</a></li>
        <li><a href="{{ URL('tarif') }}">Tarif</a></li>
        <li><a href="{{ URL('jadwal') }}">Jadwal PNPME</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Pembayaran <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('cek_transfer') }}">Cek Transfer</a></li>
                <li><a href="{{ URL('cek-kwitansi') }}">List Kuitansi</a></li>
                <li><a href="{{ URL('sptjm') }}">Cek SPTJM</a></li>
                <li><a href="{{ URL('update-pembayaran') }}">Update Pembayaran</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Komentar dan Saran <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('pendapat/keluh-saran/jawab') }}">Jawab Keluhan / Saran</a></li>
            </ul>
        </li>
        <li class="dropdown">
            @yield('menulaporan')
        </li>
        <li class="dropdown">
            @yield('menudashboard')
        </li>
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
    @if(Auth::user()->role == '5')

        <li><a href="{{ URL('admin/bidang') }}">Master</a></li>
        <li><a href="{{ URL('tarif') }}">Tarif</a></li>
        <li><a href="{{ URL('jadwal') }}">Jadwal PNPME</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Download <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('download_evaluasi') }}">Download Evaluasi</a></li>
                <li><a href="{{ URL('download-manual-book') }}">Download Manual Book dan Juknis</a></li>
                <li><a href="{{ URL('download-penggunaan') }}">Download Tutorial Penggunaan</a></li>
                <li><a href="{{ URL('download-sptjm') }}">Download Dokumen PNPME</a></li>
            </ul>
        </li>
        <li><a href="{{ URL('kirim-bahan') }}">Kirim Bahan</a></li>
        <li class="dropdown">
            @yield('menuevaluasi')
        </li>
        <li class="dropdown">
            @yield('menudashboard')
        </li>
        <li class="dropdown">
            @yield('menulaporan')
        </li>
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
    @if(Auth::user()->role == '6')
        <li><a href="{{ URL('email-blast') }}">Email Blast</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Komentar dan Saran <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('pendapat/keluh-saran/jawab') }}">Jawab Keluhan / Saran</a></li>
            </ul>
        </li>
        <li class="dropdown">
            @yield('menudashboard')
        </li>
        <li class="dropdown">
            @yield('menulaporan')
        </li>
        <!-- <li><a href="{{ URL('#') }}">Upload Evaluasi</a></li> -->
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
    @if(Auth::user()->role == '7')
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Informasi <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ URL('download_evaluasi') }}">Download Evaluasi</a></li>
                <li><a href="{{ URL('download-manual-book') }}">Download Manual Book dan Juknis</a></li>
                <li><a href="{{ URL('download-penggunaan') }}">Download Tutorial Penggunaan</a></li>
                <li><a href="{{ URL('download-sptjm') }}">Download Dokumen PNPME</a></li>
                <li><a href="{{ URL('tarif') }}">Tarif</a></li>
            </ul>
        </li>
        <!-- <li><a href="{{ URL('juklak') }}">Juklak</a></li> -->
        <li><a href="{{ URL('cek_transfer') }}">Cek Transfer</a></li>
        <li class="dropdown">
            @yield('menulaporan')
        </li>
        <li class="dropdown pisahin">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
@endif
@endsection
