@extends('layouts.header')
@section('header_t')

<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
    @if(Auth::user()->role == '1')
        <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a href="{{URL('admin/penggunaan')}}">
                    <i class="icon-youtube-play"></i>
                    <span>Tutorial Penggunaan</span>
                </a>
            </li>
            <li>
                <a href="{{URL('admin/banner')}}">
                    <i class="icon-dashboard"></i>
                    <span>Banner</span>
                </a>
            </li>

            <li>
                <a href="{{URL('admin/pks')}}">
                    <i class="icon-tags"></i>
                    <span>Dokumen PNPME</span>
                </a>
            </li>

            <li>
                <a href="{{URL('admin/data-perusahaan')}}">
                    <i class="icon-tag"></i>
                    <span>Data Perusahaan</span>
                </a>
            </li>

            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="icon-laptop"></i>
                    <span>Bidang</span>
                </a>
                <ul class="sub">
                    <li><a href="{{URL('admin/bidang')}}">Bidang</a></li>
                    <li><a href="{{URL('admin/parameter')}}">Parameter</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="icon-book"></i>
                    <span>Rujukan</span>
                </a>
                <ul class="sub">
                    <li><a href="{{URL('admin/rujukan-bta')}}">BTA</a></li>
                    <li><a href="{{URL('admin/rujukan-tc')}}">TC</a></li>
                    <li><a href="{{URL('admin/rujukan-malaria')}}">Malaria</a></li>
                    <li><a href="{{URL('admin/rujukan-hematologi')}}">Hematologi</a></li>
                    <li><a href="{{URL('admin/rujukan-kimiaklinik')}}">Kimia Klinik</a></li>
                    <li><a href="{{URL('admin/rujukan-urinalisa')}}">Urinalisa</a></li>
                    <li><a href="{{URL('admin/rujukan-kimiakesehatan')}}">Kimia Kesehatan</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="icon-laptop"></i>
                    <span>Wilayah</span>
                </a>
                <ul class="sub">
                    <li><a href="{{URL('admin/provinsi')}}">Provinsi</a></li>
                    <li><a href="{{URL('admin/kabupaten-kota')}}">Kabupaten / Kota</a></li>
                    <li><a href="{{URL('admin/kecamatan')}}">Kecamatan</a></li>
                    <li><a href="{{URL('admin/kelurahan')}}">Kelurahan</a></li>
                </ul>
            </li>
            <li>
                <a href="{{URL('admin/juklak')}}">
                    <i class="icon-list-ol"></i>
                    <span>Juklak</span>
                </a>
            </li>
            <li>
                <a href="{{URL('admin/sptjm')}}">
                    <i class="icon-file-text-alt"></i>
                    <span>Dokumen PNPME</span>
                </a>
            </li>
            <li>
                <a href="{{URL('admin/manual-book')}}">
                    <i class="icon-book"></i>
                    <span>Manual Book</span>
                </a>
            </li>
            <!-- <li>
                <a href="{{URL('admin/upload-evaluasi')}}">
                    <i class=" icon-columns"></i>
                    <span>Upload Evaluasi</span>
                </a>
            </li> -->
            <li>
                <a href="{{URL('admin/jadwal')}}">
                    <i class="icon-list-ol"></i>
                    <span>Jadwal</span>
                </a>
            </li>
            <li>
                <a href="{{URL('admin/hak-akses')}}">
                    <i class="icon-user"></i>
                    <span>Hak Akses</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="icon-gear"></i>
                    <span>Setting</span>
                </a>
                <ul class="sub">
                    <!-- <li><a href="{{URL('admin/edit-hasil')}}">Kirim Edit Hasil</a></li> -->
                    <li><a href="{{URL('admin/menu')}}">Menu Web PNPME</a></li>
                    <li><a href="{{URL('admin/siklus')}}">Siklus PNPME</a></li>
                    <li><a href="{{URL('admin/tahun-evaluasi')}}">Tahun Evaluasi</a></li>
                </ul>
            </li>
        </ul>
    @endif
    @if(Auth::user()->role == '5')
        <ul class="sidebar-menu" id="nav-accordion">
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="icon-laptop"></i>
                    <span>Bidang</span>
                </a>
                <ul class="sub">
                    <li><a href="{{URL('admin/bidang')}}">Bidang</a></li>
                    <li><a href="{{URL('admin/parameter')}}">Parameter</a></li>
                </ul>
            </li>

            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="icon-laptop"></i>
                    <span>Wilayah</span>
                </a>
                <ul class="sub">
                    <li><a href="{{URL('admin/provinsi')}}">Provinsi</a></li>
                    <li><a href="{{URL('admin/kabupaten-kota')}}">Kabupaten / Kota</a></li>
                    <li><a href="{{URL('admin/kecamatan')}}">Kecamatan</a></li>
                    <li><a href="{{URL('admin/kelurahan')}}">Kelurahan</a></li>
                </ul>
            </li>
            <li>
                <a href="{{URL('admin/juklak')}}">
                    <i class="icon-dashboard"></i>
                    <span>Juklak</span>
                </a>
            </li>
            <li>
                <a href="{{URL('admin/manual-book')}}">
                    <i class="icon-dashboard"></i>
                    <span>Manual Book</span>
                </a>
            </li>
            <!-- <li>
                <a href="{{URL('admin/upload-evaluasi')}}">
                    <i class="icon-dashboard"></i>
                    <span>Upload Evaluasi</span>
                </a>
            </li> -->
        </ul>
    @endif
        <!-- sidebar menu end-->
    </div>
</aside>
@yield('content')
@endsection