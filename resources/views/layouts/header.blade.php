<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Admin BBLK</title>
    <script src="//cdn.ckeditor.com/4.9.0/basic/ckeditor.js"></script>
    <link href="{{URL::asset('backend/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{URL::asset('backend/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('backend/css/bootstrap-reset.css')}}" rel="stylesheet">
    <link href="{{URL::asset('backend/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{URL::asset('backend/assets/data-tables/DT_bootstrap.css')}}" />
    <link href="{{URL::asset('backend/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('backend/css/style-responsive.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{URL::asset('asset/css/bootstrap-select.min.css')}}">
  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
      <header class="header white-bg">
          <div class="sidebar-toggle-box">
              <div data-original-title="Navigation" data-placement="right" class="icon-reorder tooltips"></div>
          </div>
          <!--logo start-->
          <a href="{{URL('index')}}" class="logo" >BB<span>LK</span> JAKARTA</a>
          <!--logo end-->
          <div class="top-nav ">
              <ul class="nav pull-right top-menu">
                  <!-- user login dropdown start-->
                  <li class="dropdown">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                          <span class="username">Admin</span>
                          <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu extended logout">
                          <div class="log-arrow-up"></div>
                          <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();"><i class="icon-key"></i>
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                          </li>
                      </ul>
                  </li>
                  <!-- user login dropdown end -->
              </ul>
          </div>
      </header>
      <!--header end-->
      @yield('header_t')

    </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{URL::asset('backend/js/jquery-1.8.3.min.js')}}"></script>
    <script src="{{URL::asset('backend/js/bootstrap.min.js')}}"></script>
    <script class="include" type="text/javascript" src="{{URL::asset('backend/js/jquery.dcjqaccordion.2.7.js')}}"></script>
    <script src="{{URL::asset('backend/js/jquery.scrollTo.min.js')}}"></script>
    <script src="{{URL::asset('backend/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{URL::asset('backend/assets/data-tables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('backend/assets/data-tables/DT_bootstrap.js')}}"></script>
    <script src="{{URL::asset('backend/js/respond.min.js')}}" ></script>
    <script src="{{URL::asset('backend/js/bootstrap-datetimepicker.js')}}" ></script>
    <script src="{{URL::asset('backend/js/bootstrap-datepicker.js')}}" ></script>
    <script src="{{URL::asset('backend/js/bootstrap-switch.js')}}" ></script>
    <script src="{{URL::asset('backend/js/jquery.tagsinput.js')}}"></script>
    <script src="{{URL::asset('backend/js/form-component.js')}}"></script>
    
    <script src="{{URL::asset('asset/js/bootstrap-select.min.js')}}"></script>
    <!--common script for all pages-->
    <script src="{{URL::asset('backend/js/common-scripts.js')}}"></script>

      <!--script for this page only-->
      <script src="{{URL::asset('backend/js/editable-table.js')}}"></script>

      <!-- END JAVASCRIPTS -->
      <script type="text/javascript">
          var table = $("#table-datatable");
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
      </script>
      @yield('scriptBlock')
      <script>                            
      
      jQuery(document).ready(function() {
          EditableTable.init();
      });
      </script>


  </body>
</html>
