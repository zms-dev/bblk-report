<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>BBLK</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="//cdn.ckeditor.com/4.9.0/basic/ckeditor.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="{{URL::asset('asset/css/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('asset/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('asset/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{URL::asset('asset/css/bootstrap.css')}}">
    <link href="{{URL::asset('backend/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{URL::asset('asset/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{URL::asset('asset/css/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('asset/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/side.css') }}">
   
    <script type="text/javascript" src="{{URL::asset('asset/js/jquery-1.12.4.js')}}"></script>
    <script src="{{URL::asset('asset/js/jquery.min.js')}}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <!-- <script src="{{URL::asset('asset/js/jquery.validate.min.js')}}"></script>
    <script src="{{URL::asset('asset/js/additional-methods.min.js')}}"></script> -->
    <script src="{{URL::asset('asset/js/highcharts.js')}}"></script>
    <script src="{{URL::asset('asset/js/highcharts-3d.js')}}"></script>
    <script src="{{URL::asset('backend/js/bootstrap-datetimepicker.js')}}" ></script>
    <script src="{{URL::asset('asset/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{URL::asset('js/sweetalert2.all.js')}}"></script>
    <script src="{{URL::asset('asset/js/exporting.js')}}"></script>

</head>
<style type="text/css">
    .datetimepicker{
        /*position: fixed !important;*/
    }
</style>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="border-bottom: 4px solid #34a449">
            <div class="container-fluid">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a href="{{ url('/') }}" style="margin: 5px 0px 5px 0px">
                        <img src="{{URL::asset('asset/img/logo.png')}}" style="max-height: 80px; margin: 10px; max-width: 100%">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right bawah">
                        <!-- Authentication Links -->
                        @yield('menu')
                    </ul>
                </div>
            </div>
        </nav>
        @yield('menuhasilevaluasi')
        @yield('content')
        
    </div>

    <!-- Scripts -->
    <script src="{{URL::asset('asset/js/bootstrap.js')}}"></script>
    <script src="{{URL::asset('asset/js/canvasjs.min.js')}}"></script>
    <script src="https://vuejs.org/js/vue.min.js" ></script>
    <script src="{{URL::asset('asset/js/jquery.validate.js')}}"></script>
    <script src="{{URL::asset('asset/js/select2.min.js')}}"></script>
</body>
</html>

    <script type="text/javascript" src="{{URL::asset('asset/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('asset/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('backend/assets/data-tables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('backend/assets/data-tables/DT_bootstrap.js')}}"></script>
    <script src="{{URL::asset('asset/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('.navbar a.dropdown-toggle').on('click', function(e) {
        var $el = $(this);
        var $parent = $(this).offsetParent(".dropdown-menu");
        $(this).parent("li").toggleClass('open');

        $('.nav li.open').not($(this).parents("li")).removeClass("open");

        return false;
    });
});
$(".datepick").datepicker({
      "format" : "yyyy-mm-dd",
      autoclose : true,
      todayBtn : true,
      todayHighlight : true
    })


var table = $("#table-datatable");
$.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a7d41224b401e45400ccde2/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<script src="{{url::asset('backend/js/datatables.min.js')}}"></script>
@yield('scriptBlock')
<script>
jQuery(document).ready(function() {
  EditableTable.init();
});

</script>