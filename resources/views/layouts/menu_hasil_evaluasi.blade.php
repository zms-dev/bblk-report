@section('menuhasilevaluasi')
<div class="wrapper" style="margin-top: -40px;">
    <nav id="sidebar-peserta" style="text-align: center;">
        <div class="sidebar-header">
            <a href="{{ url('data-evaluasi') }}"><h3>Laporan Akhir</h3></a>
            <form method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <select name="tanggal" class="form-control" required id="tahun">
                    <option readonly>Pilih Tahun</option>
                    @for ($i=2018; $i <= $tahunevaluasi->tahun; $i++)
                        <option {{ $tanggal=="$i"?'selected':'' }} value="{{$i}}">{{$i}}</option>
                    @endfor
                    </select>
                    <select name="siklus" class="form-control" required id="siklus" style=" margin-top: 10px;">
                        <option readonly>Pilih Siklus</option>
                        <option></option>
                        <option {{ $siklus=="1"?'selected':'' }} value="1">1</option>
                        <option {{ $siklus=="2"?'selected':'' }} value="2">2</option>
                    </select>
              <input type="submit" name="cari" required="" class="btn btn-info" value="cari" style="margin-top: 8px;">
            </form>
        </div>
        <script>
        $(document).ready(function(){
            $("#tahun").change(function(){
                console.log($(this).val());
                if ({{$tahunevaluasi->tahun}} == $(this).val()){
                    console.log('ada');
                    if ({{$tahunevaluasi->siklus}} != 2) {
                    console.log('ada siklus');
                        $('#siklus option[value="2"]').remove();
                    }
                }else{
                    $('#siklus option[value="1"]').remove();
                    $('#siklus option[value="2"]').remove();
                    $('#siklus').append($('<option>', { 
                        value: 1,
                        text : '1' 
                    })).append($('<option>', { 
                        value: 2,
                        text : '2' 
                    }));
                }
            });
            if ({{$tahunevaluasi->tahun}} == $("#tahun").val()){
                console.log('ada');
                if ({{$tahunevaluasi->siklus}} != 2) {
                console.log('ada siklus');
                    $('#siklus option[value="2"]').remove();
                }
            }
        });
        </script>
        <ul class="list-unstyled components" style="text-align: center">
            <li>
                @foreach($datas as $val)
                    <a href=" {{URL('').$val->Link}}/data-evaluasi/{{$val->id}}">
                      @if($val->alias == "HEM")
                      Hematologi   
                      @elseif($val->alias == "KKL")
                      Kimia Klinik
                      @elseif($val->alias == "URI")
                      Urinalisis
                      @elseif($val->alias == "BTA")
                      Mikroskopis BTA
                      @elseif($val->alias == "TCC")
                      Mikroskopis Telur Cacing
                      @elseif($val->alias == "HIV")
                      Anti HIV
                      @elseif($val->alias == "SIF")
                      Syphilis Anti TP
                      @elseif($val->alias == "HBS")
                      HBsAg
                      @elseif($val->alias == "HCV")
                      Anti HCV
                      @endif
                    </a>
                @endforeach
            </li>
        </ul>
    </nav>
@endsection