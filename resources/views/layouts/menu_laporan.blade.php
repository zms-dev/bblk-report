@section('menulaporan')
<a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan<b class="caret"></b></a>
<ul class="dropdown-menu">
@if (Auth::guest())
@else
    @if(Auth::user()->role == '4')
    @elseif(Auth::user()->role == '6')
    @else
    <!-- <li>
        <a href="{{url('dashboard/pnpme')}}">SIMULTAN</a>
    </li> -->
    @endif
    @if(Auth::user()->role == '5')
    <li>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tanda Terima<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li style="padding-left: 13px"><a href="{{url('/laporan/tanda-terima')}}">Tanda Terima Peserta</a></li>
            <li style="padding-left: 13px"><a href="{{url('/laporan/rekap-tanda-terima')}}">Rekap Tanda Terima</a></li>
        </ul>
    </li>
    <li>
        <a href="{{url('/monitoring-laporan-hasil')}}">Monitoring Laporan Hasil</a>
    </li>
    <li>
        <a href="{{url('laporan-cetak-hasil')}}">Cetak Hasil Peserta</a>
    </li>
    <li>
        <a href="{{url('laporan-edit-hasil')}}">Edit Hasil Peserta</a>
    </li>
    <li>
        <a href="{{url('laporan-cek-hasil')}}">Cek Hasil Peserta</a>
    </li>
    @else
    <!-- <li>
        <a href="{{url('monitoring-simultan')}}">Monitoring Simultan</a>
    </li> -->
    @endif
@endif
    <li>
        <a href="{{url('laporan-registrasi')}}">Laporan Seluruh Peserta</a>
    </li>
    <li>
        <a href="{{url('laporan-uang-masuk')}}">Laporan Penerimaan uang masuk</a>
    </li>
    <li>
        <a href="{{url('laporan-peserta')}}">Laporan Peserta PNPME</a>
    </li>
    <li>
        <a href="{{url('laporan-peserta-parameter')}}">Laporan Pengiriman Hasil PNPME</a>
    </li>
    <!-- <li>
        <a href="{{url('laporan-penerimaan-sampel')}}">Laporan Penerimaan Sampel</a>
    </li> -->
    <!-- <li>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Laporan Hasil<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li style="padding-left: 13px"><a href="#">Laporan Hasil</a></li>
            <li style="padding-left: 13px"><a href="#">Laporan Hasil Siklus 1</a></li>
            <li style="padding-left: 13px"><a href="#">Laporan Hasil Siklus 2</a></li>
        </ul>
    </li> -->
    <li>

@if (Auth::guest())
@else
    @if(Auth::user()->role == '4')
    @elseif(Auth::user()->role == '6')
    @else
        <!-- <a href="{{url('laporan-evaluasi')}}">Laporan Evaluasi</a> -->
    @endif
@endif
    </li>
</ul>
@endsection