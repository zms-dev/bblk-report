@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><h4><b>KELUHAN / SARAN PESERTA PNPME <br>
                        SIKLUS 
                        <select name="siklus" required>
                            <option></option>
                            <option value="1" class="1">1</option>
                            <option value="2" class="2">2</option>
                            @if(count($pendapat))
                            <script type="text/javascript">
                            @foreach($pendapat as $val)
                              $('option.{{$val->siklus}}').remove();
                            @endforeach
                            </script>
                            @else
                            @endif
                        </select> 
                        TAHUN {{$tahun}}</b></h4></center><br>
                    <table class="table">
                        <tr>    
                            <td width="40%">Nama Instansi</td>
                            <td>:
                                {{$perusahaan->nama_lab}}
                            </td>
                        </tr>
                        <tr>    
                            <td>Alamat Instansi</td>
                            <td>:
                                {{$perusahaan->alamat}}
                            </td>
                        </tr>
                        <tr>    
                            <td>Nama Penanggung Jawab Laboratorium</td>
                            <td>:
                                {{$perusahaan->penanggung_jawab}}
                            </td>
                        </tr>
                        <tr>    
                            <td>No. HP / Telp. Penanggung Jawab Laboratorium</td>
                            <td>:
                                {{$perusahaan->no_hp}}
                            </td>
                        </tr>
                        <tr>    
                            <td>Email</td>
                            <td>:
                                {{$perusahaan->email}}
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <th>Parameter</th>
                            <th><center>Keluhan / Saran</center></th>
                        </tr>
                        <tr>
                            <td>
                                <textarea name="parameter" id="editor1" rows="10" cols="80"></textarea>
                            </td>
                            <td>
                                <textarea name="saran" id="editor2" rows="10" cols="80"></textarea>
                            </td>
                        </tr>
                    </table>
                      {{ csrf_field() }}
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" >
    CKEDITOR.replace( 'editor1' );
    CKEDITOR.replace( 'editor2' );
</script>
@endsection
