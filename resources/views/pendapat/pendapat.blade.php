@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><h4><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL (PNPME) <br>
                        SIKLUS 
                        <select name="siklus" required>
                            <option></option>
                            <option value="1" class="1">1</option>
                            <option value="2" class="2">2</option>
                            @if(count($pendapat))
                            <script type="text/javascript">
                            @foreach($pendapat as $val)
                              $('option.{{$val->siklus}}').remove();
                            @endforeach
                            </script>
                            @else
                            @endif
                        </select> 
                        TAHUN {{$tahun}}</b></h4></center><br>
                    <table class="table-bordered table">
                        <tr>
                            <th style="text-align: center;" colspan="2">DATA PESERTA (RESPONDEN)</th>
                        </tr>
                        <tr>    
                            <td>Nama Instansi :</td>
                            <td>
                                <input size="16" type="text" value="{{$perusahaan->nama_lab}}" readonly class="form-control" name="nama_lab">
                            </td>
                        </tr>
                    </table>
                    <center><h5><b>PENDAPAT RESPONDEN TENTANG PELAYANAN PUBLIK</b><br><i>(Pilih salah satu huruf sesuai jawaban responden)</i></h5></center><br>
                    <table class="table-bordered table">
                        <tr>
                            <th>No</th>
                            <th>Parameter</th>
                            <th>No</th>
                            <th>Parameter</th>
                        </tr>
                        <tr>
                            <td>1.</td>
                            <td>
                                Bagaimana pemahaman Saudara tentang kemudahan prosedur pelayanan PNPME<br>
                                <input type="radio" name="no_1" value="a"> A. Tidak mudah
                                <input type="radio" name="no_1" value="b"> B. Kurang mudah<br>
                                <input type="radio" name="no_1" value="c"> C. Mudah
                                <input type="radio" name="no_1" value="d"> D. Sangat mudah
                            </td>
                            <td>2.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang kesesuaian persyaratan pelayanan dengan penerapannya saat mengikuti PNPME<br>
                                <input type="radio" name="no_2" value="a"> A. Tidak sesuai
                                <input type="radio" name="no_2" value="b"> B. Kurang sesuai<br>
                                <input type="radio" name="no_2" value="c"> C. Sesuai
                                <input type="radio" name="no_2" value="d"> D. Sangat sesuai
                            </td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang kejelasan informasi kegiatan PNPME di BBLK Jakarta secara umum<br>
                                <input type="radio" name="no_3" value="a"> A. Tidak jelas
                                <input type="radio" name="no_3" value="b"> B. Kurang jelas<br>
                                <input type="radio" name="no_3" value="c"> C. Jelas
                                <input type="radio" name="no_3" value="d"> D. Sangat jelas
                            </td>
                            <td>4.</td>
                            <td>
                                Bagaimana pendapat Saudara dalam kemudahan melengkapi administrasi kegiatan PNPME<br>
                                <input type="radio" name="no_4" value="a"> A. Tidak mudah
                                <input type="radio" name="no_4" value="b"> B. Kurang mudah<br>
                                <input type="radio" name="no_4" value="c"> C. Mudah
                                <input type="radio" name="no_4" value="d"> D. Sangat mudah
                            </td>
                        </tr>
                        <tr>
                            <td>5.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang penjelasan petugas dalam mengikuti kegiatan PNPME<br>
                                <input type="radio" name="no_5" value="a"> A. Tidak jelas
                                <input type="radio" name="no_5" value="b"> B. Kurang jelas<br>
                                <input type="radio" name="no_5" value="c"> C. Jelas
                                <input type="radio" name="no_5" value="d"> D. Sangat jelas
                            </td>
                            <td>6.</td>
                            <td>
                                Bagaimana pendapat Saudara tantang kecepatan petugas dalam menjawab atau merespon permasalahan kegiatan PNPME<br>
                                <input type="radio" name="no_6" value="a"> A. Tidak cepat
                                <input type="radio" name="no_6" value="b"> B. Kurang cepat<br>
                                <input type="radio" name="no_6" value="c"> C. Cepat
                                <input type="radio" name="no_6" value="d"> D. Sangat cepat
                            </td>
                        </tr>
                        <tr>
                            <td>7.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang kegiatan PNPME kami secara umum<br>
                                <input type="radio" name="no_7" value="a"> A. Tidak baik
                                <input type="radio" name="no_7" value="b"> B. Kurang baik<br>
                                <input type="radio" name="no_7" value="c"> C. Baik
                                <input type="radio" name="no_7" value="d"> D. Sangat baik
                            </td>
                            <td>8.</td>
                            <td>
                                Bagaimana pendapat Saudara tantang kesopanan dan kemarahan petugas dalam memberikan pelayanan<br>
                                <input type="radio" name="no_8" value="a"> A. Tidak sopan dan ramah
                                <input type="radio" name="no_8" value="b"> B. Kurang sopan dan ramah<br>
                                <input type="radio" name="no_8" value="c"> C. Sopan dan ramah
                                <input type="radio" name="no_8" value="d"> D. Sangat sopan dan ramah
                            </td>
                        </tr>
                        <tr>
                            <td>9.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang kewajaran biaya untuk mendapatkan pelayanan<br>
                                <input type="radio" name="no_9" value="a"> A. Tidak wajar
                                <input type="radio" name="no_9" value="b"> B. Kurang wajar<br>
                                <input type="radio" name="no_9" value="c"> C. Wajar
                                <input type="radio" name="no_9" value="d"> D. Sangat wajar
                            </td>
                            <td>10.</td>
                            <td>
                                Bagaimana pendapat Saudara tantang kesesuaian antara biaya yang dibayarkan dengan biaya yang telah di tetapkan<br>
                                <input type="radio" name="no_10" value="a"> A. Tidak sesuai
                                <input type="radio" name="no_10" value="b"> B. Kurang sesuai<br>
                                <input type="radio" name="no_10" value="c"> C. Sesuai
                                <input type="radio" name="no_10" value="d"> D. Sangat sesuai
                            </td>
                        </tr>
                        <tr>
                            <td>11.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang metode pembayaran yang kami terapkan<br>
                                <input type="radio" name="no_11" value="a"> A. Tidak rumit
                                <input type="radio" name="no_11" value="b"> B. Kurang rumit<br>
                                <input type="radio" name="no_11" value="c"> C. Rumit
                                <input type="radio" name="no_11" value="d"> D. Sangat rumit
                            </td>
                            <td>12.</td>
                            <td>
                                Bagaimana pendapat Saudara tantang rencana program meningkatkan jumlah peserta maupun jumlah parameter<br>
                                <input type="radio" name="no_12" value="a"> A. Tidak setuju
                                <input type="radio" name="no_12" value="b"> B. Kurang setuju<br>
                                <input type="radio" name="no_12" value="c"> C. Setuju
                                <input type="radio" name="no_12" value="d"> D. Sangat setuju
                            </td>
                        </tr>
                        <tr>
                            <td>13.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang merekomendasikan PNPME BBLK Jakarta kepada pihak lain<br>
                                <input type="radio" name="no_13" value="a"> A. Tidak setuju
                                <input type="radio" name="no_13" value="b"> B. Kurang setuju<br>
                                <input type="radio" name="no_13" value="c"> C. Setuju
                                <input type="radio" name="no_13" value="d"> D. Sangat setuju
                            </td>
                            <td>14.</td>
                            <td>
                                Bagaimana pendapat Saudara tantang ketepatan pelaksanaan keseluruhan PNPME terhadap jadual yang telah kami infokan<br>
                                <input type="radio" name="no_14" value="a"> A. Tidak tepat
                                <input type="radio" name="no_14" value="b"> B. Kurang tepat<br>
                                <input type="radio" name="no_14" value="c"> C. Tepat
                                <input type="radio" name="no_14" value="d"> D. Sangat tepat
                            </td>
                        </tr>
                        <tr>
                            <td>15.</td>
                            <td>
                                Bagaimana pendapat Saudara tentang ketepatan jadual pengiriman bahan uji PNPME<br>
                                <input type="radio" name="no_15" value="a"> A. Sangat terlambat
                                <input type="radio" name="no_15" value="b"> B. Terlambat<br>
                                <input type="radio" name="no_15" value="c"> C. Tepat waktu
                                <input type="radio" name="no_15" value="d"> D. Sangat Cepat
                            </td>
                            <td>16.</td>
                            <td>
                                Bagaimana pendapat Saudara tantang kemasan bahan uji PNPME BBLK Jakarta<br>
                                <input type="radio" name="no_16" value="a"> A. Tidak baik
                                <input type="radio" name="no_16" value="b"> B. Kurang baik<br>
                                <input type="radio" name="no_16" value="c"> C. Baik
                                <input type="radio" name="no_16" value="d"> D. Sangat baik
                            </td>
                        </tr>
                        <tr>
                            <td>17.</td>
                            <td>
                                Bagaimana bahan uji PNPME saat datang di tempat Saudara (Sebelum disiapkan sesuai suhu penyimpanan)<br>
                                <input type="radio" name="no_17" value="a"> A. Tidak baik, rusak berat
                                <input type="radio" name="no_17" value="b"> B. Kurang baik<br>
                                <input type="radio" name="no_17" value="c"> C. baik
                                <input type="radio" name="no_17" value="d"> D. Sangat baik
                            </td>
                            <td>18.</td>
                            <td>
                                Bagaimana pendapat Saudara tantang kualitas bahan uji PNPME pada saat sebelum dilakukan pengujian atau pemeriksaan<br>
                                <input type="radio" name="no_18" value="a"> A. Tidak baik
                                <input type="radio" name="no_18" value="b"> B. Kurang baik<br>
                                <input type="radio" name="no_18" value="c"> C. Baik
                                <input type="radio" name="no_18" value="d"> D. Sangat baik
                            </td>
                        </tr>
                        <tr>
                            <td>19.</td>
                            <td>
                                Bagaimana pendapat Saudara, apakah penjelasan Laporan Akhir atau Evaluasi PNPME sudah lengkap dan jelas<br>
                                <input type="radio" name="no_19" value="a"> A. Tidak lengkap dan jelas
                                <input type="radio" name="no_19" value="b"> B. Kurang lengkap dan jelas<br>
                                <input type="radio" name="no_19" value="c"> C. Lengkap dan jelas
                                <input type="radio" name="no_19" value="d"> D. Sangat lengkap dan jelas
                            </td>
                            <td>20.</td>
                            <td>
                                Bagaimana pendapat Saudara, apakah sesuai rekomendasi yang kami berikan terhadap hasil pemeriksaan peserta <br>
                                <input type="radio" name="no_20" value="a"> A. Tidak sesuai
                                <input type="radio" name="no_20" value="b"> B. Kurang sesuai<br>
                                <input type="radio" name="no_20" value="c"> C. Sesuai
                                <input type="radio" name="no_20" value="d"> D. Sangat sesuai
                            </td>
                        </tr>
                    </table>
                      {{ csrf_field() }}
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
