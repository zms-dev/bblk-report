@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" onsubmit="return confirm('Pastikan data hasil pemeriksaan anda sudah benar/hasil yang sudah terkirim sudah tidak dapat dirubah kembali');">
                    <center><label>Program Pemantapan Nasional Pemantapan Mutu Eksternal Mikroskopis BTA Siklus {{$siklus}} Tahun {{$date}}</label></center><br>
                    
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$data->kode_peserta}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-date="2017-07-07" data-link-field="dtp_input1">
                              <input size="16" type="text" value="{{$data->tgl_penerimaan}}" readonly class="form_datetime form-control" name="tgl_penerimaan">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kondisi Bahan Uji </label>
                        <div class="col-sm-9">  
                          <input type="radio" name="kondisi" required value="baik" {{ ($data->kondisi == 'baik') ? 'checked' : '' }}> Baik
                          <input type="radio" name="kondisi" value="pecah/tumpah" {{ ($data->kondisi == 'pecah/tumpah') ? 'checked' : '' }}> Pecah / Tumpah
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Pemeriksaan </label>
                        <div class="col-sm-9">
                            <div class="controls input-append date" data-date="2017-07-07" data-link-field="dtp_input1">
                                  <input size="16" type="text" value="{{$data->tgl_pemeriksaan}}" readonly class="form_datetime form-control" name="tgl_pemeriksaan">
                                  <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Pemeriksa </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_pemeriksa" placeholder="Nama Pemeriksa" required value="{{$data->nama_pemeriksa}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nomor Hp </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nomor_hp" placeholder="Nomor HP" required value="{{$data->nomor_hp}}">
                        </div>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Bahan Uji</th>
                                <th>Hasil Pemeriksaan</th>
                            </tr>
                        </thead>
                        <tbody id="wrapper">
                            <?php $i = 0;?>
                            @foreach($datas as $val)
                            <?php $i++; ?>
                            <tr>
                                <td>{{$i}}<input type="hidden" name="idbta[]" value="{{$val->id}}"></td>
                                <td><input type="text" class="form-control" size="16" name="kode[]" value="{{$val->kode}}" required readonly></td>
                                <td>
                                    <select class="form-control" name="hasil[]" id="hasil{{$i}}">
                                        <option value="{{$val->hasil}}">{{$val->hasil}}</option>
                                        <option value="Negatif">Negatif</option>
                                        <option value="Scanty">Scanty</option>
                                        <option value="1+">1+</option>
                                        <option value="2+">2+</option>
                                        <option value="3+">3+</option>
                                    </select>
                                    <div id="kuman{{$i}}">
                                        <div class="input-group">
                                            <input type="text" name="kuman[]" class="form-control" id="inputkuman{{$i}}" value="{{$val->kuman}}">
                                            <div class="input-group-addon">BTA/100 Lapang Pandang</div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
<script type="text/javascript">
$(function() {
    @if($val->kuman != NULL)
    @else
    $('#kuman{{$i}}').hide();
    @endif 
    $('#hasil{{$i}}').change(function(){
    var setan  = $("#hasil{{$i}} option:selected").text();
        if(setan.match('Scanty.*')) {
            $('#kuman{{$i}}').show(); 
            $("#inputkuman{{$i}}").prop('required',true);
        } else {
            $('#kuman{{$i}}').hide(); 
            $("#inputkuman{{$i}}").prop('required',false);
            $("#inputkuman{{$i}}").val('');
        } 
    });
});
</script>
                            @endforeach
                        </tbody>
                    </table>
                    <!-- <div><input type="button" id="more_fields" onclick="add_fields();" value="Tambah Baris" /></div> -->
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Catatan :</label>
                            <textarea class="form-control" name="catatan">{{$data->catatan}}</textarea>
                        </div>
                        <div class="col-sm-6">
                            <label>Nama Penanggung jawab lab :</label>
                            <input type="text" name="penanggung_jawab" class="form-control" value="{{$data->penanggung_jawab}}" required>
                        </div><br>
                    </div>
                    <small>Info : Mohon untuk mengecek kembali data yang sudah di input, karena data yang sudah di Kirim tidak bisa di edit kembali. </small><br>

                    {{ csrf_field() }}
                    <div class="clearfix"></div>
                    @if(Auth::user()->role == 3)
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px"> &nbsp;
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit" style="margin-top: 20px">
                    @else
                    <input type="submit" name="simpan" value="Validasi" class="btn btn-submit" style="margin-top: 20px">
                    @endif
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;
</script>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minView: 2
    });

function add_fields() {
    document.getElementById('wrapper').innerHTML += '<tr><td><input type="text" class="form-control" size="16" name="kode[]"></td><td><input type="text" class="form-control" size="16" name="hasil[]"></td></tr>';
}
</script>
@endsection
