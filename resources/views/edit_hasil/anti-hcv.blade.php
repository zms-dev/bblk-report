@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" onsubmit="return confirm('Pastikan data hasil pemeriksaan anda sudah benar/hasil yang sudah terkirim sudah tidak dapat dirubah kembali');">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PROGRAM  NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}<br> HASIL PEMERIKSAAN ANTI-HCV</label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta </label>
                        <div class="col-sm-10">
                            @foreach($data as $val)
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$val->kode_lab}}" placeholder="Kode Peserta" readonly>
                            <input type="hidden" name="idmaster" value="{{$val->id}}">
                            @break
                            @endforeach
                        </div>
                    </div>
                    <label>2. BAHAN UJI :</label>
                    <table class="table-bordered table">
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>
                                @foreach($data as $val)
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="{{$val->tgl_diterima}}" readonly class="form_datetime form-control" name="tgl_diterima" autocomplete="off">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                @break
                                @endforeach
                                </td>
                                <td>Diperiksa tanggal :</td>
                                <td>
                                @foreach($data as $val)
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="{{$val->tgl_diperiksa}}" readonly class="form_datetime form-control" name="tgl_diperiksa" autocomplete="off">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                @break
                                @endforeach
                                </td>
                            </tr>
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="6" width="20%"> Kondisi bahan uji saat diterima</td>
                                <td>Kode Bahan Uji <small>*(Wajib angka dan 3 digit)</small></td>
                                <td>Baik/Jernih </td>
                                <td>Keruh </td>
                                <td>Lain-lain</td>
                            </tr>
                            <?php  
                                $no = 0;
                                $idtabunga = ['1','2','3','4','5'];
                            ?>
                            @foreach($data as $val)
                            <tr>
                                <input type="hidden" value="{{$val->idimunologi}}" name="idbahan[]">
                                <td>
                                    <div class="input-group">
                                        <div class="input-group-addon">IV-{{$idtabunga[$no]}}-</div>
                                        <input type="text" class="form-control badan{{$no}}" id="tabung{{$idtabunga[$no]}}1" name="no_tabung[]" max="999" required placeholder="999" value="{{$val->no_tabung}}"  pattern="[0-9]{3}">
                                    </div>
                                </td>
                                <td><input class="bahan{{$no}}" type="checkbox" name="jenis[]" value="baik/jernih" {{ ($val->jenis == 'baik/jernih') ? 'checked' : '' }}></td>
                                <td><input class="bahan{{$no}}" type="checkbox" name="jenis[]" value="keruh" {{ ($val->jenis == 'keruh') ? 'checked' : '' }}></td>
                                <td><input class="bahan{{$no}}" type="checkbox" name="jenis[]" class="lain2{{$val->idimunologi}}" value="lain-lain" {{ ($val->jenis == 'lain-lain') ? 'checked' : '' }}>
                                </td>
                            </tr>
                            <?php $no++;?>
                            @endforeach
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Metode Pemeriksaan </td>
                            @foreach($data1 as $val)
                            <td><input class="metode_pemeriksaan" type="checkbox" name="metode[]" value="rapid" {{ ($val->metode == 'rapid') ? 'checked' : '' }}> Rapid <input class="metode_pemeriksaan" type="checkbox" name="metode[]" value="eia" {{ ($val->metode == 'eia') ? 'checked' : '' }}> EIA / Setara</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nama Reagen</td>
                            @foreach($data1 as $val)
                            <input type="hidden" value="{{$val->id}}" name="idreagen[]">
                            <td>
                                <select id="alat1" class="form-control" name="nama_reagen[]" class="form-control" >
                                    <option value="{{$val->Idreagen}}">{{$val->namareagen}}</option>
                                    @foreach($reagen as $valu)
                                      <option value="{{$valu->id}}">{{$valu->reagen}}</option>
                                    @endforeach
                                </select>
                                <div id="row_alat1" class="inpualat1">
                                    <input id="inpualat1" class="form-control" type="text" name="reagen_lain[]" value="{{$val->reagen_lain}}" />
                                </div>
                            </td>
<script>                            
$(function() {
    var setan  = $("#alat1 option:selected").text();
    if(setan.match('Lain - lain.*')) {
        $('#inpualat1').prop('required', true).show(); 
    } else {
        $('#inpualat1').prop('required', false).hide(); 
    }

    $('#alat1').change(function(){
    var setan  = $("#alat1 option:selected").text();

    if(setan.match('Lain - lain.*')) {
    $('#inpualat1').prop('required', true).show(); 
    } else {
        $('#inpualat1').prop('required', false).hide(); 
    } 
    });


});
</script>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nama Produsen</td>
                            @foreach($data1 as $val)
                            <td><input type="text" name="nama_produsen[]" class="form-control" value="{{$val->nama_produsen}}"></td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            @foreach($data1 as $val)
                            <td><input type="text" name="nomor_lot[]" class="form-control" value="{{$val->nomor_lot}}"></td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            @foreach($data1 as $val)
                            <td>
                              <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" value="{{$val->tgl_kadaluarsa}}" required="" class="form_datetime form-control" name="tgl_kadaluarsa[]" autocomplete="off">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                            </td>
                            @endforeach
                        </tr>
                    </table>
                    <label>4. HASIL PEMERIKSAAN </label><br>
                    <small>* Ket : Jika menggunakan Rapid sebagai metode pengujian, kosongkan kolom lain dan langsung mengisi hasil di bagian <b>Interpretasi Hasil</b></small>
                    <table class="table table-bordered">
                        <tr>
                            <th>Kode Bahan Uji</th>
                            <td>Abs atau OD (A) (Bila dengan  EIA)</td>
                            <td>Cut Off (B) (Bila dengan EIA)</td>
                            <td>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA)</td>
                            <td>Interpretasi hasil </td>
                        </tr>
                        <?php $no = 0; ?>
                        @foreach($data2 as $hasil)
                        <?php $no++; ?>
                        @if($hasil->tabung == '1')
                        <tr>
                            <input type="hidden" name="idhp[]" value="{{$hasil->id}}">
                            <td>
                                <div class="input-group">
                                    <div class="input-group-addon">IV-{{$idtabunga[$no - 1]}}-</div>
                                    <input type="number" class="form-control" id="tabung{{$idtabunga[$no - 1]}}2" name="kode_bahan_kontrol[]" readonly id="tabung11" required placeholder="999" value="{{$hasil->kode_bahan_kontrol}}">
                                </div>
                            </td>
                            <td><input type="text" class="decimal form-control" value="{{$hasil->abs_od}}" name="abs_od1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="{{$hasil->cut_off}}" name="cut_off1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="{{$hasil->sco}}" name="sco1[]"/></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non5">
                                    <option value="{{$hasil->interpretasi}}">{{$hasil->interpretasi}}</option>
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </table>
                    <div class="row">
                        @foreach($data as $val)
                        <div class="col-sm-6">
                            <label>Catatan :</label>
                            <textarea name="keterangan" class="form-control">{{$val->keterangan}}</textarea>
                        </div>
                        <div class="col-sm-6">
                            <label>Petugas yang melakukan pemeriksaan :</label>
                            <input type="text" name="petugas_pemeriksaan" class="form-control" value="{{$val->petugas_pemeriksaan}}" required>
                        </div><br>
                        @break
                        @endforeach
                    </div>
                    <small>Info : Mohon untuk mengecek kembali data yang sudah di input, karena data yang sudah di Kirim tidak bisa di edit kembali. </small><br>

                    {{ csrf_field() }}
                    <div class="clearfix"></div>
                    @if(Auth::user()->role == 3)
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px"> &nbsp;
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit" style="margin-top: 20px">
                    @else
                    <input type="submit" name="simpan" value="Validasi" class="btn btn-submit" style="margin-top: 20px">
                    @endif
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

$(document).ready(function(){
    $("#tabung11").change(function(){
        $val = $(this).val();
        $('#tabung12').val($val);
    });
    $("#tabung21").change(function(){
        $val1 = $(this).val();
        $('#tabung22').val($val1);
    });
    $("#tabung31").change(function(){
        $val2 = $(this).val();
        $('#tabung32').val($val2);
    });
});
$("input:checkbox").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input:checkbox[class='" + $box.attr("class") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});
today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;
</script>
<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minView: 2
    });
</script>
@endsection
