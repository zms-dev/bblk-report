@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" onsubmit="return confirm('Pastikan data hasil pemeriksaan anda sudah benar/hasil yang sudah terkirim sudah tidak dapat dirubah kembali');">
                {{ csrf_field() }}
                    <center><label>
                        FORMULIR HASIL PEMERIKSAAN PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL BALAI BESAR LABORATORIUM KESEHATAN JAKARTA<br> HEMATOLOGI SIKLUS {{$siklus}} TAHUN {{$date}} <input type="hidden" name="type" value="{{$type}}"></label></center><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$datas->kode_lab}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Bahan </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_bahan" name="kode_bahan" value="{{$datas->kode_bahan}}" placeholder="Kode Bahan" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan </label>
                        <div class="col-sm-9">
                              <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" value="{{$datas->tgl_penerimaan}}" readonly class="form_datetime form-control" name="tanggal_penerimaan">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kualitas Bahan </label>
                        <div class="col-sm-9">  
                          <input type="radio" name="kualitas" required value="baik" {{ ($datas->kualitas_bahan == 'baik') ? 'checked' : '' }}> Baik
                          <input type="radio" name="kualitas" value="tidak-dingin" {{ ($datas->kualitas_bahan == 'tidak-dingin') ? 'checked' : '' }}> Tidak Dingin
                          <input type="radio" name="kualitas" value="pecah" {{ ($datas->kualitas_bahan == 'pecah') ? 'checked' : '' }}> Pecah
                          <input type="radio" name="kualitas" value="lisis" {{ ($datas->kualitas_bahan == 'lisis') ? 'checked' : '' }}> Lisis
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Pemeriksaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-date="2017-07-07" data-link-field="dtp_input1">
                              <input size="16" type="text" value="{{$datas->tgl_pemeriksaan}}" readonly class="form_datetime form-control" name="tanggal_pemeriksaan">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Parameter</th>
                                <th>Instrument</th>
                                <th>Kode Metode Pemeriksaan</th>
                                <th>Hasil</th>
                                <th>Catatan</th>
                                <th>Tidak Dikerjakan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($data))
                            <?php $no = 0; ?>
                            @foreach($data as $val)
                            <?php $no++; ?>
                            <tr>
                                <td>{{$no}}<input type="hidden" name="id_detail[]" value="{{$val->id_detail}}"></td>
                                <td>{!!$val->nama_parameter!!}<input type="hidden" name="parameter_id[]" value="{{ $val->id }}" /></td>
                                <td>
                                    <select id="alat{{$val->id}}" class="form-control" name="alat[]" class="form-control" >
                                        <option value="{{$val->idinstrumen}}">{{$val->instrumen}}</option>
                                        @foreach($instrumen as $ins)
                                            <option value="{{$ins->id}}">{{$ins->instrumen}}</option>
                                        @endforeach
                                    </select>
                                    <div id="row_alat{{$val->id}}" class="inpualat{{$val->id}}">
                                        <input id="inpualat{{$val->id}}" class="form-control" type="text" name="instrument_lain[]" value="{{$val->instrument_lain}}" />
                                    </div>
                                </td>
                                <td id="metodelain{{$val->id}}">
                                    <select id="kodemetode{{$val->id}}" class="form-control" name="kode[]" class="form-control" >
                                        <option value="{{$val->Kode}}">{!!$val->metode_pemeriksaan!!}</option>
                                        @foreach($val->metode as $met)
                                        <option value="{{$met->id}}">{!!$met->metode_pemeriksaan!!}</option>
                                        @endforeach
                                    </select>
                                    <div id="row_dim{{$val->id}}" class="inputkode{{$val->id}}">
                                        <input id="inputkode{{$val->id}}" class="form-control" value="{!!$val->metode_lain!!}" type="text" name="metode_lain[]" />
                                    </div>
<script>                                 
$(document).ready(function(){
    $("#hasil{{$val->id}}").keyup(function(){
    var value = $(this).val();
        if (value == '-' || value == 'NaN'){
            console.log('bisa');
                $("#alat{{$val->id}}").prop('required',false);
                $("#kodemetode{{$val->id}}").prop('required',false);
        }else{
            if(value == ''){
                console.log('bisa');  
                $("#alat{{$val->id}}").prop('required',false);
                $("#kodemetode{{$val->id}}").prop('required',false);
            }else{
                console.log('gak bisa');
                $("#alat{{$val->id}}").prop('required',true);
                $("#kodemetode{{$val->id}}").prop('required',true);
            }
        }
    });
});                           
$(function() {
    @if(isset($val->instrument_lain))
    @else
    $('#row_alat{{$val->id}}').hide(); 
    @endif
    $('#alat{{$val->id}}').change(function(){
    var setan  = $("#alat{{$val->id}} option:selected").text();
        if(setan.match('Instrument lain.*')) {
            $('#row_alat{{$val->id}}').show(); 
            $("#inpualat{{$val->id}}").prop('required',true);
        } else {
            $('#row_alat{{$val->id}}').hide(); 
            $("#inpualat{{$val->id}}").prop('required',false);
            $("#inpualat{{$val->id}}").val('');
        } 
    });
});

$(function() {
    @if(isset($val->metode_lain))
    @else
    $('#row_dim{{$val->id}}').hide(); 
    @endif
    $('#kodemetode{{$val->id}}').change(function(){
    var setan  = $("#kodemetode{{$val->id}} option:selected").text();
        if(setan.match('Metode lain.*')) {
            $('#row_dim{{$val->id}}').show(); 
            $("#inputkode{{$val->id}}").prop('required',true);
        } else {
            $('#row_dim{{$val->id}}').hide(); 
            $("#inputkode{{$val->id}}").prop('required',false);
            $("#inputkode{{$val->id}}").val('');
        } 
    });
});
</script>
                                </td>
                                <td>
                                    @if($val->id == 1 || $val->id == 2 || $val->id == 5 || $val->id == 6 || $val->id == 7 || $val->id == 8)
                                        <input type="text" id="hasil{{$val->id}}" class="form-control hasil validate-hasil" size="16" name="hasil[]"  min="0" value="{{$val->Hasil}}" max="99.9" 
                                     onKeyUp="if(this.value>99.9){this.value='';}else if(this.value<0){this.value='';}" required placeholder="Maksimal 99.9">
                                    @elseif($val->id == 3)
                                        <input type="text" id="hasil{{$val->id}}" class="form-control hasil validate-hasil" size="16" name="hasil[]"  min="0" value="{{$val->Hasil}}" max="9.99" 
                                     onKeyUp="if(this.value>9.99){this.value='';}else if(this.value<0){this.value='';}" required placeholder="Maksimal 9.99">
                                    @elseif($val->id == 4)
                                        <input type="text" id="hasil{{$val->id}}" class="form-control hasil validate-hasil" size="16" name="hasil[]"  min="0" value="{{$val->Hasil}}" max="999" 
                                     onKeyUp="if(this.value>999){this.value='';}else if(this.value<0){this.value='';}" required placeholder="Maksimal 999">
                                 
                                    @endif

                                <td>{{$val->catatan}}</td>
                                <td><center>@if($val->Hasil != '-')<input type="checkbox" name="" id="tidak{{$val->id}}">@else<input type="checkbox" name="" id="tidak{{$val->id}}" checked>@endif</center></td>
                            </tr>
                            <script type="text/javascript">
                                $("#tidak{{$val->id}}").change(function() {
                                    if(this.checked) {
                                        $("#hasil{{$val->id}}").val('-');
                                        $("#kodemetode{{$val->id}}").val('');
                                        $("#alat{{$val->id}}").val('');
                                        $("#hasil{{$val->id}}").prop("readonly",true);
                                    }else{
                                        $("#hasil{{$val->id}}").val('');
                                        $("#hasil{{$val->id}}").prop("readonly",false);
                                    }
                                });
                            </script>
                            @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                    <label>Keterangan :</label>
                    <p>
                        - Pilih tombol “SIMPAN” untuk menyimpan perubahan input hasil<br>
                        - Pilih tombol “KIRIM” untuk mengirim hasil final Saudara ke penyelenggara<br>
                        - Hasil yang sudah dikirim tidak bisa dirubah lagi. Kesalahan dalam penginputan hasil yang telah dikirim sepenuhnya tanggung jawab peserta. <br>
                        - Info : Mohon untuk mengecek kembali data yang sudah di input, karena data yang sudah di Kirim tidak bisa di edit kembali.
                    </p><br>
                    <div class="col-sm-6">
                        <label>Catatan :</label>
                        <textarea class="form-control" name="catatan">{{$datas->catatan}}</textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Nama Penanggung jawab lab :</label>
                        <input type="text" name="penanggung_jawab" class="form-control" required value="{{$datas->penanggung_jawab}}">
                    </div><br>
                    <div class="clearfix"></div>
                    @if(Auth::user()->role == 3)
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px"> &nbsp;
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit" style="margin-top: 20px">
                    @else
                    <input type="submit" name="simpan" value="Validasi" class="btn btn-submit" style="margin-top: 20px">
                    @endif
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $('#autoUpdate').fadeOut('slow');
    $('#checkbox1').change(function(){
    if(this.checked)
        $('#autoUpdate').fadeIn('slow');
    else
        $('#autoUpdate').fadeOut('slow');

    });
});

@foreach($data as $val)
@if($val->catatan == 'Hasil pemeriksaan tanpa desimal')
$("body").on("keypress","#hasil{{$val->id}}", function(evt) {
    console.log(evt.keyCode)
  var keycode = evt.charCode || evt.keyCode;
  if (keycode == 46 || keycode == 44) {
    console.log('ada');
    return false;
  }
});
@endif
@endforeach

$(document).ready(function(){
    @foreach($data as $val)
    $('#hasil{{$val->id}}').blur(function(){
        var num = parseFloat($(this).val());
        @if($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal')
        console.log(num);
        var cleanNum = num.toFixed(1);
        @elseif($val->catatan == 'Hasil pemeriksaan menggunakan 2 (dua) desimal')
        var cleanNum = num.toFixed(2);
        @endif
        if (cleanNum == 'NaN') {
            $(this).val('-');
        }else{
            $(this).val(cleanNum);
        }
    });
    @endforeach
});

</script>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
</script>
@endsection
