@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pengujian</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label> FORMULIR HASIL PENGUJIAN SAMPEL KONTROL AIR
                                    PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL
                                    KIMIA AIR SIKLUS {{$siklus}} TAHUN 2018 INPUT {{$type}} <input type="hidden" name="type" value="{{$type}}">
                    </label></center><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$datas->kode_lab}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan Sampel</label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="{{$datas->tgl_penerimaan}}" readonly class="form-control form_datetime" name="tanggal_penerimaan">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kualitas Bahan </label>
                        <div class="col-sm-9">  
                          <input type="radio" name="kualitas" required value="baik" {{ ($datas->kualitas_bahan == 'baik') ? 'checked' : '' }}> Baik
                          <input type="radio" name="kualitas" value="lisis" {{ ($datas->kualitas_bahan == 'lisis') ? 'checked' : '' }}> Lisis
                          <input type="radio" name="kualitas" value="pecah" {{ ($datas->kualitas_bahan == 'pecah') ? 'checked' : '' }}> Pecah
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Penguji </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_pemeriksa" placeholder="Nama Pemeriksa" value="{{$datas->nama_pemeriksa}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nomor HP </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nomor_hp" placeholder="Nomor HP" value="{{$datas->nomor_hp}}">
                        </div>
                    </div>

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th rowspan="2">No</th>
                                <th rowspan="2">Parameter</th>
                                <th colspan="2"><center>Tanggal Penguji</center></th>
                                <th rowspan="2">Alat & Merk</th>
                                <th rowspan="2">Metode Penguji</th>
                                <th rowspan="2">Hasil Penguji</th>
                            </tr>
                            <tr>
                                <th><center>Mulai</center></th>
                                <th><center>Selesai</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($data))
                            <?php $no = 0; ?>
                            @foreach($data as $val)
                            <?php $no++; ?>
                            <tr>
                                <td>{{$no}}</td><input type="hidden" name="id_detail[]" value="{{$val->IDDetails}}">
                                <td>{!!$val->nama_parameter!!}<input type="hidden" name="parameter_id[]" value="{{ $val->id }}" /></td>
                                <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="{{$val->tanggal_mulai}}" readonly class="form-control form_datetime" name="tanggal_mulai[]" required>
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </td>
                                <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="{{$val->tanggal_selesai}}" readonly class="form-control form_datetime" name="tanggal_selesai[]" required>
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </td>
                                <td><input type="text" class="form-control" size="16" value="{{ $val->Alat }}" name="alat[]" required></td>
                                <td id="metodelain{{$val->id}}">
                                    <input type="text" class="form-control" size="16" name="kode[]" required value="{!! $val->Kode !!}">
<!--                                     <div id="row_dim{{$val->id}}" class="inputkode{{$val->id}}">
                                        <input id="inputkode{{$val->id}}" class="form-control" type="text" name="metode_lain[]" />
                                    </div> -->
<script>
                                 
// $('#kodemetode{{$val->id}}').change(function(){
//     var setan  = $("#kodemetode{{$val->id}} option:selected").text();
//     if( setan == 'Metode lain :'){
//         $('#metodelain{{$val->id}}').append('<input id="inputkode{{$val->id}}" class="form-control" type="text" name="metode_lain[]" />');
//     }else{
//         $('#inputkode{{$val->id}}').remove();
//     }
// });
$(function() {
    $('#row_dim{{$val->id}}').hide(); 
    $('#kodemetode{{$val->id}}').change(function(){
    var setan  = $("#kodemetode{{$val->id}} option:selected").text();
        if(setan.match('Metode lain.*') ) {
            $('#row_dim{{$val->id}}').show(); 
        } else {
            $('#row_dim{{$val->id}}').hide(); 
        } 
    });
});
</script>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <input type="text" class="form-control decimal" id="hasil{{$val->id}}" size="16" name="hasil[]" maxlength="5" required value="{{ $val->Hasil }}">
                                        <div class="input-group-addon">mg/l</div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <label>Keterangan :</label>
                    <p> 
                    -   Pada kolom “Alat & Merk”, tulis Nama dan Merk Alat yang digunakan<br> 
                    </p><br>
                    <div class="col-sm-6">
                        <label>Catatan :</label>
                        <textarea class="form-control" name="catatan">{{ $datas->catatan }}</textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Nama Penanggung jawab lab :</label>
                        <input type="text" name="penanggung_jawab" class="form-control" value="{{ $datas->penanggung_jawab }}" required>
                    </div><br>
                      {{ csrf_field() }}
                    <div class="clearfix"></div>
                    @if(Auth::user()->role == 3)
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px"> &nbsp;
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit" style="margin-top: 20px">
                    @else
                    <input type="submit" name="simpan" value="Validasi" class="btn btn-submit" style="margin-top: 20px">
                    @endif
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('.decimal').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});

$(document).ready(function(){
    $('#autoUpdate').fadeOut('slow');
    $('#checkbox1').change(function(){
    if(this.checked)
        $('#autoUpdate').fadeIn('slow');
    else
        $('#autoUpdate').fadeOut('slow');

    });
});

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;



function dots(){
var dateBox = document.getElementById('hasil22')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 1) {
        dateBox.value = text + ".";
    }

var dateBox = document.getElementById('hasil25')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 2) {
        dateBox.value = text + ".";
    }

var dateBox = document.getElementById('hasil26')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 2) {
        dateBox.value = text + ".";
    }

var dateBox = document.getElementById('hasil30')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 1) {
        dateBox.value = text + ".";
    }
        
var dateBox = document.getElementById('hasil31')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 2) {
        dateBox.value = text + ".";
    }
}

function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode

  // CHECK IF NUMBER
  if((charCode >=48 && charCode<=57) || (charCode>=96 && charCode<=105)){
     //IF KEY IS A NUMBER CALL DOTS
    dots();
  }
  else if(charCode==37||charCode==39||charCode==46 || charCode==8){
      // LET LEFT RIGHT BACKSPACE AND DEL PASS
}
    else{
    // BLOCK ALL OTHER KEYS
    evt.preventDefault();
    }
}
</script>
<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minView: 2
    });
</script>
@endsection
