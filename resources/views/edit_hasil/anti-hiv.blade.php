@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" onsubmit="return confirm('Pastikan data hasil pemeriksaan anda sudah benar/hasil yang sudah terkirim sudah tidak dapat dirubah kembali');">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PROGRAM  NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}<br> HASIL PEMERIKSAAN ANTI HIV</label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta </label>
                        <div class="col-sm-10">
                            @foreach($data as $val)
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$val->kode_lab}}" placeholder="Kode Peserta" readonly>
                            </div>
                            <input type="hidden" name="idmaster" value="{{$val->id}}">
                            @break
                            @endforeach
                        </div>
                    <label>2. BAHAN UJI :</label>
                    <table class="table-bordered table">
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>
                                    @foreach($data as $val)
                                  <div class="controls input-append date" data-date="2017-07-07" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="{{$val->tgl_diterima}}" readonly class="form_datetime form-control" name="tgl_diterima" autocomplete="off">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                    @break
                                    @endforeach
                                </td>
                                <td>Tanggal pemeriksaan :</td>
                                <td>
                                    @foreach($data as $val)
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="{{$val->tgl_diperiksa}}" readonly class="form_datetime form-control" name="tgl_diperiksa" autocomplete="off">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                    @break
                                    @endforeach
                                </td>
                            </tr>
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="6" width="20%"> Kondisi bahan uji saat diterima</td>
                                <td>Kode bahan uji <small>*(Wajib angka dan 3 digit)</small></td>
                                <td>Baik/Jernih</td>
                                <td>Keruh</td>
                                <td>Lain-lain</td>
                            </tr>
                            <?php  
                                $no = 0;
                                $idtabunga = ['1','2','3','4','5'];
                            ?>
                            @foreach($data as $val)
                            <tr><input type="hidden" value="{{$val->idimunologi}}" name="idbahan[]">
                                <td>
                                    <div class="input-group">
                                        <div class="input-group-addon">I-{{$idtabunga[$no]}}-</div>
                                        <input type="text" class="form-control badan{{$no}}" id="tabung{{$idtabunga[$no]}}1" name="no_tabung[]" max="999" required placeholder="999" value="{{$val->no_tabung}}"  pattern="[0-9]{3}">
                                    </div>
                                </td>
                                <td><input class="badan{{$no}}" type="checkbox" name="jenis[]" value="baik/jernih" {{ ($val->jenis == 'baik/jernih') ? 'checked' : '' }}></td>
                                <td><input class="badan{{$no}}" type="checkbox" name="jenis[]" value="keruh" {{ ($val->jenis == 'keruh') ? 'checked' : '' }}></td>
                                <td><input type="checkbox" name="jenis[]" class="lain2{{$val->idimunologi}} badan{{$no}}" value="lain-lain" {{ ($val->jenis == 'lain-lain') ? 'checked' : '' }}>
                                </td>
                            </tr>
                            <?php $no++;?>
                            @endforeach
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Keterangan</td>
                            <td>Tes I</td>
                            <td>Tes II</td>
                            <td>Tes III</td>
                        </tr>
                        <tr>
                            <td>Nama Reagen</td>
                            <?php $no = 0; ?>
                            @foreach($data1 as $val)
                            <?php $no++; ?>
                            <input type="hidden" value="{{$val->id}}" name="idreagen[]">
                            <td>
                                <select id="alat{{$no}}" class="form-control" name="nama_reagen[]" idx="{{$no}}" class="form-control" required>
                                    <option value="{{$val->Idreagen}}">{{$val->namareagen}}</option>
                                    @foreach($reagen as $valu)
                                      <option value="{{$valu->id}}">{{$valu->reagen}}</option>
                                    @endforeach
                                </select>
                                <div id="row_alat{{$no}}" class="inpualat{{$no}}">
                                    <input id="inpualat{{$no}}" class="form-control" type="text" name="reagen_lain[]" value="{{$val->reagen_lain}}" />
                                </div>
                            </td>
<script>   
$(function() {
    var setan  = $("#alat{{$no}} option:selected").text();
    if(setan.match('Lain - lain.*')) {
        $('#inpualat{{$no}}').prop('required', true).show(); 
        $('#produsen{{$no}}').prop('readonly', false).show(); 
    } else {
        $('#inpualat{{$no}}').prop('required', false).hide(); 
        $('#produsen{{$no}}').prop('readonly', true).show(); 
    }

    $('#alat{{$no}}').change(function(){
    var setan  = $("#alat{{$no}} option:selected").text();
    if(setan.match('Lain - lain.*')) {
        $('#produsen{{$no}}').prop('readonly', false).show(); 
        $('#inpualat{{$no}}').prop('required', true).show(); 
    } else {
        $('#produsen{{$no}}').prop('readonly', true).show(); 
        $('#inpualat{{$no}}').prop('required', false).hide(); 
    } 
    });


});
</script>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Metode Pemeriksaan</td>
                            <?php $no = 0;?>
                            @foreach($data1 as $val)
                            <?php $no++;?>
                            <td>
                                <input type="text" name="metode[]" value="{{$val->metode}}" id="re{{$no}}" required="" class="form-control">
                            </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nama Produsen</td>
                            <?php $no = 0;?>
                            @foreach($data1 as $val)
                            <?php $no++;?>
                            <td><input type="text" name="nama_produsen[]" class="form-control" id="produsen{{$no}}" value="{{$val->nama_produsen}}"></td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            @foreach($data1 as $val)
                            <td><input type="text" name="nomor_lot[]" class="form-control" value="{{$val->nomor_lot}}"></td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            @foreach($data1 as $val)
                            <td>
                              <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" value="{{$val->tgl_kadaluarsa}}" required="" class="form_datetime form-control" name="tgl_kadaluarsa[]" autocomplete="off">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                            </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Sensitifitas </td>
                            <?php $no = 0;?>
                            @foreach($data1 as $val)
                            <?php $no++;?>
                            @if($val->Sensitivitas == NULL)
                                <td><input type="text" class="form-control" id="sen{{$no}}" value="{{$val->sensitivitas}}" name="sensitivitas[]"></td>
                            @else
                                <td><input type="text" class="form-control" id="sen{{$no}}" value="{{$val->Sensitivitas}}" name="sensitivitas[]"></td>
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <td>Spesitivitas </td>
                            <?php $no = 0;?>
                            @foreach($data1 as $val)
                            <?php $no++;?>
                            @if($val->Sensitivitas == NULL)
                                <td><input type="text" class="form-control" id="sep{{$no}}" value="{{$val->spesifisitas}}" name="spesifisitas[]"></td>
                            @else
                                <td><input type="text" class="form-control" id="sep{{$no}}" value="{{$val->Spesifisitas}}" name="spesifisitas[]"></td>
                            @endif
                            @endforeach
                        </tr>
                    </table>
                    <label>4. HASIL PEMERIKSAAN</label><br>
                    <small>* Ket : Jika menggunakan Rapid sebagai metode pengujian, kosongkan kolom lain dan langsung mengisi hasil di bagian <b>Interpretasi Hasil</b></small>
                    <table class="table table-bordered">
                        <tr>
                            <td>Kode Bahan Uji</td>
                            @foreach($data2 as $hasil)
                            @if($hasil->tabung == '1')
                            <td colspan="4">
                                <div class="input-group">
                                    <div class="input-group-addon">I-1-</div>
                                    <input type="number" class="form-control" id="tabung12" name="kode_bahan_kontrol[]" readonly required placeholder="999" value="{{$hasil->kode_bahan_kontrol}}">
                                </div>
                            </td>
                            @break
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <td>Tes</td>
                            <td>Abs atau OD (A) (Bila dengan  EIA / Setara)</td>
                            <td>Cut Off (B) (Bila dengan EIA / Setara)</td>
                            <td>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</td>
                            <td>Interpretasi hasil</td>
                        </tr>
                        <?php  
                            $no = 0;
                            $val = ['I','II','III','IV'];
                        ?>
                        @foreach($data2 as $hasil)
                        @if($hasil->tabung == '1')
                        <tr>
                            <input type="hidden" name="idhp[]" value="{{$hasil->id}}">
                            <td>{{$val[$no]}}</td>
                            <td><input type="text" name="abs_od1[]" class="form-control decimal" value="{{$hasil->abs_od}}"></td>
                            <td><input type="text" name="cut_off1[]" class="form-control decimal" value="{{$hasil->cut_off}}"></td>
                            <td><input type="text" name="sco1[]" class="form-control decimal" value="{{$hasil->sco}}"></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non5">
                                    <option value="{{$hasil->interpretasi}}">{{$hasil->interpretasi}}</option>
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endif
                        @endforeach
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <td>Kode Bahan Uji</td>
                            @foreach($data2 as $hasil)
                            @if($hasil->tabung == '2')
                            <td colspan="4">
                                <div class="input-group">
                                    <div class="input-group-addon">I-2-</div>
                                    <input type="number" class="form-control" id="tabung22" name="kode_bahan_kontrol[]" readonly required placeholder="999" value="{{$hasil->kode_bahan_kontrol}}">
                                </div>
                            </td>
                            @break
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <td>Tes</td>
                            <td>Abs atau OD (A) (Bila dengan  EIA / Setara)</td>
                            <td>Cut Off (B) (Bila dengan EIA / Setara)</td>
                            <td>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</td>
                            <td>Interpretasi hasil</td>
                        </tr>
                        <?php  
                            $no = 0;
                            $val = ['I','II','III','IV'];
                        ?>
                        @foreach($data2 as $hasil)
                        @if($hasil->tabung == '2')
                        <tr>
                            <input type="hidden" name="idhp[]" value="{{$hasil->id}}">
                            <td>{{$val[$no]}}</td>
                            <td><input type="text" name="abs_od2[]" class="form-control decimal" value="{{$hasil->abs_od}}"></td>
                            <td><input type="text" name="cut_off2[]" class="form-control decimal" value="{{$hasil->cut_off}}"></td>
                            <td><input type="text" name="sco2[]" class="form-control decimal" value="{{$hasil->sco}}"></td>
                            <td>
                                <select name="interpretasi2[]" class="form-control non5">
                                    <option value="{{$hasil->interpretasi}}">{{$hasil->interpretasi}}</option>
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endif
                        @endforeach
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <td>Kode Bahan Uji</td>
                            @foreach($data2 as $hasil)
                            @if($hasil->tabung == '3')
                            <td colspan="4">
                                <div class="input-group">
                                    <div class="input-group-addon">I-3-</div>
                                    <input type="number" class="form-control" id="tabung32" name="kode_bahan_kontrol[]" readonly required placeholder="999" value="{{$hasil->kode_bahan_kontrol}}">
                                </div>
                            </td>
                            @break
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <td>Tes</td>
                            <td>Abs atau OD (A) (Bila dengan  EIA / Setara)</td>
                            <td>Cut Off (B) (Bila dengan EIA / Setara)</td>
                            <td>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</td>
                            <td>Interpretasi hasil</td>
                        </tr>
                        <?php  
                            $no = 0;
                            $val = ['I','II','III','IV'];
                        ?>
                        @foreach($data2 as $hasil)
                        @if($hasil->tabung == '3')
                        <tr>
                            <input type="hidden" name="idhp[]" value="{{$hasil->id}}">
                            <td>{{$val[$no]}}</td>
                            <td><input type="text" name="abs_od3[]" class="form-control decimal" value="{{$hasil->abs_od}}"></td>
                            <td><input type="text" name="cut_off3[]" class="form-control decimal" value="{{$hasil->cut_off}}"></td>
                            <td><input type="text" name="sco3[]" class="form-control decimal" value="{{$hasil->sco}}"></td>
                            <td>
                                <select name="interpretasi3[]" class="form-control non5">
                                    <option value="{{$hasil->interpretasi}}">{{$hasil->interpretasi}}</option>
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <?php $no++; ?>
                        @endif
                        @endforeach
                    </table>
                    <div class="row">
                        @foreach($data as $val)
                        <div class="col-sm-6">
                            <label>Catatan :</label>
                            <textarea name="keterangan" class="form-control">{{$val->keterangan}}</textarea>
                        </div>
                        <div class="col-sm-6">
                            <label>Petugas yang melakukan pemeriksaan :</label>
                            <input type="text" name="petugas_pemeriksaan" class="form-control" value="{{$val->petugas_pemeriksaan}}" required>
                        </div><br>
                        @break
                        @endforeach
                    </div>
                    <small>Info : Mohon untuk mengecek kembali data yang sudah di input, karena data yang sudah di Kirim tidak bisa di edit kembali. </small><br>

                    {{ csrf_field() }}
                    <div class="clearfix"></div>
                    @if(Auth::user()->role == 3)
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px"> &nbsp;
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit" style="margin-top: 20px">
                    @else
                    <input type="submit" name="simpan" value="Validasi" class="btn btn-submit" style="margin-top: 20px">
                    @endif
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('.decimal').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});
</script>

<script type="text/javascript">

$(document).ready(function(){
    $("#tabung11").keyup(function(){
        $val = $(this).val();
        $('#tabung12').val($val);
    });
    $("#tabung21").keyup(function(){
        $val1 = $(this).val();
        $('#tabung22').val($val1);
    });
    $("#tabung31").keyup(function(){
        $val2 = $(this).val();
        $('#tabung32').val($val2);
    });
});

$("select[name='nama_reagen[]']").change(function(){
  var val = $(this);
  console.log(val);
  var y = $('#re'+val.attr('idx'));
  var x = $('#produsen'+val.attr('idx'));
  var se = $('#sen'+val.attr('idx'));
  var sp = $('#sep'+val.attr('idx'));
  var n = $('#lot'+val.attr('idx'));
  var t = $('#tanggal'+val.attr('idx'));
  $.ajax({
    type: "GET",
    url : "{{url('getreagenimun').'/'}}"+val.val(),
    success: function(addr){
        y.val(addr.Hasil[0].Metode);
        x.val(addr.Hasil[0].Produsen);
        se.val(addr.Hasil[0].sensitivitas);
        sp.val(addr.Hasil[0].spesifisitas);
        if (y.val() == '-') {
            n.attr("required",false);
            t.attr("required",false);
        }else{
            n.attr("required",true);
            t.attr("required",true);
        }
    }
  });
});

$("input:checkbox").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input:checkbox[class='" + $box.attr("class") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});

$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});

</script>
@endsection
