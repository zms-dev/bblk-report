@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MIKROSKOPIS MALARIA SIKLUS {{$siklus}} TAHUN {{date('Y', strtotime($data->created_at))}} TAHUN</label></center><br>
                    
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$data->kode_peserta}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="{{$data->tgl_penerimaan}}" readonly class="form_datetime form-control" name="tgl_penerimaan" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kondisi Bahan Kontrol </label>
                        <div class="col-sm-9">  
                          <input type="radio" name="kondisi" required value="baik" {{ ($data->kondisi == 'baik') ? 'checked' : '' }}> Baik
                          <input type="radio" name="kondisi" value="pecah/tumpah" {{ ($data->kondisi == 'pecah/tumpah') ? 'checked' : '' }}> Pecah / Tumpah
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Pemeriksaan </label>
                        <div class="col-sm-9">
                            <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" value="{{$data->tgl_pemeriksaan}}" readonly class="form_datetime form-control" name="tgl_pemeriksaan" required>
                                  <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Pemeriksa </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_pemeriksa" placeholder="Nama Pemeriksa" required value="{{$data->nama_pemeriksa}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nomor Hp </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nomor_hp" placeholder="Nomor HP" required value="{{$data->nomor_hp}}">
                        </div>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Sediaan</th>
                                <th>Hasil Pemeriksaan</th>
                            </tr>
                        </thead>
                        <tbody id="wrapper">
                            <?php $i = 0;?>
                            @foreach($datas as $val)
                            <?php $i++; ?>
                            <tr>
                                <td>{{$i}}<input type="hidden" name="idmal[{{$i}}]" value="{{$val->id}}"></td>
                                <td><input type="text" class="form-control" size="16" name="kode[{{$i}}]" value="{{$val->kode}}" required></td>
                                <td>
                                    <select class="form-control" name="hasil[{{$i}}]" required id="alat{{$i}}">
                                        <option value="{{$val->hasil}}">{{$val->hasil}}</option>
                                        <option value="Positif">Positif</option>
                                        <option value="Negatif">Negatif</option>
                                    </select>
                                <div id="row_alat{{$i}}" class="inpualat3">
                                    <select class="form-control" name="spesies[{{$i}}]">
                                        <option value="{{$val->spesies}}">{{$val->spesies}}</option>
                                        <option value="Plasmodium falciparum">Plasmodium falciparum</option>
                                        <option value="Plasmodium vivax">Plasmodium vivax</option>
                                        <option value="Plasmodium malariae">Plasmodium malariae</option>
                                        <option value="Plasmodium ovale">Plasmodium ovale</option>
                                        <option value="Plasmodium knowlesi">Plasmodium knowlesi</option>
                                        <option value="Mix (Plasmodium falciparum dan Plasmodium vivax">Mix (Plasmodium falciparum dan Plasmodium vivax</option>
                                        <option value="Mix (Plasmodium falciparum dan Plasmodium malariae)">Mix (Plasmodium falciparum dan Plasmodium malariae)</option>
                                    </select>
                                    <?php
                                        $stadium = explode(",", $val->stadium);
                                    ?>
                                    <select name="stadium[{{$i}}][]" class="selectpicker form-control" multiple>
                                        <option value="" disabled>Pilih Stadium</option>
                                        <?php
                                            $selected = "";
                                            foreach ($stadium as $fkey => $value) {
                                                if($selected == ""){
                                                    if($value == "Tropozoid"){
                                                        $selected = 'selected="selected"';
                                                    }
                                                }
                                            }
                                        ?>
                                        <option value="Tropozoid"  {{ $selected }}>Tropozoid</option>
                                        <?php
                                            $selected = "";
                                            foreach ($stadium as $fkey => $value) {
                                                if($selected == ""){
                                                    if($value == "Schizont"){
                                                        $selected = 'selected="selected"';
                                                    }
                                                }
                                            }
                                        ?>
                                        <option value="Schizont" {{ $selected }}>Schizont</option>
                                        <?php
                                            $selected = "";
                                            foreach ($stadium as $fkey => $value) {
                                                if($selected == ""){
                                                    if($value == "Gametosit"){
                                                        $selected = 'selected="selected"';
                                                    }
                                                }
                                            }
                                        ?>
                                        <option value="Gametosit" {{ $selected }}>Gametosit</option>
                                    </select>
                                    <div class="input-group">
                                    <input type="text" name="parasit[{{$i}}]" class="form-control" value="{{$val->parasit}}" placeholder="Jumlah Parasit">
                                      <div class="input-group-addon">parasit/&#181;l darah</div>
                                    </div>
                                </div>
                                </td>
                            </tr>
<script>                            
$(function() {
    @if($val->hasil == 'Positif')
    $('#row_alat{{$i}}').show(); 
    @else
    $('#row_alat{{$i}}').hide(); 
    @endif
    $('#alat{{$i}}').change(function(){
    var setan  = $("#alat{{$i}} option:selected").text();
        if(setan.match('Positif.*')) {
            $('#row_alat{{$i}}').show(); 
        } else {
            $('#row_alat{{$i}}').hide(); 
        } 
    });
});
</script>                            
                            @endforeach
                        </tbody>
                    </table>
                    <!-- <div><input type="button" id="more_fields" onclick="add_fields();" value="Tambah Baris" /></div> -->
                    <div class="col-sm-6">
                        <label>Catatan :</label>
                        <textarea class="form-control" name="catatan">{{$data->catatan}}</textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Nama Penanggung jawab lab :</label>
                        <input type="text" name="penanggung_jawab" class="form-control" value="{{$data->penanggung_jawab}}" required>
                    </div><br>

                      {{ csrf_field() }}
                    <div class="clearfix"></div>
                    @if(Auth::user()->role == 3)
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px"> &nbsp;
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit" style="margin-top: 20px">
                    @else
                    <input type="submit" name="simpan" value="Validasi" class="btn btn-submit" style="margin-top: 20px">
                    @endif
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;
</script>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minView: 2
    });

function add_fields() {
    document.getElementById('wrapper').innerHTML += '<tr><td><input type="text" class="form-control" size="16" name="kode[]"></td><td><input type="text" class="form-control" size="16" name="hasil[]"></td></tr>';
}
$('.selectpicker').selectpicker({
  style: 'btn-info',
  size: 2
});
</script>
@endsection
