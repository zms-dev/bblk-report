@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" onsubmit="return confirm('Pastikan data hasil pemeriksaan anda sudah benar/hasil yang sudah terkirim sudah tidak dapat dirubah kembali');">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PROGRAM  NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI SIKLUS 1 TAHUN 2018<br> HASIL PEMERIKSAAN RPR</label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta </label>
                        <div class="col-sm-10">
                            @foreach($data as $val)
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$val->kode_lab}}" placeholder="Kode Peserta" readonly>
                            <input type="hidden" name="idmaster" value="{{$val->id}}">
                            @break
                            @endforeach
                        </div>
                    </div>
                    <label>2. BAHAN UJI :</label>
                    <table class="table-bordered table">
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>
                                @foreach($data as $val)
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="{{$val->tgl_diterima}}" readonly class="form_datetime form-control" name="tgl_diterima">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                @break
                                @endforeach
                                </td>
                                <td>Diperiksa tanggal :</td>
                                <td>
                                @foreach($data as $val)
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="{{$val->tgl_diperiksa}}" readonly class="form_datetime form-control" name="tgl_diperiksa">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                @break
                                @endforeach
                                </td>
                            </tr>
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="6" width="20%"> Kondisi bahan Uji saat diterima</td>
                                <td>Kode Bahan Uji</td>
                                <td>Baik/Jernih </td>
                                <td>Keruh </td>
                                <td>Lain-lain</td>
                            </tr>
                            <?php  
                                $no = 0;
                                $idtabunga = ['1','2','3','4','5'];
                            ?>
                            @foreach($data as $val)
                            <tr>
                                <input type="hidden" value="{{$val->idimunologi}}" name="idbahan[]">
                                <td><input type="text" name="no_tabung[]" id="tabung{{$idtabunga[$no]}}1" class="form-control badan{{$no}}" value="{{$val->no_tabung}}"></td>
                                <td><input class="badan{{$no}}" type="checkbox" name="jenis[]" value="baik/jernih" {{ ($val->jenis == 'baik/jernih') ? 'checked' : '' }}></td>
                                <td><input class="badan{{$no}}" type="checkbox" name="jenis[]" value="keruh" {{ ($val->jenis == 'keruh') ? 'checked' : '' }}></td>
                                <td>
                                    <input type="checkbox" name="jenis[]" class="lain2{{$val->idimunologi}} badan{{$no}}" value="lain-lain" {{ ($val->jenis == 'lain-lain') ? 'checked' : '' }}>
                                    <div id="row_dim{{$no}}">
                                        <input type="text" name="lain[]" class="laintext1 form-control" value="{{$val->lain}}">
                                    </div>
                                </td>
                            </tr>
                            <script type="text/javascript">
                            $(function() {
                              enable_cb{{$no}}();
                              $(".lain2{{$val->idimunologi}}").click(enable_cb{{$no}});
                            });
                            function enable_cb{{$no}}() {
                              if (this.checked) {
                                $('#row_dim{{$no}}').show(); 
                              } else {
                                $('#row_dim{{$no}}').hide(); 
                              }
                            }
                            </script>
                            <?php $no++;?>
                            @endforeach
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Metode Pemeriksaan </td>
                            @foreach($data1 as $val)
                            <td><input class="metode_pemeriksaan" type="checkbox" name="metode[]" value="flokulasi" {{ ($val->metode == 'flokulasi') ? 'checked' : '' }}> Flokulasi <input class="metode_pemeriksaan" type="checkbox" name="metode[]" value="lain-lain" {{ ($val->metode == 'lain-lain') ? 'checked' : '' }}> Lain-lain</td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>Nama Reagen</td>
                            @foreach($data1 as $val)
                            <input type="hidden" value="{{$val->id}}" name="idreagen[]">
                            <td>
                                <select id="alat1" class="form-control" name="nama_reagen[]" class="form-control" >
                                    <option value="{{$val->Idreagen}}">{{$val->namareagen}}</option>
                                    @foreach($reagen as $valu)
                                      <option value="{{$valu->id}}">{{$valu->reagen}}</option>
                                    @endforeach
                                </select>
                                <div id="row_alat1" class="inpualat1">
                                    <input id="inpualat1" class="form-control" type="text" name="reagen_lain[]" value="{{$val->reagen_lain}}" />
                                </div>
                            </td>
<script>                            
$(function() {
    var setan  = $("#alat1 option:selected").text();
    if(setan.match('Lain - lain.*')) {
        $('#inpualat1').prop('required', true).show(); 
    } else {
        $('#inpualat1').prop('required', false).hide(); 
    }

    $('#alat1').change(function(){
    var setan  = $("#alat1 option:selected").text();

    if(setan.match('Lain - lain.*')) {
    $('#inpualat1').prop('required', true).show(); 
    } else {
        $('#inpualat1').prop('required', false).hide(); 
    } 
    });


});
</script>                            
                            @endforeach
                            </td>
                        </tr>
                        @foreach($data1 as $val)
                        <tr>
                            <td>Nama Produsen</td>
                            <td><input type="text" name="nama_produsen[]" class="form-control" required  value="{{$val->nama_produsen}}"></td>
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            <td><input type="text" name="nomor_lot[]" class="form-control" required value="{{$val->nomor_lot}}"></td>
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            <td>
                              <div class="controls input-append date form_datemo" data-link-field="dtp_input1">
                                  <input size="16" type="text"  value="{{$val->tgl_kadaluarsa}}" readonly class="form-control" name="tgl_kadaluarsa[]">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <label>4. HASIL PEMERIKSAAN </label>
                    <table class="table table-bordered">
                        <tr>
                            <th>Kode Bahan Uji</th>
                            <td>Interpretasi hasil </td>
                            <td>Titer (Bila Aglutinasi)</td>
                        </tr>
                        <?php $no = 0; ?>
                        @foreach($data2 as $hasil)
                        <?php $no++; ?>
                        <input type="hidden" name="idhp[]" value="{{$hasil->id}}">
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung12" class="form-control" required  value="{{$hasil->kode_bahan_kontrol}}" readonly></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non{{$no}}">
                                    <option value="{{$hasil->interpretasi}}">{{$hasil->interpretasi}}</option>
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                            <td>
                                <input list="titer{{$no}}" name="titer[]" class="form-control" value="{{$hasil->titer}}" id="tit{{$no}}"/>
                                <datalist id="titer{{$no}}">
                                    <option value="1:1"></option><option value="1:2"></option><option value="1:4"></option><option value="1:8"></option><option value="1:16"></option><option value="1:32"></option><option value="1:64"></option><option value="1:128"></option><option value="1:256"></option><option value="1:512"></option>
                                </datalist>
                                </select>
                            </td>
                        </tr>
<script>
$(function() {
    $('#tit{{$no}}').prop('readonly', true);
    $('.non{{$no}}').change(function(){
    var setan  = $(".non{{$no}} option:selected").text();
        if(setan.match('Non Reaktif.*')) {
            $('#tit{{$no}}').prop('readonly', true);
        } else {
            $('#tit{{$no}}').prop('readonly', false);
        } 
    });
});
</script>                        
                        @endforeach
                    </table>
                    <div class="row">
                        @foreach($data as $val)
                        <div class="col-sm-6">
                            <label>Catatan :</label>
                            <textarea name="keterangan" class="form-control">{{$val->keterangan}}</textarea>
                        </div>
                        <div class="col-sm-6">
                            <label>Petugas yang melakukan pemeriksaan :</label>
                            <input type="text" name="petugas_pemeriksaan" class="form-control" value="{{$val->petugas_pemeriksaan}}" required>
                        </div><br>
                        @break
                        @endforeach
                    </div>
                    <small>Info : Mohon untuk mengecek kembali data yang sudah di input, karena data yang sudah di Kirim tidak bisa di edit kembali. </small><br>
                      {{ csrf_field() }}
                    <div class="clearfix"></div>
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px"> &nbsp;
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit" style="margin-top: 20px">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;
</script>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});

$(".form_datemo").datetimepicker({
    todayBtn: true,
    autoclose: true,
    format: "yyyy-mm-dd",
    minView: 3
});
$(document).ready(function(){
    $("#tabung11").change(function(){
        $val = $(this).val();
        $('#tabung12').val($val);
    });
    $("#tabung21").change(function(){
        $val1 = $(this).val();
        $('#tabung22').val($val1);
    });
    $("#tabung31").change(function(){
        $val2 = $(this).val();
        $('#tabung32').val($val2);
    });
    $("#tabung41").change(function(){
        $val1 = $(this).val();
        $('#tabung42').val($val1);
    });
    $("#tabung51").change(function(){
        $val2 = $(this).val();
        $('#tabung52').val($val2);
    });
});

$("input:checkbox").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input:checkbox[class='" + $box.attr("class") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});
</script>
@endsection
