@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data" onsubmit="return confirm('Pastikan data hasil pemeriksaan anda sudah benar/hasil yang sudah terkirim sudah tidak dapat dirubah kembali');">
                    <center><label> 
                        FORMULIR HASIL PEMERIKSAAN PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL URINALISIS <br>SIKLUS {{$siklus}} TAHUN {{$date}} <input type="hidden" name="type" value="{{$type}}">
                    </label></center><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$datas->kode_lab}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Bahan </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_bahan" name="kode_bahan" value="{{$datas->kode_bahan}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-date="2017-07-07" data-link-field="dtp_input1">
                              <input size="16" type="text" value="{{$datas->tgl_penerimaan}}" readonly class="form_datetime form-control" name="tanggal_penerimaan">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kualitas Bahan </label>
                        <div class="col-sm-9">  
                          <input type="radio" name="kualitas" required value="baik" {{ ($datas->kualitas_bahan == 'baik') ? 'checked' : '' }}> Baik
                          <input type="radio" name="kualitas" value="tidak-dingin" {{ ($datas->kualitas_bahan == 'tidak-dingin') ? 'checked' : '' }}> Tidak Dingin
                          <input type="radio" name="kualitas" value="pecah" {{ ($datas->kualitas_bahan == 'pecah') ? 'checked' : '' }}> Pecah
                          <input type="radio" name="kualitas" value="lisis" {{ ($datas->kualitas_bahan == 'lisis') ? 'checked' : '' }}> Lisis
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Pemeriksaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-date="2017-07-07" data-link-field="dtp_input1">
                              <input size="16" type="text" value="{{$datas->tgl_pemeriksaan}}" readonly class="form_datetime form-control" name="tanggal_pemeriksaan">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Parameter</th>
                                <th>Instrument</th>
                                <th>Reagen/Kit Carik Celup</th>
                                <th>Metode Pemeriksaan</th>
                                <th>Hasil Pemeriksaan</th>
                                <th>Catatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($data != NULL)
                            <?php $no = 0; ?>
                            @foreach($data as $val)
                            <?php $no++; ?>
                            <tr>
                                <td>{{$no}}<input type="hidden" name="id_detail[]" value="{{$val->id_detail}}"></td>
                                <td>{!!$val->nama_parameter!!}<input type="hidden" name="parameter_id[]" value="{{ $val->id }}" /></td>
                                <td><input type="text" class="form-control" size="16" name="alat[]" value="{{$val->alat}}"></td>
                                <td>
                                    <select id="alat{{$val->id}}" class="form-control" name="reagen[]" class="form-control" >
                                        <option value="{{$val->hp_reagen}}">
                                        @if($val->imu_reagen != NULL){!!$val->imu_reagen!!}@else{{$val->hp_reagen}}@endif</option>
                                        @if($val->id == '19')
                                            @foreach($val->reagen as $valr)
                                              <option value="{{$valr->id}}">{!!$valr->reagen!!}</option>
                                            @endforeach
                                        @else
                                            @foreach($reagen as $reg)
                                              <option value="{{$reg->id}}">{!!$reg->reagen!!}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div id="row_alat{{$val->id}}" class="inpualat{{$val->id}}">
                                        <input id="inpualat{{$val->id}}" class="form-control" type="text" name="reagen_lain[]" value="{!!$val->reagen_lain!!}" />
                                    </div>
                                </td>
                                <td id="metodelain{{$val->id}}">
                                    <select id="kodemetode{{$val->id}}" class="form-control" name="kode[]" class="form-control" >
                                        <option value="{{$val->Kode}}">
                                        @if($val->metode_pemeriksaan != NULL){!!$val->metode_pemeriksaan!!}@else{{$val->Kode}}@endif</option>
                                        @foreach($val->metode as $val2)
                                          <option value="{{$val2->id}}">{!!$val2->metode_pemeriksaan!!}</option>
                                        @endforeach
                                    </select>
                                    <div id="row_dim{{$val->id}}" class="inputkode{{$val->id}}">
                                        <input id="inputkode{{$val->id}}" class="form-control" type="text" name="metode_lain[]" value="{!!$val->metode_lain!!}" />
                                    </div>
                                </td>
                                <td>
                                    @if($val->hasilPemeriksaan != NULL)
                                    <select class="form-control" name="hasil[]" class="form-control" >
                                        <option value="{{$val->Hasil}}">@if($val->hp != NULL){!!$val->hp!!}@else{{$val->Hasil}}@endif</option>
                                        @foreach($val->hasilPemeriksaan as $val2)
                                          <option value="{{$val2->id}}">{!!$val2->hp!!}</option>
                                        @endforeach
                                    </select>
                                    @else
                                        @if($val->id == 9)
                                        <input type="text" id="hasil{{$val->id}}" class="form-control hasil validate-hasil" name="hasil[]" value="{{$val->Hasil}}" min="0" max="9.999" 
                                         onKeyUp="if(this.value>9.999){this.value='';}else if(this.value<0){this.value='';}" required placeholder="Maksimal 9.999">
                                        @elseif($val->id == 10)
                                        <input type="text" id="hasil{{$val->id}}" class="form-control hasil validate-hasil" name="hasil[]" value="{{$val->Hasil}}" min="0" max="9.9" 
                                         onKeyUp="if(this.value>9.9){this.value='';}else if(this.value<0){this.value='';}" required placeholder="Maksimal 9.9">
                                        @endif
                                    @endif
                                </td>
                                <td>{{$val->catatan}}</td>
                            </tr>
<script>        
$(document).ready(function(){
    $("#hasil{{$val->id}}").change(function(){
    var value = $(this).val();
        if (value == '-' || value == 'NaN' || value == 'Tidak Mengerjakan'){
            console.log('bisa');
                $("#instrument{{$val->id}}").prop('required',false);
                $("#alat{{$val->id}}").prop('required',false);
                $("#kodemetode{{$val->id}}").prop('required',false);
        }else{
            if(value == ''){
                console.log('bisa');  
                $("#instrument{{$val->id}}").prop('required',false);
                $("#alat{{$val->id}}").prop('required',false);
                $("#kodemetode{{$val->id}}").prop('required',false);
            }else{
                console.log('gak bisa');
                $("#instrument{{$val->id}}").prop('required',true);
                $("#alat{{$val->id}}").prop('required',true);
                $("#kodemetode{{$val->id}}").prop('required',true);
            }
        }
    });
});
$(function() {
    $('#row_alat{{$val->id}}').hide(); 
    $('#alat{{$val->id}}').change(function(){
    var setan  = $("#alat{{$val->id}} option:selected").text();
        if(setan.match('Reagen Lain.*')) {
            $('#row_alat{{$val->id}}').show(); 
            $("#inpualat{{$val->id}}").prop('required',true);
        } else {
            $('#row_alat{{$val->id}}').hide(); 
            $("#inpualat{{$val->id}}").prop('required',false);
            $("#inpualat{{$val->id}}").val('');
        } 
    });
});

$(function() {
    @if($val->metode_lain != NULL)
    @else
    $('#row_dim{{$val->id}}').hide(); 
    @endif
    $('#kodemetode{{$val->id}}').change(function(){
    var setan  = $("#kodemetode{{$val->id}} option:selected").text();
        if(setan.match('lain-lain.*')) {
            $('#row_dim{{$val->id}}').show(); 
            $("#inputkode{{$val->id}}").prop('required',true);
        } else {
            $('#row_dim{{$val->id}}').hide(); 
            $("#inputkode{{$val->id}}").prop('required',false);
            $("#inputkode{{$val->id}}").val('');
        } 
    });
});
</script>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <label>Keterangan :</label>
                    <p> 
                        - Pilih tombol “SIMPAN” untuk menyimpan perubahan input hasil<br>
                        - Pilih tombol “KIRIM” untuk mengirim hasil final Saudara ke penyelenggara<br>
                        - Hasil yang sudah dikirim tidak bisa dirubah lagi. Kesalahan dalam penginputan hasil yang telah dikirim sepenuhnya tanggung jawab peserta. 
                    </p><br>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Catatan :</label>
                            <textarea class="form-control" name="catatan">{{$datas->catatan}}</textarea>
                        </div>
                        <div class="col-sm-6">
                            <label>Nama Penanggung jawab lab :</label>
                            <input type="text" class="form-control" name="penanggung_jawab" value="{{$datas->penanggung_jawab}}" required>
                        </div><br>
                    </div>
                    <small>Info : Mohon untuk mengecek kembali data yang sudah di input, karena data yang sudah di Kirim tidak bisa di edit kembali. </small><br>
                      {{ csrf_field() }}
                    <div class="clearfix"></div>
                    @if(Auth::user()->role == 3)
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px"> &nbsp;
                    <input type="submit" name="simpan" value="Kirim" class="btn btn-submit" style="margin-top: 20px">
                    @else
                    <input type="submit" name="simpan" value="Validasi" class="btn btn-submit" style="margin-top: 20px">
                    @endif
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Wajib Diisi');
    }

    @if($data != NULL)
    @foreach($data as $val)
        var textfield2 = $ ("#hasil{{$val->id}}").get(0);
        textfield2.setCustomValidity("");
        if (!textfield2.validity.valid) {
          textfield2.setCustomValidity('Tidak boleh kosong, atau gunakan strip "-"');
        }
    @endforeach
    @endif
});
$("form select").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Wajib Diisi');
    }
});


$(document).ready(function(){
    @foreach($data as $val)
    $('#hasil{{$val->id}}').blur(function(){
        var num = parseFloat($(this).val());
        @if($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal')
        console.log(num);
        var cleanNum = num.toFixed(1);
        @elseif($val->catatan == 'Hasil pemeriksaan menggunakan 3 (tiga) desimal')
        var cleanNum = num.toFixed(3);
        @endif
        if (cleanNum == 'NaN') {
            $(this).val('-');
        }else{
            $(this).val(cleanNum);
        }
    });
    @endforeach
});

$(document).ready(function(){
    $('#autoUpdate').fadeOut('slow');
    $('#checkbox1').change(function(){
    if(this.checked)
        $('#autoUpdate').fadeIn('slow');
    else
        $('#autoUpdate').fadeOut('slow');

    });
});

</script>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
</script>
@endsection
