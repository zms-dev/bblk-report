@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Data Peserta</div>
                <div class="panel-body">
                    <form class="form-horizontal" action="{{url('edit-data')}}" method="post" enctype="multipart/form-data"  >
                      <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Instansi</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control hurufdoang" id="nama_lab" name="nama_lab" placeholder="Nama Laboratorium" required value="{{$data->nama_lab}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="pemerintah" class="col-sm-3 control-label">Jenis Instansi</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="pemerintah" required readonly>
                                <option value="{{$data->Idbadan}}">{{$data->Badan}}</option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="alamat" class="col-sm-3 control-label">Alamat</label>
                        <div class="col-sm-9">
                          <textarea class="form-control" id="alamat" name="alamat" required>{{$data->alamat}}</textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Kode Pos</label>
                        <div class="col-sm-9">
                          <input type="text" name="kode_pos" class="form-control numberdoang" id="kode_pos" placeholder="Kode Pos" value="{{$data->kode_pos}}" maxlength="7" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="provinsi" class="col-sm-3 control-label">Provinsi</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="provinsi" id="provinsi" required>
                                <option value="{{$data->Idprovinsi}}">{{$data->Provinsi}}</option>
                                @foreach($provinsi as $val)
                                <option value="{{$val->id}}">{{$val->name}}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kota" class="col-sm-3 control-label">Kota / Kabupaten</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="kota" id="kota" required>
                                <option value="{{$data->Idkota}}">{{$data->Kota}}</option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kecamatan" class="col-sm-3 control-label">Kecamatan</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="kecamatan" id="kecamatan" required>
                                <option value="{{$data->Idkecamatan}}">{{$data->Kecamatan}}</option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="kelurahan" class="col-sm-3 control-label">Kelurahan / Desa</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="kelurahan" id="kelurahan" required>
                                <option value="{{$data->Idkelurahan}}">{{$data->Kelurahan}}</option>
                            </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="telp" class="col-sm-3 control-label">Telp / Fax</label>
                        <div class="col-sm-9">
                          <input type="text" name="telp" class="form-control numberdoang" id="telp" placeholder="Telp / Fax / Email" required value="{{$data->Telp}}" maxlength="15">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="penanggung" class="col-sm-3 control-label">Penanggung Jawab</label>
                        <div class="col-sm-9">
                          <input type="text" name="penanggung_jawab" class="form-control hurufdoangs" id="penanggung" placeholder="Penanggung Jawab" required value="{{$data->penanggung_jawab}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="personal" class="col-sm-3 control-label">Nama Contact Person</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control hurufdoangs" name="personal" id="personal" placeholder="Personal" required value="{{$data->personal}}">
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <label for="hp" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-9">
                          <input type="text" name="Email" class="form-control" data-validation="email" id="Email" placeholder="Email" required value="{{$data->Email}}">
                        </div>
                      </div> -->
                      <div class="form-group">
                        <label for="hp" class="col-sm-3 control-label">No. HP</label>
                        <div class="col-sm-9">
                          <input type="text" name="no_hp" class="form-control numberdoang" id="hp" placeholder="No. HP" required value="{{$data->no_hp}}" maxlength="15">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="wa" class="col-sm-3 control-label">No. Whatsapp</label>
                        <div class="col-sm-9">
                          <input type="text" name="no_wa" class="form-control numberdoang" id="wa" placeholder="No. Whatsapp" required value="{{$data->no_wa}}" maxlength="15">
                        </div>
                      </div>
                      <hr>
                      <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Peserta :</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control hurufdoang" id="name" name="name" required value="{{$data->name}}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Email :</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="email" data-validation="email" name="email" required value="{{$data->email}}" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="password" class="col-sm-3 control-label">Reset Password :</label>
                        <div class="col-sm-9">
                          <input type="password" class="form-control" id="password" name="password" minlength="7">
                        </div>
                      </div>
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                      <input type="submit" value="Simpan" class="btn btn-submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  
(function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  };
}(jQuery));
$(".numberdoang").inputFilter(function(value) {
  return /^-?\d*$/.test(value); });

$( ".hurufdoang" ).keypress(function(e) {
  var key = e.keyCode;
  console.log(key);
  if (key >= 65 && key <= 122 || key == 32) {
  }else{
    e.preventDefault();
  }
});

$( ".hurufdoangs" ).keypress(function(e) {
  var key = e.keyCode;
  console.log(key);
  if (key >= 48 && key <= 57) {
      e.preventDefault();
  }
});
$("#provinsi").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('kota');
  $("#kecamatan").val("");
  $("#kelurahan").val("");
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getkota').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});

$("#kota").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('kecamatan');
  $("#kelurahan").val("");
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getkecamatan').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});


$("#kecamatan").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('kelurahan');
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getkelurahan').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});

$.validate({
  modules : 'location, date, security, file',
  onModulesLoaded : function() {
    $('#country').suggestCountry();
  }
});
</script>
@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    });
    </script>
@endif
@endsection
