@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Data Perjanjian Kerjasama</div>

                <div class="panel-body">
                  <form class="form-horizontal" action="{{URL('kuitansi-pks')}}/1" method="get">
                    <div>
                        <label for="exampleInputEmail1">Tahun</label>
                        <div class="controls input-append date form_datetime" data-link-field="dtp_input1">
                            <input size="16" type="text" value="" readonly class="form-control" name="tahun">
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                    </div><br>
                    <button type="submit" name="proses" class="btn btn-info">Proses</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});
</script>
@endsection