@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Cek SPTJM</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="get">
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-link-field="dtp_input1">
                            @if($tahun != NULL)
                              <input size="16" type="text" value="{{$tahun}}" readonly class="form-control" name="tahun">
                            @else
                              <input size="16" type="text" value="2019" readonly class="form-control" name="tahun">
                            @endif
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div>
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select name="siklus" class="form-control" required>
                            <option value="{{$siklus}}">{{$siklus}}</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div><br>
                      <button type="submit" name="proses" class="btn btn-info">Proses</button>
                    </form>
                    <br>
                    <form class="form-horizontal" action="{{url('sptjm')}}" method="post" >
                    <label>SPTJM :</label>
                    <select name="pks" class="form-control" required>
                      <option></option>
                      @foreach($pks as $val)
                      <option value="{{$val->id}}">{{$val->pks}}</option>
                      @endforeach
                    </select>
                    <br>
                    <table id="example" class="display table table-bordered" style="width:100%">
                      <thead>
                        <tr>
                            <th>Tahun</th>
                            <th>Laboratorium</th>
                            <th>Siklus</th>
                            <th>Parameter</th>
                            <th>Tarif</th>
                            <th>File</th>
                            <th>Cek</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if($data != NULL)
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++;?>
                        <tr>
                          <td>{{date('Y', strtotime($val->created_at))}}<input type="hidden" name="email[]" value="{{$val->email}}" ></td>
                          <td>{{$val->nama_lab}}<input type="hidden" name="nama_lab[]" value="{{$val->nama_lab}}" ></td>
                          <td>{{$val->siklus}}</td>
                          <td>{!!str_replace('|','<br/><br/>',$val->parameter) !!}<input type="hidden" name="parameter[]" value="{{$val->parameter}}" ></td>
                          <input type="hidden" name="siklus[]" value="{{$val->siklus}}" >
                          <?php $imun1 = 0; $imun2 = 0; $imun12 = 0;  $bidang = explode("|",$val->bidang); ?>
                          @foreach($bidang as $valb)
                              <?php 
                                  if($valb == "61" || $valb == "71" || $valb == "81" || $valb == "91"){
                                      $imun1++;
                                  }elseif($valb == "62" || $valb == "72" || $valb == "82" || $valb == "92"){
                                      $imun2++;
                                  }elseif($valb == "612" || $valb == "712" || $valb == "812" || $valb == "912"){
                                      $imun12++;
                                  }
                              ?>
                          @endforeach
                            <?php
                              $imunt1 = 0;
                              $imunt2 = 0;
                              $imunt12 = 0;
                              if($imun1 == 4){
                                  $imunt1 = "200000";
                              }elseif($imun2 == 4){
                                  $imunt2 = "200000";
                              }elseif($imun12 == 4){
                                  $imunt12 = "400000";
                              }
                            ?>
                          <td>
                            {{number_format($val->jumlah_tarif - $imunt1 - $imunt2 - $imunt12)}}
                          </td>
                          <td style="text-align: center;">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg{{$no}}">View</button>
                            <div class="modal fade bs-example-modal-lg{{$no}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                              <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                  <img src="{{URL::asset('asset/file').'/'.$val->file}}" width="100%">
                                </div>
                              </div>
                            </div><br><br>
                            <a href="{{URL::asset('asset/file').'/'.$val->file}}" class="btn btn-primary">Download</a>
                          </td>
                          <td>
                            <center><input type="checkbox" name="bidang[]" value="{{$val->id}}"></center>
                          </td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                    <label>Catatan :</label>
                    <p>*) Beri tanda &#10003; pada bidang yang sudah dikirim</p>
                    <br>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Kirim</button>
                      <a href="kuitansi-pks"><button type="button" class="btn btn-info">Cetak Kwitansi</button></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection