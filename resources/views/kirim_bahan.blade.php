@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Kirim Bahan</div>

                <div class="panel-body">
                    <form class="form-horizontal" action="{{url('kirim-bahan')}}" method="post">
                      {{ csrf_field() }}
                      <!-- <input type="text" name="jenis_barang" class="form-control" placeholder="Jenis Barang"><br>
                      <input type="text" name="jumlah" class="form-control" placeholder="Jumlah"> -->
                      <br>
                      <p>* Harap ceklis hanya pada Laboratoruim yang sama</p>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Laboratoruim</th>
                            <th><center>Bidang</center></th>
                            <th><center>Parameter</center></th>
                            <th><center>Alamat</center></th>
                            <th>Cek<!-- <center><input type="checkbox" id="checkAll"></center> --></th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++;?>
                        <tr>
                          <td>{{$no}}</td>
                          <td>{{$val->nama_lab}}<input type="hidden" name="nama_lab[]" value="{{$val->nama_lab}}" ></td>
                          <td>{{$val->Bidang}}<input type="hidden" name="email[]" value="{{$val->email}}" ></td>
                          <td>{{$val->Parameter}}<input type="hidden" name="parameter[]" value="{{$val->Parameter}}" ></td>
                          <td>{{$val->Provinsi}} - {{$val->Kota}} - {{$val->Kecamatan}} - {{$val->Kelurahan}}</td>
                          <td>
                            <center><input type="checkbox" name="bidang[]" value="{{$val->id}}"></center>
                          </td>
                        </tr>
                        @endforeach
                        @endif
                    </table>
                    {{ $data->links() }}<br>
                    <label>Catatan :</label>
                    <p>*) Beri tanda &#10003; pada bidang yang sudah dikirim</p>
                    <br>
                      <button type="submit" class="btn btn-info">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <script type="text/javascript">
   $("#checkAll").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 });
</script> -->
@endsection