@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cetak Hasil</div>
                <div class="panel-body">
                    <label>
                        <!-- - Pengisian Hasil Online PNPME Siklus 1 diperpanjang sampai tanggal 24 Mei 2018 -->
                        Info : untuk mendownload hasil input pengujian silahkan melakukan :<br>
                    </label>
                        <ol>
                            <li>Input hasil pemeriksaan di sub menu Input Hasil.</li>
                            <li>Masuk ke sub menu Edit Hasil, silahkan di cek kembali data hasil pemeriksaan, kemudian klik tombol Kirim.</li>
                            <li>Klik Icon Printer untuk mendownload Hasil Pemeriksaan yang sudah di input di sub menu Cetak Hasil.</li>
                        </ol>
                    <hr>
                    <!-- <label>* Cetak Hasil Sementara</label><br>
                    <p>Merupakan data input bidang parameter yang masih tersimpan dan belum terkirim, bila dirasa sudah final silahkan klik tombol KIRIM dalam menu EDIT HASIL sebelum waktu pengisian siklus habis.</p>
                    <table class="table table-bordered">
                        <tr>
                            <th width="5%"><center>No</center></th>
                            <th><center>Bidang</center></th>
                            <th colspan="2" width="15%"><center>Siklus&nbsp;1</center></th>
                            <th colspan="2" width="15%"><center>Siklus&nbsp;2</center></th>
                        </tr>
                        @if(count($data2))
                        <?php $no = 0; ?>
                        @foreach($data2 as $val)
                        <?php $no++ ?>

                        @if($val->id_bidang > '5')
                            <tr>
                                <td><center>{{$no}}</center></td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                @if($val->bidang == '7')
                                    @if($val->siklus_1 == 'done')
                                        @if($val->pemeriksaan == 'done')
                                            @if($val->status_data1 == 1)
                                            <td>
                                                <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-print"></i><br>Anti TP</center></a>
                                            </td>
                                            @endif
                                        @else
                                            <td></td>
                                        @endif
                                        @if($val->rpr1 == 'done')
                                            @if($val->status_datarpr1 == 1)
                                            <td>
                                                <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/print')}}/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-print"></i><br>RPR</center></a>
                                            </td>
                                            @endif
                                        @else
                                            <td></td>
                                        @endif
                                    @else
                                    <td colspan="2"></td>                           
                                    @endif
                                    @if($val->siklus_2 == 'done')
                                        @if($val->pemeriksaan2 == 'done')
                                            @if($val->status_data2 == 1)
                                            <td>
                                                <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-print"></i><br>Anti TP</center></a>
                                            </td>
                                            @endif
                                        @else
                                            <td></td>
                                        @endif
                                        @if($val->rpr2 == 'done')
                                            @if($val->status_datarpr2 == 1)
                                            <td>
                                                <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/print')}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-print"></i><br>RPR</center></a>
                                            </td>
                                            @endif
                                        @else
                                            <td></td>
                                        @endif
                                    @else
                                    <td colspan="2"></td>                           
                                    @endif
                                @elseif($val->SubBidang == '13')
                                    <td colspan="2">
                                        @if($val->siklus_1 == 'done')
                                            @if($val->status_data1 == 1)
                                            <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=1&lembar=1" class="go-three"><center><i class="glyphicon glyphicon-print"></i>&nbsp;1</center></a>
                                            <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=1&lembar=2" class="go-three"><center><i class="glyphicon glyphicon-print"></i>&nbsp;2</center></a>
                                            <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=1&lembar=3" class="go-three"><center><i class="glyphicon glyphicon-print"></i>&nbsp;3</center></a>
                                            @endif
                                        @else
                                        @endif
                                    </td>
                                    <td colspan="2">
                                        @if($val->siklus_2 == 'done')
                                            @if($val->status_data2 == 1)
                                            <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=2" target="_blank"><center><i class="glyphicon glyphicon-print"></i></center></a>
                                            @endif
                                        @else
                                        @endif
                                    </td>
                                @else
                                    <td colspan="2">
                                        @if($val->siklus_1 == 'done')
                                            @if($val->status_data1 == 1)
                                            <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=1" target="_blank"><center><i class="glyphicon glyphicon-print"></i></center></a>
                                            @endif
                                        @else
                                        @endif
                                    </td>
                                    <td colspan="2">
                                        @if($val->siklus_2 == 'done')
                                            @if($val->status_data2 == 1)
                                            <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=2" target="_blank"><center><i class="glyphicon glyphicon-print"></i></center></a>
                                            @endif
                                        @else
                                        @endif
                                    </td>
                                @endif
                            </tr>
                        @elseif($val->id_bidang < '5')
                            <tr>
                                <td><center>{{$no}}</center></td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                @if($val->siklus_1 == 'done')
                                    @if($val->pemeriksaan == 'done')
                                        @if($val->status_data1 == 1)
                                            <td>
                                                <a href="{{URL('').$val->Link}}/print/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-print"></i><br>I-01</center></a>
                                            </td>
                                        @else
                                        <td></td>
                                        @endif
                                    @else
                                        <td></td>
                                    @endif
                                    @if($val->pemeriksaan2 == 'done')
                                        @if($val->status_data2 == 1)
                                            <td>
                                                <a href="{{URL('').$val->Link}}/print/{{$val->id}}?x=b&y=1"><center><i class="glyphicon glyphicon-print"></i><br>I-02</center></a>
                                            </td>
                                        @else
                                        <td></td>
                                        @endif
                                    @else
                                        <td></td>
                                    @endif
                                @else
                                <td colspan="2"></td>
                                @endif
                                @if($val->siklus_2 == 'done')
                                    @if($val->rpr1 == 'done')
                                        @if($val->status_datarpr1 == 1)
                                            <td>
                                                <a href="{{URL('').$val->Link}}/print/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-print"></i><br>II-01</center></a>
                                            </td>
                                        @else
                                        <td></td>
                                        @endif
                                    @else
                                        <td></td>
                                    @endif
                                    @if($val->rpr2 == 'done')
                                        @if($val->status_datarpr2 == 1)
                                            <td>
                                                <a href="{{URL('').$val->Link}}/print/{{$val->id}}?x=b&y=2"><center><i class="glyphicon glyphicon-print"></i><br>II-02</center></a>
                                            </td>
                                        @else
                                        <td></td>
                                        @endif
                                    @else
                                        <td></td>
                                    @endif
                                @else
                                <td colspan="2"></td>
                                @endif
                            </tr>
                        @else
                            <tr>
                                <td><center>{{$no}}</center></td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                <td colspan="2">
                                    @if(($val->pemeriksaan == 'done') && ($val->siklus_1 == 'done'))
                                        @if($val->status_data1 == 1)
                                            <a target="_blank" href="{{URL('').$val->Link}}/print/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-print"></i></center></a>
                                        @endif
                                    @else
                                    @endif
                                </td>
                                <td colspan="2">
                                    @if(($val->pemeriksaan == 'done') && ($val->siklus_2 == 'done'))
                                        @if($val->status_data2 == 1)
                                            <a target="_blank" href="{{URL('').$val->Link}}/print/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-print"></i></center></a>
                                        @endif
                                    @else
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table><br>
                    <label>* Cetak Hasil Final</label><br>
                    <p>Merupakan data input bidang parameter yang sudah terkirim, bersifat final dan sudah tidak bisa dilakukan proses edit.</p> -->
                    <table class="table table-bordered">
                        <tr>
                            <th width="5%"><center>No</center></th>
                            <th><center>Tahun</center></th>
                            <th><center>Bidang</center></th>
                            <th colspan="2" width="15%"><center>Siklus&nbsp;1</center></th>
                            <th colspan="2" width="15%"><center>Siklus&nbsp;2</center></th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->id_bidang > '5')
                            <tr>
                                <td><center>{{$no}}</center></td>
                                <td>{{date('Y', strtotime($val->created_at))}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                @if($val->bidang == '7')
                                    @if($val->siklus_1 == 'done')
                                        @if($val->pemeriksaan == 'done')
                                            @if($val->status_data1 == 2)
                                            <td colspan="2">
                                                <a target="_blank" href="{{URL('').$val->Link}}/print/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-print"></i><br>Anti TP</center></a>
                                            </td>
                                            @endif
                                        @else
                                            <td colspan="2"></td>
                                        @endif
                                        <!-- @if($val->rpr1 == 'done')
                                            @if($val->status_datarpr1 == 2)
                                            <td>
                                                <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/print')}}/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-print"></i><br>RPR</center></a>
                                            </td>
                                            @endif
                                        @else
                                            <td></td>
                                        @endif -->
                                    @else
                                    <td colspan="2"></td>                           
                                    @endif
                                    @if($val->siklus_2 == 'done')
                                        @if($val->pemeriksaan2 == 'done')
                                            @if($val->status_data2 == 2)
                                            <td  colspan="2">
                                                <a target="_blank" href="{{URL('').$val->Link}}/print/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-print"></i><br>Anti TP</center></a>
                                            </td>
                                            @endif
                                        @else
                                            <td  colspan="2"></td>
                                        @endif
                                        <!-- @if($val->rpr2 == 'done')
                                            @if($val->status_datarpr2 == 2)
                                            <td>
                                                <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/print')}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-print"></i><br>RPR</center></a>
                                            </td>
                                            @endif
                                        @else
                                            <td></td>
                                        @endif -->
                                    @else
                                    <td colspan="2"></td>                           
                                    @endif
                                @else
                                    <td colspan="2">
                                        @if($val->siklus_1 == 'done')
                                            @if($val->status_data1 == 2)
                                            <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=1" target="_blank"><center><i class="glyphicon glyphicon-print"></i></center></a>
                                            @endif
                                        @else
                                        @endif
                                    </td>
                                    <td colspan="2">
                                        @if($val->siklus_2 == 'done')
                                            @if($val->status_data2 == 2)
                                            <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=2" target="_blank"><center><i class="glyphicon glyphicon-print"></i></center></a>
                                            @endif
                                        @else
                                        @endif
                                    </td>
                                @endif
                            </tr>
                        @elseif($val->id_bidang < '5')
                            <tr>
                                <td><center>{{$no}}</center></td>
                                <td>{{date('Y', strtotime($val->created_at))}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                
                                @if($val->siklus_1 == 'done')
                                    @if($val->pemeriksaan == 'done')
                                        @if($val->status_data1 == 2)
                                            <td>
                                                <a target="_blank" href="{{URL('').$val->Link}}/print/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-print"></i><br>I-A</center></a>
                                            </td>
                                        @else
                                        <td></td>
                                        @endif
                                    @else
                                        <td></td>
                                    @endif
                                    @if($val->pemeriksaan2 == 'done')
                                        @if($val->status_data2 == 2)
                                            <td>
                                                <a target="_blank" href="{{URL('').$val->Link}}/print/{{$val->id}}?x=b&y=1"><center><i class="glyphicon glyphicon-print"></i><br>I-B</center></a>
                                            </td>
                                        @else
                                        <td></td>
                                        @endif
                                    @else
                                        <td></td>
                                    @endif
                                @else
                                <td colspan="2"></td>
                                @endif
                                @if($val->siklus_2 == 'done')
                                    @if($val->rpr1 == 'done')
                                        @if($val->status_datarpr1 == 2)
                                            <td>
                                                <a target="_blank" href="{{URL('').$val->Link}}/print/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-print"></i><br>II-A</center></a>
                                            </td>
                                        @else
                                        <td></td>
                                        @endif
                                    @else
                                        <td></td>
                                    @endif
                                    @if($val->rpr2 == 'done')
                                        @if($val->status_datarpr2 == 2)
                                            <td>
                                                <a target="_blank" href="{{URL('').$val->Link}}/print/{{$val->id}}?x=b&y=2"><center><i class="glyphicon glyphicon-print"></i><br>II-B</center></a>
                                            </td>
                                        @else
                                        <td></td>
                                        @endif
                                    @else
                                        <td></td>
                                    @endif
                                @else
                                <td colspan="2"></td>
                                @endif
                            </tr>
                        @else
                            <tr>
                                <td><center>{{$no}}</center></td>
                                <td>{{date('Y', strtotime($val->created_at))}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                <td colspan="2">
                                    @if(($val->pemeriksaan == 'done') && ($val->siklus_1 == 'done'))
                                        @if($val->status_data1 == 2)
                                            <a target="_blank" target="_blank" href="{{URL('').$val->Link}}/print/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-print"></i></center></a>
                                        @endif
                                    @else
                                    @endif
                                </td>
                                <td colspan="2">
                                    @if(($val->pemeriksaan2 == 'done') && ($val->siklus_2 == 'done'))
                                        @if($val->status_data2 == 2)
                                            <a target="_blank" href="{{URL('').$val->Link}}/print/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-print"></i></center></a>
                                        @endif
                                    @else
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6">Data Kosong</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection