@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Formulir Pendaftaran</div>

                <div class="panel-body">
                    @if(Session::has('message'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('message') }}
                      </div>
                    @endif
                    <form class="form-horizontal" id="myform" action="{{url('daftar_old')}}" method="post" enctype="multipart/form-data"  >
                      <table class="table table-bordered">
                        <tbody class="body-data">
                          <tr>
                            <th>No</th>
                            <th>Parameter Terdaftar</th>
                            <th>Status</th>
                          </tr>
                          @if(count($daftar))
                          <?php $no = 0; ?>
                          @foreach($daftar as $val)
                          <?php $no++ ?>
                          <tr>
                            <td>{{$no}}</td>
                            <td>{{$val->bidang}}<br>{{$val->parameter}}</td>
                            <td>
                              @if($val->status == 1)
                                <font style="color: red">Menunggu verifikasi Pembayaran dari Keuangan BBLK Jakarta</font>
                              @elseif($val->status == 2)
                                <font style="color: green">Menunggu bahan Uji untuk di kirim</font>
                              @else
                                <font style="color: blue">Input hasil</font>
                              @endif
                            </td>
                          </tr>
                          @endforeach
                          @endif
                        </tbody>
                      </table>
                      <div class="form-group">
                        <label for="siklus" class="col-sm-3 control-label">Siklus</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="siklus" id="siklus" required>
                                <option></option>
                                @if(count($siklus))
                                  @foreach($siklus as $val)
                                    <?php
                                      $date = date('Y-m');
                                      $siklus1 = date('Y').'-01';
                                      $siklus2 = date('Y').'-06';
                                    ?>
                                    @if($date >= $siklus1 && $date < $siklus2)
                                    <option class="1" value="1">Siklus 1</option>
                                    <option class="12" value="12">Siklus 1 & 2</option>
                                    @else
                                    <option class="2" value="2">Siklus 2</option>
                                    @endif
                                  @endforeach
                                    <script type="text/javascript">
                                  @foreach($siklus as $val)
                                      $('option.{{$val->siklus}}').remove();
                                  @endforeach
                                  </script>
                                @else
                                  <?php
                                    $date = date('Y-m');
                                    $siklus1 = date('Y').'-01';
                                    $siklus2 = date('Y').'-06';
                                  ?>
                                  @if($date >= $siklus1 && $date < $siklus2)
                                  <option class="1" value="1">Siklus 1</option>
                                  <option class="12" value="12">Siklus 1 & 2</option>
                                  @else
                                  <option class="2" value="2">Siklus 2</option>
                                  @endif
                                @endif
                            </select>
                        </div>
                      </div>
                    <div class="table-responsive">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                              <th>No</th>
                              <th colspan="2" style="text-align: center;">Bidang Pengujian Yang Dipilih</th>
                              <th>Tarif (Rp)</th>
                          </tr>
                        </thead>
                        <tbody class="body-siklus">

                        </tbody>
                          <!-- <tr id="datasiklus">
                            <td id="nosiklus"></td>
                            <td id="paramsiklus"></td>
                            <td id="bidangsiklus"></td>
                            <td id="tarifsiklus"></td>
                          </tr> -->
                      </table>
                    </div>
                    <label>Total Pembayaran:</label><br>
                    <div class="total"></div>
                    <label>Catatan :</label>
                    <p>*) Pilih metode pembayaran anda dengan benar.</p>
                    <label for="telp" class="control-label">Metode Pembayaran</label>
                    <select id="pembayaran" class="form-control" name="pembayaran">
                      <option></option>
                      <option value="transfer">Transfer</option>
                      <option value="sptjm">SPTJM</option>
                    </select>
                    <div id="transfer" class="pembayaran" style="display:none">
                      <label for="telp" class="control-label">File</label><small> *Bukti Pembayaran Format (JPG, atau PDF) Max (5 MB)</small>
                      <input type="file" class="myFile" placeholder="Nama Laboratorium" name="file">
                    </div>
                    <div id="sptjm" class="pembayaran" style="display:none">
                      <label for="telp" class="control-label">Text</label>
                      <textarea class="form-control" name="sptjm" style="display:none;">-</textarea>
                      <label for="telp" class="control-label">File</label><small> *Bukti Pembayaran Format (JPG, atau PDF) Max (5 MB)</small>
                      <input type="file" class="myFile" placeholder="Nama Laboratorium" name="file1">
                    </div>
                    <br>
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="tombolna">
                    </div>
                    @foreach($bidang as $val)
                    <input type="hidden" name="alias[]" value="{{$val->alias}}">
                    @endforeach
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"> 

$('.myFile').bind('change', function() {
  if(this.files[0].size >= 5120000){
      alert('File bukti pembayaran melebihi 5Mb!');
      $(".tombolna").html("");
  }else{
      $(".tombolna").html("");
        var exportna = "<input type=\"submit\" id=\"Daftar\" name=\"simpan\" value=\"Kirim\" class=\"btn btn-submit\">";
        $(".tombolna").append(exportna);
  }
});

$(function() {
    $('#pembayaran').change(function(){
        $('.pembayaran').hide();
        $('#' + $(this).val()).show();
    });
});

function formatNumber (num, currency) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}



$("#siklus").change(function(){
  var val = $(this).val(), i, no = 0;
  var y = document.getElementById('datasiklus')
  $(".body-siklus").html("<tr><td colspan='4'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('datasiklus').'/'}}"+val,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus").html("");
      if(addr.Hasil != undefined){
        var no = 1;
        $.each(addr.Hasil,function(e,item){
          var tarif2 = formatNumber(item.tarif / 2);
          var tarif1 = formatNumber(item.tarif);
          var tarif3 = formatNumber(item.tarif * 2);
          if (val != '12') {
            if (item.siklus != '12') {
                if (val == '1') {
                    if (item.kuota_1 > '0') {
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                        $(".body-siklus").append(html);  
                    }else{
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                        $(".body-siklus").append(html);  
                    }
                }else{
                    if (item.kuota_2 > '0') {
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                        $(".body-siklus").append(html);  
                    }else{
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif2+"</td>";
                        $(".body-siklus").append(html);  
                    }
                }
            }else{
                if (val == '1') {
                    if (item.kuota_1 > '0') {
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                        $(".body-siklus").append(html);
                    }else{
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                        $(".body-siklus").append(html);
                    }
                }else{
                    if (item.kuota_2 > '0') {
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                        $(".body-siklus").append(html);
                    }else{
                        var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                        $(".body-siklus").append(html);
                    }
                }
            }
          }else{
            if (item.siklus != '12') {
                if (item.kuota_1 > '0') {
                    var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                    $(".body-siklus").append(html); 
                }else{
                    var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif1+"</td>";
                    $(".body-siklus").append(html); 
                } 
            }else{
                if (item.kuota_1 > '0') {
                    var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif3+"</td>";
                    $(".body-siklus").append(html);
                }else{
                    var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\"><input type='checkbox' disabled name='bidang[]' class='checkbidang "+item.id_bidang+"' value="+item.id+"></td><td class='tarifsiklus' style='text-align: right'>"+tarif3+"</td>";
                    $(".body-siklus").append(html);
                }
            }
          }
          no++;
        })
        $(function() {
          $("input.10").click(enable_cb);
        });
        $(function() {
          $("input.6").click(enable_na1);
          $("input.7").click(enable_na2);
          $("input.8").click(enable_na3);
          $("input.9").click(enable_na4);
        });

        function enable_cb() {
          if (this.checked) {
            $("input.6").prop("checked", false);
            $("input.6").attr("disabled", true);
            $("input.7").prop("checked", false);
            $("input.7").attr("disabled", true);
            $("input.8").prop("checked", false);
            $("input.8").attr("disabled", true);
            $("input.9").prop("checked", false);
            $("input.9").attr("disabled", true);
          } else {
            $("input.6").removeAttr("disabled");
            $("input.7").removeAttr("disabled");
            $("input.8").removeAttr("disabled");
            $("input.9").removeAttr("disabled");
          }
        }

        $("input[type=checkbox]").click(function(){
          var waw1 = $("input.6"), waw2 = $("input.7"), waw3 = $("input.8"), waw4 = $("input.9");
          if (waw1.prop('checked') || waw2.prop('checked') || waw3.prop('checked') || waw4.prop('checked')) {
            $("input.10").attr("disabled", true);
          } else {
            $("input.10").removeAttr("disabled");
          }
          if (waw1.prop('checked') && waw2.prop('checked') && waw3.prop('checked') && waw4.prop('checked')) {
            $("input.6").prop('checked',false);
            $("input.6").attr('disabled',true);
            $("input.7").prop('checked',false);
            $("input.7").attr('disabled',true);
            $("input.8").prop('checked',false);
            $("input.8").attr('disabled',true);
            $("input.9").prop('checked',false);
            $("input.9").attr('disabled',true);
            $("input.10").prop('checked',true);
          }
          var cekbok  = $("input[type=checkbox]"),
              total   = 0;
          cekbok.each(function(){
            var e = $(this).parents('tr').find('.tarifsiklus').html().replace(/,/g,'');
            if($(this).prop('checked') == true){
              total = total + parseInt(e);
            }
          });
          $(".total").html(formatNumber(total));
        });
      }
      return false;
    }
  });
});

$('#myform').submit(function() {
  if($('.checkbidang:checkbox:checked').length == 0){
    alert('Pilih minimal 1 Bidang Pengujian');
    return false;
  }
  var $btn = $(this).button('loading')
});

var elements = document.getElementsByTagName("INPUT");
var select = document.getElementsByTagName("select");
var textarea = document.getElementsByTagName("textarea");
for (var i = 0; i < elements.length; i++) {
    elements[i].oninvalid = function(e) {
        e.target.setCustomValidity("");
        if (!e.target.validity.valid) {
            e.target.setCustomValidity("Tolong isi input yang kosong");
        }
    };
    elements[i].oninput = function(e) {
        e.target.setCustomValidity("");
    };
}
for (var i = 0; i < select.length; i++) {
    select[i].oninvalid = function(e) {
        e.target.setCustomValidity("");
        if (!e.target.validity.valid) {
            e.target.setCustomValidity("Tolong isi input yang kosong");
        }
    };
    select[i].oninput = function(e) {
        e.target.setCustomValidity("");
    };
}
for (var i = 0; i < textarea.length; i++) {
    textarea[i].oninvalid = function(e) {
        e.target.setCustomValidity("");
        if (!e.target.validity.valid) {
            e.target.setCustomValidity("Tolong isi input yang kosong");
        }
    };
    textarea[i].oninput = function(e) {
        e.target.setCustomValidity("");
    };
}
</script>
@endsection