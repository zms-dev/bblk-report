@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PROGRAM  NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI SIKLUS 1 TAHUN 2018<br> HASIL PEMERIKSAAN RPR <input type="hidden" name="type" value="{{$type}}"></label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$perusahaan}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <label>2. BAHAN UJI :</label>
                    <table class="table-bordered table">
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_diterima" autocomplete="off">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </td>
                                <td>Diperiksa tanggal :</td>
                                <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_diperiksa" autocomplete="off">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </td>
                            </tr>
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="6" width="20%"> Kondisi bahan Uji saat diterima</td>
                                <td>Kode Bahan Uji</td>
                                <td>Baik/Jernih </td>
                                <td>Keruh </td>
                                <td>Lain-lain</td>
                            </tr>
                            <tr>
                                <td><input type="text" name="no_tabung[]" id="tabung11" class="form-control" required readonly value="SI001"></td>
                                <td><input type="checkbox" class="jenis1" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input type="checkbox" class="jenis1" name="jenis[]" value="keruh"></td>
                                <td>
                                    <input type="checkbox" class="jenis1" name="jenis[]" value="lain-lain" class="lain21">
                                    <div id="row_dim1">
                                        <input type="text" name="lain[]" class="laintext1 form-control">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="text" name="no_tabung[]" id="tabung21" class="form-control" required readonly value="SI002"></td>
                                <td><input type="checkbox" class="jenis2" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input type="checkbox" class="jenis2" name="jenis[]" value="keruh"></td>
                                <td>
                                    <input type="checkbox" class="jenis2" name="jenis[]" value="lain-lain" class="lain22">
                                    <div id="row_dim2">
                                        <input type="text" name="lain[]" class="laintext2 form-control">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="text" name="no_tabung[]" id="tabung31" class="form-control" required readonly value="SI003"></td>
                                <td><input type="checkbox" class="jenis3" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input type="checkbox" class="jenis3" name="jenis[]" value="keruh"></td>
                                <td>
                                    <input type="checkbox" class="jenis3" name="jenis[]" value="lain-lain" class="lain23">
                                    <div id="row_dim3">
                                        <input type="text" name="lain[]" class="laintext3 form-control">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="text" name="no_tabung[]" id="tabung41" class="form-control" required readonly value="SI004"></td>
                                <td><input type="checkbox" class="jenis4" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input type="checkbox" class="jenis4" name="jenis[]" value="keruh"></td>
                                <td>
                                    <input type="checkbox" class="jenis4" name="jenis[]" value="lain-lain" class="lain24">
                                    <div id="row_dim4">
                                        <input type="text" name="lain[]" class="laintext4 form-control">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="text" name="no_tabung[]" id="tabung51" class="form-control" required readonly value="SI005"></td>
                                <td><input type="checkbox" class="jenis5" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input type="checkbox" class="jenis5" name="jenis[]" value="keruh"></td>
                                <td>
                                    <input type="checkbox" class="jenis5" name="jenis[]" value="lain-lain" class="lain25">
                                    <div id="row_dim5">
                                        <input type="text" name="lain[]" class="laintext5 form-control">
                                    </div>
                                </td>
                            </tr>
                            @for ($i = 0; $i < 6; $i++)
                            <script type="text/javascript">
                            $(function() {
                              enable_cb{{$i}}();
                              $(".lain2{{$i}}").click(enable_cb{{$i}});
                            });
                            function enable_cb{{$i}}() {
                              if (this.checked) {
                                $('#row_dim{{$i}}').show(); 
                              } else {
                                $('#row_dim{{$i}}').hide(); 
                              }
                            }
                            </script>
                            @endfor
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Metode Pemeriksaan </td>
                            <td><input class="metode_pemeriksaan" type="checkbox" name="metode[]" value="flokulasi" checked> Flokulasi <input class="metode_pemeriksaan" type="checkbox" name="metode[]" value="lain-lain"> Lain-lain</td>
                        </tr>
                        <tr>
                            <td>Nama Reagen</td>
                            <td>
                                <select id="alat1" class="form-control" name="nama_reagen[]" class="form-control">
                                    <option selected></option>
                                    @foreach($reagen as $val)
                                      <option value="{{$val->id}}">{{$val->reagen}}</option>
                                    @endforeach
                                </select>
                                <div id="row_alat1" class="inpualat1">
                                    <input id="inpualat1" class="form-control" type="text" name="reagen_lain[]" />
                                </div>
                            </td>
                        </tr>
<script>                            
$(function() {
    $('#row_alat1').hide(); 
    $('#alat1').change(function(){
    var setan  = $("#alat1 option:selected").text();
        if(setan.match('Lain - lain.*')) {
            $('#row_alat1').show(); 
        } else {
            $('#row_alat1').hide(); 
        } 
    });
});
</script>                        
                        <tr>
                            <td>Nama Produsen</td>
                            <td><input type="text" name="nama_produsen[]" class="form-control" required></td>
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            <td><input type="text" name="nomor_lot[]" class="form-control" required></td>
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            <td>
                              <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" value="" readonly class="form-control form_datemo" name="tgl_kadaluarsa[]">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                            </td>
                        </tr>
                    </table>
                    <label>4. HASIL PEMERIKSAAN </label>
                    <table class="table table-bordered">
                        <tr>
                            <th>Kode Bahan Uji</th>
                            <td>Interpretasi hasil </td>
                            <td>Titer</td>
                        </tr>
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung12" class="form-control" required value="SI001" readonly></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non1">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                            <td>
                                <input list="titer1" name="titer[]" class="form-control" id="tit1"/>
                                <datalist id="titer1">
                                    <option value="1:1"></option><option value="1:2"></option><option value="1:4"></option><option value="1:8"></option><option value="1:16"></option><option value="1:32"></option><option value="1:64"></option><option value="1:128"></option><option value="1:256"></option><option value="1:512"></option>
                                </datalist>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung22" class="form-control" required value="SI002" readonly></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non2">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                            <td>
                                <input list="titer2" name="titer[]" class="form-control" id="tit2"/>
                                <datalist id="titer2">
                                    <option value="1:1"></option><option value="1:2"></option><option value="1:4"></option><option value="1:8"></option><option value="1:16"></option><option value="1:32"></option><option value="1:64"></option><option value="1:128"></option><option value="1:256"></option><option value="1:512"></option>
                                </datalist>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung32" class="form-control" required value="SI003" readonly></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non3">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                            <td>
                                <input list="titer3" name="titer[]" class="form-control" id="tit3" />
                                <datalist id="titer3">
                                    <option value="1:1"></option><option value="1:2"></option><option value="1:4"></option><option value="1:8"></option><option value="1:16"></option><option value="1:32"></option><option value="1:64"></option><option value="1:128"></option><option value="1:256"></option><option value="1:512"></option>
                                </datalist>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung42" class="form-control" required value="SI004" readonly></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non4">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                            <td>
                                <input list="titer4" name="titer[]" class="form-control" id="tit4" />
                                <datalist id="titer4">
                                    <option value="1:1"></option><option value="1:2"></option><option value="1:4"></option><option value="1:8"></option><option value="1:16"></option><option value="1:32"></option><option value="1:64"></option><option value="1:128"></option><option value="1:256"></option><option value="1:512"></option>
                                </datalist>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung52" class="form-control" required value="SI005" readonly></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                            <td>
                                <input list="titer5" name="titer[]" class="form-control" id="tit5" />
                                <datalist id="titer5">
                                    <option value="1:1"></option><option value="1:2"></option><option value="1:4"></option><option value="1:8"></option><option value="1:16"></option><option value="1:32"></option><option value="1:64"></option><option value="1:128"></option><option value="1:256"></option><option value="1:512"></option>
                                </datalist>
                                </select>
                            </td>
                        </tr>
                    </table>             
@for ($i = 0; $i < 6; $i++)
<script type="text/javascript">                      
$(function() {
    $('.non{{$i}}').change(function(){
    var setan  = $(".non{{$i}} option:selected").text();
        if(setan.match('Non Reaktif.*')) {
            $('#tit{{$i}}').prop('readonly', true);
        } else {
            $('#tit{{$i}}').prop('readonly', false);
        } 
    });
});
</script>
@endfor
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Catatan :</label>
                            <textarea name="keterangan" class="form-control">-</textarea>
                        </div>
                        <div class="col-sm-6">
                            <label>Petugas yang melakukan pemeriksaan :</label>
                            <input type="text" name="petugas_pemeriksaan" class="form-control" required>
                        </div>
                    </div>
                    <small>Info : Untuk mencetak hasil input pengujian PNPME, silahkan mengirimkan hasil pengujian di bagian Edit Hasil dengan mengklik tombol <b>Kirim</b> di Sub Menu Edit Hasil.</small><br>

                      {{ csrf_field() }}
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Tidak boleh kosong');
    }
});
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;


$('.decimal').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.\<\>]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});
</script>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});

$(".form_datemo").datetimepicker({
    todayBtn: true,
    autoclose: true,
    format: "yyyy-mm-dd",
    minView: 3
});
$(document).ready(function(){
    $("#tabung11").change(function(){
        $val = $(this).val();
        $('#tabung12').val($val);
    });
    $("#tabung21").change(function(){
        $val1 = $(this).val();
        $('#tabung22').val($val1);
    });
    $("#tabung31").change(function(){
        $val2 = $(this).val();
        $('#tabung32').val($val2);
    });
    $("#tabung41").change(function(){
        $val1 = $(this).val();
        $('#tabung42').val($val1);
    });
    $("#tabung51").change(function(){
        $val2 = $(this).val();
        $('#tabung52').val($val2);
    });
});

$("input:checkbox").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input:checkbox[class='" + $box.attr("class") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});
</script>
@endsection
