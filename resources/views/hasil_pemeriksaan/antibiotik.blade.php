@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label> PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL INDENTIFIKASI BAKTERI DAN UJI KEPEKAAN ANTIBIOTIK<br>
                                    SIKLUS {{$siklus}} TAHUN {{$date}}
                    </label></center><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Lembar </label>
                        <div class="col-sm-9">
                            @if(count($lembar))
                            @foreach($lembar as $val)
                            <input type="text" name="lembar" class="form-control" value="{{$val->lembar + 1}}" readonly>
                            @break
                            @endforeach
                            @else
                            <input type="text" name="lembar" class="form-control" value="1" readonly>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$perusahaan}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Instansi </label>
                        <div class="col-sm-9">
                            @if(count($lembar))
                            @foreach($lembar as $val)
                            <input type="text" class="form-control" name="nama_instansi" value="{{$val->nama_instansi}}" placeholder="Nama Instansi">
                            @break
                            @endforeach
                            @else
                            <input type="text" class="form-control" name="nama_instansi" value="" placeholder="Nama Instansi">
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Diterima Tanggal </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                            @if(count($lembar))
                            @foreach($lembar as $val)
                              <input size="16" type="text" value="{{$val->tgl_diterima}}" readonly class="form-control form_datetime" name="tgl_diterima">
                            @break
                            @endforeach
                            @else
                              <input size="16" type="text" value="" readonly class="form-control form_datetime" name="tgl_diterima">
                            @endif
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kondisi Bahan </label>
                        <div class="col-sm-9">  
                          <input type="radio" required name="kondisi" value="baik" {{ (old('kondisi') == 'baik') ? 'checked' : '' }}> Baik
                          <input type="checkbox" id="checkbox1"/> Kurang Baik
                          <div id="autoUpdate" class="autoUpdate">
                              <input type="radio" name="kondisi" value="rusak" {{ (old('kondisi') == 'rusak') ? 'checked' : '' }}> Rusak
                              <input type="radio" name="kondisi" value="cair" {{ (old('kondisi') == 'cair') ? 'checked' : '' }}> Cair
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Alamat </label>
                        <div class="col-sm-9">
                            @if(count($lembar))
                            @foreach($lembar as $val)
                            <textarea class="form-control" name="alamat">{{$val->alamat}}</textarea>
                            @break
                            @endforeach
                            @else
                            <textarea class="form-control" name="alamat"></textarea>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Pemeriksa </label>
                        <div class="col-sm-9">
                            @if(count($lembar))
                            @foreach($lembar as $val)
                            <input type="text" class="form-control" name="nama_pemeriksa" value="{{$val->nama_pemeriksa}}" placeholder="Nama Pemeriksaan">
                            @break
                            @endforeach
                            @else
                            <input type="text" class="form-control" name="nama_pemeriksa" value="" placeholder="Nama Pemeriksaan">
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nomor HP Pemeriksa </label>
                        <div class="col-sm-9">
                            @if(count($lembar))
                            @foreach($lembar as $val)
                            <input type="text" class="form-control" name="no_hp" value="{{$val->no_hp}}" placeholder="No HP Pemeriksa">
                            @break
                            @endforeach
                            @else
                            <input type="text" class="form-control" name="no_hp" value="" placeholder="No HP Pemeriksa">
                            @endif
                        </div>
                    </div>
                    <p> 
                    1. Masukan hasil mikroskopis, dan identifikasi untuk spesimen pada tempat yang telah disediakan dibawah ini. Hasil identifikasi ditulis pada lembar hasil yang sudah disediakan <br>
                    2. Tuliskan jumlah item dan catatan bila ada kesalahan penulisan
                    </p><br>
                    <table class="table table-bordered">
                        <tr>
                            <th>Kode Bahan :</th>
                            <td>
                                @if(count($lembar))
                                @foreach($lembar as $val)
                                <input type="text" class="form-control" value="{{substr($perusahaan,0, -5)}}{{$val->lembar + 1}}/{{$siklus}}{{substr($perusahaan, 11)}}" readonly>
                                <input type="hidden" name="kode_bahan" class="form-control" value="{{$val->lembar + 1}}" readonly>
                                @break
                                @endforeach
                                @else
                                <input type="text" class="form-control" value="{{substr($perusahaan,0, -5)}}1/{{$siklus}}{{substr($perusahaan, 11)}}" readonly>
                                <input type="hidden" name="kode_bahan" class="form-control" value="1" readonly>
                                @endif
                            </td>
                            <th>Jenis Bahan :</th>
                            <td><input type="text" name="jenis_bahan" class="form-control"></td>
                        </tr>
                        <tr>
                            <th>Siklus :</th>
                            <td><input type="text" name="siklus" class="form-control" value="{{$siklus}}" readonly></td>
                            <th>Tanggal Pelaksanaan :</th>
                            <td>
                              <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" value="" readonly class="form-control form_datetime" name="tgl_pelaksanaan">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2"><center>Metode Identifikasi Bakteri</center></th>
                        </tr>
                        <tr>
                            <td>
                                <p><select name="metode_konvensional"><option value=""></option><option value="V">V</option></select> Konvensional</p>
                                <p><select name="metode_otomatis"><option value=""></option><option value="V">V</option></select> Otomatis (sebutkan)</p>
                                <select name="metode_lainnya"><option value=""></option><option value="V">V</option></select> Lainnya (sebutkan)
                            </td>
                            <td>
                                <input type="text" name="metodetext1" class="form-control">
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <th><center>Media yang digunakan</center></th>
                            <th><center>Buatan Sendiri</center></th>
                            <th><center>Media Komersial (Sebutkan)</center></th>
                        </tr>
                        <tr>
                            <td>
                                <select name="media_digunakan[]" class="selectpicker form-control" multiple>
                                    <option value="" selected></option>
                                    <option>Blood agar</option>
                                    <option>Mac Conkey</option>
                                    <option>CLED</option>
                                    <option>Chromogenic Agar</option>
                                    <option>Lainnya</option>
                                </select>
                            </td>
                            <td>
                                <input type="checkbox" name="media_buatan" class="1 form-control" value="a" checked style="display: none;">
                                <input type="checkbox" name="media_buatan" class="1 form-control" value="v">
                            </td>
                            <td>
                                <input type="text" name="media_komersial" class="form-control">
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <th width="30%"><center>Hasil Kultur</center></th>
                            <th><center>Pilih Salah satu</center></th>
                        </tr>
                        <tr>
                            <td>Tidak terdapat pertumbuhan bakteri patogen</td>
                            <td>
                                <input type="radio" name="hasil_kultur" value="tidak" required >
                            </td>
                        </tr>
                        <tr>
                            <td>Terdapat pertumbuhan bakteri patogen</td>
                            <td>
                                <input type="radio" name="hasil_kultur" value="ada" >
                            </td>
                        </tr>
                        <tr>
                            <td><b>Spesies</b></td>
                            <td>
                                <textarea name="spesies_kultur" id="editor1" rows="10" cols="80"></textarea>
                            </td>
                        </tr>
                    </table>

                    <h5>Pewarnaan</h5>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2">Pewarnaan Gram</th>
                            <th>Gram Positif</th>
                            <th>Gram Negatif</th>
                            <th>Yeast</th>
                        </tr>
                        <tr>
                            <td>
                                <select name="pewarnaan_gram_p">
                                    <option value=""></option>
                                    <option value="Coccus">Coccus</option>
                                    <option value="Batang">Batang</option>
                                    <option value="Cocobacil">Cocobacil</option>
                                </select>
                            </td>
                            <td>
                                <select name="pewarnaan_gram_n">
                                    <option value=""></option>
                                    <option value="Coccus">Coccus</option>
                                    <option value="Batang">Batang</option>
                                    <option value="Cocobacil">Cocobacil</option>
                                </select>
                            </td>
                            <td>
                                <select name="pewarnaan_gram_y">
                                    <option value=""></option>
                                    <option value="Yeast">Yeast</option>
                                </select>
                            </td>
                        </tr>
                    </table>

                    <h5><b>UJI BIOKOMIA KONVENSIONAL UNTUK GOLONGAN GRAM NEGATIF BATANG</b></h5>
                    <table class="table table-bordered">
                        <tr>
                            <th>Kebutuhan Oksigen</th>
                            <th colspan="4">
                                <select class="form-control" name="kebutuhan_oksidasi">
                                    <option value=""></option>
                                    <option value="Strict aerob">Strict aerob</option>
                                    <option value="Fakultatif">Fakultatif</option>
                                    <option value="Mikroaerofilik">Mikroaerofilik</option>
                                    <option value="Capnophilic">Capnophilic</option>
                                    <option value="Aerotolerant">Aerotolerant</option>
                                </select>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="6">Fermentasi Karbohidrat dan uji biokimia lainnya (pilih salah satu)</th>
                        </tr>
                        <tr>
                        <?php $i = 1;?>
                        @foreach($fermentasinegatif as $val)
                            <td>{{$val->fermentasi}}<input type="hidden" name="id_fermentasi[]" value="{{$val->id}}"></td>
                            <td colspan="2">
                                <select class="form-control" name="fermentasinegatif[]">
                                    <option value="Tanpa Test">Tanpa Test</option>
                                    <option value="Positif">Positif</option>
                                    <option value="Negatif">Negatif</option>
                                </select>
                            </td>
                        <?php 
                          if($i == 2){
                            $i =1;
                            echo "</tr><tr>";
                          }else{
                            $i++;
                          }
                        ?>
                        @endforeach
                            <td>Lain-lain</td>
                            <td colspan="2"><input type="text" name="fermentasilainn" class="form-control"></td>
                        </tr>
                    </table>

                    <h5><b>UJI BIOKIMIA KONVENSIONAL UNTUK GOLONGAN GRAM POSITIF COCCUS</b></h5>
                    <table class="table table-bordered">
                        <tr>
                            <th>Hemolisa</th>
                            <th colspan="4">
                                <select class="form-control" name="hemolisa">
                                    <option value=""></option>
                                    <option value="Alfa Hemolisa">Alfa Hemolisa</option>
                                    <option value="Beta Hemolisa">Beta Hemolisa</option>
                                    <option value="Gamma Hemolisa">Gamma Hemolisa</option>
                                </select>
                            </th>
                        </tr>
                        <tr>
                            <th>Kebutuhan terhadap faktor X + V</th>
                            <th colspan="4">
                                <select class="form-control" name="faktorxv">
                                    <option value=""></option>
                                    <option value="Faktor X">Faktor X</option>
                                    <option value="Faktor V">Faktor V</option>
                                    <option value="Faktor X + V">Faktor X + V</option>
                                </select>
                            </th>
                        </tr>
                        <tr>
                            <th>Kebutuhan Oksigen</th>
                            <th colspan="4">
                                <select class="form-control" name="kebutuhan_oksigen">
                                    <option value=""></option>
                                    <option>Strict aero</option>
                                    <option>Akultatif aero</option>
                                    <option>Mikroaerofilik</option>
                                    <option>Capnophilic</option>
                                    <option>Aerotolerant</option>
                                </select>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="6">Fermentasi Karbohidrat dan uji biokimia lainnya (pilih salah satu)</th>
                        </tr>
                        <tr>
                        <?php $i = 1;?>
                        @foreach($fermentasipositif as $val)
                            <td>{{$val->fermentasi}}<input type="hidden" name="id_fermentasi_positif[]" value="{{$val->id}}"></td>
                            <td colspan="2">
                                <select class="form-control" name="fermentasipositif[]">
                                    <option value="Tanpa Test">Tanpa Test</option>
                                    <option value="Positif">Positif</option>
                                    <option value="Negatif">Negatif</option>
                                </select>
                            </td>
                        <?php 
                          if($i == 2){
                            $i =1;
                            echo "</tr><tr>";
                          }else{
                            $i++;
                          }
                        ?>
                        @endforeach
                            <td>Lain-lain</td>
                            <td colspan="2"><input type="text" name="fermentasilainp" class="form-control"></td>
                        </tr>
                    </table>

                    <h5>Uji Biokimia Automatic</h5>
                    <table class="table table-bordered">
                        <tr>
                            <th>Vitek 2</th>
                            <td>
                                <select name="vitek[]" class="selectpicker form-control" multiple>
                                    <option value="" selected></option>
                                    <option value="GN Card">GN Card</option>
                                    <option value="GP Card">GP Card</option>
                                    <option value="BCL Card">BCL Card</option>
                                    <option value="NH Card">NH Card</option>
                                    <option value="ANC Card">ANC Card</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>API</th>
                            <td>
                                <select name="api[]" class="selectpicker form-control" multiple>
                                    <option value="" selected></option>
                                    <option value="API NH">API NH</option>
                                    <option value="API CANDIDA">API CANDIDA</option>
                                    <option value="API 20 E">API 20 E</option>
                                    <option value="API 20 NE">API 20 NE</option>
                                    <option value="API STAPH">API STAPH</option>
                                    <option value="API 20 STREP">API 20 STREP</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>API</th>
                            <td>
                                <select name="api2[]" class="selectpicker form-control" multiple>
                                    <option value="" selected></option>
                                    <option>API 20 A</option>
                                    <option>API CAMPY</option>
                                    <option>API CORYNE</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>BD Phoenik</th>
                            <td>
                                <select name="bd_phoenik[]" class="selectpicker form-control" multiple>
                                    <option value="" selected></option>
                                    <option value="NID panel">NID panel</option>
                                    <option value="PID panel">PID panel</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Spesies</b></td>
                            <td>
                                <textarea name="spesies_auto" id="editor2" rows="10" cols="80"></textarea>
                            </td>
                        </tr>
                    </table>

                    <center><h2>UJI KEPEKAAN ANTIBIOTIK</h2></center>

                    <table class="table table-bordered">
                        <tr>
                            <th>Hasil Identifikasi</th>
                            <td><input type="text" name="hasil_identifikasi" class="form-control"></td>
                            <th><center>Metode/automatisasi</center></th>
                        </tr>
                        <tr>
                            <th>Standart</th>
                            <td>
                                <select name="standart[]" class="selectpicker form-control" multiple>
                                    <option value="" selected></option>
                                    <option value="CDS">CDS</option>
                                    <option value="CLSI">CLSI</option>
                                    <option value="EUCAST">EUCAST</option>
                                </select>
                            </td>
                            <td>
                                <select name="metode[]" class="selectpicker form-control" multiple>
                                    <option value="" selected></option>
                                    <option value="Disk Difusi">Disk Difusi</option>
                                    <option value="Vitex 2">Vitex 2</option>
                                    <option value="Phoenix">Phoenix</option>
                                    <option value="Agar Dilusi">Agar Dilusi</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <th>Jenis Antibiotik</th>
                            <th>Disk difusi (mm)</th>
                            <th>Interpretasi hasil</th>
                            <th>M.I.C</th>
                            <th>Interpretasi hasil</th>
                            <th>Kesimpulan</th>
                        </tr>
                        @foreach($antibiotik as $val)
                        <tr>
                            <input type="hidden" name="id_jenis_antibiotik[]" value="{{$val->id}}">
                            <td>
                                @if($val->antibiotik != 'Lain - lain')
                                {{$val->antibiotik}}
                                <input type="hidden" class="form-control" name="lain_lain[]" value="">
                                @else
                                <input type="text" class="form-control" name="lain_lain[]" value="" placeholder="Lain-lain">
                                @endif
                            </td>
                            <td><input type="text" name="disk[]" class="form-control"></td>
                            <td>
                                <select name="hasil1[]" class="form-control">
                                    <option value=""></option>
                                    <option value="S">S</option>
                                    <option value="I">I</option>
                                    <option value="R">R</option>
                                </select>
                            </td>
                            <td><input type="text" name="mic[]" class="form-control"></td>
                            <td>
                                <select name="hasil2[]" class="form-control">
                                    <option value=""></option>
                                    <option value="S">S</option>
                                    <option value="I">I</option>
                                    <option value="R">R</option>
                                </select>
                            </td>
                            <td>
                                <select name="kesimpulan[]" class="form-control">
                                    <option value=""></option>
                                    <option value="S">S</option>
                                    <option value="I">I</option>
                                    <option value="R">R</option>
                                </select>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <table class="table table-bordered">
                        <tr>
                            <th>Golongan MRSA</th>
                            <td>
                                <select class="form-control" name="mrsa">
                                    <option value=""></option>
                                    <option value="YA">YA</option>
                                    <option value="TIDAK">TIDAK</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Golongan ESBL</th>
                            <td>
                                <select class="form-control" name="esbl">
                                    <option value=""></option>
                                    <option value="YA">YA</option>
                                    <option value="TIDAK">TIDAK</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <div class="col-sm-12">
                        <label>Nama Penanggung jawab lab :</label>
                        <input type="text" name="penanggung_jawab" class="form-control">
                    </div><br>
                      {{ csrf_field() }}
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin: 15px 0px 0px 15px;">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Tidak boleh kosong');
    }
});
$(document).ready(function(){
    $('#autoUpdate').fadeOut('slow');
    $('#checkbox1').change(function(){
    if(this.checked)
        $('#autoUpdate').fadeIn('slow');
    else
        $('#autoUpdate').fadeOut('slow');

    });
});

$("input:checkbox").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input:checkbox[class='" + $box.attr("class") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});

CKEDITOR.replace( 'editor1' );
CKEDITOR.replace( 'editor2' );

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;



function dots(){
var dateBox = document.getElementById('hasil22')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 1) {
        dateBox.value = text + ".";
    }

var dateBox = document.getElementById('hasil25')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 2) {
        dateBox.value = text + ".";
    }

var dateBox = document.getElementById('hasil26')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 2) {
        dateBox.value = text + ".";
    }

var dateBox = document.getElementById('hasil30')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 1) {
        dateBox.value = text + ".";
    }
        
var dateBox = document.getElementById('hasil31')
    var chars = dateBox.value.length;
    var text = dateBox.value;

    if (chars == 2) {
        dateBox.value = text + ".";
    }
}

function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode

  // CHECK IF NUMBER
  if((charCode >=48 && charCode<=57) || (charCode>=96 && charCode<=105)){
     //IF KEY IS A NUMBER CALL DOTS
    dots();
  }
  else if(charCode==37||charCode==39||charCode==46 || charCode==8){
      // LET LEFT RIGHT BACKSPACE AND DEL PASS
}
    else{
    // BLOCK ALL OTHER KEYS
    evt.preventDefault();
    }
}
</script>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
$('.selectpicker').selectpicker({
  style: 'btn-info',
  size: 2
});
</script>
@endsection
