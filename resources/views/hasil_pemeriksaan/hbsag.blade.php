@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PROGRAM  NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}<br> HASIL PEMERIKSAAN HBsAg</label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$perusahaan}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <label>2. BAHAN UJI :</label>
                    <table class="table-bordered table">
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_diterima" autocomplete="off">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </td>
                                <td>Diperiksa tanggal :</td>
                                <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_diperiksa" autocomplete="off">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </td>
                            </tr>
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="6" width="20%"> Kondisi bahan uji saat diterima</td>
                                <td>Kode Bahan Uji <small>*(Wajib angka dan 3 digit)</small></td>
                                <td>Baik/Jernih </td>
                                <td>Keruh </td>
                                <td>Lain-lain</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="input-group">
                                        <div class="input-group-addon">III-1-</div>
                                        <input type="text" class="form-control" id="tabung11" name="no_tabung[]" max="999" required placeholder="999" pattern="[0-9]{3}">
                                    </div>
                                </td>
                                <td><input class="bahan1" type="checkbox" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input class="bahan1" type="checkbox" name="jenis[]" value="keruh"></td>
                                <td><input class="bahan1" type="checkbox" name="jenis[]" value="lain-lain"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="input-group">
                                        <div class="input-group-addon">III-2-</div>
                                        <input type="text" class="form-control" id="tabung21" name="no_tabung[]" max="999" required placeholder="999" pattern="[0-9]{3}">
                                    </div>
                                </td>
                                <td><input class="bahan2" type="checkbox" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input class="bahan2" type="checkbox" name="jenis[]" value="keruh"></td>
                                <td><input class="bahan2" type="checkbox" name="jenis[]" value="lain-lain"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="input-group">
                                        <div class="input-group-addon">III-3-</div>
                                        <input type="text" class="form-control" id="tabung31" name="no_tabung[]" max="999" required placeholder="999" pattern="[0-9]{3}">
                                    </div>
                                </td>
                                <td><input class="bahan3" type="checkbox" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input class="bahan3" type="checkbox" name="jenis[]" value="keruh"></td>
                                <td><input class="bahan3" type="checkbox" name="jenis[]" value="lain-lain"></td>
                            </tr>
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Metode Pemeriksaan </td>
                            <td><input class="metode_pemeriksaan" type="checkbox" name="metode[]" value="rapid" checked> Rapid <input class="metode_pemeriksaan" type="checkbox" name="metode[]" value="eia"> EIA / Setara </td>
                        </tr>
                        <tr>
                            <td>Nama Reagen</td>
                            <td>
                                <select id="alat1" class="form-control" name="nama_reagen[]" class="form-control" required>
                                    <option selected></option>
                                    @foreach($reagen as $val)
                                      <option value="{{$val->id}}">{{$val->reagen}}</option>
                                    @endforeach
                                </select>
                                <div id="row_alat1" class="inpualat1">
                                    <input id="inpualat1" class="form-control" type="text" name="reagen_lain[]" />
                                </div>
                            </td>
                        </tr>
<script>                            
$(function() {
    $('#row_alat1').hide(); 
    $('#alat1').change(function(){
    var setan  = $("#alat1 option:selected").text();
        if(setan.match('Lain - lain.*')) {
            $('#row_alat1').show(); 
        } else {
            $('#row_alat1').hide(); 
        } 
    });
});
</script>    
                        <tr>
                            <td>Nama Produsen</td>
                            <td><input type="text" name="nama_produsen[]" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            <td><input type="text" name="nomor_lot[]" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_kadaluarsa[]" autocomplete="off">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                            </td>
                        </tr>
                    </table>
                    <label>4. HASIL PEMERIKSAAN </label><br>
                    <small>* Ket : Jika menggunakan Rapid sebagai metode pengujian, kosongkan kolom lain dan langsung mengisi hasil di bagian <b>Interpretasi Hasil</b></small>
                    <table class="table table-bordered">
                        <tr>
                            <th>Kode Bahan Uji</th>
                            <td>Abs atau OD (A) (Bila dengan  EIA / Setara)</td>
                            <td>Cut Off (B) (Bila dengan EIA / Setara)</td>
                            <td>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</td>
                            <td>Interpretasi&nbsp;hasil&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group">
                                    <div class="input-group-addon">III-1-</div>
                                    <input type="number" class="form-control" id="tabung12" name="kode_bahan_kontrol[]" readonly required placeholder="999">
                                </div>
                            </td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco1[]"/></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non1">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group">
                                    <div class="input-group-addon">III-2-</div>
                                    <input type="number" class="form-control" id="tabung22" name="kode_bahan_kontrol[]" readonly required placeholder="999">
                                </div>
                            </td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco1[]"/></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non1">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="input-group">
                                    <div class="input-group-addon">III-3-</div>
                                    <input type="number" class="form-control" id="tabung32" name="kode_bahan_kontrol[]" readonly required placeholder="999">
                                </div>
                            </td>
                            <td><input type="text" class="decimal form-control" value="" name="abs_od1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="cut_off1[]"/></td>
                            <td><input type="text" class="decimal form-control" value="" name="sco1[]"/></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non1">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Catatan :</label>
                            <textarea name="keterangan" class="form-control">-</textarea>
                        </div>
                        <div class="col-sm-6">
                            <label>Petugas yang melakukan pemeriksaan :</label>
                            <input type="text" name="petugas_pemeriksaan" class="form-control" required>
                        </div>
                    </div>
                    <br>

                      {{ csrf_field() }}
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Tidak boleh kosong');
    }
});
$('.decimal').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.\>\<]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});

$(document).ready(function(){
    $("#tabung11").change(function(){
        $val = $(this).val();
        $('#tabung12').val($val);
    });
    $("#tabung21").change(function(){
        $val1 = $(this).val();
        $('#tabung22').val($val1);
    });
    $("#tabung31").change(function(){
        $val2 = $(this).val();
        $('#tabung32').val($val2);
    });
});
$("input:checkbox").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input:checkbox[class='" + $box.attr("class") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});
</script>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minView: 2
    });
</script>
@endsection
