@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>Program Pemantapan Nasional Pemantapan Mutu Eksternal Mikroskopis BTA Siklus {{$siklus}} Tahun {{$tahun}}</label></center><br>
                    
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{substr($perusahaan, -3)}}-BTA-<?php if($siklus == 1){echo 'I';}else{echo 'II';} ?>-{{date('y', strtotime($tahun))}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_penerimaan">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kondisi Bahan Uji </label>
                        <div class="col-sm-9">  
                          <input type="radio" name="kondisi" value="baik" required> Baik
                          <input type="radio" name="kondisi" value="pecah/tumpah"> Pecah/Tumpah
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Pemeriksaan </label>
                        <div class="col-sm-9">
                            <div class="controls input-append date" data-link-field="dtp_input1">
                                  <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_pemeriksaan">
                                  <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Pemeriksa </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_pemeriksa" placeholder="Nama Pemeriksa" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nomor Hp </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nomor_hp" placeholder="Nomor HP" required>
                        </div>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Bahan Uji</th>
                                <th>Hasil Pemeriksaan</th>
                            </tr>
                        </thead>
                        <tbody id="wrapper">
                            <?php
                                for ($i=1; $i <= 10 ; $i++) { 
                            ?>
                            <tr>
                                <td>{{$i}}</td>
                                <td><input type="text" class="form-control" size="16" name="kode[]" value="{{substr($perusahaan, -3)}}/<?php if($i < 10){ echo '0'.$i; }else{echo $i;} ?>/BTA/<?php if($siklus == 1){echo 'I';}else{echo 'II';} ?>/{{date('y', strtotime($tahun))}}" readonly></td>
                                <td>
                                    <select class="form-control" name="hasil[]" id="hasil{{$i}}">
                                        <option value=""></option>
                                        <option value="Negatif">Negatif</option>
                                        <option value="Scanty">Scanty</option>
                                        <option value="1+">1+</option>
                                        <option value="2+">2+</option>
                                        <option value="3+">3+</option>
                                    </select>
                                    <div id="kuman{{$i}}">
                                        <div class="input-group">
                                            <input type="text" name="kuman[]" class="form-control" id="inputkuman{{$i}}">
                                            <div class="input-group-addon">BTA/100 Lapang Pandang</div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
<script type="text/javascript">
$(function() {
    $('#kuman{{$i}}').hide(); 
    $('#hasil{{$i}}').change(function(){
    var setan  = $("#hasil{{$i}} option:selected").text();
        if(setan.match('Scanty.*')) {
            $('#kuman{{$i}}').show(); 
            $("#inputkuman{{$i}}").prop('required',true);
        } else {
            $('#kuman{{$i}}').hide(); 
            $("#inputkuman{{$i}}").prop('required',false);
            $("#inputkuman{{$i}}").val('');
        } 
    });
});
</script>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                    <!-- <div><input type="button" id="more_fields" onclick="add_fields();" value="Tambah Baris" /></div> -->
                    
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Catatan :</label>
                            <textarea name="catatan" class="form-control">-</textarea>
                        </div>
                        <div class="col-sm-6">
                            <label>Petugas yang melakukan pemeriksaan :</label>
                            <input type="text" name="penanggung_jawab" class="form-control" required>
                        </div>
                    </div>
                    <small>Info : Untuk mencetak hasil input pengujian PNPME, silahkan mengirimkan hasil pengujian di bagian Edit Hasil dengan mengklik tombol <b>Kirim</b> di Sub Menu Edit Hasil.</small><br>

                      {{ csrf_field() }}
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit"  style="margin: 15px 0px 0px 0px;">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Tidak boleh kosong');
    }
});

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;
</script>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minView: 2
    });

function add_fields() {
    document.getElementById('wrapper').innerHTML += '<tr><td><input type="text" class="form-control" size="16" name="kode[]"></td><td><input type="text" class="form-control" size="16" name="hasil[]"></td></tr>';
}
</script>
@endsection
