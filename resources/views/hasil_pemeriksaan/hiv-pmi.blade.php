@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>FORMULIR HASIL PEMERIKSAAN PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br>ANTI HIV (PALANG MERAH INDONESIA) SIKLUS {{$siklus}} TAHUN {{$date}} </label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$perusahaan}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <label>2. BAHAN UJI :</label>
                    <table class="table-bordered table">
                            <tr>
                                <td>Diterima tanggal :</td>
                                <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_diterima">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </td>
                                <td>Diperiksa tanggal :</td>
                                <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_diperiksa">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                                </td>
                            </tr>
                    </table>

                    <table class="table-bordered table">
                            <tr>
                                <td rowspan="6" width="20%"> Kondisi bahan uji saat diterima</td>
                                <td>Kode Bahan Uji</td>
                                <td>Baik/Jernih </td>
                                <td>Keruh </td>
                                <td>Lain-lain</td>
                            </tr>
                            <tr>
                                <td><input type="text" name="no_tabung[]" id="tabung11" class="form-control" value=""></td>
                                <td><input type="checkbox" class="jenis1" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input type="checkbox" class="jenis1" name="jenis[]" value="keruh"></td>
                                <td><input type="checkbox" class="jenis1" name="jenis[]" value="lain-lain"></td>
                            </tr>
                            <tr>
                                <td><input type="text" name="no_tabung[]" id="tabung21" class="form-control" value=""></td>
                                <td><input type="checkbox" class="jenis2" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input type="checkbox" class="jenis2" name="jenis[]" value="keruh"></td>
                                <td><input type="checkbox" class="jenis2" name="jenis[]" value="lain-lain"></td>
                            </tr>
                            <tr>
                                <td><input type="text" name="no_tabung[]" id="tabung31" class="form-control" value=""></td>
                                <td><input type="checkbox" class="jenis3" name="jenis[]" value="baik/jernih" checked></td>
                                <td><input type="checkbox" class="jenis3" name="jenis[]" value="keruh"></td>
                                <td><input type="checkbox" class="jenis3" name="jenis[]" value="lain-lain"></td>
                            </tr>
                    </table>
                    <label>3. REAGEN</label>
                    <table class="table table-bordered">
                        <tr>
                            <td>Nama Reagen</td>
                            <td>
                                <select id="alat1" class="form-control" name="nama_reagen[]" idx='1' class="form-control" required>
                                    <option selected></option>
                                    @foreach($reagen as $val)
                                      <option value="{{$val->id}}">{{$val->reagen}}</option>
                                    @endforeach
                                </select>
                                <div id="row_alat1" class="inpualat1">
                                    <input id="inpualat1" class="form-control" type="text" name="reagen_lain[]" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Metode Pemeriksaan </td>
                            <td>
                                <input type="text" name="metode[]" value="" id="re1" required="" class="form-control">
                            </td>
                        </tr>
<script>                            
$(function() {
    $('#row_alat1').hide(); 
    $('#alat1').change(function(){
    var setan  = $("#alat1 option:selected").text();
        if(setan.match('Lain - lain.*')) {
            $('#row_alat1').show(); 
        } else {
            $('#row_alat1').hide(); 
        } 
    });
});
</script>
                        <tr>
                            <td>Nama Produsen</td>
                            <td><input type="text" name="nama_produsen[]" class="form-control" id="produsen1" readonly=""></td>
                        </tr>
                        <tr>
                            <td>Nomor Lot/Batch</td>
                            <td><input type="text" name="nomor_lot[]" class="form-control" id="lot1"></td>
                        </tr>
                        <tr>
                            <td>Tanggal Kadaluarsa</td>
                            <td>
                                  <div class="controls input-append date" data-link-field="dtp_input1">
                                      <input size="16" type="text" value="" readonly class="form_datetime form-control" name="tgl_kadaluarsa[]">
                                      <span class="add-on"><i class="icon-th"></i></span>
                                  </div>
                            </td>
                        </tr>

                        <tr>
                            <td>Sensitifitas </td>
                            <td><input type="text" class="form-control" id="sen1" name="sensitivitas[]"></td>
                        </tr>
                        <tr>
                            <td>Spesitivitas </td>
                            <td><input type="text" class="form-control" id="sep1" name="spesifisitas[]"></td>
                        </tr>
                    </table>
                    <label>4. HASIL PEMERIKSAAN </label>
                    <table class="table table-bordered">
                        <tr>
                            <th>Kode Bahan Uji</th>
                            <td>Abs atau OD (A) (Bila dengan  EIA / Setara)</td>
                            <td>Cut Off (B) (Bila dengan EIA / Setara)</td>
                            <td>S/CO(A:B) atau True Value (TV) atau indeks (Bila dengan EIA / Setara)</td>
                            <td>Interpretasi&nbsp;hasil&nbsp;</td>
                        </tr>
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung12" class="form-control" value="" readonly></td>
                            <td><input type="text" name="abs_od1[]" class="form-control"></td>
                            <td><input type="text" name="cut_off1[]" class="form-control"></td>
                            <td><input type="text" name="sco1[]" class="form-control"></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung22" class="form-control" value="" readonly></td>
                            <td><input type="text" name="abs_od1[]" class="form-control"></td>
                            <td><input type="text" name="cut_off1[]" class="form-control"></td>
                            <td><input type="text" name="sco1[]" class="form-control"></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><input type="text" name="kode_bahan_kontrol[]" id="tabung32" class="form-control" value="" readonly></td>
                            <td><input type="text" name="abs_od1[]" class="form-control"></td>
                            <td><input type="text" name="cut_off1[]" class="form-control"></td>
                            <td><input type="text" name="sco1[]" class="form-control"></td>
                            <td>
                                <select name="interpretasi1[]" class="form-control non5">
                                    <option value="Tanpa test">Tanpa test</option>
                                    <option value="Reaktif">Reaktif</option>
                                    <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <div class="col-sm-6">
                        <label>Alasan bila tidak melakukan pemeriksaan :</label>
                        <textarea name="keterangan" class="form-control">-</textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Petugas yang melakukan pemeriksaan :</label>
                        <input type="text" name="petugas_pemeriksaan" class="form-control" required>
                    </div><br>

                      {{ csrf_field() }}
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin-top: 20px">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Tidak boleh kosong');
    }
});
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

$("select[name='nama_reagen[]']").change(function(){
  var val = $(this);
  console.log(val);
  var y = $('#re'+val.attr('idx'));
  var x = $('#produsen'+val.attr('idx'));
  var se = $('#sen'+val.attr('idx'));
  var sp = $('#sep'+val.attr('idx'));
  var n = $('#lot'+val.attr('idx'));
  var t = $('#tanggal'+val.attr('idx'));
  $.ajax({
    type: "GET",
    url : "{{url('getreagenimun').'/'}}"+val.val(),
    success: function(addr){
        y.val(addr.Hasil[0].Metode);
        x.val(addr.Hasil[0].Produsen);
        se.val(addr.Hasil[0].sensitivitas);
        sp.val(addr.Hasil[0].spesifisitas);
        if (y.val() == '-') {
            n.attr("required",false);
            t.attr("required",false);
        }else{
            n.attr("required",true);
            t.attr("required",true);
        }
    }
  });
});

$(document).ready(function(){
    $("#tabung11").change(function(){
        $val = $(this).val();
        $('#tabung12').val($val);
    });
    $("#tabung21").change(function(){
        $val1 = $(this).val();
        $('#tabung22').val($val1);
    });
    $("#tabung31").change(function(){
        $val2 = $(this).val();
        $('#tabung32').val($val2);
    });
});
$("input:checkbox").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    var group = "input:checkbox[class='" + $box.attr("class") + "']";
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});
today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;
</script>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minView: 2
    });
</script>
@endsection
