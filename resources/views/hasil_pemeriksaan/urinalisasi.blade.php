@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" id="form_d" method="post" enctype="multipart/form-data">
                    <center><label> 
                        FORMULIR HASIL PEMERIKSAAN PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL URINALISIS <br>SIKLUS {{$siklus}} TAHUN {{$date}} <input type="hidden" name="type" value="{{$type}}">
                    </label></center><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <!-- <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{substr($perusahaan,0, -5)}}{{$siklus}}{{substr($perusahaan, 11)}}" placeholder="Kode Peserta" readonly> -->
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{$perusahaan}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Bahan </label>
                        <div class="col-sm-9">
                            <?php 
                                if ($siklus == '1') {
                                    if ($type == 'a') {
                                        $kodebahan = 'I-A';
                                    }else{
                                        $kodebahan = 'I-B';
                                    }
                                }else{
                                    if ($type == 'a') {
                                        $kodebahan = 'II-A';
                                    }else{
                                        $kodebahan = 'II-B';
                                    }
                                }
                            ?>
                            <input type="text" class="form-control" id="kode_bahan" name="kode_bahan" value="" placeholder="Kode Bahan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" required class="form_datetime form-control" name="tanggal_penerimaan" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kualitas Bahan </label>
                        <div class="col-sm-9">  
                          <input type="radio" name="kualitas" required value="baik"> Baik
                          <input type="radio" name="kualitas" value="tidak-dingin"> Tidak Dingin
                          <input type="radio" name="kualitas" value="pecah"> Pecah
                          <input type="radio" name="kualitas" value="lisis"> Lisis
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Pemeriksaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" required class="form_datetime form-control" name="tanggal_pemeriksaan" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Parameter</th>
                                <th>Instrument</th>
                                <th>Reagen/Kit Carik Celup</th>
                                <th>Metode Pemeriksaan</th>
                                <th>Hasil Pemeriksaan</th>
                                <th>Catatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($data))
                            <?php $no = 0; ?>
                            @foreach($data as $dkey => $val)
                            <?php $no++; ?>
                            <tr>
                                <td>{{$no}}</td>
                                <td>{!!$val->nama_parameter!!}<input type="hidden" name="parameter_id[{{$dkey}}]" value="{{ $val->id }}" /></td>
                                <td><input type="text" class="form-control" size="16" name="alat[{{$dkey}}]" id="instrument{{$val->id}}"></td>
                                <td>
                                    <select id="alat{{$val->id}}" class="form-control" name="reagen[{{$dkey}}]" class="form-control" >
                                        <option selected value="-">-</option>
                                        @if($val->id == '19')
                                            @foreach($val->ReagenImunologi as $valr)
                                              <option value="{{$valr->id}}">{!!$valr->reagen!!}</option>
                                            @endforeach
                                        @else
                                            @foreach($reagen as $reg)
                                              <option value="{{$reg->id}}">{!!$reg->reagen!!}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div id="row_alat{{$val->id}}" class="inpualat{{$val->id}}">
                                        <input id="inpualat{{$val->id}}" class="form-control" type="text" name="reagen_lain[{{$dkey}}]" />
                                    </div>
                                </td>
                                <td id="metodelain{{$val->id}}">
                                    <select id="kodemetode{{$val->id}}" class="form-control" name="kode[{{$dkey}}]" class="form-control" >
                                        <option selected value="-">-</option>
                                        @foreach($val->metodePemeriksaan as $val2)
                                          <option value="{{$val2->id}}">{!!$val2->metode_pemeriksaan!!}</option>
                                        @endforeach
                                    </select>
                                    <div id="row_dim{{$val->id}}" class="inputkode{{$val->id}}">
                                        <input id="inputkode{{$val->id}}" class="form-control" type="text" name="metode_lain[{{$dkey}}]" />
                                    </div>
                                </td>
                                <td>
                                    @if(count($val->hasilPemeriksaan))
                                    <select class="form-control" name="hasil[{{$dkey}}]" class="form-control" required id="hasil{{$val->id}}">
                                        <option selected></option>
                                        <option value="Tidak Mengerjakan">Tidak Mengerjakan</option>
                                        @foreach($val->hasilPemeriksaan as $val2)
                                          <option value="{{$val2->id}}">{!!$val2->hp!!}</option>
                                        @endforeach
                                    </select>
                                    @else
                                    @if($val->id == 9)
                                    <input type="text" id="hasil{{$val->id}}" class="form-control hasil validate-hasil" name="hasil[{{$dkey}}]" min="0" max="9.999" 
                                     onKeyUp="if(this.value>9.999){this.value='';}else if(this.value<0){this.value='';}" required placeholder="Maksimal 9.999">
                                    @elseif($val->id == 10)
                                    <input type="text" id="hasil{{$val->id}}" class="form-control hasil validate-hasil" name="hasil[{{$dkey}}]" min="0" max="9.9" 
                                     onKeyUp="if(this.value>9.9){this.value='';}else if(this.value<0){this.value='';}" required placeholder="Maksimal 9.9">
                                    @endif
                                    @endif
                                </td>
                                <td>{{$val->catatan}}</td>
                            </tr>
<script>
$(document).ready(function(){
    $("#hasil{{$val->id}}").change(function(){
    var value = $(this).val();
        if (value == '-' || value == 'NaN' || value == 'Tidak Mengerjakan'){
            console.log('bisa');
                $("#instrument{{$val->id}}").prop('required',false);
                $("#alat{{$val->id}}").prop('required',false);
                $("#kodemetode{{$val->id}}").prop('required',false);
        }else{
            if(value == ''){
                console.log('bisa');  
                $("#instrument{{$val->id}}").prop('required',false);
                $("#alat{{$val->id}}").prop('required',false);
                $("#kodemetode{{$val->id}}").prop('required',false);
            }else{
                console.log('gak bisa');
                $("#instrument{{$val->id}}").prop('required',true);
                $("#alat{{$val->id}}").prop('required',true);
                $("#kodemetode{{$val->id}}").prop('required',true);
            }
        }
    });
});
$(function() {
    $('#row_alat{{$val->id}}').hide(); 
    $('#alat{{$val->id}}').change(function(){
    var setan  = $("#alat{{$val->id}} option:selected").text();
        if(setan.match('Reagen Lain.*')) {
            $('#row_alat{{$val->id}}').show(); 
            $("#inpualat{{$val->id}}").prop('required',true);
        } else {
            $('#row_alat{{$val->id}}').hide(); 
            $("#inpualat{{$val->id}}").prop('required',false);
            $("#inpualat{{$val->id}}").val('');
        } 
    });
});

$(function() {
    $('#row_dim{{$val->id}}').hide(); 
    $('#kodemetode{{$val->id}}').change(function(){
    var setan  = $("#kodemetode{{$val->id}} option:selected").text();
        if(setan.match('lain-lain.*')) {
            $('#row_dim{{$val->id}}').show(); 
            $("#inputkode{{$val->id}}").prop('required',true);
        } else {
            $('#row_dim{{$val->id}}').hide(); 
            $("#inputkode{{$val->id}}").prop('required',false);
            $("#inputkode{{$val->id}}").val('');
        } 
    });
});
</script>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <label>Keterangan :</label>
                    <p> 
                        -   Jika ada parameter yang tidak Saudara kerjakan, isi kolom hasil dengan tanda strip “-“<br>
                        -   Info : Untuk mencetak hasil input pengujian PNPME, silahkan mengirimkan hasil pengujian di bagian Edit Hasil dengan mengklik tombol <b>Kirim</b> di Sub Menu Edit Hasil.
                    </p><br>
                    <div class="col-sm-6">
                        <label>Catatan :</label>
                        <textarea class="form-control" name="catatan"></textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Nama Penanggung jawab lab :</label>
                        <input type="text" class="form-control" name="penanggung_jawab" required>
                    </div><br>
                      {{ csrf_field() }}
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin: 15px 0px 0px 15px;">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Wajib Diisi');
    }

    @if(count($data))
    @foreach($data as $val)
        var textfield2 = $ ("#hasil{{$val->id}}").get(0);
        textfield2.setCustomValidity("");
        if (!textfield2.validity.valid) {
          textfield2.setCustomValidity('Tidak boleh kosong, atau gunakan strip "-"');
        }
    @endforeach
    @endif
});
$("form select").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Wajib Diisi');
    }
});


$(document).ready(function(){
    @foreach($data as $val)
    $('#hasil{{$val->id}}').blur(function(){
        var num = parseFloat($(this).val());
        @if($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal')
        console.log(num);
        var cleanNum = num.toFixed(1);
        @elseif($val->catatan == 'Hasil pemeriksaan menggunakan 3 (tiga) desimal')
        var cleanNum = num.toFixed(3);
        @endif
        if (cleanNum == 'NaN') {
            $(this).val('-');
        }else{
            $(this).val(cleanNum);
        }
    });
    @endforeach
});

$(document).ready(function(){
    $('#autoUpdate').fadeOut('slow');
    $('#checkbox1').change(function(){
    if(this.checked)
        $('#autoUpdate').fadeIn('slow');
    else
        $('#autoUpdate').fadeOut('slow');

    });
});

</script>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
</script>
@endsection
