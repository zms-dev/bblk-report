@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" id="form_d" enctype="multipart/form-data">
                    <center><label> 
                        FORMULIR HASIL PEMERIKSAAN PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL KIMIA KLINIK <br>SIKLUS {{$siklus}} TAHUN {{$date}}<input type="hidden" name="type" value="{{$type}}">
                    </label></center><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $perusahaan }}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Bahan </label>
                        <div class="col-sm-9">
                          
                            <input type="text" class="form-control" id="kode_bahan" value="" name="kode_bahan" placeholder="Kode Bahan" required="">
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-date="2017-07-07" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" required class="form_datetime form-control" name="tanggal_penerimaan" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kualitas Bahan </label>
                        <div class="col-sm-9">  
                          <input type="radio" required name="kualitas" required value="baik"> Baik
                          <input type="radio" name="kualitas" value="tidak-dingin"> Tidak Dingin
                          <input type="radio" name="kualitas" value="pecah"> Pecah
                          <input type="radio" name="kualitas" value="lisis"> Lisis
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Pemeriksaan </label>
                        <div class="col-sm-9">
                        <div class="controls input-append date" data-date="2017-07-07" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" required class="form_datetime form-control" name="tanggal_pemeriksaan" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Parameter</th>
                                <th>Instrument</th>
                                <th>Metode Pemeriksaan</th>
                                <th>Hasil Pemeriksaan</th>
                                <th>Catatan</th>
                                <th>Tidak Dikerjakan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($data))
                            <?php $no = 0; ?>
                            @foreach($data as $dkey => $val)
                            <?php $no++; ?>
                            <tr>
                                <td>{{$no}}</td>
                                <td>{!!$val->nama_parameter!!}<input type="hidden" name="parameter_id[{{$dkey}}]" value="{{ $val->id }}" /></td>
                                <td>
                                    @if(count($val->Instrumen))
                                    <select id="alat{{$val->id}}" class="form-control" name="alat[{{$dkey}}]" class="form-control" >
                                        <option selected></option>
                                        @foreach($val->Instrumen as $ins)
                                            <option value="{{$ins->id}}">{{$ins->instrumen}}</option>
                                        @endforeach
                                    </select>
                                    @else
                                    <input type="text" id="alat{{$val->id}}" name="alat[{{$dkey}}]" class="form-control">
                                    @endif
                                    <div id="row_alat{{$val->id}}" class="inpualat{{$val->id}}">
                                        <input id="inpualat{{$val->id}}" class="form-control" type="text" name="instrument_lain[{{$dkey}}]" />
                                    </div>
                                </td>
                                <td id="metodelain{{$val->id}}">
                                @if(count($val->metodePemeriksaan))
                                <select id="kodemetode{{$val->id}}" class="form-control" name="kode[{{$dkey}}]" class="form-control">
                                    <option selected></option>
                                    @foreach($val->metodePemeriksaan as $val2)
                                      <option value="{{$val2->id}}">{!!$val2->metode_pemeriksaan!!}</option>
                                    @endforeach
                                </select>
                                @else
                                <input type="text" name="kode[{{$dkey}}]" class="form-control">
                                @endif
                                <div id="row_dim{{$val->id}}" class="inputkode{{$val->id}}">
                                    <input id="inputkode{{$val->id}}" class="form-control" type="text" name="metode_lain[{{$dkey}}]" />
                                </div>
<script>
$(document).ready(function(){
    $("#hasil{{$val->id}}").keyup(function(){
    var value = $(this).val();
        if (value == '-' || value == 'NaN'){
            console.log('bisa');
                $("#alat{{$val->id}}").prop('required',false);
                $("#kodemetode{{$val->id}}").prop('required',false);
        }else{
            if(value == ''){
                console.log('bisa');  
                $("#alat{{$val->id}}").prop('required',false);
                $("#kodemetode{{$val->id}}").prop('required',false);
            }else{
                console.log('gak bisa');
                $("#alat{{$val->id}}").prop('required',true);
                $("#kodemetode{{$val->id}}").prop('required',true);
            }
        }
    });
});
$(function() {
    $('#row_alat{{$val->id}}').hide(); 
    $('#alat{{$val->id}}').change(function(){
    var setan  = $("#alat{{$val->id}} option:selected").text();
        if(setan.match('Alat Lain.*')) {
            $('#row_alat{{$val->id}}').show(); 
            $("#inpualat{{$val->id}}").prop('required',true);
        } else {
            $('#row_alat{{$val->id}}').hide(); 
            $("#inpualat{{$val->id}}").prop('required',false);
            $("#inpualat{{$val->id}}").val('');
        } 
    });
});

$(function() {
    $('#row_dim{{$val->id}}').hide(); 
    $('#kodemetode{{$val->id}}').change(function(){
    var setan  = $("#kodemetode{{$val->id}} option:selected").text();
        if(setan.match('Metode lain.*') ) {
            $('#row_dim{{$val->id}}').show();
            $("#inputkode{{$val->id}}").prop('required',true);
        } else {
            $('#row_dim{{$val->id}}').hide(); 
            $("#inputkode{{$val->id}}").prop('required',false);
            $("#inputkode{{$val->id}}").val('');
        } 
    });
});
</script>
                                </td>
                                <td>
                                    @if($val->id == 31)
                                        <input type="text" id="hasil{{$val->id}}" class="form-control hasil validate-hasil" size="16" name="hasil[{{$dkey}}]"  min="0" max="99.9" 
                                     onKeyUp="if(this.value>99.9){this.value='';}else if(this.value<0){this.value='';}" required placeholder="Maksimal 99.9">
                                     @elseif($val->id == 29)
                                        <input type="text" id="hasil{{$val->id}}" class="form-control hasil validate-hasil" size="16" name="hasil[{{$dkey}}]"  min="0" max="999.9" 
                                     onKeyUp="if(this.value>999.9){this.value='';}else if(this.value<0){this.value='';}" required placeholder="Maksimal 999.9">
                                    @elseif($val->id == 25 || $val->id == 26 || $val->id == 30 || $val->id == 22)
                                        <input type="text" id="hasil{{$val->id}}" class="form-control hasil validate-hasil" size="16" name="hasil[{{$dkey}}]"  min="0" max="9.99" 
                                     onKeyUp="if(this.value>9.99){this.value='';}else if(this.value<0){this.value='';}" required placeholder="Maksimal 9.99">
                                    @elseif($val->id == 20 || $val->id == 21 || $val->id == 24 || $val->id == 23 || $val->id == 32 || $val->id == 27 || $val->id == 28 )
                                        <input type="text" id="hasil{{$val->id}}" class="form-control hasil validate-hasil" size="16" name="hasil[{{$dkey}}]"  min="0" max="999" 
                                     onKeyUp="if(this.value>999){this.value='';}else if(this.value<0){this.value='';}" required placeholder="Maksimal 999">
                                 
                                    @endif
                                    <!-- <input type="text" class="form-control hasil validate-hasil" id="hasil{{$val->id}}" size="16" name="hasil[{{$dkey}}]" required></td> -->
                                <td>{{$val->catatan}}</td>
                                <td><center><input type="checkbox" name="" id="tidak{{$val->id}}"></center></td>
                            </tr>
                            <script type="text/javascript">
                                $("#tidak{{$val->id}}").change(function() {
                                    if(this.checked) {
                                        $("#hasil{{$val->id}}").val('-');
                                    }else{
                                        $("#hasil{{$val->id}}").val('');
                                    }
                                });
                            </script>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    <label>Keterangan :</label>
                    <p> 
                        -   Jika ada parameter yang tidak Saudara kerjakan, isi kolom hasil dengan tanda strip “-“<br>
                        -   Info : Untuk mencetak hasil input pengujian PNPME, silahkan mengirimkan hasil pengujian di bagian Edit Hasil dengan mengklik tombol <b>Kirim</b> di Sub Menu Edit Hasil.
                    </p><br>
                    <div class="col-sm-6">
                        <label>Catatan :</label>
                        <textarea class="form-control" name="catatan"></textarea>
                    </div>
                    <div class="col-sm-6">
                        <label>Nama Penanggung jawab lab :</label>
                        <input type="text" name="penanggung_jawab" class="form-control" required>
                    </div><br>
                      {{ csrf_field() }}
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin: 15px 0px 0px 15px;">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Wajib Diisi');
    }

    @if(count($data))
    @foreach($data as $val)
        var textfield2 = $ ("#hasil{{$val->id}}").get(0);
        textfield2.setCustomValidity("");
        if (!textfield2.validity.valid) {
          textfield2.setCustomValidity('Tidak boleh kosong, atau gunakan strip "-"');
        }
    @endforeach
    @endif
});
$("form select").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Wajib Diisi');
    }
});

$(document).ready(function(){
    $('#autoUpdate').fadeOut('slow');
    $('#checkbox1').change(function(){
    if(this.checked)
        $('#autoUpdate').fadeIn('slow');
    else
        $('#autoUpdate').fadeOut('slow');

    });
});

@foreach($data as $val)
@if($val->catatan == 'Hasil pemeriksaan tanpa desimal')
$("body").on("keypress","#hasil{{$val->id}}", function(evt) {
    console.log(evt.keyCode)
  var keycode = evt.charCode || evt.keyCode;
  if (keycode == 46 || keycode == 44) {
    console.log('ada');
    return false;
  }
});
@endif
@endforeach

$(document).ready(function(){
    @foreach($data as $val)
    $('#hasil{{$val->id}}').blur(function(){
        var num = parseFloat($(this).val());
        @if($val->catatan == 'Hasil pemeriksaan menggunakan 1 (satu) desimal')
        console.log(num);
        var cleanNum = num.toFixed(1);
        @elseif($val->catatan == 'Hasil pemeriksaan menggunakan 2 (dua) desimal')
        var cleanNum = num.toFixed(2);
        @endif
        if (cleanNum == 'NaN') {
            $(this).val('-');
        }else{
            $(this).val(cleanNum);
        }
    });
    @endforeach
});

function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode

  // CHECK IF NUMBER
  if((charCode >=48 && charCode<=57) || (charCode>=96 && charCode<=105)){
     //IF KEY IS A NUMBER CALL DOTS
    dots();
  }
  else if(charCode==37||charCode==39||charCode==46 || charCode==8){
      // LET LEFT RIGHT BACKSPACE AND DEL PASS
}
    else{
    // BLOCK ALL OTHER KEYS
    evt.preventDefault();
    }
}
</script>
<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayBtn: true,
        minView: 2
    });
</script>
@endsection
