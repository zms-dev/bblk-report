@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <center><label>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MIKROBIOLOGI MIKROSKOPIS TELUR CACING SIKLUS {{$siklus}} TAHUN {{$date}}</label></center><br>
                    
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kode Peserta </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{substr($perusahaan, -3)}}-TCC-<?php if($siklus == 1){echo 'I';}else{echo 'II';} ?>-{{date('y', strtotime($date))}}" placeholder="Kode Peserta" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Penerimaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_penerimaan">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Kondisi Bahan Uji </label>
                        <div class="col-sm-9">  
                          <input type="radio" name="kondisi" value="baik" required> Baik
                          <input type="radio" name="kondisi" value="pecah/tumpah"> Pecah/Tumpah
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Tanggal Pemeriksaan </label>
                        <div class="col-sm-9">
                          <div class="controls input-append date" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" required class="form_datetime form-control" name="tgl_pemeriksaan">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nama Pemeriksa </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_pemeriksa" placeholder="Nama Pemeriksa" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama" class="col-sm-3 control-label">Nomor Hp </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nomor_hp" placeholder="Nomor HP" required>
                        </div>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Bahan Uji</th>
                                <th>Reagen Yang Digunakan</th>
                                <th>Hasil Pemeriksaan</th>
                            </tr>
                        </thead>
                        <tbody id="wrapper">
                            <?php
                                $h = 'A';
                                for ($i=1; $i <= 3 ; $i++) { 
                            ?>
                            <tr>
                                <td>{{$i}}</td>
                                <td>
                                    <div class="input-group">
                                        <div class="input-group-addon">TCC-</div>
                                        <input type="text" class="form-control" id="exampleInputAmount" name="kode_botol[]" maxlength="3" required>
                                        <div class="input-group-addon">{{$h++}}</div>
                                    </div>
                                </td>
                                <td>
                                    <select id="reagen{{$i}}" class="form-control" name="reagen[]" class="form-control" required>
                                        <option selected></option>
                                        <option value="Lugol">Lugol</option>
                                        <option value="Eosin">Eosin</option>
                                        <option value="PZ (NaCl 0,9 %)">PZ (NaCl 0,9 %)</option>
                                        <option value="lain-lain">Lain-lain</option>
                                    </select>
                                    <div id="row_alat{{$i}}" class="inpualat{{$i}}">
                                        <input id="inpualat{{$i}}" class="form-control" type="text" name="reagen_lain[]" />
                                    </div>
                                </td>
                                <td><textarea name="hasil[]" id="editor{{$i}}" rows="10" cols="80"></textarea></td>
                            </tr>
<script>                                 
$(function() {
    $('#row_alat{{$i}}').hide(); 
    $('#reagen{{$i}}').change(function(){
    var setan  = $("#reagen{{$i}} option:selected").text();
        if(setan.match('Lain-lain.*')) {
            $('#row_alat{{$i}}').show(); 
        } else {
            $('#row_alat{{$i}}').hide(); 
        } 
    });
});
</script>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                    <!-- <div><input type="button" id="more_fields" onclick="add_fields();" value="Tambah Baris" /></div> -->
                    
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Catatan :</label>
                            <textarea name="keterangan" class="form-control">-</textarea>
                        </div>
                        <div class="col-sm-6">
                            <label>Petugas yang melakukan pemeriksaan :</label>
                            <input type="text" name="penanggung_jawab" class="form-control" required>
                        </div>
                    </div>
                    <small>Info : Untuk mencetak hasil input pengujian PNPME, silahkan mengirimkan hasil pengujian di bagian Edit Hasil dengan mengklik tombol <b>Kirim</b> di Sub Menu Edit Hasil.</small><br>

                      {{ csrf_field() }}
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-submit" style="margin: 15px 0px 0px 0px;">
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("form input").on("change invalid", function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity("");
    
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Tidak boleh kosong');
    }
});
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;
document.getElementById("demo").innerHTML = today;
</script>

<script type="text/javascript" >
    CKEDITOR.replace( 'editor1' );
    CKEDITOR.replace( 'editor2' );
    CKEDITOR.replace( 'editor3' );

$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});

function add_fields() {
    document.getElementById('wrapper').innerHTML += '<tr><td><input type="text" class="form-control" size="16" name="kode_botol[]"></td><td><input type="text" class="form-control" size="16" name="reagen[]"></td><td><input type="text" class="form-control" size="16" name="hasil[]"></td></tr>';
}
</script>
@endsection
