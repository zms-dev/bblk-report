@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Status Pendaftaran</div>

                <div class="panel-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Parameter Terdaftar</th>
                        <th>Siklus</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody class="body-data">
                      @if(count($daftar))
                      <?php $no = 0; ?>
                      @foreach($daftar as $val)
                      <?php $no++ ?>
                      <tr>
                        <td>{{$no}}</td>
                        <td>{{$val->bidang}}<br>{{$val->parameter}}</td>
                        <td>{{$val->siklus}}</td>
                        <td>
                          @if($val->status == 1)
                            <font style="color: red">Menunggu verifikasi Pembayaran dari Keuangan BBLK Jakarta</font>
                          @elseif($val->status == 2)
                            <font style="color: blue">Kirim Bahan</font>
                          @elseif($val->status == 3)
                            <font style="color: blue">Input Hasil</font>
                          @endif
                        </td>
                      </tr>
                      @endforeach
                      @endif
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection