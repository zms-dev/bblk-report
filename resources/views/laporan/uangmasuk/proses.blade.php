<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
body{
	font-family: arial;
	font-size: 14px;
}
table, td, th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 15px;
}	
</style>
<body>
<h2>Laporan Penerimaan Uang Masuk {{$input['tahun']}} </h2>
<table width="100%" id="peserta_pme">
	<thead>
		<tr class="titlerowna">
			<th rowspan="2">No</th>
			<th rowspan="2">Nama Instansi</th>
			<th rowspan="2">Alamat</th>
			<th rowspan="2">No. Telpon</th>
			<th rowspan="2">Personal</th>
			<th rowspan="2">No. HP</th>
			<th rowspan="2">Email</th>
			<th colspan="3"><center>Patologi Klinik</center></th>
			<th colspan="2"><center>Mikrobiologi</center></th>
			<th colspan="5"><center>Immunologi</center></th>
			<th rowspan="2">Total (Rp)</th>
		</tr>
		<tr class="titlerowna">
			<th>H (Rp)</th>
			<th>KK (Rp)</th>
			<th>Urinalisa (Rp)</th>
			<th>BTA (Rp)</th>
			<th>TC (Rp)</th>
			<th>A&nbsp;HIV (Rp)</th>
			<th>Syph (Rp)</th>
			<th>HBsAg (Rp)</th>
			<th>A&nbsp;HCV (Rp)</th>
			<th>Paket (Rp)</th>
		</tr>
	</thead>
	<tbody>
		@if(count($data))
		<?php $no = 0; ?>
		@foreach($data as $val)
		<?php 
			$no++;
		?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->Nama}}</td>
			<td>{{$val->alamat}}</td>
			<td>{{$val->Telp}}</td>
			<td>{{$val->personal}}</td>
			<td>{{$val->no_hp}}</td>
			<td>{{$val->email}}</td>
			<td style="text-align: right;">
				@if(count($val->h))
				{{number_format($val->h[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->kk))
				{{number_format($val->kk[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->uri))
				{{number_format($val->uri[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->bta))
				{{number_format($val->bta[0]->tarif)}}
				@endif
			</td>
			<td style="text-align: right;">
				@if(count($val->tc))
				{{number_format($val->tc[0]->tarif)}}
				@endif
			</td>
			<?php $paket = 0; ?>
			@if($val->ahiv[0]->tarif != NULL && $val->syph[0]->tarif != NULL && $val->hbsag[0]->tarif != NULL && $val->ahcv[0]->tarif != NULL)
				<td style="text-align: right;">
					0
				</td>
				<td style="text-align: right;">
					0
				</td>
				<td style="text-align: right;">
					0
				</td>
				<td style="text-align: right;">
					0
				</td>
				<td style="text-align: right;">
					<?php 
						$paket = $val->ahiv[0]->tarif + $val->syph[0]->tarif + $val->hbsag[0]->tarif + $val->ahcv[0]->tarif;
						if($paket > 4000000){
							$paket = 4000000;
						}else{
							$paket = 2000000;
						}
					?>
					@if(count($val->ahcv))
					{{number_format($paket)}}
					@endif
				</td>
			@else
				<td style="text-align: right;">
					@if(count($val->ahiv))
					{{number_format($val->ahiv[0]->tarif)}}
					@endif
				</td>
				<td style="text-align: right;">
					@if(count($val->syph))
					{{number_format($val->syph[0]->tarif)}}
					@endif
				</td>
				<td style="text-align: right;">
					@if(count($val->hbsag))
					{{number_format($val->hbsag[0]->tarif)}}
					@endif
				</td>
				<td style="text-align: right;">
					@if(count($val->ahcv))
					{{number_format($val->ahcv[0]->tarif)}}
					@endif
				</td>
				<td style="text-align: right;">
					0
				</td>
			@endif
			<td style="text-align: right;">
				@if(count($val->tot))
					@if($paket == 4000000)
						{{number_format($val->tot[0]->tarif - 400000)}}
					@elseif($paket < 4000000 && $paket >= 2000000)
						{{number_format($val->tot[0]->tarif - 200000)}}
					@else
						{{number_format($val->tot[0]->tarif)}}
					@endif
				@endif
			</td>
			<td class="colomngitung" style="display: none;">
				@if(count($val->tot))
					@if($paket > 4000000)
						{{$val->tot[0]->tarif - 400000}}
					@elseif($paket <= 4000000 && $paket > 2000000)
						{{$val->tot[0]->tarif - 200000}}
					@else
						{{$val->tot[0]->tarif}}
					@endif
				@endif
			</td>
		</tr>
		<?php unset($hema, $kk, $uri, $bta, $tc, $ahiv, $ahcv, $syph, $hbsag, $paket); ?>
		@endforeach
		@endif
		<tr>
			<td colspan="17" style="text-align: right;">Total (Rp) :</td>
			<td class="hasil"></td>
		</tr>
	</tbody>
</table>
</body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script>
var total2=[0];
$(document).ready(function(){
    var $dataRows=$("#peserta_pme tr:not('.titlerowna')");
    $dataRows.each(function() {
        $(this).find('.colomngitung').each(function(i){        
            total2[i]+=parseInt( $(this).html());
        });
    });
    $("#peserta_pme td.hasil").each(function(i){  
        $(this).html(total2[i].toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        $('.hasil').html(total2.toFixed().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        // $(".hasil").html(total2);
    });
});
</script>