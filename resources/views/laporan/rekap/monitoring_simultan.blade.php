@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Monitoring Sipamela</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="uangmasuk" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun" id="tahun">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div><br>
                      
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                              <th rowspan="2">No</th>
                              <th rowspan="2">Parameter</th>
                              <th rowspan="2">Kuota</th>
                              <th rowspan="2">Daftar Online</th>
                              <th colspan="2">Verifikasi Keuangan</th>
                              <th colspan="2">Kirim Bahan</th>
                              <th colspan="2">Entri Hasil</th>
                              <th colspan="2">Evaluasi</th>
                          </tr>
                          <tr>
                            <th>Sudah</th>
                            <th>Belum</th>
                            <th>Sudah</th>
                            <th>Belum</th>
                            <th>Sudah</th>
                            <th>Belum</th>
                            <th>Sudah</th>
                            <th>Belum</th>
                          </tr>
                        </thead>
                        <tbody class="body-siklus">
                          
                        </tbody>
                        <tbody class="body-total">
                          
                        </tbody>
                      </table><br>

                      {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});


$("#tahun").change(function(){
  var val = $(this).val(), i, no = 0
  $(".body-siklus").html("<tr><td colspan='12'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('monitoringsimultan').'/'}}"+val,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus").html("");
      if(addr.Hasil != undefined){
        var no = 0;
        var i = 0;
        var kuo = ['280','240','155','110','75','150','150','150','150','40','20','35','35'];
        var total = i;
        var daf = no;
        var vers = no;
        var verb = no;
        var kirs = no;
        var kirb = no;
        var cets = no;
        var cetb = no;
        var evals = no;
        $.each(addr.Hasil,function(e,item){
          no++;
            var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.alias+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kuo[i]+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.jumlah+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.verifs+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.verifb+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.kirims+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.kirimb+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.cetaks+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.cetakb+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.evaluasis+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.cetaks+"</td>";
            $(".body-siklus").append(html);
            total = '1590';
            daf = daf + item.jumlah;
            vers = vers + item.verifs;
            verb = verb + item.verifb;
            kirs = kirs + item.kirims;
            kirb = kirb + item.kirimb;
            cets = cets + item.cetaks;
            cetb = cetb + item.cetakb;
            evals = evals + item.evaluasis;
            i++;
        })
        var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+total+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+daf+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+vers+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+verb+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kirs+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kirb+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+cets+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+cetb+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+evals+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+cets+"</td>";
        $(".body-total").append(htm);
      }
      return false;
    }
  });
});
</script>
@endsection