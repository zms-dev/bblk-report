<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table, table td, table th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

table th, table td {
    padding: 5px;
}
</style>
<table>
	<thead>
		<tr>
			<th colspan="12" style="text-align: center; font-size: 16px">Rekapitulasi Peserta PME Berdasarkan Jenis Instansi dan Parameter Tahun {{$tahun}}</th>
		</tr>
		<tr>
			<th>No</th>
	        <th>Parameter</th>
	        <th>BBLK</th>
	        <th>BLK</th>
	        <th>RS-Pemerintah</th>
	        <th>Puskesmas</th>
	        <th>Labkesda</th>
	        <th>Rs-Swasta</th>
	        <th>LabKlinik Swasta</th>
	        <th>UTD-RS</th>
	        <th>UTD-PMI</th>
	        <th>RS TNI/Polri</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$no = 0;
			$parameter = 0;
			$bblk = 0;
			$blk = 0;
			$rspem = 0;
			$pus = 0;
			$labkes = 0;
			$rsswa = 0;
			$labkl = 0;
			$utdrs = 0;
			$utdpmi = 0;
			$rstni = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$parameter = $parameter + $val->parameter;
			$bblk = $bblk + $val->bblk;
			$blk = $blk + $val->blk;
			$rspem = $rspem + $val->rspem;
			$pus = $pus + $val->pus;
			$labkes = $labkes + $val->labkes;
			$rsswa = $rsswa + $val->rsswa;
			$labkl = $labkl + $val->labkl;
			$utdrs = $utdrs + $val->utdrs;
			$utdpmi = $utdpmi + $val->utdpmi;
			$rstni = $rstni + $val->rstni;
		?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->alias}}</td>
			<td>{{$val->bblk}}</td>
			<td>{{$val->blk}}</td>
			<td>{{$val->rspem}}</td>
			<td>{{$val->pus}}</td>
			<td>{{$val->labkes}}</td>
			<td>{{$val->rsswa}}</td>
			<td>{{$val->labkl}}</td>
			<td>{{$val->utdrs}}</td>
			<td>{{$val->utdpmi}}</td>
			<td>{{$val->rstni}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td>Total :</td>
			<td>{{$bblk}}</td>
			<td>{{$blk}}</td>
			<td>{{$rspem}}</td>
			<td>{{$pus}}</td>
			<td>{{$labkes}}</td>
			<td>{{$rsswa}}</td>
			<td>{{$labkl}}</td>
			<td>{{$utdrs}}</td>
			<td>{{$utdpmi}}</td>
			<td>{{$rstni}}</td>
		</tr>
	</tbody>
</table>