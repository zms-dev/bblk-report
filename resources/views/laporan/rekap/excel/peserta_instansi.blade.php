@if ($request->export != "Export Excel")
<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table, table td, table th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

table th, table td {
    padding: 5px;
}
</style>
@endif
<table>
	<thead>
		<tr>
			<th colspan="4" style="text-align: center; font-size: 16px">Rekapitulasi Peserta PME Berdasarkan Jenis Instansi Tahun {{$tahun}}</th>
		</tr>
		<tr>
			<th>No</th>
			<th>Instansi</th>
			<th>Peserta</th>
			<th>Nominal (Rp)</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$no = 0; 
			$peserta = 0;
			$jumlah = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$peserta = $peserta + $val->peserta;
			$jumlah = $jumlah + $val->jumlah;
		?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->badan_usaha}}</td>
			<td style="text-align: right;">{{$val->peserta}}</td>
			<td style="text-align: right;">{{number_format($val->jumlah)}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td>Total :</td>
			<td style="text-align: right;">{{$peserta}}</td>
			<td style="text-align: right;">{{number_format($jumlah)}}</td>
		</tr>
	</tbody>
</table>