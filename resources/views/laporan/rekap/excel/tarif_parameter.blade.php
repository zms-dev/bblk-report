@if($request->export != "Export Excel")
<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 9px;
}
table, table td, table th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

table th, table td {
    padding: 5px;
}
</style>
@endif
<table>
	<thead>
		<tr>
			<th colspan="12" style="text-align: center; font-size: 14px">Rekapitulasi Peserta PME Berdasarkan Jenis Instansi dan Parameter Tahun {{$tahun}}</th>
		</tr>
      <tr>
      	  <th style="{{($request->export != "Export Excel" ? "width:5%;" : "")}}">No</th>
          <th>Parameter</th>
		  @foreach($badan_usaha as $key => $r)
		  	@php
				$total[$r->id] = 0;
			@endphp
			<th style="text-align:center;{{($request->export != "Export Excel" ? "width:5%;" : "")}}">{{str_replace(" ","",$r->alias)}}</th>
		  @endforeach
      </tr>
	</thead>
	<tbody>
		<?php 
			$no = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
		?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->alias}}</td>
			@foreach($badan_usaha as $key => $r)
				@php
					$vkey = "sum_".$r->id;
					$total[$r->id] = $total[$r->id] + $val->$vkey;
				@endphp
				<td style="text-align: right;">{{number_format($val->$vkey)}}</td>
			@endforeach
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td>Total :</td>
			@foreach($badan_usaha as $key => $r)
				@php
					$vkey = "sum_".$r->id;
				@endphp
				<td style="text-align: right;">{{number_format($total[$r->id])}}</td>
			@endforeach
		</tr>
	</tbody>
</table>