@if ($request->export != "Export Excel")
<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table, table td, table th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

table th, table td {
    padding: 5px;
}
</style>
@endif
<table>
	<thead>
		<tr>
			<th colspan="13" style="text-align: center; font-size: 16px">Rekap Peserta Berdasarkan Provinsi Tahun {{$tahun}}</th>
		</tr>
      <tr>
	        <th>No</th>
	        <th>Provinsi</th>
	        <th>Jml.Peserta</th>
	        <th>BLK</th>
	        <th>BBLK</th>
	        <th>RS - PEM</th>
	        <th>PUSKESMAS</th>
	        <th>LABKESDA</th>
	        <th>RS - SWASTA</th>
	        <th>LABKLINIK</th>
	        <th>UTD - RS</th>
	        <th>UTD - PMI</th>
	        <th>RP - TNI</th>
      </tr>
	</thead>
	<tbody>
		<?php 
			$no = 0; 
			$peserta = 0;
			$COUNT_1 = 0;
			$COUNT_2 = 0;
			$COUNT_3 = 0;
			$COUNT_4 = 0;
			$COUNT_5 = 0;
			$COUNT_6 = 0;
			$COUNT_7 = 0;
			$COUNT_8 = 0;
			$COUNT_9 = 0;
			$COUNT_10 = 0;
		?>
		@foreach($data as $val)
		<?php 
			$no++ ;
			$peserta = $peserta + $val->COUNT_PESERTA;
			$COUNT_1 = $COUNT_1 + $val->COUNT_1;
			$COUNT_2 = $COUNT_2 + $val->COUNT_2;
			$COUNT_3 = $COUNT_3 + $val->COUNT_3;
			$COUNT_4 = $COUNT_4 + $val->COUNT_4;
			$COUNT_5 = $COUNT_5 + $val->COUNT_5;
			$COUNT_6 = $COUNT_6 + $val->COUNT_6;
			$COUNT_7 = $COUNT_7 + $val->COUNT_7;
			$COUNT_8 = $COUNT_8 + $val->COUNT_8;
			$COUNT_9 = $COUNT_9 + $val->COUNT_9;
			$COUNT_10 = $COUNT_10 + $val->COUNT_10;
		?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->name}}</td>
			<td>{{$val->COUNT_PESERTA}}</td>
			<td>{{$val->COUNT_1}}</td>
			<td>{{$val->COUNT_2}}</td>
			<td>{{$val->COUNT_3}}</td>
			<td>{{$val->COUNT_4}}</td>
			<td>{{$val->COUNT_5}}</td>
			<td>{{$val->COUNT_6}}</td>
			<td>{{$val->COUNT_7}}</td>
			<td>{{$val->COUNT_8}}</td>
			<td>{{$val->COUNT_9}}</td>
			<td>{{$val->COUNT_10}}</td>
		</tr>
		@endforeach
		<tr>
			<td></td>
			<td>Total :</td>
			<td>{{$peserta}}</td>
			<td>{{$COUNT_1}}</td>
			<td>{{$COUNT_2}}</td>
			<td>{{$COUNT_3}}</td>
			<td>{{$COUNT_4}}</td>
			<td>{{$COUNT_5}}</td>
			<td>{{$COUNT_6}}</td>
			<td>{{$COUNT_7}}</td>
			<td>{{$COUNT_8}}</td>
			<td>{{$COUNT_9}}</td>
			<td>{{$COUNT_10}}</td>
		</tr>
	</tbody>
</table>