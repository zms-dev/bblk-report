@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Peserta Per Parameter Berdasarkan Provinsi</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="uangmasuk" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select name="siklus" class="form-control" required id="siklus">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="12">1 & 2</option>
                          </select>
                      </div><br>
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun" id="tahun" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div><br>
                      <div>
                        <input type="submit" name="export" value="Export Excel" class="btn btn-primary">&nbsp;
                        <input type="submit" name="export" value="Export PDF" class="btn btn-primary">
                      </div><br>
                      
                      <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                                <th>No</th>
                                <th>Provinsi</th>
                                <th>Jml. Peserta</th>
                                <th>HEM</th>
                                <th>KKL</th>
                                <th>URI</th>
                                <th>BTA</th>
                                <th>TCC</th>
                                <th>HIV</th>
                                <th>SIF</th>
                                <th>HBS</th>
                                <th>HCV</th>
                                <th>Total Parameter</th>
                            </tr>
                          </thead>
                          <tbody class="body-siklus">
                            
                          </tbody>
                          <tbody class="body-total">
                            
                          </tbody>
                        </table>
                      </div>
                      <br>

                      {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});


$("#tahun").change(function(){
  var val = $(this).val(), siklus = $("#siklus").val(), i, no = 0
  $(".body-siklus").html("<tr><td colspan='15'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('datapesertaprovinsi').'/'}}"+val+"?x="+siklus,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus").html("");
      if(addr.Hasil != undefined){
        var no = 0;
        var peserta = no;
        var hem = no;
        var kkl = no;
        var uri = no;
        var bta = no;
        var tcc = no;
        var hiv = no;
        var sif = no;
        var hbs = no;
        var hcv = no;
        var tot = no;
        $(".body-total").html("");
        $.each(addr.Hasil,function(e,item){
          no++;
            var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_PESERTA+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_HEM+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_KKL+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_URI+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_BTA+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_TCC+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_HIV+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_SIF+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_HBS+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_HCV+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_PARAMETER+"</td>";
            $(".body-siklus").append(html);
            peserta = peserta + item.COUNT_PESERTA;
            hem = hem + item.COUNT_HEM;
            kkl = kkl + item.COUNT_KKL;
            uri = uri + item.COUNT_URI;
            bta = bta + item.COUNT_BTA;
            tcc = tcc + item.COUNT_TCC;
            hiv = hiv + item.COUNT_HIV;
            sif = sif + item.COUNT_SIF;
            hbs = hbs + item.COUNT_HBS;
            hcv = hcv + item.COUNT_HCV;
            tot = tot + item.COUNT_PARAMETER;
        })
        var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+peserta+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hem+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+kkl+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+uri+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+bta+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+tcc+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hiv+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+sif+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hbs+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+hcv+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+tot+"</td>";
        $(".body-total").append(htm);
      }
      return false;
    }
  });
});

</script>
@endsection