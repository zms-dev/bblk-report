@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Rekapitulasi Peserta PME Berdasarkan Jenis Instansi dan Parameter</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="uangmasuk" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select name="siklus" class="form-control" required id="siklus">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="12">1 & 2</option>
                          </select>
                      </div><br>
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun" id="tahun" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div><br>
                      <div>
                        <input type="submit" name="export" value="Export Excel" class="btn btn-primary">&nbsp;
                        <input type="submit" name="export" value="Export PDF" class="btn btn-primary">
                      </div><br>
                      
                      <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                                <th>Parameter</th>
                                <th>BBLK</th>
                                <th>BLK</th>
                                <th>RS-Pemerintah</th>
                                <th>Puskesmas</th>
                                <th>Labkesda</th>
                                <th>Rs-Swasta</th>
                                <th>LabKlinik Swasta</th>
                                <th>UTD-RS</th>
                                <th>UTD-PMI</th>
                                <th>RS TNI/Polri</th>
                            </tr>
                          </thead>
                          <tbody class="body-siklus">
                            
                          </tbody>
                          <tbody class="body-total">
                            
                          </tbody>
                        </table>
                      </div>
                      <br>

                      {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});


$("#tahun").change(function(){
  var val = $(this).val(), siklus = $("#siklus").val(), i, no = 0
  $(".body-siklus").html("<tr><td colspan='11'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('datapesertaparameter').'/'}}"+val+"?x="+siklus,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus").html("");
      if(addr.Hasil != undefined){
        var no = 0;
        var bblk = no;
        var blk = no;
        var rspem = no;
        var pus = no;
        var labkes = no;
        var rsswa = no;
        var labkl = no;
        var utdrs = no;
        var utdpmi = no;
        var rstni = no;
        $.each(addr.Hasil,function(e,item){
          no++;
            var html = "<tr><td class=\"paramsiklus\">"+item.alias+"</td><td style=\"text-align: right;\">"+item.bblk+"</td><td style=\"text-align: right;\">"+item.blk+"</td><td style=\"text-align: right;\">"+item.rspem+"</td><td style=\"text-align: right;\">"+item.pus+"</td><td style=\"text-align: right;\">"+item.labkes+"</td><td style=\"text-align: right;\">"+item.rsswa+"</td><td style=\"text-align: right;\">"+item.labkl+"</td><td style=\"text-align: right;\">"+item.utdrs+"</td><td style=\"text-align: right;\">"+item.utdpmi+"</td><td style=\"text-align: right;\">"+item.rstni+"</td>";
            $(".body-siklus").append(html);
            bblk = bblk + item.bblk;
            blk = blk + item.blk;
            rspem = rspem + item.rspem;
            pus = pus + item.pus;
            labkes = labkes + item.labkes;
            rsswa = rsswa + item.rsswa;
            labkl = labkl + item.labkl;
            utdrs = utdrs + item.utdrs;
            utdpmi = utdpmi + item.utdpmi;
            rstni = rstni + item.rstni;
        })
        var htm = "<tr><td class=\"nosiklus\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+bblk+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+blk+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+rspem+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+pus+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+labkes+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+rsswa+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+labkl+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+utdrs+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+utdpmi+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+rstni+"</td>";
        $(".body-total").append(htm);
      }
      return false;
    }
  });
});
</script>
@endsection