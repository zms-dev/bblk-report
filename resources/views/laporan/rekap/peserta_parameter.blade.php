@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Peserta Berdasarkan Parameter</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="uangmasuk" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select name="siklus" class="form-control" required id="siklus">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="12">1 & 2</option>
                          </select>
                      </div><br>
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun" id="tahun" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div><br>
                      <div>
                        <input type="submit" name="export" value="Export Excel" class="btn btn-primary">&nbsp;
                        <input type="submit" name="export" value="Export PDF" class="btn btn-primary">
                      </div><br>

                      <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                                <th>No</th>
                                <th>Parameter</th>
                                <th>Jumlah Peserta</th>
                            </tr>
                          </thead>
                          <tbody class="body-siklus">
                            
                          </tbody>
                          <tbody class="body-total">
                            
                          </tbody>
                        </table>
                      </div>
                      <br>

                      {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});


$("#tahun").change(function(){
  var val = $(this).val(), siklus = $("#siklus").val(), i, no = 0
  $(".body-siklus").html("<tr><td colspan='3'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('dataparameter').'/'}}"+val+"?x="+siklus,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus").html("");
      if(addr.Hasil != undefined){
        var no = 0;
        var total = no;
        $.each(addr.Hasil,function(e,item){
          no++;
            var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.bidang+"<br>"+item.parameter+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.jumlah+"</td>";
            $(".body-siklus").append(html);
            total = total + item.jumlah;
        })
        var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+total+"</td>";
        $(".body-total").append(htm);
      }
      return false;
    }
  });
});
</script>
@endsection