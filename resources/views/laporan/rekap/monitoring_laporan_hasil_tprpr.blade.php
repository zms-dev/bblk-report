@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Monitoring Pelaporan Kirim Hasil </div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="uangmasuk" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select name="siklus" id="siklus" class="form-control">
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div><br>
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun" id="tahun">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div><br>
                      
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                              <th><center>No</center></th>
                              <th><center>Parameter</center></th>
                              <th><center>Jumlah Peserta</center></th>
                              <th><center>Belum Input Hasil</center></th>
                              <th><center>Input Hasil</center></th>
                              <th><center>Kirim Hasil</center></th>
                              <th><center>Belum Evaluasi</center></th>
                              <th><center>Sudah Evaluasi</center></th>
                          </tr> 
                        </thead>
                        <tbody class="body-siklus">
                          
                        </tbody>
                        <tbody class="body-total">
                          
                        </tbody>
                      </table><br>

                      {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});


$("#tahun").change(function(){
  var val = $(this).val(), i, no = 0, siklus = $("#siklus").val()
  $(".body-siklus").html("<tr><td colspan='12'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('monitoringsimultan')}}"+ {{Auth::user()->penyelenggara}} +"/"+ val+"?y="+ siklus,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus").html("");
      if(addr.Hasil != undefined){
        var no = 0;
        var i = 0;
        var kuo = ['280','240','155','110','75','150','150','150','150','40','20','35','35'];
        var total = i;
        var daf = no;
        var cets = no;
        var cetb = no;
        var ceta = no;
        var evals = no;
        var evals = no;
        $.each(addr.Hasil,function(e,item){
          no++;
            var html = "<tr><td class=\"nosiklus\"><center>"+no+"</center></td><td class=\"paramsiklus\">"+item.alias+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.jumlah+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.beluminput1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.input1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahinput1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+(item.sudahinput1 - item.sudahevaluasi)+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.sudahevaluasi+"</td>";
            $(".body-siklus").append(html);
            total = '1590';
            daf = daf + item.jumlah;
            cets = cets + item.cetaks;
            cetb = cetb + item.cetakb;
            ceta = ceta + item.cetaka;
            evals = evals + item.evaluasis;
            i++;
        })
        // var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+daf+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+cets+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+ceta+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+cetb+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+evals+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+cets+"</td>";
        // $(".body-total").append(htm);
      }
      return false;
    }
  });
});
</script>
@endsection