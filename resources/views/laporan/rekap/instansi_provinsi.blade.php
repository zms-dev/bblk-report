@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Instansi Per Parameter Berdasarkan Provinsi</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="uangmasuk" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select name="siklus" class="form-control" required id="siklus">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="12">1 & 2</option>
                          </select>
                      </div><br>
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun" id="tahun" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div><br>
                      <div>
                        <input type="submit" name="export" value="Export Excel" class="btn btn-primary">&nbsp;
                        <input type="submit" name="export" value="Export PDF" class="btn btn-primary">
                      </div><br>
                      
                      <div class="table-responsive">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                                <th>No</th>
                                <th>Provinsi</th>
                                <th>Jml. Peserta</th>
                                <th>BLK</th>
                                <th>BBLK</th>
                                <th>RS - PEM</th>
                                <th>PUSKESMAS</th>
                                <th>LABKESDA</th>
                                <th>RS - SWASTA</th>
                                <th>LABKLINIK</th>
                                <th>UTD - RS</th>
                                <th>UTD - PMI</th>
                                <th>RP - TNI</th>
                            </tr>
                          </thead>
                          <tbody class="body-siklus">
                            
                          </tbody>
                          <tbody class="body-total">
                            
                          </tbody>
                        </table>
                      </div>
                      <br>

                      {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});


$("#tahun").change(function(){
  var val = $(this).val(), siklus = $("#siklus").val(), i, no = 0
  $(".body-siklus").html("<tr><td colspan='15'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('datainstansiprovinsi').'/'}}"+val+"?x="+siklus,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus").html("");
      if(addr.Hasil != undefined){
        var no = 0;
        var peserta = no;
        var COUNT_1 = no;
        var COUNT_2 = no;
        var COUNT_3 = no;
        var COUNT_4 = no;
        var COUNT_5 = no;
        var COUNT_6 = no;
        var COUNT_7 = no;
        var COUNT_8 = no;
        var COUNT_9 = no;
        var COUNT_10 = no;
        $(".body-total").html("");
        $.each(addr.Hasil,function(e,item){
          no++;
            var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_PESERTA+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_2+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_3+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_4+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_5+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_6+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_7+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_8+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_9+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+item.COUNT_10+"</td>";
            $(".body-siklus").append(html);
            peserta = peserta + item.COUNT_PESERTA;
            COUNT_1 = COUNT_1 + item.COUNT_1;
            COUNT_2 = COUNT_2 + item.COUNT_2;
            COUNT_3 = COUNT_3 + item.COUNT_3;
            COUNT_4 = COUNT_4 + item.COUNT_4;
            COUNT_5 = COUNT_5 + item.COUNT_5;
            COUNT_6 = COUNT_6 + item.COUNT_6;
            COUNT_7 = COUNT_7 + item.COUNT_7;
            COUNT_8 = COUNT_8 + item.COUNT_8;
            COUNT_9 = COUNT_9 + item.COUNT_9;
            COUNT_10 = COUNT_10 + item.COUNT_10;
        })
        var htm = "<tr><td class=\"nosiklus\" colspan=\"2\" style=\"text-align: right\">Total</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+peserta+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+COUNT_1+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+COUNT_2+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+COUNT_3+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+COUNT_4+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+COUNT_5+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+COUNT_6+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+COUNT_7+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+COUNT_8+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+COUNT_9+"</td><td class=\"bidangsiklus\" style=\"text-align: right;\">"+COUNT_10+"</td>";
        $(".body-total").append(htm);
      }
      return false;
    }
  });
});
</script>
@endsection