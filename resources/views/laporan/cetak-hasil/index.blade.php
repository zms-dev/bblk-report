@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Cetak Hasil Peserta</div>

                <div class="panel-body">
                    <form method="get">                    
                        <div>
                            <label for="exampleInputEmail1">Tahun</label>
                            <div class="controls input-append date form_datetime" data-link-field="dtp_input1">
                                <input size="16" type="text" value="{{(!empty($request->query('tahun')) ? $request->query('tahun') : 2018)}}" readonly class="form-control coba" name="tahun" id="tahun" required=true>
                                <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </div>        
                        <br>
                        <div>
                            <label for="exampleInputEmail1">Siklus</label>
                            <div class="controls">
                                <select name="siklus" class="form-control" required=true>
                                    <option value="">Pilih Silkus</option>
                                    <option value="1" {{($request->query('siklus') == 1? 'selected="selected"' : '')}}>1</option> 
                                    <option value="2" {{($request->query('siklus') == 2? 'selected="selected"' : '')}}>2</option> 
                                </select>
                            </div>
                        </div>                  
                        <br>
                        <div>
                            <label for="exampleInputEmail1">Bidang</label>
                            <div class="controls">
                                <select name="sub_bidang_id" class="form-control" required=true>
                                    <option value="">Pilih Bidang</option>
                                    @foreach($bidang as $r)
                                        <option value="{{$r->id}}" {{($request->query('sub_bidang_id') == $r->id ? 'selected="selected"' : '')}}>{{$r->alias.' - '.$r->parameter}}</option> 
                                    @endforeach
                                </select>
                            </div>
                        </div>       
                        <br>
                        <div>
                            <button type="submit" class="btn btn-primary">Proses</button>
                        </div>
                    </form>
                    <br>
                    <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                            <tr>
                                <th>Kode Lab</th>
                                <th>Nama Peserta</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scriptBlock')
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});
var table = $(".dataTables-data");
var dataTable = table.DataTable({
responsive:!0,
"serverSide":true,
"processing":true,
"ajax":{
    url : "{{url('/laporan-cetak-hasil')}}?tahun={{$request->query('tahun')}}&siklus={{$request->query('siklus')}}&sub_bidang_id={{$request->query('sub_bidang_id')}}"
},
dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
language:{
    paginate:{
        previous:"&laquo;",
        next:"&raquo;"
    },search:"_INPUT_",
    searchPlaceholder:"Search..."
},
"columns":[
        {"data":"kode_lebpes","name":"tb_registrasi.kode_lebpes","searchable":true,"orderable":true,"widt":'20%'},
        {"data":"nama_lab","name":"perusahaan.nama_lab","searchable":true,"orderable":true},
        {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "20%"},
],
order:[[1,"asc"]]
});
</script>
@endsection
