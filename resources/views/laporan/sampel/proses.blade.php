<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
body{
	font-family: arial;
	font-size: 14px;
}
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #4CAF50;
    color: white;
}
</style>
<body>
<h2>Laporan Penerimaan Sampel {{$input['tahun']}} Siklus {{$input['siklus']}}</h2>
<table width="100%">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode Peserta</th>
			<th>Nama</th>
			<th>Bidang</th>
			<th>Alamat</th>
		</tr>
	</thead>
	<tbody>
		@if(count($data))
		<?php $no = 0; ?>
		@foreach($data as $val)
		<?php $no++;?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->kode_lebpes}}</td>
			<td>{{$val->Nama}}</td>
			<td>{{$val->Bidang}}</td>
			<td>{{$val->Provinsi}} - {{$val->Kota}} - {{$val->Alamat}}</td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>
</body>
</html>