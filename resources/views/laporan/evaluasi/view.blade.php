<table width="100%">
	<thead>
		<tr>
			<td colspan="5">
				<h2>Laporan Evaluasi {{$input['tahun']}} Siklus {{$input['siklus']}}</h2>
			</td>
		</tr>
		<tr>
			<th>No</th>
			<th>Kode Peserta</th>
			<th>Nama</th>
			<th>Bidang</th>
			<th>Download Hasil</th>
		</tr>
	</thead>
	<tbody>
		@if(count($data))
		<?php $no = 0; ?>
		@foreach($data as $val)
		<?php $no++; ?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->kode_lebpes}}</td>
			<td>{{$val->nama_lab}}</td>
			<td>{{$val->Bidang}}</td>
			<td>
	            <a href="{{URL::asset('asset/backend/juklak').'/'.$val->file}}" download>
	                Download
	            </a>
	        </td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>