<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
body{
	font-family: arial;
	font-size: 14px;
}
table, td, th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 5px;
}
</style>
<body>
<h2>Data Peserta {{$input['tahun']}}</h2>
<table id="peserta_pme">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode Peserta</th>
			<th>Parameter</th>
			<th>Nomor TU</th>
			<th>Nama Instansi</th>
			<th>Nama PJ Lab</th>
			<th>Nomer HP PJ lab</th>
			<th>Alamat Lab</th>
			<th>Kabupaten</th>
			<th>Propinsi</th>
			<th>Telp</th>
			<th>Email</th>
		</tr>
	</thead>
	<tbody>
		@if(count($data))
		<?php $no = 0; ?>
		@foreach($data as $val)
		<?php $no++;?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->kode_lebpes}}</td>
			<td>
				@if($val->bidang == '6')
					@if($val->pemerintah == '9' || $val->pemerintah == '8')
						HIV PMI
					@else
						{{$val->alias}}
					@endif
				@else
					{{$val->alias}}
				@endif
			</td>
			<td class="ada">{{substr($val->kode_lebpes,0, -9)}}PME/12{{substr($val->kode_lebpes, 11)}}</td>
			<td>{{$val->nama_lab}}</td>
			<td>{{$val->penanggung_jawab}}</td>
			<td>{{$val->no_hp}}</td>
			<td>{{$val->alamat}}</td>
			<td>{{$val->Kota}}</td>
			<td>{{$val->Provinsi}}</td>
			<td>{{$val->telp}}</td>
			<td>{{$val->email}}</td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   for (var i = 12; i >= 4; i--) {
	   $("#peserta_pme tr td:nth-child("+i+")").each(function() { //for each first td in every tr
	      var $this = $(this);
	      if ($this.text() == prevTDVal) { // check value of previous td text
	         span++;
	         if (prevTD != "") {
	            prevTD.attr("rowspan", span); // add attribute to previous td
	            $this.remove(); // remove current td
	         }
	      } else {
	         prevTD     = $this; // store current td 
	         prevTDVal  = $this.text();
	         span       = 1;
	      }
	   });
   }
});
</script>
</html>