<table id="peserta_pme" border="1">
	<thead>
		<tr>
			<th colspan="12" style="text-align: center;">Data Peserta {{$input['tahun']}}</th>
		</tr>
		<tr>
			<th>No</th>
			<th>Kode Peserta</th>
			<th>Parameter</th>
			<th>Nomor TU</th>
			<th>Nama Instansi</th>
			<th>Nama PJ Lab</th>
			<th>Nomer HP PJ lab</th>
			<th>Alamat Lab</th>
			<th>Kabupaten</th>
			<th>Propinsi</th>
			<th>Telp</th>
			<th>Email</th>
		</tr>
	</thead>
	<tbody>
		@if(count($data))
		<?php $no = 0; ?>
		@foreach($data as $val)
		<?php $no++;?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->kode_lebpes}}</td>
			<td>
				@if($val->bidang == '6')
					@if($val->pemerintah == '9' || $val->pemerintah == '8')
						HIV PMI
					@else
						{{$val->alias}}
					@endif
				@else
					{{$val->alias}}
				@endif
			</td>
			<td class="ada">{{substr($val->kode_lebpes,0, -9)}}PME/12{{substr($val->kode_lebpes, 11)}}</td>
			<td>{{$val->nama_lab}}</td>
			<td>{{$val->penanggung_jawab}}</td>
			<td>{{$val->no_hp}}</td>
			<td>{{$val->alamat}}</td>
			<td>{{$val->Kota}}</td>
			<td>{{$val->Provinsi}}</td>
			<td>{{$val->telp}}</td>
			<td>{{$val->email}}</td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>