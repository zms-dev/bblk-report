@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                  <p style="text-align: center; font-weight: bold;">TANDA TERIMA BAHAN KONTROL PENYELENGGARAAN NASIONAL <br> PEMANTAPAN MUTU EKSTERNAL SIKLUS {{$siklus}} TAHUN {{$tahun}}</p><br>
                  <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <p>Nama Laboratorium : <b>{{$perusahaan->nama_lab}}</b></p>
                    @if($hsudah == $hdata)
                    @else
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Bidang Pemeriksaan</th>
                          <th>Jumlah</th>
                          <th>Kondisi Bahan Uji</th>
                          <th>Keterangan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $no = 0;
                        foreach($data as $key => $val ){
                          if ($val->penerima == NULL) {
                            $no++;
                        ?>
                        <tr>
                          <td>{{$no}}</td>
                          <td>{{$val->parameter}}<input type="hidden" name="id_registrasi[]" value="{{$val->id}}"></td>
                          <td>
                            <select class="form-control" name="jumlah[]">
                              <option></option>  
                              <option value="2 Tabung">2 Tabung</option>
                              <option value="2 Botol">2 Botol</option>
                              <option value="3 Cryotube">3 Cryotube</option>
                              <option value="10 Slide">10 Slide</option>
                            </select>
                          </td>
                          <td>
                            <select class="form-control" name="kondisi[]">
                              <option></option>  
                              <option value="Baik">Baik</option>  
                              <option value="Kurang Baik">Kurang Baik</option>  
                              <option value="Pecah / Tumpah">Pecah / Tumpah</option>  
                            </select>
                          </td>
                          <td><textarea class="form-control" name="keterangan[]"></textarea></td>
                        </tr>
                        <?php
                          }
                        }
                        ?>
                      </tbody>
                    </table>
                    <font>Keterangan :</font>
                    
                    <table border="0" width="100%">
                      <tr>
                        <td rowspan="3">
                          <ul>
                            <li>Pilih salah satu Kondisi Bahan</li>
                            <li>Input keterangan jika ada</li>
                            <li>2 Tabung untuk Bidang Hematologi<br>
                                  2 Botol untuk Kimia Klinik dan Urinalisis<br>
                                  10 Slide untuk Mikroskopis BTA<br>
                                  3 cryotube untuk Telur Cacing, Anti HIV, Sifilis, HBsAg dan Anti HCV</li>
                          </ul>
                        </td>
                        <td>Tanggal diterima</td>
                        <td>&nbsp; : &nbsp;</td>
                        <td><input autocomplete="off" id="datepicker" width="270" class="form-control readonly" name="tanggal" required/></td>
                      </tr>
                      <tr>
                        <td>Yang menerima</td>
                        <td>&nbsp; : &nbsp;</td>
                        <td><input type="text" name="penerima" required class="form-control"></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td></td>
                        <td style="text-align: right;">
                          <input type="submit" name="simpan" value="Simpan" class="btn btn-primary" style="margin-top: 20px">
                        </td>
                      </tr>
                    </table>
                      {{ csrf_field() }}
                    @endif
                  </form><br>
                  <div class="well well-sm" style="position: relative;">
                    <p style="text-align: center; font-weight: bold;">TANDA TERIMA BAHAN KONTROL PENYELENGGARAAN NASIONAL <br> PEMANTAPAN MUTU EKSTERNAL SIKLUS {{$siklus}} TAHUN {{$tahun}}</p><br>
                  <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Bidang Pemeriksaan</th>
                          <th>Jumlah</th>
                          <th>Kondisi Bahan Uji</th>
                          <th>Keterangan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $no = 0;
                        foreach($data as $key => $val ){
                          if ($val->penerima != NULL) {
                            $no++;
                        ?>
                        <tr>
                          <td>{{$no}}</td>
                          <td>{{$val->parameter}}</td>
                          <td>{{$val->penerima->jumlah}}</td>
                          <td>{{$val->penerima->kondisi}}</td>
                          <td>{{$val->penerima->keterangan}}</td>
                        </tr>
                        <?php
                          }
                        }
                        ?>
                      </tbody>
                    </table>
                        <?php
                        foreach($data as $key => $val ){
                          if ($val->penerima != NULL) {
                        ?>
                          <table width="100%">
                            <tr>
                              <td width="60%"></td>
                              <td style="text-align: center;">Tanggal diterima, {{ _dateIndo($val->penerima->tanggal) }}</td>
                            </tr>
                            <tr>
                              <td></td>
                              <td style="text-align: center;">Yang Menerima</td>
                            </tr>
                            <tr>
                              <td height="50px"></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td></td>
                              <td style="text-align: center;">{{$val->penerima->penerima}}</td>
                            </tr>
                          </table>
                        <?php
                          break;
                          }
                        }
                        ?>
                    </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $("input:checkbox").click(function(){
    var group = "input:checkbox[value='"+$(this).val()+"']";
    console.log(group);
    $(group).prop("checked",false);
    $(this).prop("checked",true);
});

$(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap',
      format: 'yyyy-mm-dd'
    });
    $(".readonly").keydown(function(e){
        e.preventDefault();
    });
});
</script>
@endsection