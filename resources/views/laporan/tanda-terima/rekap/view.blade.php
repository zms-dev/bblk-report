<table border="1">
	<tr>
		<th rowspan="2">No</th>
		<th rowspan="2">Peserta</th>
		<th colspan="3">Hematologi</th>
		<th colspan="3">Kimia Klinik</th>
		<th colspan="3">Urinalisa</th>
		<th colspan="3">Mikroskopis BTA</th>
		<th colspan="3">Mikroskopis Telur Cacing</th>
		<th colspan="3">Anti HIV</th>
		<th colspan="3">Syphilis Anti TP</th>
		<th colspan="3">HBsAg</th>
		<th colspan="3">Anti HCV</th>
		<th rowspan="2">Tanggal Diterima</th>
		<th rowspan="2">Yang Menerima</th>
	</tr>
	<tr>
		<th></th>
		<th></th>
		<?php for ($i=0; $i < 9; $i++) { ?>
		<th>Jumlah</th>
		<th>Kondisi Bahan Uji</th>
		<th>Keterangan</th>
		<?php } ?>
		<th></th>
		<th></th>
	</tr>
	@foreach($data as $key => $val)
	<tr>
		<td>{{$key+1}}</td>
		<td>{{$val->nama_lab}}</td>
		@if($val->hem != NULL)
			<td>{{$val->hem->jumlah}}</td>
			<td>{{$val->hem->kondisi}}</td>
			<td>{{$val->hem->keterangan}}</td>
		@else
			<td>-</td>
			<td>-</td>
			<td>-</td>
		@endif
		@if($val->kk != NULL)
			<td>{{$val->kk->jumlah}}</td>
			<td>{{$val->kk->kondisi}}</td>
			<td>{{$val->kk->keterangan}}</td>
		@else
			<td>-</td>
			<td>-</td>
			<td>-</td>
		@endif
		@if($val->uri != NULL)
			<td>{{$val->uri->jumlah}}</td>
			<td>{{$val->uri->kondisi}}</td>
			<td>{{$val->uri->keterangan}}</td>
		@else
			<td>-</td>
			<td>-</td>
			<td>-</td>
		@endif
		@if($val->bta != NULL)
			<td>{{$val->bta->jumlah}}</td>
			<td>{{$val->bta->kondisi}}</td>
			<td>{{$val->bta->keterangan}}</td>
		@else
			<td>-</td>
			<td>-</td>
			<td>-</td>
		@endif
		@if($val->tc != NULL)
			<td>{{$val->tc->jumlah}}</td>
			<td>{{$val->tc->kondisi}}</td>
			<td>{{$val->tc->keterangan}}</td>
		@else
			<td>-</td>
			<td>-</td>
			<td>-</td>
		@endif
		@if($val->hiv != NULL)
			<td>{{$val->hiv->jumlah}}</td>
			<td>{{$val->hiv->kondisi}}</td>
			<td>{{$val->hiv->keterangan}}</td>
		@else
			<td>-</td>
			<td>-</td>
			<td>-</td>
		@endif
		@if($val->syp != NULL)
			<td>{{$val->syp->jumlah}}</td>
			<td>{{$val->syp->kondisi}}</td>
			<td>{{$val->syp->keterangan}}</td>
		@else
			<td>-</td>
			<td>-</td>
			<td>-</td>
		@endif
		@if($val->hbs != NULL)
			<td>{{$val->hbs->jumlah}}</td>
			<td>{{$val->hbs->kondisi}}</td>
			<td>{{$val->hbs->keterangan}}</td>
		@else
			<td>-</td>
			<td>-</td>
			<td>-</td>
		@endif
		@if($val->hcv != NULL)
			<td>{{$val->hcv->jumlah}}</td>
			<td>{{$val->hcv->kondisi}}</td>
			<td>{{$val->hcv->keterangan}}</td>
		@else
			<td>-</td>
			<td>-</td>
			<td>-</td>
		@endif
		<td>{{$val->tanggal}}</td>
		<td>{{$val->penerima}}</td>
	</tr>
	@endforeach
</table>