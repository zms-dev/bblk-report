@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                  <div class="well well-sm" style="position: relative;">
                    <p style="text-align: center; font-weight: bold;">TANDA TERIMA BAHAN KONTROL PENYELENGGARAAN NASIONAL <br> PEMANTAPAN MUTU EKSTERNAL SIKLUS {{$datas->siklus}} TAHUN {{date('Y', strtotime($datas->tanggal))}}</p><br>
                    <p>Nama Laboratorium : <b>{{$perusahaan->nama_lab}}</b></p>
                  <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Bidang Pemeriksaan</th>
                          <th>Jumlah</th>
                          <th>Kondisi Bahan Uji</th>
                          <th>Keterangan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $no = 0;
                        foreach($data as $key => $val ){
                          if ($val->penerima != NULL) {
                            $no++;
                        ?>
                        <tr>
                          <td>{{$no}}</td>
                          <td>{{$val->parameter}}</td>
                          <td>{{$val->penerima->jumlah}}</td>
                          <td>{{$val->penerima->kondisi}}</td>
                          <td>{{$val->penerima->keterangan}}</td>
                        </tr>
                        <?php
                          }
                        }
                        ?>
                      </tbody>
                    </table>
                        <?php
                        foreach($data as $key => $val ){
                          if ($val->penerima != NULL) {
                        ?>
                          <table width="100%">
                            <tr>
                              <td width="60%"></td>
                              <td style="text-align: center;">Tanggal diterima, {{ _dateIndo($val->penerima->tanggal) }}</td>
                            </tr>
                            <tr>
                              <td></td>
                              <td style="text-align: center;">Yang Menerima</td>
                            </tr>
                            <tr>
                              <td height="50px"></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td></td>
                              <td style="text-align: center;">{{$val->penerima->penerima}}</td>
                            </tr>
                          </table>
                        <?php
                          break;
                          }
                        }
                        ?>
                    </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection