@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Data Tanda Terima Peserta</div>
                <div class="panel-body">
                  <table class="table table-bordered">
                    <tr>
                      <th>No</th>
                      <th>Penerima</th>
                      <th>Siklus</th>
                      <th>Tahun</th>
                      <th style="text-align: center;">View</th>
                    </tr>
                    @foreach($data as $key => $val)
                    <tr>
                      <td>{{$key+1}}</td>
                      <td>{{$val->penerima}}</td>
                      <td>{{$val->siklus}}</td>
                      <td>{{$val->tanggal}}</td>
                      <td style="text-align: center;"><a href="{{URL('/laporan/tanda-terima/view')}}/{{$val->id}}">[ <i class="glyphicon glyphicon-eye-open"></i> ]</a></td>
                    </tr>
                    @endforeach
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection