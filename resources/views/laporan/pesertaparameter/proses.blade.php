<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
body{
	font-family: arial;
	font-size: 14px;
}
table, td, th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 5px;
}
</style>
<body>
<h2>Data Peserta {{$input['tahun']}} {{$input['parameter']}}</h2>
<table id="peserta_pme">
	<thead>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Kode Peserta</th>
			<!-- <th rowspan="2">Nomor TU</th> -->
			<th rowspan="2">Nama Instansi</th>
			<th rowspan="2">Parameter</th>
			<th rowspan="2">Siklus</th>
			<th rowspan="2">Nama PJ Lab</th>
			<th rowspan="2">Nomer HP PJ lab</th>
			<!-- <th rowspan="2">Alamat Lab</th> -->
			<!-- <th rowspan="2">Kabupaten</th> -->
			<!-- <th rowspan="2">Propinsi</th> -->
			<!-- <th rowspan="2">Telp</th> -->
			<th rowspan="2">Email</th>
			@if($input['parameter'] == '3' || $input['parameter'] == '2' || $input['parameter'] == '1')
			<th colspan="2">Siklus 1</th>
			<th colspan="2">Siklus 2</th>
			@elseif($input['parameter'] == '7')
			<th>Siklus 1</th>
			<th>Siklus 2</th>
			@else
			<th rowspan="2">Siklus 1</th>
			<th rowspan="2">Siklus 2</th>
			@endif
		</tr>
		<tr>
			@if($input['parameter'] == '7')
			<th>TP</th>
			<!-- <th>RPR</th> -->
			<th>TP</th>
			<!-- <th>RPR</th> -->
			@elseif($input['parameter'] == '3' || $input['parameter'] == '2' || $input['parameter'] == '1')
			<th>I-01</th>
			<th>I-02</th>
			<th>II-01</th>
			<th>II-02</th>
			@else
			@endif
		</tr>
	</thead>
	<tbody>
		@if(count($data))
		<?php $no = 0; ?>
		@foreach($data as $val)
		<?php $no++;?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->kode_lebpes}}</td>
			<!-- <td class="ada">{{substr($val->kode_lebpes,0, -9)}}PME/12{{substr($val->kode_lebpes, 11)}}</td> -->
			<td>{{$val->nama_lab}}</td>
			<td>
				{{$val->alias}}
			</td>
			<td>@if($val->siklus == 12) 1 & 2 @else {{$val->siklus}} @endif</td>
			<td>{{$val->penanggung_jawab}}</td>
			<td>{{$val->no_hp}}</td>
			<!-- <td>{{$val->alamat}}</td> -->
			<!-- <td>{{$val->Kota}}</td> -->
			<!-- <td>{{$val->Provinsi}}</td> -->
			<!-- <td>{{$val->telp}}</td> -->
			<td>{{$val->email}}</td>
			@if($input['parameter'] == '7')
			<td>
				@if($val->status_data1 == '2')
					Kirim Hasil
				@elseif($val->status_data1 == '1')
					Input Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			<!-- <td>
				@if($val->status_datarpr1 == '2')
					Kirim Hasil
				@elseif($val->status_datarpr1 == '1')
					Input Hasil
				@else
					Belum Input Hasil
				@endif
			</td> -->
			<td>
				@if($val->status_data2 == '2')
					Kirim Hasil
				@elseif($val->status_data2 == '1')
					Input Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			<!-- <td>
				@if($val->status_datarpr2 == '2')
					Kirim Hasil
				@elseif($val->status_datarpr2 == '1')
					Input Hasil
				@else
					Belum Input Hasil
				@endif
			</td> -->
			@elseif($input['parameter'] == '3' || $input['parameter'] == '2' || $input['parameter'] == '1')
			<td>
				@if($val->status_data1 == '2')
					Kirim Hasil
				@elseif($val->status_data1 == '1')
					Input Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			<td>
				@if($val->status_data2 == '2')
					Kirim Hasil
				@elseif($val->status_data2 == '1')
					Input Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			<td>
				@if($val->status_datarpr1 == '2')
					Kirim Hasil
				@elseif($val->status_datarpr1 == '1')
					Input Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			<td>
				@if($val->status_datarpr2 == '2')
					Kirim Hasil
				@elseif($val->status_datarpr2 == '1')
					Input Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			@else
			<td>
				@if($val->status_data1 == '2')
					Kirim Hasil
				@elseif($val->status_data1 == '1')
					Input Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			<td>
				@if($val->status_data2 == '2')
					Kirim Hasil
				@elseif($val->status_data2 == '1')
					Input Hasil
				@else
					Belum Input Hasil
				@endif
			</td>
			@endif
		</tr>
		@endforeach
		@endif
	</tbody>
</table>
</body>
</html>