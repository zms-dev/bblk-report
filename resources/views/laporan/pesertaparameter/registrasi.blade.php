@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Data Peserta per Parameter</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="peserta" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div><br>
                      <div>
                          <label for="exampleInputEmail1">Parameter</label>
                          <select class="form-control" name="parameter" required>
                            <option value=""></option>
                            @foreach($data as $val)
                            <option value="{{$val->id}}">{{$val->parameter}}</option>
                            @endforeach
                          </select>
                      </div><br>
                      <div>
                          <label>Siklus</label>
                          <select class="form-control" name="siklus" required>
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div><br>
                      {{ csrf_field() }}
                      <button type="submit" name="proses" class="btn btn-info" onclick="viewtable()">View Table</button>
                      <!-- <button type="submit" name="print" class="btn btn-info" onclick="printpdf()">Print PDF</button> -->
                      <button type="submit" name="print" class="btn btn-info" onclick="printexcel()">Print Excel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy",
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-right",
        minView: 4,
        startView: 4
    });


form=document.getElementById("peserta");
function viewtable() {
        form.action="{{ url('laporan-peserta-parameter/proses')}}";
        form.submit();
}
function printpdf() {
        form.action="{{ url('laporan-peserta-parameter/proses/pdf')}}";
        form.submit();
}
function printexcel() {
        form.action="{{ url('laporan-peserta-parameter/proses/excel')}}";
        form.submit();
}
</script>
@endsection