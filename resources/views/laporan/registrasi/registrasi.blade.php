@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Laporan Peserta</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="registrasi" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div><br>
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <div class="controls input-append">
                              <select name="siklus" class="form-control">
                                <option value="12">1 & 2</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                              </select>
                          </div>
                      </div><br>
                      {{ csrf_field() }}
                      <button type="submit" name="proses" class="btn btn-info" onclick="viewtable()">View Table</button>
                      <!-- <button type="submit" name="print" class="btn btn-info" onclick="printpdf()">Print PDF</button> -->
                      <button type="submit" name="print" class="btn btn-info" onclick="printexcel()">Print Excel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "yyyy",
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-right",
        minView: 4,
        startView: 4
    });


form=document.getElementById("registrasi");
function viewtable() {
        form.action="{{ url('laporan-registrasi/proses')}}";
        form.submit();
}
function printpdf() {
        form.action="{{ url('laporan-registrasi/proses/pdf')}}";
        form.submit();
}
function printexcel() {
        form.action="{{ url('laporan-registrasi/proses/excel')}}";
        form.submit();
}
</script>
@endsection