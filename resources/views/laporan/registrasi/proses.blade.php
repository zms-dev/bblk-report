<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
body{
	font-family: arial;
	font-size: 14px;
}
table, td, th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 5px;
}
</style>
<body>
<h2>Laporan Peserta {{$input['tahun']}}<br>Siklus : {{($input['siklus'] == 1 ? "I" : ($input['siklus'] == 2 ? "II" : "I & II" ))}}</h2>
<table style="min-width:100%">
	<thead>
		<tr>
			<th width="10px">No</th>
			<th width="300px">Kode Peserta</th>
			<th width="300px">Nama Perusahaan</th>
			<th width="300px">Jenis Instansi</th>
			<th width="250px">Provinsi</th>
			<th width="250px">Kota / Kabupaten</th>
			<th width="250px">Alamat</th>
			<th width="80px">Kode Pos</th>
			<th width="150px">Personal</th>
			<th width="100px">No. HP</th>
			<th width="100px">Email</th>
			<th width="50px"><center>HEMA</center></th>
			<th width="50px"><center>KK</center></th>
			<th width="50px"><center>URIN</center></th>
			<th width="50px"><center>BTA</center></th>
			<th width="50px"><center>TC</center></th>
			<th width="50px"><center>MAL</center></th>
			<th width="50px"><center>AHIV</center></th>
			<th width="50px"><center>SYPH</center></th>
			<th width="50px"><center>HBSAG</center></th>
			<th width="50px"><center>AHCV</center></th>
			<th width="50px"><center>GDR</center></th>
			<th width="50px"><center>KAI</center></th>
		</tr>
	</thead>
	<tbody>
		@if(count($data))
		<?php $no = 0; ?>
		@foreach($data as $val)
		<?php $no++;?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->kode_lebpes}}</td>
			<td>{{$val->nama_lab}}</td>
			<td>{{$val->badan_usaha}}</td>
			<td>{{$val->province_name}}</td>
			<td>{{$val->city_name}}</td>
			<td>{{$val->alamat.' Kel. '. $val->village_name. ' Kec. '  . $val->district_name}}</td>
			<td>{{$val->kode_pos}}</td>
			<td>{{$val->personal}}</td>
			<td>{{$val->no_hp}}</td>
			<td>{{$val->email}}</td>
			<td><center>{{($val->hema_count > 0 ? 'V' : '')}}</center></td>
			<td><center>{{($val->kk_count > 0 ? 'V' : '')}}</center></td>
			<td><center>{{($val->urin_count > 0 ? 'V' : '')}}</center></td>
			<td><center>{{($val->bta_count > 0 ? 'V' : '')}}</center></td>
			<td><center>{{($val->tc_count > 0 ? 'V' : '')}}</center></td>
			<td><center>{{($val->mal_count > 0 ? 'V' : '')}}</center></td>
			<td><center>{{($val->ahiv_count > 0 ? 'V' : '')}}</center></td>
			<td><center>{{($val->syph_count > 0 ? 'V' : '')}}</center></td>
			<td><center>{{($val->hbsag_count > 0 ? 'V' : '')}}</center></td>
			<td><center>{{($val->ahcv_count > 0 ? 'V' : '')}}</center></td>
			<td><center>{{($val->dar_count > 0 ? 'V' : '')}}</center></td>
			<td><center>{{($val->mal_count > 0 ? 'V' : '')}}</center></td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>
</body>
</html>