<table>
	<thead>
		<tr>
			<th colspan="16" style="text-align:center;">
			Laporan Peserta {{$input['tahun']}}
			</th>
		</tr>
		<tr>
			<th colspan="16" style="text-align:center;">
			Siklus : {{($input['siklus'] == 1 ? "I" : ($input['siklus'] == 2 ? "II" : "I & II" ))}}
			</th>
		</tr>
		<tr>
			<th>No</th>
			<th>Kode Peserta</th>
			<th>Nama Perusahaan</th>
			<th>Jenis Instansi</th>
			<th>Provinsi</th>
			<th>Kota / Kabupaten</th>
			<th>Alamat</th>
			<th>Kode Pos</th>
			<th>Personal</th>
			<th>No. HP</th>
			<th>Email</th>
			<th style="text-align:center;">HEMA</th>
			<th style="text-align:center;">KK</th>
			<th style="text-align:center;">URIN</th>
			<th style="text-align:center;">BTA</th>
			<th style="text-align:center;">TC</th>
			<th style="text-align:center;">MAL</th>
			<th style="text-align:center;">AHIV</th>
			<th style="text-align:center;">SYPH</th>
			<th style="text-align:center;">HBSAG</th>
			<th style="text-align:center;">AHCV</th>
			<th style="text-align:center;">GDR</th>
			<th style="text-align:center;">KAI</th>
		</tr>
	</thead>
	<tbody>
		@if(count($data))
		<?php $no = 0; ?>
		@foreach($data as $val)
		<?php $no++;?>
		<tr>
			<td>{{$no}}</td>
			<td>{{$val->kode_lebpes}}</td>
			<td>{{$val->nama_lab}}</td>
			<td>{{$val->badan_usaha}}</td>
			<td>{{$val->province_name}}</td>
			<td>{{$val->city_name}}</td>
			<td>{{$val->alamat.' Kel. '. $val->village_name. ' Kec. '  . $val->district_name}}</td>
			<td>{{$val->kode_pos}}</td>
			<td>{{$val->personal}}</td>
			<td>{{$val->no_hp}}</td>
			<td>{{$val->email}}</td>
			<td style="text-align:center;">{{($val->hema_count > 0 ? 'V' : '')}}</td>
			<td style="text-align:center;">{{($val->kk_count > 0 ? 'V' : '')}}</td>
			<td style="text-align:center;">{{($val->urin_count > 0 ? 'V' : '')}}</td>
			<td style="text-align:center;">{{($val->bta_count > 0 ? 'V' : '')}}</td>
			<td style="text-align:center;">{{($val->tc_count > 0 ? 'V' : '')}}</td>
			<td style="text-align:center;">{{($val->mal_count > 0 ? 'V' : '')}}</td>
			<td style="text-align:center;">{{($val->ahiv_count > 0 ? 'V' : '')}}</td>
			<td style="text-align:center;">{{($val->syph_count > 0 ? 'V' : '')}}</td>
			<td style="text-align:center;">{{($val->hbsag_count > 0 ? 'V' : '')}}</td>
			<td style="text-align:center;">{{($val->ahcv_count > 0 ? 'V' : '')}}</td>
			<td style="text-align:center;">{{($val->dar_count > 0 ? 'V' : '')}}</td>
			<td style="text-align:center;">{{($val->kai_count > 0 ? 'V' : '')}}</td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>