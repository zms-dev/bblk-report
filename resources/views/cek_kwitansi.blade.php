@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')


@section('content')
<style type="text/css">
  #example_filter{
    text-align: right !important;
  }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">List Kuitansi</div>

                <div class="panel-body">
                    <form class="form-horizontal"  method="get">
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-link-field="dtp_input1">
                            @if($tahun != NULL)
                              <input size="16" type="text" value="{{$tahun}}" readonly class="form-control" name="tahun">
                            @else
                              <input size="16" type="text" value="2019" readonly class="form-control" name="tahun">
                            @endif
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div>
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select name="siklus" class="form-control" required>
                            <option value="{{$siklus}}">{{$siklus}}</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div><br>
                      <button type="submit" name="proses" class="btn btn-info">Proses</button>
                    </form>
                    <br>
                    <table id="example" class="table table-striped table-bordered" class="display" cellspacing="0"  width="100%" >
                      <thead>
                        <tr>
                            <th>Tahun</th>
                            <th>Laboratoruim</th>
                            <th>Siklus</th>
                            <!-- <th>Bidang</th> -->
                            <th>Parameter</th>
                            <th>Tarif</th>
                            <th width="1%">Invoice</th>
                            <th width="1%">Tanggal</th>
                            <th>PDF</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!empty($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++;?>

                        <tr>
                          <form class="form-horizontal" action="{{url('cetak-kwitansi').'/'.$val->id}}" method="post" enctype="multipart/form-data" target="_blank">
                          <td>{{date('Y', strtotime($val->created_at))}}<input type="hidden" name="email[]" value="{{$val->email}}" ></td>
                          <td>{{$val->nama_lab}}<input type="hidden" name="nama_lab[]" value="{{$val->nama_lab}}" ></td>
                          <td>{{$val->siklus}}</td>
                          <!-- <td>{!!str_replace('|','<br/><br/>',$val->bidang) !!}</td> -->
                          <td>{!!str_replace('|','<br/><br/>',$val->parameter) !!}<input type="hidden" name="parameter[]" value="{{$val->parameter}}" ></td>
                          <input type="hidden" name="siklus[]" value="{{$val->siklus}}">

                          <?php $imun1 = 0; $imun2 = 0; $imun12 = 0;  $bidang = explode("|",$val->bidang); ?>
                          @foreach($bidang as $valb)
                              <?php 
                                  if($valb == "61" || $valb == "71" || $valb == "81" || $valb == "91"){
                                      $imun1++;
                                  }elseif($valb == "62" || $valb == "72" || $valb == "82" || $valb == "92"){
                                      $imun2++;
                                  }elseif($valb == "612" || $valb == "712" || $valb == "812" || $valb == "912"){
                                      $imun12++;
                                  }
                              ?>
                          @endforeach
                            <?php
                              $imunt1 = 0;
                              $imunt2 = 0;
                              $imunt12 = 0;
                              if($imun1 == 4){
                                  $imunt1 = "200000";
                              }elseif($imun2 == 4){
                                  $imunt2 = "200000";
                              }elseif($imun12 == 4){
                                  $imunt12 = "400000";
                              }
                            ?>
                          <td>
                            {{number_format($val->jumlah_tarif - $imunt1 - $imunt2 - $imunt12)}}
                          </td>
                          <td style="" width="1%">
                              @if(!empty($val->datan)) {{$val->datan->invoice}} @endif
                              <input type="text" name="invoice" style="width: 100px;" @if(!empty($val->datan)) value="{{$val->datan->invoice}}" @else value=""  @endif>
                          </td>
                          <td style="" width="1%">
                              @if(!empty($val->datan)) {{$val->datan->tanggal}} @endif
                              <input type="text" name="tanggal" style="width: 100px;" id="tanggal" @if(!empty($val->datan)) value="{{$val->datan->tanggal}}" @else value=""  @endif class="datepick form-control">
                          </td>
                          <td>
                            <input type="hidden" name="tahun" value="{{$val->tahun}}">
                            <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#myModal{{$val->siklus}}{{$val->id}}">
                              Cetak
                            </button><br>
                            <div class="modal fade" id="myModal{{$val->siklus}}{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Edit Data PDF</h4>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group" style="width: 100%">
                                      <label for="inputEmail3" class="col-md-4 control-label">Telah Diterima Dari</label>
                                      <div class="col-md-8">
                                        <input type="text" style="width: 100%" class="form-control" value="Direktur / Kepala {{$val->nama_lab}}" name="diterima">
                                      </div>
                                    </div><br>
                                    <div class="form-group" style="width: 100%">
                                      <label class="col-md-4 control-label">Untuk Pembayaran</label>
                                      <div class="col-md-8">
                                        <input type="text" style="width: 100%" class="form-control" name="pembayaran" value="Keikutsertaan Program Nasional Pemantapan Mutu Eksternal BBLK Jakarta T.A. {{date('Y', strtotime($val->created_at))}}">
                                      </div>
                                    </div><br>
                                    <div class="form-group" style="width: 100%">
                                      <label class="col-md-4 control-label">Kepada</label>
                                      <div class="col-md-8">
                                        <input type="text" style="width: 100%" class="form-control" name="kepada" value="{{$val->nama_lab}}">
                                      </div>
                                    </div><br>
                                    <div class="form-group" style="width: 100%">
                                      <label class="col-md-4 control-label">Perihal</label>
                                      <div class="col-md-8">
                                        <input type="text" style="width: 100%" class="form-control" name="perihal" value="Keikutsertaan Program Nasional Pemantapan Mutu Eksternal BBLK Jakarta T.A. {{date('Y', strtotime($val->created_at))}}">
                                      </div>
                                    </div><br>
                                    <div class="form-group" style="width: 100%">
                                      <label class="col-md-4 control-label">Kasubag</label>
                                      <div class="col-md-8">
                                        <input type="text" style="width: 100%" class="form-control" name="kasubag" value="Kasubag Keuangan">
                                      </div>
                                    </div><br>
                                    <div class="form-group" style="width: 100%">
                                      <label class="col-md-4 control-label">Nama</label>
                                      <div class="col-md-8">
                                        <input type="text" style="width: 100%" class="form-control" name="nama" value="Faradita Amara, SE">
                                      </div>
                                    </div><br>
                                    <div class="form-group" style="width: 100%">
                                      <label class="col-md-4 control-label">NIP</label>
                                      <div class="col-md-8">
                                        <input type="text" style="width: 100%" class="form-control" name="nip" value="198604182010122003">
                                      </div>
                                    </div><br>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <input type="submit" name="cetak" value="Cetak" class="btn btn-primary">
                                  </div>
                                </div>
                              </div>
                            </div>

                            <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target=".bs-example-modal-lg{{$no}}">View</button>
                            <div class="modal fade bs-example-modal-lg{{$no}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                              <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                  <img src="{{URL::asset('asset/file').'/'.$val->file}}" width="100%">
                                </div>
                              </div>
                            </div><br>
                            <a href="{{URL::asset('asset/file').'/'.$val->file}}" class="btn btn-primary btn-block" target="_blank">Download</a><br>
                            <input type="submit" name="cetak" value="Simpan Invoice" class="btn btn-primary btn-block">
                          </td>
                          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                          </form>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('#example').DataTable();
});
   $("#checkAll").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 });
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});
</script>
@endsection
