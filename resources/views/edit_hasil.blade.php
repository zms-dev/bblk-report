@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Hasil</div>
                <div class="panel-body">
                <label>
                    <!-- - Pengisian Hasil Online PNPME Siklus 1 diperpanjang sampai tanggal 24 Mei 2018 -->
                </label>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Bidang</th>
                            <th colspan="2" class="siklus1">Siklus&nbsp;1</th>
                            <th colspan="2" class="siklus2">Siklus&nbsp;2</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->id_bidang > '5')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                @if($val->bidang == '7')
                                    @if($val->siklus_1 == 'done')
                                        @if($val->pemeriksaan == 'done')
                                            @if($val->status_data1 == 1)
                                            <td class="siklus1">
                                                <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                            </td>
                                            @else
                                            <td colspan="2" class="siklus1"></td>
                                            @endif
                                        @else
                                            <td colspan="2" class="siklus1"></td>
                                        @endif
                                        <!-- @if($val->rpr1 == 'done')
                                            @if($val->status_datarpr1 == 1)
                                            <td>
                                                <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/edit')}}/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                            </td>
                                            @endif
                                        @else
                                            <td></td>
                                        @endif -->
                                    @else
                                    <td colspan="2" class="siklus1"></td>                           
                                    @endif
                                    @if($val->siklus_2 == 'done')
                                        @if($val->pemeriksaan2 == 'done')
                                            @if($val->status_data2 == 1)
                                            <td colspan="2" class="siklus2">
                                                <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                            </td>
                                            @endif
                                        @else
                                            <td colspan="2" class="siklus2"></td>
                                        @endif
                                        <!-- @if($val->rpr2 == 'done')
                                            @if($val->status_datarpr2 == 1)
                                            <td>
                                                <a href="{{URL('hasil-pemeriksaan/rpr-syphilis/edit')}}/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>RPR</center></a>
                                            </td>
                                            @endif
                                        @else
                                            <td></td>
                                        @endif -->
                                    @else
                                    <td colspan="2" class="siklus2"></td>                           
                                    @endif
                                @else
                                    <td colspan="2" class="siklus1">
                                        @if($val->siklus_1 == 'done')
                                            @if($val->status_data1 == 1)
                                            <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                            @endif
                                        @else
                                        @endif
                                    </td>
                                    <td colspan="2" class="siklus2">
                                        @if($val->siklus_2 == 'done')
                                            @if($val->status_data2 == 1)
                                            <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                            @endif
                                        @else
                                        @endif
                                    </td>
                                @endif
                            </tr>
                        @elseif($val->id_bidang < '5')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                @if($val->siklus_1 == 'done')
                                    @if($val->pemeriksaan == 'done')
                                        @if($val->status_data1 == 1)
                                            <td class="siklus1">
                                                <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>I-A</center></a>
                                            </td>
                                        @else
                                        <td class="siklus1"></td>
                                        @endif
                                    @else
                                        <td class="siklus1"></td>
                                    @endif
                                    @if($val->pemeriksaan2 == 'done')
                                        @if($val->status_data2 == 1)
                                            <td class="siklus1">
                                                <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?x=b&y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>I-B</center></a>
                                            </td>
                                        @else
                                        <td class="siklus1"></td>
                                        @endif
                                    @else
                                        <td class="siklus1"></td>
                                    @endif
                                @else
                                <td colspan="2" class="siklus1"></td>
                                @endif
                                @if($val->siklus_2 == 'done')
                                    @if($val->rpr1 == 'done')
                                        @if($val->status_datarpr1 == 1)
                                            <td class="siklus2">
                                                <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>II-A</center></a>
                                            </td>
                                        @else
                                        <td class="siklus2"></td>
                                        @endif
                                    @else
                                        <td class="siklus2"></td>
                                    @endif
                                    @if($val->rpr2 == 'done')
                                        @if($val->status_datarpr2 == 1)
                                            <td class="siklus2">
                                                <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?x=b&y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>II-B</center></a>
                                            </td>
                                        @else
                                        <td class="siklus2"></td>
                                        @endif
                                    @else
                                        <td class="siklus2"></td>
                                    @endif
                                @else
                                <td colspan="2" class="siklus2"></td>
                                @endif
                            </tr>
                        @else
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                <td colspan="2" class="siklus1">
                                    @if(($val->pemeriksaan == 'done') && ($val->siklus_1 == 'done'))
                                        @if($val->status_data1 == 1)
                                            <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                        @endif
                                    @else
                                    @endif
                                </td>
                                <td colspan="2" class="siklus2">
                                    @if(($val->pemeriksaan2 == 'done') && ($val->siklus_2 == 'done'))
                                        @if($val->status_data2 == 1)
                                            <a href="{{URL('').$val->Link}}/edit/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                        @endif
                                    @else
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6">Data Kosong</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(function() {
    @if($siklus->siklus == 1)
    $('.siklus2').hide();
    @else
    $('.siklus1').hide();
    @endif
});

(function (global) {
    if(typeof (global) === "undefined")
    {
        throw new Error("window is undefined");
    }

    var _hash = "!";
    var noBackPlease = function () {
        global.location.href += "#";
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };
    
    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {
        
        noBackPlease();
        document.body.onkeydown = function (e) {
            var elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            e.stopPropagation();
        };
        
    };

})(window);
</script>
@endsection