@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Ubah Juklak
              </header>
              <div class="panel-body">  
                    @foreach($data as $val)
                    <form action="{{url('admin/juklak/edit').'/'.$val->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Parameter Bidang</label>
                          <select class="form-control" name="id_bidang">
                            <option value="{{$val->Id}}">{{$val->Bidang}}</option>
                            @if(count($bidang))
                            @foreach($bidang as $vali)
                            <option value="{{$vali->id}}">{{$vali->parameter}}</option>
                            @endforeach
                            @endif
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">File</label>
                          <input type="file" class="form-control" name="file">
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
                  @endforeach
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection