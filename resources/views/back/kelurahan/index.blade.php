@extends('layouts.navbar')  
@section('content') 
<link href="{{URL::asset('css/datatables.min.css')}}" rel="stylesheet" />
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Table Kelurahan
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
                            <a href="{{URL('admin/kelurahan/insert')}}">
                            <button class="btn green">
                                Tambah <i class="icon-plus"></i>
                            </button>
                            </a>
                        </div>
                    </div>
                    <div class="space15"></div>
                    <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                        <tr>
                            <th><center>Aksi</center></th>
                            <th>No</th>
                            <th><center>Kelurahan</center></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@stop

@section('scriptBlock')
<script src="{{url::asset('backend/js/datatables.min.js')}}"></script>
<script>
        var table = $(".dataTables-data");
        var dataTable = table.DataTable({
        responsive:!0,
        "serverSide":true,
        "processing":true,
        "ajax":{
            url : "{{url('admin/kelurahan')}}"
        },
        dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
        language:{
            paginate:{
                previous:"&laquo;",
                next:"&raquo;"
            },search:"_INPUT_",
            searchPlaceholder:"Search..."
        },
        "columns":[
            {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
            {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
            {"data":"name","name":"name","searchable":true,"orderable":true},
        ],
        order:[[1,"asc"]]
    });

</script>
@stop