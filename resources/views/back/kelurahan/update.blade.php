@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Edit Kelurahan
              </header>
              <div class="panel-body">  
                    @foreach($data as $val)
                    <form action="{{url('admin/kelurahan/edit').'/'.$val->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Kecamatan</label>
                          <select class="form-control" name="district_id">
                            <option value="{{$val->district_id}}">{{$val->Name}}</option>
                            <option></option>
                            @if(count($districts))
                            @foreach($districts as $valu)
                            <option value="{{$valu->id}}">{{$valu->name}}</option>
                            @endforeach
                            @endif
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Kelurahan</label>
                          <input type="text" class="form-control" name="name" value="{{$val->name}}">
                      </div>
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                      <button type="submit" class="btn btn-info">Update</button>
                    </form>
                    @endforeach
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection