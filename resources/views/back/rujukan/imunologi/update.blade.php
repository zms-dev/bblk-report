@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Edit Rujukan Imunologi
              </header>
              <div class="panel-body">  
                    @if(count($rujukan))
                    <form action="{{url('admin/rujukan-imunologi/edit').'/'.$rujukan->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Parameter</label>
                          <input type="text" class="form-control" name="parameter" value="{{$rujukan->parameter}}" readonly>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus">
                            <option value="{{$rujukan->siklus}}">
                              @if($rujukan->siklus == 12)
                              1 & 2
                              @else
                              {{$rujukan->siklus}}
                              @endif
                            </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Tahun</label>
                          <select class="form-control" name="tahun">
                            <option value="{{$rujukan->tahun}}">{{$rujukan->tahun}}</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                          </select>
                      </div>
                      <table class="table table-bordered">
                        <tr>
                          <th>No</th>
                          <th>Kode Sediaan</th>
                          <th>Rujukan</th>
                        </tr>
                        <?php $no = 0; ?>
                        @foreach($data as $valu)
                        <?php $no++; ?>
                        <tr>
                          <input type="hidden" name="id_rujukan[]" value="{{$valu->id}}">
                          <td>{{$no}}</td>
                          <td><input type="text" class="form-control" name="kode_sediaan[]" value="{{$valu->kode_bahan_uji}}"></td>
                          <td>
                            <div class="form-group">
                                <select class="form-control" name="rujukan[]">
                                  <option value="{{$valu->nilai_rujukan}}">{{$valu->nilai_rujukan}}</option>
                                  <option value="Reaktif">Reaktif</option>
                                  <option value="Non Reaktif">Non Reaktif</option>
                                </select>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                      </table>
                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                      <button type="submit" class="btn btn-info">Update</button>
                    </form>
                    @endif
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection