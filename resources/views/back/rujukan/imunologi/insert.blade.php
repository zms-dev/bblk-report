@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Tambah Rujukan Imunologi
              </header>
              <div class="panel-body">  
                    <form action="{{url('admin/rujukan-imunologi/insert')}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Parameter</label>
                          <input type="" name="parameter" class="form-control" required 
                              @if(Auth::user()->penyelenggara == '6')
                                value="Anti HIV"
                              @elseif(Auth::user()->penyelenggara == '7')
                                value="Anti TP"
                              @elseif(Auth::user()->penyelenggara == '8')
                                value="HBsAg"
                              @elseif(Auth::user()->penyelenggara == '9')
                                value="Anti HCV"
                              @endif readonly
                          >
                      </div>

                      <div class="form-group">
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus" required="">
                            <option value=""></option>
                             <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Tahun</label>
                          <select class="form-control" name="tahun" required="">
                            <option value=""></option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                          </select>
                      </div>
                    
                      <table class="table table-bordered asu" >
                        <tr>
                          <th>No</th>
                          <th>Kode Bahan Uji</th>
                          <th>Nilai Rujukan</th>
                        </tr>
                          <?php $no=1;  $kd=0;?>
                          @for ($i = $no; $i <= 3; $i++)
                          <?php $kd++ ?>
                          <tr>
                            <td>{{$no}}</td>
                            <td><input type="text" name="kode_bahan_uji[]" class="form-control" value="{{$no++}}" required=""></td>
                            <td>
                              <select class="form-control" name="nilai_rujukan[]" required="">
                                <option value=""></option>
                                <option value="Reaktif">Reaktif</option>
                                <option value="Non Reaktif">Non Reaktif</option>
                              </select>
                              </td>
                          </tr>
                          @endfor
                      </table>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
@section('scriptBlock')
<script type="text/javascript">
    $('.lastI').hide();
    $('.lastI').hide();
    $('#rangeth1').hide();
    $('#rangeth2').hide();
    $('#parameter').change(function(e){
    console.log($(this).val());
    if ($(this).val() == 'RPR') {
      $(".lastI").prop('required',true).show();
      $(".lastI").prop('required',true).show();
      $(".last2").prop('required',true);
      $(".last2").prop('required',true);
      $(".asu").css('width', '100%');
      $('#rangeth1').show();
      $('#rangeth2').show();
    }else{
      $(".lastI").hide();
      $(".lastI").hide();
      $(".last2").prop('required',false);
      $(".last2").prop('required',false);
      $(".asu").css('width', '70%');
      $('#rangeth1').hide();
      $('#rangeth2').hide();
    }
    }); 
</script>
@endsection