@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rujukan Urinalisa Metode</div>
                <div class="panel-body">
                    <form action="{{url('/admin/rujukan-urinalisa/insertmetode')}}" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                              <label for="exampleInputEmail1">Bahan</label>
                              <select class="form-control" name="bahan" required="">
                                <option value=""></option>
                                <option value="Negatif">Negatif</option>
                                <option value="Positif">Positif</option>
                              </select>
                          </div>
                          <div class="form-group">
                              <label for="exampleInputEmail1">Tipe</label>
                              <select class="form-control" name="type" required="">
                                <option value=""></option>
                                <option value="a">A</option>
                                <option value="b">B</option>
                              </select>
                          </div>
                       
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                              <label for="exampleInputEmail1">Siklus</label>
                              <select class="form-control" name="siklus" required="">
                                <option value=""></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="12">1 & 2</option>
                              </select>
                          </div>
                          <div class="form-group">
                              <label for="exampleInputEmail1">Tahun</label>
                              <select class="form-control" name="tahun" required="">
                                <option value=""></option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                              </select>
                          </div>
                        </div>
                      </div>
                      <table class="table table-bordered">
                        <tr>
                          <th>Parameter</th>
                          <th>Metode Pemeriksaan</th>
                          <th>Batas Bawah</th>
                          <th>Batas Atas</th>
                        </tr>
                        @foreach($parameter as $val)
                        <tr>
                          <td>
                            <input type="hidden" name="parameter[]" value="{{$val->parameter_id}}">
                            {{$val->nama_parameter}}
                          </td>
                          <td>
                            <input type="hidden" name="metode[]" value="{{$val->id}}">
                            {{$val->metode_pemeriksaan}}
                          </td>
                          <td>
                            @if($val->parameter_id > 10)
                            <select class="form-control" name="batas_bawah[]"  >
                              <option value=""></option>
                              <option value="±">±</option>
                              <option value="1+">1+</option>
                              <option value="2+">2+</option>
                              <option value="3+">3+</option>
                              <option value="4+">4+</option>
                              <option value="5+">5+</option>
                              <option value="Negatif">Negatif</option>
                              <option value="Positif">Positif</option>
                            </select>
                             @else
                            <input type="text" class="form-control" name="batas_bawah[]" >
                            @endif
                          </td>
                          <td>
                             @if($val->parameter_id > 10)
                            <select class="form-control" name="batas_atas[]"  >
                              <option value=""></option>
                              <option value="±">±</option>
                              <option value="1+">1+</option>
                              <option value="2+">2+</option>
                              <option value="3+">3+</option>
                              <option value="4+">4+</option>
                              <option value="5+">5+</option>
                              <option value="Negatif">Negatif</option>
                              <option value="Positif">Positif</option>
                            </select>
                             @else
                            <input type="text" class="form-control" name="batas_atas[]"  >
                            @endif
                          </td>
                        </tr>
                        @endforeach
                      </table>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection