@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Rujukan Bj & PH</div>
        <div class="panel-body">
          <form action="{{url('admin/rujukan-urinalisa/bj-ph')}}" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="exampleInputEmail1">Parameter</label>
                <select class="form-control" name="parameter">
                  <option value=""></option>
                  <option value="9">Berat jenis</option>
                  <option value="10">Ph</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Siklus</label>
                <select class="form-control" name="siklus">
                  <option value=""></option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="12">1 & 2</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Type</label>
                <select class="form-control" name="bahan">
                  <option value=""></option>
                  <option value="Negatif">A</option>
                  <option value="Positif">B</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Tahun</label>
                <select class="form-control" name="tahun">
                  <option value=""></option>
                  <option value="2017">2017</option>
                  <option value="2018">2018</option>
                  <option value="2019">2019</option>
                  <option value="2020">2020</option>
                  <option value="2021">2021</option>
                  <option value="2022">2022</option>
                  <option value="2023">2023</option>
                  <option value="2024">2024</option>
                  <option value="2025">2025</option>
                </select>
            </div>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-info">Simpan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection