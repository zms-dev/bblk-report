@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Ubah Parameter / Sub Bidang
              </header>
              <div class="panel-body">  
                    @foreach($data as $val)
                    <form action="{{url('admin/parameter/edit').'/'.$val->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Master Bidang</label>
                          <select class="form-control" name="id_bidang">
                            <option value="{{$val->Id}}">{{$val->Bidang}}</option>
                            @if(count($bidang))
                            @foreach($bidang as $vali)
                            <option value="{{$vali->id}}">{{$vali->bidang}}</option>
                            @endforeach
                            @endif
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Parameter / Sub Bidang</label>
                          <input type="text" class="form-control" name="parameter" value="{{$val->parameter}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Tarif</label>
                          <input type="text" class="form-control" name="tarif" value="{{$val->tarif}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Kuota Siklus 1</label>
                          <input type="text" class="form-control" name="kuota_1" value="{{$val->kuota_1}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Kuota Siklus 2</label>
                          <input type="text" class="form-control" name="kuota_2" value="{{$val->kuota_2}}">
                      </div>
                      <!-- <div class="form-group">
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus">
                            @if($val->siklus == 1)
                            <option value="{{$val->siklus}}">Siklus 1</option>
                            @elseif($val->siklus == 2)
                            <option value="{{$val->siklus}}">Siklus 2</option>
                            @else
                            <option value="{{$val->siklus}}">Siklus 1 & 2</option>
                            @endif
                            <option></option>
                            <option value="1">Siklus 1</option>
                            <option value="2">Siklus 2</option>
                            <option value="12">Siklus 1 & 2</option>
                          </select>
                      </div> -->
                      <div class="form-group">
                          <label for="exampleInputEmail1">Link</label>
                          <select class="form-control" name="link" required>
                            <option value="{{$val->link}}"></option>
                            <option value="/hasil-pemeriksaan/bta">BTA</option>
                            <option value="/hasil-pemeriksaan/malaria">Malaria</option>
                            <option value="/hasil-pemeriksaan/telur-cacing">Telur Cacing</option>
                            <option value="/hasil-pemeriksaan/hematologi">Hematologi</option>
                            <option value="/hasil-pemeriksaan/urinalisasi">Urinalisa</option>
                            <option value="/hasil-pemeriksaan/kimia-air">Kimia Air</option>
                            <option value="/hasil-pemeriksaan/kimia-air-terbatas">Kimia Air Terbatas</option>
                            <option value="/hasil-pemeriksaan/kimia-klinik">Kimia Klinik</option>
                            <option value="/hasil-pemeriksaan/antibiotik">Bakteri</option>
                            <option value="/hasil-pemeriksaan/anti-hiv">Anti Hiv</option>
                            <option value="/hasil-pemeriksaan/syphilis">Syphilis</option>
                            <option value="/hasil-pemeriksaan/hbsag">HBsAg</option>
                            <option value="/hasil-pemeriksaan/anti-hcv">ANTI-HCV</option>
                            <option value="4parameter">4 Parameter (Anti HIV, TP, HBsAg, Anti HCV)</option>
                          </select>
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
                  @endforeach
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection