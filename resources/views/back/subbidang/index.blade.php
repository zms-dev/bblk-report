@extends('layouts.navbar')  
@section('content') 
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Table Parameter / Sub Bidang
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
                            <a href="{{URL('admin/parameter/insert')}}">
                            <button class="btn green">
                                Tambah <i class="icon-plus"></i>
                            </button>
                            </a>
                        </div>
                    </div>
                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Master Bidang</th>
                            <th>Parameter</th>
                            <th>Tarif (Rp)</th>
                            <th>Kuota Siklus 1</th>
                            <th>Kuota Siklus 2</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($subbidang))
                        <?php
                        $no = 0;
                        ?>
                        @foreach($subbidang as $val)
                        <?php
                        $no++;
                        ?>
                        <tr>
                            <td><center>{{$no}}</center></td>
                            <td>{{$val->Bidang}}</td>
                            <td>{{$val->parameter}}</td>
                            <td>{{$val->tarif}}</td>
                            <td>{{$val->kuota_1}}</td>
                            <td>{{$val->kuota_2}}</td>
                            <td>
                                <a href="{{URL('admin/parameter/edit').'/'.$val->id}}">
                                    <button class="btn btn-primary btn-xs">
                                        <i class="icon-pencil"></i>
                                    </button>
                                </a> <p></p>
                                <a href="{{URL('admin/parameter/delete').'/'.$val->id}}">
                                    <button class="btn btn-danger btn-xs">
                                        <i class="icon-trash "></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@endsection