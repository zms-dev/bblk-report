@extends('layouts.navbar')  
@section('content') 
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Setting Siklus PNPME
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
            <form method="post">
                  {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleInputEmail1">Siklus PNPME</label>
                    <select class="form-control" required name="siklus">
                        @if(isset($menu))
                        <option value="{{$menu->siklus}}">{{$menu->siklus}}</option>
                        @else
                        <option></option>
                        @endif
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tahun</label>
                    <select name="tahun" class="form-control" required>
                        <option readonly>Pilih Tahun</option>
                        <option {{ $menu->tahun =="2018"?'selected':'' }} value="2018">2018</option>
                        <option {{ $menu->tahun =="2019"?'selected':'' }} value="2019">2019</option>
                        <option {{ $menu->tahun =="2020"?'selected':'' }} value="2020">2020</option>
                        <option {{ $menu->tahun =="2021"?'selected':'' }} value="2021">2021</option>
                        <option {{ $menu->tahun =="2022"?'selected':'' }} value="2022">2022</option>
                        <option {{ $menu->tahun =="2023"?'selected':'' }} value="2023">2023</option>
                        <option {{ $menu->tahun =="2024"?'selected':'' }} value="2024">2024</option>
                        <option {{ $menu->tahun =="2025"?'selected':'' }} value="2025">2025</option>
                    </select>
                </div>
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
                            <input type="submit" name="proses" class="btn green" value="Proses">
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@endsection