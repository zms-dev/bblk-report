@extends('layouts.navbar')  
@section('content') 
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Setting Menu Website PNPME
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
            <form method="post">
                  {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleInputEmail1">Menu</label>
                    <select class="form-control" required name="menu">
                        <option></option>
                        <option value="Daftar PNPME">Daftar PNPME</option>
                        <option value="Input Hasil">Input Hasil</option>
                        <option value="Hasil Evaluasi">Hasil Evaluasi</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Status</label>
                    <select class="form-control" required name="status">
                        <option></option>
                        <option value="hilang">Hilang</option>
                        <option value="muncul">Muncul</option>
                    </select>
                </div>
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
                            <input type="submit" name="proses" class="btn green" value="Proses">
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@endsection