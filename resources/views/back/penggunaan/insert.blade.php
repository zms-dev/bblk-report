@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Tambah Tutorial Penggunaan
              </header>
              @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ Session::get('message') }}
                  </p>
              @endif 
              <div class="panel-body"> 
                    <form action="{{url('admin/penggunaan/insert')}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Video <small style="color: #ff7560">* Format (mp4, avi). Max size (150 MB)</small></label>
                          <input type="file" class="form-control" name="video" onchange="ValidateSize(this)" id="file" required>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Role</label>
                          <select class="form-control" name="role" required>
                            <option value=""></option>
                            <option value="1">Super User</option>
                            <option value="2">Management</option>
                            <option value="3">Peserta</option>
                            <option value="4">Keuangan</option>
                            <option value="5">Penyelenggara</option>
                            <option value="6">Tenaga Ahli</option>
                            <option value="7">Pimpinan</option>
                          </select>
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>

<script type="text/javascript" >
var uploadField = document.getElementById("file"), regex = new RegExp("(.*?)\.(docx|doc|pdf|xml|bmp|ppt|xls)$");;

uploadField.onchange = function() {
  var FileSize = file.files[0].size / 1024 / 1024;
  if (FileSize > 150) {
      alert('Maksimal Size File 150 MB');
      this.value = "";
  } else {
  }

  if (!(regex.test(val))) {
      $(this).val('');
      alert('Please select correct file format');
  }
}
</script>
@endsection