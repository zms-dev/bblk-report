<?php
function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
}
?>
@extends('layouts.navbar')
@section('content') 
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Table Tutorial Penggunaan
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
                            <a href="{{URL('admin/penggunaan/insert')}}">
                            <button class="btn green">
                                Tambah <i class="icon-plus"></i>
                            </button>
                            </a>
                        </div>
                    </div>
                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>role</th>
                            <th>File</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data))
                        <?php
                        $no = 0;
                        ?>
                        @foreach($data as $val)
                        <?php
                        $no++;
                        $role = ['','Super User','Management','User','Keuangan','Penyelenggara','Tenaga Ahli','Pimpinan'];
                        $q = $role[$val->role];
                        ?>
                        <tr>
                            <td><center>{{$no}}</center></td>
                            <td>{{$q}}</td>
                            <td>{{$val->video}}</td>
                            <td>
                                <a href="{{URL('admin/penggunaan/edit').'/'.$val->id}}">
                                    <button class="btn btn-primary btn-xs">
                                        <i class="icon-pencil"></i>
                                    </button>
                                </a>
                                <a href="{{URL('admin/penggunaan/delete').'/'.$val->id}}">
                                    <button class="btn btn-danger btn-xs">
                                        <i class="icon-trash "></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@endsection