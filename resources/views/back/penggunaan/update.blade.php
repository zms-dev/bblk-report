@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Ubah Tutorial Penggunaan
              </header>
              <div class="panel-body">  
                    @foreach($data as $val)
                    <form action="{{url('admin/penggunaan/edit').'/'.$val->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Video</label>
                          <input type="file" class="form-control" name="video">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Role</label>
                          <select class="form-control" name="role">
                          <?php
                            $role = ['','Super User','Management','User','Keuangan','Penyelenggara','Tenaga Ahli','Pimpinan'];
                            $q = $role[$val->role];
                          ?>
                            <option value="{{$val->role}}">{{$q}}</option>
                            <option value=""></option>
                            <option value="1">Super User</option>
                            <option value="2">Management</option>
                            <option value="3">Peserta</option>
                            <option value="4">Keuangan</option>
                            <option value="5">Penyelenggara</option>
                            <option value="6">Tenaga Ahli</option>
                            <option value="7">Pimpinan</option>
                          </select>
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
                  @endforeach
              </div>
          </section>
      </div>
    </div>
  </section>
</section>

<script type="text/javascript" >
    CKEDITOR.replace( 'editor1' );
</script>
@endsection