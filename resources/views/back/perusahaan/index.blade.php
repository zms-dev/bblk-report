@extends('layouts.navbar')  
@section('content') 
<link href="{{URL::asset('css/datatables.min.css')}}" rel="stylesheet" />
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Table Perusahaan
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-warning') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                        <tr>
                            <th><center>Aksi</center></th>
                            <th>No</th>
                            <th><center>Nama</center></th>
                            <th><center>Email</center></th>
                            <th><center>Nama Perusahaan</center></th>
                            <th><center>Alamat</center></th>
                            <th><center>Telepon</center></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@stop

@section('scriptBlock')
<script src="{{url::asset('backend/js/datatables.min.js')}}"></script>
<script type="text/javascript">
    function ConfirmDelete()
  {
  var x = confirm("Apakah ingin dihapus?");
  if (x)
    return true;
  else
    return false;
  }
</script>
<script>
        var table = $(".dataTables-data");
        var dataTable = table.DataTable({
        responsive:!0,
        "serverSide":true,
        "processing":true,
        "ajax":{
            url : "{{url('admin/data-perusahaan')}}"
        },
        dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
        language:{
            paginate:{
                previous:"&laquo;",
                next:"&raquo;"
            },search:"_INPUT_",
            searchPlaceholder:"Search..."
        },
        "columns":[
            {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
            {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
            {"data":"name","name":"name","searchable":true,"orderable":true},
            {"data":"email","name":"email","searchable":true,"orderable":true},
            {"data":"nama_lab","name":"nama_lab","searchable":true,"orderable":true},
            {"data":"alamat","name":"alamat","searchable":true,"orderable":true},
            {"data":"telp","name":"telp","searchable":true,"orderable":true},
        ],
        order:[[1,"asc"]]
    });

</script>
@stop