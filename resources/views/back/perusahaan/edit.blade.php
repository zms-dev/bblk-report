@extends('layouts.navbar')  
@section('content') 
<link href="{{URL::asset('css/datatables.min.css')}}" rel="stylesheet" />
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Table Perusahaan
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
                <div class="adv-table editable-table ">
                	<div class="clearfix">
                        <div class="btn-group">
                            <a href="{{URL('admin/data-perusahaan')}}">
                            <button class="btn green">
                                Kembali <i class="icon-"></i>
                            </button>
                            </a>
                        </div>
                    </div>
                    <h4>Data Tanda Terima Bahan Uji</h4>
                    <table class="table table-striped table-bordered table-hover dataTables-terima" width="100%">
                        <thead>
                        <tr>
                            <th><center>Aksi</center></th>
                            <th>No</th>
                            <th><center>Parameter</center></th>
                            <th><center>Siklus</center></th>
                            <th><center>Tahun</center></th>
                            <th><center>Kondisi</center></th>
                            <th><center>Keterangan</center></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <hr>
                    <h4>Data Bidang Pendaftaran</h4>
                    <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                        <tr>
                            <th><center>Status Siklus 1</center></th>
                            <th><center>Status Siklus 2</center></th>
                            <th>No</th>
                            <th><center>Status</center></th>
                            <th><center>Siklus</center></th>
                            <th><center>Tahun</center></th>
                            <th><center>Parameter</center></th>
                            <th><center>Hapus Bidang</center></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>

@stop

@section('scriptBlock')
<script src="{{url::asset('backend/js/datatables.min.js')}}"></script>
<script type="text/javascript">
    function ConfirmDelete()
  {
  var x = confirm("Apakah ingin dihapus?");
  if (x)
    return true;
  else
    return false;
  }
</script>
<script>
var table = $(".dataTables-data");
var dataTable = table.DataTable({
    responsive:!0,
    "serverSide":true,
    "processing":true,
    "ajax":{
        url : "{{url('admin/data-perusahaan/edit')}}/{{$id}}" ,
    },
    dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
    language:{
        paginate:{
            previous:"&laquo;",
            next:"&raquo;"
        },search:"_INPUT_",
        searchPlaceholder:"Search..."
    },
    "columns":[
        {"data":"siklus1","name":"siklus1","searchable":false,"orderable":false,"width" : "10%"},
        {"data":"siklus2","name":"siklus2","searchable":false,"orderable":false,"width" : "10%"},
        {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
        {"data":"status","name":"status","searchable":true,"orderable":true},
        {"data":"siklus","name":"siklus","searchable":true,"orderable":true},
        {"data":"tahun","name":"created_at","searchable":true,"orderable":true},
        {"data":"parameter","name":"parameter","searchable":true,"orderable":true},
        {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
    ],
    order:[[1,"asc"]]
});
var table = $(".dataTables-terima");
var dataTable = table.DataTable({
    responsive:!0,
    "serverSide":true,
    "processing":true,
    "ajax":{
        url : "{{url('admin/data-perusahaan/tanda-terima/edit')}}/{{$id}}" ,
    },
    dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
    language:{
        paginate:{
            previous:"&laquo;",
            next:"&raquo;"
        },search:"_INPUT_",
        searchPlaceholder:"Search..."
    },
    "columns":[
        {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
        {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
        {"data":"parameter","name":"parameter","searchable":true,"orderable":true},
        {"data":"siklus","name":"siklus","searchable":true,"orderable":true},
        {"data":"tahun","name":"created_at","searchable":true,"orderable":true},
        {"data":"kondisi","name":"kondisi","searchable":true,"orderable":true},
        {"data":"keterangan","name":"keterangan","searchable":true,"orderable":true},
    ],
    order:[[1,"asc"]]
});
</script>
@stop