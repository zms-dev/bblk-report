@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Edit Hak Akses
              </header>
              <div class="panel-body">  
                    @foreach($akses as $val)
                    <form action="{{url('admin/hak-akses/edit').'/'.$val->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Nama</label>
                          <input type="text" class="form-control" name="name" value="{{$val->name}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Email</label>
                          <input type="text" class="form-control" name="email" value="{{$val->email}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Role</label>
                          <select class="form-control" name="role">
                          <?php
                            $role = ['','Super User','Management','User','Keuangan','Penyelenggara','Tenaga Ahli','Pimpinan'];
                            $q = $role[$val->role];
                          ?>
                            <option value="{{$val->role}}">{{$q}}</option>
                            <option value=""></option>
                            <option value="1">Super User</option>
                            <option value="2">Management</option>
                            <option value="3">User</option>
                            <option value="4">Keuangan</option>
                            <option value="5">Penyelenggara</option>
                            <option value="6">Administrasi</option>
                            <option value="7">Pimpinan</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Parameter</label>
                          <select class="form-control" name="penyelenggara">
                            <option value="{{$val->penyelenggara}}">{{$val->parameter}}</option>
                            <option value=""></option>
                            @foreach($sub as $val)
                            <option value="{{$val->id}}">{{$val->parameter}}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Reset Password</label>
                          <input type="password" class="form-control" name="password">
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Update</button>
                    </form>
                    @endforeach
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection