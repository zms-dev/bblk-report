@extends('layouts.navbar')  
@section('content') 
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                Table Hak Akses
            </header>
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                </p>
            @endif
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="btn-group">
                            <a href="{{URL('admin/hak-akses/insert')}}">
                            <button class="btn green">
                                Tambah <i class="icon-plus"></i>
                            </button>
                            </a>
                        </div>
                    </div>
                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Role</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($akses))
                        <?php
                        $no = 0;
                        ?>
                        @foreach($akses as $val)
                        <?php
                        $no++;
                        $role = ['','Super User','Management','User','Keuangan','Penyelenggara','Tenaga Ahli','Pimpinan'];
                        $q = $role[$val->role];
                        ?>
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$q}}</td>
                            <td>{{$val->name}}</td>
                            <td>{{$val->email}}</td>
                            <td>
                                <a href="{{URL('admin/hak-akses/edit').'/'.$val->id}}" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        <i class="icon-pencil"></i>
                                    </button>
                                </a> &nbsp;
                                <a href="{{URL('admin/hak-akses/delete').'/'.$val->id}}">
                                    <button class="btn btn-danger btn-xs">
                                        <i class="icon-trash "></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
@endsection