@extends('layouts.navbar')  
@extends('layouts.header')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Tambah Evaluasi
              </header>
              <div class="panel-body">  
                    <form method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">File</label>
                          <input type="file" class="form-control myFile" name="file">
                      </div>
                      {{ csrf_field() }}
                    <div class="tombolna">
                    </div>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
<script type="text/javascript">

$('.myFile').bind('change', function() {
  if(this.files[0].size >= 20000000){
      alert('File yang di input melebihi batas 20Mb!');
      $(".tombolna").html("");
  }else{
    var exportna = "<input type=\"submit\" name=\"simpan\" value=\"Simpan\" class=\"btn btn-submit\">";
      $(".tombolna").html("");
    $(".tombolna").append(exportna);
  }
});


    $(".form_datetime").datetimepicker({
        format: "yyyy",
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-left",
        minView: 4,
        startView: 4
    });
</script>
@endsection