@extends('layouts.header')  
@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Ubah Juklak
              </header>
              <div class="panel-body">  
                    @foreach($data as $val)
                    <form action="{{url('admin/upload-evaluasi/edit').'/'.$val->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Peserta</label>
                          <select class="form-control" name="id_user">
                            <option value="{{$val->Id}}">{{$val->Name}}</option>
                            <option></option>
                            @if(count($data))
                            @foreach($data as $val)
                            <option value="{{$val->id}}">{{$val->Name}}</option>
                            @endforeach
                            @endif
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Laboratorium</label>
                          <select class="form-control" name="nama_lab">
                            <option value="{{$val->nama_lab}}">{{$val->nama_lab}} - {{$val->Bidang}}</option>
                            <option></option>
                            @if(count($data))
                            @foreach($data as $val)
                            <option value="{{$val->nama_lab}}">{{$val->nama_lab}} - {{$val->Bidang}}</option>
                            @endforeach
                            @endif
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun">
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus">
                            <option value="{{$val->siklus}}">{{$val->siklus}}</option>
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Bidang</label>
                          <select class="form-control" name="bidang">
                            <option value="{{$val->bidang}}">{{$val->Bidang}}</option>
                            <option></option>
                            @if(count($data))
                            @foreach($data as $val)
                            <option value="{{$val->bidang}}">{{$val->Bidang}}</option>
                            @endforeach
                            @endif
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">File</label>
                          <input type="file" class="form-control" name="file">
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
                  @endforeach
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-left",
    minView: 4,
    startView: 4
});
</script>
@endsection