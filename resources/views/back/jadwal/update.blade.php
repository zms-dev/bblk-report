@extends('layouts.navbar')  
@section('content')

<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">
                  Ubah Jadwal
              </header>
              <div class="panel-body">  
                    @foreach($data as $val)
                    <form action="{{url('admin/jadwal/edit').'/'.$val->id}}" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                          <label for="exampleInputEmail1">Judul</label>
                          <input type="input" class="form-control" name="judul" value="{{$val->judul}}">
                      </div>
                      <div class="form-group">
                          <label for="exampleInputEmail1">Isi</label>
                          <textarea name="isi" id="editor1" rows="10" cols="80">
                            {{$val->isi}}
                          </textarea>
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
                  @endforeach
              </div>
          </section>
      </div>
    </div>
  </section>
</section>

<script type="text/javascript" >
    CKEDITOR.replace( 'editor1' );
</script>
@endsection