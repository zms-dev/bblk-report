@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Jadwal PNPME</div>

                <div class="panel-body">
                  {!!$jadwal->isi!!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection