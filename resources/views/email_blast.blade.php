@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Email Blast</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data"  >
                        <div>
                            <label>Siklus</label>
                            <select class="form-control" name="siklus" required>
                                <?php
                                  $date = date('Y-m');
                                  $siklus = date('Y').'-01';
                                ?>
                                @if($date > $siklus)
                                <option value="1" selected>1</option>
                                <option value="2">2</option>
                                @else
                                <option value="2" selected>2</option>
                                <option value="1">1</option>
                                @endif
                            </select>
                            <label>Subjek :</label>
                            <input type="text" name="sub" class="form-control">
                            <label>Deskripsi :</label>
                            <textarea class="form-control" name="desc"></textarea> 
                            <br>
                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                            <input type="submit" name="kirim" class="btn btn-succes" value="Kirim Email">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection