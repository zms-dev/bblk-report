@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Peserta Berdasarkan Type Pembayaran</div>
                <div class="panel-body">
                  <div>
                      <label for="exampleInputEmail1">Tahun</label>
                      <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                          <input size="16" type="text" value="" readonly class="form-control coba" name="tahun" id="tahun" required>
                          <span class="add-on"><i class="icon-th"></i></span>
                      </div>
                  </div>
                  <div>
                      <label for="exampleInputEmail1">Siklus</label>
                      <select name="siklus" id="siklus" class="form-control coba">
                        <option value=""></option>
                        <option value="1">Siklus 1</option>
                        <option value="2">Siklus 2</option>
                      </select>
                  </div>
                  <div id="chart1"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{URL::asset('asset/js/highcharts.js')}}"></script>
<script src="{{URL::asset('asset/js/exporting.js')}}"></script>

<script type="text/javascript">
$(".coba").change(function(){
  var val = $('#tahun').val(), i, no = 0;
  var siklus = $('#siklus').val();

  $(function(){
    var chart1 = new Highcharts.Chart({
      chart: {
        renderTo: 'chart1',
        type: 'pie',
        innerSize: '50%',
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
        events: {
          load: requestDataC1
        }
      },
      title: {
        text : 'Grafik Peserta Berdasarkan Type Pembayaran Tahun ' + val + ' Siklus ' + siklus,
      },
      tooltip: {
          pointFormat: '{series.name}: {point.bukan} - <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
          pie: {
              dataLabels: {
                  enabled: true,
                  distance: -50,
                  style: {
                      fontWeight: 'bold',
                      color: 'white'
                  }
              },
              startAngle: -90,
              endAngle: 90,
              center: ['50%', '75%']
          }
      },
      series: [{
        name: 'Browser share',
        colorByPoint: true,
        data: []
      }]
    });
    function requestDataC1(){
      $.getJSON("{{ URL('/grafik/type-pembayaran')}}",{x:val,y:siklus}, function(json){
        chart1.series[0].setData(json,true)
        console.log(json);
      });
    }
  });
});

</script>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});
</script>
@endsection