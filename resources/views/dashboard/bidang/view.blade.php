 <table border="1" width="100%">
  <thead>
    <tr>
      <th>No</th>
      <th>Bidang Pengujian</th>
      <th>Siklus 1</th>
      <th>Siklus 2</th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody class="body-siklus">
    @if(count($data))
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++; 
      $sik1 = $val->siklus1 + $val->siklus12;
      $sik2 = $val->siklus2 + $val->siklus12;?>
    <tr>
      <td>{{$no}}</td>
      <td>{{$val->parameter}}</td>
      <td>{{$sik1}}</td>
      <td>{{$sik2}}</td>
      <td>{{$val->jumlah}}</td>
    </tr>
    @endforeach
    @endif   
  </tbody>
</table>