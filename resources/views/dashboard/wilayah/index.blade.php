@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Rekapitulasi Peserta Berdasarkan Wilayah</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="wilayah" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control" name="tahun" id="tahun" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div>
                      <br>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                            <th>No</th>
                            <th>Provinsi</th>
                            <th>Siklus 1</th>
                            <th>Siklus 2</th>
                            <th>Total</th>
                        </tr>
                      </thead>
                      <tbody class="body-siklus">
                        
                      </tbody>
                    </table>
                      {{ csrf_field() }}
                    <div class="export">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$("#tahun").change(function(){
  var val = $(this).val(), i, no = 0;
  var y = document.getElementById('datasiklus')
  $(".body-siklus").html("<tr><td colspan='6'>Please Wait... </td></tr>")
  $.ajax({
    type: "GET",
    url : "{{url('wilayah').'/'}}"+val,
    success: function(addr){
      // y.innerHTML = '<tr id="datasiklus"><td id="nosiklus"></td><td id="paramsiklus"></td><td id="bidangsiklus"></td><td id="tarifsiklus"></td></tr>';
      $(".body-siklus").html("");
      if(addr.Hasil != undefined){
        var no = 1;
        $.each(addr.Hasil,function(e,item){
          var sik1 = item.siklus1 + item.siklus12;
          var sik2 = item.siklus2 + item.siklus12;
            var html = "<tr><td class=\"nosiklus\">"+no+"</td><td class=\"paramsiklus\">"+item.name+"</td><td class=\"bidangsiklus\">"+sik1+"</td><td class='tarifsiklus'>"+sik2+"</td><td class='tarifsiklus'>"+item.jumlah+"</td>";
            $(".body-siklus").append(html);
          no++;
        })
        var exportna = "<button type=\"submit\" name=\"print\" class=\"btn btn-info\" onclick=\"printpdf()\">Print PDF</button> <button type=\"submit\" name=\"print\" class=\"btn btn-info\" onclick=\"printexcel()\">Print Excel</button>";
        $(".export").append(exportna);
      }
      return false;
    }
  });
});

$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});

form=document.getElementById("wilayah");
function printpdf() {
        form.action="{{ url('/dashboard/wilayah/pdf')}}";
        form.submit();
}
function printexcel() {
        form.action="{{ url('/dashboard/wilayah/excel')}}";
        form.submit();
}
</script>
@endsection