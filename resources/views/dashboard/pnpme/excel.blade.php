
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<?php
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Evaluasi {$input['bidang']}.xls");
?>
<style type="text/css">
body{
	font-family: arial;
	font-size: 14px;
}
table, td, th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 5px;
}	
a{
	text-decoration: none;
}
</style>
<body>
<h2 style="text-transform: capitalize;">HASIL PESERTA SIPAMELA BBLK JAKARTA PATOLOGI KLINIK SIKLUS {{$input['siklus']}} - {{$input['bidang']}} {{$input['type']}} {{$input['tahun']}}</h2>
<table width="100%" border="1">
	<thead>
		<tr>
			<th rowspan="2"><center>No</center></th>
			<th rowspan="2">Nama Instansi Peserta</th>
			<th rowspan="2">Kode Peserta</th>
			@if(count($parameter))
			@foreach($parameter as $val)
			<th colspan="3"><center>{!!$val->nama_parameter!!}</center></th>
			@endforeach
			@endif
			<th rowspan="2">Catatan</th>
		</tr>
		<tr>
			@if(count($parameter))
			@foreach($parameter as $val)
			<th>Nama Alat</th>
			<th>Metode</th>
			<th>Hasil Peserta</th>
			@endforeach
			@endif
		</tr>
	</thead>
	<tbody>
		@if(count($data))
		<?php $no = 0;?>
		@foreach($data as $val)
		<?php $no++; ?>
		<tr>
			<td><center>{{$no}}</center></td>
			<td>{{$val->instansi}}</td>
			<td>{{$val->kode_lebpes}}</td>
			@if(count($val->h))
			@foreach($val->h as $h)
			<td>{{$h->alat}}</td>
			<td>{{$h->metode_pemeriksaan}}</td>
			<td>{{$h->hasil_pemeriksaan}}</td>
			@endforeach
			@endif
			<td>{{$val->catatan}}</td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>
</div>
</body>
</html>