@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Peserta Berdasarkan Bidang Pengujian</div>
                <div class="panel-body">
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-date="2017" data-link-field="dtp_input1">
                              <input size="16" type="text" value="" readonly class="form-control coba" name="tahun" id="tahun" required>
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div>
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select name="siklus" id="siklus" class="form-control coba">
                            <option value=""></option>
                            <option value="1">Siklus 1</option>
                            <option value="2">Siklus 2</option>
                          </select>
                      </div>
                  <div id="chart1"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{URL::asset('asset/js/highcharts.js')}}"></script>
<script src="{{URL::asset('asset/js/exporting.js')}}"></script>

<script type="text/javascript">
$(".coba").change(function(){
  var val = $('#tahun').val(), i, no = 0;
  var siklus = $('#siklus').val();

  $(function(){
    var chart1 = new Highcharts.Chart({
      chart: {
        renderTo: 'chart1',
        type: 'column',
        events: {
          load: requestDataC1
        }
      },
      title: {
        text : 'Grafik Peserta Berdasarkan Bidang Pengujian Tahun ' + val + ' Siklus ' + siklus
      },
      xAxis: {
        type: 'category'
      },
      yAxis: {
        title: {
          text : 'Bidang Pengujian'
        }
      },
      legend: {
        enabled : false
      },
      plotOptions: {
        series : {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format : '{point.y:.0f} %'
          }
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.bukan}</b><br>'
      },
      series: [{
        name: 'Golongan',
        colorByPoint: true,
        data: []
      }]
    });

    function requestDataC1(){
      $.getJSON("{{ URL('/grafik/bidang-pengujian')}}",{x:val,y:siklus}, function(json){
        chart1.series[0].setData(json,true)
        console.log(json);
      });
    }
  });
});
</script>

<script type="text/javascript">

    $(".form_datetime").datetimepicker({
        format: "yyyy",
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-right",
        minView: 4,
        startView: 4
    });
</script>
@endsection