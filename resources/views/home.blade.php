@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Info Tarif PNPME BBLK Jakarta</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="3" style="text-align: center;">No</th>
                            <th rowspan="3" style="text-align: center;">Bidang</th>
                            <th rowspan="3" style="text-align: center;">Parameter</th>
                            <th colspan="2" style="text-align: center;">Tarif (Rp)</th>
                            <th colspan="4" style="text-align: center;">Kuota</th>
                        </tr>
                        <tr>
                            <th rowspan="2" style="text-align: center;">1 Siklus</th>
                            <th rowspan="2" style="text-align: center;">2 Siklus</th>
                            <th colspan="2" style="text-align: center;">Siklus 1</th>
                            <th colspan="2" style="text-align: center;">Siklus 2</th>
                        </tr>
                        <tr>
                            <th style="text-align: center;">Kuota</th>
                            <th style="text-align: center;">Sisa Kuota</th>
                            <th style="text-align: center;">Kuota</th>
                            <th style="text-align: center;">Sisa Kuota</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++; ?>
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{$val->Bidang}}</td>
                            <td>{{$val->parameter}}</td>
                            <td style="text-align: center;">
                                @if($val->id_bidang < 6 || $val->id_bidang > 7 )
                                    {{number_format($val->tarif / 2)}}
                                @else
                                    {{number_format($val->tarif)}}
                                @endif
                            </td>
                            <td style="text-align: center;">
                            @if($val->id_bidang == 6 || $val->id_bidang == 7)
                                {{number_format($val->tarif * 2)}}
                            @else
                                {{number_format($val->tarif)}}
                            @endif
                            </td>
                            <td style="text-align: center;">{{$val->kuota_1}}</td>
                            <td style="text-align: center;">{{$val->sisa_kuota_1}}</td>
                            <td style="text-align: center;">{{$val->kuota_2}}</td>
                            <td style="text-align: center;">{{$val->sisa_kuota_2}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
