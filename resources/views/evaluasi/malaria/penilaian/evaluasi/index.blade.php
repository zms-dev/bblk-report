@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluasi Malaria</div>

                <div class="panel-body">
                <table class="table table-bordered" id="peserta_pme">
                    <thead>
                        <tr class="titlerowna">
                            <th>Kode Sediaan</th>
                            <th>Hasil Peserta</th>
                            <th>Nilai Acuan</th>
                            <th>Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($datas as $val)
                        <?php $no++; ?>
                        <tr>
                            <td>{!!$val->kode!!}</td>
                            <td>
                                {{ $val->hasil }} 
                                @if($val->hasil == 'Positif')
                                    <br>{{$val->spesies}}
                                    <br>{{$val->stadium}}
                                    <br>{{$val->parasit}} 
                                @endif
                            </td>
                            @foreach($rujuk as $re)
                            @if($re->kode_bahan_uji == $no)
                            <td>
                                {{$re->rujukan}} 
                                @if($re->rujukan == 'Positif')
                                    <br>{{$re->spesies}}
                                    <br>{{$re->stadium}}
                                    <br>{{$re->parasit}} 
                                @endif
                            </td>
                            @endif
                            @endforeach
                            <td class="colomngitung">
                            @foreach($rujuk as $re)
                            @if($re->kode_bahan_uji == $no)
                                @if($re->rujukan == $val->hasil)
                                    @if($re->rujukan == 'Positif')
                                        @if($re->spesies == $val->spesies)
                                            @if($re->stadium == $val->stadium)
                                                <?php $nilai1 = $re->parasit - $re->parasit*(25/100); $nilai2 = $re->parasit + $re->parasit*(25/100);?>
                                                    @if($val->parasit > $nilai1 && $val->parasit < $nilai2)
                                                    10<input type="hidden" name="rujukan[]" value="10">
                                                    @else
                                                    6<input type="hidden" name="rujukan[]" value="6">
                                                    @endif
                                            @else
                                                6<input type="hidden" name="rujukan[]" value="6">
                                            @endif
                                        @else
                                            3<input type="hidden" name="rujukan[]" value="3">
                                        @endif
                                    @else
                                        10<input type="hidden" name="rujukan[]" value="10">
                                    @endif
                                @else
                                    0<input type="hidden" name="rujukan[]" value="0">
                                @endif
                            @endif
                            @endforeach
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        <tr>
                            <th colspan="3">Total :
                            </th>
                            <th class="total"></th>
                        </tr>
                        
                    </tbody>
                </table>
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="total" id="total">
                    <p>Kesimpulan :</p>
                    @if(count($evaluasi))
                        <blockquote>
                            {{$evaluasi->kesimpulan}}
                        </blockquote>
                        <input type="hidden" name="kesimpulan" value="{{$evaluasi->kesimpulan}}">
                    @else
                        <blockquote class="evaluasi">
                        </blockquote>
                        <input type="hidden" name="kesimpulan" id="evaluasi">
                    @endif
                    <div>
                        <h4>Komentar dan Saran :</h4>
                        <table class="table table-bordered"">
                            <tr>
                                <td><center>Jenis Kesalahan</center></td>
                                <td><center>Saran Tindakan</center></td>
                            </tr>
                            <tr>
                                @if(count($evaluasi))
                                <td><textarea class="form-control" name="kesalahan">{{$evaluasi->kesalahan}}</textarea></td>
                                <td><textarea class="form-control" name="tindakan">{{$evaluasi->tindakan}}</textarea></td>
                                @else
                                <td><textarea class="form-control" name="kesalahan"></textarea></td>
                                <td><textarea class="form-control" name="tindakan"></textarea></td>
                                @endif
                            </tr>
                        </table>
                    </div>
                @if(count($evaluasi))
                <a href="{{ url('/hasil-pemeriksaan/malaria/evaluasi/cetak/'. $id.'?y='.$siklus) }}" class="btn btn-success"   target="_BLANK">cetak</a>
                <input type="submit" name="simpan" class="btn btn-primary" value="Update">
                @else
                <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                @endif
                {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var total2=[0,0,0];
$(document).ready(function(){

    var $dataRows=$("#peserta_pme tr:not('.titlerowna')");
    
    $dataRows.each(function() {
        $(this).find('.colomngitung').each(function(i){        
            total2[i]+=parseInt( $(this).html());
        });
    });
    $(".total").each(function(i){  
        $(".total").html(total2[i]);
            document.getElementById("total").value = total2[i];
        if (total2[i] < 70) {
            console.log( "no ready!" );
            $(".evaluasi").html("Buruk");
            $(".ievaluasi").val("Buruk");
            document.getElementById("evaluasi").value = "Buruk";
        }else if(total2[i] < 79){
            $(".evaluasi").html("Baik");
            document.getElementById("evaluasi").value = "Baik";
        }else if(total2[i] < 89){
            $(".evaluasi").html("Sangat Baik");
            document.getElementById("evaluasi").value = "Sangat Baik";
        }else if(total2[i] < 100){
            $(".evaluasi").html("Sempurna");
            document.getElementById("evaluasi").value = "Sempurna";
        }
    });

});
</script>
@endsection