<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}
.peserta_pme td,.peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
</style>
<table width="100%" cellpadding="0" border="0" style="margin-top: -30px">
    <thead>
        <tr>
            <th>
               <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
            </th>
            <th width="100%">
                <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                <span style="font-size: 12px">PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MALARIA {{$siklus}} TAHUN {{$tahun}}</span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -25px; margin-left: -42px;"> 
                    Penyelenggara : Balai Besar Laboratorium Kesehatan Jakarta
                                                Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560
                                                Telp. (021) 4212524, 42804339 Facsimile (021) 4245516
                                                Website : bblkjakarta.com ; Surat Elektronik : bblkjakarta@yahoo.co.id
                </pre>
            </th>
            <th>
                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" height="90px">
            </th>
        </tr>
        <tr>
            <th colspan="3"><hr></th>
        </tr>
    </thead>
</table>

<center><label><b>LAMPIRAN EVALUASI MALARIA<br>
PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL MIKROBIOLOGI<br> MALARIA SIKLUS {{$siklus}} TAHUN {{$tahun}}
</b><input type="hidden" name="type" value="{{$siklus}}"></label></center><br>
<form class="form-horizontal" method="post" enctype="multipart/form-data">
<table>
  <tr>
    <td>Sifat</td>
    <td>:</td>
    <th>Rahasia</th>
  </tr>
  <tr>
    <td>Kode Lab Peserta</td>
    <td>:</td>
    <td>{{$register->kode_lebpes}}</td>
  </tr>
  <tr>
    <td>Nama Instansi</td>
    <td>:</td>
    <td>{{$perusahaan}}</td>
  </tr>
</table><br>
<table class="peserta_pme" width="100%">
    <tr class="titlerowna" style="text-align: center;">
        <th>Kode Sediaan</th>
        <th>Hasil Peserta</th>
        <th>Nilai Acuan</th>
        <th>Nilai</th>
    </tr>
    <tbody>
        @if(count($data))
        <?php $no = 0; ?>
        @foreach($datas as $val)
        <?php $no++; ?>
        <tr>
            <td>{!!$val->kode!!}</td>
            <td>
                {{ $val->hasil }} 
                @if($val->hasil == 'Positif')
                    <br>{{$val->spesies}}
                    <br>{{$val->stadium}}
                    <br>{{$val->parasit}} 
                @endif
            </td>
            @foreach($rujuk as $re)
            @if($re->kode_bahan_uji == $no)
            <td>
                {{$re->rujukan}} 
                @if($re->rujukan == 'Positif')
                    <br>{{$re->spesies}}
                    <br>{{$re->stadium}}
                    <br>{{$re->parasit}} 
                @endif
            </td>
            @endif
            @endforeach
            <td class="colomngitung">
            @foreach($rujuk as $re)
            @if($re->kode_bahan_uji == $no)
                @if($re->rujukan == $val->hasil)
                    @if($re->rujukan == 'Positif')
                        @if($re->spesies == $val->spesies)
                            @if($re->stadium == $val->stadium)
                                <?php $nilai1 = $re->parasit - $re->parasit*(25/100); $nilai2 = $re->parasit + $re->parasit*(25/100);?>
                                    @if($val->parasit > $nilai1 && $val->parasit < $nilai2)
                                    10<input type="hidden" name="rujukan[]" value="10">
                                    @else
                                    6<input type="hidden" name="rujukan[]" value="6">
                                    @endif
                            @else
                                6<input type="hidden" name="rujukan[]" value="6">
                            @endif
                        @else
                            3<input type="hidden" name="rujukan[]" value="3">
                        @endif
                    @else
                        10<input type="hidden" name="rujukan[]" value="10">
                    @endif
                @else
                    0<input type="hidden" name="rujukan[]" value="0">
                @endif
            @endif
            @endforeach
            </td>
        </tr>
        @endforeach
        @endif
        <tr>
            <th colspan="3">Total :
            </th>
            <th class="total">{{$evaluasi->total}}</th>
        </tr>
    </tbody>
</table><br>
<h4>Kesimpulan :</h4>
<blockquote>
    {{$evaluasi->kesimpulan}}
</blockquote>
<h4>Komentar dan Saran :</h4>
<table class="peserta_pme" width="100%">
    <tr>
        <td><center>Jenis Kesalahan</center></td>
        <td><center>Saran Tindakan</center></td>
    </tr>
    <tr>
        <td>{{$evaluasi->kesalahan}}</td>
        <td>{{$evaluasi->tindakan}}</td>
    </tr>
</table>

<br>
<div style="margin-left: 550px;">
<div style="position: relative; top: 17px">
Jakarta, 31 Juli 2018<br>
Manajer Teknis
</div>
<p>
    <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'TTD4.png')}}" width="80" height="80" style="margin-left: 20px !important;">
</p>

<div style="position: relative; top:-20px">
Ita Andayani, S.St<br>
NIP.197004101992032003
</div><br>
</div>
    