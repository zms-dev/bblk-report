<table border="1" width="100%">
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Kode</th>
        <?php for ($i=1; $i < 11; $i++) { ?>
        <th>Hasil</th>
        <th>Spesies</th>
        <th>Stadium</th>
        <th>Parasit</th>
        <?php } ?>
        <th rowspan="2">Total Nilai</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <?php for ($i=1; $i < 11; $i++) { ?>
        <th colspan="4">{{$i}}/{{$input['siklus']}}/{{$input['tahun']}}</th>
        <?php } ?>
        <th></th>
    </tr>
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++; ?>
    <tr>
        <td>{{$no}}</td>
        <td>{{$val->kode_lebpes}}</td>
        @foreach($val->hasil as $hasil)
        <td>{{$hasil->hasil}}</td>
        <td>{{$hasil->spesies}}</td>
        <td>{{$hasil->stadium}}</td>
        <td>{{$hasil->parasit}}</td>
        @endforeach
        <td>{{$val->total}}</td>
    </tr>
    @endforeach
</table>