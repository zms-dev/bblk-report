@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Peserta TC</div>
                <div class="panel-body">
                    <table class="table table-bordered" id="table">
                        <thead>
                            <tr>
                                <th>Badan Usaha</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($badan as $val)
                            <tr>
                                <td>{{$val->badan_usaha}}</td>
                                <td>{{$val->jumlah}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                  <input type="button" class="btn" value="Select Data" onclick="selectElementContents( document.getElementById('table') );">
                  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function selectElementContents(el) {
    var body = document.body, range, sel;
    if (document.createRange && window.getSelection) {
        range = document.createRange();
        sel = window.getSelection();
        sel.removeAllRanges();
        try {
            range.selectNodeContents(el);
            sel.addRange(range);
        } catch (e) {
            range.selectNode(el);
            sel.addRange(range);
        }
    } else if (body.createTextRange) {
        range = body.createTextRange();
        range.moveToElementText(el);
        range.select();
        range.execCommand("Copy");
    }
}

Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Jumlah Peserta yang Mengikuti PME TC'
    },
    tooltip: {
        pointFormat: '{series.name}</b>'
    },
    yAxis: {
        title: {
            text: 'Jumlah Peserta'
        }
    },
    xAxis: {
        categories: [
                @foreach($badan as $val)
                  '{{$val->badan_usaha}}',
                @endforeach
                  ]
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },
    series: [
        {
            'name': 'Jumlah Peserta',
            'colorByPoint': true,
            'data': [
                @foreach($badan as $val)
                
                {
                    'name' :'{{$val->badan_usaha}}',
                     'y'   :<?=$val->jumlah?>
                 },
                
                @endforeach
            ]
        }
    ]
});



</script>
@endsection
