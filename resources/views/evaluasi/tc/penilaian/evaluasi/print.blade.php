<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th{
    padding: 2px 5px;
}
td{
    padding: -8px 3px;
}
.ttd td{
    padding: 2px 5px;
}

th{
    text-align: center;
}

.header{
position: fixed;
}

.header {
top: 0;
}

#footer { 
    position: fixed; 
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<table width="100%" cellpadding="0" border="0" style="margin-top: -30px">
    <thead>
        <tr>
            <th>
               <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
            </th>
            <th></th>
            <th width="100%" style="text-align: center;">
                <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                <span style="font-size: 12px">PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br>MIKROSKOPIS TELUR CACING TAHUN {{$tahun}}</span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -25px; margin-left: -42px;"> 
                Penyelenggara : Balai Besar Laboratorium Kesehatan Jakarta
                Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560
                Telepon : (021) 4212524, 42804339 Fax. (021) 4245516
                Email : bblkjakarta@yahoo.co.id
                </pre>
            </th>
            <th>
              <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" height="75px">
            </th>
        </tr>
        <tr>
            <th colspan="4" style="padding: 0%"><hr></th>
        </tr>
    </thead>
</table>
<center><label><b>KEMENTERIAN KESEHATAN RI PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br> MIKROSKOPIS TELUR CACING SIKLUS @if($siklus == 1) I @else II  @endif TAHUN {{$tahun}}
</b><input type="hidden" name="type" value="{{$siklus}}"></label></center><br>
<br>
<table>
    <tr>
        <th style="text-align: left;">Kode Peserta</th>
        <td>&nbsp;:&nbsp;</td>
        <td>{{$register->kode_lebpes}}</td>
    </tr>
    <tr>
        <th style="text-align: left;">Nama Instansi</th>
        <td>&nbsp;:&nbsp;</td>
        <td>{{$perusahaan}}</td>
    </tr>
</table><br>
<table border="1" width="100%">
    <thead>
        <tr class="titlerowna">
            <th>Kode Sediaan</th>
            <th>Nilai Acuan</th>
            <th>Hasil Peserta</th>
            <th>Skor</th>
        </tr>
    </thead>
    <tbody>
        @if($evaluasi != NULL)

        @endif
        @if($data != NULL)
        <?php $no = 0; $i =0;$ab = ['A','B','C','D']?>
        @foreach($datas as $val)
        <?php $no++; ?>
        <tr>
            <td>TC-{!!$val->kode_botol!!}{{$ab[$i]}}</td>
            <td>
                @if($rujuk != NULL)
                    {!!$rujuk[$i]->rujukan!!}
                @endif
            </td>
            <td>
                {!!html_entity_decode($val->hasil) !!}
            </td>
            <td class="colomngitung">
                @if($evaluasi != NULL)
                    {{$evaluasi[$i]->nilai}}
                @else
                <td></td>
                @endif
            </td>
        </tr>
        <?php $i++;?>
        @endforeach
        @endif
        <tr>
            <th colspan="3">Total Nilai:
            </th>
            <th class="total">@if($nilaievaluasi->nilai >= 9.99) 10 @else {{$nilaievaluasi->nilai}} @endif</th>
        </tr>
    </tbody>
</table>
<div>
    <h4>
        Kriteria :
        <span style="text-decoration: underline;">
            @if($nilaievaluasi != NULL)
                @if($nilaievaluasi->nilai < 6.66)
                    Kurang
                @elseif($nilaievaluasi->nilai >= 6.66)
                    Baik
                @endif
            @endif
        </span>
    </h4>
</div>
@if($salah != NULL)
@if($salah->salah_spesies == "ada" || $salah->negatif_palsu == "ada" || $salah->spesies_kurang == "ada" || $salah->positif_palsu == "ada")
<table class="" border="1" style="" style="width: 30%;">
    <tr>
        <th>Jenis Kesalahan</th>
        <th>Kemungkinan Penyebab</th>
        <th>Saran Tindakan</th>
    </tr>
        @if(($salah->salah_spesies=="ada"))
        <tr>
             <td>@if($salah->salah_spesies == "ada") Salah Spesies @endif</td>
             <td>
                <ul>
                    <li style="list-style: none;">Petugas tidak mengenal bentuk morfologi telur cacing dengan baik</li>
                </ul>
            </td>
             <td>
                <ol>
                    <li type="a">
                        Cocokkan morfologi telur cacing yang ditemukan dengan atlas
                    </li>
                    <li type="a">
                        Mengikuti pelatihan mikroskopis parasitologi
                    </li>
                </ol>
             </td>
        </tr> 
        @endif
        @if(($salah->negatif_palsu == "ada"))
        <tr>
             <td>@if($salah->negatif_palsu == "ada") Negatif Palsu @endif</td>
             <td>
                <ol>
                    <li type="a">Tidak melakukan pemeriksaan pada seluruh lapang pandang</li>
                    <li type="a">Tidak cermat dalam mengamati preparat feses sehingga tidak dapat menemukan adanya telur cacing</li>
                </ol>
             </td>
             <td style="vertical-align: top;">
                <ol>
                    <li type="a">Homogenkan suspensi tinja sebelum pemeriksaan</li>
                    <li type="a">Melakukan pemeriksaan mikroskopik pada semua lapang pandang  dengan cermat</li>
                </ol>
            </td>
        </tr>
        @endif
        @if(($salah->spesies_kurang == "ada"))
        <tr>
             <td>@if($salah->spesies_kurang == "ada") Jumlah Spesies Kurang @endif</td>
             <td style="vertical-align: top">
                <ol>
                    <li type="a">Tidak melakukan pemeriksaan pada seluruh lapang pandang</li>
                    <li type="a">Petugas tidak mengenal bentuk morfologi telur cacing dengan baik</li>
                </ol>
             </td>
             <td style="vertical-align: top;">
                <ol>
                    <li type="a">Melakukan pemeriksaan mikroskopik pada semua lapang pandang dengan cermat.</li>
                    <li type="a">Cocokkan morfologi telur cacing yg ditemukan dengan atlas</li>
                    <li type="a">Mengikuti pelatihan mikroskopik parasitologi</li>
                </ol>
            </td>
        </tr>
        @endif
        @if(($salah->positif_palsu == "ada"))
        <tr>
             <td>@if($salah->positif_palsu == "ada") Positif Palsu @endif</td>
             <td style="vertical-align: top">
                <ol>
                    <li type="a">Petugas tidak mengenal bentuk morfologi telur cacing dengan baik</li>
                </ol>
             </td>
             <td style="vertical-align: top;">
                <ol>
                    <li type="a">Mengikuti pelatihan mikroskopis parasitologi</li>
                </ol>
            </td>
        </tr>
        @endif
</table>
@endif
@endif

<br>
    @if($salah->saran == null)
        @else
            <h4>Saran :</h4>
            {!! $salah->saran !!}
    @endif
<h4>Keterangan :</h4>
<p style="font-family: DejaVu Sans; sans-serif;">Skor &#8805; 6.6 : Baik<br>Skor &lt; 6.6 : Kurang</p>
<!-- <table style="font-family: DejaVu Sans; sans-serif;">
    <tr>
        <td>Skor &#8805; 6.6</td>
        <td>:</td>
        <td>Baik</td>
    </tr>
    <tr>
        <td>Skor &lt; 6.6</td>
        <td>:</td>
        <td>Kurang</td>
    </tr>
</table> -->
<div id="footer">
    EVALUASI PNPME MIKROSKOPIS TELUR CACING SIKLUS @if($siklus == '1') I @else II @endif TAHUN {{$tahun}}
</div>

<div style="margin-left: 520px !important; margin-top: 0% !important; " >
<table border="" style="page-break-inside: avoid;" class="ttd">
    <tr>
        <td style="z-index: 2;">
            Jakarta, @if($tanggalevaluasi != NULL){{_dateIndo($tanggalevaluasi->tanggal)}} @endif
            <br>
            
        </td>
    </tr>
    <tr>
        <td>
            Penanggungjawab 
            <br>
            Lab. Mikrobiologi Klinik
            <br>
        </td>
    </tr>
    <tr>
        <td style="">
             <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'telurCacing.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'telurCacing.png')}}" height="60px"  style=" /*border:1px solid black;*/ width: 80% !important; margin-left: -0px !important; z-index: 1;">
        </td>
    </tr>
    <tr>
        <td style="">
            dr. Nita Nurhidayati, Sp.MK
        </td>
    </tr>
</table>
</div>
{{-- <div style="margin-left:553px; ">
<div style="position: relative; top: 17px;">
Jakarta, 31 Juli 2018<br>
Manajer Teknis
</div>
<p>
<img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'telur.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'telur.png')}}" height="180px" style="position: absolute; top: -28%;">
</p>

<div style="position: relative; top:72px">
dr. Nita Nurhidayati, Sp.MK
</div>
</div><br> --}}
<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>



    