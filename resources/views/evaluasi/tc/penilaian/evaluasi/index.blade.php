@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Penilaian TC</div>

                <div class="panel-body">
                @if($evaluasi != NULL)
                    <form class="form-horizontal" method="post" id="someForm" enctype="multipart/form-data" >
                @else
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                @endif

                <table>
                    <tr>
                        <th>Kode Peserta</th>
                        <td>&nbsp;:&nbsp;</td>
                        <td>{{$register->kode_lebpes}}</td>
                    </tr>
                    <tr>
                        <th>Nama Instansi</th>
                        <td>&nbsp;:&nbsp;</td>
                        <td>{{$perusahaan}}</td>
                    </tr>
                </table><br>
                <table class="table table-bordered" id="peserta_pme">
                    <thead>
                        <tr class="titlerowna">
                            <th>Kode Sediaan</th>
                            <th>Nilai Acuan</th>
                            <th>Hasil Peserta</th>
                            <th>Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($evaluasi != NULL)
                        @foreach($evaluasi as $val)
                            <input type="hidden" name="id[]" value="{{$val->id}}">
                        @endforeach
                        @endif
                        @if($data != NULL)
                        <?php $no = 0; $i=0; $ab = ['A','B','C','D']?>
                        @foreach($datas as $val)
                        <?php $no++; ?>
                        <tr>
                            <td>TC-{!!$val->kode_botol!!}{{$ab[$i]}}<input type="hidden" name="kode_sediaan[]" value="{!!$val->kode_botol!!}"></td>
                            <td>
                                @if($rujuk != NULL)
                                        {!!$rujuk[$i]->rujukan!!}
                                @endif
                            </td>
                            <td>
                                {!!html_entity_decode($val->hasil) !!}
                            </td>
                            <td class="colomngitung">
                                @if($evaluasi != NULL)
                                    <input type="number" max="3.33"  step="0.01" min="0" name="nilai[]" value="{{$evaluasi[$i]->nilai}}">
                                @else
                                <input type="number" max="3.33" step="0.01" min="0" name="nilai[]" required step="any">
                                @endif
                            </td>
                        </tr>
                        <?php $i++;?>
                        @endforeach
                        @endif
                        <tr>
                            <th colspan="3">Total Nilai:
                            </th>
                            <th class="total">{{ ($nilaievaluasi->nilai >= 9.99) ? 10 : $nilaievaluasi->nilai }}</th>
                        </tr>
                    </tbody>
                </table>
                <div>
                    @if($evaluasi != NULL)
                    <h4>Kesimpulan :
                        <span style="text-decoration: underline;">
                            @if($nilaievaluasi != NULL)
                                @if($nilaievaluasi->nilai < 6.66)
                                    Kurang
                                @elseif($nilaievaluasi->nilai >= 6.66)
                                    Baik
                                @endif
                            @endif
                        </span>
                    </h4>
                    @endif
                </div>
                <br>
                <div>
                    <table class="table table-bordered" style="" style="width: 30%;">
                        <tr>
                            <th width="10%">Pilih</th>
                            <th>Jenis Kesalahan</th>
                        </tr>
                        @if($salah != NULL)
                        <tr>
                            <td><input type="checkbox" name="salah_spesies" value="ada"
                                @if($salah->salah_spesies == "ada")
                                    checked="1" 
                                @endif
                                ></td>
                            <td>Salah Spesies</td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="negatif_palsu" value="ada"
                                @if($salah->negatif_palsu == "ada")
                                    checked="1" 
                                @endif
                                ></td>
                            <td>Negatif Palsu</td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="spesies_kurang" value="ada"
                                @if($salah->spesies_kurang == "ada")
                                    checked="1" 
                                @endif
                                ></td>
                            <td>Jumlah Spesies Kurang</td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="positif_palsu" value="ada"
                                @if($salah->positif_palsu == "ada")
                                    checked="1" 
                                @endif
                                ></td>
                            <td>Positif palsu</td>
                        </tr>
                        @else
                         <tr>
                            <td><input type="checkbox" name="salah_spesies" value="ada"></td>
                            <td>Salah Spesies</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="negatif_palsu" value="ada"></td>
                            <td>Negatif Palsu</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="spesies_kurang" value="ada"></td>
                            <td>Jumlah Spesies Kurang</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="positif_palsu" value="ada"></td>
                            <td>Positif palsu</td>
                        </tr>
                        @endif
                    </table>
                </div>
                <div><br>
                        <table class="table table-bordered"">
                            <tr>
                                <td><center>Saran (maksimal 300 huruf)</center></td>
                            </tr>
                            <tr>
                                @if($salah != NULL)
                                <td><textarea class="form-control" id="mytext" name="saran" >{{ $salah->saran }}</textarea></td>
                                @else
                                <td><textarea class="form-control" id="mytext" name="saran" ></textarea></td>
                                @endif
                            </tr>
                        </table>
                    </div>
                 @if($evaluasi != NULL)
                <a href="{{ url('hasil-pemeriksaan/telur-cacing/evaluasi/print/'. $id.'?y='.$siklus) }}" class="btn btn-success"   target="_BLANK">cetak</a>
                <input type="hidden" name="method" value="Update">
                <input type="submit" class="btn btn-primary" value="Update" name="simpan" onclick="Update()" />
                @else
                <input type="hidden" name="method" value="Simpan">
                <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                @endif
                {{ csrf_field() }}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

CKEDITOR.replace('mytext');

         
    CKEDITOR.instances.mytext.on('key',function(event){
        var deleteKey = 46;
        var backspaceKey = 8;
        var keyCode = event.data.keyCode;
        if (keyCode === deleteKey || keyCode === backspaceKey)
            return true;
        else
        {
            var textLimit = 300;
            var str = CKEDITOR.instances.mytext.getData();
            if (str.length >= textLimit)
                return false;
        }
    }); 


form=document.getElementById("someForm");
function Update() {
    form.action="{{url('hasil-pemeriksaan/telur-cacing/evaluasi/update').'/'.$id.'?y='.$siklus}}";
    form.submit();
}
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});

</script>
@endsection