<table border="1">
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Nama Instansi</th>
        <th rowspan="2">Kode</th>
        <?php for ($i=1; $i < 4; $i++) { ?>
        <th>Rujukan</th>
        <th>Hasil Peserta</th>
        <th>Nilai</th>
        <?php } ?>
        <th rowspan="2">Total Nilai</th>
        <th rowspan="2">Catatan</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <?php for ($i=1; $i < 4; $i++) { ?>
        <th colspan="3">{{$i}}/{{$input['siklus']}}/{{$input['tahun']}}</th>
        <?php } ?>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++; ?>
    <tr>
        <td>{{$no}}</td>
        <td>{{$val->nama_lab}}</td>
        <td>{{$val->kode_lebpes}}</td>
            <?php
                $strhasil = html_entity_decode($val->input[0]->hasil);
                $strhasil = str_replace(["<p>","<li>","<ol>","</ol>","<ul>","</ul>"], "", $strhasil);
                $strhasil = str_replace(["</p>","</li>"], " <br> ", $strhasil);
                $strhasil = str_replace("em>", "i>", $strhasil);
            ?>
        @if(count($rujukan))
            <td>{!! html_entity_decode($rujukan[0]->rujukan)!!}</td>
        @else
            <td></td>
        @endif
            <td>{!! $strhasil !!}</td>
        @if(count($val->hasil))
            <td>{{$val->hasil[0]->nilai}}</td>
        @else
            <td></td>
        @endif
            <?php
                $strhasil = html_entity_decode($val->input[1]->hasil);
                $strhasil = str_replace(["<p>","<li>","<ol>","</ol>","<ul>","</ul>"], "", $strhasil);
                $strhasil = str_replace(["</p>","</li>"], " <br> ", $strhasil);
                $strhasil = str_replace("em>", "i>", $strhasil);
            ?>
        @if(count($rujukan))
            <td>{!! html_entity_decode($rujukan[1]->rujukan)!!}</td>
        @else
            <td></td>
        @endif
            <td>{!! $strhasil !!}</td>
        @if(count($val->hasil))
            <td>{{$val->hasil[1]->nilai}}</td>
        @else
            <td></td>
        @endif
            <?php
                $strhasil = html_entity_decode($val->input[2]->hasil);
                $strhasil = str_replace(["<p>","<li>","<ol>","</ol>","<ul>","</ul>"], "", $strhasil);
                $strhasil = str_replace(["</p>","</li>"], " <br> ", $strhasil);
                $strhasil = str_replace("em>", "i>", $strhasil);
            ?>
        @if(count($rujukan))
            <td>{!! html_entity_decode($rujukan[2]->rujukan)!!}</td>
        @else
            <td></td>
        @endif
            <td>{!! $strhasil !!}</td>
        @if(count($val->hasil))
            <td>{{$val->hasil[2]->nilai}}</td>
            <td>{{$val->total}}</td>
        @else
            <td></td>
            <td></td>
        @endif
        <td>{{$val->input[0]->catatan}}</td>
    </tr>
    @endforeach
</table>