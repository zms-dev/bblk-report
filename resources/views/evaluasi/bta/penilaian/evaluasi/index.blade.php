@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Penilaian BTA</div>

                <div class="panel-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                <table>
                    <tr>
                        <th>Kode Peserta</th>
                        <td>&nbsp;:&nbsp;</td>
                        <td>{{substr($kode->kode_peserta,0,12)}}</td>
                    </tr>
                    <tr>
                        <th>Nama Instansi</th>
                        <td>&nbsp;:&nbsp;</td>
                        <td>{{$perusahaan}}</td>
                    </tr>
                </table><br>
                <table class="table table-bordered" id="peserta_pme">
                    <thead>
                        <tr class="titlerowna">
                            <th>Kode Sediaan</th>
                            <th>Nilai Acuan</th>
                            <th>Hasil Peserta</th>
                            <th>Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($evaluasi != NULL)
                        @foreach($evaluasi as $val)
                            <input type="hidden" name="id[]" value="{{$val->id}}">                            
                        @endforeach
                        @endif
                        @if($data != NULL)
                        <?php $no = 0; $i = 0; ?>
                        @foreach($datas as $val)
                        <?php $no++; ?>
                        <tr>
                            <td>{!!$val->kode!!}<input type="hidden" value="{!!$val->kode!!}" name="kode_sediaan[]"></td>
                            @if($rujuk != NULL)
                            <td>
                                @foreach($rujuk as $re)
                                    @if($re->kode_bahan_uji == $no)
                                    {{$re->rujukan}}@if($re->kuman != NULL): {{$re->kuman}}@endif
                                    @endif
                                @endforeach
                            </td>
                            @else
                            <td></td>
                            @endif
                            <td>{{ $val->hasil }} @if($val->kuman != NULL): {{ $val->kuman }} @endif</td>
                            <td>
                                <select name="nilai[]">
                                    @if($evaluasi != NULL)
                                        @foreach($evaluasi as $valu)
                                            @if($valu->kode_sediaan == $val->kode)
                                            <option value="{{$valu->keterangan}}">{{$valu->keterangan}} : {{$valu->nilai}} </option>
                                            @endif
                                        @endforeach
                                    @else
                                        @if($rujuk != NULL)
                                            @if($val->hasil == 'Negatif' && $rujuk[$i]->rujukan == 'Negatif')
                                                <option value="Benar">Benar : 10</option>
                                            @elseif($val->hasil == 'Negatif' && $rujuk[$i]->rujukan == 'Scanty')
                                                <option value="NPR">NPR : 5</option>
                                            @elseif($val->hasil == 'Negatif' && $rujuk[$i]->rujukan == '1+')
                                                <option value="NPT">NPT : 0</option>
                                            @elseif($val->hasil == 'Negatif' && $rujuk[$i]->rujukan == '2+')
                                                <option value="NPT">NPT : 0</option>
                                            @elseif($val->hasil == 'Negatif' && $rujuk[$i]->rujukan == '3+')
                                                <option value="NPT">NPT : 0</option>

                                            @elseif($val->hasil == 'Scanty' && $rujuk[$i]->rujukan == 'Scanty')
                                                <option value="Benar">Benar : 10</option>
                                            @elseif($val->hasil == 'Scanty' && $rujuk[$i]->rujukan == 'Negatif')
                                                <option value="PPR">PPR : 5</option>
                                            @elseif($val->hasil == 'Scanty' && $rujuk[$i]->rujukan == '1+')
                                                <option value="Benar">Benar : 10</option>
                                            @elseif($val->hasil == 'Scanty' && $rujuk[$i]->rujukan == '2+')
                                                <option value="KH">KH : 5</option>
                                            @elseif($val->hasil == 'Scanty' && $rujuk[$i]->rujukan == '3+')
                                                <option value="KH">KH : 5</option>

                                            @elseif($val->hasil == '1+' && $rujuk[$i]->rujukan == 'Negatif')
                                                <option value="PPT">PPT : 0</option>
                                            @elseif($val->hasil == '1+' && $rujuk[$i]->rujukan == 'Scanty')
                                                <option value="Benar">Benar : 10</option>
                                            @elseif($val->hasil == '1+' && $rujuk[$i]->rujukan == '1+')
                                                <option value="Benar">Benar : 10</option>
                                            @elseif($val->hasil == '1+' && $rujuk[$i]->rujukan == '2+')
                                                <option value="Benar">Benar : 10</option>
                                            @elseif($val->hasil == '1+' && $rujuk[$i]->rujukan == '3+')
                                                <option value="KH">KH : 5</option>

                                            @elseif($val->hasil == '2+' && $rujuk[$i]->rujukan == 'Negatif')
                                                <option value="KH">KH : 5</option>
                                            @elseif($val->hasil == '2+' && $rujuk[$i]->rujukan == 'Scanty')
                                                <option value="PPT">PPT : 0</option>
                                            @elseif($val->hasil == '2+' && $rujuk[$i]->rujukan == '1+')
                                                <option value="Benar">Benar : 10</option>
                                            @elseif($val->hasil == '2+' && $rujuk[$i]->rujukan == '2+')
                                                <option value="Benar">Benar : 10</option>
                                            @elseif($val->hasil == '2+' && $rujuk[$i]->rujukan == '3+')
                                                <option value="Benar">Benar : 10</option>

                                            @elseif($val->hasil == '3+' && $rujuk[$i]->rujukan == 'Negatif')
                                                <option value="KH">KH : 5</option>
                                            @elseif($val->hasil == '3+' && $rujuk[$i]->rujukan == 'Scanty')
                                                <option value="PPT">PPT : 0</option>
                                            @elseif($val->hasil == '3+' && $rujuk[$i]->rujukan == '1+')
                                                <option value="KH">KH : 5</option>
                                            @elseif($val->hasil == '3+' && $rujuk[$i]->rujukan == '2+')
                                                <option value="Benar">Benar : 10</option>
                                            @elseif($val->hasil == '3+' && $rujuk[$i]->rujukan == '3+')
                                                <option value="Benar">Benar : 10</option>
                                            @else
                                                <option value=""></option>
                                            @endif
                                        @else
                                            <option value=""></option>
                                        @endif
                                    @endif
                                    <option value="Benar">Benar : 10</option>
                                    <option value="KH">KH : 5</option>
                                    <option value="PPR">PPR : 5</option>
                                    <option value="NPR">NPR : 5</option>
                                    <option value="PPT">PPT : 0</option>
                                    <option value="NPT">NPT : 0</option>
                                </select>
                            </td>
                        </tr>
                        <?php $i++;?>
                        @endforeach
                        @endif
                        <tr>
                            @if($status != NULL)
                            <th colspan="3">Total</th>
                            <th>{{$skor}}</th>
                            @endif
                        </tr>
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-md-6">
                        <div>
                            <label for="exampleInputEmail1">Status</label>
                            <select class="form-control" name="status" required>
                                @if($status != NULL)
                                <option value="{{$status->status}}">{{$status->status}}</option>
                                @else
                                <option></option>
                                @endif
                                <option value="Lulus">Lulus</option>
                                <option value="Tidak Lulus">Tidak Lulus</option>
                            </select>
                        </div>
                    </div>
                </div><br>
                <table class="table table-bordered" style="" style="width: 30%;">
                        <tr>
                            <th width="10%">Pilih</th>
                            <th>Jenis Kesalahan</th>
                        </tr>
                         @if($saran != NULL)
                         <tr>
                            <td><input type="checkbox" name="kh" value="ada"
                                @if($saran->kh == "ada")
                                    checked="1" 
                                @endif
                                ></td>
                            <td>Kesalahan Hitung</td>
                        </tr>
                         <tr>
                            <td><input type="checkbox" name="npr" value="ada"
                                @if($saran->npr == "ada")
                                    checked="1" 
                                @endif
                                ></td>
                            <td>NPR</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="npt" value="ada"
                                 @if($saran->npt == "ada")
                                    checked="1" 
                                @endif
                                ></td>
                            <td>NPT</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="ppr" value="ada"
                                 @if($saran->ppr == "ada")
                                    checked="1" 
                                @endif
                                ></td>
                            <td>PPR</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="ppt" value="ada"
                                 @if($saran->ppt == "ada")
                                    checked="1" 
                                @endif
                                ></td>
                            <td>PPT</td>
                        </tr>
                        @else
                        <tr>
                            <td><input type="checkbox" name="kh" value="ada"></td>
                            <td>Kesalahan Hitung</td>
                        </tr>
                         <tr>
                            <td><input type="checkbox" name="npr" value="ada"></td>
                            <td>NPR</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="npt" value="ada"></td>
                            <td>NPT</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="ppr" value="ada"></td>
                            <td>PPR</td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="ppt" value="ada"></td>
                            <td>PPT</td>
                        </tr>
                        @endif
                    </table>
                    <!-- <label>Tanggal TTD :</label>
                    <div class="controls input-append date" data-link-field="dtp_input1">
                        @if($saran != NULL)
                        <input size="16" type="text" value="{{$saran->tanggal_ttd}}" autocomplete="off" required class="form_datetime form-control" name="ttd">
                        @else
                        <input size="16" type="text" value="" required autocomplete="off" class="form_datetime form-control" name="ttd">
                        @endif
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div><br> -->
                @if($evaluasi != NULL)
                <input type="submit" name="simpan" class="btn btn-primary" value="Update">
                <a href="{{ url('/hasil-pemeriksaan/bta/evaluasi/cetak/'. $id.'?y='.$siklus) }}" class="btn btn-success"   target="_BLANK">Cetak</a>
                @else
                <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                @endif
                {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
</script>
@endsection