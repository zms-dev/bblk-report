<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}

#footer { 
    position: fixed; 
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
.page-num:after {
        counter-increment: page;
        content: counter(page);
}
</style>
<table width="100%" cellpadding="0" border="0" style="margin-top: -30px">
    <thead>
        <tr>
            <th>
               <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
            </th>
            <th></th>
            <th width="100%" style="text-align: center;">
                <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                <span style="font-size: 12px">PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br>MIKROSKOPIS BTA TAHUN {{$tahun}}</span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -25px; margin-left: -42px;"> 
                Penyelenggara : Balai Besar Laboratorium Kesehatan Jakarta
                Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560
                Telepon : (021) 4212524, 42804339 Fax. (021) 4245516
                Email : bblkjakarta@yahoo.co.id
                </pre>
            </th>
            <th>
              <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" height="75px">
            </th>
        </tr>
        <tr>
            <th colspan="4"><hr></th>
        </tr>
    </thead>
</table>

<center><label><b>KEMENTERIAN KESEHATAN RI<br>
PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br> MIKROSKOPIS BTA SIKLUS @if($siklus==1) I @else II @endif TAHUN {{$tahun}}
</b><input type="hidden" name="type" value="{{$siklus}}"></label></center><br>
<table>
  <tr>
    <th>Kode Peserta</th>
    <td>:</td>
    <td>{{substr($kode->kode_peserta,0,12)}}</td>
  </tr>
  <tr>
    <th>Nama Instansi</th>
    <td>:</td>
    <td>{{$perusahaan}}</td>
  </tr>
</table>
<br><br>
<table border="1" width="100%" style="">
    <tr class="titlerowna" style="text-align: center;">
        <th>Kode Sediaan</th>
        <th>Nilai Acuan</th>
        <th>Hasil Peserta</th>
        <th>Hasil Evaluasi</th>
        <th>Skor</th>
    </tr>
     @if($data != NULL)
        <?php 
          $no = 0;
          $kh = 0;
          $npr = 0;
          $ppr = 0;
          $npt = 0;
          $ppt = 0;
        ?>
        @foreach($datas as $val)
          <?php $no++; ?>
        <tr>
            <td>{!!$val->kode!!}<input type="hidden" value="{!!$val->kode!!}" name="kode_sediaan[]"></td>
            @if($rujuk != NULL)
              <td>
                @foreach($rujuk as $re)
                    @if($re->kode_bahan_uji == $no)
                        {{$re->rujukan}}
                    @endif
                @endforeach
              </td>
            @else
            <td></td>
            @endif
            <td>{{ $val->hasil }}</td>
            <td>
              @if($evaluasi != NULL)
              @foreach($evaluasi as $valu)
                  @if($valu->kode_sediaan == $val->kode)
                  {{$valu->keterangan}}
                  <?php 
                    if ($valu->keterangan == 'KH') {
                      $kh++;
                    }elseif($valu->keterangan == 'NPR') {
                      $npr++;
                    }elseif($valu->keterangan == 'PPR') {
                      $ppr++;
                    }elseif($valu->keterangan == 'NPT') {
                      $npt++;
                    }elseif($valu->keterangan == 'PPT') {
                      $ppt++;
                    }
                  ?>
                  @endif
              @endforeach
              @endif
            </td>
            <td>
              @if($evaluasi != NULL)
                @foreach($evaluasi as $valu)
                  @if($valu->kode_sediaan == $val->kode)
                    {{$valu->nilai}}
                  @endif
                @endforeach
              @endif
            </td>
        </tr>
        @endforeach
        @endif
</table><br>
<table border="1">
  <tr>
    <th>Skor Total</th>
    <td>:</td>
    <th><center>{{$skor}}</center></th>
  </tr>
  <tr>
    <th>Keputusan</th>
    <td>:</td>
    <th><center>{{$status->status}}</center></th>
  </tr>
</table>
<br>
<br>
<div id="footer">
    EVALUASI PNPME MIKROSKOPIS BTA SIKLUS @if($siklus == '1') I @else II @endif TAHUN {{$tahun}}
</div>

@if($saran != NULL)
<?php 
  $jumlah = 0;
  if($saran->kh == 'ada'){$jumlah = $jumlah + 1;}
  if($saran->npr == 'ada'){$jumlah = $jumlah + 1;}
  if($saran->npt == 'ada'){$jumlah = $jumlah + 1;}
  if($saran->ppt == 'ada'){$jumlah = $jumlah + 1;}
  if($saran->ppr == 'ada'){$jumlah = $jumlah + 1;}
?>
@if($saran->kh == 'ada' || $saran->npr == 'ada' || $saran->npt == 'ada' || $saran->ppr == 'ada' || $saran->ppt == 'ada')
<table border="1" width="100%">
  <tr style="text-align: center;">
    <th>Jenis Kesalahan</th>
    <th>Kemungkinan Penyebab</th>
    <th>Saran Tindakan</th>
  </tr>
        @if($saran->kh == "ada")
        <tr>
             <td align="center">@if($saran->kh == "ada") KH @endif</td>
             <td>
                  <ul>
                    <li style="list-style: none;">
                      Kesalahan hitung jumlah BTA dalam 100LP.
                    </li>
                  </ul>
                
              </td>
             <td>
                <ul>
                    <li style="list-style: none;">
                      Lakukan pencatatan hasil baca BTA dalam 100 LP sesuai skala IUATLD.    
                    </li>
                </ul>
            </td>
        </tr>
        @endif
        @if($saran->npr == "ada")
        <tr style="vertical-align: top;">
             <td align="center" style="padding: 4%;">@if($saran->npr == "ada") NPR @endif</td>
             <td>
                <ol style="">
                  <li type="a">Pembacaan kurang dari 100LP.</li>
                  <li type="a">Teknis pewarnaan salah; BTA pucat, tidak kontras dengan warna latar.</li>
                </ol>
             </td>
             <td>
                <ol>
                  <li type="a">Lakukan pembacaan 100 LP sepanjang garis tengah dari ujung kiri ke kanan atau sebaliknya.</li>
                  <li type="a">Pewarnaan ulang dengan memperhatikan prosedur pewarnaan yang tepat.</li>
                </ol>
             </td>
        </tr>
        @endif
        @if($saran->npt=="ada")
        <tr style="vertical-align: top;">
             <td align="center" style="padding: 5%;">@if($saran->npt == "ada") NPT @endif</td>
             <td> 
                  <ol>
                    <li type="a">Teknik pemeriksaan mikroskopis BTA tidak sesuai petunjuk teknis.</li>
                    <li type="a">Pembacaan kurang dari 100LP.</li>
                    <li type="a">Teknis pewarnaan salah; BTA pucat, tidak kontras dengan warna latar.</li>
                    <li type="a">Kerusakan Mikroskop.</li>
                    <li type="a">Salah menyalin hasil.</li>
                  </ol>
             </td>
             <td>
                <ol>
                  <li type="a">Lakukan pemeriksaan mikroskopis BTA sesuai dengan prosedur.</li>
                  <li type="a">Lakukan pembacaan 100 LP sepanjang garis tengah dari ujung kiri ke kanan atau sebaliknya.</li>
                  <li type="a">Pewarnaan ulang dengan memperhatikan prosedur pewarnaan yang tepat.</li>
                  <li type="a">Periksa fungsi mikroskop.</li>
                  <li type="a">Periksa buku register laboratorium/ form TB 04</li>
                </ol>
             </td>
        </tr>
        @endif
        @if($saran->ppr == "ada")
        <tr style="vertical-align: top;">
             <td align="center" style="padding: 5%;">@if($saran->ppr == "ada") PPR @endif</td>
             <td>
                  <ol>
                    <li type="a">Petugas tidak mengenal bentuk BTA.</li>
                    <li type="a">BTA terbawa melalui pipet minyak emersi dari sediaan BTA positif sebelumnya (carry over)</li>
                    <li type="a">Artefak (endapan zat warna atau Kristal).</li>
                  </ol>
             </td>
             <td>
                <ol>
                  <li type="a">Memiliki sediaan BTA positif sebagai pembanding.</li>
                  <li type="a">Saat meneteskan minyak emersi ujung pipet tidak boleh menyentuh kaca sediaan. Bersihkan lensa objektif 100x.</li>
                  <li type="a">Pewarnaan ulang  dengan memperhatikan prosedur pewarnaan yang tepat.</li>
                  <li type="a">Saat supervisi lakukan uji kualitas ZN+C10.</li>
                </ol>
             </td>
        </tr>
        @endif
        @if($saran->ppt == "ada")
        <tr style="vertical-align: top;">
             <td align="center" style="padding: 6%;">@if($saran->ppt == "ada") PPT @endif</td>
             <td>
                <ol>
                  <li type="a">Petugas tidak mengenal bentuk BTA.</li>
                  <li type="a">BTA terbawa melalui pipet minyak emersi dari sediaan BTA positif sebelumnya (carry over).</li>
                  <li type="a">Salah menyalin hasil.</li>
                  <li type="a">Artefak (endapan zat warna atau Kristal).</li>
                </ol>
            </td>
             <td>
                <ol>
                  <li type="a">Memiliki sediaan BTA positif sebagai pembanding.</li>
                  <li type="a">Saat meneteskan minyak emersi ujung pipet tidak boleh menyentuh kaca sediaan. Bersihkan lensa objektif 100x.</li>
                  <li type="a">Periksa buku register laboratorium/form.TB 04.</li>
                  <li type="a">Pewarnaan ulang dengan memperhatikan prosedur pewarnaan yang tepat.</li>
                  <li type="a">Saat supervisi lakukan uji kualitas ZN.</li>
                </ol>
            </td>
        </tr>
        @endif
</table>
@endif
@endif
<br>

@if($jumlah <= 2)
<div style="margin-left: 520px !important;" >
@else
<div style="margin-left: 520px !important; page-break-before: always;" >
@endif
<table border="" style="page-break-inside: avoid;">
    <tr>
        <td style="z-index: 2;">
            Jakarta, @if($tanggalevaluasi != NULL){{_dateIndo($tanggalevaluasi->tanggal)}}@endif
            <br>
        </td>
    </tr>
    <tr>
      <td>
        Penanggungjawab 
        <br>
        Lab. Mikrobiologi Klinik
        <br>
      </td>
    </tr>
    <tr>
        <td style="">
             <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'telurCacing.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'telurCacing.png')}}" height="60px"  style=" /*border:1px solid black;*/ width: 80% !important; margin-left: -0px !important; z-index: 1;">
        </td>
    </tr>
    <tr>
        <td style="">
            dr. Nita Nurhidayati, Sp.MK
        </td>
    </tr>
</table>
</div><br>
<div>
  <span>Kelulusan >= 80, tanpa NPT atau PPT, sesuai Petunjuk Teknis Pembuatan Sediaan </span>
  <br>
  <span>Rujukan Mikroskopis TB Untuk uji Profisiensi (Kemenkes, 2013]</span>
</div>
<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>



    