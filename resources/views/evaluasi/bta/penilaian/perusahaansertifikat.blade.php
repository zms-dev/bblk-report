@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sertifikat</div>
                <div class="panel-body">
                    <!-- <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td colspan="3" align="center"><b>Print Seluruh Sertifikat</b></td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td width="40%">
                                <div class="form-group">
                                    <label for="">Siklus</label>
                                    <input type="hidden" value="{{$bidang}}" id="bidang">
                                    <select class="form-control" name="siklus" id="siklus">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                    </select>
                                </div>
                            </td>
                            <td width="40%">
                                <div class="form-group">
                                    <label for="">Tanggal</label>
                                    <input type="text" name="tanggal" class="form-control" id="tanggal" value="2018-08-03" readonly>
                                </div>
                            </td>
                            <td width="20%">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-primary btn-block go">Print</button>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table> -->
                    <blockquote style="font-size: 12px">
                    <button type="button" class="btn-primary" style="border: none;" disabled>&nbsp;</button> : Data belum di Evaluasi &nbsp;
                    <button type="button" class="btn-info" style="border: none;" disabled>&nbsp;</button> : Data Sudah di Evaluasi <br><br>
                    </blockquote>

                    <div class="well well-sm">
                        <form method="post" enctype="multipart/form-data">
                            <input type="hidden" readonly="" name="parameter" value="{{ Auth::user()->name }}">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pilih Tanggal Sertifikat :</label>
                                <input type="text" name="tanggal_sertifikat" id="tanggal" class="datepick form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nomor Sertifikat :</label>
                                <input type="text" name="nomor_sertifikat" id="nomor" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Siklus :</label>
                                <select name="siklus" class="form-control" required>
                                    <option></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tahun :</label>
                                <div class="controls input-append date " data-link-field="dtp_input1">
                                    <input size="16" type="text" value="" readonly class="form_datetime form-control" name="tahun" id="tahun" required>
                                    <span class="add-on"><i class="icon-th"></i></span>
                                </div>
                            </div>

                            <br>
                            <input type="submit" name="simpan" value="Update" class="btn btn-primary">
                        {{ csrf_field() }}
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="exampleInputEmail1">Tahun</label>
                            <div class="controls input-append date " data-link-field="dtp_input1">
                                <input size="16" type="text" value="" readonly class="form-control form_datetime" name="tahun" id="tahun1">
                                <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Siklus </label>
                            <select name="siklus" class="form-control" id="siklus1">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                        </div><br>
                        <div class="col-md-4">
                            <label></label>
                            <button class="btn btn-primary" name="" id="cari">Cari</button><br><br>
                        </div><br>
                    </div>
                    
                    <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                        <tr>
                            <th><center>Aksi</center></th>
                            <th><center>No</center></th>
                            <th><center>Instansi</center></th>
                            <th><center>Alamat</center></th>
                            <th><center>Kode Peserta</center></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    });
    </script>
@endif
@stop

@section('scriptBlock')
<script>
    function saveZip(bidang){
        $.ajax({
            url : "{{url('sertifikat/create-zip')}}",
            type : 'post',
            dataType : 'json',
            data : {
                "bidang" : bidang
            },
            beforeSend : function(){
                swal({
                    type : 'info',
                    title: 'Harap menunggu',
                    text: 'Sedang memproses Zip File',
                    showCancelButton: false,
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    allowEscapeKey : false,
                    allowEnterKey : false
                })
            },
            success : function(response){
                swal({
                    title: 'Berhasil',
                    text: 'Download file telah tersedia silahkan klik tombol download',
                    type: 'success',
                    confirmButtonText: 'DOWNLOAD',

                }).then((result) => {
                    window.open(response.url)
                });

            },
            error : function(e){
                swal({
                    title: 'GAGAL',
                    text: 'Terjadi kesahalan saat memproses zip',
                    type: 'error',
                    confirmButtonText: 'DOWNLOAD',

                }).then((result) => {
                    saveZip(bidang)
                });
                // saveFile(data,bidang,key)
            }
        })
    }
    function saveFile(data,bidang,key,y,x,tanggal)
    {
        //console.log(data[key]);
        var urut = key + 1;
        var jumlahDw = data.length;
        var r = data[key];
        console.log(r);
        $.ajax({
            url : "{{url('sertifikat/cetak-spect')}}?y="+y+'&x='+x+'&tanggal='+tanggal,
            type : 'post',
            dataType : 'json',
            data : {
                "r" : r,
                "bidang" : bidang
            },
            beforeSend : function(){
                swal({
                    type : 'info',
                    title: 'Harap menunggu :'+ urut + "/" + jumlahDw,
                    text: 'Sedang memproses sertifikat - ' + r.kode_lebpes,
                    showCancelButton: false,
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    allowEscapeKey : false,
                    allowEnterKey : false
                })
            },
            success : function(e){
                var nextKey = key+1 *1;
                nextKey = parseInt(nextKey);
                if(data[nextKey] != undefined){
                    saveFile(data,bidang,nextKey,y,x,tanggal)
                }else{
                    saveZip(bidang);
                }
                return true;
            },
            error : function(e){
                var url = result.url;
                swal({
                    title: 'GAGAL',
                    text: 'Terjadi kesahalan saat download no.leb : ' + r.kode_lebpes,
                    type: 'error',
                    confirmButtonText: 'DOWNLOAD',

                }).then((result) => {
                    saveFile(data,bidang,key,y,x,tanggal)
                });
                // saveFile(data,bidang,key)
            }
        })
    }
    $(".go").on("click",function(e){
        e.preventDefault()
        var y = $('#siklus').val();
        var idLink = $('#bidang').val();
        var tanggal = $("#tanggal").val();
        var baseUrl = "{{URL('')}}/sertifikat/cetak-all/"+idLink+"?&y="+y+"&tanggal="+tanggal;
        var url = baseUrl;
        console.log(url);
        $.ajax({
            url : url,
            dataType : 'json',
            timeout : 0,
            beforeSend : function(e){
                swal({
                    type : 'info',
                    title: 'Harap menunggu',
                    text: 'Sedang memproses gambar sertifikat, harap jangan menutup halaman ini.',
                    showCancelButton: false,
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    allowEscapeKey : false,
                    allowEnterKey : false
                })
            },
            success : function(result){
                console.log(result);
                if(result.data.length > 0){
                    saveFile(result.data,result.bidang,0,result.y,result.x,result.tanggal)
                }else{
                    swal({
                        title: 'Gagal',
                        text: 'Data peserta tidak tersedia.',
                        type: 'error',
                        confirmButtonText: 'OK'
                    });
                }

                // var url = result.url;
                // swal({
                //     title: 'Berhasil',
                //     text: 'Download terlah tersedia',
                //     type: 'success',
                //     confirmButtonText: 'DOWNLOAD',

                // }).then((result) => {
                //     window.open(url);
                // });

            },
            error : function(){
                swal({
                    title: 'Gagal',
                    text: 'terjadi kesalahan silahkan ulangi kembali',
                    type: 'error',
                    confirmButtonText: 'OK',
                });
            }
        })
    })

    // $('#tanggal').datepicker({
    //     format: 'yyyy-mm-dd',
    // });
        $("#cari").click(function(){
        var siklus = $('#siklus1').val();
        var tahun = $('#tahun1').val();
        var table = $(".dataTables-data");
        var dataTable = table.DataTable({
        "bDestroy":true,
        responsive:!0,
        "serverSide":true,
        "processing":true,
        "ajax":{
            url : "{{url('/evaluasi-mikro/sertifikat').'?siklus='}}"+siklus+"{{'&tahun='}}"+tahun,
        },
        dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
        language:{
            paginate:{
                previous:"&laquo;",
                next:"&raquo;"
            },search:"_INPUT_",
            searchPlaceholder:"Search..."
        },
        "columns":[
                {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
                {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
                {"data":"nama_lab","name":"nama_lab","searchable":true,"orderable":true},
                {"data":"alamat","name":"alamat","searchable":true,"orderable":true},
                {"data":"kode_lebpes","name":"kode_lebpes","searchable":true,"orderable":true,"visible":false},

        ],
        order:[[1,"asc"]]
    });
        });

     $(".form_datetime").datetimepicker({
        format: "yyyy",
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-right",
        minView: 4,
        startView: 4
    });

</script>
@stop
