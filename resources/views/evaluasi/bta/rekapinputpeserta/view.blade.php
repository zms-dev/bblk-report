<table border="1" width="100%">

    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Nama Instansi</th>
        <th rowspan="2">Kode Peserta</th>
        @for ($i=1; $i < 11; $i++)
          <th colspan="2">{{$i}}</th>
        @endfor
    </tr>
    <tr>
    @for ($i=1; $i < 11; $i++)
      <th>Kode Bahan Uji</th>
      <th>Hasil Pemeriksaan</th>
    @endfor
    </tr>
    @php
      $no = 0;
    @endphp
    @foreach ($datas as $key => $value)
      @php
        $no++;
      @endphp
      <tr>
        <td>{{$no}}</td>
        <td>{{$value->nama_lab}} kole {{$value->id}}</td>
        <td>{{$value->kode_lebpes}}</td>
        @foreach ($value->tb as $key => $kole)
          <td>{{$kole->kode}}</td>
          <td>{{$kole->hasil}}</td>
        @endforeach
      </tr>
    @endforeach
</table>
