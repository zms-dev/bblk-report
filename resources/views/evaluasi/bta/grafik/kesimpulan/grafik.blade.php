@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Kesimpulan Hasil Peserta BTA</div>
                <div class="panel-body">
                  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Nilai Peserta PNPME Siklus {{$input['siklus']}} Tahun {{$tahun}}'
    },
    xAxis: {
        categories: [
        @foreach($arrList as $key => $aray)
            {{$key}},
        @endforeach
        ],
        crosshair: true,
        title: {
            enabled: true,
            text: 'Nilai Peserta',
            style: {
                fontWeight: 'normal'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah Peserta'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0,
            dataLabels: {
                enabled: true,
            }
        }
    },
    series: [
        {
            name: 'Lulus',
            data: [
                    @foreach($arrList as $val)
                        {{$val}},
                    @endforeach
                  ]
        }, 
        {
            name: 'Tidak Lulus',
            data: [
                    @foreach($arrList2 as $val)
                        {{$val}},
                    @endforeach
                  ]
        }
    ]
});
</script>
@endsection