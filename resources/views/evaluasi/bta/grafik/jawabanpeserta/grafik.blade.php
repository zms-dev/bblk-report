@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Hasil BTA</div>
                <div class="panel-body">
                  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Jawaban Peserta'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total jawaban peserta'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
    },
    "series": [
        {
            "name": "Grafik Jawaban Peserta",
            "colorByPoint": true,
            "data": [
                @foreach($arrData as $key=>$val)
                    {
                        "name": "{{$key}}",
                        "y": <?=$val[$input['kode_sediaan']]?>,
                    },
                @endforeach
            ]
        }
    ],
});
</script>
@endsection