@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Peserta PNPME</div>
                <div class="panel-body">
                    <table class="table table-bordered" id="table">
                        <thead>
                            <tr>
                                <th>Badan Usaha</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($badan as $val)
                            <tr>
                                <td>{{$val->badan_usaha}}</td>
                                <td>{{$val->jumlah}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                  <input type="button" class="btn" value="Select Data" onclick="selectElementContents( document.getElementById('table') );">
                  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function selectElementContents(el) {
    var body = document.body, range, sel;
    if (document.createRange && window.getSelection) {
        range = document.createRange();
        sel = window.getSelection();
        sel.removeAllRanges();
        try {
            range.selectNodeContents(el);
            sel.addRange(range);
        } catch (e) {
            range.selectNode(el);
            sel.addRange(range);
        }
    } else if (body.createTextRange) {
        range = body.createTextRange();
        range.moveToElementText(el);
        range.select();
        range.execCommand("Copy");
    }
}

Highcharts.chart('container', {
    chart: {
        type: 'column',
    },
    title: {
        text: 'Jumlah Peserta yang Mengikuti PME BTA'
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    xAxis: {
        categories: [
                @foreach($badan as $val)
                  '{{$val->badan_usaha}}',
                @endforeach
                  ]
    },
    yAxis: {
        title: {
            text: 'Jumlah Peserta'
        }
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },
    series: [
        {
            'name': 'Jumlah Peserta',
            'colorByPoint': true,
            'data': [
                @foreach($badan as $val)
                
                {
                    'name' :'{{$val->badan_usaha}}',
                     'y'   :<?=$val->jumlah?>
                 },
                
                @endforeach
            ]
        }
    ]
});

// OldCharts Pie

// Highcharts.chart('container', {
//     chart: {
//         type: 'pie',
//         options3d: {
//             enabled: true,
//             alpha: 45,
//             beta: 0
//         }
//     },
//     title: {
//         text: 'Jumlah Peserta yang Mengikuti PME BTA'
//     },
//     tooltip: {
//         pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
//     },
//     plotOptions: {
//         pie: {
//             allowPointSelect: true,
//             cursor: 'pointer',
//             depth: 35,
//             dataLabels: {
//                 enabled: true,
//                 format: '<b>{point.name}</b>: {point.percentage:.1f} %'
//             }
//         }
//     },
//     series: [{
//         type: 'pie',
//         name: 'Jumlah Peserta',
//         data: [
//             @foreach($badan as $val)
//             ['{{$val->badan_usaha}}', {{$val->jumlah}}],
//             @endforeach
//         ]
//     }]
// });
</script>
@endsection