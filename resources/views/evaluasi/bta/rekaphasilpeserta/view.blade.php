<table >
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Nama Instansi</th>
        <th rowspan="2">Kode</th>
        <?php for ($i=1; $i < 11; $i++) { ?>
        <th>Jwb</th>
        <th>Nilai</th>
        <th>Hasil</th>
        <?php } ?>
        <th rowspan="2">Kesimpulan</th>
        <th rowspan="2">Total Nilai</th>
        <th rowspan="2">Catatan</th>
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <?php for ($i=1; $i < 11; $i++) { ?>
        <th colspan="3">{{$i}}/{{$input['siklus']}}/{{$input['tahun']}}</th>
        <?php } ?>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    <?php $no = 0; ?>
    @foreach($data as $val)
    <?php $no++; ?>
    <tr>
        <td>{{$no}}</td>
        <td>{{$val->nama_lab}}</td>
        <td>{{$val->kode_lebpes}}</td>
        @if(count($val->hasil))
            @foreach($val->hasil as $hasil)
            <td>{{$hasil->hasil}}</td>
            <td>{{$hasil->nilai}}</td>
            <td>{{$hasil->keterangan}}</td>
            @endforeach
        @else
            <?php for ($i=1; $i < 11; $i++) { ?>
            <th></th>
            <th></th>
            <th></th>
            <?php } ?>
        @endif
        @if(count($val->status))
            @foreach($val->status as $status)
                @if(count($status))
                    <td>{{$status->status}}</td>
                @else
                    <td></td>
                @endif
            @endforeach
        @else
        <td></td>
        @endif
        <td>{{$val->total}}</td>
        <td>
            @if(count($val->status))
                {{$val->hasil[0]->catatan}}
            @endif
        </td>
    </tr>
    @endforeach
</table>