<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;   
    text-align: center;
}


table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
</style>
<table>
    <tr>
        <th colspan="4">DATA JUMLAH PESERTA PNPME BERDASARKAN KELOMPOK INSTRUMENT / ALAT</th>
    </tr>
    <tr>
        <th colspan="4">BALAI BESAR LABORATORIUM KESEHATAN JAKARTA</th>
    </tr>
</table>
<table>
    <tr>
        <td>Siklus</td>
        <td>: {{$input['siklus']}}</td>
    </tr>
    <tr>
        <td>Tahun</td>
        <td>: {{$input['tahun']}}</td>
    </tr>
    <tr>
        <td>Kode Bahan</td>
        <td>: @if($input['tipe'] == 'a') 01 @else 02 @endif</td>
    </tr>
</table>
<br>
<table border="1" width="100%">
    <thead>
        <tr>
            <th>NO</th>
            <th>Parameter</th>
            <th>Instrumen / Alat</th>
            <th>Jumlah</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; ?>
        @foreach($alat as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{{$val->nama_parameter}}</td>
            <td>{{$val->instrumen}}</td>
            <td>{{$val->jumlah}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

<table style="page-break-before: always;">
    <tr>
        <th colspan="4">DATA JUMLAH PESERTA PNPME BERDASARKAN KELOMPOK METODE</th>
    </tr>
    <tr>
        <th colspan="4">BALAI BESAR LABORATORIUM  KESEHATAN JAKARTA</th>
    </tr>
</table>
<table>
    <tr>
        <td>Siklus</td>
        <td>: {{$input['siklus']}}</td>
    </tr>
    <tr>
        <td>Tahun</td>
        <td>: {{$input['tahun']}}</td>
    </tr>
    <tr>
        <td>Kode Bahan</td>
        <td>: @if($input['tipe'] == 'a') 01 @else 02 @endif</td>
    </tr>
</table>
<br>
<table border="1" width="100%">
    <thead>
        <tr>
            <th>NO</th>
            <th>Parameter</th>
            <th>Metode</th>
            <th>Jumlah</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; ?>
        @foreach($metode as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{{$val->nama_parameter}}</td>
            <td>{!!$val->metode_pemeriksaan!!}</td>
            <td>{{$val->jmlah}}</td>
        </tr>
        @endforeach
    </tbody>
</table>


<table style="page-break-before: always;">
    <tr>
        <th colspan="4">DATA JUMLAH PESERTA PNPME BERDASARKAN KELOMPOK INSTRUMENT / ALAT LAIN</th>
    </tr>
    <tr>
        <th colspan="4">BALAI BESAR LABORATORIUM  KESEHATAN JAKARTA</th>
    </tr>
</table>
<table>
    <tr>
        <td>Siklus</td>
        <td>: {{$input['siklus']}}</td>
    </tr>
    <tr>
        <td>Tahun</td>
        <td>: {{$input['tahun']}}</td>
    </tr>
    <tr>
        <td>Kode Bahan</td>
        <td>: @if($input['tipe'] == 'a') 01 @else 02 @endif</td>
    </tr>
</table>
<br>
<table border="1" width="100%">
    <thead>
        <tr>
            <th>NO</th>
            <th>Alat Lain</th>
            <th>Jumlah</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; ?>
        @foreach($alatlain as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{{$val->instrument_lain}}</td>z
            <td>{{$val->jumlah}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

<table style="page-break-before: always;">
    <tr>
        <th colspan="4">DATA JUMLAH PESERTA PNPME BERDASARKAN KELOMPOK METODE LAIN</th>
    </tr>
    <tr>
        <th colspan="4">BALAI BESAR LABORATORIUM KESEHATAN JAKARTA</th>
    </tr>
</table>
<table>
    <tr>
        <td>Siklus</td>
        <td>: {{$input['siklus']}}</td>
    </tr>
    <tr>
        <td>Tahun</td>
        <td>: {{$input['tahun']}}</td>
    </tr>
    <tr>
        <td>Kode Bahan</td>
        <td>: @if($input['tipe'] == 'a') 01 @else 02 @endif</td>
    </tr>
</table>
<br>
<table border="1" width="100%">
    <thead>
        <tr>
            <th>NO</th>
            <th>Parameter</th>
            <th>Metode</th>
            <th>Jumlah</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; ?>
        @foreach($metodelain as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{{$val->nama_parameter}}</td>
            <td>{!!$val->metode_lain!!}</td>
            <td>{{$val->jmlah}}</td>
        </tr>
        @endforeach
    </tbody>
</table>