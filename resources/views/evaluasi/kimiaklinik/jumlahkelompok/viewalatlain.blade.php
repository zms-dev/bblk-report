<table>
    <tr>
        <th colspan="4">DATA JUMLAH PESERTA PNPME BERDASARKAN KELOMPOK ALAT LAIN</th>
    </tr>
    <tr>
        <th colspan="4">BALAI BESAR LABORATORIUM JAKARTA</th>
    </tr>
</table>
<table>
    <tr>
        <td>Siklus</td>
        <td>: {{$input['siklus']}}</td>
    </tr>
    <tr>
        <td>Tahun</td>
        <td>: {{$input['tahun']}}</td>
    </tr>
    <tr>
        <td>Kode Bahan</td>
        <td>: @if($input['tipe'] == 'a') 01 @else 02 @endif</td>
    </tr>
</table>
<table border="1">
    <thead>
        <tr>
            <th>NO</th>
            <th>Alat Lain</th>
            <th>Jumlah</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; ?>
        @foreach($alatlain as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{!!$val->instrument_lain!!}</td>
            <td>{{$val->jumlah}}</td>
        </tr>
        @endforeach
    </tbody>
</table>