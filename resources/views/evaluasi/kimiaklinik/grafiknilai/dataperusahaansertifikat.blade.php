@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sertifikat Kimia Klinik Siklus {{$siklus}} Tahun {{$tahun}}</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="get" enctype="multipart/form-data">
                    <table class="table table-bordered">
                        <tr>
                            <th width="3%">No</th>
                            <th>Bidang</th>
                            <th width="15%" colspan="2">Aksi</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->bidang == '2')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}</td>
                                @if($val->tanggalna == $tahun)
                                    @if($siklus == 1)

                                    @if($val->siklus_1 == 'done')
                                        <td>
                                        	<a href="" class="go-link"  data-id="{{$val->perusahaan_id}}" data-x="a" data-y="1" data-link="{{$val->Link}}" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i><br>1</center></a>
                                        </td>
                                    @else
                                        <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                    @endif
                                    
                                     @elseif($siklus == 2)

                                    @if($val->siklus_2 == 'done')
                                        <td>
                                            <a href="" class="go-link"  data-id="{{$val->perusahaan_id}}" data-x="a" data-y="2" data-link="{{$val->Link}}" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i><br>2</center></a>
                                        </td>
                                    @else
                                        <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                    @endif
                                    @endif
                                @else
                                    <td>Tahun {{$tahun}}</td>
                                @endif
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".go-link").on("click",function(e){
        e.preventDefault()
        var y = $(this).data("y");
        var x = $(this).data("x");
        var idLink = $(this).data("id");
        var link = $(this).data("link");
        var tanggal = $("#tanggal").val();
        var nomor = $("#nomor").val();
        var baseUrl = "{{URL('')}}"+link+"/sertifikat/print/"+idLink+"?x="+x+"&y="+y+"&tanggal="+tanggal+"&nomor="+nomor+"&download=true";
        var url = baseUrl;
        console.log(url);
        
        window.location.href = url;
    })
</script>
@endsection
