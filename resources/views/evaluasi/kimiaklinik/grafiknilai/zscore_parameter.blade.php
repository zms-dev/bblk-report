@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<script src="{{URL('/asset/js/html2pdf.bundle.min.js')}}"></script>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                    <a style="margin: 10px" onclick="javascript:printDiv('report-chart')"><li class="glyphicon glyphicon-print"></li></a>
                    <div class="report-chart" id="report-chart">

                    <style>
                        .report-chart-title{
                            text-align:center;
                            padding:15px 0px;
                            margin-bottom:30px;
                        }
                        .report-chart-title h3{
                            font-size:22px;
                            margin:0px;
                            padding:0px;
                        }
                        .row-chart{
                            padding:0px;
                        }
                        .row-chart .col{
                            display:block;
                            width:50%;
                            float:left;
                        }
                        .row-chart:after{
                            content : " ";
                            clear:both;
                            display:block;
                        }
                        .graph-container{
                            padding:0px;
                            margin-bottom:30px;
                            width:100%;
                            display:block;
                            padding:0;
                            text-align:center;
                        }
                        .graph-container .graph{
                            width:90%;
                            margin:auto;
                            display:inline-block;
                            height:300px;
                        }
                        .table-information{
                            border-bottom:1px solid  #ddd;
                        }
                        .table-information td{
                            height:36px;
                            vertical-align:middle !important;
                        }
                        .report-chart-title h3{
                            font-size:16px;
                        }
                        .color-green{
                            background: #1bbd20 !important;color: #1bbd20 !important;;display:inline-block;width:15px;height:25px;
                        }
                        .color-red{
                            background: #BF0B23 !important;color: #BF0B23 !important;;display:inline-block;width:15px;height:25px;
                        }
                        @media print {
                            .graph-container .graph{
                                width:100%;
                                margin:auto;
                                display:inline-block;
                                height:300px;
                            }
                        }
                    </style>
                    <div class="report-chart-title">
                        <h3>GRAFIK HISTOGRAM DISTRIBUSI HASIL PNPME DAN Z SCORE BIDANG KIMIA KLINIK<br>
BERDASARKAN KELOMPOK SELURUH PESERTA</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-information table-condensed">
                                <tbody>
                                    <tr>
                                        <td width="150px" style="font-weight:bold;">KODE PESERTA</td>
                                        <td width="10px">:</td>
                                        <td width="40%">{{$data->kode_lebpes}}</td>
                                        <td width="50px" style="font-weight:bold;"><div class="color-red">a</div></td>
                                        <td width="10px">:</td>
                                        <td>OUT</td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight:bold;">SIKLUS</td>
                                        <td>:</td>
                                        <td>{{$siklus}} - {{($type == 'a' ? '01' : '02')}}</td>
                                        <td><div class="color-green">a</div></td>
                                        <td>:</td>
                                        <td>Posisi Saudara</td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight:bold;">TAHUN</td>
                                        <td>:</td>
                                        <td>{{$tahun}}</td>
                                        <td style="font-weight:bold;">*</td>
                                        <td>:</td>
                                        <td>Median</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <div class="row-chart">
                                <?php $a = 0;?>
                                @foreach($param as $key => $val)
                                    @if(!empty($val->hasil_pemeriksaan) && $val->hasil_pemeriksaan != "-" && $val->hasil_pemeriksaan != "0" && $val->hasil_pemeriksaan != "0.00" && $val->hasil_pemeriksaan != "0.0" && !empty($val->batas_bawah) && !empty($val->batas_atas) && is_numeric($val->hasil_pemeriksaan))

                                            <div class="col">
                                                <div class="graph-container">
                                                    <div class="graph graph-nilai-sama" id="graph-nilai-sama-{{$key}}" data-json="{{json_encode($val)}}">

                                                    </div>
                                                </div>
                                            </div>
                                    
                                    @endif
                                    @if(!empty($val->zscore) && $val->zscore != "-")
                                        <div class="col">
                                            <div class="graph-container">
                                                <div class="graph graph-nilai-zscore" id="graph-nilai-zscore-{{$key}}" data-json="{{json_encode($val)}}">

                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <?php $a++; if($a == 4):$a=0;?>
                                    <?php else:?>
                                    <?php endif;?>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var graphList = $(".graph-nilai-sama");
$.each(graphList,function(e,f){
    var id = $(this).attr('id');
    var dataJson =  $(this).data('json');
    dataJson.batas_bawah = dataJson.batas_bawah *1;
    dataJson.batas_atas = dataJson.batas_atas *1;
    dataJson.ring_1 = dataJson.ring_1 *1;
    dataJson.ring_2 = dataJson.ring_2 *1;
    dataJson.ring_3 = dataJson.ring_3 *1;
    dataJson.ring_4 = dataJson.ring_4 *1;
    dataJson.ring_5 = dataJson.ring_5 *1;
    dataJson.out_bawah = dataJson.out_bawah *1;
    dataJson.out_atas = dataJson.out_atas *1;
    dataJson.ring_1_bawah = dataJson.ring_1_bawah *1;
    dataJson.ring_1_atas = dataJson.ring_1_atas *1;
    dataJson.ring_2_bawah = dataJson.ring_2_bawah *1;
    dataJson.ring_2_atas = dataJson.ring_2_atas *1;
    dataJson.ring_3_bawah = dataJson.ring_3_bawah *1;
    dataJson.ring_3_atas = dataJson.ring_3_atas *1;
    dataJson.ring_4_bawah = dataJson.ring_4_bawah *1;
    dataJson.ring_4_atas = dataJson.ring_4_atas *1;
    dataJson.ring_5_bawah = dataJson.ring_5_bawah *1;
    dataJson.ring_5_atas = dataJson.ring_5_atas *1;
    var series = [
        {
            "name": "Distribusi Hasil",
            "colorByPoint": false,
            data : [
                {
                    "name": "< " + dataJson.batas_bawah,
                    "color": (dataJson.hasil_pemeriksaan < dataJson.batas_bawah ? '#1bbd20' : '#BF0B23'),
                    "y" : dataJson.out_bawah,
                    "a" : (dataJson.median != 0 && dataJson.median < dataJson.batas_bawah ? '*' : '' )
                },
                {
                    "name": "" + dataJson.ring_1_bawah + " - " + dataJson.ring_1_atas,
                    "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_1_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_1_atas ? '#1bbd20' : '#42b9ff'),
                    "y" : dataJson.ring_1,
                    "a" : (dataJson.median != 0 && (dataJson.median >= dataJson.ring_1_bawah && dataJson.median <= dataJson.ring_1_atas) ? '*' : '' )
                },
                {
                    "name": "" + dataJson.ring_2_bawah + " - " + dataJson.ring_2_atas,
                    "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_2_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_2_atas ? '#1bbd20' : '#42b9ff'),
                    "y" : dataJson.ring_2,
                    "a" : (dataJson.median != 0 && (dataJson.median >= dataJson.ring_2_bawah && dataJson.median <= dataJson.ring_2_atas) ? '*' : '' )
                },
                {
                    "name": "" + dataJson.ring_3_bawah + " - " + dataJson.ring_3_atas,
                    "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_3_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_3_atas ? '#1bbd20' : '#42b9ff'),
                    "y" : dataJson.ring_3,
                    "a" : (dataJson.median != 0 && (dataJson.median >= dataJson.ring_3_bawah && dataJson.median <= dataJson.ring_3_atas) ? '*' : '' )
                },
                {
                    "name": "" + dataJson.ring_4_bawah + " - " + dataJson.ring_4_atas,
                    "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_4_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_4_atas ? '#1bbd20' : '#42b9ff'),
                    "y" : dataJson.ring_4,
                    "a" : (dataJson.median != 0 && (dataJson.median >= dataJson.ring_4_bawah && dataJson.median <= dataJson.ring_4_atas) ? '*' : '' )
                },
                {
                    "name": "" + dataJson.ring_5_bawah + " - " + dataJson.ring_5_atas,
                    "color": (dataJson.hasil_pemeriksaan >= dataJson.ring_5_bawah && dataJson.hasil_pemeriksaan <= dataJson.ring_5_atas ? '#1bbd20' : '#42b9ff'),
                    "y" : dataJson.ring_5,
                    "a" : (dataJson.median != 0 && (dataJson.median >= dataJson.ring_5_bawah && dataJson.median <= dataJson.ring_5_atas) ? '*' : '' )
                },
                {
                    "name": "> " + dataJson.batas_atas,
                    "color": (dataJson.hasil_pemeriksaan > dataJson.batas_atas ? '#1bbd20' : '#BF0B23'),
                    "y" : dataJson.out_atas,
                    "a" : (dataJson.median != 0 && dataJson.median > dataJson.batas_atas ? '*' : '' )
                },
            ]
        }
    ]
    Highcharts.chart(id, {
        exporting: { enabled: false },
        chart: {
            type: 'column',
        },
        title: {
            text: 'Grafik Distribusi Hasil ' + dataJson.nama_parameter +'<br>Nilai Saudara : ' + dataJson.hasil_pemeriksaan,
            style : {
                'font-size' : "10px",
                'font-weight' : "bold",
            },
            useHtml : true
        },
        xAxis: {
            type: 'category',
            labels : {
                style : {
                    'font-size' : "8px",
                    'font-weight' : "bold",
                }
            }
        },
        yAxis: {
            title: {
                text : 'Jumlah Peserta',
                style : {
                    'font-size' : "8px"
                }
            },
            labels : {
                style : {
                    'font-size' : "8px",
                    'font-weight' : "bold",
                }
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y} {point.a}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
        },

        "series":series
    })
})
var graphList = $(".graph-nilai-zscore");
$.each(graphList,function(e,f){
    var id = $(this).attr('id');
    var dataJson =  $(this).data('json');
    if(dataJson.zscore != "-" &&  dataJson.zscore != null){
        dataJson.zscore = dataJson.zscore *1;
    }

    var series = [
        {
            "name": "Nilai Zscore",
            "colorByPoint": false,
            data : [
                {
                    "name": "< -3",
                    "color": (dataJson.zscore < -3 ? '#1bbd20' : '#BF0B23'),
                    "y" : dataJson.zscore_kurang_3,
                    "a" : (dataJson.median != 0 && dataJson.median < -3 ? '*' : '' )
                },
                {
                    "name": ">= -3 s.d < -2",
                    "color": (dataJson.zscore < -2 && dataJson.zscore >= -3 ? '#1bbd20' : '#42b9ff'),
                    "y" : dataJson.zscore_kurang_antara23,
                    "a" : (dataJson.median != 0 && dataJson.median < -2 && dataJson.median >= -3  ? '*' : '' )
                },
                {
                    "name": ">= -2 s.d <= -1",
                    "color": (dataJson.zscore <= -1 && dataJson.zscore >= -2 ? '#1bbd20' : '#42b9ff'),
                    "y" : dataJson.zscore_kurang_antara12,
                    "a" : (dataJson.median != 0 && dataJson.median <= -1 && dataJson.median >= -2  ? '*' : '' )
                },
                {
                    "name": "> -1 s.d <= 1",
                    "color": (dataJson.zscore <= 1 && dataJson.zscore > -1 ? '#1bbd20' : '#42b9ff'),
                    "y" : dataJson.zscore_kurang_antara11,
                    "a" : (dataJson.median != 0 && dataJson.median <= 1 && dataJson.median > -1  ? '*' : '' )
                },
                {
                    "name": "> 1 s.d <= 2",
                    "color": (dataJson.zscore <= 2 && dataJson.zscore > 1 ? '#1bbd20' : '#42b9ff'),
                    "y" : dataJson.zscore_antara12,
                    "a" : (dataJson.median != 0 && dataJson.median <= 2 && dataJson.median > 1  ? '*' : '' )
                },
                {
                    "name": "> 2 s.d <= 3",
                    "color": (dataJson.zscore <= 3 && dataJson.zscore > 2 ? '#1bbd20' : '#42b9ff'),
                    "y" : dataJson.zscore_antara23,
                    "a" : (dataJson.median != 0 && dataJson.median <= 3 && dataJson.median > 2  ? '*' : '' )
                },
                {
                    "name": "> 3",
                    "color": (dataJson.zscore > 3 ? '#1bbd20' : '#BF0B23'),
                    "y" : dataJson.zscore_3,
                    "a" : (dataJson.median != 0 && dataJson.median  > 3  ? '*' : '' )
                },
            ]
        }
    ]
    Highcharts.chart(id, {
        exporting: { enabled: false },
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Zscore ' + dataJson.nama_parameter + '<br>Nilai Z-Score Saudara : ' + dataJson.zscore,
            style : {
                'font-size' : "10px",
                'font-weight' : "bold",
            },
            useHtml : true
        },
        xAxis: {
            type: 'category',
            labels : {
                style : {
                    'font-size' : "8px",
                    'font-weight' : "bold",
                }
            }
        },
        yAxis: {
            title: {
                text : 'Jumlah Peserta',
                style : {
                    'font-size' : "8px"
                }
            },
            labels : {
                style : {
                    'font-size' : "8px",
                    'font-weight' : "bold",
                }
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y} {point.a}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
        },

        "series":series
    })
})
function printDiv(divID) {
    //Get the HTML of div
    var divElements = document.getElementById(divID).innerHTML;
    //Get the HTML of whole page
    var oldPage = document.body.innerHTML;

    //Reset the page's HTML with div's HTML only
    document.body.innerHTML = 
        "<html><head><title></title></head><body>" + 
        divElements + "</body>";

    //Print Page
    window.print();

    //Restore orignal HTML
    document.body.innerHTML = oldPage;

    
}
</script>
@endsection