@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Nilai yang Sama Kimia Klinik</div>
                <a class="btn btn-primary" style="margin: 10px" onclick="javascript:printDiv('datana')">Print <li class="glyphicon glyphicon-print"></li></a>
                <div class="panel-body" id="datana">
                @foreach($datapes as $val)
                  <table width="90%;">
                    <tr>
                      <td><div id="container{{$val->id}}" ></div></td>
                      <td><div id="containermetode{{$val->id}}" ></div></td>
                    </tr>
                  </table>
                @endforeach
                  <label>Keterangan :</label>
                  <blockquote style="font-size: 12px">
                    <style type="text/css">
                      @media print{          
                          #color-red{
                              -webkit-print-color-adjust: exact!important;
                              background: #1bbd20 !important;display:inline-block;width:10px;height:25px;
                          }

                      }
                    </style>
                    <table>
                      <tr>
                          <th><div id="color-red" style="background-color:#1bbd20;width: 10px;height: 20px;"></div></th>
                          <th></th>
                          <td>&nbsp;Posisi Peserta</td>
                      </tr>
                    </table>
                  </blockquote>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function printDiv(divID) {
    //Get the HTML of div
    var divElements = document.getElementById(divID).innerHTML;
    //Get the HTML of whole page
    var oldPage = document.body.innerHTML;

    //Reset the page's HTML with div's HTML only
    document.body.innerHTML = 
      "<html><head><title></title></head><body>" + 
      divElements + "</body>";

    //Print Page
    window.print();

    //Restore orignal HTML
    document.body.innerHTML = oldPage;

  
}
// Create the chart
@foreach($datapes as $val1)
Highcharts.chart('container{{$val1->id}}', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Nilai yang Sama Parameter {{$val1->nama_parameter}}',
        style : {
            'font-size' : "10px",
            'font-weight' : "bold",
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
      title: {
        text : 'Jumlah Peserta'
      }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            pointPadding: 0,
            groupPadding: 0,
            borderWidth: 0,
            shadow: false,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
    },

    "series": [
        {
            "name": "Nilai yang Sama",
            "colorByPoint": false,
            "data": [
              @foreach($val1->batas as $batas1)
                {
                  "name": "{{$batas1->batas_bawah}} - {{$batas1->batas_atas}}",
                  <?php $no = 0; ?>
                  @foreach($val1->data as $val)
                    @if($val->hasil_pemeriksaan >= $batas1->batas_bawah && $val->hasil_pemeriksaan <= $batas1->batas_atas )
                      <?php $no++; ?>
                      @if(count($val->peserta))
                        "color": "#1bbd20",
                      @endif
                    @endif
                  @endforeach
                  "y": {{$no}},
                },
              @endforeach
            ]
        }
    ],
});
@endforeach
// Create the chart
@foreach($datapes as $val1)
Highcharts.chart('containermetode{{$val1->id}}', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Nilai yang Sama Metode {!!$val1->metode_pemeriksaan!!}',
        style : {
            'font-size' : "10px",
            'font-weight' : "bold",
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
      title: {
        text : 'Jumlah Peserta'
      }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            pointPadding: 0,
            groupPadding: 0,
            borderWidth: 0,
            shadow: false,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
    },

    "series": [
        {
            "name": "Nilai yang Sama",
            "colorByPoint": false,
            "data": [
              @foreach($val1->batasmetode as $batas2)
                {
                  "name": "{{$batas2->batas_bawah}} - {{$batas2->batas_atas}}",
                  <?php $no = 0; ?>
                  @foreach($val1->datametode as $val)
                    @if($val->hasil_pemeriksaan >= $batas2->batas_bawah && $val->hasil_pemeriksaan <= $batas2->batas_atas )
                      <?php $no++; ?>
                      @if(count($val->peserta))
                        "color": "#1bbd20",
                      @endif
                    @endif
                  @endforeach
                  "y": {{$no}},
                },
              @endforeach
            ]
        }
    ],
});
@endforeach
</script>
@endsection