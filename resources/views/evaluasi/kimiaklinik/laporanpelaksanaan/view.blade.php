<table border="1">
    <thead>
        <tr>
            <th rowspan="2">Kode Lab</th>
            <th rowspan="2">Provinsi</th>
            <th rowspan="2">Tanggal Terima Bahan</th>
            <th colspan="2">Kualitas Bahan</th>
            <th colspan="2">Tanggal Mengerjakan</th>
            <th colspan="2">Tanggal Kirim Hasil</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>01</th>
            <th>02</th>
            <th>01</th>
            <th>02</th>
            <th>01</th>
            <th>02</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $val)
        <tr>
            <td>{{$val->kode_lab}}</td>
            <td>{{$val->name}}</td>
            <td>{{$val->tgl_penerimaan}}</td>
            @if(!empty($val->tipe_1))
                <td style="text-transform: capitalize;">{{$val->tipe_1->kualitas_bahan}}</td>
            @else
                <td style="text-transform: capitalize;"></td>
            @endif
            @if(!empty($val->tipe_2))
                <td style="text-transform: capitalize;">{{$val->tipe_2->kualitas_bahan}}</td>
            @else
                <td style="text-transform: capitalize;"></td>
            @endif
            @if(!empty($val->tipe_1))
                <td style="text-transform: capitalize;">{{$val->tipe_1->tgl_pemeriksaan}}</td>
            @else
                <td style="text-transform: capitalize;"></td>
            @endif
            @if(!empty($val->tipe_2))
                <td style="text-transform: capitalize;">{{$val->tipe_2->tgl_pemeriksaan}}</td>
            @else
                <td style="text-transform: capitalize;"></td>
            @endif
            @if(!empty($val->tipe_1))
                <td style="text-transform: capitalize;">{{$val->tipe_1->updated_at}}</td>
            @else
                <td style="text-transform: capitalize;"></td>
            @endif
            @if(!empty($val->tipe_2))
                <td style="text-transform: capitalize;">{{$val->tipe_2->updated_at}}</td>
            @else
                <td style="text-transform: capitalize;"></td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>