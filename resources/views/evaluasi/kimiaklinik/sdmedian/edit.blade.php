@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Nilai SD dan Median Per Parameter (Kimia Klinik)</div>
                <div class="panel-body">
                @if(Session::has('message'))
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                  </div>
                @endif
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Siklus</label>
                              <select class="form-control" name="siklus" required>
                                <option value="{{$edit->siklus}}">{{$edit->siklus}}</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Tahun</label>
                              <select class="form-control" name="tahun" required>
                                <option value="{{$edit->tahun}}">{{$edit->tahun}}</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Tipe</label>
                              <select class="form-control" name="tipe" required>
                                <option value="{{$edit->tipe}}">@if($edit->tipe == 'a')01 @else 02 @endif</option>
                                <option value="a">01</option>
                                <option value="b">02</option>
                              </select>
                          </div>
                        </div>
                      </div>
                      <br>
                    <table class="table table-bordered">
                      <tr>
                        <th>Parameter</th>
                        <th>SD</th>
                        <th>Median</th>
                      </tr>
                      @foreach($sd as $val)
                      <input type="hidden" name="id[]" value="{{$val->id}}">
                      <tr>
                        <td>
                          <input type="hidden" name="parameter[]" value="{{$val->parameter}}">{{$val->nama_parameter}}
                        </td>
                        <td><input type="text" name="sd[]" class="form-control" value="{{$val->sd}}"></td>
                        <td><input type="text" name="median[]" class="form-control" value="{{$val->median}}"></td>
                      </tr>
                      @endforeach
                    </table>
                    <input type="submit" name="proses" class="btn btn-primary" value="Update">
                    {{ csrf_field() }}
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection