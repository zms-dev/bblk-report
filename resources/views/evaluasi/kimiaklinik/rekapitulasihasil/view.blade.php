<table border="1">
    <thead>
        <tr>
            <th rowspan="2">Nilai</th>
            <th rowspan="2">Kriteria</th>
            <th colspan="2">Peserta</th>
            <th colspan="2">Metode</th>
            <th colspan="2">Alat</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>Jumlah</th>
            <th>%</th>
            <th>Jumlah</th>
            <th>%</th>
            <th>Jumlah</th>
            <th>%</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>| Z Score | ≤ 2</td>
            <td>Memuaskan</td>
            <td>
            @foreach($datapesertaa as $val)
                @if($val->score == 'Memuaskan')
                    {{$val->jumlah}}
                @endif
            @endforeach
            </td>
            <td>
            @foreach($datapesertaa as $val)
                @if($val->score == 'Memuaskan')
                    <?php 
                        $persen1 = ($val->jumlah / $jumlahpesertaa) * 100;
                        echo number_format($persen1, 2)."%";
                    ?>
                @endif
            @endforeach
            </td>
            <td>
            @foreach($dataalata as $val)
                @if($val->score == 'Memuaskan')
                    {{$val->jumlah}}
                @endif
            @endforeach
            </td>
            <td>
            @foreach($dataalata as $val)
                @if($val->score == 'Memuaskan')
                    <?php 
                        $persen1 = ($val->jumlah / $jumlahalata) * 100;
                        echo number_format($persen1, 2)."%";
                    ?>
                @endif
            @endforeach
            </td>
            <td>
            @foreach($datametodea as $val)
                @if($val->score == 'Memuaskan')
                    {{$val->jumlah}}
                @endif
            @endforeach
            </td>
            <td>
            @foreach($datametodea as $val)
                @if($val->score == 'Memuaskan')
                    <?php 
                        $persen1 = ($val->jumlah / $jumlahmetodea) * 100;
                        echo number_format($persen1, 2)."%";
                    ?>
                @endif
            @endforeach
            </td>
        </tr>
        <tr>
            <td>2 < | Z Score | ≤ 3</td>
            <td>Peringatan</td>
            <td>
            @foreach($datapesertaa as $val)
                @if($val->score == 'Peringatan')
                    {{$val->jumlah}}
                @endif
            @endforeach
            </td>
            <td>
            @foreach($datapesertaa as $val)
                @if($val->score == 'Peringatan')
                    <?php 
                        $persen1 = ($val->jumlah / $jumlahpesertaa) * 100;
                        echo number_format($persen1, 2)."%";
                    ?>
                @endif
            @endforeach
            </td>
            <td>
            @foreach($dataalata as $val)
                @if($val->score == 'Peringatan')
                    {{$val->jumlah}}
                @endif
            @endforeach
            </td>
            <td>
            @foreach($dataalata as $val)
                @if($val->score == 'Peringatan')
                    <?php 
                        $persen1 = ($val->jumlah / $jumlahalata) * 100;
                        echo number_format($persen1, 2)."%";
                    ?>
                @endif
            @endforeach
            </td>
            <td>
            @foreach($datametodea as $val)
                @if($val->score == 'Peringatan')
                    {{$val->jumlah}}
                @endif
            @endforeach
            </td>
            <td>
            @foreach($datametodea as $val)
                @if($val->score == 'Peringatan')
                    <?php 
                        $persen1 = ($val->jumlah / $jumlahmetodea) * 100;
                        echo number_format($persen1, 2)."%";
                    ?>
                @endif
            @endforeach
            </td>
        </tr>
        <tr>
            <td>| Z Score | > 3</td>
            <td>Tidak Memuaskan</td>
            <td>
            @foreach($datapesertaa as $val)
                @if($val->score == 'Tidak Memuaskan')
                    {{$val->jumlah}}
                @endif
            @endforeach
            </td>
            <td>
                @foreach($datapesertaa as $val)
                    @if($val->score == 'Tidak Memuaskan')
                        <?php 
                            $persen1 = ($val->jumlah / $jumlahpesertaa) * 100;
                            echo number_format($persen1, 2)."%";
                        ?>
                    @endif
                @endforeach
            </td>
            <td>
            @foreach($dataalata as $val)
                @if($val->score == 'Tidak Memuaskan')
                    {{$val->jumlah}}
                @endif
            @endforeach
            </td>
            <td>
            @foreach($dataalata as $val)
                @if($val->score == 'Tidak Memuaskan')
                    <?php 
                        $persen1 = ($val->jumlah / $jumlahalata) * 100;
                        echo number_format($persen1, 2)."%";
                    ?>
                @endif
            @endforeach
            </td>
            <td>
            @foreach($datametodea as $val)
                @if($val->score == 'Tidak Memuaskan')
                    {{$val->jumlah}}
                @endif
            @endforeach
            </td>
            <td>
            @foreach($datametodea as $val)
                @if($val->score == 'Tidak Memuaskan')
                    <?php 
                        $persen1 = ($val->jumlah / $jumlahmetodea) * 100;
                        echo number_format($persen1, 2)."%";
                    ?>
                @endif
            @endforeach
            </td>
        </tr>
        <tr>
            <td>-</td>
            <td>Tidak di Analisis</td>
            <td>
            @foreach($datapesertaa as $val)
                @if($val->score == 'Tidak di Analisis')
                    {{$val->jumlah}}
                @endif
            @endforeach
            </td>
            <td>
            @foreach($datapesertaa as $val)
                @if($val->score == 'Tidak di Analisis')
                    <?php 
                        $persen1 = ($val->jumlah / $jumlahpesertaa) * 100;
                        echo number_format($persen1, 2)."%";
                    ?>
                @endif
            @endforeach
            </td>
            <td>
            @foreach($dataalata as $val)
                @if($val->score == 'Tidak di Analisis')
                    {{$val->jumlah}}
                @endif
            @endforeach
            </td>
            <td>
            @foreach($dataalata as $val)
                @if($val->score == 'Tidak di Analisis')
                    <?php 
                        $persen1 = ($val->jumlah / $jumlahalata) * 100;
                        echo number_format($persen1, 2)."%";
                    ?>
                @endif
            @endforeach
            </td>
            <td>
            @foreach($datametodea as $val)
                @if($val->score == 'Tidak di Analisis')
                    {{$val->jumlah}}
                @endif
            @endforeach
            </td>
            <td>
            @foreach($datametodea as $val)
                @if($val->score == 'Tidak di Analisis')
                    <?php 
                        $persen1 = ($val->jumlah / $jumlahmetodea) * 100;
                        echo number_format($persen1, 2)."%";
                    ?>
                @endif
            @endforeach
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">JUMLAH</td>
            <td>{{$jumlahpesertaa}}</td>
            <td>100%</td>
            <td>{{$jumlahalata}}</td>
            <td>100%</td>
            <td>{{$jumlahmetodea}}</td>
            <td>100%</td>
        </tr>
    </tbody>
</table>