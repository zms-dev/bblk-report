@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Input Range Per Instrumen / Alat (Kimia Klinik)</div>
                <div class="panel-body">
                @if(Session::has('message'))
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                  </div>
                @endif
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="siklus" value="{{$siklus}}">
                    <input type="hidden" name="tahun" value="{{$tahun}}">
                    <input type="hidden" name="tipe" value="{{$tipe}}">
                    <br>
                    <div class="table-responsive" style="overflow:auto;max-width:100%;">
                        <table class="table table-bordered" style="width:1400px">
                        <thead>
                            <tr>
                            <th rowspan="2" width="400px">Parameter</th>
                            <th rowspan="2" width="200px">Instumen</th>
                            <th rowspan="2" width="100px">Batas Bawah</th>
                            <th rowspan="2" width="100px">Batas Atas</th>
                            <th rowspan="2" width="100px">Interval</th>
                            @for($i = 1; $i <= 3; $i++)
                                <th colspan="2">Ring {{$i}}</th>
                            @endfor
                            </tr>
                            <tr>
                                @for($i = 1; $i <= 3; $i++)
                                    <th width="100px">Bawah</th>
                                    <th width="100px">Atas</th>
                                @endfor
                                
                            </tr>
                        </thead>
                        <tbody class="tbody-ring">
                            @foreach($sd as $key => $val)
                            <tr>
                            <td>
                                {{$val->nama_parameter}}
                                <input type="hidden" class="form-control" name="data[{{$key}}][id_tb_ring]" value="{{$val->id}}">
                                <input type="hidden" class="form-control" name="data[{{$key}}][parameter]" value="{{$val->id_parameter}}">
                            </td>
                            <td>
                                {{$val->nama_instrumen}}
                                <input type="hidden" class="form-control" name="data[{{$key}}][instrumen_id]" value="{{$val->id_instrumen}}">
                            </td>
                            <td><input type="text" class="form-control batas_bawah" name="data[{{$key}}][batas_bawah]" value="{{$val->batas_bawah *1}}"></td>
                            <td><input type="text" class="form-control batas_atas" name="data[{{$key}}][batas_atas]" value="{{$val->batas_atas *1}}"></td>
                            <td><input type="text" class="form-control batas_interval" name="data[{{$key}}][batas_interval]"  value="{{$val->batas_interval *1}}" readonly="true"></td>
                            @for($i = 1; $i <= 3; $i++)
                                <?php $ring_bawah = "ring_".$i."_bawah";?>
                                <?php $ring_atas = "ring_".$i."_atas";?>
                                <td><input type="text" class="form-control {{$ring_bawah}}" name="data[{{$key}}][ring_{{$i}}_bawah]" value="{{$val->$ring_bawah * 1}}" readonly="true"></td>
                                <td><input type="text" class="form-control {{$ring_atas}}" name="data[{{$key}}][ring_{{$i}}_atas]" value="{{$val->$ring_atas * 1}}" readonly="true"></td>
                                @endfor
                            </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                    <input type="submit" name="proses" class="btn btn-primary" value="Simpan">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".tbody-ring input").on("keyup",function(e){
        var tr = $(this).closest('tr');
        console.log(tr);
        var bb = tr.find(".batas_bawah").val();
        var ba = tr.find(".batas_atas").val();
        if((bb != 0 && bb != "") || (ba != 0 && ba != "")){
            console.log("bb : "+bb);
            //HITUNG INTERVAL
            var bi = tr.find(".batas_interval");
            var hitungIntv = Number.parseFloat((ba *1 - bb *1) /3).toFixed(2);
            bi.val(hitungIntv);
            var r1b = tr.find(".ring_1_bawah");
            var r1a = tr.find(".ring_1_atas");
            var hitung1B = Number.parseFloat((bb*1)).toFixed(2);
            var hitung1A = Number.parseFloat((bb*1) + (hitungIntv*1)).toFixed(2);
            r1b.val(hitung1B);
            r1a.val(hitung1A);;
            var r2b = tr.find(".ring_2_bawah");
            var r2a = tr.find(".ring_2_atas");
            var hitung2B = Number.parseFloat((hitung1A*1)+ 0.01*1).toFixed(2);
            var hitung2A = Number.parseFloat((hitung2B*1) + (hitungIntv*1)).toFixed(2);
            r2b.val(hitung2B);
            r2a.val(hitung2A);
            var r3b = tr.find(".ring_3_bawah");
            var r3a = tr.find(".ring_3_atas");
            var hitung3B = Number.parseFloat((hitung2A*1)+ 0.01*1).toFixed(2);
            var hitung3A = Number.parseFloat((hitung3B*1) + (hitungIntv*1)).toFixed(2);
            r3b.val(hitung3B);
            r3a.val(hitung3A);
        }else{
            var bi = tr.find(".batas_interval");
            var r1b = tr.find(".ring_1_bawah");
            var r1a = tr.find(".ring_1_atas");
            var r2b = tr.find(".ring_2_bawah");
            var r2a = tr.find(".ring_2_atas");
            var r3b = tr.find(".ring_3_bawah");
            var r3a = tr.find(".ring_3_atas");
            bi.val(0);
            r1b.val(0);
            r1a.val(0);
            r2b.val(0);
            r2a.val(0);
            r3b.val(0);
            r3a.val(0);
            
        }
    })
</script>
@endsection