@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Input Range Per Parameter (Hematologi)</div>
                <div class="panel-body">
                @if(Session::has('message'))
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                  </div>
                @endif
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Parameter</label>
                              <select class="form-control" name="parameter">
                                <option value="{{$sd->parameter}}">{{$sd->nama_parameter}}</option>
                                @foreach($parameter as $val)
                                <option value="{{$val->id}}">{{$val->nama_parameter}}</option>
                                @endforeach
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Siklus</label>
                              <select class="form-control" name="siklus">
                                <option value="{{$sd->siklus}}">{{$sd->siklus}}</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Range 1</label>
                              <input type="text" name="ring1" class="form-control" value="{{$sd->ring1}}">
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Range 2</label>
                              <input type="text" name="ring2" class="form-control" value="{{$sd->ring2}}">
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Range 3</label>
                              <input type="text" name="ring3" class="form-control" value="{{$sd->ring3}}">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Tahun</label>
                              <select class="form-control" name="tahun">
                                <option value="{{$sd->tahun}}">{{$sd->tahun}}</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Type</label>
                              <select class="form-control" name="tipe">
                                <option value="{{$sd->tipe}}">{{$sd->tipe}}</option>
                                <option value="a">01</option>
                                <option value="b">02</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Range 4</label>
                              <input type="text" name="ring4" class="form-control" value="{{$sd->ring4}}">
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Range 5</label>
                              <input type="text" name="ring5" class="form-control" value="{{$sd->ring5}}">
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Range 6</label>
                              <input type="text" name="ring6" class="form-control" value="{{$sd->ring6}}">
                          </div>
                        </div>
                      </div>
                      <br>
                    <input type="submit" name="proses" class="btn btn-primary" value="Update">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection