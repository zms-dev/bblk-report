@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Input Range Per Instrumen / Alat (Kimia Klinik)</div>
                <div class="panel-body">
                @if(Session::has('message'))
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                  </div>
                @endif
                    <form class="form-horizontal" method="get" action="{{URL('evaluasi/input-ring/kimiaklinik-alat/insert')}}" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Siklus</label>
                              <select class="form-control" name="siklus" required="required">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Tahun</label>
                              <select class="form-control" name="tahun" required="required">
                                <option></option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                              </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Type</label>
                              <select class="form-control" name="tipe" required="required">
                                <option></option>
                                <option value="a">01</option>
                                <option value="b">02</option>
                              </select>
                          </div>
                        </div>
                      </div>
                      <br>
                    <input type="submit" name="proses" class="btn btn-primary" value="Simpan">
                    {{ csrf_field() }}
                    </form>
                    <br>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Tahun</th>
                          <th>Siklus</th>
                          <th>Tipe</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($sd as $val)
                        <tr>
                          <td>{{$val->tahun}}</td>
                          <td>{{$val->siklus}}</td>
                          <td>@if($val->tipe == 'a') 01 @else 02 @endif</td>
                          <td>
                              <a href="{{URL('evaluasi/input-ring/kimiaklinik-alat/insert').'?tahun='.$val->tahun.'&siklus='.$val->siklus.'&tipe='.$val->tipe}}" style="float: left; margin-bottom: 5px">
                                <button class="btn btn-primary btn-xs">
                                  <i class="glyphicon glyphicon-pencil"></i>
                                </button>
                              </a> &nbsp;
                              <a href="{{URL('evaluasi/input-ring/kimiaklinik-alat/delete').'?tahun='.$val->tahun.'&siklus='.$val->siklus.'&tipe='.$val->tipe}}">
                                <button class="btn btn-danger btn-xs">
                                  <i class="glyphicon glyphicon-trash"></i>
                                </button>
                              </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $sd->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection