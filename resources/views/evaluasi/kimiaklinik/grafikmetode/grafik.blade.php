@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Z-Score Peserta per Alat / Instrumen Kimia Klinik</div>

                <div class="panel-body">
                  <div id="container"></div>
                  <blockquote style="font-size: 14px">
                    <table>
                      <tr>
                        <td>Catatan : &nbsp;</td>
                        <td>- | Z Score | <= 2 </td>
                        <td>&nbsp; = Memuaskan</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td>- 2 < | Z Score | <= 3 </td>
                        <td>&nbsp; = Peringatan</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td>- | Z Score | >= 3 </td>
                        <td>&nbsp; = Tidak Memuaskan</td>
                      </tr>
                    </table>
                  </blockquote>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var chart = Highcharts.chart('container', {
  @foreach($data as $val)
  title: {text: 'Grafik Z-Score Peserta per Metode {!!$metodena->metode_pemeriksaan!!}'},
  @break
  @endforeach
  subtitle: {text: 'Plain'},
  xAxis: {categories: [
      @foreach($data as $val)
      '{{$val->nilainya}}',
      @endforeach]},
  yAxis: {
    title: {
      text : 'Jumlah Peserta'
    }
  },
  plotOptions: {
      series: {
          borderWidth: 0,dataLabels: { 
              enabled: true,
              format:'{point.y}'
          }
      }
  },
  series: [{type: 'column',
      name : 'Z-score',
      data: [
      @foreach($data as $val)
          {
          y: {{$val->nilai1}},
                @foreach($datap as $valu)
                <?php if($valu->nilainya == $val->nilainya) { ?>
                  <?php if($valu->nilai1 == '1') { ?>
                    color: '#BF0B23'
                  <?php } ?>
                <?php } ?>
                @endforeach
          },
      @endforeach
      ],
      showInLegend: true
    }]
});
</script>
@endsection