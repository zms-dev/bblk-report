@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Input Rentang Per Metode (Kimia Klinik)</div>
                <div class="panel-body">
                @if(Session::has('message'))
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                  </div>
                @endif
                    <a href="{{url('evaluasi/input-ring-metode/kimiaklinik/insert')}}" class="btn btn-primary">Tambah</a><br><br>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Parameter</th>
                          <th>Metode</th>
                          <th>Tahun</th>
                          <th>Siklus</th>
                          <th>Tipe</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($batas as $val)
                        <tr>
                          <td>{{$val->nama_parameter}}</td>
                          <td>{{$val->metode_pemeriksaan}}</td>
                          <td>{{$val->tahun}}</td>
                          <td>{{$val->siklus}}</td>
                          <td>@if($val->type == 'a') A @else B @endif</td>
                          <td>
                              <a href="{{URL('evaluasi/input-ring-metode/kimiaklinik/edit').'/'.$val->id}}" style="float: left; margin-bottom: 5px">
                                <button class="btn btn-primary btn-xs">
                                  <i class="glyphicon glyphicon-pencil"></i>
                                </button>
                              </a> &nbsp;
                              <a href="{{URL('evaluasi/input-ring-metode/kimiaklinik/delete').'/'.$val->id}}">
                                <button class="btn btn-danger btn-xs">
                                  <i class="glyphicon glyphicon-trash"></i>
                                </button>
                              </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $batas->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$("#parameter").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('metode');
  $("#metode").val("");
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getmetode').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});
</script>
@endsection