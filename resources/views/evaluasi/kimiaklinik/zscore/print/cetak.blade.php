<style type="text/css">
@page {
margin: 162px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 3px;
    text-align: left;
    border: 1px solid #333;
}

#header { 
    position: fixed; 
    border-bottom:1px solid gray;
    padding-top: -140px;
}
#footer { 
    position: fixed; 
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}

.table-bordered tbody td{
    height:30px;

}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th width="12%"></th>
                <th>
                   <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px"> 
                </th>
                <th width="50%" style="padding-left: 5px; text-align: center;" >
                    <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 12px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br>KIMIA KLINIK SIKLUS @if($siklus == 1) I @else II @endif TAHUN {{$date}}</b></span><br>
                    <span style="font-size: 12px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Jakarta</b></span>
                    <div style="padding-left: 75px; margin-top: -12px; font-size: 12px; margin-left: -80px;">
                        <span><br/>Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560<br>Telp. (021) 4212524, 42804339 Facsimile (021) 4245516<br>Website : bblkjakarta.id ; Surat Elektronik : bblkjakarta@yahoo.co.id</span>
                    </div>
                </th>
                <th width="%"></th>
               <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" height="70px">                
                    </th>

                
            </tr>
            <tr>
                <th colspan="6" style="padding: 1px;"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<div id="footer">
    HASIL EVALUASI KIMIA KLINIK SIKLUS @if($siklus == 1) I @else II @endif TAHUN {{$date}}
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                    <label>
                        <center>
                            <font size="12px">
                                <b>HASIL EVALUASI PNPME KIMIA KLINIK SIKLUS @if($siklus == 1) I @else II @endif TAHUN {{$date}}<br>BAHAN KONTROL @if($type == 'a') A @else B @endif</b><br><br>
                            </font>
                        </center>
                    </label><br>
              <table width="100%">
                <tr>
                  <th width="70px">Kode Peserta</th>
                  <td width="10px"> : </td>
                  <td>{{$register->kode_lebpes}}</td>
                </tr>
                <tr>
                  <th>Nama Instansi</th>
                  <td> : </td>
                  <td>{{$perusahaan}}</td>
                </tr>
              </table><br>
              <table class="utama" border="1" cellspacing="0" cellpadding="1"  width="100%">
                        <thead>
                            <tr>
                                <!-- <th rowspan="2" width="2%" style="text-align:center;">No.</th> -->
                                <th rowspan="2" width="" style="text-align:center;">Parameter</th>
                                <th rowspan="2" width="15%" style="text-align:center;">Alat</th>
                                <th rowspan="2" width="20%" style="text-align:center;">Metode Pemeriksaan</th>
                                <th rowspan="2" width="5%" style="text-align:center;">Hasil Saudara</th>
                                @if(count($zscore))
                                <th colspan="3" style="text-align:center;">Seluruh Peserta</th>
                                @endif
                                @if(count($zscoremetode))
                                <th colspan="3" style="text-align:center;">Kelompok Metode</th>
                                @else
                                <th colspan="2" style="text-align:center;">Kelompok Metode</th>
                                @endif
                               
                            </tr>
                            <tr>
                                <th width="5%" style="text-align:center;">Target</th>
                                <th width="5%" style="text-align:center;">Z-Score</th>
                                @if(count($zscore))
                                <th width="10%" style="text-align:center;">Keterangan</th>
                                @endif
                                <th width="5%" style="text-align:center;">Target</th>
                                <th width="5%" style="text-align:center;">Z-Score</th>
                                @if(count($zscoremetode))
                                <th width="10%" style="text-align:center;">Keterangan</th>
                                @endif
                          
                            </tr>
                        </thead>
                        <tbody><?php $no = 1;?>
                            @foreach($data as $val)
                            <?php
                                $labelHasil = $val->hasil_pemeriksaan;
                                try{
                                    $val->hasil_pemeriksaan = $val->hasil_pemeriksaan  *1;
                                }catch(\Exception $e){
                                    $val->hasil_pemeriksaan = $val->hasil_pemeriksaan;
                                }
                                if($val->hasil_pemeriksaan == "-" || $val->hasil_pemeriksaan == null || $val->hasil_pemeriksaan == '0'  || $val->hasil_pemeriksaan == '0.00' ||  $val->hasil_pemeriksaan == '0.0' || empty($val->hasil_pemeriksaan) || is_string($val->hasil_pemeriksaan)){
                                    if($val->hasil_pemeriksaan == '0' || $val->hasil_pemeriksaan == '0.0' || $val->hasil_pemeriksaan == '0.00' || is_string($val->hasil_pemeriksaan)){
                                        $val->hasil_pemeriksaan = $val->hasil_pemeriksaan;
                                    }else{
                                        $val->hasil_pemeriksaan = NULL;
                                    }
                                    $target_parameter = "-";
                                    $zscore_parameter = "-";
                                    $keteranganParameter = "Tidak Dianalisis";
                                    $target_metode = "-";
                                    $zscore_metode = "-";
                                    $keteranganMetode = "Tidak Dianalisis";
                                    $target_alat = "-";
                                    $zscore_alat = "-";
                                    $keteranganAlat = "Tidak Dianalisis";
                                }else{
                                    $target_parameter = (!empty($val->sd) ? (!empty($val->sd->median) ? $val->sd->median : '-') : '-');
                                    $sd_parameter = (!empty($val->sd) ? (!empty($val->sd->sd) ? $val->sd->sd : '-') : '-');
                                    $zscore_parameter = '-';
                                    if(($target_parameter != '-' && $sd_parameter != '-')){
                                        $zscore_parameter = number_format(($val->hasil_pemeriksaan - $val->sd->median) / $val->sd->sd,2);
                                    }
                                    $keteranganParameter = "";
                                    if($zscore_parameter == '-'){
                                        $keteranganParameter = 'Tidak Dievaluasi';
                                    }else if($zscore_parameter <= 2 && $zscore_parameter >= -2){
                                        $keteranganParameter = 'Memuaskan';
                                    }else if($zscore_parameter <= 3 && $zscore_parameter >= -3){
                                        $keteranganParameter = 'Peringatan';
                                    }else{
                                        $keteranganParameter = 'Tidak Memuaskan';
                                    }
                                    $target_metode = (!empty($val->sdmetode) ? (!empty($val->sdmetode->median) ? $val->sdmetode->median : '-') : '-');
                                    $sd_metode = (!empty($val->sdmetode) ? (!empty($val->sdmetode->sd) ? $val->sdmetode->sd : '-') : '-');
                                    $zscore_metode = '-';
                                    if(($target_metode != '-' && $sd_metode != '-')){
                                        $zscore_metode = number_format(($val->hasil_pemeriksaan - $val->sdmetode->median) / $val->sdmetode->sd,2);
                                    }
                                    $keteranganMetode = "";
                                    if($zscore_metode == '-'){
                                        $keteranganMetode = 'Tidak Dievaluasi';
                                    }else if($zscore_metode <= 2 && $zscore_metode >= -2){
                                        $keteranganMetode = 'Memuaskan';
                                    }else if($zscore_metode <= 3 && $zscore_metode >= -3){
                                        $keteranganMetode = 'Peringatan';
                                    }else{
                                        $keteranganMetode = 'Tidak Memuaskan';
                                    }

                                   
                                    
                                }
                                
                            ?>
                            <tr>
                                <!-- <td>{{$no++}}</td> -->
                                <td>
                                    {{$val->nama_parameter}}
                               </td>
                                <td style="text-align: center;">
                                    @if($val->instrumen == 'Alat Lain :')@elseif(empty($val->instrumen))-@else{{$val->instrumen}}@endif
                                    {{$val->instrument_lain}}
                                </td>
                                <td style="text-align: center;">
                                    @if($val->metode_pemeriksaan == 'Metode lain :')@elseif(empty($val->metode_pemeriksaan))-@else{!!$val->metode_pemeriksaan!!}@endif
                                    {!!$val->metode_lain!!}
                                </td>
                                <td style="text-align:center;">@if($keteranganParameter == 'Tidak Dianalisis')-@else {{($labelHasil)}}@endif</td>
                                <td style="text-align:center;">
                                    {{$target_parameter}}
                                </td>
                                <td style="text-align:center;">
                                    {{$zscore_parameter}}
                                </td>
                                <td style="text-align:center;">
                                    @if($keteranganParameter == 'Tidak Dianalisis')
                                    -
                                    @else
                                    {{$keteranganParameter}}
                                    @endif
                                </td>
                                <td style="text-align:center;">
                                    {{$target_metode}}
                                </td>
                                <td style="text-align:center;">
                                    {{$zscore_metode}}
                                </td>
                                <td style="text-align:center;">
                                    @if($keteranganMetode == 'Tidak Dianalisis')
                                    -
                                    @else
                                    {{$keteranganMetode}}
                                    @endif
                                </td>
                               
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                <table width="100%" border="">
                    <tr>
                        <td width="50%" style="vertical-align:top;padding-top:20px;">
                            <b>Keterangan : </b>
                            <p style="margin-left: 30px">Kriteria Nilai :</p>
                            <ul>
                                <li><span style="width:80px;display:inline-block;"><b>|</b> Z Score |<span style="font-family: DejaVu Sans; sans-serif;">&le;</span> 2</span> = Memuaskan</li>
                                <li><span style="width:80px;display:inline-block;">2 &lt;<b>|</b> Z Score|<span style="font-family: DejaVu Sans; sans-serif;">&le;</span> 3</span> = Peringatan</li>
                                <li><span style="width:80px;display:inline-block;"><b>|</b> Z Score|<span style="font-family: DejaVu Sans; sans-serif;">&gt;</span> 3</span> = Tidak Memuaskan</li>
                            </ul>
                        </td>
                        <td width="50%" style="vertical-align:top;">
                        <div style="position: relative;left: 65%; top: -3%;">
                        <p>
                        <div style="position: relative; top: 22px">
                        Jakarta,  {{ _dateIndo($evaluasi->tanggal) }}<br>
                        Penanggung Jawab
                        </div>
                        <p>

                            <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'imunologi_ttd.png')}}" width="160" style="margin: -40px 0px !important; position: absolute;">
                        </p>
                        <div style="position: relative; top:80px">
                        dr. Siti Kurnia Eka Rusmiarti, Sp.PK
                        </div>
                        </p>
                        </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>