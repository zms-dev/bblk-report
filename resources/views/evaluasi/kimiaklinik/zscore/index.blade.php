@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hitung Z-Score Kimia Klinik</div>

                <div class="panel-body">
                @if(count($zscore))
                <!-- <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('/hasil-pemeriksaan/kimia-klinik/evaluasi')}}/print/{{$id}}?x={{$type}}&y={{$siklus}}" target="_blank"> -->
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                @else
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th rowspan="2"><center>No.</center></th>
                                <th rowspan="2"><center>Kode Peserta</center></th>
                                <th rowspan="2"><center>Parameter</center></th>
                                <th rowspan="2"><center>Alat</center></th>
                                <th rowspan="2"><center>Metode Pemeriksaan</center></th>
                                <th rowspan="2"><center>Hasil Saudara</center></th>
                                <th colspan="3" class="text-center">Seluruh Peserta</th>
                                <th colspan="3" class="text-center">Kelompok Metode</th>
<!--                                 <th colspan="3">Alat / Instrumen</th>
 -->                            </tr>
                            <tr>
                                <th class="text-center">Target</th>
                                <th class="text-center">Z&nbsp;Score</th>
                                <th class="text-center">Keterangan</th>
                                <th class="text-center">Target</th>
                                <th class="text-center">Z&nbsp;Score</th>
                                <th class="text-center">Keterangan</th>
                      <!--           <th class="text-center">Target</th>
                                <th class="text-center">Z-Score</th> -->
<!--                                 <th class="text-center">Keterangan</th>
 -->                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;?>
                            @foreach($data as $val)
                            <?php
                                $labelHasil = $val->hasil_pemeriksaan;
                                try{
                                    $val->hasil_pemeriksaan = $val->hasil_pemeriksaan  *1;
                                }catch(\Exception $e){
                                    $val->hasil_pemeriksaan = $val->hasil_pemeriksaan;
                                }
                                if($val->hasil_pemeriksaan == "-" || $val->hasil_pemeriksaan == null || $val->hasil_pemeriksaan == '0'  || $val->hasil_pemeriksaan == '0.00' ||  $val->hasil_pemeriksaan == '0.0' || empty($val->hasil_pemeriksaan) || is_string($val->hasil_pemeriksaan)){
                                    if($val->hasil_pemeriksaan == '0' || $val->hasil_pemeriksaan == '0.0' || $val->hasil_pemeriksaan == '0.00' || is_string($val->hasil_pemeriksaan)){
                                        $val->hasil_pemeriksaan = $val->hasil_pemeriksaan;
                                    }else{
                                        $val->hasil_pemeriksaan = NULL;
                                    }
                                    
                                    $target_parameter = "-";
                                    $zscore_parameter = "-";
                                    $keteranganParameter = "Tidak Dianalisis";
                                    $target_metode = "-";
                                    $zscore_metode = "-";
                                    $keteranganMetode = "Tidak Dianalisis";
                                    $target_alat = "-";
                                    $zscore_alat = "-";
                                    $keteranganAlat = "Tidak Dianalisis";
                                }else{
                                    $target_parameter = (!empty($val->sd) ? (!empty($val->sd->median) ? $val->sd->median : '-') : '-');
                                    $sd_parameter = (!empty($val->sd) ? (!empty($val->sd->sd) ? $val->sd->sd : '-') : '-');
                                    $zscore_parameter = '-';
                                    if(($target_parameter != '-' && $sd_parameter != '-')){
                                        $zscore_parameter = number_format(($val->hasil_pemeriksaan - $val->sd->median) / $val->sd->sd,2);
                                    }
                                    $keteranganParameter = "";
                                    if($zscore_parameter == '-'){
                                        $keteranganParameter = 'Tidak Dievaluasi';
                                    }else if($zscore_parameter <= 2 && $zscore_parameter >= -2){
                                        $keteranganParameter = 'Memuaskan';
                                    }else if($zscore_parameter <= 3 && $zscore_parameter >= -3){
                                        $keteranganParameter = 'Peringatan';
                                    }else{
                                        $keteranganParameter = 'Tidak Memuaskan';
                                    }
                                    $target_metode = (!empty($val->sdmetode) ? (!empty($val->sdmetode->median) ? $val->sdmetode->median : '-') : '-');
                                    $sd_metode = (!empty($val->sdmetode) ? (!empty($val->sdmetode->sd) ? $val->sdmetode->sd : '-') : '-');
                                    $zscore_metode = '-';
                                    if(($target_metode != '-' && $sd_metode != '-')){
                                        $zscore_metode = number_format(($val->hasil_pemeriksaan - $val->sdmetode->median) / $val->sdmetode->sd,2);
                                    }
                                    $keteranganMetode = "";
                                    if($zscore_metode == '-'){
                                        $keteranganMetode = 'Tidak Dievaluasi';
                                    }else if($zscore_metode <= 2 && $zscore_metode >= -2){
                                        $keteranganMetode = 'Memuaskan';
                                    }else if($zscore_metode <= 3 && $zscore_metode >= -3){
                                        $keteranganMetode = 'Peringatan';
                                    }else{
                                        $keteranganMetode = 'Tidak Memuaskan';
                                    }

                                    $target_alat = (!empty($val->sdalat) ? (!empty($val->sdalat->median) ? $val->sdalat->median : '-') : '-');
                                    $sd_alat = (!empty($val->sdalat) ? (!empty($val->sdalat->sd) ? $val->sdalat->sd : '-') : '-');
                                    $zscore_alat = '-';
                                    if(($target_alat != '-' && $sd_alat != '-')){
                                        $zscore_alat = number_format(($val->hasil_pemeriksaan - $val->sdalat->median) / $val->sdalat->sd,2);
                                    }
                                    $keteranganAlat = "";
                                    if($zscore_alat == '-'){
                                        $keteranganAlat = 'Tidak Dievaluasi';
                                    }else if($zscore_alat <= 2 && $zscore_alat >= -2){
                                        $keteranganAlat = 'Memuaskan';
                                    }else if($zscore_alat <= 3 && $zscore_alat >= -3){
                                        $keteranganAlat = 'Peringatan';
                                    }else{
                                        $keteranganAlat = 'Tidak Memuaskan';
                                    }
                                    
                                    
                                }
                                
                            ?>
                            <tr>
                                <td>{{$no++}}</td>
                                <td>{{$val->kode_lab}}</td>
                                <td>
                                    {{$val->nama_parameter}}
                                    <input type="hidden" name="parameter[]" value="{{$val->id}}">
                                    <input type="hidden" name="zscore[]" value="{{$zscore_parameter}}">
                                    <input type="hidden" name="zscoremetode[]" value="{{$zscore_metode}}">
                                    <input type="hidden" name="zscorealat[]" value="{{$zscore_alat}}">
                                </td>
                                <td>
                                    @if($val->instrumen == NULL) {{(!empty($val->Instrumen) ? $val->Instrumen : '-')}} @else {{$val->instrumen}} @endif {{(!empty($val->instrument_lain) ? $val->instrument_lain : '' )}}
                                    <input type="hidden" name="alat[]" value="{{$val->idalat}}">
                                </td>
                                <td>
                                    {!!(!empty($val->metode_pemeriksaan) ? $val->metode_pemeriksaan : '-')!!} {{$val->metode_lain}}
                                    <input type="hidden" name="metode[]" value="{{$val->idmetode}}">
                                </td>
                                <td>{{($labelHasil)}}</td>
                                <td class="text-center">
                                    {{$target_parameter}}
                                </td>
                                <td class="text-center">
                                    {{$zscore_parameter}}
                                </td>
                                <td class="text-center">
                                    {{$keteranganParameter}}
                                </td>
                                <td class="text-center">
                                    {{$target_metode}}
                                </td>
                                <td class="text-center">
                                    {{$zscore_metode}}
                                </td>
                                <td class="text-center">
                                    {{$keteranganMetode}}
                                </td>
                                <!-- <td class="text-center">
                                    {{$target_alat}}
                                </td>
                                <td class="text-center">
                                    {{$zscore_alat}}
                                </td>
                                <td class="text-center">
                                    {{$keteranganAlat}}
                                </td> -->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @if(count($zscore))
                <input type="submit" name="simpan" class="btn btn-primary btn-print" value="Print">
                <input type="submit" name="simpan" class="btn btn-danger" value="Batal Evaluasi">
                @else
                <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                @endif
                {{ csrf_field() }}
                </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".btn-print").on("click",function(e){
        e.preventDefault();
        window.open("{{url('/hasil-pemeriksaan/kimia-klinik/evaluasi')}}/print/{{$id}}?x={{$type}}&y={{$siklus}}");
    })
</script>
@endsection