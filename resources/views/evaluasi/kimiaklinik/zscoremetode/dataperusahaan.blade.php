@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Hitung Z-Scroe Metode Kimia Klinik</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Bidang</th>
                            <th colspan="2">Siklus&nbsp;1</th>
                            <th colspan="2">Siklus&nbsp;2</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->bidang == '2')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                @if($val->siklus_1 == 'done')
                                    @if($val->pemeriksaan == 'done')
                                        <td>
                                            <a href="{{URL('').$val->Link}}/evaluasi-metode/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>I-01</center></a>
                                        </td>
                                    @else
                                    <td></td>
                                    @endif
                                    @if($val->pemeriksaan2 == 'done')
                                        <td>
                                            <a href="{{URL('').$val->Link}}/evaluasi-metode/{{$val->id}}?x=b&y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>I-02</center></a>
                                        </td>
                                    @else
                                    <td></td>
                                    @endif
                                @else
                                <td></td>
                                @endif
                                @if($val->siklus_2 == 'done')
                                    @if($val->rpr1 == 'done')
                                        <td>
                                            <a href="{{URL('').$val->Link}}/evaluasi-metode/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>II-01</center></a>
                                        </td>
                                    @else
                                    <td></td>
                                    @endif
                                    @if($val->rpr2 == 'done')
                                        <td>
                                            <a href="{{URL('').$val->Link}}/evaluasi-metode/{{$val->id}}?x=b&y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>II-02</center></a>
                                        </td>
                                    @else
                                    <td></td>
                                    @endif
                                @else
                                <td></td>
                                @endif
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection