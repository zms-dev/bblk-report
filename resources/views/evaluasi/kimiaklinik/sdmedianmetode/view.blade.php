@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Input Nilai SD dan Median Per Metode (Kimia Klinik)</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="siklus" value="{{$siklus}}">
                    <input type="hidden" name="tahun" value="{{$tahun}}">
                    <input type="hidden" name="tipe" value="{{$tipe}}">
                    <table class="table table-bordered">
                      <tr>
                        <th>Parameter</th>
                        <th>Metode</th>
                        <th>SD</th>
                        <th>Median</th>
                      </tr>
                      @foreach($metode as $val)
                      <tr>
                        <td>
                          <input type="hidden" name="parameter[]" value="{{$val->parameter_id}}">
                          <input type="hidden" name="id_sd_med[]" value="{{$val->id_sd_med}}">
                          {!!$val->nama_parameter!!}
                        </td>
                        <td>
                          <input type="hidden" name="metode[]" value="{{$val->id}}">{!!$val->metode_pemeriksaan!!}
                        </td>
                        <td><input type="text" name="sd[]" class="form-control" value="{{$val->sd}}"></td>
                        <td><input type="text" name="median[]" class="form-control" value="{{$val->median}}"></td>
                      </tr>
                      @endforeach
                    </table>
                    <input type="submit" name="proses" class="btn btn-primary" value="Simpan">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection