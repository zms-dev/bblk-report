<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama, table.utama td, table.utama th {    
    border: 1px solid #ddd;
    text-align: left;
}

table.utama {
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td {
    padding: 5px;
}
</style>
<table width="100%">
  <tr>
    <th colspan="4"><center>HASIL SD DAN MEDIAN PER METODE</center></th>
  </tr>
  <tr>
    <th colspan="4"><center>PNPME BALAI BESAR LABORATORIUM KESEHATAN JAKARTA</center></th>
  </tr>
</table>
<table>
  <tr>
    <td>Siklus </td>
    <td>: {{$request->siklus}}</td>
  </tr>  
  <tr>
    <td>Tahun</td>
    <td>: {{$request->tahun}}</td>
  </tr>  
  <tr>
    <td>Kode Bahan</td>
    <td>: @if($request->tipe == 'a')01 @else 02 @endif</td>
  </tr>  
</table>
<table class="utama">
  <tr>
    <th>Parameter</th>
    <th>Metode</th>
    <th>SD</th>
    <th>Median</th>
  </tr>
  @foreach($sd as $val)
  <tr>
    <td>{{$val->nama_parameter}}</td>
    <td>{!!$val->metode_pemeriksaan!!}</td>
    <td>{{$val->sd}}</td>
    <td>{{$val->median}}</td>
  </tr>
  @endforeach
</table>