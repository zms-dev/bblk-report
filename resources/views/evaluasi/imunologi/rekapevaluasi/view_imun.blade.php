<table border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Instansi</th>
            <th>Kode</th>
            <th>Form</th>
            @if($type == 1)
                <th>Nama Produsen</th>
                <th>Reagen</th>
                <th>Metode</th>
                <th>Lot</th>
                <th>Exp. Reagen</th>
            @else
                <th>Nama Produsen 1</th>
                <th>Reagen 1</th>
                <th>Metode 1</th>
                <th>Lot 1</th>
                <th>Exp. Reagen 1</th>
                <th>Nama Produsen 2</th>
                <th>Reagen 2</th>
                <th>Metode 2</th>
                <th>Lot 2</th>
                <th>Exp. Reagen 2</th>
                <th>Nama Produsen 3</th>
                <th>Reagen 3</th>
                <th>Metode 3</th>
                <th>Lot 3</th>
                <th>Exp. Reagen 3</th>
            @endif
            <?php $no = 0; ?>
            @foreach($data[0]->strategi as $st)
                <?php $no++; ?>
                <th>Hasil {{$st->kode_bahan_kontrol}}</th>
                @if($type == 1)
                <th>Kesesuaian Hasil {{$st->kode_bahan_kontrol}}</th>
                @endif
            @endforeach
            <th>Penilaian</th>
            <th>Ketepatan Hasil</th>
            <th>Kesesuaian Strategi</th>
        </tr>
    </thead>
    <tbody>
        <?php $angka = 0; ?>
        @foreach($data as $val)
        <?php $angka++; ?>
        <tr>
            <td>{{$angka}}</td>
            <td>{{$val->nama_lab}}</td>
            <td>{{$val->kode_lebpes}}</td>
            <td>{{$val->jenis_form}}</td>
            @if($type == 1)
                @foreach($val->reagen as $reg)
                <td>{{$reg->nama_produsen }}</td>
                <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                <td>{{$reg->metode}}</td>
                <td>{{$reg->nomor_lot}}</td>
                <td>{{$reg->tgl_kadaluarsa}}</td>
                @endforeach
                <?php $non = 0; ?>
                @foreach($val->strategi as $st)
                    <?php $non++; ?>
                    <td>{{$st->interpretasi}}</td>
                    @if($type == 1)
                    <td>
                        @if($st->interpretasi == 'Tanpa test')
                        @else
                            @if($st->interpretasi == $st->nilai_rujukan)
                            Benar
                            @else
                            Salah
                            @endif
                        @endif
                    </td>
                    @endif
                @endforeach
            @else
                @if(count($val->reagen) == 3)
                    @foreach($val->reagen as $reg)
                    <td>{{$reg->nama_produsen }}</td>
                    <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                    <td>{{$reg->metode}}</td>
                    <td>{{$reg->nomor_lot}}</td>
                    <td>{{$reg->tgl_kadaluarsa}}</td>
                    @endforeach
                @elseif(count($val->reagen) == 2)
                    @foreach($val->reagen as $reg)
                    <td>{{$reg->nama_produsen }}</td>
                    <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                    <td>{{$reg->metode}}</td>
                    <td>{{$reg->nomor_lot}}</td>
                    <td>{{$reg->tgl_kadaluarsa}}</td>
                    @endforeach
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @elseif(count($val->reagen) == 1)
                    @foreach($val->reagen as $reg)
                    <td>{{$reg->nama_produsen }}</td>
                    <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                    <td>{{$reg->metode}}</td>
                    <td>{{$reg->nomor_lot}}</td>
                    <td>{{$reg->tgl_kadaluarsa}}</td>
                    @endforeach
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @endif
                <?php $non = 0; ?>
                @foreach($val->strategi as $st)
                    <?php $non++; ?>
                    <td>{{$st->interpretasi}}</td>
                @endforeach
            @endif
            <td>
            @if(count($val->saran))
                @if($val->saran->nilai_1 != NULL)
                    Tahap pemeriksaan yang Saudara kerjakan sudah Sesuai dengan strategi III pemeriksaan Anti HIV. |
                @endif
                @if($val->saran->nilai_2 != NULL)
                    Tahap pemeriksaan yang Saudadra kerjakan Tidak Sesuai dengan stragegi III pemeriksaan Anti HIV. Gunakan alur pemeriksaan yang sesuai dengan strategi III untuk menegakkan diagnosis HIV. |
                @endif
                @if($val->saran->nilai_3 != NULL)
                    Jenis reagen yang Saudara gunakan Sesuai dengan hasil evaluasi laboratorium rujukan nasional RSUPN Cipto Mangukusumo. |
                @endif
                @if($val->saran->nilai_4 != NULL)
                    Jenis reagen yang Saudara gunakan Tidak Sesuai dengan hasil evaluasi laboratorium rujukan RSUPN Cipto Mangukusumo. Gunakan jenis reagen yang sudah dilakukan evaluasi oleh RSUPN Cipto Mangukusumo. |
                @endif
                @if($val->saran->nilai_5 != NULL)
                    Urutan reagen yang Saudara gunakan Sesuai dengan hasil evaluasi laboratorium rujukan nasional RSUPN Cipto Mangukusumo. |
                @endif
                @if($val->saran->nilai_6 != NULL)
                    Urutan reagen yang Saudara gunakan Tidak Sesuai dengan hasil evaluasi laboratorium rujukan nasional RSUPN Cipto Mangukusumo. Gunakan urutan reagen berdasarkan ketentuan sensitivitas dan spesifisitas, lihat hasil evaluasi RSUPn Cipto Mangukusumo. |
                @endif
                @if($val->saran->nilai_7 != NULL)
                    Tahap pemeriksaan yang Saudara kerjaan Sesuai dengan strategi I pemeriksaan Anti HIV. |
                @endif
            @endif
            </td>
            <td>{!!html_entity_decode($val->ketepatan)!!}</td>
            <td>{!!html_entity_decode($val->kesesuaian)!!}</td>
        </tr>
        @endforeach
    </tbody>
</table>