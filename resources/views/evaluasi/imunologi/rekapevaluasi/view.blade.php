<table border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Instansi</th>
            <th>Kode</th>
            <th>Nama Produsen</th>
            <th>Reagen</th>
            <th>Metode</th>
            <th>Lot</th>
            <th>Exp. Reagen</th>
            <?php
                for ($i=1; $i <= 3; $i++) {
            ?>
                <th>Hasil {{$kode}}{{$i}}</th>
                @if($type == 1)
                <th>Titer {{$kode}}{{$i}}</th>
                <th>Rentan Titer {{$kode}}{{$i}}</th>
                @endif
                <th>Kesesuaian {{$kode}}{{$i}}</th>
            <?php } ?>
            <th> Ketepatan Hasil</th>
            <th>Saran</th>
        </tr>
    </thead>
    <tbody>
        <?php $angka = 0; $r = 0;?>
        @foreach($data as $val)
        <?php $angka++; ?>
        <tr>
            <td>{{$angka}}</td>
            <td>{{$val->nama_lab}}</td>
            <td>{{$val->kode_lebpes}}</td>
            @if(count($val->reagen))
              @foreach($val->reagen as $reagen)
              <td>{{$reagen->nama_produsen }}</td>
              @endforeach
            @else
              <td></td>
            @endif
            @foreach($val->reagen as $reg)
            <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
            <td>{{$reg->metode}}</td>
            <td>{{$reg->nomor_lot}}</td>
            <td>{{$reg->tgl_kadaluarsa}}</td>
            @endforeach
            @if(count($val->strategi))
            <?php $no = 0;?>
            @foreach($val->strategi as $st)
            <td>{{$st->interpretasi}}</td>
            <td>
                @if($st->interpretasi == $st->rujukan[$no]->nilai_rujukan)
                    <?php
                        $kesesuaian_hasil = 'Benar';
                    ?>
                    {{$kesesuaian_hasil}}
                @elseif($st->interpretasi == 'tanpa-test')
                    -
                @else
                    <?php
                        $kesesuaian_hasil = 'Salah';
                    ?>
                    {{$kesesuaian_hasil}}
                @endif
            </td>
            <?php $no++; ?>
            @endforeach
            @else
            <?php
                $r++;
                for ($i=1; $i <= 3; $i++) {
            ?>
            <td></td>
            <td></td>
            <?php } ?>
            @endif
            <td>
            @foreach($val->kesimpulan as $kesimpulan)
                {{$kesimpulan->ketepatan}}
            @endforeach
            </td>
            @if(count($val->catatan)>0)
            <td>{{strip_tags($val->catatan->catatan)}}</td>
            @else
            <td>...</td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
