@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluasi</div>
                <div class="panel-body">
                    <center><label>EVALUASI PESERTA PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL SIFILIS SIKLUS {{$type}} TAHUN {{$tahun}} <br> HASIL EVALUASI ANTI TP<input type="hidden" name="type" value="{{$type}}"></label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta </label>
                        <div class="col-sm-10">
                            <input readonly="" type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $kodeperusahaan }}" placeholder="Kode Peserta">
                        </div>
                    </div><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama Instansi </label>
                        <div class="col-sm-10">
                            <input readonly="" type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $perusahaan }}" placeholder="Kode Peserta">
                        </div>
                    </div>
                    @if(count($kesimpulan))
                        <form id="someForm" action="" method="POST">
                    @else
                        <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    @endif
                    <label>2. HASIL EVALUASI</label>
                    <table class="table table-bordered" id="peserta_pme">
                        <tr class="titlerowna">
                            <th>Kode Bahan Uji</th>
                            <th>Reagen</th>
                            <th>Tanggal Kadaluarsa</th>
                            <th>Hasil Pemeriksaan</th>
                            <th>Hasil Rujukan</th>
                            <th>Ketepatan Hasil</th>
                            <!-- <td>Kesesuaian Strategi</td> -->
                        </tr>
                        <?php  
                            $no = 0;
                            $val = ['I','II','III','IV','V'];
                        ?>
                        @if(count($data2))
                            @if(count($strategi))
                                @foreach($strategi as $str)
                                    <input type="hidden" name="idevaluasi[]" value="{{$str->id}}">
                                @endforeach
                            @endif
                        @foreach($data2 as $hasil)
                        <tr><input readonly="" type="hidden" name="idhp[]" value="{{$hasil->id}}">
                            <td>
                                <?php if(strlen($hasil->kode_bahan_kontrol) > 3){ ?>
                                    <input readonly="" type="hidden" name="kode_bahan[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}">{{$hasil->kode_bahan_kontrol}}
                                <?php }else{?>
                                    <input readonly="" type="hidden" name="kode_bahan[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}">II-{{$no+1}}-{{$hasil->kode_bahan_kontrol}}
                                <?php }?>
                            </td>
                            @if($hasil->interpretasi == 'Tanpa test')
                            <td></td>
                            <td></td>
                            @else
                                @if(!empty($reagen[0]))
                                    <td>
                                        {!! str_replace('Lain - lain', '', $reagen[0]->reagen_lain.$reagen[0]->reagen)!!}
                                    </td>
                                @else
                                <td></td>
                                @endif
                                <td>{{$reagen[0]->tgl_kadaluarsa}}</td>
                            @endif
                            <td>{{$hasil->interpretasi}}</td>
                            @if(count($rujukan[$no]))
                                <td>{{$rujukan[$no]->nilai_rujukan}}</td>
                                <td>
                                    @if($hasil->interpretasi == $rujukan[$no]->nilai_rujukan)
                                    Benar
                                    @elseif($hasil->interpretasi == 'tanpa-test')
                                    @else
                                    Salah
                                    @endif
                                </td>
                            @endif
                            <input type="hidden" name="id_master_imunologi" value="{{$hasil->id_master_imunologi}}">
                        <?php $no++; ?>
                            <td hidden="">
                                <select name="strategi[]" class="form-control" id="strategi1">
                                        @if(count($strategi))
                                            @foreach($strategi as $str)
                                                @if($str->tabung == $no)
                                                    <option value="{{$str->nilai}}">{{$str->kategori}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    <option value=""></option>
                                    <option value="0">Tidak Sesuai</option>
                                    <option value="5">Sesuai</option>
                                </select>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        @foreach($rujukan as $val)
                        <tr>
                            <td>{{$val->kode_bahan_uji}}</td>
                            <td></td>
                            <td>TIDAK DIKERJAKAN</td>
                            <td>{{$val->nilai_rujukan}}</td>
                            <td></td>
                            <td>
                            </td>
                        </tr>
                        @endforeach
                        <input type="hidden" name="id_master_imunologi" value="">
                        @endif
                    </table>
                    @if(count($evaluasi))
                    <p>
                        <input type="checkbox" name="nilai_8" value="ada" {{ ($evaluasi->nilai_8 == 'ada') ? 'checked' : '' }}> 
                        1. Hasil pemeriksaan tidak dilakukan evaluasi karena tidak mencantumkan tanggal kedaluwarsa reagen.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_9" value="ada" {{ ($evaluasi->nilai_9 == 'ada') ? 'checked' : '' }}> 
                        2. Hasil pemeriksaan tidak dilakukan evaluasi karena reagen yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    @else
                    <p>
                        <input type="checkbox" name="nilai_8" value="ada"> 
                        1. Hasil pemeriksaan tidak dilakukan evaluasi karena tidak mencantumkan tanggal kedaluwarsa reagen.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_9" value="ada"> 
                        2. Hasil pemeriksaan tidak dilakukan evaluasi karena reagen yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    @endif

                    <h4>Penilaian Hasil Pemeriksaan Peserta :</h4>
                    <blockquote>
                        1. Ketepatan Hasil Pemeriksaan  <select name="ketepatan">
                                                @if(count($kesimpulan))
                                                    @if($kesimpulan->ketepatan != NULL)
                                                        <option value="{{$kesimpulan->ketepatan}}">{{$kesimpulan->ketepatan}}</option>
                                                        <option value="Baik">Baik</option>
                                                        <option value="Kurang">Kurang</option>
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @else
                                                        <option></option>
                                                        <option value="Baik">Baik</option>
                                                        <option value="Kurang">Kurang</option>
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @endif
                                                @else
                                                    <option></option>
                                                    <option value="Baik">Baik</option>
                                                    <option value="Kurang">Kurang</option>
                                                    <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                @endif
                                            </select><br>
                    </blockquote>
                    <label>Saran :</label>
                    @if(count($catatan))
                    <textarea name="catatan" id="editor1" rows="10" cols="80">{!!$catatan->catatan!!}</textarea>
                    @else
                    <textarea name="catatan" id="editor1" rows="10" cols="80"></textarea>
                    @endif
                    <br>
                    @if(count($kesimpulan))
                    <a href="{{url('hasil-pemeriksaan/syphilis/evaluasi/print').'/'.$register->id.'?y='.$type}}">
                        <input type="button" class="btn btn-primari" value="Print" name="print"/>
                    </a>
                    <input type="button" class="btn btn-primari" value="Update" name="update" onclick="askForUpdate()" />
                    @else
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-primari">
                    @endif
                      {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
CKEDITOR.replace( 'editor1' );
form=document.getElementById("someForm");
function askForUpdate() {
        form.action="{{url('hasil-pemeriksaan/syphilis/evaluasi/update').'/'.$register->id.'?y='.$type}}";
        form.submit();
}

var total2=[0,0,0];
$(document).ready(function(){

    var $dataRows=$("#peserta_pme tr:not('.titlerowna')");
    
    $dataRows.each(function() {
        $(this).find('.colomngitung').each(function(i){        
            total2[i]+=parseInt( $(this).html());
        });
    });
    $("#peserta_pme td.evaluasi").each(function(i){  
        if (total2[i] < 15) {
            console.log( "no ready!" );
            $(".evaluasi").html("Kurang Baik");
        }else{
            $(".evaluasi").html("Baik");
            console.log( "Baik" );
        }
    });

});

$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
</script>
@endsection
