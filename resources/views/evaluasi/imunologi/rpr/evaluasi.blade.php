@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluasi</div>
                <div class="panel-body">
                    <center><label>EVALUASI PESERTA PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL SIFILIS SIKLUS {{$type}} TAHUN {{$tahun}} <br> HASIL EVALUASI RPR<input type="hidden" name="type" value="{{$type}}"></label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta </label>
                        <div class="col-sm-10">
                            <input readonly="" type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $kodeperusahaan }}" placeholder="Kode Peserta">
                        </div>
                    </div><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama Instansi </label>
                        <div class="col-sm-10">
                            <input readonly="" type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $perusahaan }}" placeholder="Kode Peserta">
                        </div>
                    </div>
                    @if(count($kesimpulan))
                        <form id="someForm" action="" method="POST">
                    @else
                        <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    @endif
                    <label>2. HASIL EVALUASI</label>
                    <table class="table table-bordered" id="peserta_pme">
                        <tr class="titlerowna">
                            <th>Kode Bahan Uji</th>
                            <th>Reagen</th>
                            <th>Hasil Pemeriksaan</th>
                            <th>Titer</th>
                            <th>Hasil Rujukan</th>
                            {{--<th>Kesesuaian Hasil</th>--}}
                            <th>Rentang Titer yang Dapat Diterima</th>
                            <th>Kesesuaian Hasil</th>
                        </tr>
                        <?php  
                            $no = 0;
                            $val = ['I','II','III','IV','V'];
                        ?>
                        @if(count($data2))
                        @if(count($strategi))
                            @foreach($strategi as $str)
                                <input type="hidden" name="idevaluasi[]" value="{{$str->id}}">
                            @endforeach
                        @endif
                        @foreach($data2 as $hasil)
                        <tr><input readonly="" type="hidden" name="idhp[]" value="{{$hasil->id}}">
                            <td><input readonly="" type="text" name="kode_bahan[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}"></td>
                        @if($hasil->interpretasi == 'Tanpa test')
                        <td></td>
                        @else
                            @if(!empty($reagen[0]))
                                <td>
                                    @if($reagen[0]->reagen == 'Lain - lain')
                                    {{$reagen[0]->reagen_lain}}
                                    @else
                                    {{$reagen[0]->reagen}}
                                    @endif
                                </td>
                            @else
                            <td></td>
                            @endif
                        @endif
                            <td>{{$hasil->interpretasi}}</td>
                            <td hidden="">
                                @if($hasil->interpretasi == $hasil->nilai_rujukan)
                                <?php
                                    $kesesuaian_hasil = 'Benar';
                                ?>
                                 {{ $kesesuaian_hasil }}
                                @elseif($hasil->interpretasi == 'tanpa-test')
                                @else
                                <?php
                                    $kesesuaian_hasil = 'Salah';
                                ?>
                                {{ $kesesuaian_hasil }}
                                @endif
                            </td>
                            <td>
                                {{$hasil->titer}}&nbsp;
                                @if(count($hasil->titer))
                                    @if(explode(":", $hasil->titer) >= explode(":", $hasil->range1) && explode(":", $hasil->titer) <= explode(":", $hasil->range2))
                                    <?php $titer = 'Benar';
                                    ?>
                                    <!-- ({{ $titer }}) -->
                                    @else
                                    <?php $titer = 'Salah';
                                    ?>
                                    <!-- ({{ $titer }}) -->
                                    @endif
                                @else
                                    <?php 
                                        $titer = '';
                                    ?>

                                @endif
                            </td>
                            <td>{{$hasil->nilai_rujukan}}</td>
                            <td>{{$hasil->range1}} - {{$hasil->range2}}</td>
                            <td>
                                <?php
                                    if($hasil->interpretasi == 'Non Reaktif'){
                                        if ($kesesuaian_hasil == 'Benar') {
                                            echo "Benar";
                                        }else{
                                            echo "Salah";                                    
                                        }
                                    }else{
                                        if ($titer == '') {
                                            echo "-";
                                        }else if (($kesesuaian_hasil == 'Benar') && ($titer == 'Benar')) {
                                            echo "Benar";
                                        }else if (($kesesuaian_hasil == 'Salah') && ($titer == 'Salah')) {
                                            echo "Salah";
                                        }else{
                                            echo "Salah";                                    
                                        }
                                    }
                                ?>
                            </td>
                        <?php $no++; ?>
                            <input type="hidden" name="id_master_imunologi" value="{{$hasil->id_master_imunologi}}">
                            <!-- <td>
                                <select name="strategi[]" class="form-control" id="strategi1">
                                        @if(count($strategi))
                                            @foreach($strategi as $str)
                                                @if($str->tabung == $no)
                                                    <option value="{{$str->nilai}}">{{$str->kategori}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    <option value=""></option>
                                    <option value="0">Salah</option>
                                    <option value="5">Benar</option>
                                </select>
                            </td>-->
                        </tr>
                        @endforeach
                        @else
                        @foreach($rujukan as $val)
                        <tr>
                            <td>{{$val->kode_bahan_uji}}</td>
                            <td></td>
                            <td>TIDAK DIKERJAKAN</td>
                            <td></td>
                            <td>{{$val->nilai_rujukan}}</td>
                            <td>{{$val->range1}} - {{$val->range2}}</td>
                            <td></td>
                        </tr>
                        @endforeach
                        <input type="hidden" name="id_master_imunologi" value="">
                        @endif
                    </table>
                    <h4>Penilaian Hasil Pemeriksaan Peserta :</h4>
                    <blockquote>
                        1. Ketepatan Hasil  <select name="ketepatan">
                                                @if(count($kesimpulan))
                                                    @if($kesimpulan->ketepatan != NULL)
                                                        <option value="{{$kesimpulan->ketepatan}}">{{$kesimpulan->ketepatan}}</option>
                                                        <option value="Baik">Baik</option>
                                                        <option value="Kurang">Kurang</option>
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @else
                                                        <option></option>
                                                        <option value="Baik">Baik</option>
                                                        <option value="Kurang">Kurang</option>
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @endif
                                                @else
                                                    <option></option>
                                                    <option value="Baik">Baik</option>
                                                    <option value="Kurang">Kurang</option>
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                @endif
                                            </select><br>
                    </blockquote>
                    <label>Saran :</label>
                    @if(count($catatan))
                    <textarea name="catatan" id="editor1" rows="10" cols="80">{!!$catatan->catatan!!}</textarea>
                    @else
                    <textarea name="catatan" id="editor1" rows="10" cols="80"></textarea>
                    @endif
                    <br>
                    <label>Tanggal TTD :</label>
                    <div class="controls input-append date" data-link-field="dtp_input1">
                        @if(count($catatan))
                        <input size="16" type="text" value="{{$catatan->tanggal_ttd}}" required class="form_datetime form-control" name="ttd">
                        @else
                        <input size="16" type="text" value="" required class="form_datetime form-control" name="ttd">
                        @endif
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div><br>
                    @if(count($kesimpulan))
                    <a href="{{url('hasil-pemeriksaan/rpr-syphilis/evaluasi/print').'/'.$register->id.'?y='.$type}}">
                        <input type="button" class="btn btn-primari" value="Print" name="print"/>
                    </a>
                    <input type="button" class="btn btn-primari" value="Update" name="update" onclick="askForUpdate()" />
                    @else
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-primari">
                    @endif
                      {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
CKEDITOR.replace( 'editor1' );
form=document.getElementById("someForm");
function askForUpdate() {
        form.action="{{url('hasil-pemeriksaan/rpr-syphilis/evaluasi/update').'/'.$register->id.'?y='.$type}}";
        form.submit();
}

var total2=[0,0,0];
$(document).ready(function(){

    var $dataRows=$("#peserta_pme tr:not('.titlerowna')");
    
    $dataRows.each(function() {
        $(this).find('.colomngitung').each(function(i){        
            total2[i]+=parseInt( $(this).html());
        });
    });
    $("#peserta_pme td.evaluasi").each(function(i){  
        if (total2[i] < 15) {
            console.log( "no ready!" );
            $(".evaluasi").html("Tidak Baik");
        }else{
            $(".evaluasi").html("Baik");
            console.log( "Baik" );
        }
    });

});

$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
</script>
@endsection
