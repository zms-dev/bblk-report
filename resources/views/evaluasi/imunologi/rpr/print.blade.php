<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;   
    text-align: center;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
</style>
<table width="100%" cellpadding="0" border="0">
    <thead>
        <tr>
            <th>
                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
            </th>
            <th width="100%">
                <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                <span style="font-size: 12px">PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL SIFILIS SIKLUS {{$type}} TAHUN {{$tahun}}</span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -25px; margin-left: -42px;"> 
                    Penyelenggara : Balai Besar Laboratorium Kesehatan Jakarta
                                                Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560
                                                Telp. (021) 4212524, 42804339 Facsimile (021) 4245516
                                                Website : bblkjakarta.com ; Surat Elektronik : bblkjakarta@yahoo.co.id
                </pre>
            </th>
        </tr>
        <tr>
            <th colspan="2"><hr></th>
        </tr>
    </thead>
</table>

<center><label><b>LAMPIRAN EVALUASI PESERTA<br>
PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL SIFILIS <br> RPR SIKLUS {{$type}} TAHUN {{$tahun}}</b>
<input type="hidden" name="type" value="{{$type}}"></label></center><br>
<form class="form-horizontal" method="post" enctype="multipart/form-data">
<b>Nama Instansi :</b> &nbsp;{{$perusahaan}}<br>
<b>Kode Peserta :</b> &nbsp;{{$kodeperusahaan}}
<br><br>
<table class="table table-bordered" id="peserta_pme">
    <tr class="titlerowna">
        <th>Kode Bahan Uji</th>
        <th>Nama Reagen</th>
        <th>Hasil Pemeriksaan</th>
        <th>Titer</th>
        <th>Hasil Rujukan</th>
        <th>Rentang Titer</th>
        <th>Kesesuaian Hasil</th>
        <th>Nilai Peserta</th>
    </tr>
    <?php  
        $no = 0;
        $val = ['I','II','III','IV','V'];
        $tabung = '';
    ?>
    @if(count($data2))
        @foreach($data2 as $hasil)
        <tr>
            <td align="middle">{{$hasil->kode_bahan_kontrol}}</td>
            @if($hasil->interpretasi == 'Tanpa test')
                @if($no != 0)
                @else
                    <td rowspan="5"></td>
                @endif
            @else
                @if($no != 0)
                @else
                    @if(!empty($reagen[0]))
                        <td rowspan="5">
                            @if($reagen[0]->reagen == 'Lain - lain')
                            {{$reagen[0]->reagen_lain}}
                            @else
                            {{$reagen[0]->reagen}}
                            @endif
                        </td>
                    @else
                        <td rowspan="5"></td>
                    @endif
                @endif
            @endif
            <td>{{$hasil->interpretasi}}</td>
            {{--  --}}
            <td>
                {{$hasil->titer}}&nbsp;
                @if(count($hasil->titer))
                    @if(explode(":", $hasil->titer) >= explode(":", $hasil->range1) && explode(":", $hasil->titer) <= explode(":", $hasil->range2))
                    <?php $titer = 'Benar';
                    ?>
                    <!-- ({{ $titer }}) -->
                    @else
                    <?php $titer = 'Salah';
                    ?>
                    <!-- ({{ $titer }}) -->
                    @endif
                @else
                    <?php 
                        $titer = '';
                    ?>

                @endif
            </td>
            <td>{{$hasil->nilai_rujukan}}</td>
            <td>{{$hasil->range1}} - {{$hasil->range2}}</td>
            <td>
                @if($hasil->interpretasi == $hasil->nilai_rujukan)
                    <?php
                        $kesesuaian_hasil = 'Benar';
                    ?>
                    @elseif($hasil->interpretasi == 'tanpa-test')
                    @else
                    <?php
                        $kesesuaian_hasil = 'Salah';
                    ?>
                    @endif
                <?php
                    if($hasil->interpretasi == 'Non Reaktif'){
                        if ($kesesuaian_hasil == 'Benar') {
                            echo "Benar";
                        }else{
                            echo "Salah";                                    
                        }
                    }else if ($hasil->interpretasi == 'Tanpa test') {
                        echo "-";
                    }else{
                        if ($titer == '') {
                            echo "-";
                        }else if (($kesesuaian_hasil == 'Benar') && ($titer == 'Benar')) {
                            echo "Benar";
                        }else if (($kesesuaian_hasil == 'Salah') && ($titer == 'Salah')) {
                            echo "Salah";
                        }else{
                            echo "Salah";                                    
                        }
                    }
                ?>
            </td>

            @if($no != 0)
            @else
            <td rowspan="5">{{$kesimpulan->ketepatan}}</td>
            @endif
        <?php $no++; $tabung = $hasil->tabung ?>
        </tr>
        @endforeach
    @else
    @foreach($rujukan as $val)
    <tr>
        <td>{{$val->kode_bahan_uji}}</td>
        <td></td>
        <td>TIDAK DIKERJAKAN</td>
        <td></td>
        <td>{{$val->nilai_rujukan}}</td>
        <td>{{$val->range1}} - {{$val->range2}}</td>
        <td></td>
        @if($no != 0)
        @else
        <td rowspan="5">{{$kesimpulan->ketepatan}}</td>
        @endif
    </tr><?php $no++; ?>
    @endforeach
    <input type="hidden" name="id_master_imunologi" value="">
    @endif
</table><br>
<p>
    {!!$catatan->catatan!!}
</p>
<p>
<div style="position: relative; top: 22px">
Jakarta, 31 Juli 2018<br>
Manajer Teknis
</div>
<p>
    <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'ttd_eki.png')}}" width="80" height="80" style="margin-left: 20px !important;">
</p>

<div style="position: relative; top:-20px">
dr. Eky Setiyo Kurniawan<br>
NIP 198106142010121001
</div>
</p>


<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>