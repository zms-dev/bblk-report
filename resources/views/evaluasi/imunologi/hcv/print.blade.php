<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;   
    text-align: center;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
#footer { 
    position: fixed; 
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<div id="footer">
    EVALUASI PNPME IMUNOLOGI ANTI HCV SIKLUS @if($type == '1') I @else II @endif TAHUN {{$tahun}}
</div>
<table width="100%" cellpadding="0" border="0" style="margin-top: -30px">
    <thead>
        <tr>
            <th>
                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
            </th>
            <th width="100%" style="text-align: center;">
                <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                <span style="font-size: 12px">PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br>ANTI HCV TAHUN {{$tahun}}</span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -25px; margin-left: -42px;"> 
                    Penyelenggara : Balai Besar Laboratorium Kesehatan Jakarta
                    Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560
                    Telp. (021) 4212524, 42804339 Facsimile (021) 4245516
                    Email : bblkjakarta@yahoo.co.id
                </pre>
            </th>
            <th>
                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" height="75px">
            </th>
        </tr>
        <tr>
            <th colspan="3"><hr></th>
        </tr>
    </thead>
</table>

<center><label><b>HASIL EVALUASI PESERTA<br>
PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL ANTI HCV <br>SIKLUS @if($type == '1') I @else II @endif TAHUN {{$tahun}}</b>
<input type="hidden" name="type" value="{{$type}}"></label></center><br>
<form class="form-horizontal" method="post" enctype="multipart/form-data">
<b>Nama Instansi :</b> &nbsp;{{$perusahaan}}<br>
<b>Kode Peserta :</b> &nbsp;{{$kodeperusahaan}}
<br><br>
<b>Nama Reagen :</b>
<table>
    <?php $no = 0; $i = 1;  ?>
    @foreach($data2 as $hasil)
    @if($hasil->tabung == '1')
        @if($hasil->interpretasi == 'tanpa-test')
        @else
            @if(!empty($reagen[$no]))
                @if($no != 0)
                @else
                    @foreach($reagen as $reg)
                    @if($reg->reagen != null)
                        <tr>
                            <td width="5px">{{$i}}. </td>
                            <td>
                                @if($reg->reagen == 'Lain - lain')
                                    {{$reg->reagen_lain}}
                                @else
                                    {{$reg->reagen}}
                                @endif
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>No. Lot/Batch : {{$reg->nomor_lot}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Tanggal Kedaluwarsa : {{$reg->tgl_kadaluarsa}}</td>
                        </tr>
                    @else
                    @endif
                    <?php $i++; ?>
                    @endforeach
                @endif
            @else
            @endif
        @endif
    @endif
    <?php $no++;?>
    @endforeach
</table>
<br>
<table class="table table-bordered" id="peserta_pme">
    <tr class="titlerowna">
        <!-- <th>Nama Reagen</th> -->
        <th>Kode Bahan Uji</th>
        <th>Hasil Pemeriksaan</th>
        <th>Hasil Rujukan</th>
        <th>Ketepatan Hasil</th>
        <!-- <th>Kesesuaian Strategi</th> -->
        <!-- <th>Nilai Peserta</th> -->
    </tr>
    <?php  
        $no = 0;
        $val = ['I','II','III','IV','V'];
        $tabung = '';
    ?>
    @if($data2 != NULL)
    @foreach($data2 as $hasil)
    @if($hasil->tabung == '1')
    <tr>
        <td align="middle">
            <?php if(strlen($hasil->kode_bahan_kontrol) > 3){ ?>
                {{$hasil->kode_bahan_kontrol}}
            <?php }else{?>
                IV-{{$no+1}}-{{$hasil->kode_bahan_kontrol}}
            <?php }?>
        </td>
        <td>{{$hasil->interpretasi}}</td>
        <td style="text-align: center;">
            @if(isset($rujukan[$no]))
            {{$rujukan[$no]->nilai_rujukan}}
            @endif
        </td>
        <td style="text-align: center">
            @if(isset($rujukan[$no]))
                @if($rujukan[$no]->nilai_rujukan == $hasil->interpretasi)
                    Benar
                @else
                    Salah
                @endif
            @endif
        </td>
    <?php $no++; $tabung = $hasil->tabung ?>
    </tr>
    @endif
    @endforeach
    @else
        @foreach($rujukan as $val)
        <tr>
            <td></td>
            <td>{{$val->kode_bahan_uji}}</td>
            <td>TIDAK DIKERJAKAN</td>
            <td>{{$val->nilai_rujukan}}</td>
            <td></td>
            @if($no != 0)
            @else
            <td rowspan="3">{{$kesimpulan->ketepatan}}</td>
            @endif
        </tr>
        <?php $no++;?>
        @endforeach
    @endif
</table>
<br>
<h4>Hasil Pemeriksaan :</h4>
<p><b>{{$kesimpulan->ketepatan}}</b></p>
@if($evaluasi != NULL)
@if($evaluasi->nilai_8 != 'ada' && $evaluasi->nilai_9 != 'ada')
@else
<h4>Komentar :</h4>
@endif
<ul>
    @if($evaluasi->nilai_8 == 'ada')
        <li>
            Hasil pemeriksaan tidak dilakukan evaluasi karena tidak mencantumkan tanggal kedaluwarsa reagen.
        </li>
    @endif
    @if($evaluasi->nilai_9 == 'ada')
        <li>
            Hasil pemeriksaan tidak dilakukan evaluasi karena reagen yang digunakan telah melewati tanggal kedaluwarsa.
        </li>
    @endif
</ul>
@else
@endif
@if($catatan->catatan != '')
<h4>Saran : </h4>
<p>
    <span style="font-family: DejaVu Sans; sans-serif;">{!!$catatan->catatan!!}</span>
</p>
@endif

<div style="position: relative; left: 75%;">
<p>
    <div style="position: relative; top: 0px;">
    Jakarta, @if($tanggalevaluasi != NULL){{ _dateIndo($tanggalevaluasi->tanggal) }} @endif<br>
    Penanggung jawab<br>
    Laboratorium Imunologi
    </div>
    <p>
        <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'imunologi_ttd.png')}}" width="180" style="margin: -75px 0px -50px 0px !important;">
    </p>

    <div style="position: relative; top:-20px">
    dr. Siti Kurnia Eka Rusmiarti, Sp.PK
    </div>
</p>
</div>
<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>