<html>
	<head>
		<style type="text/css">
		html{
			width: 1200px;
			height: 830px;
			/*padding-top: 30px;*/
			/*padding-bottom: 30px;*/
		}
		body{
			padding: 0;
			margin: 0;
			padding-bottom: 25px;
		}
		.container{
			background: url("{{url('asset/img/serti.png')}}");
			position:relative;
			background-repeat: no-repeat;;
			background-size: cover;
			background-position: center;
			margin: 0;
			height: 102%;
			width: 110%;
		}
		.header{
		position: fixed;
		}
		.kode{
		 	font-family: times new roman;
		 	color: #050708;
		 	font-size: 24px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 275px;
		 }


		 .peserta{
		 	font-family: Arial;
		 	width: 70%;
		 	font-size: 25px;
		 	font-weight : 900;
		 	position: absolute;
		 	text-transform: uppercase;
		 	left: 180;
		 	right: 0;
			text-align: center;
			margin-top: 375px;
		 }

		 .kepada{
		 	font-family: times new roman;
		 	width: 70%;
		 	font-size: 32px;
		 	font-weight : 900;
		 	position: absolute;
		 	left: 180;
		 	right: 0;
			text-align: center;
			margin-top: 330px;
		 }

		 .siklus{
		 	font-family: times new roman;
		 	font-size: 22px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 450px;
		 }
		 .bawah{
		 	font-family: Arial;
		 	font-size: 17px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 675px;
		 }
		 .tanggal{
		 	font-family: times new roman;
		 	font-size: 15px;
		 	position: absolute;
		 	left: 0;
		 	right: 120;
			text-align: right;
			margin-top: 620px;
		 }

		 .jenis{
		 	font-family: times new roman;
		 	font-size: 22px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 580px;
		 }
		 .jenis div{
			 width:40%;
			 display:inline-block;
			 margin:auto;
		 }
		</style>
	</head>
	<body>
		<div class="container">
		@foreach($data as $data2)
		<?php 
			$kode = $data2->kode_lebpes;
			$kode1 = substr($kode,5,-6);
		?>
		<div style="" class="kode"><b>No. {{ $sertifikat->nomor_sertifikat }}</b></div>
		<div style="" class="kepada"><b>Diberikan Kepada</b></div>
		<div style="" class="peserta"><b>{{ $data2->nama_lab}}</b></div>
		<div style="" class="siklus">
			<b>
			SEBAGAI PESERTA<br>
			Pemantapan Mutu Eksternal (PME) Tingkat Nasional<br>
			(KEPMENKES RI No. HK.02.02/MENKES/400/2016)<br>
			SIKLUS {{ $type }} TAHUN {{$tahun}}
			</b>
		</div>
		<div style="" class="jenis">
		 	<div><b>Bidang Pengujian Urinasisa<br>{{ $data2->jenis_form}}</b></div>
		</div>
		<div style="" class="tanggal"><b>Jakarta, @if($sertifikat->tanggal_sertifikat>0){{ _dateIndo($sertifikat->tanggal_sertifikat)}} @endif</b></div>
		@endforeach
		</div>
	</body>
</html>
