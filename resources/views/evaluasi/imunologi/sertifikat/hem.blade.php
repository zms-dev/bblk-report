<html>
	<head>
		<style type="text/css">
		html{
			width: 1396px;
			height: 830px;
			margin-left: 4px;
		}
		body{
			padding: 0;
			margin: 0;
		}
		.container{
			background: url("{{url('asset/img/serti2019.png')}}");
			position:relative;
			background-repeat: no-repeat;;
			background-size: cover;
			background-position: center;
			margin: 0;
			height: 100%;
			width: 100%;
		}
		.header{
			position: fixed;
		}
		.kode{
		 	font-family: Arial;
		 	font-size: 22px;
		 	font-weight : 900;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 330px;
		 }


		 .peserta{
		 	font-family: Arial;
		 	font-size: 34px;
		 	font-weight : 900;
		 	position: absolute;
		 	text-transform: uppercase;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 420px;
    		padding: 0px 15%;
		 }

		 .kepada{
		 	font-family: Arial;
		 	font-size: 22px;
		 	font-weight : 900;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 360px;
		 }

		 .siklus{
		 	font-family: times new roman;
		 	font-size: 22px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 520px;
			letter-spacing: 3;
		 }
		 .bawah{
		 	font-family: Arial;
		 	font-size: 17px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 675px;
		 }
		 .tanggal{
		 	font-family: Arial;
		 	font-size: 22px;
		 	position: absolute;
		 	left: 0;
		 	right: 120;
			text-align: right;
			margin-top: 730px;
			letter-spacing: 3;
		 }

		 .jenis{
		 	font-family: times new roman;
		 	font-size: 22px;
		 	position: absolute;
		 	left: 0;
		 	right: 0;
			text-align: center;
			margin-top: 650px;
			letter-spacing: 3;
		 }
		 .jenis div{
			 width:40%;
			 display:inline-block;
			 margin:auto;
		 }
		</style>
	</head>
	<body>
		<div class="container">
		@foreach($data as $data2)
		<?php 
			$kode = $data2->kode_lebpes;
			$kode1 = substr($kode,5,-6);
		?>
		<div style="" class="kode"><b>No. {{ $sertifikat->nomor_sertifikat }}</b></div>
		<div style="" class="kepada"><b>DIBERIKAN KEPADA</b></div>
		<div style="" class="peserta"><b>{{ $data2->nama_lab}}</b></div>
		<div style="" class="siklus">
			SEBAGAI PESERTA<br>
			Pemantapan Mutu Eksternal (PME) Tingkat Nasional<br>
			(KEPMENKES RI No. HK.02.02/MENKES/400/2016)<br>
			SIKLUS {{ $type }} TAHUN {{$tahun}}
		</div>
		<div style="" class="jenis">
		 	<div>Bidang Pengujian {{ $data2->jenis_form}}</div>
		</div>
		<div style="" class="tanggal">Jakarta, @if($sertifikat->tanggal_sertifikat > 0 ){{ _dateIndo($sertifikat->tanggal_sertifikat)}} @endif</div>
		@endforeach
		</div>
	</body>
</html>
