@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Penilaian dan Lampiran Evaluasi</div>
                <div class="panel-body">
                    <center><label>EVALUASI PESERTA PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI TAHUN {{$date}} <br> HASIL PEMERIKSAAN ANTI HIV <input type="hidden" name="type" value="{{$type}}"></label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta </label>
                        <div class="col-sm-10">
                            <input readonly="" type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $kodeperusahaan }}" placeholder="Kode Peserta">
                        </div>
                    </div><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama Instansi </label>
                        <div class="col-sm-10">
                            <input readonly="" type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $perusahaan }}" placeholder="Kode Peserta">
                        </div>
                    </div>


                    <label>2. HASIL PEMERIKSAAN</label>
                    @if($evaluasi != NULL)
                        <form id="someForm" action="" method="post">
                    @else
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                    @endif
                    @if($strategi != NULL)
                        @foreach($strategi as $str)
                            <input type="hidden" name="idevaluasi[]" value="{{$str->id}}">
                        @endforeach
                    @endif
                    <table class="table table-bordered" id="peserta_pme">
                        <tr class="titlerowna">
                            <th rowspan="2" style="vertical-align: middle;"><center>Uji</center></th>
                            <th rowspan="2" style="vertical-align: middle;" width="10%"><center>Kode Bahan Uji</center></th>
                            <th rowspan="2" style="vertical-align: middle;"><center>Metode Pemeriksaan</center></th>
                            <th rowspan="2" style="vertical-align: middle;"><center>Nama Reagen</center></th>
                            <th rowspan="2" style="vertical-align: middle;"><center>Tanggal Kedaluwarsa</center></th>
                            <th rowspan="2" style="vertical-align: middle;"><center>Hasil Pemeriksaan</center></th>
                            <th rowspan="2" style="vertical-align: middle;"><center>Hasil Rujukan</center></th>
                            <!-- <td>Kesesuaian Strategi</td> -->
                            <th colspan="2"><center>Ketepatan Hasil</center></th>
                            <th colspan="2" style="vertical-align: middle;"><center>Kesesuaian Strategi</center></th>
                        </tr>
                        <tr>
                            <th><center>Nilai</center></th>
                            <th><center>Kategori</center></th>
                            <th width="7%"><center>Nilai</center></th>
                            <th><center>Kategori</center></th>
                        </tr>
                        <?php
                            $no = 0;
                            $val = ['1','2','3','4'];
                            $tabung = '';
                        ?>
                        @foreach($data2 as $hasil)
                        @if($hasil->tabung == '1')
                            <input type="hidden" name="id_master_imunologi" value="{{$hasil->id_master_imunologi}}">
                        <tr>
                            <td><center>{{$val[$no]}}</center></td>
                            <td>
                                @if($type == 1 && $tahun  == 2018)
                                <input readonly="" type="text" name="kode_bahan_kontrol[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}">
                                @else
                                <input readonly="" type="text" name="kode_bahan_kontrol[]" class="form-control" required value="I-1-{{$hasil->kode_bahan_kontrol}}">
                                @endif
                            </td>
                            @if($hasil->interpretasi == 'Tanpa test')
                            <td></td>
                            <td></td>
                            <td></td>
                            @else
                                @if(!empty($reagen[$no]))
                                <td style="text-transform: uppercase;"><center>{{$reagen[$no]->metode}}</center></td>
                                <td>
                                    {{-- {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain.$reagen[$no]->reagen)!!} --}}
                                    @if($reagen[$no]->id == '121')
                                      {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain)!!}
                                    @else
                                      {{$reagen[$no]->reagen}}
                                    @endif<br>
                                    Sensitivitas : {{$reagen[$no]->sensitivitas}}
                                    @if($reagen[$no]->reagen == 'Lain - lain')
                                        <input type="hidden" name="idreagen[]" value="{{$reagen[$no]->idreagen}}">
                                        @if($reagen[$no]->sensi != NULL)
                                        <input type="text" name="sensitivitas[]" value="{{$reagen[$no]->sensi}}">
                                        @else
                                        <input type="text" name="sensitivitas[]" value="">
                                        @endif
                                    @endif
                                    , Spesifisitas : {{$reagen[$no]->spesifisitas}}
                                    @if($reagen[$no]->reagen == 'Lain - lain')
                                        @if($reagen[$no]->spesi != NULL)
                                        <input type="text" name="spesifisitas[]" value="{{$reagen[$no]->spesi}}">
                                        @else
                                        <input type="text" name="spesifisitas[]" value="">
                                        @endif
                                    @endif
                                </td>
                                <td>{{$reagen[$no]->tgl_kadaluarsa}}</td>
                                @else
                                <td></td>
                                <td></td>
                                <td></td>
                                @endif
                            @endif
                            <td><center>@if($hasil->interpretasi == 'Tanpa test') Tanpa Uji @else {{$hasil->interpretasi}} @endif</center></td>
                            @if($tabung == $hasil->tabung)
                            @else
                                @foreach($rujukan as $rujuk)
                                    @if($rujuk->kode_bahan_uji == $hasil->tabung)
                                    <td rowspan="3" style="text-align: center;">{{$rujuk->nilai_rujukan}}</td>
                                    @endif
                                @endforeach
                            @endif
                            <td>
                                <?php
                                    if($evaluasi != NULL){
                                        if ($evaluasi->nilai_11 == 'ada' && $val[$no] == 3) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_10 == 'ada' && $val[$no] == 2) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_9 == 'ada' && $val[$no] == 1) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_8 == 'ada' && $val[$no] == 1) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_13 == 'ada' && $val[$no] == 2) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_14 == 'ada' && $val[$no] == 3) {
                                            echo "0";
                                        }else{
                                            foreach($rujukan as $rujuk){
                                                if($rujuk->kode_bahan_uji == $hasil->tabung){
                                                    if($rujuk->nilai_rujukan == $hasil->interpretasi){
                                                        echo "4";
                                                    }else{
                                                        echo "0";
                                                    }
                                                }
                                            }
                                        }
                                    }else{
                                        foreach($rujukan as $rujuk){
                                            if($rujuk->kode_bahan_uji == $hasil->tabung){
                                                if($rujuk->nilai_rujukan == $hasil->interpretasi){
                                                    echo "4";
                                                }else{
                                                    echo "0";
                                                }
                                            }
                                        }
                                    }
                                ?>
                            </td>
                            <td style="text-align: center">
                                <?php
                                    if($evaluasi != NULL){
                                        if ($evaluasi->nilai_11 == 'ada' && $val[$no] == 3) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_10 == 'ada' && $val[$no] == 2) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_9 == 'ada' && $val[$no] == 1) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_8 == 'ada' && $val[$no] == 1) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_13 == 'ada' && $val[$no] == 2) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_14 == 'ada' && $val[$no] == 3) {
                                            echo "Tidak dinilai";
                                        }else{
                                            foreach($rujukan as $rujuk){
                                                if($rujuk->kode_bahan_uji == $hasil->tabung){
                                                    if($rujuk->nilai_rujukan == $hasil->interpretasi){
                                                        echo "Benar";
                                                    }else{
                                                        echo "Salah";
                                                    }
                                                }
                                            }
                                        }
                                    }else{
                                        foreach($rujukan as $rujuk){
                                            if($rujuk->kode_bahan_uji == $hasil->tabung){
                                                if($rujuk->nilai_rujukan == $hasil->interpretasi){
                                                    echo "Benar";
                                                }else{
                                                    echo "Salah";
                                                }
                                            }
                                        }
                                    }
                                ?>
                            </td>
                            @if($tabung == $hasil->tabung)
                            @else
                                <input type="hidden" name="id_master_imunologi" value="{{$hasil->id_master_imunologi}}">
                                <td rowspan="3">
                                    <select name="strategi[]" class="form-control" id="strategi1" required>
                                            @if($strategi != NULL)
                                                @foreach($strategi as $str)
                                                    @if($str->tabung == 1)
                                                        <option value="{{$str->nilai}}">{{$str->nilai}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        <option value=""></option>
                                        <option value="0">0</option>
                                        <option value="5">5</option>
                                    </select>
                                </td>
                                <td id="kategori1" rowspan="3">
                                    @if($strategi != NULL)
                                        @foreach($strategi as $str)
                                            @if($str->tabung == 1)
                                                {{$str->kategori}}
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                            @endif
                        <?php $no++; $tabung = $hasil->tabung ?>
                        </tr>
                        @endif
                        @endforeach
                        <!-- <tr>
                            <td colspan="5"></td>
                            <td>Evaluasi</td>
                            <td style="font-weight: bold;" class="evaluasi1"></td>
                        </tr> -->
                        <?php
                            $no = 0;
                            $val = ['1','2','3','4'];
                            $tabung = '';
                        ?>
                        @foreach($data2 as $hasil)
                        @if($hasil->tabung == '2')
                        <tr>
                            <td><center>{{$val[$no]}}</center></td>
                            <td>
                                @if($type == 1 && $tahun  == 2018)
                                <input readonly="" type="text" name="kode_bahan_kontrol[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}">
                                @else
                                <input readonly="" type="text" name="kode_bahan_kontrol[]" class="form-control" required value="I-2-{{$hasil->kode_bahan_kontrol}}">
                                @endif
                            </td>
                            @if($hasil->interpretasi == 'Tanpa test')
                            <td></td>
                            <td></td>
                            <td></td>
                                @else
                                @if(!empty($reagen[$no]))
                                <td style="text-transform: uppercase;"><center>{{$reagen[$no]->metode}}</center></td>
                                <td>
                                    {{-- {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain.$reagen[$no]->reagen)!!} --}}
                                    @if($reagen[$no]->id == '121')
                                      {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain)!!}
                                    @else
                                      {{$reagen[$no]->reagen}}
                                    @endif<br>
                                    Sensitivitas : {{$reagen[$no]->sensitivitas}}
                                    @if($reagen[$no]->reagen == 'Lain - lain')
                                        @if($reagen[$no]->sensi != NULL)
                                            {{$reagen[$no]->sensi}}
                                        @else
                                        @endif
                                    @endif
                                    , Spesifisitas : {{$reagen[$no]->spesifisitas}}
                                    @if($reagen[$no]->reagen == 'Lain - lain')
                                        @if($reagen[$no]->spesi != NULL)
                                            {{$reagen[$no]->spesi}}
                                        @else
                                        @endif
                                    @endif
                                </td>
                                <td>{{$reagen[$no]->tgl_kadaluarsa}}</td>
                                @else
                                <td></td>
                                <td></td>
                                <td></td>
                                @endif
                            @endif
                            <td><center>@if($hasil->interpretasi == 'Tanpa test') Tanpa Uji @else {{$hasil->interpretasi}} @endif</center></td>
                            @if($tabung == $hasil->tabung)
                            @else
                                @foreach($rujukan as $rujuk)
                                    @if($rujuk->kode_bahan_uji == $hasil->tabung)
                                    <td rowspan="3" style="text-align: center;">{{$rujuk->nilai_rujukan}}</td>
                                    @endif
                                @endforeach
                            @endif
                            <td>
                                <?php
                                    if($evaluasi != NULL){
                                        if ($evaluasi->nilai_11 == 'ada' && $val[$no] == 3) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_10 == 'ada' && $val[$no] == 2) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_9 == 'ada' && $val[$no] == 1) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_8 == 'ada' && $val[$no] == 1) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_13 == 'ada' && $val[$no] == 2) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_14 == 'ada' && $val[$no] == 3) {
                                            echo "0";
                                        }else{
                                            foreach($rujukan as $rujuk){
                                                if($rujuk->kode_bahan_uji == $hasil->tabung){
                                                    if($rujuk->nilai_rujukan == $hasil->interpretasi){
                                                        echo "4";
                                                    }else{
                                                        echo "0";
                                                    }
                                                }
                                            }
                                        }
                                    }else{
                                        foreach($rujukan as $rujuk){
                                            if($rujuk->kode_bahan_uji == $hasil->tabung){
                                                if($rujuk->nilai_rujukan == $hasil->interpretasi){
                                                    echo "4";
                                                }else{
                                                    echo "0";
                                                }
                                            }
                                        }
                                    }
                                ?>
                            </td>
                            <td style="text-align: center">
                                <?php
                                    if($evaluasi != NULL){
                                        if ($evaluasi->nilai_11 == 'ada' && $val[$no] == 3) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_10 == 'ada' && $val[$no] == 2) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_9 == 'ada' && $val[$no] == 1) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_8 == 'ada' && $val[$no] == 1) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_13 == 'ada' && $val[$no] == 2) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_14 == 'ada' && $val[$no] == 3) {
                                            echo "Tidak dinilai";
                                        }else{
                                            foreach($rujukan as $rujuk){
                                                if($rujuk->kode_bahan_uji == $hasil->tabung){
                                                    if($rujuk->nilai_rujukan == $hasil->interpretasi){
                                                        echo "Benar";
                                                    }else{
                                                        echo "Salah";
                                                    }
                                                }
                                            }
                                        }
                                    }else{
                                        foreach($rujukan as $rujuk){
                                            if($rujuk->kode_bahan_uji == $hasil->tabung){
                                                if($rujuk->nilai_rujukan == $hasil->interpretasi){
                                                    echo "Benar";
                                                }else{
                                                    echo "Salah";
                                                }
                                            }
                                        }
                                    }
                                ?>
                            </td>
                            @if($tabung == $hasil->tabung)
                            @else
                                <input type="hidden" name="id_master_imunologi" value="{{$hasil->id_master_imunologi}}">
                                <td rowspan="3">
                                    <select name="strategi[]" class="form-control" id="strategi2" required>
                                            @if($strategi != NULL)
                                                @foreach($strategi as $str)
                                                    @if($str->tabung == 2)
                                                        <option value="{{$str->nilai}}">{{$str->nilai}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        <option value=""></option>
                                        <option value="0">0</option>
                                        <option value="5">5</option>
                                    </select>
                                </td>
                                <td id="kategori2" rowspan="3">
                                    @if($strategi != NULL)
                                        @foreach($strategi as $str)
                                            @if($str->tabung == 2)
                                                {{$str->kategori}}
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                            @endif
                        </tr>
                        <?php $no++; $tabung = $hasil->tabung?>
                        @endif
                        @endforeach
                        <!-- <tr>
                            <td colspan="5"></td>
                            <td>Evaluasi</td>
                            <td style="font-weight: bold;" class="evaluasi2"></td>
                        </tr> -->
                        <?php
                            $no = 0;
                            $val = ['1','2','3','4'];
                            $tabung = '';
                        ?>
                        @foreach($data2 as $hasil)
                        @if($hasil->tabung == '3')
                        <tr>
                            <td><center>{{$val[$no]}}</center></td>
                            <td>
                                @if($type == 1 && $tahun  == 2018)
                                <input readonly="" type="text" name="kode_bahan_kontrol[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}">
                                @else
                                <input readonly="" type="text" name="kode_bahan_kontrol[]" class="form-control" required value="I-3-{{$hasil->kode_bahan_kontrol}}">
                                @endif
                            </td>
                            @if($hasil->interpretasi == 'Tanpa test')
                            <td></td>
                            <td></td>
                            <td></td>
                            @else
                                @if(!empty($reagen[$no]))
                                <td style="text-transform: uppercase;"><center>{{$reagen[$no]->metode}}</center></td>
                                <td>
                                    {{-- {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain.$reagen[$no]->reagen)!!} --}}
                                    @if($reagen[$no]->id == '121')
                                      {!! str_replace('Lain - lain', '', $reagen[$no]->reagen_lain)!!}
                                    @else
                                      {{$reagen[$no]->reagen}}
                                    @endif<br>
                                    Sensitivitas : {{$reagen[$no]->sensitivitas}}
                                    @if($reagen[$no]->reagen == 'Lain - lain')
                                        @if($reagen[$no]->sensi != NULL)
                                            {{$reagen[$no]->sensi}}
                                        @else
                                        @endif
                                    @endif
                                    , Spesifisitas : {{$reagen[$no]->spesifisitas}}
                                    @if($reagen[$no]->reagen == 'Lain - lain')
                                        @if($reagen[$no]->spesi != NULL)
                                            {{$reagen[$no]->spesi}}
                                        @else
                                        @endif
                                    @endif
                                </td>
                                <td>{{$reagen[$no]->tgl_kadaluarsa}}</td>
                                @else
                                <td></td>
                                <td></td>
                                <td></td>
                                @endif
                            @endif
                            <td><center>@if($hasil->interpretasi == 'Tanpa test') Tanpa Uji @else {{$hasil->interpretasi}} @endif</center></td>
                            @if($tabung == $hasil->tabung)
                            @else
                                @foreach($rujukan as $rujuk)
                                    @if($rujuk->kode_bahan_uji == $hasil->tabung)
                                    <td rowspan="3" style="text-align: center;">{{$rujuk->nilai_rujukan}}</td>
                                    @endif
                                @endforeach
                            @endif
                            <td>
                                <?php
                                    if($evaluasi != NULL){
                                        if ($evaluasi->nilai_11 == 'ada' && $val[$no] == 3) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_10 == 'ada' && $val[$no] == 2) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_9 == 'ada' && $val[$no] == 1) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_8 == 'ada' && $val[$no] == 1) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_13 == 'ada' && $val[$no] == 2) {
                                            echo "0";
                                        }elseif ($evaluasi->nilai_14 == 'ada' && $val[$no] == 3) {
                                            echo "0";
                                        }else{
                                            foreach($rujukan as $rujuk){
                                                if($rujuk->kode_bahan_uji == $hasil->tabung){
                                                    if($rujuk->nilai_rujukan == $hasil->interpretasi){
                                                        echo "4";
                                                    }else{
                                                        echo "0";
                                                    }
                                                }
                                            }
                                        }
                                    }else{
                                        foreach($rujukan as $rujuk){
                                            if($rujuk->kode_bahan_uji == $hasil->tabung){
                                                if($rujuk->nilai_rujukan == $hasil->interpretasi){
                                                    echo "4";
                                                }else{
                                                    echo "0";
                                                }
                                            }
                                        }
                                    }
                                ?>
                            </td>
                            <td style="text-align: center">
                                <?php
                                    if($evaluasi != NULL){
                                        if ($evaluasi->nilai_11 == 'ada' && $val[$no] == 3) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_10 == 'ada' && $val[$no] == 2) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_9 == 'ada' && $val[$no] == 1) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_8 == 'ada' && $val[$no] == 1) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_13 == 'ada' && $val[$no] == 2) {
                                            echo "Tidak dinilai";
                                        }elseif ($evaluasi->nilai_14 == 'ada' && $val[$no] == 3) {
                                            echo "Tidak dinilai";
                                        }else{
                                            foreach($rujukan as $rujuk){
                                                if($rujuk->kode_bahan_uji == $hasil->tabung){
                                                    if($rujuk->nilai_rujukan == $hasil->interpretasi){
                                                        echo "Benar";
                                                    }else{
                                                        echo "Salah";
                                                    }
                                                }
                                            }
                                        }
                                    }else{
                                        foreach($rujukan as $rujuk){
                                            if($rujuk->kode_bahan_uji == $hasil->tabung){
                                                if($rujuk->nilai_rujukan == $hasil->interpretasi){
                                                    echo "Benar";
                                                }else{
                                                    echo "Salah";
                                                }
                                            }
                                        }
                                    }
                                ?>
                            </td>
                            @if($tabung == $hasil->tabung)
                            @else
                                <input type="hidden" name="id_master_imunologi" value="{{$hasil->id_master_imunologi}}">
                                <td rowspan="3">
                                    <select name="strategi[]" class="form-control" id="strategi3" required>
                                            @if($strategi != NULL)
                                                @foreach($strategi as $str)
                                                    @if($str->tabung == 3)
                                                        <option value="{{$str->nilai}}">{{$str->nilai}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        <option value=""></option>
                                        <option value="0">0</option>
                                        <option value="5">5</option>
                                    </select>
                                </td>
                                <td id="kategori3" rowspan="3">
                                    @if($strategi != NULL)
                                        @foreach($strategi as $str)
                                            @if($str->tabung == 3)
                                                {{$str->kategori}}
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                            @endif
                        </tr>
                        <?php $no++; $tabung = $hasil->tabung?>
                        @endif
                        @endforeach
                        <!-- <tr>
                            <td colspan="5"></td>
                            <td>Evaluasi</td>
                            <td style="font-weight: bold;" class="evaluasi3"></td>
                        </tr> -->
                    </table>
                    @if($evaluasi != NULL)
                    <p>
                        <input type="checkbox" name="nilai_1" value="ada" {{ ($evaluasi->nilai_1 == 'ada') ? 'checked' : '' }}> 
                        1. Alur pemeriksaan yang Saudara kerjakan sudah SESUAI dengan strategi III pemeriksaan Anti HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_2" value="ada" {{ ($evaluasi->nilai_2 == 'ada') ? 'checked' : '' }}> 
                        2. Alur pemeriksaan yang Saudara kerjakan TIDAK SESUAI dengan strategi III pemeriksaan Anti HIV. Gunakan alur pemeriksaan yang sesuai dengan strategi III pemeriksaan HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_3" value="ada" {{ ($evaluasi->nilai_3 == 'ada') ? 'checked' : '' }}>
                        3. Jenis reagen yang Saudara gunakan SESUAI dengan hasil evaluasi laboratorium rujukan nasional RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_4" value="ada" {{ ($evaluasi->nilai_4 == 'ada') ? 'checked' : '' }}> 
                        4. Jenis reagen yang Saudara gunakan TIDAK SESUAI dengan hasil evaluasi laboratorium rujukan RSUPN Cipto Mangunkusumo. Gunakan jenis reagen yang sudah dilakukan evaluasi oleh RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_5" value="ada" {{ ($evaluasi->nilai_5 == 'ada') ? 'checked' : '' }}> 
                        5. Urutan reagen yang Saudara gunakan SESUAI dengan strategi III pemeriksaan Anti HIV. Sensitivitas dan spesifisitas reagen berdasarkan hasil evaluasi reagen laboratorium rujukan RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_6" value="ada" {{ ($evaluasi->nilai_6 == 'ada') ? 'checked' : '' }}> 
                        6. Urutan reagen yang Saudara gunakan TIDAK SESUAI dengan strategi III pemeriksaan Anti HIV. Gunakan urutan reagen berdasarkan sensitivitas dan spesifisitas hasil evaluasi reagen laboratorium rujukan RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_7" value="ada" {{ ($evaluasi->nilai_7 == 'ada') ? 'checked' : '' }}> 
                        7. Jenis reagen yang saudara gunakan SESUAI dengan strategi 1 pemeriksaan Anti HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_12" value="ada" {{ ($evaluasi->nilai_12 == 'ada') ? 'checked' : '' }}> 
                        8. Jenis reagen yang saudara gunakan TIDAK SESUAI dengan strategi 1 pemeriksaan Anti HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_15" value="ada" {{ ($evaluasi->nilai_15 == 'ada') ? 'checked' : '' }}> 
                        9. Strategi 1 (Pemenkes RI no.15 Tahun 2015) Gunakan reagensia yang memiliki sensitivitas dan spesifisitas >= 99%
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_8" value="ada" {{ ($evaluasi->nilai_8 == 'ada') ? 'checked' : '' }}> 
                        10. Hasil pemeriksaan tidak dilakukan penilaian karena tidak mencantumkan tanggal kedaluwarsa reagen 1.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_13" value="ada" {{ ($evaluasi->nilai_13 == 'ada') ? 'checked' : '' }}>
                        11. Hasil pemeriksaan tidak dilakukan penilaian karena tidak mencantumkan tanggal kedaluwarsa reagen 2.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_14" value="ada" {{ ($evaluasi->nilai_14 == 'ada') ? 'checked' : '' }}>
                        12. Hasil pemeriksaan tidak dilakukan penilaian karena tidak mencantumkan tanggal kedaluwarsa reagen 3.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_9" value="ada" {{ ($evaluasi->nilai_9 == 'ada') ? 'checked' : '' }}> 
                        13. Hasil pemeriksaan tidak dilakukan penilaian karena reagen 1 yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_10" value="ada" {{ ($evaluasi->nilai_10 == 'ada') ? 'checked' : '' }}> 
                        14. Hasil pemeriksaan tidak dilakukan penilaian karena reagen 2 yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_11" value="ada" {{ ($evaluasi->nilai_11 == 'ada') ? 'checked' : '' }}>
                        15. Hasil pemeriksaan tidak dilakukan penilaian karena reagen 3 yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    @else
                    <p>
                        <input type="checkbox" name="nilai_1" value="ada"> 
                        1. Alur pemeriksaan yang Saudara kerjakan sudah SESUAI dengan strategi III pemeriksaan Anti HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_2" value="ada"> 
                        2. Alur pemeriksaan yang Saudara kerjakan TIDAK SESUAI dengan strategi III pemeriksaan Anti HIV. Gunakan alur pemeriksaan yang sesuai dengan strategi III pemeriksaan HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_3" value="ada"}>
                        3. Jenis reagen yang Saudara gunakan SESUAI dengan hasil evaluasi laboratorium rujukan nasional RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_4" value="ada"> 
                        4. Jenis reagen yang Saudara gunakan TIDAK SESUAI dengan hasil evaluasi laboratorium rujukan RSUPN Cipto Mangunkusumo. Gunakan jenis reagen yang sudah dilakukan evaluasi oleh RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_5" value="ada"> 
                        5. Urutan reagen yang Saudara gunakan SESUAI dengan strategi III pemeriksaan Anti HIV. Sensitivitas dan spesifisitas reagen berdasarkan hasil evaluasi reagen laboratorium rujukan RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_6" value="ada"> 
                        6. Urutan reagen yang Saudara gunakan TIDAK SESUAI dengan strategi III pemeriksaan Anti HIV. Gunakan urutan reagen berdasarkan sensitivitas dan spesifisitas hasil evaluasi reagen laboratorium rujukan RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_7" value="ada"> 
                        7. Jenis reagen yang saudara gunakan SESUAI dengan strategi 1 pemeriksaan Anti HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_12" value="ada"> 
                        8. Jenis reagen yang saudara gunakan TIDAK SESUAI dengan strategi 1 pemeriksaan Anti HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_15" value="ada"> 
                        9. Strategi 1 (Pemenkes RI no.15 Tahun 2015) Gunakan reagensia yang memiliki sensitivitas dan spesifisitas >= 99%
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_8" value="ada"> 
                        10. Hasil pemeriksaan tidak dilakukan penilaian karena tidak mencantumkan tanggal kedaluwarsa reagen 1.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_13" value="ada"> 
                        11. Hasil pemeriksaan tidak dilakukan penilaian karena tidak mencantumkan tanggal kedaluwarsa reagen 2.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_14" value="ada"> 
                        12. Hasil pemeriksaan tidak dilakukan penilaian karena tidak mencantumkan tanggal kedaluwarsa reagen 3.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_9" value="ada"> 
                        13. Hasil pemeriksaan tidak dilakukan penilaian karena reagen 1 yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_10" value="ada"> 
                        14. Hasil pemeriksaan tidak dilakukan penilaian karena reagen 2 yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_11" value="ada"> 
                        15. Hasil pemeriksaan tidak dilakukan penilaian karena reagen 3 yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    @endif

                    <h4>Penilaian Hasil Pemeriksaan Peserta :</h4>
                    <blockquote>
                        1. Ketepatan Hasil Pemeriksaan <select name="ketepatan" required>
                                                @if($kesimpulan != NULL)
                                                    @if($kesimpulan->ketepatan != NULL)
                                                        <option value="{{$kesimpulan->ketepatan}}">{{$kesimpulan->ketepatan}}</option>
                                                        <option value="Baik">Baik</option>
                                                        <option value="Kurang">Kurang</option>
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @else
                                                        <option></option>
                                                        <option value="Baik">Baik</option>
                                                        <option value="Kurang">Kurang</option>
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @endif
                                                @else
                                                    <option></option>
                                                    <option value="Baik">Baik</option>
                                                    <option value="Kurang">Kurang</option>
                                                    <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                @endif
                                            </select><br>
                        2. Kesesuaian Strategi  <select name="kesesuaian" required>
                                                    @if($kesimpulan != NULL)
                                                        @if($kesimpulan->ketepatan != NULL)
                                                        <option value="{{$kesimpulan->kesesuaian}}">{{$kesimpulan->kesesuaian}}</option>
                                                            <option value="Sesuai">Sesuai</option>
                                                            <option value="Tidak Sesuai">Tidak Sesuai</option>
                                                        @else
                                                            <option></option>
                                                            <option value="Sesuai">Sesuai</option>
                                                            <option value="Tidak Sesuai">Tidak Sesuai</option>
                                                        @endif
                                                    @else
                                                        <option></option>
                                                        <option value="Sesuai">Sesuai</option>
                                                        <option value="Tidak Sesuai">Tidak Sesuai</option>
                                                    @endif
                                                </select>
                    </blockquote>
                    <label>Saran :</label>
                    @if($catatan != NULL)
                    <textarea name="catatan" id="editor1" rows="10" cols="80">{!!$catatan->catatan!!}</textarea>
                    @else
                    <textarea name="catatan" id="editor1" rows="10" cols="80"></textarea>
                    @endif
                    <br>
                    @if($evaluasi != NULL)
                    <a href="{{url('hasil-pemeriksaan/anti-hiv/evaluasi/print').'/'.$register->id.'?y='.$type}}">
                        <input type="button" class="btn btn-primari" value="Print" name="print"/>
                    </a>
                    <input type="submit" class="btn btn-primari" value="Update" name="update" onclick="askForUpdate()" />
                    @else
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-primary">
                    @endif
                      {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
CKEDITOR.replace( 'editor1' );

form=document.getElementById("someForm");
function askForPrint() {
        form.action="{{url('hasil-pemeriksaan/anti-hiv/evaluasi/print').'/'.$register->id.'?y='.$type}}";
        form.submit();
}
function askForUpdate() {
        form.action="{{url('hasil-pemeriksaan/anti-hiv/evaluasi/update').'/'.$register->id.'?y='.$type}}";
        form.submit();
}

var total1=[0,0,0];
var total2=[0,0,0];
var total3=[0,0,0];
$(document).ready(function(){

    var $dataRows=$("#peserta_pme tr:not('.titlerowna')");

    $dataRows.each(function() {
        $(this).find('.colomngitung1').each(function(i){
            total1[i]+=parseInt( $(this).html());
            hasil1 = total1[i] / 3;
        });
        $(this).find('.colomngitung2').each(function(i){
            total2[i]+=parseInt( $(this).html());
            hasil2 = total2[i] / 3;
        });
        $(this).find('.colomngitung3').each(function(i){
            total3[i]+=parseInt( $(this).html());
            hasil3 = total3[i] / 3;
        });
    });

    $("#peserta_pme td.evaluasi1").each(function(i){
        if (hasil1 < 4) {
            console.log( "no ready!" );
            $(".evaluasi1").html("Tidak Baik");
        }else{
            $(".evaluasi1").html("Baik");
            console.log( "Baik" );
        }
    });
    $("#peserta_pme td.evaluasi2").each(function(i){
        if (hasil2 < 4) {
            console.log( "no ready!" );
            $(".evaluasi2").html("Tidak Baik");
        }else{
            $(".evaluasi2").html("Baik");
            console.log( "Baik" );
        }
    });
    $("#peserta_pme td.evaluasi3").each(function(i){
        if (hasil3 < 4) {
            console.log( "no ready!" );
            $(".evaluasi3").html("Tidak Baik");
        }else{
            $(".evaluasi3").html("Baik");
            console.log( "Baik" );
        }
    });

});

$('#strategi1').change(function() {
    if($(this).val() == '0'){
        $("#kategori1").text('Tidak Sesuai');
        console.log('Tidak Sesuai')
    }
    else{
        $("#kategori1").text('Sesuai');
        console.log('Sesuai')
    }
});
$('#strategi2').change(function() {
    if($(this).val() == '0'){
        $("#kategori2").text('Tidak Sesuai');
        console.log('Tidak Sesuai')
    }
    else{
        $("#kategori2").text('Sesuai');
        console.log('Sesuai')
    }
});
$('#strategi3').change(function() {
    if($(this).val() == '0'){
        $("#kategori3").text('Tidak Sesuai');
        console.log('Tidak Sesuai')
    }
    else{
        $("#kategori3").text('Sesuai');
        console.log('Sesuai')
    }
});

$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});

$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
</script>
@endsection
