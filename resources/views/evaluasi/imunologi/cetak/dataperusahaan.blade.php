@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cetak Lampiran Evaluasi Siklus 
                {{$siklus}} Tahun {{$tahun}}</div>

                <div class="panel-body">
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ Session::get('message') }}
                        </p>
                    @endif
                    <table class="table table-bordered">
                        <tr>
                            <th width="2%"><center>No</center></th>
                            <th><center>Bidang</center></th>
                            <th colspan="2" width="15%"><center>Aksi</center></th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->id_bidang > '5')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                @if($val->bidang == '7')
                                    @if($val->tanggalna == $tahun)
                                        @if($siklus == 1)
                                            <td>
                                            @if($val->siklus_1 == 'done')
                                                @if($val->pemeriksaan == 'done')
                                                    @if($val->status_data1 == 2)
                                                        <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=1"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                    @else
                                                        Belum Dikirim
                                                    @endif
                                                @endif                        
                                            @else
                                            Belum Dikirim
                                            @endif
                                            </td>
                                        @elseif($siklus == 2)                                    
                                            <td>
                                                @if($val->siklus_2 == 'done')
                                                    @if($val->pemeriksaan2 == 'done')
                                                        @if($val->status_data2 == 2)
                                                            <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=2"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                        @else
                                                            Belum Dikirim
                                                        @endif
                                                    @endif
                                                @else
                                            Belum Dikirim
                                            @endif
                                            </td>
                                        @else
                                            <td>Belum Dikirim</td>
                                        @endif
                                    @else
                                        <td>Tahun {{$tahun}}</td>
                                    @endif
                                @else
                                @if($val->tanggalna == $tahun)
                                    @if($siklus == 1)
                                        <td colspan="2" class="text-center" style="padding: 2%;">
                                            @if($val->siklus_1 == 'done')
                                                @if($val->status_data1 == 2)
                                                <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=1" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                                @else
                                                Belum Dikirim
                                                @endif
                                            @else
                                            Belum Dikirim
                                            @endif
                                        </td>
                                    @elseif($siklus == 2)
                                    <td colspan="2" class="text-center"  style="padding: 2%;">
                                        @if($val->siklus_2 == 'done')
                                            @if($val->status_data2 == 2)
                                            <a href="{{URL('').$val->Link}}/evaluasi/print/{{$val->id}}?y=2" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                            @else
                                            Belum Dikirim
                                            @endif
                                        @else
                                         Belum Dikirim
                                        @endif
                                    </td>
                                    @else
                                    <td>Belum Dikirim</td>
                                    @endif
                                @else
                                    <td>Tahun {{$tahun}}</td>
                                @endif
                            @endif
                                
                                @if($val->bidang == '7')                                
                                @endif

                        
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="6">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection