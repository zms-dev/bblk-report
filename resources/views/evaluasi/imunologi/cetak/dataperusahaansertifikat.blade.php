@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sertifikat Imunologi Siklus 
                {{$siklus}} Tahun {{$tahun}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="get" enctype="multipart/form-data">
                   {{--  <div>
                        <label for="exampleInputEmail1">Pilih Tanggal Sertifikat :</label>
                        <input type="text" name="tanggal" id="tanggal" class="datepick form-control" required="" value="{{ date('Y-m-d') }}">
                    </div><br>
                    <div>
                        <label for="exampleInputEmail1">Nomor Sertifikat :</label>
                        <input type="text" name="nomor" id="nomor" class="form-control" required="" value="IMN-0-0-0">
                    </div><br> --}}
                    <table class="table table-bordered">
                        <tr>
                            <th width="2%">No</th>
                            <th>Bidang</th>
                            <th colspan="2" width="15%">Aksi</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->id_bidang > '5')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}</td>
                                 @if($val->bidang == '7')
                                    @if($val->tanggalna == $tahun)
                                        @if($siklus == 1)
                                            <td>
                                            @if($val->siklus_1 == 'done')
                                                @if($val->pemeriksaan == 'done')
                                                    @if($val->status_data1 == 2)
                                                         <a href="" class="go-link"  data-id="{{$val->id}}" data-y="2" data-link="{{$val->Link}}" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                    @else
                                                        Belum Dikirim
                                                    @endif
                                                @endif                        
                                            @else
                                            Belum Dikirim
                                            @endif
                                            </td>
                                        @elseif($siklus == 2)                                    
                                            <td>
                                                @if($val->siklus_2 == 'done')
                                                    @if($val->pemeriksaan2 == 'done')
                                                        @if($val->status_data2 == 2)
                                                             <a href="" class="go-link"  data-id="{{$val->id}}" data-y="2" data-link="{{$val->Link}}" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i><br>Anti TP</center></a>
                                                        @else
                                                            Belum Dikirim
                                                        @endif
                                                    @endif
                                                @else
                                            Belum Dikirim
                                            @endif
                                            </td>
                                        @else
                                            <td>Belum Dikirim</td>
                                        @endif
                                    @else
                                        <td>Tahun {{$tahun}}</td>
                                    @endif
                                @else
                                @if($val->tanggalna == $tahun)
                                    @if($siklus == 1)
                                        <td colspan="2" class="text-center" style="padding: 2%;">
                                            @if($val->siklus_1 == 'done')
                                                @if($val->status_data1 == 2)
                                                <a href="" class="go-link"  data-id="{{$val->id}}" data-y="1" data-link="{{$val->Link}}" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i><br></center></a>

                                                {{-- <a href="{{URL('').$val->Link}}/sertifikat/print/{{$val->id}}?y=1" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i></center></a> --}}
                                                @else
                                                Belum Dikirim
                                                @endif
                                            @else
                                            Belum Dikirim
                                            @endif
                                        </td>
                                    @elseif($siklus == 2)
                                        <td colspan="2" class="text-center"  style="padding: 2%;">
                                            @if($val->siklus_2 == 'done')
                                                @if($val->status_data2 == 2)
                                               <a href="" class="go-link"  data-id="{{$val->id}}" data-y="2" data-link="{{$val->Link}}" target="_blank"><center><i class="glyphicon glyphicon-pencil"></i><br></center></a>

                                                @else
                                                Belum Dikirim
                                                @endif
                                            @else
                                             Belum Dikirim
                                            @endif
                                        </td>
                                    @else
                                        <td>Belum Dikirim</td>
                                    @endif
                                @else
                                    <td>Tahun {{$tahun}}</td>
                                @endif
                                @endif
                                
                                @if($val->bidang == '7')                                
                                @endif

                        
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
    <script type="text/javascript">
    $(".go-link").on("click",function(e){
        e.preventDefault()
        var y = $(this).data("y");
        var idLink = $(this).data("id");
        var link = $(this).data("link");
        var tanggal = $("#tanggal").val();
        var nomor = $("#nomor").val();
        var baseUrl = "{{URL('')}}"+link+"/sertifikat/print/"+idLink+"?y="+y+"&t="+tanggal+"&n="+nomor+"&download=true";
        var url = baseUrl;
        console.log(url);
        
        window.location.href = url;
    })

    $(".go-link-rpr").on("click",function(e){
        e.preventDefault()
        var y = $(this).data("y");
        var idLink = $(this).data("id");
        var tanggal = $("#tanggal").val();
        var nomor = $("#nomor").val();
        var baseUrl = "{{URL('')}}"+"/hasil-pemeriksaan/rpr-syphilis"+"/sertifikat/print/"+idLink+"?y="+y+"&t="+tanggal+"&n="+nomor+"&download=true";
        var url = baseUrl;
        console.log(url);
        
        window.location.href = url;
    })
</script>
@endsection