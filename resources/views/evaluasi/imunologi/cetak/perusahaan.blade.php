@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cetak Lampiran Evaluasi</div>
                <div class="panel-body">
                    <blockquote style="font-size: 12px">
                         <button type="button" class="btn-primary" style="border: none;" disabled>&nbsp;</button> : Data belum di Evaluasi &nbsp;
                        <button type="button" class="btn-info" style="border: none;" disabled>&nbsp;</button> : Data Sudah di Evaluasi <br><br>
                    </blockquote>
                     <div>
                        <label>Siklus </label>
                        <select name="siklus" class="form-control" id="siklus1">
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                        <div>
                              <label for="exampleInputEmail1">Tahun</label>
                              <div class="controls input-append date form_datetime" data-link-field="dtp_input1">
                                  <input size="16" type="text" value="" readonly class="form-control" name="tahun" id="tahun">
                                  <span class="add-on"><i class="icon-th"></i></span>
                              </div>
                        </div>
                        <br>
                        <button class="btn btn-primary" name="" id="cari">Cari</button>
                    </div><br>
                   <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                        <tr>
                            <th><center>Aksi</center></th>
                            <th><center>No</center></th>
                            <th><center>Instansi</center></th>
                            <th><center>Alamat</center></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    });
    </script>
@endif
@stop

@section('scriptBlock')
<script>
         $("#cari").click(function(){
        var siklus = $('#siklus1').val();
        var tahun = $('#tahun').val();
        var table = $(".dataTables-data");
        var dataTable = table.DataTable({
        "bDestroy":true,
        responsive:!0,
        "serverSide":true,
        "processing":true,
        "ajax":{
            url : "{{url('/evaluasi/cetak-imunologi').'?siklus='}}"+siklus+"{{'&tahun='}}"+tahun,
        },
        dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
        language:{
            paginate:{
                previous:"&laquo;",
                next:"&raquo;"
            },search:"_INPUT_",
            searchPlaceholder:"Search..."
        },
         "columns":[
                {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
                {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
                {"data":"nama_lab","name":"nama_lab","searchable":true,"orderable":true},
                {"data":"alamat","name":"alamat","searchable":true,"orderable":true},
                {"data":"kode_lebpes","name":"kode_lebpes","visible":false,"searchable":true}

        ],
        order:[[1,"asc"]]
        });
    });
         
   $(".form_datetime").datetimepicker({
        format: "yyyy",
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-right",
        minView: 4,
        startView: 4
    });

</script>
@stop