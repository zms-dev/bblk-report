<table border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Instansi</th>
            <th>Kode</th>
            <th>Nama Produsen</th>
            <th>Reagen</th>
            <th>Metode</th>
            <th>Lot</th>
            <th>Exp. Reagen</th>
            <?php for ($i=1; $i <= 3; $i++) { ?>
            <th>Hasil Tabung {{$i}}</th>
            @if($type == 1)
                <th>Titer Tabung {{$i}}</th>
            @endif
            <?php } ?>
            <th>Catatan</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; ?>
        @foreach($data as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{{$val->nama_lab}}</td>
            <td>{{$val->kode_lebpes}}</td>
            @foreach($val->reagen as $reg)
            <td>{{$reg->nama_produsen}}</td>
            <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
            <td>{{$reg->metode}}</td>
            <td>{{$reg->nomor_lot}}</td>
            <td>{{$reg->tgl_kadaluarsa}}</td>
            @endforeach
            @foreach($val->hpimun as $st)
                <td>{{$st->interpretasi}}</td>
                @if($type == 1)
                    <th>{{$st->titer}}</th>
                @endif
            @endforeach
            <td>{{$val->keterangan}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
