<table border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Instansi</th>
            <th>Kode</th>
            @if($type == 1)
                <th>Nama Produsen</th>
                <th>Reagen</th>
                <th>Metode</th>
                <th>Lot</th>
                <th>Exp. Reagen</th>
            @else
                <th>Nama Produsen 1</th>
                <th>Reagen 1</th>
                <th>Metode 1</th>
                <th>Lot 1</th>
                <th>Exp. Reagen 1</th>
                <th>Nama Produsen 2</th>
                <th>Reagen 2</th>
                <th>Metode 2</th>
                <th>Lot 2</th>
                <th>Exp. Reagen 2</th>
                <th>Nama Produsen 3</th>
                <th>Reagen 3</th>
                <th>Metode 3</th>
                <th>Lot 3</th>
                <th>Exp. Reagen 3</th>
            @endif
            <?php $no = 0; ?>
            @foreach($data[0]->hpimun as $st)
                <?php $no++; ?>
                <th>Hasil Tabung {{$st->tabung}}</th>
            @endforeach
            <th>Catatan</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; ?>
        @foreach($data as $val)
        <?php $no++; ?>
        <tr>
            <td>{{$no}}</td>
            <td>{{$val->nama_lab}}</td>
            <td>{{$val->kode_lebpes}}</td>

            @if($type == 1)
                @foreach($val->reagen as $reg)
                <td>{{$reg->nama_produsen}}</td>
                <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                <td>{{$reg->metode}}</td>
                <td>{{$reg->nomor_lot}}</td>
                <td>{{$reg->tgl_kadaluarsa}}</td>
                @endforeach
            @else
                @if(count($val->reagen) == 3)
                    @foreach($val->reagen as $reg)
                    <td>{{$reg->nama_produsen}}</td>
                    <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                    <td>{{$reg->metode}}</td>
                    <td>{{$reg->nomor_lot}}</td>
                    <td>{{$reg->tgl_kadaluarsa}}</td>
                    @endforeach
                @elseif(count($val->reagen) == 2)
                    @foreach($val->reagen as $reg)
                    <td>{{$reg->nama_produsen}}</td>
                    <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                    <td>{{$reg->metode}}</td>
                    <td>{{$reg->nomor_lot}}</td>
                    <td>{{$reg->tgl_kadaluarsa}}</td>
                    @endforeach
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @elseif(count($val->reagen) == 1)
                    @foreach($val->reagen as $reg)
                    <td>{{$reg->nama_produsen}}</td>
                    <td>{{$reg->reagen}} @if($reg->reagen_lain != NULL): {{$reg->reagen_lain}}@endif</td>
                    <td>{{$reg->metode}}</td>
                    <td>{{$reg->nomor_lot}}</td>
                    <td>{{$reg->tgl_kadaluarsa}}</td>
                    @endforeach
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @endif
            @endif
            <?php $non = 0; ?>
            @foreach($val->hpimun as $st)
                <?php $non++; ?>
                <td>{{$st->interpretasi}}</td>
            @endforeach
            <td>{{$val->keterangan}}</td>
        </tr>
        @endforeach
    </tbody>
</table>