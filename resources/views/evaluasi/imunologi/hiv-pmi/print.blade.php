<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}

#peserta_pme td,#peserta_pme th{
    border: 1px solid #ddd;
    text-align: center;
}

.reagen {
    width: 75%;
    margin-left: 20px;
}
.reagen:before{
    position: absolute;
    margin-left: -15px;
    color: #000;
    content: counter(paragraph) ". ";
    counter-increment: paragraph;
}
.tabel {
    border-collapse: collapse;
    width: 100%;
}

.tabel th, .tabel td {
    padding: 2px;
}

.header{
position: fixed;
}

.header {
top: 0;
}

#footer { 
    position: fixed; 
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<div id="footer">
    EVALUASI PNPME IMUNOLOGI ANTI HIV SIKLUS @if($type == '1') I @else II @endif TAHUN {{$tahun}}
</div>
<table class="tabel" width="100%" cellpadding="0" border="0">
    <thead>
        <tr>
            <th>
                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
            </th>
            <th width="100%" style="text-align: center;">
                <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                <span style="font-size: 12px">PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br>ANTI HIV TAHUN {{$tahun}}</span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -7 -25px; margin-left: -42px;">
                Penyelenggara : Balai Besar Laboratorium Kesehatan Jakarta
                Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560
                Telepon : (021) 4212524, 42804339 Fax. (021) 4245516
                Email : bblkjakarta@yahoo.co.id
                </pre>
            </th>
            <th>
                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" height="75px">
            </th>
        </tr>
        <tr>
            <th colspan="3"><hr></th>
        </tr>
    </thead>
</table>
<center><label><b>HASIL EVALUASI PESERTA <br>
PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL ANTI HIV SIKLUS @if($type == '1') I @else II @endif TAHUN {{$tahun}}</b>
<input type="hidden" name="type" value="{{$type}}"></label></center><br>
<form class="form-horizontal" method="post" enctype="multipart/form-data">
<b>Nama Instansi :</b> &nbsp;{{$perusahaan}}<br>
<b>Kode Peserta :</b> &nbsp;{{$kodeperusahaan}}
<br><br>
<b>Nama Reagen :</b>
<table>
    <?php $no = 0; $i = 1;  ?>
    @foreach($data2 as $hasil)
    @if($hasil->tabung == '1')
        @if($hasil->interpretasi == 'tanpa-test')
        @else
            @if(!empty($reagen[$no]))
                @if($no != 0)
                @else
                    @foreach($reagen as $reg)
                    @if($reg->reagen != null)
                        <tr>
                            <td width="5px">{{$i}}. </td>
                            <td>
                                @if($reg->reagen == 'Lain - lain')
                                    {{$reg->reagen_lain}}
                                @else
                                    {{$reg->reagen}}
                                @endif
                                &nbsp;
                            </td>
                            <td>
                                Sensitivitas : {{$reg->sensitivitas}}
                                @if($reg->reagen == 'Lain - lain')
                                        {{$reg->sensi}}
                                @endif
                                , Spesifisitas : {{$reg->spesifisitas}}
                                @if($reg->reagen == 'Lain - lain')
                                    {{$reg->spesi}}
                                @endif  
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>No. Lot/Batch : {{$reg->nomor_lot}}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Tanggal Kedaluwarsa : {{$reg->tgl_kadaluarsa}}</td>
                            <td></td>
                        </tr>
                    @else
                    @endif
                    <?php $i++; ?>
                    @endforeach
                @endif
            @else
            @endif
        @endif
    @endif
    <?php $no++;?>
    @endforeach
</table>
<br>
<table id="peserta_pme" class="tabel">
    <tr class="titlerowna">
        <th rowspan="2">Kode Bahan Uji</th>
        <th rowspan="2">Hasil Pemeriksaan</th>
        <th rowspan="2">Hasil Rujukan</th>
        <th colspan="2">Ketepatan Hasil</th>
        <th colspan="2">Kesesuaian Strategi</th>
    </tr>
    <tr>
        <th>Nilai</th>
        <th>Kategori</th>
        <th>Nilai</th>
        <th>Kategori</th>
    </tr>
    <?php  
        $no = 0;
        $val = ['I','II','III','IV','V'];
        $tabung = '';
        $nilai1 = 0;
        $nilai2 = 0;
        $bagi= 0;
        $bagi2= 0;
    ?>
    @foreach($data2 as $hasil)
    <tr>
        <td align="middle">
            <?php if(strlen($hasil->kode_bahan_kontrol) > 3){ ?>
                {{$hasil->kode_bahan_kontrol}}
            <?php }else{?>
                I-{{$no+1}}-{{$hasil->kode_bahan_kontrol}}
            <?php }?>
        </td>
        <td>{{$hasil->interpretasi}}</td>
        <td>{{$rujukan[$no]->nilai_rujukan}}</td>
        <td>
            @if($hasil->interpretasi == $rujukan[$no]->nilai_rujukan)
            <?php $nilai1 = $nilai1 + 4; $bagi++;?>
            4
            @elseif($hasil->interpretasi == 'Tanpa test')
            @else
            <?php $bagi++;?>
            0
            @endif
        </td>
        <td>
            @if($hasil->interpretasi == $rujukan[$no]->nilai_rujukan)
            Benar
            @elseif($hasil->interpretasi == 'Tanpa test')
            @else
            Salah
            @endif
        </td>
        <td>
            @if(count($strategi))
                @foreach($strategi as $str)
                    @if($str->tabung == $no+1)
                        <?php $nilai2 = $nilai2 + $str->nilai; $bagi2++;?>
                        {{$str->nilai}}
                    @endif
                @endforeach
            @endif
        </td>
        <td>
            @if(count($strategi))
                @foreach($strategi as $str)
                    @if($str->tabung == $no+1)
                        {{$str->kategori}}
                    @endif
                @endforeach
            @endif
        </td>
    <?php $no++; $tabung = $hasil->tabung ?>
    </tr>
    @endforeach
    <tr>
        <td colspan="3" style="text-align: right;">Total :</td>
        <td colspan="2"><?php $nilai1 = $nilai1 / $bagi?>{{number_format($nilai1,2)}}</td>
        <td colspan="2"><?php $nilai2 = $nilai2 / $bagi2?>{{number_format($nilai2,2)}}</td>
    </tr>
</table>
<h4>Hasil Pemeriksaan :</h4>
<p>
   1. Ketepatan Hasil : <b>{{$kesimpulan->ketepatan}}</b><br>2. Kesesuaian Strategi : <b>{{$kesimpulan->kesesuaian}}</b>
</p>
@if(count($evaluasi))
@if($evaluasi->nilai_1 != 'ada' && $evaluasi->nilai_2 != 'ada' && $evaluasi->nilai_3 != 'ada' && $evaluasi->nilai_4 != 'ada' && $evaluasi->nilai_5 != 'ada' && $evaluasi->nilai_6 != 'ada' && $evaluasi->nilai_7 != 'ada' && $evaluasi->nilai_8 != 'ada' && $evaluasi->nilai_9 != 'ada' && $evaluasi->nilai_10 != 'ada')
@else
<h4>Komentar :</h4>
@endif
<ul>
    @if($evaluasi->nilai_1 == 'ada')
        <li>
            Alur pemeriksaan yang Saudara kerjakan sudah SESUAI dengan strategi III pemeriksaan Anti HIV.
        </li>
    @endif
    @if($evaluasi->nilai_2 == 'ada')
    <li>
        Alur pemeriksaan yang Saudara kerjakan TIDAK SESUAI dengan strategi III pemeriksaan Anti HIV. Gunakan alur pemeriksaan yang sesuai dengan strategi III pemeriksaan HIV.
    </li>
    @endif
    @if($evaluasi->nilai_3 == 'ada')
    <li>
        Jenis reagen yang Saudara gunakan SESUAI dengan hasil evaluasi laboratorium rujukan nasional RSUPN Cipto Mangunkusumo.
    </li>
    @endif
    @if($evaluasi->nilai_4 == 'ada')
    <li>
        Jenis reagen yang Saudara gunakan TIDAK SESUAI dengan hasil evaluasi laboratorium rujukan RSUPN Cipto Mangunkusumo. Gunakan jenis reagen yang sudah dilakukan evaluasi oleh RSUPN Cipto Mangunkusumo.
    </li>
    @endif
    @if($evaluasi->nilai_5 == 'ada')
    <li>
        Urutan reagen yang Saudara gunakan SESUAI dengan strategi III pemeriksaan Anti HIV. Sensitivitas dan spesifisitas reagen berdasarkan hasil evaluasi reagen laboratorium rujukan RSUPN Cipto Mangunkusumo.
    </li>
    @endif
    @if($evaluasi->nilai_6 == 'ada')
    <li>
        Urutan reagen yang Saudara gunakan TIDAK SESUAI dengan strategi III pemeriksaan Anti HIV. Gunakan urutan reagen berdasarkan sensitivitas dan spesifisitas hasil evaluasi reagen laboratorium rujukan RSUPN Cipto Mangunkusumo.
    </li>
    @endif
    @if($evaluasi->nilai_7 == 'ada')
        <li>
            Jenis reagen yang saudara gunakan SESUAI dengan strategi 1 pemeriksaan Anti HIV.
        </li>
    @endif
    @if($evaluasi->nilai_12 == 'ada')
        <li>
            Jenis reagen yang saudara gunakan TIDAK SESUAI dengan strategi 1 pemeriksaan Anti HIV.
        </li>
    @endif
    @if($evaluasi->nilai_15 == 'ada')
        <li>
            Strategi 1 (Pemenkes RI no.15 Tahun 2015) Gunakan reagensia yang memiliki sensitivitas dan spesifisitas >= 99%
        </li>
    @endif
    @if($evaluasi->nilai_8 == 'ada' || $evaluasi->nilai_13 == 'ada' || $evaluasi->nilai_14 == 'ada')
        <li>
            Hasil pemeriksaan tidak dilakukan penilaian karena tidak mencantumkan tanggal kedaluwarsa reagen.
        </li>
    @endif
    @if($evaluasi->nilai_9 == 'ada' || $evaluasi->nilai_10 == 'ada' || $evaluasi->nilai_11 == 'ada')
        <li>
            Hasil pemeriksaan tidak dilakukan penilaian karena reagen yang digunakan telah melewati tanggal kedaluwarsa.
        </li>
    @endif
</ul>
@else
@endif
<div style="margin-top: -14px;">
@if($catatan->catatan != '')
<h4>Saran : </h4>
@endif
<p>
    <span style="font-family: DejaVu Sans; sans-serif;">{!!$catatan->catatan!!}</span>
</p>
</div>
<br>
<div style="position: relative; left: 75%;">
<p>
    <div style="position: relative; top: 0px;">
    Jakarta, @if(count($tanggalevaluasi)){{ _dateIndo($tanggalevaluasi->tanggal) }} @endif<br>
    Penanggung jawab<br>
    Laboratorium Imunologi
    </div>
    <p>
        <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'imunologi_ttd.png')}}" width="180" style="margin: -75px 0px -50px 0px !important;">
    </p>

    <div style="position: relative; top:-20px">
    dr. Siti Kurnia Eka Rusmiarti, Sp.PK
    </div>
</p>
</div>



<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>