@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Penilaian dan Lampiran Evaluasi</div>
                <div class="panel-body">
                    <center><label>EVALUASI PESERTA PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL IMUNOLOGI TAHUN {{$date}} <br> HASIL PEMERIKSAAN ANTI HIV <input type="hidden" name="type" value="{{$type}}"></label></center><br>
                    <label>1. IDENTITAS LABORATORIUM</label>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Kode Peserta </label>
                        <div class="col-sm-10">
                            <input readonly="" type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $kodeperusahaan }}" placeholder="Kode Peserta">
                        </div>
                    </div><br>
                    <div class="form-group">
                        <label for="nama" class="col-sm-2 control-label">Nama Instansi </label>
                        <div class="col-sm-10">
                            <input readonly="" type="text" class="form-control" id="kode_peserta" name="kode_peserta" value="{{ $perusahaan }}" placeholder="Kode Peserta">
                        </div>
                    </div>
                    @if(count($kesimpulan))
                        <form id="someForm" action="" method="post">
                    @else
                        <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    @endif
                    <label>2. HASIL EVALUASI</label>

                    @if(count($strategi))
                        @foreach($strategi as $str)
                            <input type="hidden" name="idevaluasi[]" value="{{$str->id}}">
                        @endforeach
                    @endif
                    <table class="table table-bordered" id="peserta_pme">
                        <tr class="titlerowna">
                            <th rowspan="2" style="text-align: center;">Kode Bahan Uji</th>
                            <th rowspan="2" style="text-align: center;">Metode Pemeriksaan</th>
                            <th rowspan="2" style="text-align: center;">Reagen</th>
                            <th rowspan="2" style="text-align: center;">Hasil Pemeriksaan</th>
                            <th rowspan="2" style="text-align: center;">Hasil Rujukan</th>
                            <th colspan="2" style="text-align: center;">Ketepatan Hasil</th>
                            <th colspan="2" style="text-align: center;">Kesesuaian Strategi</th>
                        </tr>
                        <tr>
                            <th>Nilai</th>
                            <th>Kategori</th>
                            <th>Nilai</th>
                            <th>Kategori</th>
                        </tr>
                        <?php
                            $no = 0;
                            $val = ['I','II','III','IV','V'];
                        ?>
                        @foreach($data2 as $hasil)
                        <tr><input readonly="" type="hidden" name="idhp[]" value="{{$hasil->id}}">
                            <td>
                                <?php if(strlen($hasil->kode_bahan_kontrol) > 3){ ?>
                                    <input readonly="" type="hidden" name="kode_bahan_kontrol[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}">{{$hasil->kode_bahan_kontrol}}
                                <?php }else{?>
                                    <input readonly="" type="hidden" name="kode_bahan_kontrol[]" class="form-control" required value="{{$hasil->kode_bahan_kontrol}}">I-1-{{$hasil->kode_bahan_kontrol}}
                                <?php }?>
                            </td>
                        @if($hasil->interpretasi == 'Tanpa test')
                        <td></td>
                        <td></td>
                        @else
                            @if(!empty($reagen[0]))
                                <td style="text-transform: uppercase; text-align: center;">{{$reagen[0]->metode}}</td>
                                <td style="text-align: center;">
                                    {!! str_replace('Lain - lain', '', $reagen[0]->reagen_lain.$reagen[0]->reagen)!!}<br>
                                    SensitiVitas : {{$reagen[0]->sensitivitas}}
                                    @if($reagen[0]->reagen == 'Lain - lain')
                                        <input type="hidden" name="idreagen[]" value="{{$reagen[0]->idreagen}}">
                                        @if($reagen[0]->sensi != NULL)
                                        <input type="text" name="sensitivitas[]" value="{{$reagen[0]->sensi}}">
                                        @else
                                        <input type="text" name="sensitivitas[]" value="">
                                        @endif
                                    @endif
                                    , Spesifisitas : {{$reagen[0]->spesifisitas}}
                                    @if($reagen[0]->reagen == 'Lain - lain')
                                        @if($reagen[0]->spesi != NULL)
                                        <input type="text" name="spesifisitas[]" value="{{$reagen[0]->spesi}}">
                                        @else
                                        <input type="text" name="spesifisitas[]" value="">
                                        @endif
                                    @endif
                                </td>
                            @else
                            <td></td>
                            <td></td>
                            @endif
                        @endif
                            <td style="text-align: center;">{{$hasil->interpretasi}}</td>
                            <td style="text-align: center;">{{$rujukan[$no]->nilai_rujukan}}</td>
                            <td style="text-align: center;">
                                @if($hasil->interpretasi == $rujukan[$no]->nilai_rujukan)
                                4
                                @elseif($hasil->interpretasi == 'Tanpa test')
                                @else
                                0
                                @endif
                            </td>
                            <td style="text-align: center;">
                                @if($hasil->interpretasi == $rujukan[$no]->nilai_rujukan)
                                Benar
                                @elseif($hasil->interpretasi == 'Tanpa test')
                                @else
                                Salah
                                @endif
                            </td>
                            <td>
                                <select name="strategi[]" class="form-control" id="strategi{{$no+1}}" required>
                                        @if(count($strategi))
                                            @foreach($strategi as $str)
                                                @if($str->tabung == $no+1)
                                                    <option value="{{$str->nilai}}">{{$str->nilai}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    <option value=""></option>
                                    <option value="0">0</option>
                                    <option value="5">5</option>
                                </select>
                            </td>
                            <td style="text-align: center;" id="kategori{{$no+1}}">
                                @if(count($strategi))
                                    @foreach($strategi as $str)
                                        @if($str->tabung == $no+1)
                                            {{$str->kategori}}
                                        @endif
                                    @endforeach
                                @endif
                            </td>
                            <input type="hidden" name="id_master_imunologi" value="{{$hasil->id_master_imunologi}}">
                        </tr>
                        <?php $no++; ?>
                        @endforeach
                    </table>
                    @if(count($evaluasi))
                    <p>
                        <input type="checkbox" name="nilai_1" value="ada" {{ ($evaluasi->nilai_1 == 'ada') ? 'checked' : '' }}> 
                        1. Alur pemeriksaan yang Saudara kerjakan sudah SESUAI dengan strategi III pemeriksaan Anti HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_2" value="ada" {{ ($evaluasi->nilai_2 == 'ada') ? 'checked' : '' }}> 
                        2. Alur pemeriksaan yang Saudara kerjakan TIDAK SESUAI dengan strategi III pemeriksaan Anti HIV. Gunakan alur pemeriksaan yang sesuai dengan strategi III pemeriksaan HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_3" value="ada" {{ ($evaluasi->nilai_3 == 'ada') ? 'checked' : '' }}>
                        3. Jenis reagen yang Saudara gunakan SESUAI dengan hasil evaluasi laboratorium rujukan nasional RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_4" value="ada" {{ ($evaluasi->nilai_4 == 'ada') ? 'checked' : '' }}> 
                        4. Jenis reagen yang Saudara gunakan TIDAK SESUAI dengan hasil evaluasi laboratorium rujukan RSUPN Cipto Mangunkusumo. Gunakan jenis reagen yang sudah dilakukan evaluasi oleh RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_5" value="ada" {{ ($evaluasi->nilai_5 == 'ada') ? 'checked' : '' }}> 
                        5. Urutan reagen yang Saudara gunakan SESUAI dengan strategi III pemeriksaan Anti HIV. Sensitivitas dan spesifisitas reagen berdasarkan hasil evaluasi reagen laboratorium rujukan RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_6" value="ada" {{ ($evaluasi->nilai_6 == 'ada') ? 'checked' : '' }}> 
                        6. Urutan reagen yang Saudara gunakan TIDAK SESUAI dengan strategi III pemeriksaan Anti HIV. Gunakan urutan reagen berdasarkan sensitivitas dan spesifisitas hasil evaluasi reagen laboratorium rujukan RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_7" value="ada" {{ ($evaluasi->nilai_7 == 'ada') ? 'checked' : '' }}> 
                        7. Jenis reagen yang saudara gunakan SESUAI dengan strategi 1 pemeriksaan Anti HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_12" value="ada" {{ ($evaluasi->nilai_12 == 'ada') ? 'checked' : '' }}> 
                        8. Jenis reagen yang saudara gunakan TIDAK SESUAI dengan strategi 1 pemeriksaan Anti HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_15" value="ada" {{ ($evaluasi->nilai_15 == 'ada') ? 'checked' : '' }}> 
                        9. Strategi 1 (Pemenkes RI no.15 Tahun 2015) Gunakan reagensia yang memiliki sensitivitas dan spesifisitas >= 99%
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_8" value="ada" {{ ($evaluasi->nilai_8 == 'ada') ? 'checked' : '' }}> 
                        10. Hasil pemeriksaan tidak dilakukan penilaian karena tidak mencantumkan tanggal kedaluwarsa reagen 1.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_13" value="ada" {{ ($evaluasi->nilai_13 == 'ada') ? 'checked' : '' }}>
                        11. Hasil pemeriksaan tidak dilakukan penilaian karena tidak mencantumkan tanggal kedaluwarsa reagen 2.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_14" value="ada" {{ ($evaluasi->nilai_14 == 'ada') ? 'checked' : '' }}>
                        12. Hasil pemeriksaan tidak dilakukan penilaian karena tidak mencantumkan tanggal kedaluwarsa reagen 3.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_9" value="ada" {{ ($evaluasi->nilai_9 == 'ada') ? 'checked' : '' }}> 
                        13. Hasil pemeriksaan tidak dilakukan penilaian karena reagen 1 yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_10" value="ada" {{ ($evaluasi->nilai_10 == 'ada') ? 'checked' : '' }}> 
                        14. Hasil pemeriksaan tidak dilakukan penilaian karena reagen 2 yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_11" value="ada" {{ ($evaluasi->nilai_11 == 'ada') ? 'checked' : '' }}>
                        15. Hasil pemeriksaan tidak dilakukan penilaian karena reagen 3 yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    @else
                    <p>
                        <input type="checkbox" name="nilai_1" value="ada"> 
                        1. Tahap pemeriksaan yang Saudara kerjakan sudah SESUAI dengan strategi III pemeriksaan Anti HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_2" value="ada"> 
                        2. Tahap pemeriksaan yang Saudara kerjakan TIDAK SESUAI dengan strategi III pemeriksaan Anti HIV. Gunakan alur pemeriksaan yang sesuai dengan strategi III untuk menegakkan diagnosis HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_3" value="ada"}>
                        3. Jenis reagen yang Saudara gunakan SESUAI dengan hasil evaluasi laboratorium rujukan nasional RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_4" value="ada"> 
                        4. Jenis reagen yang Saudara gunakan TIDAK SESUAI dengan hasil evaluasi laboratorium rujukan RSUPN Cipto Mangunkusumo. Gunakan jenis reagen yang sudah dilakukan evaluasi oleh RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_5" value="ada"> 
                        5. Urutan reagen yang Saudara gunakan SESUAI dengan hasil evaluasi laboratorium rujukan nasional RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_6" value="ada">
                        6. Urutan reagen yang Saudara gunakan TIDAK SESUAI dengan strategi III pemeriksaan Anti HIV. Gunakan urutan reagen berdasarkan sensitivitas dan spesifisitas hasil evaluasi reagen laboratorium rujukan RSUPN Cipto Mangunkusumo.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_7" value="ada"> 
                        7. Jenis reagen yang saudara gunakan SESUAI dengan strategi 1 pemeriksaan Anti HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_12" value="ada"> 
                        8. Jenis reagen yang saudara gunakan TIDAK SESUAI dengan strategi 1 pemeriksaan Anti HIV.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_15" value="ada"> 
                        9. Strategi 1 (Pemenkes RI no.15 Tahun 2015) Gunakan reagensia yang memiliki sensitivitas dan spesifisitas >= 99%
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_8" value="ada"> 
                        10. Hasil pemeriksaan tidak dilakukan evaluasi karena tidak mencantumkan tanggal kedaluwarsa reagen.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_13" value="ada"> 
                        11. Hasil pemeriksaan tidak dilakukan evaluasi karena reagen yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_14" value="ada">
                        12. Hasil pemeriksaan tidak dilakukan penilaian karena tidak mencantumkan tanggal kedaluwarsa reagen 3.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_9" value="ada"> 
                        13. Hasil pemeriksaan tidak dilakukan penilaian karena reagen 1 yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_10" value="ada"> 
                        14. Hasil pemeriksaan tidak dilakukan penilaian karena reagen 2 yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    <p>
                        <input type="checkbox" name="nilai_11" value="ada">
                        15. Hasil pemeriksaan tidak dilakukan penilaian karena reagen 3 yang digunakan telah melewati tanggal kedaluwarsa.
                    </p>
                    @endif

                    <h4>Penilaian Hasil Pemeriksaan Peserta :</h4>
                    <blockquote>
                        1. Ketepatan Hasil  <select name="ketepatan">
                                                @if(count($kesimpulan))
                                                    @if($kesimpulan->ketepatan != NULL)
                                                        <option value="{{$kesimpulan->ketepatan}}">{{$kesimpulan->ketepatan}}</option>
                                                        <option value="Baik">Baik</option>
                                                        <option value="Kurang">Kurang</option>
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @else
                                                        <option></option>
                                                        <option value="Baik">Baik</option>
                                                        <option value="Kurang">Kurang</option>
                                                        <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                    @endif
                                                @else
                                                    <option></option>
                                                    <option value="Baik">Baik</option>
                                                    <option value="Kurang">Kurang</option>
                                                    <option value="Tidak dapat dinilai">Tidak dapat dinilai</option>
                                                @endif
                                            </select><br>
                        2. Kesesuaian Strategi  <select name="kesesuaian">
                                                    @if(count($kesimpulan))
                                                        @if($kesimpulan->ketepatan != NULL)
                                                        <option value="{{$kesimpulan->kesesuaian}}">{{$kesimpulan->kesesuaian}}</option>
                                                            <option value="Sesuai">Sesuai</option>
                                                            <option value="Tidak Sesuai">Tidak Sesuai</option>
                                                        @else
                                                            <option></option>
                                                            <option value="Sesuai">Sesuai</option>
                                                            <option value="Tidak Sesuai">Tidak Sesuai</option>
                                                        @endif
                                                    @else
                                                        <option></option>
                                                        <option value="Sesuai">Sesuai</option>
                                                        <option value="Tidak Sesuai">Tidak Sesuai</option>
                                                    @endif
                                                </select>
                    </blockquote>
                    <label>Saran :</label>
                    @if(count($catatan))
                    <textarea name="catatan" id="editor1" rows="10" cols="80">{!!$catatan->catatan!!}</textarea>
                    @else
                    <textarea name="catatan" id="editor1" rows="10" cols="80"></textarea>
                    @endif
                    <br>
                    @if(count($kesimpulan))
                    <a href="{{url('hasil-pemeriksaan/anti-hiv/evaluasi/print').'/'.$register->id.'?y='.$type}}">
                        <input type="button" class="btn btn-primari" value="Print" name="print"/>
                    </a>
                    <input type="button" class="btn btn-primari" value="Update" name="update" onclick="askForUpdate()" />
                    @else
                    <input type="submit" name="simpan" value="Simpan" class="btn btn-primari">
                    @endif
                      {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
CKEDITOR.replace( 'editor1' );
form=document.getElementById("someForm");
function askForPrint() {
        form.action="{{url('hasil-pemeriksaan/anti-hiv/evaluasi/print').'/'.$register->id.'?y='.$type}}";
        form.submit();
}
function askForUpdate() {
        form.action="{{url('hasil-pemeriksaan/anti-hiv/evaluasi/update').'/'.$register->id.'?y='.$type}}";
        form.submit();
}

var total2=[0,0,0];
$(document).ready(function(){

    var $dataRows=$("#peserta_pme tr:not('.titlerowna')");

    $dataRows.each(function() {
        $(this).find('.colomngitung').each(function(i){
            total2[i]+=parseInt( $(this).html());
        });
    });
    $("#peserta_pme td.evaluasi").each(function(i){
        if (total2[i] < 15) {
            console.log( "no ready!" );
            $(".evaluasi").html("Tidak Baik");
        }else{
            $(".evaluasi").html("Baik");
            console.log( "Baik" );
        }
    });

});

$('#strategi1').change(function() {
    if($(this).val() == '0'){
        $("#kategori1").text('Tidak Sesuai');
        console.log('Tidak Sesuai')
    }
    else{
        $("#kategori1").text('Sesuai');
        console.log('Sesuai')
    }
});
$('#strategi2').change(function() {
    if($(this).val() == '0'){
        $("#kategori2").text('Tidak Sesuai');
        console.log('Tidak Sesuai')
    }
    else{
        $("#kategori2").text('Sesuai');
        console.log('Sesuai')
    }
});
$('#strategi3').change(function() {
    if($(this).val() == '0'){
        $("#kategori3").text('Tidak Sesuai');
        console.log('Tidak Sesuai')
    }
    else{
        $("#kategori3").text('Sesuai');
        console.log('Sesuai')
    }
});

$(".form_datetime").datetimepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayBtn: true,
    minView: 2
});
</script>
@endsection
