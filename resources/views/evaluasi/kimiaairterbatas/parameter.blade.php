@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Rekapitulasi Hasil PNPME</div>

                <div class="panel-body">
                    <h4>Parameter Hemoglobin</h4>
                    <table class="table table-bordered">
                        <tr>
                            <th rowspan="2"><center>Nilai</center></th>
                            <th rowspan="2"><center>Kriteria</center></th>
                            <th colspan="2"><center>Seluruh Peserta</center></th>
                            <th colspan="2"><center>Kelompok Metode</center></th>
                            <th colspan="2"><center>Kelompok Alat</center></th>
                        </tr>
                        <tr>
                          <td>Jumlah</td>
                          <td>%</td>
                          <td>Jumlah</td>
                          <td>%</td>
                          <td>Jumlah</td>
                          <td>%</td>
                        </tr>
                        <tr>
                          <td>l Z Score l ≤ 2</td>
                          <td>Memuaskan</td>
                          <td>269</td>
                          <td>91.19</td>
                          <td>264</td>
                          <td>89.49</td>
                          <td>250</td>
                          <td>84.75</td>
                        </tr>
                        <tr>
                          <td>2 < l Z Score l ≤ 3</td>
                          <td>Peringatan</td>
                          <td>14</td>
                          <td>4.75</td>
                          <td>13</td>
                          <td>4.41</td>
                          <td>27</td>
                          <td>9.15</td>
                        </tr>
                        <tr>
                          <td>l Z Score l > 3</td>
                          <td>Tidak Memuaskan</td>
                          <td>10</td>
                          <td>3.39</td>
                          <td>16</td>
                          <td>5.42</td>
                          <td>16</td>
                          <td>5.42</td>
                        </tr>
                        <tr>
                          <td colspan="2">Tidak Dianalisa</td>
                          <td>2</td>
                          <td>0.68</td>
                          <td>2</td>
                          <td>0.68</td>
                          <td>2</td>
                          <td>0.68</td>
                        </tr>
                        <tr>
                          <td colspan="2">Jumlah</td>
                          <td>295</td>
                          <td>100</td>
                          <td>295</td>
                          <td>100</td>
                          <td>295</td>
                          <td>100</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection