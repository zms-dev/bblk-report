@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Nilai Peserta </div>
                <div class="panel-body">
                  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Nilai Peserta Kimia Air Terbatas'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total Nilai Peserta Kimia Klinik'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
    },

    "series": [
        {
            "name": "Keterangan",
            "colorByPoint": true,
            "data": [
                {
                    "name": "Memuaskan",
                    @php
                        $arrC = [
                                    0 => 0,
                                    1 => 0,
                                    2 => 0,
                                    3 => 0,
                                ];
                    @endphp
                    @foreach($data as $val)
                    @if(count($zscore))
                        @foreach($zscore as $za)
                                @if($za->parameter == $val->id)
                                         <?php 
                                                if ($za->zscore != 'Tidak di Evaluasi') {
                                                    if($za->zscore <= 2 && $za->zscore >= -2){
                                                        $keterangan = 'Memuaskan';
                                                        $arrC['0']++;
                                                    }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                        $keterangan = 'Meragukan';
                                                        $arrC['1']++;
                                                    }else{
                                                        $keterangan = 'Kurang Memuaskan';
                                                        $arrC['2']++;
                                                    }
                                                }else{
                                                    $keterangan = 'Tidak di Evaluasi';
                                                    $arrC['3']++;
                                                }
                                                ?>
                                        "y" :{{$arrC[0]}},
                                @endif
                            @endforeach
                        @endif
                        @endforeach
                    
                },
                {
                    "name": "Meragukan",
                    @foreach($data as $val)
                    @if(count($zscore))
                        @foreach($zscore as $za)
                                @if($za->parameter == $val->id)
                                         <?php 
                                                if ($za->zscore != 'Tidak di Evaluasi') {
                                                    if($za->zscore <= 2 && $za->zscore >= -2){
                                                        $keterangan = 'Memuaskan';
                                                        $arrC['0']++;
                                                    }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                        $keterangan = 'Meragukan';
                                                        $arrC['1']++;
                                                    }else{
                                                        $keterangan = 'Kurang Memuaskan';
                                                        $arrC['2']++;
                                                    }
                                                }else{
                                                    $keterangan = 'Tidak di Evaluasi';
                                                    $arrC['3']++;
                                                }
                                                ?>
                                        "y" :{{$arrC[1]}},
                                @endif
                            @endforeach
                        @endif
                        @endforeach
                    
                },
                {
                    "name": "Kurang",
                    @foreach($data as $val)
                    @if(count($zscore))
                        @foreach($zscore as $za)
                                @if($za->parameter == $val->id)
                                         <?php 
                                                if ($za->zscore != 'Tidak di Evaluasi') {
                                                    if($za->zscore <= 2 && $za->zscore >= -2){
                                                        $keterangan = 'Memuaskan';
                                                        $arrC['0']++;
                                                    }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                        $keterangan = 'Meragukan';
                                                        $arrC['1']++;
                                                    }else{
                                                        $keterangan = 'Kurang Memuaskan';
                                                        $arrC['2']++;
                                                    }
                                                }else{
                                                    $keterangan = 'Tidak di Evaluasi';
                                                    $arrC['3']++;
                                                }
                                                ?>
                                        "y" :{{$arrC[2]}},
                                @endif
                            @endforeach
                        @endif
                        @endforeach
                },
                {
                    "name": "Tidak di Evaluasi",
                    @foreach($data as $val)
                    @if(count($zscore))
                        @foreach($zscore as $za)
                                @if($za->parameter == $val->id)
                                         <?php 
                                                if ($za->zscore != 'Tidak di Evaluasi') {
                                                    if($za->zscore <= 2 && $za->zscore >= -2){
                                                        $keterangan = 'Memuaskan';
                                                        $arrC['0']++;
                                                    }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                        $keterangan = 'Meragukan';
                                                        $arrC['1']++;
                                                    }else{
                                                        $keterangan = 'Kurang Memuaskan';
                                                        $arrC['2']++;
                                                    }
                                                }else{
                                                    $keterangan = 'Tidak di Evaluasi';
                                                    $arrC['3']++;
                                                }
                                                ?>
                                        "y" :{{$arrC[3]}},
                                @endif
                            @endforeach
                        @endif
                        @endforeach
                },
                
            ]
        }
    ],
});
</script>
@endsection