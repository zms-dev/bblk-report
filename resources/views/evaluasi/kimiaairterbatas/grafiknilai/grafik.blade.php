@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Parameter Kimia Air Terbatas dengan Nilai yang Sama</div>

                <div class="panel-body">
                  <div id="container"></div>

                  <label>Keterangan :</label>
                  <blockquote style="font-size: 12px">
                    <label style="background-color: #BF0B23">&nbsp; &nbsp;</label> < 2 SD / > 2 SD<br>
                    <label style="background-color: #1bbd20">&nbsp; &nbsp;</label> Median<br>
                    <label > &nbsp;*</label> Posisi Peserta<br>
                  </blockquote>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Nilai yang Sama dengan Parameter {{$parameter->nama_parameter}}'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
      title: {
        text : 'Jumlah Peserta'
      }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y} {point.a}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
    },

    "series": [
        {
            "name": "Nilai yang Sama",
            "colorByPoint": false,
            "data": [
              @foreach($ring as $ring)
                {
                  "name": "< {{$ring->ring1}}",
                  <?php $no = 0; ?>
                  @foreach($data as $val)
                    @if($val->hasil_pemeriksaan < $ring->ring1)
                      <?php $no++; ?>
                      @if(count($val->peserta))
                        "a": "*",
                      @break
                      @endif
                    @endif
                  @endforeach
                  "color": '#BF0B23',
                  "y": {{$no}},
                },
                {
                    "name": ">= {{$ring->ring1}} < {{$ring->ring2}}",
                    <?php $no = 0; ?>
                    @foreach($data as $val)
                      @if($val->hasil_pemeriksaan >= $ring->ring1 && $val->hasil_pemeriksaan < $ring->ring2)
                      <?php $no++; ?>
                        @if(count($val->peserta))
                          "a": "*",
                        @break
                        @endif
                      @endif
                    @endforeach
                    @if($median->median >= $ring->ring1 && $median->median < $ring->ring2)
                      "color": '#1bbd20', 
                    @endif
                    "y": {{$no}},
                },
                {
                    "name": ">= {{$ring->ring2}} < {{$ring->ring3}}",
                    <?php $no = 0; ?>
                    @foreach($data as $val)
                      @if($val->hasil_pemeriksaan >= $ring->ring2 && $val->hasil_pemeriksaan < $ring->ring3)
                      <?php $no++; ?>
                        @if(count($val->peserta))
                          "a": "*",
                        @break
                        @endif
                      @endif
                    @endforeach
                    @if($median->median >= $ring->ring2 && $median->median < $ring->ring3)
                      "color": '#1bbd20', 
                    @endif
                    "y": {{$no}},
                },
                {
                    "name": ">= {{$ring->ring3}} < {{$ring->ring4}}",
                    <?php $no = 0; ?>
                    @foreach($data as $val)
                      @if($val->hasil_pemeriksaan >= $ring->ring3 && $val->hasil_pemeriksaan < $ring->ring4)
                      <?php $no++; ?>
                        @if(count($val->peserta))
                          "a": "*",
                        @break
                        @endif
                      @endif
                    @endforeach
                    @if($median->median >= $ring->ring3 && $median->median < $ring->ring4)
                      "color": '#1bbd20', 
                    @endif
                    "y": {{$no}},
                },
                {
                    "name": ">= {{$ring->ring4}} < {{$ring->ring5}}",
                    <?php $no = 0; ?>
                    @foreach($data as $val)
                      @if($val->hasil_pemeriksaan >= $ring->ring4 && $val->hasil_pemeriksaan < $ring->ring5)
                      <?php $no++; ?>
                        @if(count($val->peserta))
                          "a": "*",
                        @break
                        @endif
                      @endif
                    @endforeach
                    @if($median->median >= $ring->ring4 && $median->median < $ring->ring5)
                      "color": '#1bbd20', 
                    @endif
                    "y": {{$no}}, 
                },
                {
                    "name": "> {{$ring->ring6}}",
                    <?php $no = 0; ?>
                    @foreach($data as $val)
                      @if($val->hasil_pemeriksaan >= $ring->ring6)
                      <?php $no++; ?>
                        @if(count($val->peserta))
                          "a": "*",
                        @break
                        @endif
                      @endif
                    @endforeach
                    "color": '#BF0B23', 
                    "y": {{$no}},
                },
              @endforeach
            ]
        }
    ],
});
</script>
@endsection