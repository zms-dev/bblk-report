<style type="text/css">
@page {
margin: 150px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #ddd;
}

#header { 
    position: fixed; 
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer { 
    position: fixed; 
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px"> 
                </th>
                <td width="100%" style="padding-left: 20px">
                    <span style="font-size: 12px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 10px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL KIMIA AIR TERBATAS SIKLUS {{$siklus}} TAHUN {{$date}}</b></span><br>
                    <span style="font-size: 10px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Jakarta</b></span>
                    <div style="padding-left: 75px; margin-top: -15px; font-size: 10px">
                        <span><br/>Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560<br>Telp. (021) 4212524, 42804339 Facsimile (021) 4245516<br>Website : bblkjakarta.com ; Surat Elektronik : bblkjakarta@yahoo.co.id</span>
                    </div>
                </td>
                <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" height="90px">
                </th>
            </tr>
            <tr>
                <th colspan="3"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<div id="footer">
    EVALUASI PNPME KIMIA AIR TERBATAS SIKLUS {{$siklus}} TAHUN {{$date}}
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                    <label>
                        <center>
                            <font size="10px">
                                <b>HASIL AKHIR EVALUASI KIMIA AIR TERBATAS SIKLUS {{$siklus}} TAHUN {{$date}}</b><br>
                                KODE PESERTA : {{$datas->kode_lab}}
                            </font>
                        </center>
                    </label><br>
              <table class="table utama" >
                        <thead>
                            <tr>
                                <th>Kode Lab</th>
                                <th>Parameter</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Alat & Merek</th>
                                <th>Metode Pengujian</th>
                                <th>Hasil Pengujian</th>
                                <th>Z-Score</th>
                                @if(count($zscore))
                                <th>Keterangan</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $val)
                            <tr>
                                <td>{{$val->kode_lab}}</td>
                                <td>
                                    {{$val->nama_parameter}}
                                    <input type="hidden" name="parameter[]" value="{{$val->id}}">
                                </td>
                                <td>{{date('d/m/Y', strtotime($val->tanggal_mulai))}}</td>
                                <td>{{date('d/m/Y', strtotime($val->tanggal_selesai))}}</td>
                                <td>{{$val->alat}}</td>
                                <td>{{$val->kode_metode_pemeriksaan}}</td>
                                <td>{{$val->hasil_pemeriksaan}}</td>
                                <td>
                                    @if(count($zscore))
                                        @foreach($zscore as $za)
                                            @if($za->parameter == $val->id)
                                                {{$za->zscore}}
                                            @endif
                                        @endforeach
                                    @else
                                    {{$z}}
                                    <input type="hidden" name="zscore[]" value="{{$z}}">
                                    @endif
                                </td>
                                @if(count($zscore))
                                    @foreach($zscore as $za)
                                        @if($za->parameter == $val->id)
                                            <td>
                                                <?php 
                                                if ($za->zscore != 'Tidak di Evaluasi') {
                                                    if($za->zscore <= 2 && $za->zscore >= -2){
                                                        $keterangan = 'Memuaskan';
                                                    }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                        $keterangan = 'Peringatan';
                                                    }else{
                                                        $keterangan = 'Tidak Memuaskan';
                                                    }
                                                }else{
                                                    $keterangan = 'Tidak di Evaluasi';
                                                }
                                                ?>
                                                {{$keterangan}}
                                            </td>
                                        @endif
                                    @endforeach
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                 <table width="100%">
                    <tr>
                        <th width="65%">Keterangan :</th>
                        <td>Jakarta, 31 Juli 2018</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td width="20%">Manajer Teknis</td>
                    </tr>
                    <tr>
                        <td style="padding-left: 0px" rowspan="3">
                            <ul>
                                <li><span style="width:80px;display:inline-block;"><b>|</b> Z Score |<span style="font-family: DejaVu Sans; sans-serif;">&le;</span> 2</span> = Memuaskan</li>
                                <li><span style="width:80px;display:inline-block;">2 &lt;<b>|</b> Z Score|<span style="font-family: DejaVu Sans; sans-serif;">&le;</span> 3</span> = Meragukan</li>
                                <li><span style="width:80px;display:inline-block;"><b>|</b> Z Score |&gt; 3</span> = Kurang Memuaskan</li>
                            </ul>
                        </td>

                    </tr>
                    <tr>
                        <td><img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'TTD_Kimia.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'TTD_Kimia.png')}}" height="90px" style="position: absolute; top: -6%;"></td>
                    </tr>
                    <tr>
                        <td>Mochammad Feri Hadiyanto, S.Si<br>NIP. 198102112009121004</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>