@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
              <div class="panel-heading">Grafik Nilai yang Sama</div>
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                <div class="panel-body">
                  
                  <div>
                      <label for="exampleInputEmail1">Tahun</label>
                      <input type="text" value="{{$tahun}}" readonly class="form-control coba" name="tahun" id="tahun" required>
                  </div>
                  <div>
                      <label for="exampleInputEmail1">Type</label>
                      <input type="text" value="{{$type}}" readonly class="form-control coba" name="type" id="type" required>
                  </div>
                  <div>
                      <label for="exampleInputEmail1">Siklus</label>
                      <input type="text" value="{{$siklus}}" readonly class="form-control coba" name="siklus" id="siklus" required>
                  </div><br>
                  <input type="submit" name="proses" class="btn btn-primary" value="Proses">
                  {{ csrf_field() }}
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
@endsection