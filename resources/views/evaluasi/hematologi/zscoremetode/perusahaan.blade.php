@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Hitung Z-Score Metode</div>
                <div class="panel-body">
                    <div class="well well-sm" hidden="">
                @if($showAllertPending)
                        <div class="alert alert-info">System masih melakukan proses evaluasi..</div><br>
                    @endif
                    @if($showAllertSuccess)
                        <div class="alert alert-success">System telah selesai melakukan proses evaluasi..</div><br>
                    @endif
                    @if($showForm)
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('hasil-pemeriksaan/hematologi/evaluasi-metode/auto-save/data')}}">
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="exampleInputEmail1">Siklus</label>
                                <select class="form-control coba" name="siklus" id="siklus" required>
                                    <option></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label for="exampleInputEmail1">Type</label>
                                <select class="form-control coba" name="type" id="type" required>
                                    <option></option>
                                    <option value="a">A</option>
                                    <option value="b">B</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label for="exampleInputEmail1">Tahun</label>
                                <select class="form-control coba" name="date" id="date">
                                    <?php 
                                        for ($i=date('Y'); $i >= 2018 ; $i--) {
                                    ?>
                                    <option value="{{$i}}">{{$i}}</option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div><br>
                        <input type="submit" name="simpan" class="btn btn-primary" value="Proses">
                        {{ csrf_field() }}
                    </form>
                    @endif
                    </div>
                    <!-- <br>
                        <blockquote style="font-size: 12px">
                            <button type="button" class="btn-primary" style="border: none;" disabled>&nbsp;</button> : Data belum di Evaluasi &nbsp;
                            <button type="button" class="btn-info" style="border: none;" disabled>&nbsp;</button> : Data Sudah di Evaluasi <br><br>
                        </blockquote>
                    <br> -->
                    <div class="row">
                        <div class="col-md-4">
                            <label for="exampleInputEmail1">Tahun</label>
                            <div class="controls input-append date " data-link-field="dtp_input1">
                                <input size="16" type="text" value="" readonly class="form-control form_datetime" name="tahun" id="tahun">
                                <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Siklus </label>
                            <select name="siklus" class="form-control" id="siklus1">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                        </div><br>
                        <div class="col-md-4">
                            <label></label>
                            <button class="btn btn-primary" name="" id="cari">Cari</button><br><br>
                        </div><br>
                    </div>
                    <div class="well well-sm">
                        Keterangan Warna Status :<br>
                        <p><div class="status-danger" style="width: 20px; float: left;">&nbsp;</div> &nbsp; = Status belum selesai</p>
                        <p><div class="status-success" style="width: 20px; float: left;">&nbsp;</div> &nbsp; = Status Sudah selesai</p>
                    </div>
                    <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                        <tr>
                            <th><center>Aksi</center></th>
                            <th><center>No</center></th>
                            <th><center>Kode Lebpes</center></th>
                            <th><center>Instansi</center></th>
                            <th><center>Alamat</center></th>
                            <th><center>Status A</center></th>
                            <th><center>Status B</center></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    });
    </script>
@endif
@stop

@section('scriptBlock')
<script>
        $("#cari").click(function(){
         var siklus = $('#siklus1').val();
        var tahun = $('#tahun').val();
        var table = $(".dataTables-data");
        var dataTable = table.DataTable({
        "bDestroy":true,
        responsive:!0,
        "serverSide":true,
        "processing":true,
        "ajax":{
            url : "{{url('/evaluasi/hitung-zscore-metode/hematologi').'?siklus='}}"+siklus+"{{'&tahun='}}"+tahun,
        },
        dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
        language:{
            paginate:{
                previous:"&laquo;",
                next:"&raquo;"
            },search:"_INPUT_",
            searchPlaceholder:"Search..."
        },
        "columns":[
                {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
                {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
                {"data":"kode_lebpes","name":"kode_lebpes","searchable":true,"orderable":true,"width":"20%"},
                {"data":"nama_lab","name":"nama_lab","searchable":true,"orderable":true},
                {"data":"alamat","name":"alamat","searchable":true,"orderable":true},
                {"data":"posisia","name":"posisia","searchable":false,"orderable":false,"width" : "100px"},
                {"data":"posisib","name":"posisib","searchable":false,"orderable":false,"width" : "100px"},
        ],
        order:[[1,"asc"]]
    });
});
  $(".form_datetime").datetimepicker({
        format: "yyyy",
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-right",
        minView: 4,
        startView: 4
    });
</script>
@stop