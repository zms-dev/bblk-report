@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Z-Score Peserta per Parameter Hematologi</div>
                <a class="btn btn-primary" style="margin: 10px" onclick="javascript:printDiv('datana')">Cetak Grafik <li class="glyphicon glyphicon-print"></li></a>
                <div class="panel-body" id="datana">
                  @foreach($alatmetode as $val)
                  <table width="90%;">
                    <tr>
                      <td><div id="container{{$val->id}}" ></div></td>
                      <td><div id="container_metode{{$val->id}}" ></div></td>
                    </tr>
                  </table>
                  @endforeach
                  <blockquote style="font-size: 14px">
                     <style type="text/css">
                      @media print{          
                          #color-red{
                              -webkit-print-color-adjust: exact!important;
                              background: #1bbd20 !important;display:inline-block;width:15px;height:25px;
                          }

                      }
                    </style>
                    <table>
                      <tr>
                        <td>Catatan : &nbsp;</td>
                        <td>- | Z Score | <= 2 </td>
                        <td>&nbsp; = Memuaskan</td>
                        <td width="20%" id="color-red" style="text-align: right;">&nbsp;<kbd style="background-color: #67ca3b;"></kbd>&nbsp;</td>
                        <td>&nbsp;Nilai Target</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td>- 2 < | Z Score | <= 3 </td>
                        <td>&nbsp; = Peringatan</td>
                        <td style="text-align: right; font-size: 30px;">*&nbsp;</td>
                        <td>&nbsp;Posisi Peserta</td>
                      </tr>
                      <tr>
                        <td></td>
                        <td>- | Z Score | >= 3 </td>
                        <td>&nbsp; = Tidak Memuaskan</td>
                        <td></td>
                        <td></td>
                      </tr>
                    </table>
                  </blockquote>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.highcharts.com/modules/pattern-fill.js"></script>
<script type="text/javascript">
  function printDiv(divID) {
    //Get the HTML of div
    var divElements = document.getElementById(divID).innerHTML;
    //Get the HTML of whole page
    var oldPage = document.body.innerHTML;

    //Reset the page's HTML with div's HTML only
    document.body.innerHTML = 
      "<html><head><title></title></head><body>" + 
      divElements + "</body>";

    //Print Page
    window.print();

    //Restore orignal HTML
    document.body.innerHTML = oldPage;

  
}
@foreach($alatmetode as $val1)
var chart = Highcharts.chart('container{{$val1->id}}', {
   @foreach($val1->data as $val)
  title: {
    text: 'Grafik Z-Score Peserta per Parameter <br>{!!$val1->nama_parameter!!}',
    style : {
        'font-size' : "10px",
        'font-weight' : "bold",
    }
  },
  @break
  @endforeach
  subtitle: {
    text: ''
  },
  xAxis: {categories: [
      @foreach($val1->data as $val)
      '{{$val->nilainya}}',
      @endforeach]},
  yAxis: {
    title: {
      text : 'Jumlah Peserta',
    }
  },
  plotOptions: {
      series: {
          borderWidth: 0,dataLabels: {
              enabled: true,
              format:'{point.y} {point.a}'
          }
      }
  },
  series: [{type: 'column',
      name : 'Z-score',
      data: [
      @foreach($val1->data as $val)
          {
          y: {{$val->nilai1}},
          <?php if($val->nilainya == '0.0') { ?>
                color: '#67ca3b',
          <?php } ?>
         
          @foreach($val1->datap as $valu)
            <?php if($valu->nilainya == $val->nilainya) { ?>
              <?php if($valu->nilai1 == '1') { ?>
                a: "*",
              <?php } ?>
            <?php } ?>
          @endforeach
          },
      @endforeach
      ],
      showInLegend: true}]
});
@endforeach
@foreach($alatmetode as $val1)

var chart = Highcharts.chart('container_metode{{$val1->id}}', {
  @foreach($val1->data as $val)
  title: {
    text: 'Grafik Z-Score Peserta {!!$val1->nama_parameter!!} <br> Metode {!!$val1->metode_pemeriksaan!!} ',
    style : {
        'font-size' : "10px",
        'font-weight' : "bold",
    }
  },
  @break
  @endforeach
  subtitle: {text: ''},
  xAxis: {categories: [
      @foreach($val1->data as $val)
      '{{$val->nilainya}}',
      @endforeach]},
  yAxis: {
    title: {
      text : 'Jumlah Peserta'
    }
  },
  plotOptions: {
      series: {
          borderWidth: 0,dataLabels: {
              enabled: true,
              format:'{point.y} {point.a}'
          }
      }
  },
  series: [{type: 'column',
      name : 'Z-score',
      data: [
      @foreach($val1->data_metode as $val)
          {
          y: {{$val->nilai1}},
          <?php if($val->nilainya == '0.0') { ?>
            color: '#67ca3b',
          <?php } ?>
         
          @foreach($val1->datap_metode as $valu)
            <?php if($valu->nilainya == $val->nilainya) { ?>
              <?php if($valu->nilai1 == '1') { ?>
                a: "*",
              <?php } ?>
            <?php } ?>
          @endforeach
          },
      @endforeach
      ],
      showInLegend: true}]
});
@endforeach
</script>
@endsection
