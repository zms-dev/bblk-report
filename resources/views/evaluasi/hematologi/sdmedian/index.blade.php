@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Input Nilai SD dan Median Per Parameter (Hematologi)</div>
                <div class="panel-body">
                @if(Session::has('message'))
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                  </div>
                @endif
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Parameter</label>
                              <select class="form-control" name="parameter">
                                <option></option>
                                @foreach($parameter as $val)
                                <option value="{{$val->id}}">{!!$val->nama_parameter!!}</option>
                                @endforeach
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Siklus</label>
                              <select class="form-control" name="siklus">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Tahun</label>
                              <select class="form-control" name="tahun">
                                <option></option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                              </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Type</label>
                              <select class="form-control" name="tipe">
                                <option></option>
                                <option value="a">A</option>
                                <option value="b">B</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">SD</label>
                              <input type="text" name="sd" class="form-control">
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Median</label>
                              <input type="text" name="median" class="form-control">
                          </div>
                        </div>
                      </div>
                      <br>
                    <input type="submit" name="proses" class="btn btn-primary" value="Simpan">
                    {{ csrf_field() }}
                    </form>
                    <br>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Parameter</th>
                          <th>SD</th>
                          <th>Median</th>
                          <th>Tahun</th>
                          <th>Siklus</th>
                          <th>Tipe</th>
                          <th colspan="2">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($sd as $val)
                        <tr>
                          <td>{!!$val->nama_parameter!!}</td>
                          <td>{{$val->sd}}</td>
                          <td>{{$val->median}}</td>
                          <td>{{$val->tahun}}</td>
                          <td>{{$val->siklus}}</td>
                          <td>{{$val->tipe}}</td>
                          <td>
                              <a href="{{URL('evaluasi/input-sd-median/hematologi/edit').'/'.$val->id}}" style="float: left; margin-bottom: 5px">
                                <button class="btn btn-primary btn-xs">
                                  <i class="glyphicon glyphicon-pencil"></i>
                                </button>
                              </a> &nbsp;
                              <a href="{{URL('evaluasi/input-sd-median/hematologi/delete').'/'.$val->id}}">
                                <button class="btn btn-danger btn-xs">
                                  <i class="glyphicon glyphicon-trash"></i>
                                </button>
                              </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $sd->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection