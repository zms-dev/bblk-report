<table border="1">
    <thead>
        <tr>
            <th rowspan="2">Nama Instansi</th>
            <th rowspan="2">Kode Lab</th>
            @foreach($parameterna as $val)
            <th colspan="3">{!!$val->nama_parameter!!}</th>
            @endforeach
            <th rowspan="2">Catatan</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            @foreach($parameterna as $val)
            <th><center>Hasil</center></th>
            <th><center>Metode</center></th>
            <th><center>Alat</center></th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($data as $val)
        <tr>
            <td>{{$val->nama_lab}}</td>
            <td>{{$val->kode_lab}}</td>
            @foreach($val->parameter as $par)
                @foreach($par->detail as $hasil)
                    <td>'{{$hasil->hasil_pemeriksaan}}</td>
                    <td>{!!$hasil->metode_pemeriksaan!!} @if($hasil->metode_pemeriksaan == 'Metode lain :') {{$hasil->metode_lain}} @endif</td>
                    <td>@if($hasil->instrumen == NULL) {!!$hasil->alat!!} {{$hasil->instrument_lain}} @else {!!$hasil->instrumen!!} {{$hasil->instrument_lain}} @endif</td>
                @endforeach
            @endforeach
            <td>{{$val->catatan}}</td>
        </tr>
        @endforeach
    </tbody>
</table>