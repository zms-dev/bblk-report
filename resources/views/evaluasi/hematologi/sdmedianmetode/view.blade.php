@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Input Nilai SD dan Median Per Metode (Hematologi)</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Siklus</label>
                              <select class="form-control" name="siklus" required>
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Tahun</label>
                              <select class="form-control" name="tahun" required>
                                <option></option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Type</label>
                              <select class="form-control" name="tipe" required>
                                <option></option>
                                <option value="a">A</option>
                                <option value="b">B</option>
                              </select>
                          </div>
                        </div>
                      </div>
                      <br>
                    <table class="table table-bordered">
                      <tr>
                        <th>Parameter</th>
                        <th>Metode</th>
                        <th>SD</th>
                        <th>Median</th>
                      </tr>
                      @foreach($metode as $val)
                      <tr>
                        <td>
                          <input type="hidden" name="parameter[]" value="{{$val->parameter_id}}">{!!$val->nama_parameter!!}
                        </td>
                        <td>
                          <input type="hidden" name="metode[]" value="{{$val->id}}">{!!$val->metode_pemeriksaan!!}
                        </td>
                        <td><input type="text" name="sd[]" class="form-control"></td>
                        <td><input type="text" name="median[]" class="form-control"></td>
                      </tr>
                      @endforeach
                    </table>
                    <input type="submit" name="proses" class="btn btn-primary" value="Simpan">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection