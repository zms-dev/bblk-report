@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Input Nilai SD dan Median per Metode (Hematologi)</div>
                <div class="panel-body">
                @if(Session::has('message'))
                  <div class="alert {{ Session::get('alert-class') }}" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                  </div>
                @endif
                    <form class="form-horizontal" method="get" enctype="multipart/form-data" action="{{URL('evaluasi/input-sd-median-metode/hematologi/insert')}}">
                      <input type="submit" name="proses" class="btn btn-primary" value="Tambah">
                    {{ csrf_field() }}
                    </form>
                    <br>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Tahun</th>
                          <th>Siklus</th>
                          <th>Tipe</th>
                          <th width="20%">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($sd as $val)
                        <tr>
                          <td>{{$val->tahun}}</td>
                          <td>{{$val->siklus}}</td>
                          <td>@if($val->tipe == 'a')A @else B @endif</td>
                          <td>
                              <a href="{{URL('evaluasi/input-sd-median-metode/hematologi/edit').'/'.$val->id}}" style="float: left; margin-bottom: 5px">
                                <button class="btn btn-primary btn-xs">
                                  <i class="glyphicon glyphicon-pencil"></i>
                                </button>
                              </a> &nbsp;
                              <a href="{{URL('evaluasi/input-sd-median-metode/hematologi/delete').'/'.$val->id}}">
                                <button class="btn btn-danger btn-xs">
                                  <i class="glyphicon glyphicon-trash"></i>
                                </button>
                              </a> &nbsp;
                              <a href="{{URL('evaluasi/input-sd-median-metode/hematologi/print').'/'.$val->id}}" target="_blank">
                                <button class="btn btn-success btn-xs">
                                  <i class="glyphicon glyphicon-print"></i>
                                </button>
                              </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $sd->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection