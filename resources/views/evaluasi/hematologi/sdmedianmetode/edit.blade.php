@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Nilai SD dan Median Per Metode (Hematologi)</div>
                <div class="panel-body">
                @if(Session::has('message'))
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                  </div>
                @endif
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Siklus</label>
                              <select class="form-control" name="siklus" required>
                                <option value=""></option>
                                <option value="1" {{ ($edit->siklus == '1') ? 'selected' : '' }}>1</option>
                                <option value="2" {{ ($edit->siklus == '2') ? 'selected' : '' }}>2</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Tahun</label>
                              <select class="form-control" name="tahun" required>
                                <option value="{{$edit->tahun}}">{{$edit->tahun}}</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Tipe</label>
                              <select class="form-control" name="tipe" required>
                                <option value=""></option>
                                <option value="a" {{ ($edit->tipe == 'a') ? 'selected' : '' }}>01</option>
                                <option value="b" {{ ($edit->tipe == 'b') ? 'selected' : '' }}>02</option>
                              </select>
                          </div>
                        </div>
                      </div>
                      <br>
                    <input type="hidden" name="parameter" value="{{$edit->parameter}}">
                    <table class="table table-bordered">
                      <tr>
                        <th>Parameter</th>
                        <th>Alat / Instrument</th>
                        <th>SD</th>
                        <th>Median</th>
                      </tr>
                      @foreach($sd as $val)
                      <input type="hidden" name="id[]" value="{{$val->id}}">
                      <tr>
                        <td>
                          <input type="hidden" name="parameter[]" value="{{$val->parameter}}">{!!$val->nama_parameter!!}
                        </td>
                        <td>
                          <input type="hidden" name="metode[]" value="{{$val->metode}}">{{$val->metode_pemeriksaan}}
                        </td>
                        <td><input type="text" name="sd[]" class="form-control" value="{{$val->sd}}"></td>
                        <td><input type="text" name="median[]" class="form-control" value="{{$val->median}}"></td>
                      </tr>
                      @endforeach
                    </table>
                    <input type="submit" name="proses" class="btn btn-primary" value="Update">
                    {{ csrf_field() }}
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection