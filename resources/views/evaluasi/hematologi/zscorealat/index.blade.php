@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Hitung Z-Scroe Alat / Instrument Hematologi</div>

                <div class="panel-body">
                @if(count($zscore))
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('/hasil-pemeriksaan/hematologi/evaluasi-alat')}}/print/{{$id}}?x={{$type}}&y={{$siklus}}" target="_blank">
                @else
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                @endif
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Kode Lab</th>
                                <th>Parameter</th>
                                <th>Instrument</th>
                                <th>Metode Pemeriksaan</th>
                                <th style="text-align: center;">Hasil</th>
                                <th style="text-align: center;">Nilai Target</th>
                                <th style="text-align: center;">Z-Score</th>
                                @if(count($zscore))
                                <th>Keterangan</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $val)
                            <tr>
                                <td>{{$val->kode_lab}}</td>
                                <td>
                                    {!!$val->nama_parameter!!}
                                    <input type="hidden" name="parameter[]" value="{{$val->id_parameter}}">
                                </td>
                                <td>
                                    {{$val->instrumen}} <br> {{$val->instrument_lain}}
                                    <input type="hidden" name="alat[]" value="{{$val->id}}">
                                </td>
                                <td>{!!$val->metode_pemeriksaan!!} <br> {{$val->metode_lain}}</td>
                                <td style="text-align: center;">{{$val->hasil_pemeriksaan}}</td>
                                <td style="text-align: center;">
                                    @if(count($val->sd))
                                    {{$val->sd[0]->median}}
                                    @else
                                    -
                                    @endif
                                </td>
                                <td style="text-align: center;">
                                    <?php
                                        if (count($val->sd)) {
                                            $na = ($val->hasil_pemeriksaan - $val->sd[0]->median) / $val->sd[0]->sd;
                                            $z = number_format($na, 1);
                                        }else{
                                            $z = "Tidak dapat dievaluasi";
                                        }
                                    ?>
                                    @if(count($zscore))
                                        @foreach($zscore as $za)
                                            @if($za->parameter == $val->id_parameter)
                                                @if($za->alat == $val->id)
                                                @if($za->zscore == 'Tidak di analisis')
                                                Tidak di analisis
                                                @else
                                                {{$za->zscore}}
                                                @endif
                                                @endif
                                            @endif
                                        @endforeach
                                    @else
                                        @if($val->hasil_pemeriksaan == NULL)
                                        Tidak di analisis
                                        <input type="hidden" name="zscore[]" value="Tidak di analisis">
                                        @else
                                        {{$z}}
                                        <input type="hidden" name="zscore[]" value="{{$z}}">
                                        @endif
                                    @endif
                                </td>
                                @if(count($zscore))
                                    @foreach($zscore as $za)
                                        @if($za->parameter == $val->id_parameter)
                                            @if($za->alat == $val->id)
                                                <td>
                                                    <?php 
                                                        if($za->zscore == "Tidak dapat dievaluasi"){
                                                            $keterangan = '-';
                                                        }elseif($za->zscore <= 2 && $za->zscore >= -2){
                                                            $keterangan = 'Memuaskan';
                                                        }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                            $keterangan = 'Peringatan';
                                                        }else{
                                                            $keterangan = 'Tidak Memuaskan';
                                                        }
                                                    ?>
                                                    <center>{{$keterangan}}</center>
                                                </td>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @if(count($zscore))
                <input type="submit" name="simpan" class="btn btn-primary" value="Print">
                @else
                <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                @endif
                {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection