<style type="text/css">
@page {
margin: 150px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #ddd;
}

#header { 
    position: fixed; 
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer { 
    position: fixed; 
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
                </th>
                <td width="100%" style="padding-left: 20px">
                    <span style="font-size: 12px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 10px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL HEMATOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}</b></span><br>
                    <span style="font-size: 10px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Jakarta</b></span>
                    <div style="padding-left: 75px; margin-top: -15px; font-size: 10px">
                        <span><br/>Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560<br>Telp. (021) 4212524, 42804339 Facsimile (021) 4245516<br>Website : bblkjakarta.com ; Surat Elektronik : bblkjakarta@yahoo.co.id</span>
                    </div>
                </td>
                <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo-bblk-sby.png')}}" height="90px">
                </th>
            </tr>
            <tr>
                <th colspan="3"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<div id="footer">
    EVALUASI PNPME ALAT / INSTRUMEN HEMATOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                    <label>
                        <center>
                            <font size="10px">
                                <b>HASIL AKHIR EVALUASI ALAT / INSTRUMEN HEMATOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}</b><br><br>
                                KODE PESERTA : {{$datas->kode_lab}}
                            </font>
                        </center>
                    </label><br>
               <table class="table utama">
                        <thead>
                            <tr>
                                <th>Kode Lab</th>
                                <th>Parameter</th>
                                <th>Instrument</th>
                                <th>Metode Pemeriksaan</th>
                                <th style="text-align: center;">Hasil</th>
                                <th style="text-align: center;">Nilai Target</th>
                                <th style="text-align: center;">Z-Score</th>
                                @if(count($zscore))
                                <th>Keterangan</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $val)
                            <tr>
                                <td>{{$val->kode_lab}}</td>
                                <td>
                                    {!!$val->nama_parameter!!}
                                    <input type="hidden" name="parameter[]" value="{{$val->id}}">
                                </td>
                                <td>{{$val->instrumen}}  {{$val->instrument_lain}}</td>
                                <td>{!!$val->metode_pemeriksaan!!}  {{$val->metode_lain}}</td>
                                <td>{{$val->hasil_pemeriksaan}}</td>
                                <td>
                                    @if (count($val->sd)) 
                                        {{$val->sd[0]->median}}
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    <?php
                                        if (count($val->sd)) {
                                            $na = ($val->hasil_pemeriksaan - $val->sd[0]->median) / $val->sd[0]->sd;
                                            $z = number_format($na, 1);
                                        }else{
                                            $z = "Tidak dapat dievaluasi";
                                        }
                                    ?>
                                    @if(count($zscore))
                                        @foreach($zscore as $za)
                                            @if($za->parameter == $val->id_parameter)
                                                @if($za->alat == $val->id)
                                                @if($za->zscore == 'Tidak di analisis')
                                                Tidak di analisis
                                                @else
                                                {{$za->zscore}}
                                                @endif
                                                @endif
                                            @endif
                                        @endforeach
                                    @else
                                        @if($val->hasil_pemeriksaan == NULL)
                                        Tidak di analisis
                                        <input type="hidden" name="zscore[]" value="Tidak di analisis">
                                        @else
                                        {{$z}}
                                        <input type="hidden" name="zscore[]" value="{{$z}}">
                                        @endif
                                    @endif
                                </td>
                                @if(count($zscore))
                                    @foreach($zscore as $za)
                                        @if($za->parameter == $val->id_parameter)
                                            @if($za->alat == $val->id)
                                                <td>
                                                    <?php 
                                                        if($za->zscore == "Tidak dapat dievaluasi"){
                                                            $keterangan = '-';
                                                        }elseif($za->zscore <= 2 && $za->zscore >= -2){
                                                            $keterangan = 'Memuaskan';
                                                        }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                            $keterangan = 'Peringatan';
                                                        }else{
                                                            $keterangan = 'Tidak Memuaskan';
                                                        }
                                                    ?>
                                                    <center>{{$keterangan}}</center>
                                                </td>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table><br>
                <p><b>Keterangan :</b></p><br>
                    <p style="position: relative; left: 3%;">
                        Kriteria Skor<br>
                        > 3,00 = Sangat Baik<br>
                        > 2,00 – 3,00 = Baik<br>
                        > 1,00 – 2,00 = Kurang<br>
                        <= 1,00 = Buruk
                    </p>

                    <div style="position: relative; left: 81%; top: -15%;">
                    <p>
                        <div style="position: relative; top: 22px">
                        Jakarta, 31 Juli 2018<br>
                        Manajer Teknis
                        </div>
                        <p>
                            <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'ttd_martini.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'ttd_martini.png')}}" width="80" height="80" style="margin-left: 20px !important;">
                        </p>

                        <div style="position: relative; top:-20px">
                        Martini,S,Si<br>
                        NIP 196609141992032002
                        </div>
                    </p>
                    </div>
            </div>
        </div>
    </div>
</div>