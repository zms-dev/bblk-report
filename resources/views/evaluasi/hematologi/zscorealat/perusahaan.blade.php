@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Data Perusahaan Hematologi</div>
                <div class="panel-body">
                    @if($showAllertPending)
                        <div class="alert alert-info">System masih melakukan proses evaluasi..</div><br>
                    @endif
                    @if($showAllertSuccess)
                        <div class="alert alert-success">System telah selesai melakukan proses evaluasi..</div><br>
                    @endif
                    @if($showForm)
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('hasil-pemeriksaan/hematologi/evaluasi-alat/auto-save/data')}}">
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="exampleInputEmail1">Siklus</label>
                                <select class="form-control coba" name="siklus" id="siklus" required>
                                    <option></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label for="exampleInputEmail1">Type</label>
                                <select class="form-control coba" name="type" id="type" required>
                                    <option></option>
                                    <option value="a">A</option>
                                    <option value="b">B</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label for="exampleInputEmail1">Tahun</label>
                                <select class="form-control coba" name="date" id="date">
                                    <?php 
                                        for ($i=date('Y'); $i >= 2018 ; $i--) {
                                    ?>
                                    <option value="{{$i}}">{{$i}}</option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div><br>
                        <input type="submit" name="simpan" class="btn btn-primary" value="Proses">
                        {{ csrf_field() }}
                    </form>
                    @endif
                    <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                        <tr>
                            <th><center>Aksi</center></th>
                            <th>No</th>
                            <th><center>Perusahaan</center></th>
                            <th>Alamat</th>
                            <th>Kode lebpes</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    });
    </script>
@endif
@stop

@section('scriptBlock')
<script>
        var table = $(".dataTables-data");
        var dataTable = table.DataTable({
        responsive:!0,
        "serverSide":true,
        "processing":true,
        "ajax":{
            url : "{{url('/evaluasi/hitung-zscore/hematologi')}}"
        },
        dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
        language:{
            paginate:{
                previous:"&laquo;",
                next:"&raquo;"
            },search:"_INPUT_",
            searchPlaceholder:"Search..."
        },
        "columns":[
                {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
                {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
                {"data":"nama_lab","name":"nama_lab","searchable":true,"orderable":true},
                {"data":"alamat","name":"alamat","searchable":true,"orderable":true},
                 {"data":"kode_lebpes","name":"kode_lebpes","searchable":true,"orderable":true,"visible":false},
        ],
        order:[[1,"asc"]]
    });

</script>
@stop