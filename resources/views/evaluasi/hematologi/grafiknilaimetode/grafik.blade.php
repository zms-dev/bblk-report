@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Metode Hematologi dengan Nilai yang Sama</div>

                <div class="panel-body">
                  <label>Keterangan :</label>
                  <blockquote style="font-size: 12px">
                    <label style="background-color: #BF0B23">&nbsp; &nbsp;</label> < 2 SD / > 2 SD<br>
                    <label style="background-color: #1bbd20">&nbsp; &nbsp;</label> Median<br>
                    <label > &nbsp;*</label> Posisi Peserta<br>
                  </blockquote>
                  @foreach($datapes as $val)
                  <div class="col-md-6">
                    <div id="container{{$val->parameter_id}}"></div>
                  </div>
                  @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
@foreach($datapes as $val)
// Create the chart
Highcharts.chart('container{{$val->parameter_id}}', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Parameter {!!$val->nama_parameter!!}<br>Metode {{$val->metode_pemeriksaan}}<br> dengan Nilai yang Sama'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
      title: {
        text : 'Jumlah Peserta'
      }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y} {point.a}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>= <b>{point.y}</b><br/>'
    },

    "series": [
        {
            "name": "Nilai yang Sama",
            "colorByPoint": false,
            "data": [
              @foreach($val->ring as $ring)
                {
                  "name": "< {{$ring->ring1}}",
                  <?php $no = 0; ?>
                  @foreach($val->data as $valu)
                    @if($valu->hasil_pemeriksaan < $ring->ring1)
                      <?php $no++; ?>
                      @if(count($valu->peserta))
                        "a": "*",
                      @break
                      @endif
                    @endif
                  @endforeach
                  "color": '#BF0B23',
                  "y": {{$no}},
                },
                {
                    "name": ">= {{$ring->ring1}} < {{$ring->ring2}}",
                    <?php $no = 0; ?>
                    @foreach($val->data as $valu)
                      @if($valu->hasil_pemeriksaan >= $ring->ring1 && $valu->hasil_pemeriksaan < $ring->ring2)
                      <?php $no++; ?>
                        @if(count($valu->peserta))
                          "a": "*",
                        @break
                        @endif
                      @endif
                    @endforeach
                    @if($val->median->median >= $ring->ring1 && $val->median->median < $ring->ring2)
                      "color": '#1bbd20', 
                    @endif
                    "y": {{$no}},
                },
                {
                    "name": ">= {{$ring->ring2}} < {{$ring->ring3}}",
                    <?php $no = 0; ?>
                    @foreach($val->data as $valu)
                      @if($valu->hasil_pemeriksaan >= $ring->ring2 && $valu->hasil_pemeriksaan < $ring->ring3)
                      <?php $no++; ?>
                        @if(count($valu->peserta))
                          "a": "*",
                        @break
                        @endif
                      @endif
                    @endforeach
                    @if($val->median->median >= $ring->ring2 && $val->median->median < $ring->ring3)
                      "color": '#1bbd20', 
                    @endif
                    "y": {{$no}},
                },
                {
                    "name": ">= {{$ring->ring3}} < {{$ring->ring4}}",
                    <?php $no = 0; ?>
                    @foreach($val->data as $valu)
                      @if($valu->hasil_pemeriksaan >= $ring->ring3 && $valu->hasil_pemeriksaan < $ring->ring4)
                      <?php $no++; ?>
                        @if(count($valu->peserta))
                          "a": "*",
                        @break
                        @endif
                      @endif
                    @endforeach
                    @if($val->median->median >= $ring->ring3 && $val->median->median < $ring->ring4)
                      "color": '#1bbd20', 
                    @endif
                    "y": {{$no}},
                },
                {
                    "name": ">= {{$ring->ring4}} < {{$ring->ring5}}",
                    <?php $no = 0; ?>
                    @foreach($val->data as $valu)
                      @if($valu->hasil_pemeriksaan >= $ring->ring4 && $valu->hasil_pemeriksaan < $ring->ring5)
                      <?php $no++; ?>
                        @if(count($valu->peserta))
                          "a": "*",
                        @break
                        @endif
                      @endif
                    @endforeach
                    @if($val->median->median >= $ring->ring4 && $val->median->median < $ring->ring5)
                      "color": '#1bbd20', 
                    @endif
                    "y": {{$no}}, 
                },
                {
                    "name": "> {{$ring->ring6}}",
                    <?php $no = 0; ?>
                    @foreach($val->data as $valu)
                      @if($valu->hasil_pemeriksaan >= $ring->ring6)
                      <?php $no++; ?>
                        @if(count($valu->peserta))
                          "a": "*",
                        @break
                        @endif
                      @endif
                    @endforeach
                    "color": '#BF0B23', 
                    "y": {{$no}},
                },
              @endforeach
            ]
        }
    ],
});
@endforeach
</script>
@endsection