<style type="text/css">
@page {
margin: 165px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #ddd;
}

#header {
    position: fixed;
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer {
    position: fixed;
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<div id="header">
<table width="100%" cellpadding="0" border="0" >
    <thead>
        <tr>
            <th>
                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
            </th>
            <th width="100%" style="text-align: center;">
                <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                <span style="font-size: 12px">PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL HEMATOLOGI TAHUN {{$date}}</span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -7 -25px; margin-left: -42px;">
                Penyelenggara : Balai Besar Laboratorium Kesehatan Jakarta
                Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560
                Telepon : (021) 4212524, 42804339 Fax. (021) 4245516
                Email : bblkjakarta@yahoo.co.id
                </pre>
            </th>
            <th>
                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" height="70px">
            </th>
        </tr>
        <tr>
            <th colspan="3" style="padding: -2px;"><hr></th>
        </tr>
    </thead>
</table>
</div>
<div id="footer">
    EVALUASI PNPME HEMATOLOGI SIKLUS {{$siklus}} TAHUN {{$date}}
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                    <label>
                        <center>
                            <font size="12px">
                                <b>HASIL AKHIR EVALUASI HEMATOLOGI SIKLUS {{$siklus}}-{{$urut}} TAHUN {{$date}}</b><br><br>
                                KODE PESERTA : {{$datas->kode_lab}}
                            </font>
                        </center>
                    </label><br>
               <table class="table utama">
                        <thead>
                            <tr>
                                <th>Kode Lab</th>
                                <th>Parameter</th>
                                <th>Instrument</th>
                                <th>Metode Pemeriksaan</th>
                                <th>Hasil</th>
                                <th>Nilai Target</th>
                                <th>Z-Score</th>
                                @if(count($zscore))
                                <th>Keterangan</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $val)
                            <tr>
                                <td>{{$val->kode_lab}}</td>
                                <td>
                                    {!!$val->nama_parameter!!}
                                    <input type="hidden" name="parameter[]" value="{{$val->id}}">
                                </td>
                                <td>{{$val->alat}}  {{$val->instrument_lain}}</td>
                                <td>{!!$val->metode_pemeriksaan!!}  {{$val->metode_lain}}</td>
                                <td>{{$val->hasil_pemeriksaan}}</td>
                                <td>
                                    @if (count($val->sd))
                                        {{$val->sd[0]->median}}
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    <?php
                                        if (count($val->sd)) {
                                            $z = ($val->hasil_pemeriksaan - $val->sd[0]->median) / $val->sd[0]->sd;
                                        }else{
                                            $z = $val->hasil_pemeriksaan;
                                        }
                                    ?>
                                    @if(count($zscore))
                                        @foreach($zscore as $za)
                                            @if($za->parameter == $val->id)
                                                @if($za->zscore == 'Tidak di analisis')
                                                Tidak di analisis
                                                @else
                                                {{$za->zscore}}
                                                @endif
                                            @endif
                                        @endforeach
                                    @else
                                        @if($val->hasil_pemeriksaan == NULL)
                                        Tidak di analisis
                                        <input type="hidden" name="zscore[]" value="Tidak di analisis">
                                        @else
                                        {{number_format($z, 1)}}
                                        <input type="hidden" name="zscore[]" value="{{number_format($z, 1)}}">
                                        @endif
                                    @endif
                                </td>
                                @if(count($zscore))
                                    @foreach($zscore as $za)
                                        @if($za->parameter == $val->id)
                                            <td>
                                                <?php
                                                    if($za->zscore == 'Tidak di analisis'){
                                                        $keterangan = 'Tidak di analisis';
                                                    }else if($za->zscore <= 2 && $za->zscore >= -2){
                                                        $keterangan = 'Memuaskan';
                                                    }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                        $keterangan = 'Peringatan';
                                                    }else{
                                                        $keterangan = 'Tidak Memuaskan';
                                                    }
                                                ?>
                                                {{$keterangan}}
                                            </td>
                                        @endif
                                    @endforeach
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table><br>
                    <p><b>Keterangan :</b></p><br>
                    <p style="position: relative; left: 3%;">
                        Kriteria Skor<br>
                        > 3,00 = Sangat Baik<br>
                        > 2,00 – 3,00 = Baik<br>
                        > 1,00 – 2,00 = Kurang<br>
                        <= 1,00 = Buruk
                    </p>

                    <div style="position: relative; left: 77%; top: -15%;">
                    <p>
                        <div style="position: relative; top: 22px">
                        Jakarta, @if(count($evaluasi->tanggal)){{ _dateIndo($evaluasi->tanggal) }}@endif<br>
                        Manajer Teknis
                        </div>
                        <p>
                            <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'imunologi_ttd.png')}}" width="160" style="margin: -40px 0px !important;">
                        </p>

                        <div style="position: relative; top:-20px">
                        dr. Siti Kurnia Eka Rusmiarti, Sp.PK
                        </div>
                    </p>
                    </div>
            </div>
        </div>
    </div>
</div>
