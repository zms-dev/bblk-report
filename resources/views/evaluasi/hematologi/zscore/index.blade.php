@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hitung Z-Score Hematologi</div>

                <div class="panel-body">

                @if(count($zscore))
                <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('/hasil-pemeriksaan/hematologi/evaluasi')}}/print/{{$id}}?x={{$type}}&y={{$siklus}}" target="_blank">
                    
                @else
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                @endif
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Kode Peserta</th>
                                <th>Parameter</th>
                                <th>Alat</th>
                                <th>Metode Pemeriksaan</th>
                                <th style="text-align: center;">Hasil</th>
                                <th style="text-align: center;">Nilai Target</th>
                                <th style="text-align: center;">Z-Score</th>
                                @if(count($zscore))
                                <th>Keterangan</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $val)
                            <tr>
                                <td>{{$val->kode_lab}}</td>
                                <td>
                                    {!!$val->nama_parameter!!}
                                    <input type="hidden" name="parameter[]" value="{{$val->id}}">
                                </td>
                                <td>{{$val->alat}} <br> {{$val->instrument_lain}}</td>
                                <td>{!!$val->metode_pemeriksaan!!} <br> {{$val->metode_lain}}</td>
                                <td style="text-align: center;">{{$val->hasil_pemeriksaan}}</td>
                                <td style="text-align: center;">
                                    @if(count($val->sd))
                                    {{$val->sd[0]->median}}
                                    @else
                                    -
                                    @endif
                                </td>
                                <td style="text-align: center;">
                                    <?php
                                        $val->hasil_pemeriksaan = ($val->hasil_pemeriksaan == "-" ? NULL : $val->hasil_pemeriksaan);
                                        if (count($val->sd) && $val->hasil_pemeriksaan != NULL) {
                                            $z = ($val->hasil_pemeriksaan - $val->sd[0]->median) / $val->sd[0]->sd; 
                                        }else{
                                            $z = $val->hasil_pemeriksaan;
                                        }
                                    ?>
                                    @if(count($zscore))
                                        @foreach($zscore as $za)
                                            @if($za->parameter == $val->id)
                                                @if($za->zscore == 'Tidak di analisis')
                                                Tidak di analisis
                                                @else
                                                {{$za->zscore}}
                                                @endif
                                            @endif
                                        @endforeach
                                    @else
                                        @if($val->hasil_pemeriksaan == NULL)
                                        Tidak di analisis
                                        <input type="hidden" name="zscore[]" value="Tidak di analisis">
                                        @else
                                        {{number_format($z, 1)}}
                                        <input type="hidden" name="zscore[]" value="{{number_format($z, 1)}}">
                                        @endif
                                    @endif
                                </td>
                                @if(count($zscore))
                                    @foreach($zscore as $za)
                                        @if($za->parameter == $val->id)
                                            <td>
                                                <?php 
                                                    if($za->zscore == 'Tidak di a'){
                                                        $keterangan = 'Tidak di analisis';
                                                    }else if($za->zscore <= 2 && $za->zscore >= -2){
                                                        $keterangan = 'Memuaskan';
                                                    }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                        $keterangan = 'Peringatan';
                                                    }else{
                                                        $keterangan = 'Tidak Memuaskan';
                                                    }
                                                ?>
                                                {{$keterangan}}
                                            </td>
                                        @endif
                                    @endforeach
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @if(count($zscore))
                <!-- <input type="submit" name="simpan" class="btn btn-primary" value="Print"> -->
                @else
                <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                @endif
                {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection