@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Input Rentang Per Parameter (Hematologi)</div>
                <div class="panel-body">
                @if(Session::has('message'))
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                  </div>
                @endif
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Parameter</label>
                              <select class="form-control" name="id_parameter" required>
                                <option value="{{$data->id_parameter}}">{!!$data->nama_parameter!!}</option>
                                @foreach($parameter as $val)
                                <option value="{{$val->id}}">{!!$val->nama_parameter!!}</option>
                                @endforeach
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Metode</label>
                              <select class="form-control" name="metode" id="metode" required>
                                <option value="{{$data->metode}}">{{$data->metode_pemeriksaan}}</option>
                                <option></option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Siklus</label>
                              <select class="form-control" name="siklus" required>
                                <option value="{{$data->siklus}}">{{$data->siklus}}</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                              </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Tahun</label>
                              <select class="form-control" name="tahun" required>
                                <option value="{{$data->tahun}}">{{$data->tahun}}</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Type</label>
                              <select class="form-control" name="type" required>
                                <option value="{{$data->type}}">{{$data->type}}</option>
                                <option value="a">A</option>
                                <option value="b">B</option>
                              </select>
                          </div>
                        </div>
                      </div>
                      <br>
                      <table class="table table-bordered" id="batas">
                        <tr>
                          <th>Batas Bawah</th>
                          <th>Batas Atas</th>
                        </tr>
                        <tbody id="targetBody">
                          @foreach($batas as $val)
                          <input type="hidden" name="id[]" class="form-control" value="{{$val->id}}">
                          <tr>
                            <td><input type="text" name="batas_bawah[]" class="form-control" value="{{$val->batas_bawah}}" required></td>
                            <td><input type="text" name="batas_atas[]" class="form-control" value="{{$val->batas_atas}}" required></td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                      <br>
                    <div><input type="button" id="more_fields" onclick="add_fields();" value="Tambah Batas" /></div><br>
                    <input type="submit" name="proses" class="btn btn-primary" value="Update">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$("#parameter").change(function(){
  var val = $(this).val(), i;
  var y = document.getElementById('metode');
  $("#metode").val("");
  y.innerHTML = "<option>Please Wait... </option>";
  $.ajax({
    type: "GET",
    url : "{{url('getmetode').'/'}}"+val,
    success: function(addr){
      y.innerHTML = "<option> </option>"
      for(i = 0; i < addr.Hasil.length; i++){
        var option = document.createElement("option");
        option.value = addr.Hasil[i].Kode;
        option.text = addr.Hasil[i].Nama;
        y.add(option);
      }
      return false;
    }
  });
});

function add_fields() {
  var target = $("#targetBody"),
      tr      = $("<tr />", {

      }).appendTo(target),
      td = $("<td />").appendTo(tr),
      input = $('<input type="text" name="batas_bawah[]" class="form-control">').appendTo(td);
      /*td ka dua*/
  var td2 = $("<td />").appendTo(tr),
      input2 = $('<input type="text" name="batas_atas[]" class="form-control">').appendTo(td2);
};
</script>
@endsection