<table border="1">
    <thead>
        <tr>
            <th>Kode Lab</th>
            @foreach($parameterna as $val)
            <th>{!!$val->nama_parameter!!}</th>
            @endforeach
            <th>Tanggal Kirim Data</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $val)
        <tr>
            <td>{{$val->kode_lab}}</td>
            @foreach($val->parameter as $par)
                @foreach($par->detail as $hasil)
                    <td>'{{$hasil->hasil_pemeriksaan}}</td>
                @endforeach
            @endforeach
            <td>{{$val->updated_at}}</td>
        </tr>
        @endforeach
    </tbody>
</table>