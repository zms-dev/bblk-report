<table border="1">
    <thead>
        <tr>
            <th colspan="8" style="text-align: center;">Rekapitulasi Hasil PNPME Parameter {!!$parameter->nama_parameter!!} Siklus {{$input['siklus']}} / @if($input['tipe'] == 'a') 01 @else 02 @endif</th>
        </tr>
        <tr>
            <th rowspan="2">Nilai</th>
            <th rowspan="2">Kriteria</th>
            <th colspan="2">Peserta</th>
            <th colspan="2">Metode</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th>Jumlah</th>
            <th>%</th>
            <th>Jumlah</th>
            <th>%</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>| Z Score | ≤ 2</td>
            <td>Memuaskan</td>
            <td>
                {{$datapesertaa->MEMUASKAN}}
            </td>
            <td>
                <?php 
                    $persen1 = ($datapesertaa->MEMUASKAN / $datapesertaa->JUMLAH_PESERTA) * 100;
                    echo number_format($persen1, 2)."%";
                ?>
            </td>
            <td>
                {{$datametodea->MEMUASKAN_METODE}}
            </td>
            <td>
                <?php 
                    $persen1 = ($datametodea->MEMUASKAN_METODE / $datametodea->JUMLAH_PESERTA_METODE) * 100;
                    echo number_format($persen1, 2)."%";
                ?>
            </td>
        </tr>
        <tr>
            <td>2 &lt; | Z Score | ≤ 3</td>
            <td>Peringatan</td>
            <td>
                {{$datapesertaa->PERINGATAN}}
            </td>
            <td>
                <?php 
                    $persen1 = ($datapesertaa->PERINGATAN / $datapesertaa->JUMLAH_PESERTA) * 100;
                    echo number_format($persen1, 2)."%";
                ?>
            </td>
            <td>
                {{$datapesertaa->PERINGATAN}}
            </td>
            <td>
                <?php 
                    $persen1 = ($datametodea->PERINGATAN_METODE / $datametodea->JUMLAH_PESERTA_METODE) * 100;
                    echo number_format($persen1, 2)."%";
                ?>
            </td>
        </tr>
        <tr>
            <td>| Z Score | > 3</td>
            <td>Tidak Memuaskan</td>
            <td>
                {{$datapesertaa->TIDAKS_MEMUASKAN}}
            </td>
            <td>
                <?php 
                    $persen1 = ($datapesertaa->TIDAKS_MEMUASKAN / $datapesertaa->JUMLAH_PESERTA) * 100;
                    echo number_format($persen1, 2)."%";
                ?>
            </td>
            <td>
                {{$datametodea->TIDAKS_MEMUASKAN_METODE}}
            </td>
            <td>
                <?php 
                    $persen1 = ($datametodea->TIDAKS_MEMUASKAN_METODE / $datametodea->JUMLAH_PESERTA_METODE) * 100;
                    echo number_format($persen1, 2)."%";
                ?>
            </td>
        </tr>
        <tr>
            <td>-</td>
            <td>Tidak di Analisis</td>
            <td>
                {{$datapesertaa->TIDAK_DI_ANAL}}
            </td>
            <td>
                <?php 
                    $persen1 = ($datapesertaa->TIDAK_DI_ANAL / $datapesertaa->JUMLAH_PESERTA) * 100;
                    echo number_format($persen1, 2)."%";
                ?>
            </td>
            <td>
                {{$datametodea->TIDAK_DI_ANAL_METODE}}
            </td>
            <td>
                <?php 
                    $persen1 = ($datametodea->TIDAK_DI_ANAL_METODE / $datametodea->JUMLAH_PESERTA_METODE) * 100;
                    echo number_format($persen1, 2)."%";
                ?>
            </td>
        </tr>
        <tr>
            <td>-</td>
            <td>Tidak di Evaluasi</td>
            <td>
                {{$datapesertaa->TIDAK_DI_EVALUASI}}
            </td>
            <td>
                <?php 
                    $persen1 = ($datapesertaa->TIDAK_DI_EVALUASI / $datapesertaa->JUMLAH_PESERTA) * 100;
                    echo number_format($persen1, 2)."%";
                ?>
            </td>
            <td>
                {{$datametodea->TIDAK_DI_EVALUASI_METODE}}
            </td>
            <td>
                <?php 
                    $persen1 = ($datametodea->TIDAK_DI_EVALUASI_METODE / $datametodea->JUMLAH_PESERTA_METODE) * 100;
                    echo number_format($persen1, 2)."%";
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">JUMLAH</td>
            <td>{{$datapesertaa->JUMLAH_PESERTA}}</td>
            <td>100%</td>
            <td>{{$datametodea->JUMLAH_PESERTA_METODE}}</td>
            <td>100%</td>
        </tr>
    </tbody>
</table>