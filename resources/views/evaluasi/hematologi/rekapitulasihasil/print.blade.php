<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama, table.utama td, table.utama th {    
    border: 1px solid #ddd;
    text-align: left;
}

table.utama {
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td {
    padding: 5px;
}
</style>
<table width="100%">
  <tr>
    <th colspan="3"><center>LAPORAN PELAKSAAN PNME BIDANG KIMIA KLINIK</center></th>
  </tr>
  <tr>
    <th colspan="3"><center>BALAI BESAR LABORATORIUM KESEHATAN JAKARTA</center></th>
  </tr>
  <tr>
    <th width="10%">Siklus</th>
    <th width="5%">:</th>
    <th>{{$input['siklus']}}</th>
  </tr>
  <tr>
    <th>Tahun</th>
    <th>:</th>
    <th>{{$input['tahun']}}</th>
  </tr>
</table>
<br>
<br>
<table width="100%" class="utama">
    <thead>
        <tr>
            <th rowspan="2">NO</th>
            <th rowspan="2">Kode Lab</th>
            <th rowspan="2">Provinsi</th>
            <th rowspan="2">Tanggal Terima Bahan</th>
            <th colspan="2">Kualitas Bahan</th>
            <th colspan="2">Tanggal Mengerjakan</th>
            <th colspan="2">Tanggal Kirim Hasil</th>
        </tr>
        <tr>
            <th>01</th>
            <th>02</th>
            <th>01</th>
            <th>02</th>
            <th>01</th>
            <th>02</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1;?>
        @foreach($data as $val)
        <tr>
            <td>{{$no}}</td>
            <td>{{$val->kode_lab}}</td>
            <td>{{$val->name}}</td>
            <td>{{$val->tgl_penerimaan}}</td>
            @if(!empty($val->tipe_1))
                <td style="text-transform: capitalize;">{{$val->tipe_1->kualitas_bahan}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->tipe_2))
                <td style="text-transform: capitalize;">{{$val->tipe_2->kualitas_bahan}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->tipe_1))
                <td style="text-transform: capitalize;">{{$val->tipe_1->tgl_pemeriksaan}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->tipe_2))
                <td style="text-transform: capitalize;">{{$val->tipe_2->tgl_pemeriksaan}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->tipe_1))
                <td style="text-transform: capitalize;">{{$val->tipe_1->updated_at}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
            @if(!empty($val->tipe_2))
                <td style="text-transform: capitalize;">{{$val->tipe_2->updated_at}}</td>
            @else
                <td style="text-transform: capitalize;">&nbsp;</td>
            @endif
        </tr>
        <?php $no++;?>
        @endforeach
    </tbody>
</table>