<style type="text/css">
@page {
margin: 150px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: center;
    border: 1px solid #ddd;
}

#header {
    position: fixed;
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer {
    position: fixed;
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
        <thead>
            <tr>
                <th width="12%"></th>
                <th>
                   <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px"> 
                </th>
                <th width="50%" style="padding-left: 5px; text-align: center;" >
                    <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 12px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br>HEMATOLOGI SIKLUS @if($siklus == 1) I @else II @endif TAHUN {{$date}}</b></span><br>
                    <span style="font-size: 12px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Jakarta</b></span>
                    <div style="padding-left: 75px; margin-top: -12px; font-size: 12px; margin-left: -80px;">
                        <span><br/>Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560<br>Telp. (021) 4212524, 42804339 Facsimile (021) 4245516<br>Website : bblkjakarta.id ; Surat Elektronik : bblkjakarta@yahoo.co.id</span>
                    </div>
                </th>
                <th width="%"></th>
               <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" height="70px">                
                    </th>

                
            </tr>
            <tr>
                <th colspan="6" style="padding: 1px;"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<div id="footer">
    HASIL EVALUASI HEMATOLOGI SIKLUS @if($siklus == 1) I @else II @endif TAHUN {{$date}}
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
              <label>
                  <center>
                      <font size="10px">
                          <b>HASIL EVALUASI PNPME HEMATOLOGI SIKLUS @if($siklus == 1) I @else II @endif TAHUN {{$date}}<br>BAHAN KONTROL {{$urut}}</b>
                          {{-- KODE PESERTA : {{$datas->kode_lab}} --}}
                          {{-- KELOMPOK (SELURUH PESERTA) --}}
                      </font>
                  </center>
              </label><br>
              <table width="100%">
                <tr>
                  <th width="70px">Kode Peserta</th>
                  <td width="10px"> : </td>
                  <td>{{$register->kode_lebpes}}</td>
                </tr>
                <tr>
                  <th>Nama Instansi</th>
                  <td> : </td>
                  <td>{{$perusahaan}}</td>
                </tr>
              </table><br>
               <table class="table utama">
                        <thead>
                            <tr>
                              <!-- <th rowspan="2">Kode Lab</th> -->
                              <th rowspan="2">Parameter</th>
                              <th rowspan="2">Nama Alat</th>
                              <th rowspan="2">Metode Pemeriksaan</th>
                              <th rowspan="2">Hasil</th>
                              <th colspan="3" align="center">Seluruh Peserta</th>
<!--                               <th colspan="3" align="center">Kelompok Alat</th>
 -->                          <th colspan="3" align="center">Kelompok Metode</th>
                            </tr>
                            <tr>
                                <th>Nilai Target</th>
                                <th>Z-Score</th>
                                <th>Keterangan</th>
                                <th>Nilai Target</th>
                                <th>Z-Score</th>
                                <th>Keterangan</th>
                              <!--   <th>Nilai Target</th>
                                <th>Z-Score</th>
                                <th>Keterangan</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $val)
                            <tr>
                                <!-- <td>{{$val->kode_lab}}</td> -->
                                <td>
                                    {!!$val->nama_parameter!!}
                                </td>
                                <td>
                                  @if($val->instrumen != NULL)
                                    @if($val->instrumen == 'Instrument lain :') @else {{$val->instrumen}} @endif {{$val->instrument_lain}}
                                  @else
                                  {{$val->alat}}
                                  @endif
                                </td>
                                <td>@if($val->metode_pemeriksaan == 'Metode lain :')@else{!!$val->metode_pemeriksaan!!}@endif  {{$val->metode_lain}}</td>
                                <td>{{$val->hasil_pemeriksaan}}</td>
                                <td>
                                    @if (count($val->sd))
                                        {{$val->sd[0]->median}}
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                        @if (count($val->sd))
                          @if($val->sd[0]->median == '-')
                            -
                          @else
                                    <?php
                                        $val->hasil_pemeriksaan = ($val->hasil_pemeriksaan == "-" ? NULL : $val->hasil_pemeriksaan);
                                        if (count($val->sd)) {
                                            $z = ($val->hasil_pemeriksaan - $val->sd[0]->median) / $val->sd[0]->sd;
                                        }else{
                                            $z = $val->hasil_pemeriksaan;
                                        }
                                    ?>
                                    @if(count($zscore))
                                        @foreach($zscore as $za)
                                            @if($za->parameter == $val->parameterID)
                                                @if($za->zscore == 'Tidak di analisis')
                                                -
                                                @else
                                                {{$za->zscore}}
                                                @endif
                                            @endif
                                        @endforeach
                                    @else
                                        @if($val->hasil_pemeriksaan == NULL)
                                        -
                                        <input type="hidden" name="zscore[]" value="Tidak di analisis">
                                        @else
                                        {{number_format($z, 1)}}
                                        <input type="hidden" name="zscore[]" value="{{number_format($z, 1)}}">
                                        @endif
                                    @endif
                          @endif
                        @else
                          -
                        @endif
                                </td>
                        @if (count($val->sd))
                          @if($val->sd[0]->median == '-')
                            -
                          @else
                                    @foreach($zscore as $za)
                                        @if($za->parameter == $val->parameterID)
                                            <td>
                                                <?php
                                                    if($za->zscore == 'Tidak di analisis'){
                                                        $keterangan = '-';
                                                    }else if($za->zscore <= 2 && $za->zscore >= -2){
                                                        $keterangan = 'Memuaskan';
                                                    }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                        $keterangan = 'Peringatan';
                                                    }else{
                                                        $keterangan = 'Tidak Memuaskan';
                                                    }
                                                ?>
                                                {{$keterangan}}
                                            </td>
                                        @endif
                                    @endforeach
                          @endif
                        @else
                          <td>Tidak dapat dievaluasi</td>
                        @endif
                          <!-- <td>
                              @if (count($val->sd_alat))
                                  {{$val->sd_alat[0]->median}}
                              @else
                              -
                              @endif
                          </td>
                          <td>
                          @if (count($val->sd_alat))
                            @if($val->sd_alat[0]->median == '-')
                              -
                            @else
                                    <?php
                                        if (count($val->sd_alat)) {
                                            $na = ($val->hasil_pemeriksaan - $val->sd_alat[0]->median) / $val->sd_alat[0]->sd;
                                            $z = number_format($na, 1);
                                        }else{
                                            $z = "Tidak dapat dievaluasi";
                                        }
                                    ?>
                                    @if(count($zscore_alat))
                                        @foreach($zscore_alat as $za)
                                            @if($za->parameter == $val->id_parameter)
                                                @if($za->alat == $val->alatID)
                                                @if($za->zscore == 'Tidak di analisis')
                                                Tidak di analisis
                                                @else
                                                {{$za->zscore}}
                                                @endif
                                                @endif
                                            @endif
                                        @endforeach
                                    @else
                                        @if($val->hasil_pemeriksaan == NULL)
                                        Tidak di analisis
                                        <input type="hidden" name="zscore[]" value="Tidak di analisis">
                                        @else
                                        {{$z}}
                                        <input type="hidden" name="zscore[]" value="{{$z}}">
                                        @endif
                                    @endif
                            @endif
                          @else
                            -
                          @endif
                          </td>
                          @if (count($val->sd_alat))
                            @if($val->sd_alat[0]->median == '-')
                              -
                            @else
                                @if(count($zscore_alat))
                                  @foreach($zscore_alat as $za)
                                      @if($za->parameter == $val->id_parameter)
                                          @if($za->alat == $val->alatID)
                                              <td>
                                                  <?php
                                                      if($za->zscore == "Tidak dapat dievaluasi"){
                                                          $keterangan = '-';
                                                      }elseif($za->zscore <= 2 && $za->zscore >= -2){
                                                          $keterangan = 'Memuaskan';
                                                      }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                          $keterangan = 'Peringatan';
                                                      }else{
                                                          $keterangan = 'Tidak Memuaskan';
                                                      }
                                                  ?>
                                                  <center>{{$keterangan}}</center>
                                              </td>
                                          @endif
                                      @endif
                                  @endforeach
                                @else
                                  <td>-</td>
                                @endif
                          @endif
                        @else
                          <td>Tidak dapat dievaluasi</td>
                        @endif -->

                              <td>
                                  @if (count($val->sd_metode) && $val->sd_metode[0]->median != "" && $val->sd_metode[0]->median != "-")
                                      {{$val->sd_metode[0]->median}}
                                  @else
                                  -
                                  @endif
                              </td>
                              <td>
                                  <?php
                                      $val->hasil_pemeriksaan = ($val->hasil_pemeriksaan == "-" ? NULL : $val->hasil_pemeriksaan);
                                      if (empty($val->sd_metode[0]->median)) {
                                          $z = "-";
                                      }elseif($val->sd_metode[0]->median == '-'){
                                          $z = "-";
                                      }else{
                                          $na = ($val->hasil_pemeriksaan - $val->sd_metode[0]->median) / $val->sd_metode[0]->sd;
                                          $z = number_format($na, 1);
                                      }
                                  ?>
                                  @if (count($val->sd_metode))
                                    @if($val->sd_metode[0]->median == '-')
                                      -
                                    @else
                                      @if(count($zscore_metode))
                                          @foreach($zscore_metode as $za)
                                                  @if($za->parameter == $val->id_parameter)
                                                      @if($za->metode == $val->metodeID)
                                                        @if($za->zscore == 'Tidak di analisis')
                                                        Tidak di analisis
                                                        @elseif($za->zscore == 'Tidak dapat dievaluasi')
                                                        -
                                                        @else
                                                        {{$za->zscore}}
                                                        @endif
                                                      @endif
                                                  @endif
                                          @endforeach
                                      @else
                                          @if($val->hasil_pemeriksaan == NULL)
                                          -
                                          <input type="hidden" name="zscore[]" value="Tidak di analisis">
                                          @else
                                          {{$z}}
                                          <input type="hidden" name="zscore[]" value="{{$z}}">
                                          @endif
                                      @endif
                                    @endif
                                  @else
                                      -
                                  @endif
                              </td>
                              @if (count($val->sd_metode))
                                @if($val->sd_metode[0]->median == '-')
                                  <td>Tidak dapat dievaluasi</td>
                                @else
                                  @if(count($zscore_metode))
                                    @foreach($zscore_metode as $za)
                                      @if($za->parameter == $val->id_parameter)
                                        @if($za->metode == $val->metodeID)
                                          <td>
                                            <?php
                                              if($za->zscore == "Tidak dapat dievaluasi" || $za->zscore == "-" ){
                                                $keterangan = '-';
                                              }elseif($za->zscore <= 2 && $za->zscore >= -2){
                                                $keterangan = 'Memuaskan';
                                              }else if($za->zscore <= 3 && $za->zscore >= -3){
                                                $keterangan = 'Peringatan';
                                              }else{
                                                $keterangan = 'Tidak Memuaskan';
                                              }
                                            ?>
                                            <center>{{$keterangan}}</center>
                                          </td>
                                        @endif
                                      @endif
                                    @endforeach
                                  @else
                                    <td>-</td>
                                  @endif
                                @endif
                              @else
                                <td>
                                  @if($val->hasil_pemeriksaan == '-' || $val->hasil_pemeriksaan == NULL)
                                    -
                                  @else
                                    Tidak dapat dianalisis
                                  @endif
                                </td>
                              @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table><br>
                <table width="100%" border="">
                    <tr>
                        <td width="50%" style="vertical-align:top;padding-top:20px;">
                            <b>Keterangan : </b>
                            <p style="margin-left: 30px">Kriteria Nilai :</p>
                            <ul>
                                <li><span style="width:80px;display:inline-block;"><b>|</b> Z Score |<span style="font-family: DejaVu Sans; sans-serif;">&le;</span> 2</span> = Memuaskan</li>
                                <li><span style="width:80px;display:inline-block;">2 &lt;<b>|</b> Z Score|<span style="font-family: DejaVu Sans; sans-serif;">&le;</span> 3</span> = Peringatan</li>
                                <li><span style="width:80px;display:inline-block;"><b>|</b> Z Score|<span style="font-family: DejaVu Sans; sans-serif;">&gt;</span> 3</span> = Tidak Memuaskan</li>
                            </ul>
                        </td>
                        <td width="50%" style="vertical-align:top;">
                        <div style="position: relative;left: 65%; top: -3%;">
                        <p>
                        <div style="position: relative; top: 22px">
                        Jakarta,  {{ _dateIndo($evaluasi->tanggal) }}<br>
                        Penanggung Jawab
                        </div>
                        <p>

                            <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'imunologi_ttd.png')}}" width="160" style="margin: -40px 0px !important; position: absolute;">
                        </p>
                        <div style="position: relative; top:80px">
                        dr. Siti Kurnia Eka Rusmiarti, Sp.PK
                        </div>
                        </p>
                        </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
