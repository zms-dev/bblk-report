@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Persentase Peserta per Instansi</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" id="uangmasuk" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Parameter</label>
                          <select class="form-control" name="bidang" required>
                            <option></option>
                            @foreach($data as $val)
                            <option value="{{$val->id}}">{{$val->alias}}</option>
                            @endforeach
                          </select>
                      </div><br>
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus" required>
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div><br>
                    <input type="submit" name="proses" class="btn btn-primary" value="Proses">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection