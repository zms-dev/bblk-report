@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Persentase Rekap Reagen</div>
                <div class="panel-body">
                    <table class="table table-bordered" id="table">
                        <thead>
                            <tr>
                                <th>Nama Reagen</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $val)
                            <tr>
                                <td>{{$val->reagen}}</td>
                                <td><?php $persen = $val->jumlah / $total * 100 ?> {{$persen}}%</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                  <input type="button" class="btn" value="Select Data" onclick="selectElementContents( document.getElementById('table') );">
                  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function selectElementContents(el) {
    var body = document.body, range, sel;
    if (document.createRange && window.getSelection) {
        range = document.createRange();
        sel = window.getSelection();
        sel.removeAllRanges();
        try {
            range.selectNodeContents(el);
            sel.addRange(range);
        } catch (e) {
            range.selectNode(el);
            sel.addRange(range);
        }
    } else if (body.createTextRange) {
        range = body.createTextRange();
        range.moveToElementText(el);
        range.select();
        range.execCommand("Copy");
    }
}

Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: 'Persentase Reagen yang di Gunakan'
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -90,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Reagen'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Reagen : <b>{point.y}%</b>'
    },
    series: [{
        name: 'Population',
        colorByPoint: true,
        data: [
            @foreach($data as $val)
            ['{{$val->reagen}}', <?php $persen = $val->jumlah / $total * 100 ?> {{$persen}}],
            @endforeach
        ],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y}%',
            y: 10,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});
</script>
@endsection