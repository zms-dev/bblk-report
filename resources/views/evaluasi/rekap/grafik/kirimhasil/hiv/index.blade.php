@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Peserta Kirim Hasil</div>
                <div class="panel-body">
                  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
        @foreach($data as $val)
            '{{$val->badan_usaha}}',
        @endforeach
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Peserta ({{$sub->alias}})'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            dataLabels: {
                    enabled: true,
                    crop: false,
                    overflow: 'none'
                }
        }
    },
    series: [{
        name: 'Peserta',
        data: [
                @foreach($data as $val)
                {{$val->peserta}},
                @endforeach
                ]

    }, {
        name: 'Kirim Hasil',
        data: [
                @foreach($data as $val)
                {{$val->kirimhasil}},
                @endforeach
                ]

    }, {
        name: 'Tidak Kirim Hasil',
        data: [
                @foreach($data as $val)
                {{$val->tidakkirimhasil}},
                @endforeach
                ]

    }]
});
</script>
@endsection