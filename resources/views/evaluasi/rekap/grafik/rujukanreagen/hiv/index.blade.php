@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Rujukan per Reagen</div>
                <div class="panel-body">
                  <div id="container" style="min-width: 310px; height: 1500px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Reagen ({{$sub->alias}})'
    },
    xAxis: {
        categories: [
                @foreach($data as $val)
                    '{{$val->reagen}}',
                @endforeach],
        title: {
            text: null
        }
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        },
        series: {
            pointWidth: 15
        }
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Total',
        data: [
                @foreach($data as $val) 
                {{$val->total}},
                @endforeach
                ]

    }, {
        name: 'Sesuai Rujukan',
        color: '#f7cc2e',
        data: [
                @foreach($data as $val)
                {{$val->sesuai}},
                @endforeach
                ]
    }, {
        name: 'Tidak Sesuai Rujukan',
        color: '#90ed7d',
        data: [
                @foreach($data as $val)
                {{$val->tidaksesuai}},
                @endforeach
                ]
    }]
});
</script>
@endsection