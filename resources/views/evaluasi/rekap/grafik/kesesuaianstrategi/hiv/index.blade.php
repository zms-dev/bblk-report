@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Tahap Pemeriksaan Kesesuaian Strategi</div>
                <div class="panel-body">
                  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            'Total Peserta',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Peserta ({{$sub->alias}})'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            dataLabels: {
                    enabled: true,
                    crop: false,
                    overflow: 'none'
                }
        }
    },
    series: [{
        name: 'Sesuai Strategi III',
        data: [
                {{$sesuai3}},
            ]

    }, {
        name: 'Tidak Sesuai Strategi III',
        data: [
                {{$tidaksesuai3}},
                ]

    },{
        name: 'Sesuai Strategi I',
        data: [
                {{$sesuai1}}
                ]

    },{
        name: 'Tidak Sesuai Strategi I',
        data: [
                {{$tidaksesuai1}}
                ]

    }]
});
</script>
@endsection