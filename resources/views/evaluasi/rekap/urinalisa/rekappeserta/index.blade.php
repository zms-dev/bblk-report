<table border="1">
    <thead>
        <tr>
            <th rowspan="2">Kode Lab</th>
            <th rowspan="2">Nama Instansi</th>
            @foreach($parameterna as $val)
            <th colspan="4">{!!$val->nama_parameter!!}</th>
            @endforeach
        </tr>
        <tr>
            <th></th>
            <th></th>
            @foreach($parameterna as $val)
            <th><center>Hasil</center></th>
            <th><center>Instrument</center></th>
            <th><center>Metode</center></th>
            <th><center>Reagen</center></th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($data as $val)
        <tr>
            <td>{{$val->kode_lab}}</td>
            <td>{{$val->nama_lab}}</td>
            @foreach($val->parameter as $par)
                @foreach($par->detail as $hasil)
                    @if($hasil->parameter_id <= '10')
                    <td>'{{$hasil->hasil_pemeriksaan}}</td>
                    @else
                    <td>{{$hasil->hp}}</td>
                    @endif
                    <td>{{$hasil->alat}}</td>
                    <td>{!!$hasil->metode_pemeriksaan!!} {{$hasil->metode_lain}}</td>
                    <td>{!!$hasil->reagen!!}@if($hasil->reagen_lain != NULL) : {{$hasil->reagen_lain}}@endif</td>
                @endforeach
            @endforeach
        </tr>
        @endforeach
    </tbody>
</table>