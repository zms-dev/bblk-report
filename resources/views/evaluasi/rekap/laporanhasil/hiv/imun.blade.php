
@if(count($data))
  <table id="peserta_pme">
  @foreach($data as $perusahaan)
      <tr class="text-tengah">
          <th>{{$perusahaan->kode_lebpes}}</th>
          <th>Nama Reagen</th>
          <th>Hasil Pemeriksaan</th>
          @if($perusahaan->jenis_form == 'rpr-syphilis')
          <th>Titer</th>
          <th>Hasil Rujukan</th>
          <th>Rentan Titer</th>
          @else
          <th>Hasil Rujukan</th>
          @endif
      </tr>
      <?php
          $no = 0;
      ?>
      @foreach($perusahaan->data2 as $hasil)
      <tr>
          <td align="middle" style="vertical-align: middle;">{{$hasil->rujukan[$no]->kode_bahan_uji}}</td>
              @if(!empty($perusahaan->data3[$no]))
                <td rowspan="3" style="vertical-align: middle;">
                @if($perusahaan->data3[$no]->reagen == 'Lain - lain')
                  {{$perusahaan->data3[$no]->reagen_lain}}
                @else
                  {{$perusahaan->data3[$no]->reagen}}
                @endif
                </td>
              @else
                <td></td>
              @endif
          <td><center>{{$hasil->interpretasi}}</center></td>
          @if($perusahaan->jenis_form == 'rpr-syphilis')
          <td>{{$hasil->titer}}</td>
          <td>{{$hasil->nilai_rujukan}}</td>
          <td>{{$hasil->range1}} - {{$hasil->range2}}</td>
          @else
          <td>{{$hasil->rujukan[$no]->nilai_rujukan}}</td>
          @endif
      </tr>
      <?php $no++; ?>
      @endforeach
  @endforeach
  </table>
@else
  Data tidak ditemukan!
@endif
