
@if(count($data))
  <table id="peserta_pme">
  @foreach($data as $perusahaan)
      <tr class="text-tengah">
          <th>{{$perusahaan->kode_lebpes}}</th>
          <th>Nama Reagen</th>
          <th>Hasil Pemeriksaan</th>
          <th>Hasil Rujukan</th>
      </tr>
      <?php
          $no = 0;
          $val = ['I','II','III','IV'];
      ?>
      @foreach($perusahaan->data2 as $hasil)
      @if($hasil->tabung == '1')
      <tr>
          @if($no != 0)
          <td></td>
          @else
          <td rowspan="3" align="middle" style="vertical-align: middle;">{{$hasil->rujukan[0]->kode_bahan_uji}}</td>
          @endif
          <td>
              @if(!empty($perusahaan->data3[$no]))
                @if($perusahaan->data3[$no]->reagen == 'Lain - lain')
                  {{$perusahaan->data3[$no]->reagen_lain}}
                @else
                  {{$perusahaan->data3[$no]->reagen}}
                @endif
              @else
              -
              @endif
          </td>
          <td><center>{{$hasil->interpretasi}}</center></td>
          <td>{{$hasil->rujukan[0]->nilai_rujukan}}</td>
      </tr>
      <?php $no++; ?>
      @endif
      @endforeach
      <?php
          $no = 0;
          $val = ['I','II','III','IV'];
      ?>
      @foreach($perusahaan->data2 as $hasil)
      @if($hasil->tabung == '2')
      <tr>
          @if($no != 0)
          <td></td>
          @else
          <td rowspan="3" align="middle" style="vertical-align: middle;">{{$hasil->rujukan[1]->kode_bahan_uji}}</td>
          @endif
          <td>
              @if(!empty($perusahaan->data3[$no]))
                @if($perusahaan->data3[$no]->reagen == 'Lain - lain')
                  {{$perusahaan->data3[$no]->reagen_lain}}
                @else
                  {{$perusahaan->data3[$no]->reagen}}
                @endif
              @else
              -
              @endif
          </td>
          <td><center>{{$hasil->interpretasi}}</center></td>
          <td>{{$hasil->rujukan[1]->nilai_rujukan}}</td>
      </tr>
      <?php $no++; ?>
      @endif
      @endforeach
      <?php
          $no = 0;
          $val = ['I','II','III','IV'];
      ?>
      @foreach($perusahaan->data2 as $hasil)
      @if($hasil->tabung == '3')
      <tr>
          @if($no != 0)
          <td></td>
          @else
          <td rowspan="3" align="middle" style="vertical-align: middle;">{{$hasil->rujukan[2]->kode_bahan_uji}}</td>
          @endif
          <td>
              @if(!empty($perusahaan->data3[$no]))
                @if($perusahaan->data3[$no]->reagen == 'Lain - lain')
                  {{$perusahaan->data3[$no]->reagen_lain}}
                @else
                  {{$perusahaan->data3[$no]->reagen}}
                @endif
              @else
              -
              @endif
          </td>
          <td><center>{{$hasil->interpretasi}}</center></td>
          <td>{{$hasil->rujukan[2]->nilai_rujukan}}</td>
      </tr>
      <?php $no++; ?>
      @endif
      @endforeach
  @endforeach
  </table>
@else
  Data tidak ditemukan!
@endif
