@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">REKAP INPUT PESERTA FORMAT B</div>
                <div class="panel-body">
                    <blockquote style="font-size: 12px">
                      Silahkan mengisi menu EVALUASI-RUJUKAN bila data belum tampil!
                    </blockquote>
                    <form class="form-horizontal" method="post" id="uangmasuk" enctype="multipart/form-data" target="_blank">
                      @if(Session::has('message'))
                          <p class="alert {{ Session::get('alert-class') }}">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              {!! Session::get('message') !!}
                          </p>
                      @endif
                      <div>
                          <label for="exampleInputEmail1">Parameter</label>
                          <select class="form-control" name="bidang">

                            <option></option>
                            @foreach($data as $val)
                            <option value="{{$val->id}}">{{Auth::user()->name}}</option>
                            @endforeach
                          </select>
                      </div>
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus">
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div>
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <select class="form-control" name="tahun" required>
                            <option></option>
                            <?php for ($i=2018; $i <= date('Y'); $i++) { ?>
                              <option value="{{$i}}">{{$i}}</option>
                            <?php } ?>
                          </select>
                      </div><br>
                    <input type="submit" name="proses" class="btn btn-primary" value="Proses">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
