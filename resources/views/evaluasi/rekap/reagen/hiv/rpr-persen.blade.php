@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Reagen TP-SYPHILIS
                </div>
                
                <div class="panel-body">
                    <table class="table table-bordered" id="table">
                        <thead>
                            <tr>
                                <th>Nama Reagen</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $val)
                            @if($val->jenis_form == "Syphilis")
                            <tr>
                                    <td>{{$val->reagen}}</td>
                                    <td><?php $persen = $val->jumlah / $total * 100 ?> {{number_format($persen, 2)}}%</td>
                                
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                  <input type="button" class="btn" value="Select Data" onclick="selectElementContents( document.getElementById('table') );">
                  <div id="container_tp" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function selectElementContents(el) {
    var body = document.body, range, sel;
    if (document.createRange && window.getSelection) {
        range = document.createRange();
        sel = window.getSelection();
        sel.removeAllRanges();
        try {
            range.selectNodeContents(el);
            sel.addRange(range);
        } catch (e) {
            range.selectNode(el);
            sel.addRange(range);
        }
    } else if (body.createTextRange) {
        range = body.createTextRange();
        range.moveToElementText(el);
        range.select();
        range.execCommand("Copy");
    }
}

Highcharts.chart('container_tp', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: 'Reagen yang di Gunakan'
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -90,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        maxPadding: 0.2,
        title: {
            text: 'Reagen'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Reagen : <b>{point.y}</b>'
    },
    series: [{
        name: 'Population',
        colorByPoint: true,
        data: [
            @foreach($data as $val)
                @if($val->jenis_form == "Syphilis")
                    ['{{$val->reagen}}', <?php $persen = $val->jumlah / $total * 100 ?> {{number_format($persen, 2)}}],
                @endif
            @endforeach
        ],
        dataLabels: {
            enabled: true,
            format: '{point.y}',
        }
    }]
});
</script>
@endsection