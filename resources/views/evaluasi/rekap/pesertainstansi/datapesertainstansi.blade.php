@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Peserta Instansi {{$bidang2->alias}}</div>

                <div class="panel-body">
                  <table id="table" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Instansi</th>
                        <th>Peserta</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no = 0; ?>
                      @foreach($data as $val)
                      <?php $no++; ?>
                      <tr>
                        <td>{{$no}}</td>
                        <td>{{$val->badan_usaha}}</td>
                        <td>{{$val->peserta}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  <input type="button" class="btn" value="Select Data" onclick="selectElementContents( document.getElementById('table') );">

                  <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function selectElementContents(el) {
    var body = document.body, range, sel;
    if (document.createRange && window.getSelection) {
        range = document.createRange();
        sel = window.getSelection();
        sel.removeAllRanges();
        try {
            range.selectNodeContents(el);
            sel.addRange(range);
        } catch (e) {
            range.selectNode(el);
            sel.addRange(range);
        }
    } else if (body.createTextRange) {
        range = body.createTextRange();
        range.moveToElementText(el);
        range.select();
        range.execCommand("Copy");
    }
}



Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: 'Rekap Peserta Instansi {{$bidang2->alias}}'
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        maxPadding: 0.2,
        title: {
            text: 'Peserta ({{$bidang2->alias}})'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Peserta : <b>{point.y}</b>'
    },
    series: [{
        name: 'Population',
        colorByPoint: true,
        data: [
            @foreach($data as $val)
            ['{{$val->badan_usaha}}', {{$val->peserta}}],
            @endforeach
        ],
        dataLabels: {
            enabled: true,
            format: '{point.y}',
        }
    }]
});
</script>
@endsection