<style type="text/css">
@page {
margin: 160px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}
table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #ddd;
}
#header {
    position: fixed;
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer {
    position: fixed;
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" style="text-align: center;">
        <thead>
            <tr>
                <th>
                     <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
                </th>
                <th width="100%" style="padding-left: 20px">
                    <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                    <span style="font-size: 12px"><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br>URINALISIS SIKLUS @if($siklus == 1) I @else II @endif TAHUN {{$date}}</b></span><br>
                    <span style="font-size: 12px">Penyelenggara : <b>Balai Besar Laboratorium Kesehatan Jakarta</b></span>
                    <div style="padding-left: 75px; margin-top: -15px; font-size: 12px;margin-left: -81px;">
                        <span><br/>Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560<br>Telp. (021) 4212524, 42804339 Facsimile (021) 4245516<br>Website : bblkjakarta.id ; Surat Elektronik : bblkjakarta@yahoo.co.id</span>
                    </div>
                </th>
                <th>
                    <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'logo.png')}}" height="70px">                
                    </th>
            </tr>
            <tr>
                <th colspan="3" style="padding: -2px;"><hr></th>
            </tr>
        </thead>
    </table>
</div>
<div id="footer">
    HASIL EVALUASI URINALISIS SIKLUS @if($siklus == 1) I @else II @endif TAHUN {{$date}}
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                    <label>
                        <center>
                            <font size="12px">
                                <b>HASIL EVALUASI PNPME URINALISIS SIKLUS @if($siklus == 1) I @else II @endif TAHUN {{$date}}<br>BAHAN KONTROL @if($type=='a')A @else B @endif</b><br><br>
                                <!-- KODE PESERTA : {{$datas->kode_lab}} -->
                            </font>
                        </center>
                    </label><br>
                    <table width="100%">
                        <tr>
                          <th width="70px">Kode Peserta</th>
                          <td width="10px">:</td>
                          <td>{{$register->kode_lebpes}}</td>
                        </tr>
                        <tr>
                          <th>Nama Instansi</th>
                          <td>:</td>
                          <td>{{$perusahaan}}</td>
                        </tr>
                    </table><br>
                <table class="table utama">
                     <thead>
                        <tr>
                            <th rowspan="2" style="text-align: center;">No</th>
                            <th rowspan="2" style="text-align: center;">Parameter</th>
                            <th rowspan="2" style="text-align: center;">Metode</th>
                            <th rowspan="2" style="text-align: center;">Hasil Saudara</th>
                            <!-- <th>Reagen</th> -->
                            <th colspan="2"  style="text-align: center;">Seluruh Peserta</th>
                            <th colspan="2"  style="text-align: center;">Kelompok Metode</th>
                        </tr>
                        <tr>
                            <th style="text-align: center;">Konsensus</th>
                            <th style="text-align: center;">Nilai</th>
                            <th style="text-align: center;">Konsensus</th>
                            <th style="text-align: center;">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                            @if(count($data))
                               <?php $no = 0; $jumlah = 0;?>
                            <?php $no = 0; ?>
                            @foreach($data as $val)
                            <?php $no++; ?>
                            <tr>
                                <td style="text-align: center">{{$no}}<input type="hidden" name="id_detail[]" value="{{$val->id_detail}}"></td>
                                <td>{!!$val->nama_parameter!!}<input type="hidden" name="parameter_id[]" value="{{ $val->id }}" /></td>
                                <td style="text-align: center">{!!$val->metode_pemeriksaan!!}<input type="hidden" name="parameter_id[]" value="{{ $val->id }}" /></td>
                                <td style="text-align: center">
                                    @if($val->id <= 10)
                                        @if($val->Hasil != "-")
                                            {{$val->Hasil}}
                                        @else
                                            -
                                            <?php $jumlah++; ?>
                                        @endif
                                    @else
                                        @if(count($val->hp))
                                            @if(count($val->hasilPemeriksaan))
                                                {!!$val->hp!!}
                                            @else
                                                {{$val->Hasil}}
                                            @endif
                                        @else
                                            Tidak mengerjakan
                                            <?php $jumlah++; ?>
                                        @endif
                                    @endif
                                </td>
                                <td style="text-align: center">
                                    @if($val->id <= 10)
                                        @if($val->Hasil != "-")
                                            @foreach($val->target as $target)
                                                 {{ $target->batas_bawah }} - {{ $target->batas_atas }}
                                            @endforeach
                                        @else
                                        -
                                        @endif
                                    @else
                                        @if(count($val->hp))
                                            @foreach($val->target as $target)
                                                 @if($target->batas_bawah != 'Negatif' && $target->batas_bawah != 'Positif')
                                                 {{ $target->batas_bawah }} - {{ $target->batas_atas }}
                                                 @else
                                                 {{ $target->batas_bawah }}
                                                @endif
                                            @endforeach
                                        @else
                                        -
                                        @endif
                                    @endif
                                </td>
                                
                                <td style="text-align: center" class="ngitung">
                                    <?php
                                        $batas_bawah = str_replace(['+','±'], '0', $target->batas_bawah);
                                        $batas_atas = str_replace(['+','±'], '0', $target->batas_atas);
                                          
                                    ?>
                                    @if($val->id <= 10)
                                        @if($val->Hasil != "-" && $val->Hasil != NULL && $val->Hasil != "Tidak mengerjakan" && $target->batas_bawah != NULL)
                                            <?php
                                                $hasil = number_format($val->Hasil, 3);
                                                if ($hasil >= $batas_bawah && $hasil <= $batas_atas) {
                                                    echo "<div style='background-color:#34a449; color:#34a449;' class='hitung'>4</div>"."<br>";
                                                }else{
                                                    echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung'>0</div>"."<br>";
                                                }
                                            ?>
                                        @elseif($val->Hasil == NULL)
                                        <div style='background-color:#eee; color:#eee;'>0</div>
                                        @elseif($target->batas_bawah == NULL && $target->batas_atas == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @elseif($target->batas_bawah == NULL && $target->batas_atas == NULL && $val->Hasil == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @else
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @endif
                                    @else
                                        @if($val->hp != NULL && $val->Hasil != "Tidak mengerjakan" && $target->batas_bawah != NULL)
                                            @if($batas_bawah == 'Negatif' AND $val->hp == 'Negatif')
                                                <?php
                                                    echo "<div style='background-color:#34a449; color:#34a449;' class='hitung'>4</div>"."<br>";
                                                ?>  
                                            @elseif($batas_bawah == 'Negatif' AND $val->hp == 'Positif')
                                                <?php
                                                    echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung'>0</div>"."<br>";
                                                ?>  
                                            @elseif($batas_bawah == 'Positif' AND $val->hp == 'Negatif')
                                                <?php
                                                    echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung'>0</div>"."<br>";
                                                ?>                                    
                                            @elseif($batas_bawah == 'Positif' AND $val->hp == 'Positif')
                                                <?php
                                                    echo "<div style='background-color:#34a449; color:#34a449;' class='hitung'>4</div>"."<br>";
                                                ?>  
                                            @else
                                                <?php
                                                    $hasil = str_replace(['+','±'], '0', $val->hp);
                                                    if ($hasil >= $batas_bawah && $hasil <= $batas_atas) {
                                                        echo "<div style='background-color:#34a449; color:#34a449;' class='hitung2'>4</div>"."<br>";
                                                    }else{
                                                        echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung2'>0</div>"."<br>";
                                                    }
                                                ?>
                                            @endif

                                        @elseif($val->hp == NULL)
                                        <div style='background-color:#eee; color:#eee;'>0</div>
                                        @elseif($target->batas_bawah == NULL && $target->batas_atas == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @elseif($target->batas_bawah == NULL && $target->batas_atas == NULL && $val->hp == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @else
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @endif
                                    @endif
                                </td>
                                <td style="text-align: center">
                                    @if($val->id <= 10)
                                        @if($val->Hasil != "-")
                                            @foreach($val->metode as $metode)
                                                 {{ $metode->batas_bawah }} - {{ $metode->batas_atas }}
                                            @endforeach
                                        @else
                                        -
                                        @endif
                                    @else
                                        @if(count($val->hp))
                                            @foreach($val->metode as $metode)
                                                @if($metode->batas_bawah != 'Negatif' && $metode->batas_bawah != 'Positif')
                                                 {{ $metode->batas_bawah }}  - {{ $metode->batas_atas }}
                                                 @else
                                                 {{ $metode->batas_bawah }}
                                                @endif
                                            @endforeach
                                        @else
                                        -
                                        @endif
                                    @endif
                                </td>
                            <td style="text-align: center" class="ngitung2">
                                    <?php
                                        $batas_bawah_metode = str_replace(['+','±'], '0', $metode->batas_bawah);
                                        $batas_atas_metode = str_replace(['+','±'], '0', $metode->batas_atas);
                                          
                                    ?>
                                    @if($val->id <= 10)
                                        @if($val->Hasil != "-" && $val->Hasil != NULL && $val->Hasil != "Tidak mengerjakan" && $metode->batas_bawah != NULL)
                                            <?php
                                                $hasil = number_format($val->Hasil, 3);
                                                if ($hasil >= $batas_bawah_metode && $hasil <= $batas_atas_metode) {
                                                    echo "<div style='background-color:#34a449; color:#34a449;' class='hitung2'>4</div>"."<br>";
                                                }else{
                                                    echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung2'>0</div>"."<br>";
                                                }
                                            ?>
                                        @elseif($val->Hasil == NULL)
                                        <div style='background-color:#eee; color:#eee;'>0</div>
                                        @elseif($metode->batas_bawah == NULL && $metode->batas_atas == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @elseif($metode->batas_bawah == NULL && $metode->batas_atas == NULL && $val->Hasil == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @else
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @endif
                                    @else
                                         @if($val->hp != NULL && $val->Hasil != "Tidak mengerjakan" && $metode->batas_bawah != NULL)
                                            @if($batas_bawah_metode == 'Negatif' AND $val->hp == 'Negatif')
                                                <?php
                                                    echo "<div style='background-color:#34a449; color:#34a449;' class='hitung2'>4</div>"."<br>";
                                                ?>  
                                            @elseif($batas_bawah_metode == 'Negatif' AND $val->hp == 'Positif')
                                                <?php
                                                    echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung2'>0</div>"."<br>";
                                                ?>  
                                            @elseif($batas_bawah_metode == 'Positif' AND $val->hp == 'Negatif')
                                                <?php
                                                    echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung2'>0</div>"."<br>";
                                                ?>                                    
                                            @elseif($batas_bawah_metode == 'Positif' AND $val->hp == 'Positif')
                                                <?php
                                                    echo "<div style='background-color:#34a449; color:#34a449;' class='hitung2'>4</div>"."<br>";
                                                ?>  
                                            @else
                                                <?php
                                                    $hasil = str_replace(['+','±'], '0', $val->hp);
                                                    if ($hasil >= $batas_bawah_metode && $hasil <= $batas_atas_metode) {
                                                        echo "<div style='background-color:#34a449; color:#34a449;' class='hitung2'>4</div>"."<br>";
                                                    }else{
                                                        echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung2'>0</div>"."<br>";
                                                    }
                                                ?>
                                            @endif

                                        @elseif($val->hp == NULL)
                                        <div style='background-color:#eee; color:#eee;'>0</div>
                                        @elseif($metode->batas_bawah == NULL && $metode->batas_atas == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @elseif($metode->batas_bawah == NULL && $metode->batas_atas == NULL && $val->hp == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @else
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @endif
                    </tbody>
                    <!-- <tfoot>
                         <tr>
                                <td colspan="5" style="text-align: right;">Skor Rata-rata :</td>
                                @if(count($skoring))
                                @foreach($skoring as $val)
                                    <td>{{$val->skoring}}</td>
                                    <td></td>
                                    <td>{{$val->skoringmetode }}</td>
                                @endforeach
                                @else
                                <td class="isi"></td>
                                <td></td>
                                <td class="isi2"></td>

                                <input type="hidden" name="skoring" id="skoring">
                                <input type="hidden" name="skoringmetode" id="skoringmetode">
                                @endif
                            </tr>
                    </tfoot> -->
                </table><br>
                <table>
                    <tr>
                        <th>Catatan: </th>
                        <td>@if($val->catatan == '')
                            -
                            @else
                            {{ $val->catatan }}
                            @endif
                        </td>
                    </tr>
                </table>
                <br><br>
                    <i style='background-color:#34a449; color:#34a449; width: 50px;' class='hitung2'>&nbsp; &nbsp; &nbsp; </i>
                    Masuk Konsensus.<br>
                    <i style='background-color:#e52d27; color:#e52d27; width: 50px;' class='hitung2'>&nbsp; &nbsp; &nbsp; </i>
                    Keluar Konsensus.<br>
                    <i style='background-color:#ddd; color:#ddd; width: 50px;' class='hitung2'>&nbsp; &nbsp; &nbsp; </i>
                    Tidak ada Hasil Peserta dan Konsensus.<br>
                    <i style='background-color:#333; color:#333; width: 50px;' class='hitung2'>&nbsp; &nbsp; &nbsp; </i>
                    Tidak ada Konsensus.

                        

                    <div style="position: relative; left: 75%; top: -10%;">
                    <p>
                        <div style="position: relative; top: 22px">
                        Jakarta, {{ _dateIndo($evaluasi->tanggal) }}<br>
                        Penanggung Jawab
                        </div>
                        <p>
                            <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'imunologi_ttd.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'imunologi_ttd.png')}}" width="160" style="margin: -40px 0px !important;">
                        </p>

                        <div style="position: relative; top:-20px">
                        dr. Siti Kurnia Eka Rusmiarti, Sp.PK
                        </div>
                    </p>
                    </div>

            </div>
        </div>
    </div>
</div>
