    @extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <a class="btn btn-primary" style="margin: 10px" onclick="javascript:printDiv('datana')">Cetak Grafik <li class="glyphicon glyphicon-print"></li></a>
                <div class="panel-body" id="datana">
                    <style type="text/css">
                      @media print{          
                          .color-red{
                              -webkit-print-color-adjust: exact!important;
                              color: #bf0b23 !important;
                          }

                      }
                    </style>
                    <center><h4>HISTOGRAM HASIL URINALISA SEMUA PESERTA</h4></center>
                    <label>KODE PESERTA :</label>&nbsp;{{$peserta->kode_lebpes}} <br>
                    <label>Siklus :</label>&nbsp;{{$siklus}} - @if($type == 'a') 01 @else 02 @endif<br>
                    <label>Tahun :</label>&nbsp;{{$date}}
                    <hr>
                    <div class="row">
                    <table width="100%">
                        <tr>
                    <?php $no = 0;?>
                    @foreach($parameter as $val)
                    <?php $no++ ;?>
                    <td width="33.3%">
                        <div class="col-md-12">
                            <table style="margin: 10px 0px">
                                <tr>
                                    <th>Parameter</th>
                                    <th>&nbsp;:</th>
                                    <td>&nbsp;{{$val->nama_parameter}}</td>
                                </tr>
                                <tr  style="color: #bf0b23">
                                    <th class="color-red">Target Parameter </th>
                                    <th>&nbsp;:</th>
                                    @if($val->target->batas_bawah == NULL || $val->target->batas_atas == NULL)
                                        <td class="">-</td>
                                    @else
                                        @if($val->id <= 10)
                                            <td>&nbsp;{{$val->target->batas_bawah}} - {{$val->target->batas_atas}}</td>                                    
                                        @else
                                            <td>&nbsp;{{$val->target->batas_bawah}}</td>
                                        @endif
                                    @endif

                                </tr>
                                <tr>
                                    <th>Hasil Saudara </th>
                                    <th>&nbsp;:</th>
                                    @if(count($val->hasilna) && $val->hasilna->hp != '-')
                                    <td>&nbsp;{{$val->hasilna->hp}} *</td>
                                    @else
                                        @if($val->id <= 10)
                                            <td>&nbsp;-</td>
                                        @else
                                            <td>&nbsp;Tidak Mengerjakan</td>
                                        @endif
                                    @endif
                                </tr>
                            </table>
                            <div id="container{{$val->id}}"></div>
                        </div>
                    </td>
                    <?php if ($no == '3') { $no = 0;?>
                        </tr><tr>
                    <?php } ?>
                    @endforeach
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function printDiv(divID) {
    //Get the HTML of div
    var divElements = document.getElementById(divID).innerHTML;
    //Get the HTML of whole page
    var oldPage = document.body.innerHTML;

    //Reset the page's HTML with div's HTML only
    document.body.innerHTML = 
      "<html><head><title></title></head><body>" + 
      divElements + "</body>";

    //Print Page
    window.print();

    //Restore orignal HTML
    document.body.innerHTML = oldPage;

  
}
@foreach($parameter as $val)
    var chart = Highcharts.chart('container{{$val->id}}', {
        chart: {
            renderTo: 'container',
            printWidth: 600
        },
        title: {
            text: '{{$val->nama_parameter}}'
        },
        subtitle: {
            text: 'Plain'
        },
        yAxis: { //--- Primary yAxis
            title: {
                text: 'Jumlah Peserta'
            }
        },
        xAxis: {
            categories: [
            @foreach($val->masing as $data)
                "{{$data->hp}}",
            @endforeach
            ]
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: { 
                    enabled: true,
                    format: '{point.y} {point.a}'
                }
            }
        },

        series: [{type: 'column',name:'Hasil Peserta',

            data: [
            @foreach($val->masing as $data)
                {
                y: {{$data->Jumlah}}, 
                @if(count($val->hasilna))
                    @if($val->hasilna->hp == $data->hp)
                        a: "*",
                    @else
                    @endif
                @else
                @endif
                    <?php if($val->target->rujukan == $data->hp) { ?>
                        color: '#BF0B23'
                    <?php } ?>
                },
            @endforeach
            ],
            showInLegend: true}]

    });
@endforeach
</script>
@endsection