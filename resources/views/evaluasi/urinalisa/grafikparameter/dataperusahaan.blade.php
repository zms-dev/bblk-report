@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Histogram Hasil Seluruh Peserta Siklus {{$siklus}} Tahun {{$tahun}}</div>

                <div class="panel-body">
                    @if(Session::has('message'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ Session::get('message') }}
                      </div>
                    @endif
                    <blockquote style="font-size: 12px">
                        Silahkan mengisi menu EVALUASI-TABEL RUJUKAN apabila data tidak tampil !
                    </blockquote>
                    <table class="table table-bordered">
                        <tr>
                            <th width="5%"><center>No</center></th>
                            <th><center>Bidang</center></th>
                            <th colspan="2" width="15%"><center>View</center></th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>
                        @if($val->id_bidang == '4')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                    @if($val->siklus_1 == 'done')
                                    @if($siklus == 1)
                                        @if($val->pemeriksaan == 'done')
                                            <td>
                                                <a href="{{URL('').$val->Link}}/penilaian-parameter/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-print"></i><br>I-A</center></a>
                                            </td>
                                        @else
                                        <td class="text-center" style="padding: 2%;">Belum Dikirim</td>

                                        @endif
                                        @if($val->pemeriksaan2 == 'done')
                                            <td>
                                                <a href="{{URL('').$val->Link}}/penilaian-parameter/{{$val->id}}?x=b&y=1"><center><i class="glyphicon glyphicon-print"></i><br>I-B</center></a>
                                            </td>
                                        @else
                                        <td class="text-center" style="padding: 2%;">Belum Dikirim</td>

                                        @endif
                                    @else
                                    <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                    @endif
                                @else
                                    @if($val->siklus_2 == 'done')
                                        @if($val->rpr1 == 'done')
                                            <td>
                                                <a href="{{URL('').$val->Link}}/penilaian-parameter/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-print"></i><br>II-A</center></a>
                                            </td>
                                        @else
                                        <td class="text-center" style="padding: 2%;">Belum Dikirim</td>

                                        @endif
                                        @if($val->rpr2 == 'done')
                                            <td>
                                                <a href="{{URL('').$val->Link}}/penilaian-parameter/{{$val->id}}?x=b&y=2"><center><i class="glyphicon glyphicon-print"></i><br>II-B</center></a>
                                            </td>
                                        @else
                                        <td class="text-center" style="padding: 2%;">Belum Dikirim</td>

                                        @endif
                                    @else
                                    <td class="text-center" style="padding: 2%;">Belum Dikirim</td>
                                    @endif
                                @endif
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection