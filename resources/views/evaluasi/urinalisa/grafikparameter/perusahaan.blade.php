@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Histogram Hasil Seluruh Peserta</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="exampleInputEmail1">Tahun</label>
                            <div class="controls input-append date " data-link-field="dtp_input1">
                                <input size="16" type="text" value="" readonly class="form-control form_datetime" name="tahun" id="tahun">
                                <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Siklus </label>
                            <select name="siklus" class="form-control" id="siklus1">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                        </div><br>
                        <div class="col-md-4">
                            <label></label>
                            <button class="btn btn-primary" name="" id="cari">Cari</button><br><br>
                        </div><br>
                    </div>
                    <div class="well well-sm">
                        Keterangan Warna Status :<br>
                        <p><div class="status-danger" style="width: 20px; float: left;">&nbsp;</div> &nbsp; = Status belum selesai</p>
                        <p><div class="status-success" style="width: 20px; float: left;">&nbsp;</div> &nbsp; = Status Sudah selesai</p>
                    </div>
                    <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                        <thead>
                        <tr>
                            <th rowspan="2"><center>Aksi</center></th>
                            <th rowspan="2"><center>No</center></th>
                            <th rowspan="2"><center>Kode Peserta</center></th>
                            <th rowspan="2"><center>Instansi</center></th>
                            <th rowspan="2"><center>Alamat</center></th>
                            <th colspan="2" class="text-center"><center>Status</center></th>
                        </tr>
                        <tr>
                            <th class="text-center"><center>A</center></th>
                            <th class="text-center"><center>B</center></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    });
    </script>
@endif
@stop

@section('scriptBlock')
<script>
$("#cari").click(function(){
    var siklus = $('#siklus1').val();
    var tahun = $('#tahun').val();
    var table = $(".dataTables-data");
    var dataTable = table.DataTable({
    "bDestroy":true,
    responsive:!0,
    "serverSide":true,
    "processing":true,
    "ajax":{
        url : "{{url('penilaian/parameter-urinalisa').'?siklus='}}"+siklus+"{{'&tahun='}}"+tahun,
    },
    dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
    language:{
        paginate:{
            previous:"&laquo;",
            next:"&raquo;"
        },search:"_INPUT_",
        searchPlaceholder:"Search..."
    },
    "columns":[
            {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
            {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
            {"data":"kode_lebpes","name":"kode_lebpes","searchable":true,"orderable":true,"width":"20%"},
            {"data":"nama_lab","name":"nama_lab","searchable":true,"orderable":true},
            {"data":"alamat","name":"alamat","searchable":true,"orderable":true},
            {"data":"posisia","name":"bahan","searchable":true,"orderable":true,"width" : "100px"},
            {"data":"posisib","name":"bahan","searchable":true,"orderable":true,"width" : "100px"},
    ],
    order:[[1,"asc"]]
    });
});
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});
</script>
@stop