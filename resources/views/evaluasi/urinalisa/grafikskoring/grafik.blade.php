@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Skoring Hasil PNPME Seluruh Peserta</div>

                <div class="panel-body" id="datana">
                    <div id="container"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

var chart = Highcharts.chart('container', {
  @foreach($data as $val)
  title: {text: 'GRAFIK SCORING HASIL PNPME BIDANG URINALISA <br>BERDASARKAN KELOMPOK SELURUH PESERTA <br>SIKLUS {{$siklus}} -@if($type == "a") A @else B @endif TAHUN {{$tahun}}'},
  @break
  @endforeach
  subtitle: {text: ''},
  xAxis: {categories: [
      @foreach($data as $val)
      '{{$val->nilainya}}',
      @endforeach]},
  yAxis: {
    title: {
      text : 'Jumlah Peserta'
    }
  },
  plotOptions: {
      series: {
          borderWidth: 0,dataLabels: { 
              enabled: true,
              format:'{point.y}'
          }
      }
  },
  series: [{type: 'column',
      name : 'Kriteria Nilai',
      data: [
      @foreach($data as $val)
          {
          y: {{$val->nilai1}},
          },
      @endforeach
      ],
      showInLegend: true}]
});
</script>
@endsection