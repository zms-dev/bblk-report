@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Hasil Pemeriksaan</div>

                <div class="panel-body" id="datana">
                @if(count($skoring))
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('hasil-pemeriksaan/urinalisasi/evaluasi')}}/print/{{$id}}?x={{$type}}&y={{$siklus}}" >
                @else
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                @endif
                    <table>
                        <tr>
                          <th>Kode Peserta</th>
                          <td>&nbsp; : &nbsp;</td>
                          <td>{{$register->kode_lebpes}}</td>
                        </tr>
                        <tr>
                          <th>Nama Instansi</th>
                          <td>&nbsp; : &nbsp;</td>
                          <td>{{$perusahaan}}</td>
                        </tr>
                    </table><br>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align: center;" rowspan="2">No</th>
                                <th rowspan="2" style="text-align: center;">Parameter</th>
                                <th style="text-align: center;" rowspan="2">Metode</th>
                                <th style="text-align: center;" rowspan="2">Hasil Saudara</th>
                                <!-- <th>Reagen</th> -->
                                <th style="text-align: center;" colspan="2"  style="text-align: center;">Seluruh Peserta</th>
                                <th style="text-align: center;" colspan="2"  style="text-align: center;">Kelompok Metode</th>
                            </tr>
                            <tr>
                                <th style="text-align: center;">Konsensus</th>
                                <th style="text-align: center;">Nilai</th>
                                <th style="text-align: center;">Konsensus</th>
                                <th style="text-align: center;">Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($skoring as $val)
                                <input type="hidden"  name="id" value="{{ $val->id }}">
                            @endforeach
                            @if(count($data))
                            <?php $no = 0; $jumlah = 0;?>

                            @foreach($data as $val)
                            <?php $no++; ?>
                                <td style="text-align: center;">{{$no}}<input type="hidden" name="id_detail[]" value="{{$val->id_detail}}"></td>
                                <td>{!!$val->nama_parameter!!}<input type="hidden" name="parameter_id[]" value="{{ $val->id }}" /></td>
                                <td style="text-align: center;">{!!$val->metode_pemeriksaan!!}<input type="hidden" name="parameter_id[]" value="{{ $val->id }}" /></td>
                                <td style="text-align: center;">
                                    @if($val->id <= 10)
                                        @if($val->Hasil != "-")
                                            {{$val->Hasil}}
                                        @else
                                            -
                                            <?php $jumlah++; ?>
                                        @endif
                                    @else
                                        @if(count($val->hp))
                                            @if(count($val->hasilPemeriksaan))
                                                {!!$val->hp!!}
                                            @else
                                                {{$val->Hasil}}
                                            @endif
                                        @else
                                            Tidak mengerjakan
                                            <?php $jumlah++; ?>
                                        @endif
                                    @endif
                                </td>

                               <!--  <td>
                                    @if($val->id <= 10)
                                        @if($val->Hasil != "-")
                                            @if($val->hp_reagen == '-')
                                            -
                                            @else
                                            {!!$val->imu_reagen!!}<br>
                                            {!!$val->reagen_lain!!}
                                            @endif
                                        @else
                                            -
                                        @endif
                                  
                                        @if(count($val->hp))
                                            @if($val->hp_reagen == '-')
                                                -
                                            @else
                                                {!!$val->imu_reagen!!}<br>
                                                {!!$val->reagen_lain!!}
                                            @endif
                                        @else
                                            -
                                        @endif
                                    @endif
                                </td> -->
                                <td style="text-align: center;">
                                    @if($val->id <= 10)
                                        @if($val->Hasil != "-")
                                            @foreach($val->target as $target)
                                                 {{ $target->batas_bawah }} - {{ $target->batas_atas }}
                                            @endforeach
                                        @else
                                        -
                                        @endif
                                    @else
                                        @if(count($val->hp))
                                            @foreach($val->target as $target)
                                                 @if($target->batas_bawah != 'Negatif' && $target->batas_bawah != 'Positif')
                                                 {{ $target->batas_bawah }} - {{ $target->batas_atas }}
                                                 @else
                                                 {{ $target->batas_bawah }}
                                                @endif
                                            @endforeach
                                        @else
                                        -
                                        @endif
                                    @endif
                                </td>
                       
                                <td style="text-align: center;" class="ngitung">
                                    <?php
                                        $batas_bawah = str_replace(['+','±'], '0', $target->batas_bawah);
                                        $batas_atas = str_replace(['+','±'], '0', $target->batas_atas);
                                          
                                    ?>
                                    @if($val->id <= 10)
                                        @if($val->Hasil != "-" && $val->Hasil != NULL && $val->Hasil != "Tidak mengerjakan" && $target->batas_bawah != NULL)
                                            <?php
                                                $hasil = number_format($val->Hasil, 3);
                                                if ($hasil >= $batas_bawah && $hasil <= $batas_atas) {
                                                    echo "<div style='background-color:#34a449; color:#34a449;' class='hitung'>4</div>"."<br>";
                                                }else{
                                                    echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung'>0</div>"."<br>";
                                                }
                                            ?>
                                        @elseif($val->Hasil == NULL)
                                        <div style='background-color:#eee; color:#eee;'>0</div>
                                        @elseif($target->batas_bawah == NULL && $target->batas_atas == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @elseif($target->batas_bawah == NULL && $target->batas_atas == NULL && $val->Hasil == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @else
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @endif
                                    @else
                                        @if($val->hp != NULL && $val->Hasil != "Tidak mengerjakan" && $target->batas_bawah != NULL)
                                            @if($batas_bawah == 'Negatif' AND $val->hp == 'Negatif')
                                                <?php
                                                    echo "<div style='background-color:#34a449; color:#34a449;' class='hitung'>4</div>"."<br>";
                                                ?>  
                                            @elseif($batas_bawah == 'Negatif' AND $val->hp == 'Positif')
                                                <?php
                                                    echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung'>0</div>"."<br>";
                                                ?>  
                                            @elseif($batas_bawah == 'Positif' AND $val->hp == 'Negatif')
                                                <?php
                                                    echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung'>0</div>"."<br>";
                                                ?>                                    
                                            @elseif($batas_bawah == 'Positif' AND $val->hp == 'Positif')
                                                <?php
                                                    echo "<div style='background-color:#34a449; color:#34a449;' class='hitung'>4</div>"."<br>";
                                                ?>  
                                            @else
                                                <?php
                                                    $hasil = str_replace(['+','±'], '0', $val->hp);
                                                    if ($hasil >= $batas_bawah && $hasil <= $batas_atas) {
                                                        echo "<div style='background-color:#34a449; color:#34a449;' class='hitung'>4</div>"."<br>";
                                                    }else{
                                                        echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung'>0</div>"."<br>";
                                                    }
                                                ?>
                                            @endif

                                        @elseif($val->hp == NULL)
                                        <div style='background-color:#eee; color:#eee;'>0</div>
                                        @elseif($target->batas_bawah == NULL && $target->batas_atas == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @elseif($target->batas_bawah == NULL && $target->batas_atas == NULL && $val->hp == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @else
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @endif
                                    @endif
                                </td>
                                <td style="text-align: center;">
                                    @if($val->id <= 10)
                                        @if($val->Hasil != "-")
                                            @foreach($val->metode as $metode)
                                                 {{ $metode->batas_bawah }} - {{ $metode->batas_atas }}
                                            @endforeach
                                        @else
                                        -
                                        @endif
                                    @else
                                        @if(count($val->hp))
                                            @foreach($val->metode as $metode)
                                                @if($metode->batas_bawah != 'Negatif' && $metode->batas_bawah != 'Positif')
                                                 {{ $metode->batas_bawah }}  - {{ $metode->batas_atas }}
                                                 @else
                                                 {{ $metode->batas_bawah }}
                                                @endif
                                            @endforeach
                                        @else
                                        -
                                        @endif
                                    @endif
                                </td>
                       
                                <td style="text-align: center;" class="ngitung2">
                                    <?php
                                        $batas_bawah_metode = str_replace(['+','±'], '0', $metode->batas_bawah);
                                        $batas_atas_metode = str_replace(['+','±'], '0', $metode->batas_atas);
                                          
                                    ?>
                                    @if($val->id <= 10)
                                        @if($val->Hasil != "-" && $val->Hasil != NULL && $val->Hasil != "Tidak mengerjakan" && $metode->batas_bawah != NULL)
                                            <?php
                                                $hasil = number_format($val->Hasil, 3);
                                                if ($hasil >= $batas_bawah_metode && $hasil <= $batas_atas_metode) {
                                                    echo "<div style='background-color:#34a449; color:#34a449;' class='hitung2'>4</div>"."<br>";
                                                }else{
                                                    echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung2'>0</div>"."<br>";
                                                }
                                            ?>
                                        @elseif($val->Hasil == NULL)
                                        <div style='background-color:#eee; color:#eee;'>0</div>
                                        @elseif($metode->batas_bawah == NULL && $metode->batas_atas == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @elseif($metode->batas_bawah == NULL && $metode->batas_atas == NULL && $val->Hasil == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @else
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @endif
                                    @else
                                         @if($val->hp != NULL && $val->Hasil != "Tidak mengerjakan" && $metode->batas_bawah != NULL)
                                            @if($batas_bawah_metode == 'Negatif' AND $val->hp == 'Negatif')
                                                <?php
                                                    echo "<div style='background-color:#34a449; color:#34a449;' class='hitung2'>4</div>"."<br>";
                                                ?>  
                                            @elseif($batas_bawah_metode == 'Negatif' AND $val->hp == 'Positif')
                                                <?php
                                                    echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung2'>0</div>"."<br>";
                                                ?>  
                                            @elseif($batas_bawah_metode == 'Positif' AND $val->hp == 'Negatif')
                                                <?php
                                                    echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung2'>0</div>"."<br>";
                                                ?>                                    
                                            @elseif($batas_bawah_metode == 'Positif' AND $val->hp == 'Positif')
                                                <?php
                                                    echo "<div style='background-color:#34a449; color:#34a449;' class='hitung2'>4</div>"."<br>";
                                                ?>  
                                            @else
                                                <?php
                                                    $hasil = str_replace(['+','±'], '0', $val->hp);
                                                    if ($hasil >= $batas_bawah_metode && $hasil <= $batas_atas_metode) {
                                                        echo "<div style='background-color:#34a449; color:#34a449;' class='hitung2'>4</div>"."<br>";
                                                    }else{
                                                        echo "<div style='background-color:#e52d27; color:#e52d27;' class='hitung2'>0</div>"."<br>";
                                                    }
                                                ?>
                                            @endif
                                      
                                        @elseif($val->hp == NULL)
                                        <div style='background-color:#eee; color:#eee;'>0</div>
                                        @elseif($metode->batas_bawah == NULL && $metode->batas_atas == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @elseif($metode->batas_bawah == NULL && $metode->batas_atas == NULL && $val->hp == NULL)
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @else
                                        <div style='background-color:#333; color:#333;'>0</div>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @endif
                            <tr>
                                <td  colspan="5" style="text-align: right;">Skor Rata-rata :</td>
                                @if(count($skoring))
                                @foreach($skoring as $val)
                                    <td style="text-align: center;">{{$val->skoring}}</td>
                                    <td></td>
                                    <td style="text-align: center;">{{$val->skoringmetode }}</td>
                                @endforeach
                                @else
                                <td class="isi"></td>
                                <td style="text-align: center;"></td>
                                <td class="isi2"></td>

                                <input type="hidden" name="skoring" id="skoring">
                                <input type="hidden" name="skoringmetode" id="skoringmetode">
                                @endif
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" value="{{$data[0]->target[0]->bahan}}" name="bahan">

              <div class="form-horizontal">
                        <label>Catatan:</label>
                        <div class="row">
                            <div class="col-md-6">
                                @if(count($skoring))
                                    @foreach($skoring as $catatan)
                                        <textarea name="catatan" class="form-control" placeholder="-">{{ $catatan->catatan }}</textarea>
                                    @endforeach
                                @else
                                    <textarea name="catatan" class="form-control" placeholder="-"></textarea>
                                @endif
                            </div>
                        </div>
                    </div><br>

                @if(count($skoring))
                    <input type="submit" name="simpan" class="btn btn-primary" value="Print">
                    <input type="submit" name="simpan" class="btn btn-danger" value="Batal Evaluasi">
                @else
                    <input type="submit" name="simpan" class="btn btn-primary" value="Simpan">
                @endif
                {{ csrf_field() }}
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
form=document.getElementById("someForm");
var total2=[0];
var totalmetode=[0];
$(document).ready(function(){
    var $dataRows=$("#datana tr:not('.titlerowna')");
    $dataRows.each(function() {
        $(this).find('.ngitung .hitung').each(function(i){
            if (isNaN($(this).html())) {
            }else{
                total2[i]+=parseInt( $(this).html());
                console.log(total2[i])
            }
        });
        $(this).find('.ngitung2 .hitung2').each(function(i){
            if (isNaN($(this).html())) {
            }else{
                totalmetode[i]+=parseInt( $(this).html());
                // console.log(totalmetode[i])
            }
        });
    });
    $("#datana td.isi").each(function(i){
        var data = total2[i] / ({{$no}} - {{$jumlah}});
        $(this).html(data.toFixed(2));
        $('#skoring').val(data.toFixed(2));
    });
    $("#datana td.isi2").each(function(i){
        var data = totalmetode[i] / ({{$no}} - {{$jumlah}});
        $(this).html(data.toFixed(2));
        $('#skoringmetode').val(data.toFixed(2));
    });
});
</script>
@endsection
