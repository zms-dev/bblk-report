@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Penggunaan Reagen Peserta</div>
                <a class="btn btn-primary" style="margin: 10px" onclick="javascript:printDiv('datana')">Cetak Grafik <li class="glyphicon glyphicon-print"></li></a>
                <div class="panel-body" id="datana">
                    <style type="text/css">
                      @media print{          
                          .color-red{
                              -webkit-print-color-adjust: exact!important;
                              color: #bf0b23 !important;
                              background-color: #bf0b23 !important;
                          }

                      }
                    </style>
                    @foreach($reagen as $val)
                    <table style="margin: 10px 0px">
                        <tr>
                            <th>Kode Peserta</th>
                            <th>&nbsp;:</th>
                            <td>&nbsp;{{$val->kode_lab}}</td>
                        </tr>
                        <tr>
                            <th>Siklus </th>
                            <th>&nbsp;:</th>
                            <td>&nbsp;{{$siklus}} - @if($type == 'a') A @else B @endif</td>
                        </tr>
                        <tr>
                            <th>Tahun </th>
                            <th>&nbsp;:</th>
                            <td>&nbsp;{{$date}}</td>
                        </tr>
                        <tr>
                            <th><kbd class="color-red" style="background-color: #bf0b23"></kbd></th>
                            <th>&nbsp;:</th>
                            <td>&nbsp;Posisi Peserta</td>
                        </tr>
                    </table>
                    @break
                    @endforeach
                    <div id="container"></div><br>
                    <div id="container1"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function printDiv(divID) {
    //Get the HTML of div
    var divElements = document.getElementById(divID).innerHTML;
    //Get the HTML of whole page
    var oldPage = document.body.innerHTML;

    //Reset the page's HTML with div's HTML only
    document.body.innerHTML = 
      "<html><head><title></title></head><body>" + 
      divElements + "</body>";

    //Print Page
    window.print();

    //Restore orignal HTML
    document.body.innerHTML = oldPage;

  
}
var chart = Highcharts.chart('container', {
    @foreach($reagen as $val)
    title: {text: 'HISTOGRAM PENGGUNAAN REAGEN BIDANG URINALISA<br>BERDASARKAN KELOMPOK SELURUH PESERTA'},
    @break
    @endforeach
    subtitle: {text: 'Batang Peserta Berwarna Merah'},
    xAxis: {categories: [
        @foreach($reagen as $val)
        '{{$val->reagen}}',
        @endforeach]},  
    yAxis: {
        title: {
            text: 'Jumlah Peserta'
        }
    },
    plotOptions: {
        series: {
            borderWidth: 0,dataLabels: { 
                enabled: true,
                format:'{point.y}'
            }
        }
    },
    series: [{type: 'column',
        name: 'REAGEN PESERTA',
        data: [
        @foreach($reagen as $val)
            {
            y: {{$val->jumlah}},
                @foreach($data as $valu)
                <?php if($valu->id == $val->id) { ?>
                    color: '#BF0B23'
                <?php } ?>
                @break
                @endforeach
            },
        @endforeach
        ],
        showInLegend: true}]
});

var chart = Highcharts.chart('container1', {
    @foreach($kehamilan as $val)
    title: {text: 'HISTOGRAM PENGGUNAAN REAGEN TEST KEHAMILAN BIDANG URINALISA <br>BERDASARKAN KELOMPOK SELURUH PESERTA'},
    @break
    @endforeach
    subtitle: {text: 'Batang Peserta Berwarna Merah'},
    xAxis: {categories: [
        @foreach($kehamilan as $val)
        '{{$val->reagen}}',
        @endforeach]},
    yAxis: {
        title: {
            text: 'Jumlah Peserta'
        }
    },
    plotOptions: {
        series: {
            borderWidth: 0,dataLabels: { 
                enabled: true,
                format:'{point.y}'
            }
        }
    },
    series: [{type: 'column',
        name: 'REAGEN PESERTA',
        data: [
        @foreach($kehamilan as $val)
            {
            y: {{$val->jumlah}},
                @foreach($data2 as $valu)
                <?php if($valu->id == $val->id) { ?>
                    color: '#BF0B23'
                <?php } ?>
                @endforeach
            },
        @endforeach
        ],
        showInLegend: true}]

});
</script>
@endsection