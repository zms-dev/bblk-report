@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rujukan Bakteri</div>
                <div class="panel-body">
                   @if(Session::has('message'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ Session::get('message') }}
                    </div>
                  @endif
                    <form action="{{url('/evaluasi/input-rujukan/bakteri/insert')}}" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                              <label for="exampleInputEmail1">Jenis Pemeriksaan</label>
                              <select class="form-control" required="" name="jenis_pemeriksaan">
                                <option value=""></option>
                                <option value="Identifikasi">Identifikasi</option>
                                <option value="Uji Kepekaan Antibiotik">Uji Kepekaan Antibiotik</option>
                              </select>
                          </div>
                          <div class="form-group">
                              <label for="exampleInputEmail1">Lembar</label>
                              <select class="form-control" required="" name="lembar">
                                <option value=""></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                              <label for="exampleInputEmail1">Siklus</label>
                              <select class="form-control" required="" name="siklus">
                                <option value=""></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                              </select>
                          </div>
                          <div class="form-group">
                              <label for="exampleInputEmail1">Tahun</label><br>
                              <input type="text" name="tahun" class="form-control" id="year">
                          </div>
                        </div>

                        <div class="col-md-12">
                          <label>Rujukan</label><br>
                          <div class="form-group">
                              <textarea id="ckeditor" required="" name="rujukan"></textarea>
                                
                          </div>
                        </div>
                      </div>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Simpan</button>
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
@section('scriptBlock')
<script type="text/javascript">
    $('#year').datepicker({
        format: 'yyyy',
        viewMode: "years", //this
        minViewMode: "years",//and this
        autoclose : true,
    });

    CKEDITOR.replace( 'ckeditor',
            {
                toolbar : 'Basic', /* this does the magic */
                enterMode : CKEDITOR.ENTER_BR,
                shiftEnterMode : CKEDITOR.ENTER_P
            });
</script>
@stop