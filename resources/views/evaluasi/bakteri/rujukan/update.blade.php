@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Update Rujukan Urinalisa</div>
          <div class="panel-body">
            @foreach($data as $val)
            <form action="{{url('/evaluasi/input-rujukan/bakteri/edit/').'/'.$val->id}}" method="post" enctype="multipart/form-data">
              <div class="row">

                        <div class="col-md-6">
                          <div class="form-group">
                              <label for="exampleInputEmail1">Jenis Pemeriksaan</label>
                              <select class="form-control" name="jenis_pemeriksaan" required="">
                                <option value="{{ $val->jenis_pemeriksaan }}">{{$val->jenis_pemeriksaan}}</option>
                                <option value="Identifikasi">Identifikasi</option>
                                <option value="Uji Kepekaan Antibiotik">Uji Kepekaan Antibiotik</option>
                              </select>
                          </div>
                          <div class="form-group">
                              <label for="exampleInputEmail1">Lembar</label>
                              <select class="form-control" name="lembar" required="">
                                <option value="{{ $val->lembar }}">{{ $val->lembar }}</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                              <label for="exampleInputEmail1">Siklus</label>
                              <select class="form-control" name="siklus" required="">
                                <option value="{{ $val->siklus }}">{{ $val->siklus }}</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                              </select>
                          </div>
                         <div class="form-group">
                              <label for="exampleInputEmail1">Tahun</label><br>
                              <input type="text" name="tahun" class="form-control" value="{{ $val->tahun }}" id="year">
                          </div>
                        </div>

                        <div class="col-md-12">
                          <label>Rujukan</label><br>
                          <div class="form-group">
                              <textarea id="ckeditor" name="rujukan" required="">{!! $val->rujukan !!}</textarea>
                                
                          </div>
                        </div>
                      </div>
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <button type="submit" class="btn btn-info">Update</button>
            </form>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scriptBlock')
<script type="text/javascript">
      $('#year').datepicker({
        format: 'yyyy',
        viewMode: "years", //this
        minViewMode: "years",//and this
        autoclose : true,
    });
    CKEDITOR.replace( 'ckeditor',
            {
                toolbar : 'Basic', /* this does the magic */
                enterMode : CKEDITOR.ENTER_BR,
                shiftEnterMode : CKEDITOR.ENTER_P
            });
</script>
@stop