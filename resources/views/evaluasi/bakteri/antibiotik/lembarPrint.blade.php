<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;   
    text-align: center;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
</style>
<table width="100%" cellpadding="0" border="0">
    <thead>
        <tr>
            <th>
                <img alt="" src="http://webdesign.zamasco.com/img/logo-kemenkes.png" height="120px">
            </th>
            <th width="100%">
                <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                <span style="font-size: 12px">PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL UJI KEPEKAAN ANTIBIOTIK {{$lembar}} TAHUN {{$years}}</span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -25px; margin-left: -42px;"> 
                    Penyelenggara : Balai Besar Laboratorium Kesehatan Jakarta
                                                Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560
                                                Telp. (021) 4212524, 42804339 Facsimile (021) 4245516
                                                Email : bblkjakarta@yahoo.co.id
                </pre>
            </th>
        </tr>
        <tr>
            <th colspan="2"><hr></th>
        </tr>
    </thead>
</table>

<center><label><b>LAMPIRAN UJI KEPEKAAN ANTIBIOTIK<br>
PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br> UJI KEPEKAAN ANTIBIOTIK LEMBAR {{$lembar}} TAHUN {{$years}}
</b><input type="hidden" name="type" value="{{$lembar}}"></label></center><br>
<form class="form-horizontal" method="post" enctype="multipart/form-data">

<br><br>

<table width="100%" cellpadding="0" border="1">
    <tr>
        <th>Jenis Antibiotik</th>
        <th>Interpretasi hasil</th>
        <th>Penilaian</th>
    </tr>

    @foreach($ujiantibiotik as $value)
        <input type="hidden"  name="id[]" value="{{ $value->id }}">
    @endforeach
    
    <?php $i=0?>
    @foreach($kepekaan as $peka)
    
    <tr>
        {{-- <td hidden=""><input type="text" name="id_anti[]" value="{{ $peka->id_anti }}"></td>
        <td hidden=""><input type="text" name="antibiotik[]" value="{{$peka->antibiotik}} @if($peka->lain_lain != ''): {{$peka->lain_lain}}@endif"></td>
        <td hidden=""><input type="text" name="hasil[]" value="{{$peka->hasil1}}"></td>
         --}}
        <td>{{$peka->antibiotik}} @if($peka->lain_lain != ''): {{$peka->lain_lain}}@endif</td>
        <td>{{$peka->hasil1}}</td>
        <td>
            @if(count($ujiantibiotik))
                @foreach($ujiantibiotik as $valu)
                        @if($valu->id_antibiotik == $peka->id_anti)
                       {{$valu->penilaian}}
                        @endif
                @endforeach
            @endif
                
            </select>
        </td>
    </tr>
    @endforeach
</table>
{{-- <br>
<b>Keterangan :</b> &nbsp;{{$status->status}}<br>
 --}}

<br>
<div style="margin-left: 552px;">
<div style="position: relative; top: 17px">
Jakarta, 31 Juli 2018<br>
Manajer Teknis
</div>
<p>
    <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'TTD4.png')}}" width="80" height="80" style="margin-left: 20px !important;">
</p>

<div style="position: relative; top:-20px">
Ita Andayani, S.St<br>
NIP.197004101992032003
</div>
</div><br>

<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>



    