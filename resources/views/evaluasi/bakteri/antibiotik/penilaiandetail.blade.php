@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Evaluasi Bakteri</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Bidang</th>
                            <th >Siklus&nbsp;1</th>
                            <th >Siklus&nbsp;2</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->bidang == '13')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                <td class="text-center">
                                    @if(($val->pemeriksaan == 'done') && ($val->status_data1 == '2'))
                                        <a href="{{url('evaluasi/antibiotik-lembar').'/'.$val->id.'?y=1&lembar=1'}}" class="go-three"><center><i class="glyphicon glyphicon-print"></i>&nbsp;1</center></a>
                                        <a href="{{url('evaluasi/antibiotik-lembar').'/'.$val->id.'?y=1&lembar=2'}}" class="go-three"><center><i class="glyphicon glyphicon-print"></i>&nbsp;2</center></a>
                                        <a href="{{url('evaluasi/antibiotik-lembar').'/'.$val->id.'?y=1&lembar=3'}}" class="go-three"><center><i class="glyphicon glyphicon-print"></i>&nbsp;3</center></a>
                                    @else
                                       Belum Dikirim
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if(($val->pemeriksaan2 == 'done') && ($val->status_data2 == '2'))
                                    <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=2" target="_blank"><center><i class="glyphicon glyphicon-print"></i></center></a>
                                    @else
                                        Belum Dikirim
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection