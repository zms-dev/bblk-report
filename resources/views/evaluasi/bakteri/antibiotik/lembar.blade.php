@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Uji Kepekaan Antibiotik</div>
                <div class="panel-body">
                   @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ Session::get('message') }}
                    </div>
                  @endif
                    <form class="form-horizontal"  method="post" enctype="multipart/form-data">

                     <table class="table table-bordered">
                        <tr>
                            <th>Jenis Antibiotik</th>
                            <th>Interpretasi hasil</th>
                            <th>Penilaian</th>
                        </tr>

                        @foreach($ujiantibiotik as $value)
                            <input type="hidden"  name="id[]" value="{{ $value->id }}">
                        @endforeach
                        
                        <?php $i=0?>
                        @foreach($val->kepekaan as $peka)
                        
                        <tr>
                        	<td hidden=""><input type="text" name="id_anti[]" value="{{ $peka->id_anti }}"></td>
                            <td hidden=""><input type="text" name="antibiotik[]" value="{{$peka->antibiotik}} @if($peka->lain_lain != ''): {{$peka->lain_lain}}@endif"></td>
                            <td hidden=""><input type="text" name="hasil[]" value="{{$peka->hasil1}}"></td>
                            
                            <td>{{$peka->antibiotik}} @if($peka->lain_lain != ''): {{$peka->lain_lain}}@endif</td>
                            <td>{{$peka->hasil1}}</td>
                            <td><select name="penilaian[]" class="form-control">
                            	@if(count($ujiantibiotik))
	                            	@foreach($ujiantibiotik as $valu)
	                                        @if($valu->id_antibiotik == $peka->id_anti)
	                                        <option value="{{$valu->penilaian}}">{{$valu->penilaian}}</option>
	                                        @endif
	                                @endforeach
								@endif
								<option></option>
								<option value="Benar">Benar</option>
               					<option value="Salah">Salah</option>
                            		
                            	</select>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                      {{ csrf_field() }}
                        @if(count($ujiantibiotik))
                            <a href="{{url('evaluasi/bakteri/grafik-keterangan-peserta/print').'/'.$id.'?y=&lembar='.$lembar}}" target="_blank">
		                        <input type="button" class="btn btn-primari" value="Print" name="print"/>
		                    </a>
                            <input type="submit" name="simpan" class="btn btn-primary" style="" value="Update">
                        @else
                            <input type="submit" name="simpan" class="btn btn-primary" style="" value="Simpan">
                        @endif
                  </form>
              </div>
          </section>
      </div>
    </div>
  </section>
</section>
@endsection
@section('scriptBlock')
<script type="text/javascript">
    $('#year').datepicker({
        format: 'yyyy',
        viewMode: "years", //this
        minViewMode: "years",//and this
        autoclose : true,
    });

    CKEDITOR.replace( 'ckeditor',
            {
                toolbar : 'Basic', /* this does the magic */
                enterMode : CKEDITOR.ENTER_BR,
                shiftEnterMode : CKEDITOR.ENTER_P
            });
</script>
@stop