@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Grafik Jawaban Peserta</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" target="_blank">
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select class="form-control" name="siklus" required>
                            <option></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div><br>
                      <div>
                         	<label for="exampleInputEmail1">Tahun</label><br>
                        	<input type="text" name="tahun" class="form-control" id="year">
                      </div><br>
                      <div>
                          <label for="exampleInputEmail1">Penilaian Evaluasi</label>
                          <select class="form-control" name="jenis_pemeriksa" required>
                            <option></option>
                            <option value="Identifikasi">Identifikasi</option>
                            <option value="Uji Kepekaan Antibiotik">Uji Kepekaan Antibiotik</option>
                          </select>
                      </div><br>
                    <input type="submit" name="proses" class="btn btn-primary" value="Proses">
                    {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scriptBlock')
<script type="text/javascript">
    $('#year').datepicker({
        format: 'yyyy',
        viewMode: "years", //this
        minViewMode: "years",//and this
        autoclose : true,
    });
</script>
@stop