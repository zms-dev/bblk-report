@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Rekap Hasil Peserta Bakteri</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Bidang</th>
                            <th colspan="1">Siklus&nbsp;1</th>
                            <th colspan="1">Siklus&nbsp;2</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->bidang == '13')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                @if($val->siklus_1 == 'done')
                                    <td><a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=1&lembar=1" class="go-three"><center><i class="glyphicon glyphicon-print"></i>&nbsp;1</center></a>
                                    <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=1&lembar=2" class="go-three"><center><i class="glyphicon glyphicon-print"></i>&nbsp;2</center></a>
                                    <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=1&lembar=3" class="go-three"><center><i class="glyphicon glyphicon-print"></i>&nbsp;3</center></a></td>
                                @else
                                <td class="text-center" style="padding: 2%">Belum Dikirim</td>
                                @endif
                                @if($val->siklus_2 == 'done')
                                    <td><a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=2&lembar=1" class="go-three"><center><i class="glyphicon glyphicon-print"></i>&nbsp;1</center></a>
                                    <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=2&lembar=2" class="go-three"><center><i class="glyphicon glyphicon-print"></i>&nbsp;2</center></a>
                                    <a href="{{URL('').$val->Link}}/print/{{$val->id}}?y=2&lembar=3" class="go-three"><center><i class="glyphicon glyphicon-print"></i>&nbsp;3</center></a></td>
                                @else
                                @else
                                <td class="text-center" style="padding: 2%">Belum Dikirim</td>
                                @endif
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
