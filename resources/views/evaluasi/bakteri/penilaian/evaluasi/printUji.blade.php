<style type="text/css">
@page {
margin: 150px 50px 80px 50px;
}
body{
    font-family: Helvetica;
    font-size: 10px;
}
table.utama{
    border-collapse: collapse;
    width: 100%;
}

table.utama th, table.utama td  {
    padding: 5px;
    text-align: left;
    border: 1px solid #ddd;
}

#header { 
    position: fixed; 
    border-bottom:1px solid gray;
    padding-top: -140px
}
#footer { 
    position: fixed; 
    border-top:1px solid gray;
    bottom: 0px;
    width: 100%;
}
</style>
<div id="header">
    <table width="100%" cellpadding="0" border="0" >
    <thead>
        <tr>
            <th>
                <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="120px">
            </th>
            <th width="100%">
                <span style="font-size: 16px"><b>KEMENTERIAN KESEHATAN RI</b></span><br>
                <span style="font-size: 12px">PROGRAM NASIONAL PEMANTAPAN EVALUASI BAKTERI UJI KEPEKAAN ANTIBIOTIK {{$siklus}} TAHUN {{$years}}</span>
                <pre style="font-family: Helvetica;font-size: 12px; padding: -25px; margin-left: -42px;"> 
                    Penyelenggara : Balai Besar Laboratorium Kesehatan Jakarta
                                                Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560
                                                Telp. (021) 4212524, 42804339 Facsimile (021) 4245516
                                                Email : bblkjakarta@yahoo.co.id
                </pre>
            </th>
        </tr>
        <tr>
            <th colspan="2"><hr></th>
        </tr>
    </thead>
</table>
</div>

<center><label><b>LAMPIRAN EVALUASI BAKTERI UJI KEPEKAAN ANTIBIOTIK<br>
PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL <br> EVALUASI BAKTERI UJI KEPEKAAN ANTIBIOTIK SIKLUS {{$siklus}} TAHUN {{$years}}
</b><input type="hidden" name="type" value="{{$siklus}}"></label></center><br>
<form class="form-horizontal" method="post" enctype="multipart/form-data">

<br><br>

<table class="table utama"  id="example">
    <thead>
        <tr class="titlerowna">
            <th>Kode Soal</th>
            <th>Jenis Pemeriksaan</th>
            <th>Nilai Rujukan</th>
            <th>Jawaban Peserta</th>
            <th>Skor</th>
        </tr>
    </thead>
    <tbody>
       
        <?php $no = 0; $i=0;?>
        @foreach($data as $datas)
        <?php $no++; ?>  
        <tr>
            <td class="" rowspan="">0{{ $datas->lembar }}</td>
            <td>{{ $datas->jenis_pemeriksaan }}</td>
            <td>{!! $datas->rujukan !!}</td>
            @if($datas->jenis_pemeriksaan == "Identifikasi")
            <td>{!! $datas->spesies_auto !!}</td>
            @elseif($datas->jenis_pemeriksaan== "Uji Kepekaan Antibiotik")
            <td>{!! $datas->spesies_kultur !!}</td>
            @endif
            <td >
                @if(count($evaluasi))
                    <?php $skor = 0; ?>
                    @if(!empty($evaluasi[$i]->id_data_antibiotik))
                        @if($evaluasi[$i]->id_data_antibiotik == $datas->id)
                        	{{$evaluasi[$i]->skor}}
                        @endif
                    @endif
                @endif
            </td>
        </tr>
        <?php
         $i++;?>
        @endforeach

    </tbody>
</table>
<br>
<b>Keterangan :</b> &nbsp;{{$status->keterangan}}<br>
<br>
<div style="margin-left: 552px;">
<div style="position: relative; top: 17px">
Jakarta, 31 Juli 2018<br>
Manajer Teknis
</div>
<p>
    <img src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'TTD4.png')}}" width="80" height="80" style="margin-left: 20px !important;">
</p>

<div style="position: relative; top:-20px">
Ita Andayani, S.St<br>
NIP.197004101992032003
</div>
</div><br>


<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>



    