@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Hitung Z-Scroe Kimia Air</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Bidang</th>
                            <th>Siklus&nbsp;1</th>
                            <th>Siklus&nbsp;2</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->bidang == '11')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}<br>{{$val->Parameter}}</td>
                                <td class="text-center" style="padding: 2%;">
                                    @if(($val->pemeriksaan == 'done') && ($val->siklus_1 == 'done'))
                                    <a href="{{URL('').$val->Link}}/evaluasi/{{$val->id}}?x=a&y=1"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                    @else
                                    Belum Dikirim
                                    @endif
                                </td>
                                <td class="text-center" style="padding: 2%;">
                                    @if(($val->pemeriksaan == 'done') && ($val->siklus_2 == 'done'))
                                    <a href="{{URL('').$val->Link}}/evaluasi/{{$val->id}}?x=a&y=2"><center><i class="glyphicon glyphicon-pencil"></i></center></a>
                                    @else
                                    Belum Dikirim
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection