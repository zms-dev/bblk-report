@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sertifikat KIMKES</div>

                <div class="panel-body">
                	<form class="form-horizontal" method="get" enctype="multipart/form-data">
                          <div>
                              <label for="exampleInputEmail1">Pilih Tanggal Sertifikat :</label>
                              <input type="text" name="tanggal" id="tanggal" class="datepick form-control" required="" value="{{ date('Y-m-d') }}">

                          </div><br>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Bidang</th>
                            <th>Siklus&nbsp;1</th>
                            <th>Siklus&nbsp;2</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>

                        @if($val->bidang == '11')
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$val->Bidang}}</td>
                                <td class="text-center" style="padding: 2%;">
                                    @if(($val->pemeriksaan == 'done') && ($val->siklus_1 == 'done'))
                                    <a href="" class="go-link" data-id="{{$val->perusahaan_id}}" data-x="a" data-y="1" data-link="{{$val->Link}}"><center><i class="glyphicon glyphicon-pencil"></i><br></center></a>
                                    @else
                                    Belum Dikirim
                                    @endif
                                </td>
                                <td class="text-center" style="padding: 2%;">
                                    @if(($val->pemeriksaan == 'done') && ($val->siklus_2 == 'done'))

                                    <a href="" class="go-link" data-id="{{$val->perusahaan_id}}" data-x="a" data-y="2" data-link="{{$val->Link}}"><center><i class="glyphicon glyphicon-pencil"></i><br></center></a>
                                    @else
                                    Belum Dikirim
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
              		</form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".go-link").on("click",function(e){
        e.preventDefault()
        var x = $(this).data("x");
        var y = $(this).data("y");
        var idLink = $(this).data("id");
        var link = $(this).data("link");
        var tanggal = $("#tanggal").val();
        var baseUrl = "{{URL('')}}"+link+"/sertifikat/print/"+idLink+"?x="+x+"&y="+y+"&tanggal="+tanggal;
        var url = baseUrl;
        console.log(url);
        window.location.href = url;
    })
</script>
@endsection