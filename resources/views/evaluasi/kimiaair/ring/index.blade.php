@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_laporan')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Input Range Per Parameter (Kimia Air)</div>
                <div class="panel-body">
                @if(Session::has('message'))
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('message') }}
                  </div>
                @endif
                    <form class="form-horizontal" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Parameter</label>
                              <select class="form-control" name="parameter">
                                <option></option>
                                @foreach($parameter as $val)
                                <option value="{{$val->id}}">{{$val->nama_parameter}}</option>
                                @endforeach
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Siklus</label>
                              <select class="form-control" name="siklus">
                                <option></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Range 1</label>
                              <input type="text" name="ring1" class="form-control">
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Range 2</label>
                              <input type="text" name="ring2" class="form-control">
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Range 3</label>
                              <input type="text" name="ring3" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div>
                              <label for="exampleInputEmail1">Tahun</label>
                              <select class="form-control" name="tahun">
                                <option></option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                              </select>
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Range 4</label>
                              <input type="text" name="ring4" class="form-control">
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Range 5</label>
                              <input type="text" name="ring5" class="form-control">
                          </div>
                          <div>
                              <label for="exampleInputEmail1">Range 6</label>
                              <input type="text" name="ring6" class="form-control">
                          </div>
                        </div>
                      </div>
                      <br>
                    <input type="submit" name="proses" class="btn btn-primary" value="Simpan">
                    {{ csrf_field() }}
                    </form>
                    <br>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th rowspan="2">Parameter</th>
                          <th rowspan="2">Tahun</th>
                          <th rowspan="2">Siklus</th>
                          <th colspan="6"><center>Ring</center></th>
                          <th rowspan="2">Tipe</th>
                          <th rowspan="2">Aksi</th>
                        </tr>
                        <tr>
                          <th>1</th>
                          <th>2</th>
                          <th>3</th>
                          <th>4</th>
                          <th>5</th>
                          <th>6</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($sd as $val)
                        <tr>
                          <td>{{$val->nama_parameter}}</td>
                          <td>{{$val->tahun}}</td>
                          <td>{{$val->siklus}}</td>
                          <td>{{$val->ring1}}</td>
                          <td>{{$val->ring2}}</td>
                          <td>{{$val->ring3}}</td>
                          <td>{{$val->ring4}}</td>
                          <td>{{$val->ring5}}</td>
                          <td>{{$val->ring6}}</td>
                          <td>@if($val->tipe == 'a') 01 @else 02 @endif</td>
                          <td>
                              <a href="{{URL('evaluasi/input-ring/hematologi/edit').'/'.$val->id}}" style="float: left; margin-bottom: 5px">
                                <button class="btn btn-primary btn-xs">
                                  <i class="glyphicon glyphicon-pencil"></i>
                                </button>
                              </a> &nbsp;
                              <a href="{{URL('evaluasi/input-ring/hematologi/delete').'/'.$val->id}}">
                                <button class="btn btn-danger btn-xs">
                                  <i class="glyphicon glyphicon-trash"></i>
                                </button>
                              </a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $sd->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection