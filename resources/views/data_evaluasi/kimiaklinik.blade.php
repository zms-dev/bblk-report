@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_hasil_evaluasi')
@section('content')
        <!-- Page Content  -->
        <div id="content">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fa fa-align-justify"></i>
                        <span></span>
                    </button>
                </div>
                <label  style="margin: 10px 20px 0px 0px; font-size: 28px; position: absolute; top: 0; right: 0">KIMIA KLINIK</label>
          </nav>
                 
          <div class="line"></div>
          <div class="row">
            <div class="col-md-12">
              
              <div class="col-md-12" style="text-align:center;">

                    <p>
                        Laporan Penyelenggaraan PNPME Kimia klinik
                    </p>
                        <a href="{{url('/laporan-akhir-kimiaklinik/data-evaluasi/')}}/{{$id}}" target="_blank" class="btn btn-info" style="display: none;'">Sampul Laporan</a>
                        <button class="btn-show btn btn-info" style="width: 170px">Laporan</button>
                      {{--   <a href="{{url('/laporan-akhir-kimiaklinik/data-evaluasi/penutup/')}}/{{$id}}" class="btn btn-info" target="_blank">Penutup Laporan</a> --}}

                        <br><br>
                        <div class="myText">
                          <div class="row">
                              <div class="col-md-8 col-md-offset-2">
                                  <div class="panel panel-default">
                                      <div class="panel-heading">Data Upload Laporan Akhir</div>
                                      <div class="panel-body">
                                          
                                          <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                                              <thead>
                                              <tr>
                                                  <th>No</th>
                                                  <th>File</th>
                                                  <th>Tahun</th>
                                                  <th>Siklus</th>
                                                  <th>Parameter</th>
                                                  <th>Aksi</th>
                                              </tr>
                                              </thead>
                                              <tbody>
                                              </tbody>
                                          </table>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="line"></div>
                    @if(count($data) > 0)
                    @foreach($data as $val)
                    <?php 
                      if($val->siklus_1 == 'done'){
                        $siklus1='1';
                      }elseif($val->siklus_2 == 'done'){
                        $siklus2='2';
                      }
                     ?>
                    @if($val->bidang == '2')
                    <div class="row" style="">
                        <div class="col-md-12" >
                            <div class="col-md-12" style="text-align:center;">
                                <p>
                                    Hasil Peserta
                                </p>
                                <div class="btn-group">
                                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 170px">
                                    Cetak Hasil Evaluasi <span class="caret"></span>
                                  </button>

                                  <ul class="dropdown-menu">
                                  @if($siklus == "1")
                                    @if($val->siklus_1 == 'done')
                                      @if($val->pemeriksaan == 'done')
                                        @if($val->status_data1 == 2)
                                          <li><a href="{{url('kimia-klinik/data-evaluasi/')}}/{{$id}}?x=a&y=1" target="_blank">I-A</a></li>
                                        @endif
                                      @endif
                                      @if($val->pemeriksaan2 == 'done')
                                        @if($val->status_data2 == 2)
                                          <li><a href="{{url('kimia-klinik/data-evaluasi/')}}/{{$id}}?x=b&y=1" target="_blank">I-B</a></li>
                                        @endif
                                      @endif
                                    @endif
                                  @else
                                    @if($val->siklus_2 == 'done')
                                      @if($val->rpr1 == 'done')
                                        @if($val->status_datarpr1 == 2)
                                          <li><a href="{{url('kimia-klinik/data-evaluasi/')}}/{{$id}}?x=a&y=2" target="_blank">II-A</a></li>
                                        @endif
                                      @endif
                                      @if($val->rpr2 == 'done')
                                        @if($val->status_datarpr2 == 2)
                                          <li><a href="{{url('kimia-klinik/data-evaluasi/')}}/{{$id}}?x=b&y=2" target="_blank">II-B</a></li>
                                        @endif
                                      @endif
                                    @endif
                                    @endif
                                  </ul>
                                </div>  
                            
                            </div>

                           <!--  <div class="col-md-12" style="text-align:center; display: none">
                                <p>
                                    Z Score per Parameter
                                </p>
                                <div class="btn-group">
                                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Print Data <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu">
                                  @if($siklus == "1")
  
                                    @if($val->siklus_1 == 'done')
                                      @if($val->pemeriksaan == 'done')
                                        @if($val->status_data1 == 2)
                                          <li><a href="{{url('kimia-klinik/data-evaluasi/grafik-zscore/')}}/{{$id}}?x=a&y=1" target="_blank">I-A</a></li>
                                        @endif
                                      @endif
                                      @if($val->pemeriksaan2 == 'done')
                                        @if($val->status_data2 == 2)
                                          <li><a href="{{url('kimia-klinik/data-evaluasi/grafik-zscore/')}}/{{$id}}?x=b&y=1" target="_blank">I-B</a></li>
                                        @endif
                                      @endif
                                    @endif
                                  @else
                                    @if($val->siklus_2 == 'done')
                                      @if($val->rpr1 == 'done')
                                        @if($val->status_datarpr1 == 2)
                                          <li><a href="{{url('kimia-klinik/data-evaluasi/grafik-zscore/')}}/{{$id}}?x=a&y=2" target="_blank">II-A</a></li>
                                        @endif
                                      @endif
                                      @if($val->rpr2 == 'done')
                                        @if($val->status_datarpr2 == 2)
                                          <li><a href="{{url('kimia-klinik/data-evaluasi/grafik-zscore/')}}/{{$id}}?x=b&y=2" target="_blank">II-B</a></li>
                                        @endif
                                      @endif
                                    @endif
                                  @endif
                                  </ul>
                                </div>  
                              
                            </div> -->
<!-- 
                            <div class="col-md-12" style="text-align:center;">
                                <p>
                                    Grafik Nilai yang Sama
                                </p>
                                <div class="btn-group">
                                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 170px">
                                    Print Data <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu">
                                  @if($siklus == "1")
                                     @if($val->siklus_1 == 'done')
                                      @if($val->pemeriksaan == 'done')
                                        @if($val->status_data1 == 2)
                                           <li><a href="{{url('hasil-pemeriksaan/kimia-klinik/nilai-sama/')}}/{{$id}}?x=a&y=1" target="_blank">I-A</a></li>
                                        @endif
                                      @endif
                                      @if($val->pemeriksaan2 == 'done')
                                        @if($val->status_data2 == 2)
                                          <li><a href="{{url('hasil-pemeriksaan/kimia-klinik/nilai-sama/')}}/{{$id}}?x=b&y=1" target="_blank">I-B</a></li>
                                        @endif
                                      @endif
                                    @endif
                                  @else
                                    @if($val->siklus_2 == 'done')
                                      @if($val->rpr1 == 'done')
                                        @if($val->status_datarpr1 == 2)
                                          <li><a href="{{url('hasil-pemeriksaan/kimia-klinik/nilai-sama/')}}/{{$id}}?x=a&y=2" target="_blank">I-B</a></li>
                                        @endif
                                      @endif
                                      @if($val->rpr2 == 'done')
                                        @if($val->status_datarpr2 == 2)
                                          <li><a href="{{url('hasil-pemeriksaan/kimia-klinik/nilai-sama/')}}/{{$id}}?x=b&y=2" target="_blank">II-B</a></li>
                                        @endif
                                      @endif
                                    @endif
                                  @endif
                                  </ul>
                                </div>   
                            </div> -->

                            <!-- <div class="col-md-12" style="text-align:center; display: none;">
                                <p>
                                    Z Score Per Alat / Instrumen
                                </p>
                                <div class="btn-group">
                                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Print Data <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu">
                                    @if($val->siklus_1 == 'done')
                                      @if($val->pemeriksaan == 'done')
                                        @if($val->status_data1 == 2)
                                          <li><a href="{{url('kimia-klinik/data-evaluasi/grafik-zscore-alat/')}}/{{$id}}?x=a&y=1" target="_blank">I-A</a></li>
                                        @endif
                                      @endif
                                      @if($val->pemeriksaan2 == 'done')
                                        @if($val->status_data2 == 2)
                                          <li><a href="{{url('kimia-klinik/data-evaluasi/grafik-zscore-alat/')}}/{{$id}}?x=b&y=1" target="_blank">I-B</a></li>
                                        @endif
                                      @endif
                                    @endif
                                    <li role="separator" class="divider"></li>
                                    @if($val->siklus_2 == 'done')
                                      @if($val->rpr1 == 'done')
                                        @if($val->status_datarpr1 == 2)
                                          <li><a href="{{url('kimia-klinik/data-evaluasi/grafik-zscore-alat/')}}/{{$id}}?x=a&y=2" target="_blank">II-A</a></li>
                                        @endif
                                      @endif
                                      @if($val->rpr2 == 'done')
                                        @if($val->status_datarpr2 == 2)
                                          <li><a href="{{url('kimia-klinik/data-evaluasi/grafik-zscore-alat/')}}/{{$id}}?x=b&y=2" target="_blank">II-B</a></li>
                                        @endif
                                      @endif
                                    @endif
                                    
                                  </ul>
                                </div> 
                            </div> -->

                            <div class="col-md-12" style="text-align:center;">
                                <p>
                                    Grafik Z-Score Hasil Peserta
                                </p>
                                <div class="btn-group">
                                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 170px">
                                    Cetak Grafik <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu">
                                  @if($siklus == "1")
                                    @if($val->siklus_1 == 'done')
                                      @if($val->pemeriksaan == 'done')
                                        @if($val->status_data1 == 2)
                                            <li><a href="{{url('hasil-pemeriksaan/kimia-klinik/grafik-zscore-semua/')}}/{{$id}}?x=a&y=1" target="_blank">I-A</a></li>
                                        @endif
                                      @endif
                                      @if($val->pemeriksaan2 == 'done')
                                        @if($val->status_data2 == 2)
                                          <li><a href="{{url('hasil-pemeriksaan/kimia-klinik/grafik-zscore-semua/')}}/{{$id}}?x=b&y=1" target="_blank">I-B</a></li>
                                        @endif
                                      @endif
                                    @endif
                                  @else
                                    @if($val->siklus_2 == 'done')
                                      @if($val->rpr1 == 'done')
                                        @if($val->status_datarpr1 == 2)
                                          <li><a href="{{url('hasil-pemeriksaan/kimia-klinik/grafik-zscore-semua/')}}/{{$id}}?x=a&y=2" target="_blank">I-B</a></li>
                                        @endif
                                      @endif
                                      @if($val->rpr2 == 'done')
                                        @if($val->status_datarpr2 == 2)
                                          <li><a href="{{url('hasil-pemeriksaan/kimia-klinik/grafik-zscore-semua/')}}/{{$id}}?x=b&y=2" target="_blank">I-B</a></li>
                                        @endif
                                      @endif
                                    @endif
                                  @endif
                                  </ul>
                                </div>  
                                 <br><br>
                              <div class="btn-group">
                                @if($siklus == "1")
                                  @if($val->siklus_1 == 'done')
                                    @if($val->pemeriksaan == 'done')
                                      @if($val->status_data1 == 2)   
                                        <a href="{{ url('/sertifikat/kimia-klinik/data-evaluasi/') }}/{{$id}}?x=a&y=1&tanggal=undefined&nomor=undefined&download=true" target="_blank" class="btn btn-info" style="width: 170px">
                                            Cetak Sertifikat
                                        </a>
                                       @endif
                                    @endif
                                  @endif 
                                @else  
                                  @if($val->siklus_2 == 'done')
                                    @if($val->status_datarpr1 == 2)
                                      <a href="{{ url('/sertifikat/kimia-klinik/data-evaluasi/') }}/{{$id}}?x=a&y=2&tanggal=undefined&nomor=undefined&download=true" target="_blank" class="btn btn-info" style="width: 170px">
                                        Cetak Sertifikat
                                      </a>
                                    @endif
                                  @endif
                                @endif
                              </div>
                              <p><small>(*Laporan Akhir, Hasil, Grafik dan Sertifikat mohon dicetak di kertas berukuran A4)</small></p>
                            </div>

                        </div>
                    </div>
              
                

                
                  <!--   <div class="row">
                        <div class="col-md-12">
                            
                            <div class="col-md-4" style="text-align:center; display: none;">
                                <p>
                                    Grafik Metode
                                </p>
                                <div class="btn-group">
                                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Print Data <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu">
                                    @if($val->siklus_1 == 'done')
                                      @if($val->pemeriksaan == 'done')
                                        @if($val->status_data1 == 2)
                                           <li><a href="{{url('/kimia-klinik/data-evaluasi/nilai-sama-metode')}}/{{$id}}?x=a&y=1" target="_blank">I-A</a></li>
                                        @endif
                                      @endif
                                      @if($val->pemeriksaan2 == 'done')
                                        @if($val->status_data2 == 2)
                                          <li><a href="{{url('/kimia-klinik/data-evaluasi/nilai-sama-metode')}}/{{$id}}?x=b&y=1" target="_blank">I-B</a></li>
                                        @endif
                                      @endif
                                    @endif
                                    <li role="separator" class="divider"></li>
                                    @if($val->siklus_2 == 'done')
                                      @if($val->rpr1 == 'done')
                                        @if($val->status_datarpr1 == 2)
                                          <li><a href="{{url('/kimia-klinik/data-evaluasi/nilai-sama-metode')}}/{{$id}}?x=a&y=2" target="_blank">I-B</a></li>
                                        @endif
                                      @endif
                                      @if($val->rpr2 == 'done')
                                        @if($val->status_datarpr2 == 2)
                                          <li><a href="{{url('/kimia-klinik/data-evaluasi/nilai-sama-metode/')}}/{{$id}}?x=b&y=2" target="_blank">I-B</a></li>
                                        @endif
                                      @endif
                                    @endif
                                  </ul>
                                </div>
                            </div>
                          </div>
                      </div> 
               -->
              @endif
              @endforeach
              @else
              <p style="font-size: 30px; text-align: center;">Tidak Ada Data</p>
              @endif
              <div class="line"></div>

        </div>
    </div>

<!-- Footer -->
    <div class=" footer">
        <div class="col-md-12 copy"><marquee>COPYRIGHT &copy; BBLK ALL RIGHT RESERVED - POWERED BY Pilar</marquee></div>
    </div>
@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    });
    </script>
@endif
@endsection
@section('scriptBlock')
  
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar-peserta').toggleClass('active');
            });
        });
    </script>
<script>
      var table = $(".dataTables-data");
      var dataTable = table.DataTable({
      responsive:!0,
      "serverSide":true,
      "processing":true,
      "ajax":{
          url : "{{url('/data-evaluasi/data-laporan-akhir/kk')}}"
      },
      dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
      language:{
          paginate:{
              previous:"&laquo;",
              next:"&raquo;"
          },search:"_INPUT_",
          searchPlaceholder:"Search..."
      },
      "columns":[
              {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
              {"data":"file","name":"file","searchable":true,"orderable":true},
              {"data":"tahun","name":"tahun","searchable":true,"orderable":true},
              {"data":"siklus","name":"siklus","searchable":true,"orderable":true},
              {"data":"alias","name":"alias","searchable":true,"orderable":true},
              {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
      ],
      order:[[1,"asc"]]
  });

</script>
 
    <script type="text/javascript">
        $(".myText").hide();
        $(".btn-show").click(function() {
          var targetna = $(".myText");
          if(targetna.css('display') == 'none'){
            console.log('show');
           $(".myText").show('100');
          }else{
            console.log('hide');
            targetna.hide('100');
          }
        
        });
        $(".form_datetime").datetimepicker({
          format: "yyyy",
          autoclose: true,
          todayBtn: true,
          pickerPosition: "bottom-right",
          minView: 4,
          startView: 4
      });
    </script>

@endsection
