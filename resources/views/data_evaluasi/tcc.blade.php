@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_hasil_evaluasi')
@section('content')
        <!-- Page Content  -->
        <div id="content">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fa fa-align-justify"></i>
                        <span></span>
                    </button>
                </div>
                <label style="margin: 10px 20px 0px 0px; font-size: 28px; position: absolute; top: 0; right: 0">MIKROSKOPIS TELUR CACING</label>
            </nav> 
            <div class="line"></div>
            <div class="row">
            <div class="col-md-12">
              <div class="col-md-12" style="text-align:center;">
                    <p>
                       Laporan Penyelenggaraan PNPME Telur Cacing
                    </p>
                        <!-- <a href="{{url('/laporan-akhir-tcc/data-evaluasi/')}}/{{$id}}" target="_blank" class="btn btn-info">Sampul Laporan</a> -->
                        <button class="btn-show btn btn-info" style="width: 170px">Laporan</button>
                      {{--   <a href="{{url('/laporan-akhir-tcc/data-evaluasi/penutup/')}}/{{$id}}" class="btn btn-info" target="_blank">Penutup Laporan</a>--}}
                        <br><br>
                        <div class="myText">
                          <div class="row">
                              <div class="col-md-8 col-md-offset-2">
                                  <div class="panel panel-default">
                                      <div class="panel-heading">Data Upload Laporan Akhir</div>
                                      <div class="panel-body">
                                          
                                          <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                                              <thead>
                                              <tr>
                                                  <th>No</th>
                                                  <th>File</th>
                                                  <th>Tahun</th>
                                                  <th>Siklus</th>
                                                  <th>Parameter</th>
                                                  <th>Aksi</th>
                                              </tr>
                                              </thead>
                                              <tbody>
                                              </tbody>
                                          </table>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>

            <div class="line"></div>
            @if(count($data)>0)
            @foreach($data as $val)
            <?php 
                if($val->siklus_1 == 'done'){
                  $siklus1='1';
                }elseif($val->siklus_2 == 'done'){
                  $siklus2='2';
                }
               ?>
            @if($val->bidang == '5')
                <div class="row">
                    <div class="col-md-12" >
                        <div class="col-md-12" style="text-align:center;">
                            <p>
                                Hasil Peserta
                            </p>
                            <div class="btn-group">
                              @if($siklus == "1")
                                @if($val->siklus_1 == 'done')
                                  @if($val->pemeriksaan == 'done')
                                     @if($val->status_data1 == 2)
                                        <a style="width: 170px" class="btn btn-info" href="{{url('telur-cacing/data-evaluasi/print')}}/{{$id}}?y=1" target="_blank">Print Hasil Evaluasi</a>
                                     @endif
                                  @endif
                                @endif
                              @else
                                @if($val->siklus_2 == 'done')
                                  @if($val->pemeriksaan2 == 'done')
                                    @if($val->status_data2 == 2)
                                        <a style="width: 170px" class="btn btn-info" href="{{url('telur-cacing/data-evaluasi/print')}}/{{$id}}?y=2" target="_blank">Print Hasil Evaluasi</a>
                                    @endif
                                  @endif
                                @endif
                               @endif
                            </div><br><br>
                            <div class="btn-group">
                              @if($siklus == "1")
                                @if($val->siklus_1 == 'done')
                                  @if($val->pemeriksaan == 'done')
                                     @if($val->status_data1 == 2)
                                        <a style="width: 170px" class="btn btn-info" href="{{ url('hasil-pemeriksaan/telur-cacing/sertifikat/print/') }}/{{$id}}?y=1&tanggal=undefined&nomor=undefined&bidang=5&download=true" target="_blank">Cetak Sertifikat</a>
                                     @endif
                                  @endif
                                @endif
                              @else
                                @if($val->siklus_2 == 'done')
                                  @if($val->pemeriksaan2 == 'done')
                                    @if($val->status_data2 == 2)
                                      <a style="width: 170px" class="btn btn-info" href="{{ url('hasil-pemeriksaan/telur-cacing/sertifikat/print/') }}/{{$id}}?y=2&tanggal=undefined&nomor=undefined&bidang=5&download=true" target="_blank">Cetak Sertifikat</a>
                                    @endif
                                  @endif
                                @endif                                    
                               @endif
                              </ul>
                            </div>
                          <p><small>(*Laporan Akhir, Hasil, Grafik dan Sertifikat mohon dicetak di kertas berukuran A4)</small></p>
                        </div>
                        @endif
                    @endforeach
              @else
              <p style="font-size: 30px; text-align: center; position: absolute;top: -20px;">Tidak Ada Data</p>
              @endif
              <!-- <div class="col-md-12" style="margin-top: 20px" align="center">
                 <a href="{{ url('hasil-pemeriksaan/telur-cacing/sertifikat/print/') }}/{{$id}}?y=1&tanggal=undefined&nomor=undefined&bidang=5&download=true" class="btn btn-info">Cetak Sertifikat</a>
              </div> -->

        </div>
    </div>
        <div class="line"></div>

</div>
</div> 
<!-- Footer -->
    <div class=" footer">
        <div class="copy"><marquee>COPYRIGHT &copy; BBLK ALL RIGHT RESERVED - POWERED BY Pilar</marquee></div>
    </div>
@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
    });
    </script>
@endif
@endsection

@section('scriptBlock')
  
<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar-peserta').toggleClass('active');
        });
    });
</script>
<script>
      var table = $(".dataTables-data");
      var dataTable = table.DataTable({
      responsive:!0,
      "serverSide":true,
      "processing":true,
      "ajax":{
          url : "{{url('/data-evaluasi/data-laporan-akhir/tcc')}}"
      },
      dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
      language:{
          paginate:{
              previous:"&laquo;",
              next:"&raquo;"
          },search:"_INPUT_",
          searchPlaceholder:"Search..."
      },
      "columns":[
              {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
              {"data":"file","name":"file","searchable":true,"orderable":true},
              {"data":"tahun","name":"tahun","searchable":true,"orderable":true},
              {"data":"siklus","name":"siklus","searchable":true,"orderable":true},
              {"data":"alias","name":"alias","searchable":true,"orderable":true},
              {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
      ],
      order:[[1,"asc"]]
  });

</script>
 
    <script type="text/javascript">
   

        $(".myText").hide();
        $(".btn-show").click(function() {
          var targetna = $(".myText");
          if(targetna.css('display') == 'none'){
            console.log('show');
           $(".myText").show('100');
          }else{
            console.log('hide');
            targetna.hide('100');
          }
        
        });
    </script>
@endsection
