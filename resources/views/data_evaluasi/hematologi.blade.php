@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_hasil_evaluasi')
@section('content')
        <!-- Page Content  -->
        <div id="content">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fa fa-align-justify"></i>
                        <span></span>
                    </button>
                </div>
                <label style="margin: 10px 20px 0px 0px; font-size: 28px; position: absolute; top: 0; right: 0">HEMATOLOGI</label>
          </nav>  
          <div class="line"></div>
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12" style="text-align:center;">
                <p>
                    Laporan Penyelenggaraan PNPME Hematologi
                </p>
                <a href="{{url('/laporan-akhir-hematologi/data-evaluasi/')}}/{{$id}}" class="btn btn-info" style="display: none;">Sampul Laporan</a>
                  <button class="btn-show btn btn-info" style="width: 170px">Laporan</button>
                  <br><br>
                  <div class="myText">
                    <div class="row">
                      <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                          <div class="panel-heading">Data Upload Laporan Akhir</div>
                          <div class="panel-body">   
                            <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                              <thead>
                              <tr>
                                  <th>No</th>
                                  <th>File</th>
                                  <th>Tahun</th>
                                  <th>Siklus</th>
                                  <th>Parameter</th>
                                  <th>Aksi</th>
                              </tr>
                              </thead>
                              <tbody>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
                          
            <div class="line"></div>
                    @if(count($data))
                    <?php $no = 0; ?>
                    @foreach($data as $val)
                    <?php 
                      if($val->siklus_1 == 'done'){
                        $siklus1='1';
                      }elseif($val->siklus_2 == 'done'){
                        $siklus2='2';
                      }
                     ?>
                    @if($val->bidang == '1')
                    <div class="row" style="display: show;">
                      <div class="col-md-12" style="text-align: center;">
                              <p>
                                  Hasil Saudara
                              </p>
                              <div class="btn-group">
                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 170px">
                                  Catak Hasil Evaluasi <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                @if($siklus == "1")
                                  @if($val->siklus_1 == 'done')
                                    @if($val->pemeriksaan == 'done')
                                      @if($val->status_data1 == 2)
                                        <li><a href="{{url('/data-evaluasi/hematologi/')}}/print/{{$id}}?x=a&y=1" >I-A</a></li>
                                      @endif
                                    @endif
                                    @if($val->pemeriksaan2 == 'done')
                                      @if($val->status_data2 == 2)
                                      <li><a href="{{url('/data-evaluasi/hematologi/')}}/print/{{$id}}?x=b&y=1" >I-B</a></li>
                                      @endif
                                    @endif
                                  @endif
                                @else
                                  @if($val->siklus_2 == 'done')
                                    @if($val->rpr1 == 'done')
                                      @if($val->status_datarpr1 == 2)
                                        <li><a href="{{url('/data-evaluasi/hematologi/')}}/print/{{$id}}?x=a&y=2" >II-A</a></li>
                                      @endif
                                    @endif
                                    @if($val->rpr2 == 'done')
                                      @if($val->status_datarpr2 == 2)
                                        <li><a href="{{url('/data-evaluasi/hematologi/')}}/print/{{$id}}?x=b&y=2" >II-B</a></li>
                                      @endif
                                    @endif
                                  @endif
                                @endif
                                </ul>
                              </div>  
                          <br><br>
                          
                              <p>
                                  Grafik Z-Score Hasil Peserta
                              </p>
                              <div class="btn-group">
                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 170px">
                                  Cetak Grafik <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                  @if($siklus == "1")
                                  @if($val->siklus_1 == 'done')
                                    @if($val->pemeriksaan == 'done')
                                       @if($val->status_data1 == 2)                                          
                                         <li><a href="{{url('/data-evaluasi/grafik-zscore-semua')}}/{{$id}}?x=a&y=1" >I-A</a></li>
                                       @endif
                                    @endif
                                    @if($val->pemeriksaan2 == 'done')
                                      @if($val->status_data2 == 2)
                                        <li><a href="{{url('/data-evaluasi/grafik-zscore-semua')}}/{{$id}}?x=b&y=1" >I-B</a></li>
                                      @endif
                                    @endif
                                  @endif                                    
                                  @else
                                   @if($val->siklus_2 == 'done')
                                    @if($val->rpr1 == 'done')
                                      @if($val->status_datarpr1 == 2)
                                         <li><a href="{{url('/data-evaluasi/grafik-zscore-semua')}}/{{$id}}?x=a&y=2" >II-A</a></li>
                                      @endif
                                    @endif
                                    @if($val->rpr2 == 'done')
                                      @if($val->status_datarpr2 == 2)
                                        <li><a href="{{url('/data-evaluasi/grafik-zscore-semua')}}/{{$id}}?x=b&y=2" >II-B</a></li>
                                      @endif
                                    @endif
                                  @endif  
                                  @endif
                                </ul>
                              </div>  
                          <br><br>
                              <!-- <p>
                                  Grafik Nilai yang Sama
                              </p>
                              <div class="btn-group">
                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 170px">
                                  Print Data <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                @if($siklus == "1")
                                   @if($val->siklus_1 == 'done')
                                    @if($val->pemeriksaan == 'done')
                                       @if($val->status_data1 == 2)                                          
                                         <li><a href="{{url('/data-evaluasi/grafik-zscore-nilai-sama/')}}/{{$id}}?x=a&y=1" >I-A</a></li>
                                       @endif
                                    @endif
                                    @if($val->pemeriksaan2 == 'done')
                                      @if($val->status_data2 == 2)
                                        <li><a href="{{url('/data-evaluasi/grafik-zscore-nilai-sama/')}}/{{$id}}?x=b&y=1" >I-B</a></li>
                                      @endif
                                    @endif
                                  @endif                                    
                                 @else   
                                     @if($val->siklus_2 == 'done')
                                    @if($val->rpr1 == 'done')
                                      @if($val->status_datarpr1 == 2)
                                         <li><a href="{{url('/data-evaluasi/grafik-zscore-nilai-sama/')}}/{{$id}}?x=a&y=2" >II-A</a></li>
                                      @endif
                                    @endif
                                    @if($val->rpr2 == 'done')
                                      @if($val->status_datarpr2 == 2)
                                        <li><a href="{{url('/data-evaluasi/grafik-zscore-nilai-sama/')}}/{{$id}}?x=b&y=2" >II-B</a></li>
                                      @endif
                                    @endif
                                  @endif
                                  @endif    
                                </ul>
                              </div>
                          <br><br> -->
                           <div class="btn-group">
                            @if($siklus == "1")
                              @if($val->siklus_1 == 'done')
                                @if($val->pemeriksaan == 'done')
                                   @if($val->status_data1 == 2)
                                      <a href="{{ url('data-evaluasi/hematologi/sertifikat/print/') }}/{{$id}}?x=a&y=1&tanggal=undefined&nomor=undefined&download=true" class="btn btn-info" style="width: 170px">
                                          Cetak Sertifikat
                                      </a>
                                   @endif
                                @endif
                              @endif 
                            @else  
                              @if($val->siklus_2 == 'done')      
                                @if($val->status_datarpr1 == 2)
                                  <a href="{{ url('data-evaluasi/hematologi/sertifikat/print/') }}/{{$id}}?x=a&y=2&tanggal=undefined&nomor=undefined&download=true" class="btn btn-info" style="width: 170px">
                                    Cetak Sertifikat
                                  </a>
                                @endif
                              @endif
                            @endif
                          </div>
                        <p><small>(*Laporan Akhir, Hasil, Grafik dan Sertifikat mohon dicetak di kertas berukuran A4)</small></p>
                      </div>
                    </div>
                    <div class="line"></div>
                  </div>
                </div>
                    
              @endif
              @endforeach
              @else
              <p style="font-size: 30px; text-align: center;">Tidak Ada Data</p>
              @endif
  
      <div class=" footer" style="">
          <div class="copy"><marquee>COPYRIGHT &copy; BBLK ALL RIGHT RESERVED - POWERED BY Pilar</marquee></div>
      </div>

@if(Session::has('message'))
<div id="snackbar">{{ Session::get('message') }}</div>

<script>
$('document').ready(function(){
    var x = document.getElementById("snackbar")
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
});
</script>
@endif
@stop


@section('scriptBlock')
<script type="text/javascript">
    $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar-peserta').toggleClass('active');
            });
        });

</script>
<script>
      var table = $(".dataTables-data");
      var dataTable = table.DataTable({
      responsive:!0,
      "serverSide":true,
      "processing":true,
      "ajax":{
          url : "{{url('/data-evaluasi/data-laporan-akhir/')}}"
      },
      dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
      language:{
          paginate:{
              previous:"&laquo;",
              next:"&raquo;"
          },search:"_INPUT_",
          searchPlaceholder:"Search..."
      },
      "columns":[
              {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
              {"data":"file","name":"file","searchable":true,"orderable":true},
              {"data":"tahun","name":"tahun","searchable":true,"orderable":true},
              {"data":"siklus","name":"siklus","searchable":true,"orderable":true},
              {"data":"alias","name":"alias","searchable":true,"orderable":true},
              {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
      ],
      order:[[1,"asc"]]
  });

</script>
 
    <script type="text/javascript">
      
        $(".myText").hide();
        $(".btn-show").click(function() {
          var targetna = $(".myText");
          if(targetna.css('display') == 'none'){
            console.log('show');
           $(".myText").show('100');
          }else{
            console.log('hide');
            targetna.hide('100');
          }
        
        });
    </script>
@endsection
