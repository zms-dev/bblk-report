@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')
@section('content')
   <div class="wrapper" style="margin-top: -40px;">
        <!-- Sidebar  -->
        <nav id="sidebar-peserta" style="text-align: center;">
            <div class="sidebar-header">
                <a href="{{ url('data-evaluasi') }}"><h3>Laporan Akhir</h3></a> 
                    <form method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">  
                        <select name="tanggal" class="form-control" required>
                        <option readonly>Pilih Tahun</option>
                        <option {{ $tanggal=="2017"?'selected':'' }} value="2017">2017</option>
                        <option {{ $tanggal=="2018"?'selected':'' }} value="2018">2018</option>
                        <option {{ $tanggal=="2019"?'selected':'' }} value="2019">2019</option>
                        <option {{ $tanggal=="2020"?'selected':'' }} value="2020">2020</option>
                        <option {{ $tanggal=="2021"?'selected':'' }} value="2021">2021</option>
                        <option {{ $tanggal=="2022"?'selected':'' }} value="2022">2022</option>
                        <option {{ $tanggal=="2023"?'selected':'' }} value="2023">2023</option>
                        <option {{ $tanggal=="2024"?'selected':'' }} value="2024">2024</option>
                        <option {{ $tanggal=="2025"?'selected':'' }} value="2025">2025</option>
                        </select>
                      <input type="submit" name="cari" required="" class="btn btn-info" value="cari" style="margin-top: 8px;">
                    </form>
            </div>

            <ul class="list-unstyled components" style="text-align: center">        
                <li>
                 
                    @foreach($datas as $val)
                    <a href=" {{URL('').$val->Link}}/data-evaluasi/{{$val->id}}">{{ $val->alias }}</a>
                    @endforeach
                </li>
              </ul>                    
        </nav>
        <!-- Page Content  -->
        <div id="content">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fa fa-align-justify"></i>
                        <span></span>
                    </button>
                </div>
                <label style="margin: 10px 20px 0px 0px; font-size: 28px; position: absolute; top: 0; right: 0">BAKTERI DAN UJI KEPEKAAN ANTIBIOTIK</label>
            </nav>
             
            <div class="line"></div>
            <div class="row">
                <div class="col-md-12">
                  <div class="col-md-12" style="text-align:center;">
                        <p>
                            Laporan Akhir Bakteri dan Uji Kepekaan Antibiotik
                        </p>                                         
                            <a href="{{url('/laporan-akhir-bac/data-evaluasi/')}}/{{$id}}" class="btn btn-info" target="_blank">Sampul Laporan</a>
                            <button class="btn-show btn btn-info" asu="asu" style="width: 170px">Laporan</button>
                            {{-- <a href="{{url('/laporan-akhir-bac/data-evaluasi/penutup/')}}/{{$id}}" class="btn btn-info" target="_blank">Penutup Laporan</a> --}}
                            <br><br>
                            <div class="myText">
                              <div class="row">
                                  <div class="col-md-8 col-md-offset-2">
                                      <div class="panel panel-default">
                                          <div class="panel-heading">Data Upload Laporan Akhir</div>
                                          <div class="panel-body">
                                              
                                              <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                                                  <thead>
                                                  <tr>
                                                      <th>No</th>
                                                      <th>File</th>
                                                      <th>Tahun</th>
                                                      <th>Siklus</th>
                                                      <th>Parameter</th>
                                                      <th>Bagian</th>
                                                      <th>Aksi</th>
                                                  </tr>
                                                  </thead>
                                                  <tbody>
                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>  
            <div class="line"></div>
            
                    <div class="row">
                        <div class="col-md-12" >
                            
                            @if(count($data1))
                            @foreach($data1 as $val)
                            @if($val->bidang == '13')
                            <div class="col-md-4" style="text-align:center;">
                                <p>
                                    Hasil Saudara Penilaian Uji Kepekaan Antibiotik
                                </p>
                                	<div class="btn-group">
	                                  <button type="button"  class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 170px">
	                                    Print Data <span class="caret"></span>
	                                  </button>
	                                  <ul class="dropdown-menu">
                                        @if($val->siklus_1 == 'done')
                                        @if($val->status_data1 == 2)
                                        <li><a href="{{url('/antibiotik/data-evaluasi/uji-kepekaan-antibiotik/print')}}/{{$id}}?y=1&lembar=1" target="_blank">Lembar 1</a></li>
                                        <li><a href="{{url('/antibiotik/data-evaluasi/uji-kepekaan-antibiotik/print')}}/{{$id}}?y=1&lembar=2" target="_blank">Lembar 2</a></li>
                                        <li><a href="{{url('/antibiotik/data-evaluasi/uji-kepekaan-antibiotik/print')}}/{{$id}}?y=1&lembar=3" target="_blank">Lembar 3</a></li>
                                        @endif
                                        @else
                                        Not Found
                                        @endif
                                        @if($val->siklus_2 == 'done')
                                            @if($val->status_data2 == 2)
                                            <li><a href="{{url('/antibiotik/data-evaluasi/uji-kepekaan-antibiotik/print')}}/{{$id}}?y=2&lembar=1" target="_blank">Lembar 1</a></li>
                                            <li><a href="{{url('/antibiotik/data-evaluasi/uji-kepekaan-antibiotik/print')}}/{{$id}}?y=2&lembar=2" target="_blank">Lembar 2</a></li>
                                            <li><a href="{{url('/antibiotik/data-evaluasi/uji-kepekaan-antibiotik/print')}}/{{$id}}?y=2&lembar=3" target="_blank">Lembar 3</a></li>
                                            @endif
                                        @else
                                        @endif
	                                  </ul>
	                                </div>

                                    
                            </div> 
                            
                            <div class="col-md-4" style="text-align:center;">
                                <p>
                                    Hasil Saudara Bakteri Identifikasi

                                </p>
                                	<div class="btn-group">
	                                  <button type="button"  class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 170px">
	                                    Print Data <span class="caret"></span>
	                                  </button>
	                                  <ul class="dropdown-menu">
                                        @if(($val->pemeriksaan == 'done') && ($val->status_data1 == '2'))
	                                    <li><a href="{{url('/antibiotik/data-evaluasi/penilaian/')}}/{{$id}}?y=1" target="_blank">siklus 1</a></li>
                                        @else
                                        @endif
                                        @if(($val->pemeriksaan2 == 'done') && ($val->status_data2 == '2'))
                                    	<li><a href="{{url('/antibiotik/data-evaluasi/penilaian/')}}/{{$id}}?y=2" target="_blank">siklus 2</a></li>
                                        @else
                                        @endif
	                           
	                                  </ul>
	                                </div>  
                            </div> 
                           <div class="col-md-4" style="text-align:center;">
                                <p>
                                    Hasil Saudara Uji Kepekaan Antibiotik

                                </p>
                                  <div class="btn-group">
                                    <button type="button"  class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 170px">
                                      Print Data <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        @if(($val->pemeriksaan == 'done') && ($val->status_data1 == '2'))
                                      <li><a href="{{url('/antibiotik/data-evaluasiuji/peka/')}}/{{$id}}?y=1" target="_blank">siklus 1</a></li>
                                        @else
                                        @endif
                                        @if(($val->pemeriksaan2 == 'done') && ($val->status_data2 == '2'))
                                      <li><a href="{{url('/antibiotik/data-evaluasiuji/peka/')}}/{{$id}}?y=2" target="_blank">siklus 2</a></li>
                                      @else
                                        @endif
                                    </ul>
                          </div>  
                            </div>
		                </div> 
			        </div>
                    <div class="row">
                        <div class="col-md-12" >
                            
                        </div> 
                    </div>
                    @endif
                    @endforeach
                    @else
                    <tr>
                        <p style="font-size: 30px; text-align: center;">Tidak Ada Data</p>
                    </tr>
                    @endif   
                <div class="line"></div>

</div>
</div> 
<!-- Footer -->
    <div class="footer">
        <div class=" copy"><marquee>COPYRIGHT &copy; BBLK ALL RIGHT RESERVED - POWERED BY Pilar</marquee></div>
    </div>
@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    });
    </script>
@endif
@endsection

@section('scriptBlock')
<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar-peserta').toggleClass('active');
        });
    });
</script>
<script>
      var table = $(".dataTables-data");
      var dataTable = table.DataTable({
      responsive:!0,
      "serverSide":true,
      "processing":true,
      "ajax":{
          url : "{{url('/data-evaluasi/data-laporan-akhir/bac')}}"
      },
      dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
      language:{
          paginate:{
              previous:"&laquo;",
              next:"&raquo;"
          },search:"_INPUT_",
          searchPlaceholder:"Search..."
      },
      "columns":[
              {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
              {"data":"file","name":"file","searchable":true,"orderable":true},
              {"data":"tahun","name":"tahun","searchable":true,"orderable":true},
              {"data":"siklus","name":"siklus","searchable":true,"orderable":true},
              {"data":"alias","name":"alias","searchable":true,"orderable":true},
              {"data":"bagian","name":"bagian","searchable":true,"orderable":true},
              {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
      ],
      order:[[1,"asc"]]
  });

</script>
 
<script type="text/javascript">
 
    $(".myText").hide();
    $(".btn-show").click(function() {
      var targetna = $(".myText");
      if(targetna.css('display') == 'none'){
        console.log('show');
       $(".myText").show('100');
      }else{
        console.log('hide');
        targetna.hide('100');
      }
    
    });
</script>
@endsection
