@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')
@section('content')
   <div class="wrapper" style="margin-top: -40px;">
        <!-- Sidebar  -->
        <nav id="sidebar-peserta" style="text-align: center;">
            <div class="sidebar-header">
                <a href="{{ url('data-evaluasi') }}"><h3>Laporan Akhir</h3></a> 
                    <form method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">  
                        <select name="tanggal" class="form-control" required>
                        <option readonly>Pilih Tahun</option>
                        <?php for ($i=2018; $i <= $setsiklus->tahun ; $i++) {?>
                          <option {{ $tanggal=="$i"?'selected':'' }} value="{{$i}}">{{$i}}</option>
                        <?php }?>
                        </select>
                      <input type="submit" name="cari" required="" class="btn btn-info" value="cari" style="margin-top: 8px;">
                    </form>
            </div>

            <ul class="list-unstyled components" style="text-align: center">        
                <li>
                 
                    @foreach($datas1 as $val)
                    <a href=" {{URL('').$val->Link}}/data-evaluasi/{{$val->id}}">
                        @if($val->alias == "HEM")
                      Hematologi   
                      @elseif($val->alias == "KKL")
                      Kimia Klinik
                      @elseif($val->alias == "URI")
                      Urinalisa
                      @elseif($val->alias == "BTA")
                      Mikroskopis BTA
                      @elseif($val->alias == "TCC")
                      Mikroskopis Telur Cacing
                      @elseif($val->alias == "HIV")
                      Anti HIV
                      @elseif($val->alias == "SIF")
                      Syphilis Anti TP
                      @elseif($val->alias == "HBS")
                      HBsAg
                      @elseif($val->alias == "HCV")
                      Anti HCV
                      @endif
                    </a>
                    @endforeach
                </li>
              </ul>                    
        </nav>
        <!-- Page Content  -->
        
        <div id="content">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fa fa-align-justify"></i>
                        <span></span>
                    </button>
                </div>
                <label style="margin: 10px 20px 0px 0px; font-size: 28px; position: absolute; top: 0; right: 0">URINALISA</label>
            </nav>
            <div class="line"></div>
                <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('/urinalisasi/data-evaluasi')}}/print/{{$id}}?x={{$type}}&y={{$siklus}}" target="_blank">
            
                                <input type="hidden" value="{{$input['bahan']}}" name="bahan">
                                <div class="col-md-12" style="text-align:center;">
                                    <p>
                                        Evaluasi Urinalisasi
                                        <center>Siklus {{ $siklus }}</center>
                                    </p>
                                    <input type="submit" name="simpan" value="Print" class="btn btn-info">
                                    {{ csrf_field() }}
                                </div>  
                            
                </form>
        </div>
    </div>
    <div class="footer">
        <div class="copy"><marquee>COPYRIGHT &copy; BBLK ALL RIGHT RESERVED - POWERED BY Pilar</marquee></div>
    </div>

@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    });
    </script>
@endif
 
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar-peserta').toggleClass('active');
            });
        });
    </script>
@endsection
