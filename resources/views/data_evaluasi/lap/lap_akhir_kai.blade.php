<style type="text/css">
body{
    font-family: Helvetica;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}
</style>
<table width="100%" cellpadding="0" border="0" style="margin-top: 25px">
        <tr align="center">
            <th width="100%" >
                <span style="font-size: 25px; "><b>LAPORAN AKHIR</b></span><br>
                <br>
                <span style="font-size: 18px; "><b>PROGRAM NASIONAL PEMANTAPAN MUTU EKSTERNAL</b></span><br>
                <span style="font-size: 18px; "><b>KIMIA AIR</b></span><br>
                <span style="font-size: 18px; "><b>SIKLUS 1 TAHUN {{ $t }}</b></span><br> 
            </th>
        </tr>
        <?php $evaluasi = DB::table('tb_hide_menu')->where('status','=','muncul')->first(); ?>       
        <?php 
          $kode = $data->kode_lebpes;
          $kode1 = substr($kode,5,-6);
        ?>
        <tr align="center">
            <th width="100%" >
              <pre style=""></pre><br><br><br>
                <span style="font-size: 17px; "><b>Nomor : @if(count($data) > 0){{ str_replace("$kode1/", "", $kode) }}@else...@endif</b></span><br>
                <span style="font-size: 17px; "><b>Tanggal : 31 Juli 2018</b></span><br>
            </th>
        </tr>

        <tr align="center">
            <th width="100%" >
              <pre style=""> 
                </pre><br><br>
                <?php
                  $tahun = date('Y');
                  $taun = substr($tahun, 2);

                  $kode_p = $data->kode_lebpes; 
                  $kode_p1 = substr($kode_p, 5,-6);
                  $kode_tahun = substr($kode_p, 12);
                ?>
                <span style="font-size: 15px; "><b>Kode Peserta: @if($data)/{{ $kode_p1 }}/{{$kode_tahun}}@else...@endif</b></span><br>
            </th>
        </tr>
        <tr align="center">
            <th width="100%" >
            	<pre style=""> 
                </pre>
                   <img alt="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" src="{{public_path('asset'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'kimkes.png')}}" height="330px">
            </th>
        </tr>
        <tr align="center">
            <td width="100%" >
              <pre style=""> 
                </pre><br><br><br>
                <span style="font-size: 17px;"><b>PENYELENGGARA&nbsp;:</b></span><br>
            </td>

        </tr>
        <tr align="center">
            <th width="100%" >
              <pre style=""> 
                </pre>
                <span style="font-size: 15px;">KEMENTERIAN KESEHATAN RI</span><br>
                <span style="font-size: 15px;">DIREKTORAT JENDERAL PELAYANAN KESEHATAN</span><br>
                <span style="font-size: 15px;">BALAI BESAR LABORATORIUM KESEHATAN JAKARTA</span><br>
                <span style="font-size: 15px;">KEMENTERIAN KESEHATAN RI</span><br>
            </th>
            
        </tr>
        <tr align="center">
            <td width="100%">
                <span style="font-size: 14px;">JAKARTA<br/>Jalan Percetakan Negara No. 23 B Jakarta Pusat - 10560<br>
                <span style="font-size: 14px;">Telp. (021) 4212524, 42804339 Facsimile (021) 4245516</span><br>
                s<span style="font-size: 14px;">Website : bblkjakarta.com ; Surat Elektronik : bblkjakarta@yahoo.co.id</span>
            </td>
            
        </tr>

        
</table>


<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>



    