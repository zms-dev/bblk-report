<style type="text/css">
body{
    font-family: arial;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}

p{
  font-size:15px;
  word-spacing: 0.3em;
}

li{
  font-size:15px;
  word-spacing: 0.3em;
}


#footer { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 6%;
}

#footer-hal { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 80%;
}
</style>
<body>
<p style="margin: 3%">
  <b>VII. REFERENSI</b>
</p>
<p style="margin:4%;">
  <ol>
    <li>Badan Standarisasi Nasional,<i>Penilaian kesesuaian- persyaratan umum uji profisiensi (SNI ISO/IEC 17043:2010)</i>, Jakarta.<br><br></li>
    <li>Keputusan Menteri Kesehatan Republik Indonesia. Nomor HK.02.02/ MENKES/400/2016 tentang <i>Balai Besar Laboratorium Kesehatan Sebagai Penyelenggara Pemantapan Mutu Eksternal Tingkat Nasional</i>, 3 Agustus 2016, Jakarta.<br><br></li>
    <li>Peraturan Menteri Kesehatan Republik Indonesia. Nomor 15 Tahun 2015 tentang<i> Pelayanan Laboratorium Pemeriksa HIV dan Infeksi Oprtunistik</i>, 3 Maret 2015, Jakarta.<br><br></li>
    <li>Inhouse Training SPIN, <i>Implementasi SNI ISO/IEC 17043: 2010 dan Teknik Analisis Data Uji Profisiensi/ Interlaboratory Comparison</i>, 15-17 September 2016, Jakarta.<br><br></li>
    <li>Departemen Kesehatan Republik Indonesia, Direktorat Jenderal Bina Pelayanan Medik, Direktorat Bina Pelayanan Penunjang Medik, Pedoman Praktik Laboratorium Kesehatan Yang Benar (Good Laboratory Practice), Tahun 2008, Jakarta.</li>
  </ol>
</p>

<p style=" margin-left:  60%;">
  Deputi Manajer Mutu<br>
  Balai Besar Laboratorium Kesehatan <br>

  <br><br><br><br><br>
  dr. Koesprijani, Sp.PK<br>
  Nip. 196402231990032001

</p>

<div id="footer">
    PNPME-BBLK JAKARTA
    
</div>
<div id="footer-hal">
    Halaman …..  dari …
</div>
<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>
</body>



    