<style type="text/css">
body{
    font-family: arial;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}

p{
  font-size:15px;
  word-spacing: 0.3em;
}

li{
  font-size:15px;
  word-spacing: 0.3em;
}

#footer { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 6%;
}

#footer-hal { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 80%;
}
</style>
<body>
<p style="margin: 3%">
  <b>V. REFERENSI</b>
</p>
<p style="margin:4%;">
<ul style="list-style-type: none;">
  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Program Nasional Pemantapan Mutu Eksternal Kimia Air Terbatas Siklus II Tahun<br>2017 yang diselenggarakan oleh BBLK Jakarta diikuti oleh 31 (tiga puluh satu) laboratorium peserta. Keseluruhan parameter yang diujikan pada PNPME - KAT Siklus II Tahun 2017 untuk sampel kontrol air ini telah dapat diolah dan dapat memberikan gambaran mengenai kinerja masing-masing laboratorium peserta. Program Nasional Pemantapan Mutu Eksternal untuk sampel kontrol air PNPME - KAT Siklus II Tahun 2017 dengan kode B2 untuk pengujian parameter Besi (Fe) dan Mangan (Mn), diikuti oleh 31 (tiga puluh satu) laboratorium peserta, dengan rincian 28 (dua puluh delapan) laboratorium peserta telah mengirimkan hasil pengujian dan 3 (tiga) laboratorium peserta tidak mengirimkan hasil pengujiannya.<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Berdasarkan pengolahan data menggunakan metode perhitungan Robust Z-Score,<br> diperoleh hasil untuk pengujian logam besi (Fe), dari 28 (dua puluh delapan) laboratorium peserta yang mengirimkan hasil pemeriksaan, sebanyak 27 (dua puluh tujuh) laboratorium peserta memberikan hasil <b>memuaskan</b>, dan 1 (satu) laboratorium peserta memberikan hasil <b>kurang memuaskan</b>.<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Untuk pengujian mangan (Mn), dari 28 (dua puluh delapan) laboratorium peserta yang mengirimkan hasil pemeriksaan, sebanyak 22 (dua puluh dua) laboratorium peserta memberikan hasil <b>memuaskan</b>, 2 (dua) laboratorium peserta memberikan hasil <b>meragukan</b>, dan 4 (empat) laboratorium peserta memberikan hasil <b>kurang memuaskan</b>.</li>
</ul>
</p>

<div id="footer">
    PNPME-BBLK JAKARTA
    
</div>
<div id="footer-hal">
    Halaman …..  dari …
</div>
<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>
</body>



    