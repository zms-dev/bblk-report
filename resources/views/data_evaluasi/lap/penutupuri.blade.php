<style type="text/css">
body{
    font-family: arial;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}

p{
  font-size:15px;
  word-spacing: 0.3em;
}

li{
  font-size:15px;
  word-spacing: 0.3em;
}


#footer { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 6%;
}

#footer-hal { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 80%;
}
</style>
<body>
<p style="margin: 3%">
  <b>VIII. REFERENSI</b>
</p>
<p style="margin:4%;">
  <ol>
    <li>Keputusan Menteri Kesehatan Republik Indonesia, Nomor HK,02,02/MENKES/ 400/ 2016, Tentang Balai Besar Laboratorium Kesehatan sebagai penyelenggara PME tingkat Nasional, 3 Agustus 2016, Jakarta,<br><br></li>
    <li>Badan Standarisasi, penilaian kesesuaian - persyaratan umum Uji Profesiensi (SNI ISO/ IEC 17043 : 2010), Jakarta,<br><br></li>
    <li>Departemen Kesehatan RI, Direktorat Jendral Pelayanan medik, Direktorat Laboratorium Kesehatan, Tentang Pedoman Praktek Laboratorium yang benar (GLP), 2008,<br><br></li>
    <li>Implementasi SNI ISO/ IEC 17043 : 2010 dan Teknik Analisis data uji profisiensi / Interlaboratory Comparison, 2016,</li>
  </ol>
  
</p>

<p style=" margin-left:  60%;">
  Jakarta, {{ _dateIndo($t) }}<br>
  Deputi Manajer Mutu<br>
  Balai Besar Laboratorium Kesehatan <br>
  J a k a r t a

  <br><br><br><br><br>
  dr. Eky Setiyo Kurniawan<br>
  NIP 198106142010121001
</p>

<div id="footer">
    PNPME-BBLK JAKARTA
    
</div>
<div id="footer-hal">
    Halaman …..  dari …
</div>
<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>
</body>



    