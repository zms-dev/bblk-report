<style type="text/css">
body{
    font-family: arial;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}

p{
  font-size:15px;
  word-spacing: 0.3em;
}

li{
  font-size:15px;
  word-spacing: 0.3em;
}


#footer { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 6%;
}

#footer-hal { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 80%;
}
</style>
<body>
<p style="margin: 3%">
  <b>VII. REFERENSI</b>
</p>
<p style="margin:4%;">
  <ol>
  <li>Kementerian Kesehatan RI.2013.<i>Petunjuk Teknis Pembuatan Sediaan Rujukan Mikroskopis TB Untuk Uji Profisiensi</i>. Jakarta:Kemenkes RI<br><br></li>
  <li><i>Penilaian Kesesuaian Persyaratan Umum Uji Profisiensi</i>. ISO/IEC 17043 : 2010<br><br></li>
  <li>SPIN.2016. <i>Implementasi SNI ISO/IEC 17043:2010 Dan Teknik Analisis Data Uji Profisiensi / Interlaboratory Comparison</i>Spinsinergi-Bandung<br><br></li>
  <li>Aziz Mohamed Abdoel,dkk.2002.<i>External Quality Assessment  for AFB Smear Microscopy</i>.Washington,DC:Association of Public Health Laboratories<br><br></li>
  <li>Kantor Isabel Narvaiz De,dkk.1998.<i>Laboratory Services in Tuberculosis Control Microscopy Part II</i>.Genewa:WHO<br><br></li>
</ol>
</p>

<p style=" margin-left:  60%;">
  Jakarta, {{ _dateIndo($t) }}<br>
  Deputi Manajer Mutu<br>

  <br><br><br><br><br>
  <b>dr. Koesprijani,Sp.PK<b><br>
  NIP. 196402231990032001
</p>

<div id="footer">
    PNPME-BBLK JAKARTA
</div>
<div id="footer-hal">
    Halaman …..  dari …
</div>
<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>
</body>



    