<style type="text/css">
body{
    font-family: arial;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}

p{
  font-size:15px;
  word-spacing: 0.3em;
}

li{
  font-size:15px;
  word-spacing: 0.3em;
}


#footer { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 6%;
}

#footer-hal { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 80%;
}
</style>
<body>
<p style="margin: 3%">
  <b>VIII. REFERENSI</b>
</p>
<p style="margin:4%; ">
  <ul style="list-style-type: none;">
    <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Laporan akhir ini kami kirimkan pada para peserta sebagai umpan balik dari BBLK Surabaya yang diharapkan dapat menjadi acuan yang berguna untuk memelihara atau meningkatkan kinerja laboratorium peserta di masa yang akan datang dalam melakukan pemeriksaan Kimia Air untuk pengujian parameter Besi (Fe), Mangan (Mn), Kadmium (Cd), dan Kromium (Cr). Balai Besar Laboratorium Kesehatan Jakarta juga menyertakan sertifikat keikutsertaan kepada laboratorium yang telah memberikan laporan hasil pemeriksaan. Mohon hubungi contact person kami, Almeida Dos Santos, S.Si. (HP 0852 3106 2866), bila ada hal yang kurang jelas.<br><br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Terima kasih atas kesediaan bapak/ibu/saudara/i sekalian yang telah mengikuti PNPME - KA Siklus II tahun 2017 ini. </li>
  </ul>
  

</p>

<p style=" margin-left:  60%;">
  Jakarta, {{_dateIndo($t)}}<br><br>
  Deputi Manajer Mutu<br>
  Balai Besar Laboratorium Kesehatan <br>
  S u r a b a y a

  <br><br><br><br><br>
  <b>dr. Koesprijani, Sp. PK</b><br>
  NIP 19640223 199003 2 001

</p>

<div id="footer">
    PNPME-BBLK JAKARTA
    
</div>
<div id="footer-hal">
    Halaman …..  dari …
</div>
<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>
</body>



    