<style type="text/css">
body{
    font-family: arial;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}

p{
  font-size:15px;
  word-spacing: 0.3em;
}
li{
  font-size:15px;
  word-spacing: 0.3em;
}


#footer { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 6%;
}

#footer-hal { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 80%;
}
</style>
<body>
<p style="margin: 3%">
  <b>VI. KESIMPULAN & PENUTUP</b>
</p>
<p style="margin:4%;">
<ol>
  <li>Program Nasional Pemantapan Mutu Eksternal Mikrobiologi telur cacing Siklus 1 tahun 2017 yang diikuti oleh 77 (tujuh puluh tujuh) peserta. Seluruh peserta telah mengirimkan laporan hasil jawaban.<br><br></li>
  <li>Sebanyak 64% atau 49 (empat puluh sembilan) peserta mendapatkan nilai 76-100 untuk keempat sediaan, mendapat nilai SANGAT BAIK<br><br></li>
  <li>Sebanyak 18% atau 14 (empat belas) peserta mendapatkan nilai 51-75 untuk keempat sediaan, mendapat nilai BAIK<br><br></li>
  <li>Sebanyak 14% atau 11 (sebelas) peserta mendapatkan nilai 26-50 untuk keempat sediaan, mendapat nilai KURANG<br><br></li>
  <li>Sebanyak 4% atau 3 (tiga) peserta mendapatkan nilai 0-20 untuk keempat sediaan, mendapat nilai KURANG SEKALI<br><br></li>
</ol>
<ul style="list-style-type: none;">
  <li>&nbsp;&nbsp;&nbsp;&nbsp;Laporan akhir ini kami kirimkan kepada peserta sebagai umpan balik keikutsertaan program Nasional Pemantapan Mutu Eksternal (PNPME) yang diselenggarakan oleh  Balai Besar Laboratorium Kesehatan Jakarta yang dapat digunakan untuk melakukan perbaikan atau peningkatan dalam melakukan pemeriksaan telur cacing. Balai Besar Laboratorium Kesehatan Jakarta sebagai penyelenggara PNPME juga menyertakan sertifikat keikutsertaan kepada laboratorium peserta. Mohon hubungi kontak person <b>Husnul Kholidah Silvy Safitri,SST</b> telp. 081938264349 bila ada yang kurang jelas.<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Terima kasih atas kesediaan saudara telah mengikuti Program Nasional Pemantapan Mutu Eksternal Mikrobiologi Telur Cacing Siklus 1 tahun 2017.</li>
</ul>

</p>

<div id="footer">
    PNPME-BBLK JAKARTA
    
</div>
<div id="footer-hal">
    Halaman …..  dari …
</div>
<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>
</body>



    