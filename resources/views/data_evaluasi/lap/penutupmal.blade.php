<style type="text/css">
body{
    font-family: arial;
    font-size: 12px;
}
#peserta_pme td,#peserta_pme th{ 
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

th, td {
    padding: 2px 5px;
}

.header{
position: fixed;
}

.header {
top: 0;
}

p{
  font-size:15px;
  word-spacing: 0.3em;
}


#footer { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 6%;
}

#footer-hal { 
    position: fixed; 
    bottom: 0px;
    width: 100%;
    margin-left: 80%;
}
</style>
<body>
<p style="margin: 3%">
  <b>VII. REFERENSI</b>
</p>
<p style="margin:4%;">
  <ol>
    <li>Penilaian Kesesuaian Persyaratan Umum Uji Profisiensi. ISO/IEC 17043 : 2010<br><br></li>
    <li>SPIN.2016. Implementasi SNI ISO/IEC 17043:2010 <i>Dan Teknik Analisis Data Uji Profisiensi / Interlaboratory Comparison</i>. Spinsinergi : Bandung<br><br></li>
    <li>Peraturan Menteri Kesehatan Republik Indonesia Nomor 68 Tahun 2015<br><br></li>
    <li>DEPKES.RI. 2008. <i>Pedoman Praktek Laboratorium Yang Benar (Good Laboratory Practice)</i> .Cetakan 3. Direktorat Laboratorium Kesehatan. Direktorat Jenderal Pelayanan Medik Departemen Kesehatan RI : Jakarta<br><br></li>
    <li>KEMENKES.RI.2016. Modul Peningkatan Kemampuan Mikroskopis Malaria. Direktorat Jendral Pencegahan dan Pengendalian Penyakit Tular Vektor dan Zoonotik.</li>
  </ol>
  


</p>

<p style=" margin-left:  60%;">
  {{ _dateIndo($t) }}<br>
  Deputi Manajer Mutu<br>
  Balai Besar Laboratorium Kesehatan <br>
  S u r a b a y a

  <br><br><br><br><br>
 dr. Koesprijani, Sp.PK<br>
NIP.196402231990032001

</p>

<div id="footer">
    PNPME-BBLK JAKARTA
    
</div>
<div id="footer-hal">
    Halaman …..  dari …
</div>
<script type="text/javascript">
$(document).ready(function() {
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $("#peserta_pme tr td:first-child").each(function() { //for each first td in every tr
      var $this = $(this);
      if ($this.text() == prevTDVal) { // check value of previous td text
         span++;
         if (prevTD != "") {
            prevTD.attr("rowspan", span); // add attribute to previous td
            $this.remove(); // remove current td
         }
      } else {
         prevTD     = $this; // store current td 
         prevTDVal  = $this.text();
         span       = 1;
      }
   });
});
</script>
</body>



    