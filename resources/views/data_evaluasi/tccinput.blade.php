@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')
@section('content')
   <div class="wrapper" style="margin-top: -40px;">
        <!-- Sidebar  -->
       <nav id="sidebar-peserta" style="text-align: center;">
            <div class="sidebar-header">
                <a href="{{ url('data-evaluasi') }}"><h3>Laporan Akhir</h3></a> 
                    <form method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">  
                        <select name="tanggal" class="form-control" required>
                        <option readonly>Pilih Tahun</option>
                        <option {{ $tanggal=="2017"?'selected':'' }} value="2017">2017</option>
                        <option {{ $tanggal=="2018"?'selected':'' }} value="2018">2018</option>
                        <option {{ $tanggal=="2019"?'selected':'' }} value="2019">2019</option>
                        <option {{ $tanggal=="2020"?'selected':'' }} value="2020">2020</option>
                        <option {{ $tanggal=="2021"?'selected':'' }} value="2021">2021</option>
                        <option {{ $tanggal=="2022"?'selected':'' }} value="2022">2022</option>
                        <option {{ $tanggal=="2023"?'selected':'' }} value="2023">2023</option>
                        <option {{ $tanggal=="2024"?'selected':'' }} value="2024">2024</option>
                        <option {{ $tanggal=="2025"?'selected':'' }} value="2025">2025</option>
                        </select>
                      <input type="submit" name="cari" required="" class="btn btn-info" value="cari" style="margin-top: 8px;">
                    </form>
            </div>

            <ul class="list-unstyled components" style="text-align: center">        
                <li>
                 
                    @foreach($datas1 as $val)
                    <a href=" {{URL('').$val->Link}}/data-evaluasi/{{$val->id}}">
                      @if($val->alias == "HEM")
                      Hematologi   
                      @elseif($val->alias == "KKL")
                      Kimia Klinik
                      @elseif($val->alias == "URI")
                      Urinalisa
                      @elseif($val->alias == "BTA")
                      Mikroskopis BTA
                      @elseif($val->alias == "TCC")
                      Mikroskopis Telur Cacing
                      @elseif($val->alias == "HIV")
                      Anti HIV
                      @elseif($val->alias == "SIF")
                      Syphilis Anti TP
                      @elseif($val->alias == "HBS")
                      HBsAg
                      @elseif($val->alias == "HCV")
                      Anti HCV
                      @endif
                    </a>
                    @endforeach
                </li>
              </ul>                    
        </nav>
        <!-- Page Content  -->
        <div id="content">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fa fa-align-justify"></i>
                        <span></span>
                    </button>
                </div>
                <label style="margin: 10px 20px 0px 0px; font-size: 28px; position: absolute; top: 0; right: 0" style="font-size: 30px; text-align: center;">Mikroskopis Telur Cacing</label>
            </nav>  
            <div class="line"></div>
                <form class="form-horizontal" method="get" enctype="multipart/form-data" action="{{url('/telur-cacing/data-evaluasi')}}/print/{{$id}}?y={{$siklus}}" target="_blank">
                <div class="">
                    <div class="row">
                        <div class="col-md-12" >
                            <div class="col-md-12" style="text-align:center;">
                                <p>
                                    Evaluasi Mikroskopis Telur Cacing
                                    <center>Siklus {{ $siklus }}</center>
                                </p>
                                    <input type="submit" name="simpan" value="Print" class="btn btn-info">
                                    {{ csrf_field() }}
                                </div>  
                            </div>  
                        </div> 
                    </div>
                </form>
            <div class="line"></div>

        </div>
    </div>
</div> 
<!-- Footer -->
    <div class=" footer">
        <div class="col-md-12 copy"><marquee>COPYRIGHT &copy; BBLK ALL RIGHT RESERVED - POWERED BY Pilar</marquee></div>
    </div>
</div>
@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
    });
    </script>
@endif
 
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar-peserta').toggleClass('active');
            });
        });
    </script>
@endsection
