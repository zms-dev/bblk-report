@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')
@extends('layouts.menu_hasil_evaluasi')
@section('content')
        <!-- Page Content  -->
        <div id="content">
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fa fa-align-justify"></i>
                        <span></span>
                    </button>
                </div>
                <label style="margin: 10px 20px 0px 0px; font-size: 28px; position: absolute; top: 0; right: 0">ANTI HIV</label>
            </nav>
            <div class="line"></div>
             <div class="row">
                <div class="col-md-12">
                  <div class="col-md-12" style="text-align:center;">
                        <p>
                            Laporan Penyelenggaraan PNPME Anti HIV
                        </p>
                                  <a href="{{url('/laporan-akhir-hiv/data-evaluasi/')}}/{{$id}}" class="btn btn-info" target="_blank" style="display: none;">Sampul Laporan</a>
                            
                            <button class="btn-show btn btn-info" style="width: 170px">Laporan</button>
                         {{--    <a href="{{url('/laporan-akhir-hiv/data-evaluasi/penutup/')}}/{{$id}}" class="btn btn-info" target="_blank">Penutup Laporan</a> --}}

                            <br><br>
                            <div class="myText">
                              <div class="row">
                                  <div class="col-md-8 col-md-offset-2">
                                      <div class="panel panel-default">
                                          <div class="panel-heading">Data Upload Laporan Akhir</div>
                                          <div class="panel-body">
                                              
                                              <table class="table table-striped table-bordered table-hover dataTables-data" width="100%">
                                                  <thead>
                                                  <tr>
                                                      <th>No</th>
                                                      <th>File</th>
                                                      <th>Tahun</th>
                                                      <th>Siklus</th>
                                                      <th>Parameter</th>
                                                      <th>Aksi</th>
                                                  </tr>
                                                  </thead>
                                                  <tbody>
                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>  
               @if(count($data))    
                    @foreach($data as $val)
                        @if($val->id_bidang > '5')
                        <div class="row">
                        <div class="col-md-12" >
                            <div class="col-md-12" style="text-align:center;">
                                <p>
                                    Hasil Saudara
                                </p>
                                <div class="btn-group">
                                  @if($siklus == "1")
                                    @if($val->siklus_1 == 'done')
                                      @if($val->pemeriksaan == 'done')
                                         @if($val->status_data1 == 2)                                          
                                            <a style="width: 170px" class="btn btn-info" href="{{url('anti-hiv/data-evaluasi/print/')}}/{{$id}}?y=1" target="_blank">Print Hasil Evaluasi</a>
                                         @endif
                                      @endif
                                    @endif
                                  @else
                                    @if($val->siklus_2 == 'done')
                                      @if($val->pemeriksaan2 == 'done')
                                        @if($val->status_data2 == 2)
                                          <a style="width: 170px" class="btn btn-info" href="{{url('anti-hiv/data-evaluasi/print/')}}/{{$id}}?y=2" target="_blank">Print Hasil Evaluasi</a>
                                        @endif
                                      @endif
                                    @endif
                                  @endif
                                </div><br><br>
                                <div class="btn-group">
                                  @if($siklus == "1")
                                    @if($val->siklus_1 == 'done')
                                      @if($val->pemeriksaan == 'done')
                                         @if($val->status_data1 == 2)     
                                            <a style="width: 170px" class="btn btn-info" style="" href="{{ url('hasil-pemeriksaan/anti-hiv/sertifikat/print/') }}/{{$val->id}}?y=2&t=undefined&n=undefined&download=true" target="_blank">Cetak Sertifikat</a>
                                         @endif
                                      @endif
                                    @endif
                                  @else
                                    @if($val->siklus_2 == 'done')
                                      @if($val->pemeriksaan2 == 'done')
                                        @if($val->status_data2 == 2)
                                          <a style="width: 170px" class="btn btn-info" style="" href="{{ url('hasil-pemeriksaan/anti-hiv/sertifikat/print/') }}/{{$val->id}}?y=2&t=undefined&n=undefined&download=true" target="_blank">Cetak Sertifikat</a>
                                        @endif
                                      @endif
                                    @endif
                                  @endif
                                  </ul>
                                </div> 
                              <p><small>(*Laporan Akhir, Hasil, Grafik dan Sertifikat mohon dicetak di kertas berukuran A4)</small></p>
                            </div>
                        </div> 
                    </div>
                    <!-- <div class="col-md-12" align="center" style="margin-top: 20px">
                      <a style="" href="{{ url('hasil-pemeriksaan/anti-hiv/sertifikat/print/') }}/{{$val->perusahaan_id}}?y=1&t=undefined&n=undefined&download=true" class="btn btn-info">Cetak Sertifikat</a>
                    </div> -->
                    @endif
              @endforeach
              @else
              <p style="font-size: 30px; text-align: center;" hidden="">Tidak Ada Data</p>
              @endif
              <div class="line"></div>
</div>
</div> 
<!-- Footer -->
    <div class="footer">
        <div class=" copy"><marquee>COPYRIGHT &copy; BBLK ALL RIGHT RESERVED - POWERED BY Pilar</marquee></div>
    </div>
@if(Session::has('message'))
    <div id="snackbar">{{ Session::get('message') }}</div>

    <script>
    $('document').ready(function(){
          var x = document.getElementById("snackbar")
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
    });
    </script>
@endif
 
 
@endsection

@section('scriptBlock')
<script>
      var table = $(".dataTables-data");
      var dataTable = table.DataTable({
      responsive:!0,
      "serverSide":true,
      "processing":true,
      "ajax":{
          url : "{{url('/data-evaluasi/data-laporan-akhir/hiv')}}"
      },
      dom:"<'row'<'col-sm-6'i><'col-sm-6'f>><'row'<'col-sm-12'<'table-responsive'tr>>><'row'<'col-sm-6'l><'col-sm-6'p>>",
      language:{
          paginate:{
              previous:"&laquo;",
              next:"&raquo;"
          },search:"_INPUT_",
          searchPlaceholder:"Search..."
      },
      "columns":[
              {"data":"rownum","name":"rownum","searchable":false,"orderable":true, "width" : "5%"},
              {"data":"file","name":"file","searchable":true,"orderable":true},
              {"data":"tahun","name":"tahun","searchable":true,"orderable":true},
              {"data":"siklus","name":"siklus","searchable":true,"orderable":true},
              {"data":"alias","name":"alias","searchable":true,"orderable":true},
              {"data":"action","name":"action","searchable":false,"orderable":false,"width" : "10%"},
      ],
      order:[[1,"asc"]]
  });

</script>
 
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar-peserta').toggleClass('active');
            });
        });

        $(".myText").hide();
        $(".btn-show").click(function() {
          var targetna = $(".myText");
          if(targetna.css('display') == 'none'){
            console.log('show');
           $(".myText").show('100');
          }else{
            console.log('hide');
            targetna.hide('100');
          }
        
        });
    </script>
@endsection

