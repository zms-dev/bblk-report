<?php
function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
}
?>
@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Download Manual Books dan Juknis</div>

                @if (Auth::guest())
                @else
                <div class="panel-body">
                    <br><label><p>Download Manual Book Input Hasil:</p></label>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Bidang Parameter</th>
                            <th>Download</th>
                        </tr>
                        @if(count($data))
                        <?php $no = 0; ?>
                        @foreach($data as $val)
                        <?php $no++ ?>
                        <tr>
                          <td><center>{{$no}}</center></td>
                          <td width="90%">{{$val->desc}}</td>
                          <td><center>
                            <a href="{{URL::asset('asset/backend/manual').'/'.$val->file}}" download>
                                <i class="glyphicon glyphicon-cloud-download"></i>
                            </a></center>
                          </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="3">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                    <br><label><p>Download Juklak :</p></label>
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>File</th>
                            <th>Download</th>
                        </tr>
                        @if(count($juklak))
                        <?php $no = 0; ?>
                        @foreach($juklak as $val)
                        <?php $no++ ?>
                        <tr>
                          <td><center>{{$no}}</center></td>
                          <td width="90%">{{$val->file}}</td>
                          <td><center>
                            <a href="{{URL::asset('asset/backend/juklak').'/'.$val->file}}" download>
                                <i class="glyphicon glyphicon-cloud-download"></i>
                            </a></center>
                          </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="3">Data tidak ditemukan</td>
                        </tr>
                        @endif
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection