@extends('layouts.app')
@extends('layouts.menu')
@extends('layouts.menu_dashboard')
@extends('layouts.menu_laporan')
@extends('layouts.menu_evaluasi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Update Pembayaran</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="get">
                      <div>
                          <label for="exampleInputEmail1">Tahun</label>
                          <div class="controls input-append date form_datetime" data-link-field="dtp_input1">
                            @if($tahun != NULL)
                              <input size="16" type="text" value="{{$tahun}}" readonly class="form-control" name="tahun">
                            @else
                              <input size="16" type="text" value="2019" readonly class="form-control" name="tahun">
                            @endif
                              <span class="add-on"><i class="icon-th"></i></span>
                          </div>
                      </div>
                      <div>
                          <label for="exampleInputEmail1">Siklus</label>
                          <select name="siklus" class="form-control" required>
                            <option value="{{$siklus}}">{{$siklus}}</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                      </div><br>
                      <button type="submit" name="proses" class="btn btn-info">Proses</button>
                    </form>
                    <br>
                    <form class="form-horizontal" action="{{url('update-pembayaran')}}" method="post" enctype="multipart/form-data" >
                    <table id="example" class="display table table-bordered" style="width:100%">
                      <thead>
                        <tr>
                            <th>Nama Peserta</th>
                            <th>Labolatoruim</th>
                            <th>Siklus</th>
                            <th>Parameter</th>
                            <th>Tarif</th>
                            <th>Upload File</th>
                            <th>Cek</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if($data != NULL)
                        <?php $no = 0; ?>
                        @foreach($data as $key => $val)
                        <?php $no++;?>
                        <tr>
                          <td>{{date('Y', strtotime($val->created_at))}}<input type="hidden" name="email[{{$key+1}}]" value="{{$val->email}}" ></td>
                          <td>{{$val->nama_lab}}<input type="hidden" name="nama_lab[{{$key+1}}]" value="{{$val->nama_lab}}" ></td>
                          <td>{{$val->siklus}}</td>
                          <td>{!!str_replace('|','<br/><br/>',$val->parameter) !!}<input type="hidden" name="parameter[{{$key+1}}]" value="{{$val->parameter}}" ></td>
                          <input type="hidden" name="siklus[{{$key+1}}]" value="{{$val->siklus}}" >

                          <?php $imun1 = 0; $imun2 = 0; $imun12 = 0;  $bidang = explode("|",$val->bidang); ?>
                          @foreach($bidang as $valb)
                              <?php 
                                  if($valb == "Imunologi1"){
                                      $imun1++;
                                  }elseif($valb == "Imunologi2"){
                                      $imun2++;
                                  }elseif($valb == "Imunologi12"){
                                      $imun12++;
                                  }
                              ?>
                          @endforeach
                            <?php
                              $imunt1 = 0;
                              $imunt2 = 0;
                              $imunt12 = 0;
                              if($imun1 == 4){
                                  $imunt1 = "200000";
                              }elseif($imun2 == 4){
                                  $imunt2 = "200000";
                              }elseif($imun12 == 4){
                                  $imunt12 = "400000";
                              }
                            ?>
                          <td>
                            {{number_format($val->jumlah_tarif - $imunt1 - $imunt2 - $imunt12)}}
                          </td>
                          <td>
                            <input type="file" name="file[{{$key+1}}]">
                          </td>
                          <td>
                            <center><input type="checkbox" name="bidang[{{$key+1}}]" value="{{$val->id}}"></center>
                          </td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                    <label>Catatan :</label>
                    <p>*) Beri tanda &#10003; pada lab yang tidak memiliki PKS</p>
                    <br>
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(".form_datetime").datetimepicker({
    format: "yyyy",
    autoclose: true,
    todayBtn: true,
    pickerPosition: "bottom-right",
    minView: 4,
    startView: 4
});
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@endsection