<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use App\HpHeader;
use DB;
use App\Parameter;
use App\MetodePemeriksaan;
use App\HpDetail;
use App\ZScore;
use App\ZScoreAlat;
use App\ZScoreMetode;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\register as Register;
use App\JobsEvaluasi;


class Evaluasi implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $dataInput = [];
    protected $nameEvaluasi = "";
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dataInput,$nameEvaluasi)
    {
        $this->dataInput = $dataInput;
        $this->nameEvaluasi = $nameEvaluasi;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $input = $this->dataInput;
        $siklus = $input['siklus'];
        $type = $input['type'];
        $date = $input['date'];
        if($this->nameEvaluasi == "evaluasi-kimiaklinik"){
            
        $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','2')->get();
            foreach($datas as $r){
                $r->sub_bidang =  DB::table('tb_registrasi')
                    ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                    ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                    ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                    ->where('tb_registrasi.perusahaan_id', $r->id)
                    ->where('sub_bidang.id', '=', '2')
                    ->where('status', 3)
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2);
                        })
                    ->where(function($query)
                        {
                            if ('pemeriksaan' != 'done') {
                                $query->orwhere('pemeriksaan2', 'done');
                            }
                            if ('pemeriksaan2' != 'done') {
                                $query->orwhere('pemeriksaan', 'done');
                            }
                        })
                    ->get();
                foreach($r->sub_bidang as $sb){
                    $zscore = DB::table('tb_zscore')
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->where('tahun', $date)
                            ->where('form', '=', 'kimia klinik')
                            ->where('id_registrasi', $sb->id);
                    $zscore->delete();
                    $zscoremetode = DB::table('tb_zscore_metode')
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->where('tahun', $date)
                            ->where('form', '=', 'kimia klinik')
                            ->where('id_registrasi', $sb->id);
                    $zscoremetode->delete();
                    $dataSave = [];
                    $dataParamA = DB::table('parameter')
                        ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                        ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                        ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                        ->leftjoin('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('hp_headers.siklus', $siklus)
                        ->where('hp_headers.type', $type)
                        ->where('tb_registrasi.id', $sb->id)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->select('tb_instrumen.id as idalat', 'metode_pemeriksaan.id as idmetode',  'hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.instrument_lain', 'hp_details.alat as Instrumen', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                        ->get();
                    foreach ($dataParamA as $key => $valParam) {
                        $sd = DB::table('tb_sd_median')
                                ->where('parameter', $valParam->id)
                                ->where('siklus', $siklus)
                                ->where('tahun', $date)
                                ->where('tipe', $type)
                                ->where('form', '=', 'kimia klinik')
                                ->get();
                        $valParam->sd = $sd;

                        $sdmetode = DB::table('tb_sd_median_metode')
                                ->where('metode', $valParam->idmetode)
                                ->where('siklus', $siklus)
                                ->where('tahun', $date)
                                ->where('tipe', $type)
                                ->where('form', '=', 'kimia klinik')
                                ->first();
                        $valParam->sdmetode = $sdmetode;

                        $dataSave['parameter_id'][$key] = $valParam->id;
                        $dataSave['metode_id'][$key] = $valParam->idmetode;
                        $valParam->hasil_pemeriksaan = str_replace(",",".",$valParam->hasil_pemeriksaan);
                        $valParam->hasil_pemeriksaan = ($valParam->hasil_pemeriksaan == "-" ? NULL : str_replace(",",".",$valParam->hasil_pemeriksaan));

                        $labelHasil = $valParam->hasil_pemeriksaan;
                        try{
                            $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan  *1;
                        }catch(\Exception $e){
                            $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                        }
                        // dd($valParam->sd[0]->median);
                        if($valParam->hasil_pemeriksaan == "-" || $valParam->hasil_pemeriksaan == null || empty($valParam->hasil_pemeriksaan) || is_string($valParam->hasil_pemeriksaan)){
                            if($valParam->hasil_pemeriksaan == '0' || $valParam->hasil_pemeriksaan == '0.0' || $valParam->hasil_pemeriksaan == '0.00' || is_string($valParam->hasil_pemeriksaan)){
                                $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                            }else{
                                $valParam->hasil_pemeriksaan = NULL;
                            }
                            $dataSave['zscore'][$key] = "-";
                            $dataSave['zscore_metode'][$key] = "-";
                        }else{
                            $target_parameter = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->median) ? $valParam->sd[0]->median : '-') : '-');
                            $sd_parameter = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->sd) ? $valParam->sd[0]->sd : '-') : '-');
                            // dd($sd_parameter);
                            $dataSave['zscore'][$key] = "-";
                            if(($target_parameter != '-' && $sd_parameter != '-')){
                                $dataSave['zscore'][$key] = number_format(($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd,2);
                            }

                            $target_metode = (!empty($valParam->sdmetode) ? (!empty($valParam->sdmetode[0]->median) ? $valParam->sdmetode[0]->median : '-') : '-');
                            $sd_metode = (!empty($valParam->sdmetode) ? (!empty($valParam->sdmetode[0]->sd) ? $valParam->sdmetode[0]->sd : '-') : '-');
                            $dataSave['zscore_metode'][$key] = '-';
                            if(($target_metode != '-' && $sd_metode != '-')){
                                $dataSave['zscore_metode'][$key] = number_format(($valParam->hasil_pemeriksaan - $valParam->sdmetode[0]->median) / $valParam->sdmetode[0]->sd,2);
                            }
                        }
                        // dd($dataSave['zscore'][$key]);
                    }
                    $i = 0;
                    if(!empty($dataSave)){
                        foreach ($dataSave['parameter_id'] as $alat) {
                            $Save = new ZScore;
                            $Save->id_registrasi = $sb->id;
                            $Save->parameter = $dataSave['parameter_id'][$i];
                            $Save->zscore = $dataSave['zscore'][$i];
                            $Save->type = $type;
                            $Save->siklus = $siklus;
                            $Save->tahun = $date;
                            $Save->form = 'kimia klinik';
                            $Save->save();

                            $Save = new ZScoreMetode;
                            $Save->id_registrasi = $sb->id;
                            $Save->parameter = $dataSave['parameter_id'][$i];
                            $Save->metode = $dataSave['metode_id'][$i];
                            $Save->zscore = $dataSave['zscore_metode'][$i];
                            $Save->type = $type;
                            $Save->siklus = $siklus;
                            $Save->tahun = $date;
                            $Save->form = 'kimia klinik';
                            $Save->save();
                            $i++;
                        }
                    }
                }
            }
        }elseif($this->nameEvaluasi == "evaluasi-parameter"){
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','1')->get();
            foreach($datas as $r){
                $r->sub_bidang =  DB::table('tb_registrasi')
                    ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                    ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                    ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                    ->where('tb_registrasi.perusahaan_id', $r->id)
                    ->where('sub_bidang.id', '=', '1')
                    ->where('status', 3)
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2);
                        })
                    ->where(function($query)
                        {
                            if ('pemeriksaan' != 'done') {
                                $query->orwhere('pemeriksaan2', 'done');
                            }
                            if ('pemeriksaan2' != 'done') {
                                $query->orwhere('pemeriksaan', 'done');
                            }
                        })
                    ->get();
                foreach($r->sub_bidang as $sb){
                    $zscore = DB::table('tb_zscore')
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->where('tahun', $date)
                            ->where('form', '=', 'hematologi')
                            ->where('id_registrasi', $sb->id);
                    $zscore->delete();
                    $dataSave = [];
                    $dataParamA = DB::table('parameter')
                        ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                        ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                        ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('hp_headers.siklus', $siklus)
                        ->where('hp_headers.type', $type)
                        ->where('tb_registrasi.id', $sb->id)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan',DB::raw('hp_details.id as hp_detail_id'))
                        ->orderBy('parameter.bagian', 'asc')
                        ->get();
                    foreach ($dataParamA as $key => $valParam) {
                        $sd = DB::table('tb_sd_median')
                                ->where('parameter', $valParam->id)
                                ->where('siklus', $siklus)
                                ->where('tahun', $date)
                                ->where('tipe', $type)
                                ->where('form', '=', 'hematologi')
                                ->get();
                        $valParam->sd = $sd;
                        $dataSave['parameter_id'][$key] = $valParam->id;
                        $valParam->hasil_pemeriksaan = str_replace(",",".",$valParam->hasil_pemeriksaan);
                        $valParam->hasil_pemeriksaan = ($valParam->hasil_pemeriksaan == "-" ? NULL : str_replace(",",".",$valParam->hasil_pemeriksaan));
                        if (count($valParam->sd) && $valParam->hasil_pemeriksaan != NULL) {
                            $z = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                            $dataSave['zscore'][$key] = number_format($z, 1);
                        }else{
                            $z = "-";
                            if($valParam->hasil_pemeriksaan == NULL){
                                $dataSave['zscore'][$key] = "Tidak di analisis";
                            }elseif($z == '-'){
                                $dataSave['zscore'][$key] = "-";
                            }
                        }
                    }
                    $i = 0;
                    if(!empty($dataSave)){
                        foreach ($dataSave['parameter_id'] as $alat) {
                            $Save = new ZScore;
                            $Save->id_registrasi = $sb->id;
                            $Save->parameter = $dataSave['parameter_id'][$i];
                            $Save->zscore = $dataSave['zscore'][$i];
                            $Save->type = $type;
                            $Save->siklus = $siklus;
                            $Save->tahun = $date;
                            $Save->form = 'hematologi';
                            $Save->save();
                            $i++;
                        }
                    }
                }

            }
        }elseif($this->nameEvaluasi == "evaluasi-alat"){
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','1')->get();

            foreach($datas as $r){
            $r->sub_bidang =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $r->id)
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
                foreach($r->sub_bidang as $sb){
                    $zscore = DB::table('tb_zscore_alat')
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->where('tahun', $date)
                            ->where('form', '=', 'hematologi')
                            ->where('id_registrasi', $sb->id);
                            $zscore->delete();
                    $dataSave = [];
                    $dataParamA = DB::table('parameter')
                            ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                            ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                            ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                            ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_headers.siklus', $siklus)
                            ->where('hp_headers.type', $type)
                            ->where('tb_registrasi.id', $sb->id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'tb_instrumen.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                            ->orderBy('parameter.bagian', 'asc')
                            ->get();
                    foreach ($dataParamA as $key => $valParam) {
                        $sd = DB::table('tb_sd_median_alat')
                                ->where('parameter', $valParam->id_parameter)
                                ->where('alat', $valParam->id)
                                ->where('siklus', $siklus)
                                ->where('tahun', $date)
                                ->where('tipe', $type)
                                ->where('form', '=', 'hematologi')
                                ->get();
                        $valParam->sd = $sd;

                        $dataSave['parameter_id'][$key] = $valParam->id_parameter;
                        $dataSave['alat'][$key] = $valParam->id;
                        $valParam->hasil_pemeriksaan = str_replace(",",".",$valParam->hasil_pemeriksaan);

                        $valParam->hasil_pemeriksaan = ($valParam->hasil_pemeriksaan == "-" ? NULL : str_replace(",",".",$valParam->hasil_pemeriksaan));
                        if (count($valParam->sd) && $valParam->hasil_pemeriksaan != NULL) {
                            $z = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                            $dataSave['zscore'][$key] = number_format($z, 1);
                        }else{
                            $z = "-";
                            if($valParam->hasil_pemeriksaan == NULL){
                                $dataSave['zscore'][$key] = "Tidak di analisis";
                            }elseif($z == '-'){
                                $dataSave['zscore'][$key] = "-";
                            }
                        }

                    }
                    $i = 0;
                    if(!empty($dataSave)){
                        foreach ($dataSave['parameter_id'] as $alat) {
                            $Save = new ZScoreAlat;
                            $Save->id_registrasi = $sb->id;
                            $Save->parameter = $dataSave['parameter_id'][$i];
                            $Save->alat = $dataSave['alat'][$i];
                            $Save->zscore = $dataSave['zscore'][$i];
                            $Save->type = $type;
                            $Save->siklus = $siklus;
                            $Save->tahun = $date;
                            $Save->form = 'hematologi';
                            $Save->save();
                            $i++;
                        }
                    }
                }
            }
        }else{
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','1')->get();

            foreach($datas as $r){
                $r->sub_bidang =  DB::table('tb_registrasi')
                    ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                    ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                    ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                    ->where('tb_registrasi.perusahaan_id', $r->id)
                    ->where('sub_bidang.id', '=', '1')
                    ->where('status', 3)
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2);
                        })
                    ->where(function($query)
                        {
                            if ('pemeriksaan' != 'done') {
                                $query->orwhere('pemeriksaan2', 'done');
                            }
                            if ('pemeriksaan2' != 'done') {
                                $query->orwhere('pemeriksaan', 'done');
                            }
                        })
                    ->get();
                foreach($r->sub_bidang as $sb){
                    $zscore = DB::table('tb_zscore_metode')
                                ->where('siklus', $siklus)
                                ->where('type', $type)
                                ->where('tahun', $date)
                                ->where('form', '=', 'hematologi')
                                ->where('id_registrasi', $sb->id);
                                $zscore->delete();

                    $dataSave = [];

                    $dataParamA = DB::table('parameter')
                            ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                            ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                            ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                            ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_headers.siklus', $siklus)
                            ->where('hp_headers.type', $type)
                            ->where('tb_registrasi.id', $sb->id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'metode_pemeriksaan.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                            ->orderBy('parameter.bagian', 'asc')
                            ->get();
                    foreach ($dataParamA as $key => $valParam) {
                        $sd = DB::table('tb_sd_median_metode')
                                ->where('parameter', $valParam->id_parameter)
                                ->where('metode', $valParam->id)
                                ->where('siklus', $siklus)
                                ->where('tahun', $date)
                                ->where('tipe', $type)
                                ->where('form', '=', 'hematologi')
                                ->get();
                        $valParam->sd = $sd;

                        $dataSave['parameter_id'][$key] = $valParam->id_parameter;
                        $dataSave['metode'][$key] = $valParam->id;
                        $valParam->hasil_pemeriksaan = str_replace(",",".",$valParam->hasil_pemeriksaan);

                        $valParam->hasil_pemeriksaan = ($valParam->hasil_pemeriksaan == "-" ? NULL : str_replace(",",".",$valParam->hasil_pemeriksaan));
                        if (count($valParam->sd) && $valParam->hasil_pemeriksaan != NULL && $valParam->sd[0]->median != 0) {
                            $z = ($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd;
                            $dataSave['zscore'][$key] = number_format($z, 1);
                        }else{
                            $z = "-";
                            if($valParam->hasil_pemeriksaan == NULL){
                                $dataSave['zscore'][$key] = "Tidak di analisis";
                            }elseif($z == '-'){
                                $dataSave['zscore'][$key] = "-";
                            }
                        }
                        

                    }
                    $i = 0;
                    if(!empty($dataSave)){
                        foreach ($dataSave['parameter_id'] as $alat) {
                            $Save = new ZScoreMetode;
                            $Save->id_registrasi = $sb->id;
                            $Save->parameter = $dataSave['parameter_id'][$i];
                            $Save->metode = $dataSave['metode'][$i];
                            $Save->zscore = $dataSave['zscore'][$i];
                            $Save->type = $type;
                            $Save->siklus = $siklus;
                            $Save->tahun = $date;
                            $Save->form = 'hematologi';
                            $Save->save();
                            $i++;
                        }
                    }
                }
            }
        }
        
        JobsEvaluasi::where('name','=',$this->nameEvaluasi)->where('status','=',0)->update(['status'=>1]);
    }
}
