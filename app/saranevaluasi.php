<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class saranevaluasi extends Model{
    protected $table = 'tb_saran_evaluasi';
    protected $fillable = ['*'];
    public $timestamps = false;

    // public function perusahaan ()
    // {
    // 	return $this->belongsTo('App\daftar', 'perusahaan_id', 'id');
    // }
}