<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rujukanbta extends Model{
    protected $table = 'tb_rujukan_bta';
    protected $fillable = ['kode_bahan_uji','rujukan','siklus','tahun','kuman'];
    public $timestamps = false;
}