<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class bayar extends Model{
    protected $table = 'tb_bayar';
    protected $fillable = ['id_registrasi','tarif','created_by']; 
}