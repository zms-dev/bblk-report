<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class masterimunologi extends Model{
    protected $table = 'master_imunologi';

    protected $fillable = ['kode_peserta','petugas_pemeriksaan','hasil_pemeriksaan','jenis_form','id_registrasi','kode_lab','keterangan'];

    // protected $table = 'tb_cobaan';
    // protected $fillable = ['id_registrasi','siklus','kode_peserta','tgl_penerimaan','tgl_pemeriksaan','kualitas','kolesterol','trigliserida','jenis','catatan','penanggung_jawab','created_by'];
}