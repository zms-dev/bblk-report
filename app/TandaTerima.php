<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TandaTerima extends Model{
    protected $table = 'tb_tanda_terima';

    protected $fillable = ['*'];
    public $timestamps = false;
}