<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    protected $table = 'parameter';

    public function metodePemeriksaan()
    {
    	return $this->hasMany('App\MetodePemeriksaan', 'parameter_id', 'id');
    }
    public function hasilPemeriksaan()
    {
    	return $this->hasMany('App\TbHp', 'parameter_id', 'id');
    }
    public function ReagenImunologi()
    {
    	return $this->hasMany('App\TbReagenImunologi', 'parameter_id', 'id');
    }
    public function Instrumen()
    {
        return $this->hasMany('App\Instrumen', 'id_parameter', 'id');
    }
}
