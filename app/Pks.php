<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pks extends Model{
    protected $table = 'tb_pks';
    protected $fillable = ['pks']; 
}