<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kepekaan extends Model{
    protected $table = 'tb_kepekaan';

    protected $fillable = ['*'];
    public $timestamps = false;
}