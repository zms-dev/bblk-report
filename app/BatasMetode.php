<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BatasMetode extends Model{
    protected $table = 'tb_rentang_metode';

    protected $fillable = ['*'];
    public $timestamps = false;
}