<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class bahanimunologi extends Model{
    protected $table = 'bahan_imunologi';

    protected $fillable = ['id_master_imunologi','tgl_diterima','tgl_diperiksa','no_tabung','jenis'];

    // protected $table = 'tb_cobaan';
    // protected $fillable = ['id_registrasi','siklus','kode_peserta','tgl_penerimaan','tgl_pemeriksaan','kualitas','kolesterol','trigliserida','jenis','catatan','penanggung_jawab','created_by'];
    public $timestamps = false;
}