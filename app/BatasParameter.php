<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BatasParameter extends Model{
    protected $table = 'tb_rentang_parameter';

    protected $fillable = ['*'];
    public $timestamps = false;
}