<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SdMedianMetode extends Model{
    protected $table = 'tb_sd_median_metode';

    protected $fillable = ['*'];
    public $timestamps = false;
}