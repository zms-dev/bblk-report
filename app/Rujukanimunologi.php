<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rujukanimunologi extends Model{
    protected $table = 'tb_rujukan_imunologi';
    protected $fillable = ['parameter','kode_bahan_uji','nilai_rujukan','siklus','tahun','range1','range2'];
    public $timestamps = false;
}