<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rujukanurinalisametode extends Model{
    protected $table = 'tb_urinalisa_metode';
    protected $fillable = ['*'];
    public $timestamps = false;
}