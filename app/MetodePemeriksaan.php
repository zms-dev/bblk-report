<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetodePemeriksaan extends Model
{
    protected $table = 'metode_pemeriksaan';
}
