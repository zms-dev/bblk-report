<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TanggalEvaluasi extends Model
{
	protected $table = 'tanggal_evaluasi';
    protected $fillable = ['*'];
    public $timestamps = false;	
}
