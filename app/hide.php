<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Hide extends Model
{
	protected $table = 'tb_hide_menu';
    protected $fillable = ['*'];
    public $timestamps = true;
}
