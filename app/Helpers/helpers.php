<?php

function numbToWords($x)
{
    $x = abs($x);
    $angka = array("", "satu", "dua", "tiga", "empat", "lima",
    "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($x <12) {
        $temp = " ". $angka[$x];
    } else if ($x <20) {
        $temp = numbToWords($x - 10). " belas";
    } else if ($x <100) {
        $temp = numbToWords($x/10)." puluh". numbToWords($x % 10);
    } else if ($x <200) {
        $temp = " seratus" . numbToWords($x - 100);
    } else if ($x <1000) {
        $temp = numbToWords($x/100) . " ratus" . numbToWords($x % 100);
    } else if ($x <2000) {
        $temp = " seribu" . numbToWords($x - 1000);
    } else if ($x <1000000) {
        $temp = numbToWords($x/1000) . " ribu" . numbToWords($x % 1000);
    } else if ($x <1000000000) {
        $temp = numbToWords($x/1000000) . " juta" . numbToWords($x % 1000000);
    } else if ($x <1000000000000) {
        $temp = numbToWords($x/1000000000) . " milyar" . numbToWords(fmod($x,1000000000));
    } else if ($x <1000000000000000) {
        $temp = numbToWords($x/1000000000000) . " trilyun" . numbToWords(fmod($x,1000000000000));
    }     
    return $temp;
}

function _dateIndo($date)
{ 
    $BulanIndo = array(
        "Januari", "Februari", "Maret",
        "April", "Mei", "Juni",
        "Juli", "Agustus", "September",
        "Oktober", "November", "Desember");
    $tahun = substr($date, 0, 4); // memisahkan format tahun menggunakan substring
    $bulan = substr($date, 5, 2); // memisahkan format bulan menggunakan substring
    $tgl = substr($date, 8, 2); // memisahkan format tanggal menggunakan substring
    $result = $tgl . " " . $BulanIndo[(int) $bulan - 1] . " " . $tahun;
    return($result);
}


function mkRange($start,$end) {
    $count = strToInt($end) - strToInt($start);
    $r = array();
    do {$r[] = $start++;} while ($count--);
    return $r;
}
 
function strToInt($str) {
    $str = strrev($str);
    $dec = 0;
    for ($i = 0; $i < strlen($str); $i++) {
        $dec += (base_convert($str[$i],36,10)-9) * pow(26,$i);
    }
    return $dec;
}