<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class reagenimunologi extends Model{
    protected $table = 'reagen_imunologi';

    protected $fillable = ['id_master_imunologi','metode','nama_reagen','nama_produsen','nomor_lot','tgl_kadaluarsa','sensitivitas','spesifisitas'];

    // protected $table = 'tb_cobaan';
    // protected $fillable = ['id_registrasi','siklus','kode_peserta','tgl_penerimaan','tgl_pemeriksaan','kualitas','kolesterol','trigliserida','jenis','catatan','penanggung_jawab','created_by'];
    public $timestamps = false;
}