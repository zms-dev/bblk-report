<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HpHeader extends Model
{
    protected $fillable = ['kode_peserta', 'tgl_pemeriksaan', 'tgl_penerimaan', 'catatan', 'kualitas_bahan', 'kategori','penanggung_jawab', 'type','kode_lab','nama_pemeriksa','nomor_hp','kode_bahan'];

    public $timestamps = false;

    public function detail()
    {
    	return $this->hasMany('App\HpDetail', 'id', 'hp_header_id');
    }
}
