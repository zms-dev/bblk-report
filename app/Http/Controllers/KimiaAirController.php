<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;

use App\Parameter;
use App\Ring;
use App\HpHeader;
use App\ZScore;
use App\HpDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\register as Register;
use Redirect;
use Validator;
use Session;

class KimiaAirController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {
        $data = Parameter::where('kategori', 'kimia air')->get();
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');

        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;
        
        return view('hasil_pemeriksaan/kimia-air', compact('data', 'perusahaan','kode', 'type', 'siklus','date'));
    }

    public function view(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'hp_details.tanggal_mulai', 'hp_details.tanggal_selesai', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan')
            ->where('parameter.kategori', 'kimia air')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
        $datas = HpHeader::where('id_registrasi', $id)
                ->where('type', $type)
                ->first();

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        
        // return view('cetak_hasil/kimia-klinik', compact('data', 'perusahaan', 'datas', 'type'));
        $pdf = PDF::loadview('cetak_hasil/kimia-air', compact('data', 'register', 'perusahaan','datas', 'type','siklus','date'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Kimia-Air.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;
        $type = $input['type'];

        $validasi = HpHeader::where(['id_registrasi'=>$id , 'siklus'=>$siklus , 'type'=>$type])->get();
        
        $rules = array(
            'kode_peserta' => 'required',
            'tanggal_penerimaan' => 'required',
            'tanggal_pemeriksaan' => 'required',
            'kualitas' => 'required'
        );

        $messages = array(
            'kode_peserta.required' => 'Kode Peserta Wajib diisi',
            'tanggal_penerimaan.required' => 'Tanggal Penerimaan wajib diisi',
            'tanggal_pemeriksaan.required' => 'Tanggal Pemeriksaan wajib diisi',
            'kualitas.required' => 'Kualitas wajib dipliih'
        );
        if (count($validasi)>0) {
            Session::flash('message', 'Hasil Kimia Air Sudah Ada!'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
    }else{
        $saveHeader = new HpHeader;
        $saveHeader->kode_lab = $input['kode_peserta'];
        $saveHeader->kode_peserta = $perusahaanID;
        $saveHeader->tgl_penerimaan = date('Y-m-d', strtotime($input['tanggal_penerimaan']));
        $saveHeader->kualitas_bahan = $input['kualitas'];
        $saveHeader->catatan = $input['catatan'];
        $saveHeader->nama_pemeriksa = $input['nama_pemeriksa'];
        $saveHeader->nomor_hp = $input['nomor_hp'];
        $saveHeader->kategori = 'kimia air';
        $saveHeader->id_registrasi = $id;
        $saveHeader->penanggung_jawab = $input['penanggung_jawab'];
        $saveHeader->type = $input['type'];
        $saveHeader->siklus = $siklus;
        $saveHeader->save();

        $saveHeaderID = $saveHeader->id;

        $i = 0;
        foreach ($input['alat'] as $alat) {
            if($alat != ''){
                $saveDetail = new HpDetail;
                $saveDetail->parameter_id = $input['parameter_id'][$i];
                $saveDetail->tanggal_mulai = $input['tanggal_mulai'][$i];
                $saveDetail->tanggal_selesai = $input['tanggal_selesai'][$i];
                $saveDetail->hp_header_id = $saveHeaderID;
                $saveDetail->alat = $alat;
                $saveDetail->kode_metode_pemeriksaan = $input['kode'][$i];
                $saveDetail->hasil_pemeriksaan = $input['hasil'][$i];
                $saveDetail->metode_lain = $input['metode_lain'][$i];
                $saveDetail->save();
            }
            $i++;
        }
       
        $date_sekarang = date('Y-m-d H:i:s');
        DB::table('tanggal_input')->insert(['id_register' => $id, 'siklus' => $siklus, 'input_date' => $date_sekarang]);
        if ($siklus == '1') {
            Register::where('id',$id)->update(['siklus_1'=>'done', 'rpr1'=>'done', 'status_data1'=>'1']);
            Register::where('id',$id)->update(['pemeriksaan'=>'done']);
        }else{
            Register::where('id',$id)->update(['siklus_2'=>'done', 'rpr2'=>'done', 'status_data2'=>'1']);
            Register::where('id',$id)->update(['pemeriksaan2'=>'done']);
        }
        return redirect('hasil-pemeriksaan');
         }
    }

    public function edit(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'hp_details.tanggal_mulai', 'hp_details.tanggal_selesai', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.id as IDDetails')
            ->where('parameter.kategori', 'kimia air')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
        $datas = HpHeader::where('id_registrasi', $id)
                ->where('type', $type)
                ->first();

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        
        return view('edit_hasil/kimia-air', compact('data', 'perusahaan', 'datas', 'type','siklus','date'));
    }
    public function update(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;

        // dd($input);
        $SaveHeader['tgl_penerimaan'] = $request->tanggal_penerimaan;
        $SaveHeader['kualitas_bahan'] = $request->kualitas;
        $SaveHeader['nama_pemeriksa'] = $request->nama_pemeriksa;
        $SaveHeader['nomor_hp'] = $request->nomor_hp;
        $SaveHeader['catatan'] = $request->catatan;
        $SaveHeader['penanggung_jawab'] = $request->penanggung_jawab;

        HpHeader::where('id_registrasi',$id)->where('siklus', $siklus)->update($SaveHeader);
        // dd($input);
        $i = 0;
        foreach ($input['alat'] as $alat) {
            if($alat != ''){
                $SaveDetail['tanggal_mulai'] = $request->tanggal_mulai[$i];
                $SaveDetail['tanggal_selesai'] = $request->tanggal_selesai[$i];
                $SaveDetail['alat'] = $request->alat[$i];
                $SaveDetail['kode_metode_pemeriksaan'] = $request->kode[$i];
                $SaveDetail['hasil_pemeriksaan'] = $request->hasil[$i];
                HpDetail::where('id', $request->id_detail[$i])->update($SaveDetail);
            }
            $i++;
        }
        if ($request->simpan == "Kirim" || $request->simpan == "Validasi") {
            if ($siklus == '1') {
                Register::where('id',$id)->update(['status_data1'=>'2']);
            }else{
                Register::where('id',$id)->update(['status_data2'=>'2']);
            }
            if ($request->simpan == "Validasi") {
                DB::table('validasi_input')->where('id_register', $id)->where('siklus', $siklus)->delete();
                DB::table('validasi_input')->insert(
                    ['id_register' => $id, 'siklus' => $siklus, 'status' => 'done']
                );
                return back();
            }else{
                return redirect('edit-hasil');
            }
        }else{
            return redirect('edit-hasil');
        }
    }


    public function evaluasi(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');

        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.tanggal_mulai', 'hp_details.tanggal_selesai', 'hp_details.alat', 'hp_details.kode_metode_pemeriksaan', 'hp_details.hasil_pemeriksaan')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('form', '=', 'kimia air')
                    ->get();
            $val->sd = $sd;
        }
        $zscore = DB::table('tb_zscore')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('id_registrasi', $id)
                    ->get();
         // dd($data);
        return view('evaluasi.kimiaair.zscore.index', compact('data', 'input', 'date', 'type','zscore','id', 'siklus'));
    }

    public function cetakevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');

        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.tanggal_mulai', 'hp_details.tanggal_selesai', 'hp_details.alat', 'hp_details.kode_metode_pemeriksaan', 'hp_details.hasil_pemeriksaan')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('form', '=', 'kimia air')
                    ->get();
            $val->sd = $sd;
        }
        $zscore = DB::table('tb_zscore')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('id_registrasi', $id)
                    ->get();
         // dd($data);
        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();
        $tanggal = Date('Y m d');
         // dd($data);
        if (count($zscore)>0) {
        $pdf = PDF::loadview('evaluasi.kimiaair.zscore.cetak', compact('data', 'input', 'date', 'type','zscore','id', 'siklus', 'tanggal','datas'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('Kimia Air.pdf');
        }else{
             Session::flash('message', 'KAI belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }

    public function insertevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $input = $request->all();

        $i = 0;
        foreach ($input['parameter'] as $alat) {
            $Save = new ZScore;
            $Save->id_registrasi = $id;
            $Save->parameter = $input['parameter'][$i];
            $Save->zscore = $input['zscore'][$i];
            $Save->siklus = $siklus;
            $Save->tahun = $date;
            $Save->form = 'kimia air';
            $Save->save();
            $i++;
        }
        return redirect('evaluasi/hitung-zscore/kimiaair');
    }

    public function kimringedit(\Illuminate\Http\Request $request, $id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air')->get();
        $sd = DB::table('tb_ring')
            ->join('parameter','parameter.id','=','tb_ring.parameter')
            ->where('form', '=', 'kimia air')
            ->orderBy('tb_ring.id', 'desc')
            ->select('tb_ring.*', 'parameter.nama_parameter')
            ->where('tb_ring.id', $id)
            ->first();
        return view('evaluasi.kimiaair.ring.view', compact('sd', 'parameter'));
    }

    public function kimringeditna(\Illuminate\Http\Request $request, $id)
    {
        $Update['parameter'] = $request->parameter;
        $Update['siklus'] = $request->siklus;
        $Update['tahun'] = $request->tahun;
        $Update['ring1'] = $request->ring1;
        $Update['ring2'] = $request->ring2;
        $Update['ring3'] = $request->ring3;
        $Update['ring4'] = $request->ring4;
        $Update['ring5'] = $request->ring5;
        $Update['ring6'] = $request->ring6;

        Ring::where('id',$id)->update($Update);
        // dd($input);
        Session::flash('message', 'Edit Ring telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-ring/kimiaair');
    }

    public function kimringdelet($id){
        Session::flash('message', 'Hapus Ring telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        Ring::find($id)->delete();
        return redirect('evaluasi/input-ring/kimiaair');
    }

    public function kimring()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air')->get();
        $sd = DB::table('tb_ring')
            ->join('parameter','parameter.id','=','tb_ring.parameter')
            ->where('form', '=', 'kimia air')
            ->orderBy('tb_ring.id', 'desc')
            ->select('tb_ring.*', 'parameter.nama_parameter')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaair.ring.index', compact('parameter','sd'));
    }

    public function kimringna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $Savedata = new Ring;
        $Savedata->parameter = $input['parameter'];
        $Savedata->siklus = $input['siklus'];
        $Savedata->tahun = $input['tahun'];
        $Savedata->ring1 = $input['ring1'];
        $Savedata->ring2 = $input['ring2'];
        $Savedata->ring3 = $input['ring3'];
        $Savedata->ring4 = $input['ring4'];
        $Savedata->ring5 = $input['ring5'];
        $Savedata->ring6 = $input['ring6'];
        $Savedata->form = 'kimia air';
        $Savedata->save();

        Session::flash('message', 'Input SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-ring/kimiaair');
    }

}