<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Rujukanbta;
use App\Rujukanmalaria;
use App\Rujukanhem;
use App\Rujukantc;
use App\Rujukanurinalisa;
use App\Rujukanurinalisametode;
use App\Rujukanimunologi;
use Illuminate\Support\Facades\Redirect;

class RujukanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function indexbta()
    {
        $rujukan = DB::table('tb_rujukan_bta')->get();
        return view('back/rujukan/bta/index', compact('rujukan'));
    }
    public function editbta($id)
    {
        $data = DB::table('tb_rujukan_bta')->where('id', $id)->get();
        return view('back/rujukan/bta/update', compact('data'));
    }

    public function updatebta(\Illuminate\Http\Request $request, $id)
    {
        $data['kode_bahan_uji'] = $request->kode_bahan_uji;
        $data['rujukan'] = $request->rujukan;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        $data['kuman'] = $request->kuman;

        Rujukanbta::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan BTA Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-bta');
    }    

    public function insertbta(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Rujukanbta::create($data);
        // dd($data);
        Session::flash('message', 'Master Rujukan BTA Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-bta');
    }

    public function deletebta($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Rujukanbta::find($id)->delete();
        return redirect("admin/rujukan-bta");
    }


    public function indexmalaria()
    {
        $rujukan = DB::table('tb_rujukan_malaria')->get();
        return view('back/rujukan/malaria/index', compact('rujukan'));
    }
    public function editmalaria($id)
    {
        $data = DB::table('tb_rujukan_malaria')->where('id', $id)->get();
        
        return view('back/rujukan/malaria/update', compact('data'));
    }

    public function updatemalaria(\Illuminate\Http\Request $request, $id)
    {
                
        $stadium="";
        if (!empty($request['stadium'])) {
            $stadium = implode(",",$request['stadium']);     
        }
        // dd($stadium);

        $data['kode_bahan_uji'] = $request->kode_bahan_uji;
        $data['rujukan'] = $request->rujukan;
        $data['spesies'] = $request->spesies;
        $data['stadium'] = $stadium;
        $data['parasit'] = $request->parasit;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        Rujukanmalaria::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan Malaria Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-malaria');
    }    

    public function insertmalaria(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        // dd($data);
        $stadium = "";

        if (!empty($data['stadium'])) {
        $stadium = implode(",",$data['stadium']);     
        }

        // dd($stadium);
        $save = new Rujukanmalaria;
        $save->kode_bahan_uji = $data['kode_bahan_uji'];
        $save->rujukan = $data['rujukan'];
        $save->spesies = $data['spesies'];
        $save->stadium = $stadium;
        $save->parasit = $data['parasit'];
        $save->siklus = $data['siklus'];
        $save->tahun = $data['tahun'];
        $save->save();

        // dd($data);
        Session::flash('message', 'Master Rujukan Malaria Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-malaria');
    }

    public function deletemalaria($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Rujukanmalaria::find($id)->delete();
        return redirect("admin/rujukan-malaria");
    }


    public function indexhematologi()
    {
        $rujukan = DB::table('tb_rujukan_hematologi')->where('form', '=', 'hem')->get();
        return view('back/rujukan/hematologi/index', compact('rujukan'));
    }
    public function edithematologi($id)
    {
        $data = DB::table('tb_rujukan_hematologi')->where('id', $id)->get();
        return view('back/rujukan/hematologi/update', compact('data'));
    }

    public function updatehematologi(\Illuminate\Http\Request $request, $id)
    {
        $data['kode'] = $request->kode;
        $data['nilai'] = $request->nilai;
        $data['kriteria'] = $request->kriteria;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        Rujukanhem::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan Hematologi Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-hematologi');
    }    

    public function inserthematologi(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Rujukanhem::create($data);
        // dd($data);
        Session::flash('message', 'Master Rujukan Hematologi Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-hematologi');
    }

    public function deletehematologi($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Rujukanhem::find($id)->delete();
        return redirect("admin/rujukan-hematologi");
    }


    public function indexkimiaklinik()
    {
        $rujukan = DB::table('tb_rujukan_hematologi')->where('form', '=', 'kkl')->get();
        return view('back/rujukan/kimiaklinik/index', compact('rujukan'));
    }
    public function editkimiaklinik($id)
    {
        $data = DB::table('tb_rujukan_hematologi')->where('id', $id)->get();
        return view('back/rujukan/kimiaklinik/update', compact('data'));
    }

    public function updatekimiaklinik(\Illuminate\Http\Request $request, $id)
    {
        $data['kode'] = $request->kode;
        $data['nilai'] = $request->nilai;
        $data['kriteria'] = $request->kriteria;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        Rujukanhem::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan Kimiaklinik Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-kimiaklinik');
    }    

    public function insertkimiaklinik(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Rujukanhem::create($data);
        // dd($data);
        Session::flash('message', 'Master Rujukan Kimiaklinik Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-kimiaklinik');
    }

    public function deletekimiaklinik($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Rujukanhem::find($id)->delete();
        return redirect("admin/rujukan-kimiaklinik");
    }


    public function indexkimiakesehatan()
    {
        $rujukan = DB::table('tb_rujukan_hematologi')->where('form', '=', 'kimkes')->get();
        return view('back/rujukan/kimiakesehatan/index', compact('rujukan'));
    }
    public function editkimiakesehatan($id)
    {
        $data = DB::table('tb_rujukan_hematologi')->where('id', $id)->get();
        return view('back/rujukan/kimiakesehatan/update', compact('data'));
    }

    public function updatekimiakesehatan(\Illuminate\Http\Request $request, $id)
    {
        $data['kode'] = $request->kode;
        $data['nilai'] = $request->nilai;
        $data['hasil'] = $request->hasil;
        $data['kriteria'] = $request->kriteria;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        Rujukanhem::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan Kimiakesehatan Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-kimiakesehatan');
    }    

    public function insertkimiakesehatan(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Rujukanhem::create($data);
        // dd($data);
        Session::flash('message', 'Master Rujukan Kimiakesehatan Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-kimiakesehatan');
    }

    public function deletekimiakesehatan($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Rujukanhem::find($id)->delete();
        return redirect("admin/rujukan-kimiakesehatan");
    }


    public function indextc()
    {
        $rujukan = DB::table('tb_rujukan_tc')->where('form', '=', 'tc')->get();
        return view('back/rujukan/tc/index', compact('rujukan'));
    }
    public function edittc($id)
    {
        $data = DB::table('tb_rujukan_tc')->where('id', $id)->get();
        return view('back/rujukan/tc/update', compact('data'));
    }

    public function updatetc(\Illuminate\Http\Request $request, $id)
    {
        $data['kode_sediaan'] = $request->kode_sediaan;
        $data['rujukan'] = $request->rujukan;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        Rujukantc::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan TC Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-tc');
    }    

    public function inserttc(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Rujukantc::create($data);
        // dd($data);
        Session::flash('message', 'Master Rujukan TC Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-tc');
    }

    public function deletetc($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Rujukantc::find($id)->delete();
        return redirect("admin/rujukan-tc");
    }


    public function indeximunologi()
    {
        $data = ["6" => "Anti HIV", "7" => "Anti TP", "8" => "HBsAg", "9" => "Anti HCV"];
        $rujukan = DB::table('tb_rujukan_imunologi')
                    ->where('parameter', $data[Auth::user()->penyelenggara])
                    ->groupBy('parameter', 'siklus', 'tahun')
                    ->get();
        // dd($rujukan);
        return view('back/rujukan/imunologi/index', compact('rujukan'));
    }
    public function editimunologi($id)
    {
        $rujukan = DB::table('tb_rujukan_imunologi')->where('id', $id)->first();
        // dd($rujukan);
        $data = DB::table('tb_rujukan_imunologi')
                ->where('parameter', $rujukan->parameter)
                ->where('siklus', $rujukan->siklus)
                ->where('tahun', $rujukan->tahun)
                ->get();
        return view('back/rujukan/imunologi/update', compact('data', 'rujukan'));
    }

    public function updateimunologi(\Illuminate\Http\Request $request, $id)
    {
        // dd($request->all());
        $input = request::all();
        $parameter = $request->parameter;
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');

        for ($i=0; $i < count($input['kode_sediaan']) ; $i++) { 
        // dd($data);
            // dd($request->id[$i]);
            $data['kode_bahan_uji'] = $request->kode_sediaan[$i];
            $data['nilai_rujukan'] = $request->rujukan[$i];
            $data['siklus'] = $request->siklus;
            $data['tahun'] = $request->tahun;
            Rujukanimunologi::where('id','=', $request->id_rujukan[$i])->update($data);
        }
        Session::flash('message', 'Master Rujukan Imunologi Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-imunologi');
    }    

    public function insertimunologi(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        $parameter = DB::table('tb_rujukan_imunologi')
                    ->where('parameter', $request->parameter)
                    ->where('siklus', $request->siklus)
                    ->where('tahun', $request->tahun)
                    ->get();
        if (count($parameter)) {
            Session::flash('message', 'Master Rujukan Imunologi Telah Ada!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('admin/rujukan-imunologi');
        }else{
            for ($i=0; $i < count($data['kode_bahan_uji']) ; $i++) { 
                $save = new Rujukanimunologi;
                $save->kode_bahan_uji = $request->kode_bahan_uji[$i];
                $save->nilai_rujukan =$request->nilai_rujukan[$i];
                $save->siklus = $request->siklus;
                $save->parameter = $request->parameter;
                $save->tahun = $request->tahun;
                $save->save();
            }
        }
        // dd($data);
        Session::flash('message', 'Master Rujukan Imunologi Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/rujukan-imunologi');
    }

    public function deleteimunologi($id){
        $rujukan = DB::table('tb_rujukan_imunologi')->where('id', $id)->first();
        // dd($rujukan);
        Rujukanimunologi::where('parameter','=', $rujukan->parameter)
            ->where('siklus','=', $rujukan->siklus)
            ->where('tahun','=', $rujukan->tahun)->delete();
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning');
        return redirect("admin/rujukan-imunologi");
    }


    public function indexurinalisa()
    {
        $rujukan = DB::table('tb_rujukan_urinalisa')
                    ->join('parameter','tb_rujukan_urinalisa.parameter','=','parameter.id')
                    ->select('tb_rujukan_urinalisa.*','parameter.nama_parameter')
                    ->groupBy('siklus','tahun','type')
                    ->paginate(11);
        return view('back/rujukan/urinalisa/index', compact('rujukan'));
    }

    public function indexurinalisametode()
    {
        $rujukan = DB::table('tb_urinalisa_metode')
                    ->join('parameter','tb_urinalisa_metode.parameter','=','parameter.id')
                    ->select('tb_urinalisa_metode.*','parameter.nama_parameter')
                    ->groupBy('siklus','tahun','type')
                    ->paginate(11);
        return view('back/rujukan/urinalisa/indexmetode', compact('rujukan'));
    }
    
    public function inserturinalisa()
    {
        $parameter = DB::table('parameter')
                    ->where('kategori','=','urinalisa')
                    // ->where('id','!=','9')
                    // ->where('id','!=','10')
                    ->get();
        return view('back/rujukan/urinalisa/insert', compact('parameter'));
    }

    public function insertmetode()
    {
        $parameter = DB::table('parameter')
                    ->join('metode_pemeriksaan','metode_pemeriksaan.parameter_id','=','parameter.id')
                    ->where('parameter.kategori','=','urinalisa')
                    ->orderBy('parameter_id','ASC')
                    ->get();
        // dd($parameter);
        return view('back/rujukan/urinalisa/insertmetode', compact('parameter'));
    }

    public function inserturinalisa2()
    {
        $parameter = DB::table('parameter')
                    ->where('kategori','=','urinalisa')
                    ->get();
        return view('back/rujukan/urinalisa/bj-ph', compact('parameter'));
    }

    public function editurinalisa($id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','urinalisa')->get();
        $rujukan = DB::table('tb_rujukan_urinalisa')
                ->join('parameter','tb_rujukan_urinalisa.parameter','=','parameter.id')
                ->where('tb_rujukan_urinalisa.id', $id)
                ->select('tb_rujukan_urinalisa.*','parameter.nama_parameter')
                ->first();

        $data = DB::table('tb_rujukan_urinalisa')
                ->join('parameter','parameter.id','=','tb_rujukan_urinalisa.parameter')
                ->where('tb_rujukan_urinalisa.type','=', $rujukan->type)
                ->where('tb_rujukan_urinalisa.tahun','=', $rujukan->tahun)
                ->where('tb_rujukan_urinalisa.siklus','=', $rujukan->siklus)
                ->select('tb_rujukan_urinalisa.*','parameter.nama_parameter')
                ->get();
        return view('back/rujukan/urinalisa/update', compact('rujukan','parameter','data'));
    }

     public function editurinalisametode($id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','urinalisa')->get();
        $rujukan = DB::table('tb_urinalisa_metode')
                ->join('parameter','tb_urinalisa_metode.parameter','=','parameter.id')
                ->where('tb_urinalisa_metode.id', $id)
                ->select('tb_urinalisa_metode.*','parameter.nama_parameter')
                ->first();

        $data = DB::table('tb_urinalisa_metode')
                ->join('parameter','parameter.id','=','tb_urinalisa_metode.parameter')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_urinalisa_metode.metode')
                ->where('tb_urinalisa_metode.type','=', $rujukan->type)
                ->where('tb_urinalisa_metode.tahun','=', $rujukan->tahun)
                ->where('tb_urinalisa_metode.siklus','=', $rujukan->siklus)
                ->select('tb_urinalisa_metode.*','parameter.nama_parameter','metode_pemeriksaan.metode_pemeriksaan')
                ->get();

        return view('back/rujukan/urinalisa/updatemetode', compact('rujukan','parameter','data'));
    }

    public function updateurinalisa(\Illuminate\Http\Request $request, $id)
    {
         $i = 0;
            foreach ($request->parameter as $parameter) {
            $data['parameter'] = $request->parameter[$i];
            $data['siklus'] = $request->siklus;
            $data['tahun'] = $request->tahun;
            $data['batas_bawah'] = $request->batas_bawah[$i];
            $data['batas_atas'] = $request->batas_atas[$i];
            $data['bahan'] = $request->bahan;
            $data['type'] = $request->type;
            Rujukanurinalisa::where('id',$request->id[$i])->update($data);
            $i++;
        }

        Session::flash('message', 'Master Rujukan Urinalisa Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-urinalisa');
    }  

    public function updateurinalisametode(\Illuminate\Http\Request $request, $id)
    {
         $i = 0;
            foreach ($request->parameter as $parameter) {
            $data['parameter'] = $request->parameter[$i];
            $data['metode'] = $request->metode[$i];
            $data['siklus'] = $request->siklus;
            $data['tahun'] = $request->tahun;
            $data['batas_bawah'] = $request->batas_bawah[$i];
            $data['batas_atas'] = $request->batas_atas[$i];
            $data['bahan'] = $request->bahan;
            $data['type'] = $request->type;
            Rujukanurinalisametode::where('id',$request->id[$i])->update($data);
            $i++;
        }

        Session::flash('message', 'Master Rujukan Urinalisa Metode Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/rujukan-urinalisa-metode');
    }  

    public function inserturinalisana2(\Illuminate\Http\Request $request)
    {
        $data = Request::all();

        $urin = DB::table('tb_rujukan_urinalisa')
                ->where('parameter', $data['parameter'])
                ->where('siklus', $data['siklus'])
                ->where('bahan', $data['bahan'])
                ->where('tahun', $data['tahun'])
                ->count();
        // dd($urin);
        if ($urin > 0 ) {
            Session::flash('message', 'Master Rujukan Urinalisa Gagal Ditambahkan!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('admin/rujukan-urinalisa');
        }else{
            if($data['bahan'] == 'Negatif'){
                $bahan = 'a';
            }else{
                $bahan = 'b';
            }
            $ngitung = DB::table('hp_details')
                        ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                        ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                        ->where('hp_headers.siklus', '=', $data['siklus'])
                        ->where('hp_headers.type', '=', $bahan)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $data['tahun'])
                        ->where('hp_details.parameter_id','=',$data['parameter'])
                        ->avg('hp_details.hasil_pemeriksaan');
            // dd($bahan);
            $data['rujukan'] = $ngitung;
            Rujukanurinalisa::create($data);
            Session::flash('message', 'Master Rujukan Urinalisa Berhasil Ditambahkan!'); 
            Session::flash('alert-class', 'alert-success'); 
            return redirect('admin/rujukan-urinalisa');
        }
    }  

    public function inserturinalisana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = count($request['parameter']);
        // dd($data);
        $urin = DB::table('tb_rujukan_urinalisa')
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tahun', $input['tahun'])
                ->where('bahan', $input['bahan'])
                ->count();
        if ($urin > 0 ) {
            Session::flash('message', 'Master Rujukan Urinalisa Gagal Ditambahkan!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('admin/rujukan-urinalisa');
        }else{
            $i = 0;
            foreach ($input['parameter'] as $parameter) {
                $saveDetail = new Rujukanurinalisa;
                $saveDetail->parameter = $input['parameter'][$i];
                $saveDetail->batas_bawah = $input['batas_bawah'][$i];
                $saveDetail->batas_atas = $input['batas_atas'][$i];
                $saveDetail->siklus = $input['siklus'];
                $saveDetail->type = $input['type'];
                $saveDetail->tahun = $input['tahun'];
                $saveDetail->bahan = $input['bahan'];
                $saveDetail->save();
                $i++;
            }
            // dd($data);
            // dd($data);
            Session::flash('message', 'Master Rujukan Urinalisa Berhasil Ditambahkan!'); 
            Session::flash('alert-class', 'alert-success'); 
            return redirect('admin/rujukan-urinalisa');
        }
    }

    public function inserturinalisanametode(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = count($request['parameter']);
        // dd($data);
        $urin = DB::table('tb_urinalisa_metode')
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tahun', $input['tahun'])
                ->where('bahan', $input['bahan'])
                ->count();
        if ($urin > 0 ) {
            Session::flash('message', 'Master Rujukan Urinalisa Gagal Ditambahkan!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('admin/rujukan-urinalisa');
        }else{
             $i = 0;
            foreach ($input['parameter'] as $parameter) {
                $saveDetail = new Rujukanurinalisametode;
                $saveDetail->parameter = $input['parameter'][$i];
                $saveDetail->metode = $input['metode'][$i];
                $saveDetail->batas_bawah = $input['batas_bawah'][$i];
                $saveDetail->batas_atas = $input['batas_atas'][$i];
                $saveDetail->siklus = $input['siklus'];
                $saveDetail->type = $input['type'];
                $saveDetail->tahun = $input['tahun'];
                $saveDetail->bahan = $input['bahan'];
                $saveDetail->save();
                $i++;
            }
            // dd($data);
            // dd($data);
            Session::flash('message', 'Master Rujukan Urinalisa Metode Berhasil Ditambahkan!'); 
            Session::flash('alert-class', 'alert-success'); 
            return redirect('admin/rujukan-urinalisa-metode');
        }
    }

    public function deleteurinalisa($id){
        $rujukan = DB::table('tb_rujukan_urinalisa')
                ->join('parameter','tb_rujukan_urinalisa.parameter','=','parameter.id')
                ->where('tb_rujukan_urinalisa.id', $id)
                ->select('tb_rujukan_urinalisa.*','parameter.nama_parameter')
                ->first();
        Rujukanurinalisa::where('siklus','=', $rujukan->siklus)->where('tahun','=', $rujukan->tahun)->where('type','=', $rujukan->type)->delete();
                
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        return redirect("admin/rujukan-urinalisa");
    }

    public function deleteurinalisametode($id){
        $rujukan = DB::table('tb_urinalisa_metode')
                ->join('parameter','tb_urinalisa_metode.parameter','=','parameter.id')
                ->where('tb_urinalisa_metode.id', $id)
                ->select('tb_urinalisa_metode.*','parameter.nama_parameter')
                ->first();
        Rujukanurinalisametode::where('siklus','=', $rujukan->siklus)->where('tahun','=', $rujukan->tahun)->where('type','=', $rujukan->type)->delete();
                
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        return redirect("admin/rujukan-urinalisa-metode");
    }
}