<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use PDF;
use App\Daftar;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Rujukanbakteri;
use App\EvaluasiBakteri;
use App\Statusbakteri;
use App\Antibiotik;
use App\register as Register;
use Illuminate\Support\Facades\Redirect;

class EvaluasiBakteriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function indexbakteri(){
        $rujukan = Rujukanbakteri::orderBy('id','ASC')->paginate(11);
        return view('evaluasi/bakteri/rujukan/index', compact('rujukan'));
    }

    public function insertBakteri()
    {
        return view('evaluasi/bakteri/rujukan/insert', compact('parameter'));
    }

    public function insertBakteri2(\Illuminate\Http\Request $request)
    {
        $data = Request::all();

        $fail = Rujukanbakteri::where('tahun','=',$request['tahun'])
                ->where('siklus','=',$request['siklus'])
                ->where('lembar','=',$request['lembar'])
                ->where('jenis_pemeriksaan','=',$request['jenis_pemeriksaan'])->count();

        if ($fail == 0) {
            Rujukanbakteri::create($data);
            Session::flash('message', 'Master Rujukan Bakteri Berhasil Disimpan!');
            Session::flash('alert-class', 'alert-success');
            return redirect('evaluasi/input-rujukan/bakteri');
        }else{
            Session::flash('message', 'Master Rujukan Bakteri Sudah Ada!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('evaluasi/input-rujukan/bakteri/insert');
        }
    }
    public function editBakteri($id)
    {
        $data = DB::table('tb_rujukan')->where('tb_rujukan.id', $id)->get();
        return view('evaluasi/bakteri/rujukan/update', compact('data'));
    }

    public function deleteBakteri($id){
        Session::flash('message', 'Data Telah Dihapus!');
        Session::flash('alert-class', 'alert-warning');
        Rujukanbakteri::find($id)->delete();
        return redirect("evaluasi/input-rujukan/bakteri");
    }
     public function updateBakteri(\Illuminate\Http\Request $request, $id)
    {
        $data['lembar'] = $request->lembar;
        $data['siklus'] = $request->siklus;
        $data['tahun'] = $request->tahun;
        $data['rujukan'] = $request->rujukan;
        $data['jenis_pemeriksaan'] = $request->jenis_pemeriksaan;
        Rujukanbakteri::where('id',$id)->update($data);
        Session::flash('message', 'Master Rujukan Bakteri Berhasil Diubah!');
        Session::flash('alert-class', 'alert-success');

        return redirect('evaluasi/input-rujukan/bakteri');
    }

    public function perusahaanbakteri(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','13')->get();
            return Datatables::of($datas)
                    // dd($datas);

            ->addColumn('action', function($data){
                    return "".'<a href="penilaian/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.bakteri.penilaian.perusahaan');
    }

    public function perusahaanbakterisertifikat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','13')->get();
            return Datatables::of($datas)
                    // dd($datas);

            ->addColumn('action', function($data){
                    return "".'<a href="sertifikat/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.bakteri.penilaian.perusahaansertifikat');
    }
    public function dataperusahaanbakteri1sertifikat($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '13')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        return view('evaluasi.bakteri.penilaian.dataperusahaansertifikat', compact('data'));
    }

    public function perusahaanbakteri1(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes
'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','13')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="penilaian_uji_kepekaan_antibiotik/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.bakteri.penilaian.perusahaanuji');
    }

    public function dataperusahaanbakteri($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '13')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        return view('evaluasi.bakteri.penilaian.dataperusahaan', compact('data'));
    }
    public function dataperusahaanbakteri1($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '13')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        return view('evaluasi.bakteri.penilaian.dataperusahaanuji', compact('data'));
    }

    public function evaluasi(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->y;
        $years = date('Y');
        $register = Register::find($id);
        $evaluasi = DB::table('tb_evaluasi_bakteri')
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(tb_evaluasi_bakteri.created_at)'), '=' , $years)
                ->where('tb_evaluasi_bakteri.jenis_pemeriksa','=','Identifikasi')
                ->where('tb_evaluasi_bakteri.id_registrasi','=',$id)
                ->get();

        $identifikasi = DB::table("tb_evaluasi_bakteri")
                ->select(DB::raw("SUM(skor) as skor"))
                ->where('jenis_pemeriksa','Identifikasi')
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(tb_evaluasi_bakteri.created_at)'), '=' , $years)
                ->get();
        
        $status = Statusbakteri::where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.siklus','=', $siklus)
            ->where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.jenis_pemeriksa','=','Identifikasi')
            ->where('tb_status_bakteri.id_registrasi', $id)->first();
        // dd($status);

        $data = DB::table('data_antibiotik')
            ->leftjoin('tb_rujukan','tb_rujukan.lembar','=', 'data_antibiotik.lembar')
            ->select('data_antibiotik.id','data_antibiotik.id_registrasi','data_antibiotik.kode_lab','data_antibiotik.lembar','data_antibiotik.lembar as lembar_rujuk','data_antibiotik.spesies_auto','data_antibiotik.spesies_kultur','tb_rujukan.rujukan','tb_rujukan.jenis_pemeriksaan','tb_rujukan.siklus')
            ->where('tb_rujukan.siklus','=', $siklus)
            ->where('tb_rujukan.tahun','=', $years)
            ->where('tb_rujukan.jenis_pemeriksaan','=', 'Identifikasi')
            ->where('data_antibiotik.siklus','=', $siklus)
            ->where('data_antibiotik.id_registrasi', $id)
            ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $years)
            ->orderBy('data_antibiotik.lembar','asc')
            ->get();
        // dd($data);
        // dd($data);
        return view('evaluasi.bakteri.penilaian.evaluasi.index', compact('data','status','identifikasi','evaluasi','valid','rujuk','ujiKepekaan','id','siklus'));
    }

    public function kepekaan(\Illuminate\Http\Request $request, $id){
        $siklus = $request->y;
        $years = date('Y');
        $register = Register::find($id);
        $evaluasi = DB::table('tb_evaluasi_bakteri')
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(tb_evaluasi_bakteri.created_at)'), '=' , $years)
                ->where('tb_evaluasi_bakteri.jenis_pemeriksa','=','Uji Kepekaan Antibiotik')
                ->where('tb_evaluasi_bakteri.id_registrasi','=',$id)
                ->get();
        $ujiKepekaan = DB::table("tb_evaluasi_bakteri")
                ->select(DB::raw("SUM(skor) as skor"))
                ->where('siklus', $siklus)
                ->where('jenis_pemeriksa','Uji Kepekaan Antibiotik')
                ->get();

        $status =statusbakteri::where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.siklus','=', $siklus)
            ->where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.jenis_pemeriksa','=','Uji Kepekaan Antibiotik')
            ->where('tb_status_bakteri.id_registrasi', $id)->first();

        $data = DB::table('data_antibiotik')
            ->leftjoin('tb_rujukan','tb_rujukan.lembar','=', 'data_antibiotik.lembar')
            ->select('data_antibiotik.id','data_antibiotik.id_registrasi','data_antibiotik.kode_lab','data_antibiotik.lembar','data_antibiotik.lembar as lembar_rujuk','data_antibiotik.spesies_auto','data_antibiotik.spesies_kultur','tb_rujukan.rujukan','tb_rujukan.jenis_pemeriksaan','tb_rujukan.siklus')
            ->where('tb_rujukan.siklus','=', $siklus)
            ->where('tb_rujukan.tahun','=', $years)
            ->where('tb_rujukan.jenis_pemeriksaan','=', 'Uji Kepekaan Antibiotik')
            ->where('data_antibiotik.siklus','=', $siklus)
            ->where('data_antibiotik.id_registrasi', $id)
            ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $years)
            ->orderBy('data_antibiotik.lembar','asc')
            ->get();
        // dd($data);
        // dd($data);

        return view('evaluasi.bakteri.penilaian.evaluasi.kepekaan', compact('data','status','identifikasi','evaluasi','valid','rujuk','ujiKepekaan','id','siklus'));
    }
    public function evaluasiInsert(\Illuminate\Http\Request $request, $id)
    {
        $data = $request->all();
        $evaluasi = DB::table('tb_evaluasi_bakteri')->get();
        $input = $request->all();
        $siklus = $request->get('y');
        $years = date('Y');

        if ($data['simpan']== "Simpan") {
            $b = new Statusbakteri;
            $b->id_registrasi = $id;
            $b->siklus = $siklus;
            $b->tahun = $years;
            $b->keterangan = $request->keterangan;
            $b->jenis_pemeriksa = 'Identifikasi';
            $b->save();

            for ($i=0; $i < count($data['skor']); $i++) {

                $a = new EvaluasiBakteri;
                $a->id_data_antibiotik = $request->id_data_antibiotik[$i];
                $a->kode_soal = $request->kode_soal[$i];
                $a->jenis_pemeriksa = $request->jenis_pemeriksaan[$i];
                $a->nilai_rujukan = $request->rujukan[$i];
                $a->jawaban_peserta = $request->jawaban_peserta1[$i];
                $a->jawaban_peserta = $request->jawaban_peserta2[$i];
                $a->skor = $request->skor[$i];
                $a->siklus = $siklus;
                $a->id_registrasi = $id;
                $a->save();
                Session::flash('message', 'Master Rujukan Bakteri Berhasil Disimpan!');
                Session::flash('alert-class', 'alert-danger');
            }
        }
        // dd($data);
        return back();
    }

    public function evaluasiPrint(\Illuminate\Http\Request $request, $id){
        $input = $request->all();       
        $siklus = $request->y;
        $years = date('Y');
        $tanggal = date('Y m d');
        $register = Register::find($id);
        $evaluasi = DB::table('tb_evaluasi_bakteri')
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(tb_evaluasi_bakteri.created_at)'), '=' , $years)
                ->where('tb_evaluasi_bakteri.jenis_pemeriksa','=','Identifikasi')
                ->where('tb_evaluasi_bakteri.id_registrasi','=',$id)
                ->get();
        // dd($evaluasi);


        $status = Statusbakteri::where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.siklus','=', $siklus)
            ->where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.jenis_pemeriksa','=','Identifikasi')
            ->where('tb_status_bakteri.id_registrasi', $id)->first();
        // dd($status);

        $data = DB::table('data_antibiotik')
            ->leftjoin('tb_rujukan','tb_rujukan.lembar','=', 'data_antibiotik.lembar')
            ->select('data_antibiotik.id','data_antibiotik.id_registrasi','data_antibiotik.kode_lab','data_antibiotik.lembar','data_antibiotik.lembar as lembar_rujuk','data_antibiotik.spesies_auto','data_antibiotik.spesies_kultur','tb_rujukan.rujukan','tb_rujukan.jenis_pemeriksaan','tb_rujukan.siklus')
            ->where('tb_rujukan.siklus','=', $siklus)
            ->where('tb_rujukan.tahun','=', $years)
            ->where('tb_rujukan.jenis_pemeriksaan','=', 'Identifikasi')
            ->where('data_antibiotik.siklus','=', $siklus)
            ->where('data_antibiotik.id_registrasi', $id)
            ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $years)
            ->orderBy('data_antibiotik.lembar','asc')
            ->get();
        // dd($data);
        // dd($data);

        if ($input['simpan'] == 'Print') {
            if ((count($evaluasi)AND count($status)) > 0) {
            $pdf = PDF::loadview('evaluasi/bakteri/penilaian/evaluasi/print', compact('data','status','identifikasi','evaluasi','valid','rujuk','ujiKepekaan','id','siklus','tanggal','years'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri Identifikasi.pdf');
            }else{
            session::flash('message', 'Bakteri belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
            }
        }else if($input['simpan'] == 'Update'){
            $status = Statusbakteri::where("tb_status_bakteri.id_registrasi", $id)
            ->where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.siklus','=', $siklus)
            ->where('tb_status_bakteri.jenis_pemeriksa','=', 'Identifikasi')
            ->update(["keterangan" => $request->keterangan]);
            // dd($status);
            for ($i=0; $i < count($input['skor']); $i++) {
                EvaluasiBakteri::where("id", $request->id[$i])->update(["skor" => $request->skor[$i]]);
                Session::flash('message', 'Master Rujukan Bakteri Berhasil Disimpan!');
                Session::flash('alert-class', 'alert-danger');
            }
            return back();
        }
    }


    public function evaluasiInsertUji(\Illuminate\Http\Request $request, $id)
    {
        $data = $request->all();
        $evaluasi = DB::table('tb_evaluasi_bakteri')->get();
        $input = $request->all();
        $siklus = $request->get('y');
        $years = date('Y');

        if ($data['simpan']== "Simpan") {
            $b = new Statusbakteri;
            $b->id_registrasi = $id;
            $b->siklus = $siklus;
            $b->tahun = $years;
            $b->keterangan = $request->keterangan;
            $b->jenis_pemeriksa = 'Uji Kepekaan Antibiotik';
            $b->save();

            for ($i=0; $i < count($data['skor']); $i++) {

                $a = new EvaluasiBakteri;
                $a->id_data_antibiotik = $request->id_data_antibiotik[$i];
                $a->kode_soal = $request->kode_soal[$i];
                $a->jenis_pemeriksa = $request->jenis_pemeriksaan[$i];
                $a->nilai_rujukan = $request->rujukan[$i];
                $a->jawaban_peserta = $request->jawaban_peserta1[$i];
                $a->jawaban_peserta = $request->jawaban_peserta2[$i];
                $a->skor = $request->skor[$i];
                $a->siklus = $siklus;
                $a->id_registrasi = $id;
                $a->save();
                Session::flash('message', 'Master Rujukan Bakteri Berhasil Disimpan!');
                Session::flash('alert-class', 'alert-danger');
            }
        }
        // dd($data);
        return back();
    }

    public function evaluasiujiPrint(\Illuminate\Http\Request $request, $id){
        $input = $request->all();       
        // dd($input);
        $siklus = $request->y;
        $years = date('Y');
        $tanggal = date('Y m d');
        $register = Register::find($id);
        $evaluasi = DB::table('tb_evaluasi_bakteri')
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(tb_evaluasi_bakteri.created_at)'), '=' , $years)
                ->where('tb_evaluasi_bakteri.jenis_pemeriksa','=','Uji Kepekaan Antibiotik')
                ->where('tb_evaluasi_bakteri.id_registrasi','=',$id)
                ->get();


        $status = Statusbakteri::where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.siklus','=', $siklus)
            ->where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.jenis_pemeriksa','=','Uji Kepekaan Antibiotik')
            ->where('tb_status_bakteri.id_registrasi', $id)->first();
        // dd($status);

        $data = DB::table('data_antibiotik')
            ->leftjoin('tb_rujukan','tb_rujukan.lembar','=', 'data_antibiotik.lembar')
            ->select('data_antibiotik.id','data_antibiotik.id_registrasi','data_antibiotik.kode_lab','data_antibiotik.lembar','data_antibiotik.lembar as lembar_rujuk','data_antibiotik.spesies_auto','data_antibiotik.spesies_kultur','tb_rujukan.rujukan','tb_rujukan.jenis_pemeriksaan','tb_rujukan.siklus')
            ->where('tb_rujukan.siklus','=', $siklus)
            ->where('tb_rujukan.tahun','=', $years)
            ->where('tb_rujukan.jenis_pemeriksaan','=', 'Uji Kepekaan Antibiotik')
            ->where('data_antibiotik.siklus','=', $siklus)
            ->where('data_antibiotik.id_registrasi', $id)
            ->where(DB::raw('YEAR(data_antibiotik.created_at)'), '=' , $years)
            ->orderBy('data_antibiotik.lembar','asc')
            ->get();
        // dd($data);
        // dd($data);

        if ($input['simpan'] == 'Print') {
            if ((count($status)AND count($evaluasi))>0) {
            $pdf = PDF::loadview('evaluasi/bakteri/penilaian/evaluasi/printUji', compact('data','status','identifikasi','evaluasi','valid','rujuk','ujiKepekaan','id','siklus','tanggal','years'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri Uji Kepekaan Antibiotik.pdf');
            }else{
            Session::flash('message', 'Bakteri belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
            }
        }else if($input['simpan'] == 'Update'){
            $status = Statusbakteri::where("tb_status_bakteri.id_registrasi", $id)
            ->where('tb_status_bakteri.tahun','=', $years)
            ->where('tb_status_bakteri.siklus','=', $siklus)
            ->where('tb_status_bakteri.jenis_pemeriksa','=', 'Uji Kepekaan Antibiotik')
            ->update(["keterangan" => $request->keterangan]);
            // dd($status);
            for ($i=0; $i < count($input['skor']); $i++) {
                EvaluasiBakteri::where("id", $request->id[$i])->update(["skor" => $request->skor[$i]]);
                Session::flash('message', 'Master Rujukan Bakteri Berhasil Disimpan!');
                Session::flash('alert-class', 'alert-danger');
            }
            return back();
        }

    }

    
    public function grafikketeranganpeserta(){
        return view('evaluasi.bakteri.grafik.keterangan.index');
    }

     public function grafikketeranganpesertanya(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $lulus = DB::table('tb_status_bakteri')
                    ->where('tb_status_bakteri.tahun','=',$input['tahun'])
                    ->where('tb_status_bakteri.siklus','=',$input['siklus'])
                    ->where('tb_status_bakteri.jenis_pemeriksa','=',$input['jenis_pemeriksa'])
                    ->where('tb_status_bakteri.keterangan','=', 'Lulus')
                    ->select('tb_status_bakteri.*',DB::raw('count(keterangan) as keterangan'))
                    ->get();
        $tidak = DB::table('tb_status_bakteri')
                    ->where('tb_status_bakteri.tahun','=',$input['tahun'])
                    ->where('tb_status_bakteri.siklus','=',$input['siklus'])
                    ->where('tb_status_bakteri.jenis_pemeriksa','=',$input['jenis_pemeriksa'])
                    ->where('tb_status_bakteri.keterangan','=', 'Tidak Lulus')
                    ->select('tb_status_bakteri.*',DB::raw('count(keterangan) as keterangan'))
                    ->get();
        // var_dump($data);
        // dd($data);
        return view('evaluasi.bakteri.grafik.keterangan.grafik', compact('lulus','tidak', 'input'));
    }

    public function penilaian(\Illuminate\Http\Request $request){
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes
'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','13')->get();
            return Datatables::of($datas)
                    // dd($datas);

            ->addColumn('action', function($data){
                    return "".'<a href="antibiotik/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.bakteri.antibiotik.index');
    }



    public function penilaiandetailantibiotik($id)
    {
        $data =  DB::table('tb_registrasi')
        ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
        ->where('tb_registrasi.perusahaan_id', $id)
        ->where('sub_bidang.id', '=', '13')
        ->where('status', 3)
        ->where(function($query)
            {
                $query->where('status_data1', 2)
                    ->orwhere('status_data2', 2)
                    ->orwhere('status_datarpr1', 2)
                    ->orwhere('status_datarpr2', 2);
            })
        ->where(function($query)
            {
                $query->where('pemeriksaan2', '=' , 'done')
                        ->orwhere('pemeriksaan', '=' , 'done')
                        ->orwhere('siklus_1', '=' , 'done')
                        ->orwhere('siklus_2', '=' , 'done');
            })
        ->get();
        return view('evaluasi.bakteri.antibiotik.penilaiandetail', compact('data','data2'));
    }

    public function lembar(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        $years = date('Y');
        $lembar = $request->get('lembar');

        $ujiantibiotik = DB::table('tb_uji_kepekaan_antibiotik')
            ->where('lembar', $lembar)
            ->where(DB::raw('YEAR(tb_uji_kepekaan_antibiotik.created_at)'), '=' , $years)
            ->get();

        // dd($ujiantibiotik);

        if(empty($lembar)){
            $lembar = 1;
        }

        $data = DB::table('data_antibiotik')->where('id_registrasi', $id)->where('siklus', $siklus)->where('lembar', '=', $lembar)->get();
        // dd($data);
        foreach ($data as $key => $val) {
            $kepekaan = DB::table('tb_kepekaan')
                                ->leftjoin('antibiotik', 'tb_kepekaan.id_jenis_antibiotik', '=', 'antibiotik.id')
                                ->join('data_antibiotik', 'tb_kepekaan.id_antibiotik', '=', 'data_antibiotik.id')
                                ->select('antibiotik.*','antibiotik.id as id_anti','data_antibiotik.*','tb_kepekaan.*')
                                ->where('data_antibiotik.lembar',$lembar)
                                ->where('data_antibiotik.id', $val->id)
                                ->get();
            $val->kepekaan = $kepekaan;
        }
        // dd($data);
        // $peka = DB::table('data_peka_antibiotik')->where('id_registrasi', $id)->where('siklus', $siklus)->where('lembar', '=', $lembar)->first();
        // dd($data);

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;

        return view('evaluasi.bakteri.antibiotik.lembar', compact('id','ujiantibiotik','lembar','data','perusahaan','peka', 'type','siklus','date','val'));
    }

    public function cetakimonologi(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $tanggal = date('Y m d');
        $years = date('Y');
        $lembar = $request->get('lembar');

        $ujiantibiotik = DB::table('tb_uji_kepekaan_antibiotik')
            ->where('lembar', $lembar)
            ->where(DB::raw('YEAR(tb_uji_kepekaan_antibiotik.created_at)'), '=' , $years)
            ->get();

        // dd($ujiantibiotik);

        if(empty($lembar)){
            $lembar = 1;
        }

        $data = DB::table('data_antibiotik')->where('id_registrasi', $id)->where('siklus', $siklus)->where('lembar', '=', $lembar)->get();
        // dd($data);
        // foreach ($data as $key => $val) {
            $kepekaan = DB::table('tb_kepekaan')
                                ->leftjoin('antibiotik', 'tb_kepekaan.id_jenis_antibiotik', '=', 'antibiotik.id')
                                ->join('data_antibiotik', 'tb_kepekaan.id_antibiotik', '=', 'data_antibiotik.id')
                                ->select('antibiotik.*','antibiotik.id as id_anti','data_antibiotik.*','tb_kepekaan.*')
                                ->where('data_antibiotik.lembar',$lembar)
                                // ->where('data_antibiotik.id', $val->id)
                                ->get();
            // dd($kepekaan);
        //     $val->kepekaan = $kepekaan;
        // }

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;


        if (count($ujiantibiotik) > 0) {
        $pdf = PDF::loadview('evaluasi.bakteri.antibiotik.lembarPrint', compact('id','tanggal','years','ujiantibiotik','lembar','data','perusahaan','peka', 'type','siklus','date','val','kepekaan'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('Uji Kepekaan Antibiotik.pdf');
        }else{
            Session::flash('message', 'Bakteri belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }

    }

    public function insertAntibiotik(\Illuminate\Http\Request $request, $id)
    {
        $data = $request->all();
        $years = date('Y');
        $lembar = $request->get('lembar');
        if(empty($lembar)){
            $lembar = 1;
        }
        // dd($identifikasi);

        if ($data['simpan'] == "Simpan") {
            for ($i=0; $i < count($data['penilaian']); $i++) {
                $a = new Antibiotik;
                $a->id_registrasi = $id;
                $a->id_antibiotik = $request->id_anti[$i];
                $a->lembar = $lembar;
                $a->jenis_antibiotik = $request->antibiotik[$i];
                $a->interpretasi_hasil = $request->hasil[$i];
                $a->penilaian = $request->penilaian[$i];
                $a->save();
                Session::flash('message', 'Uji Kepekaan Antibiotik Berhasil Disimpan!');
                Session::flash('alert-class', 'alert-success');
            }
        }else{
            for ($i=0; $i < count($data['penilaian']); $i++) {
                Antibiotik::where("id", $request->id[$i])->update(["penilaian" => $request->penilaian[$i]]);
                Session::flash('message', 'Uji Kepekaan Antibiotik Berhasil Diupdate!');
                Session::flash('alert-class', 'alert-primary');
            }
        }
        return back();
    }

    public function perusahaan(\Illuminate\Http\Request $request)
    {
      // return('asd');
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes'))
                    ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang','=','13')
                    ->get();
                    // dd($datas);
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="bakteri/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Lihat
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.bakteri.rekapcetakpeserta.perusahaan');
    }

    public function dataperusahaan($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '13')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
            
        return view('evaluasi.bakteri.rekapcetakpeserta.dataperusahaan', compact('data'));
    }
}
