<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Pks;
use Illuminate\Support\Facades\Redirect;

class PksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $pks = DB::table('tb_pks')->get();
        return view('back/pks/index', compact('pks'));
    }

    public function edit($id)
    {
        $data = DB::table('tb_pks')->where('id', $id)->get();
        return view('back/pks/update', compact('data'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data['pks'] = $request->pks;
        Pks::where('id',$id)->update($data);
        Session::flash('message', 'Perjanjian Kerjasama Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/pks');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Pks::create($data);
        Session::flash('message', 'Perjanjian Kerjasama Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/pks');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Pks::find($id)->delete();
        return redirect("admin/pks");
    }
}
