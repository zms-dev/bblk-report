<?php

namespace App\Http\Controllers;
use DB;
use PDF;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        $banner = DB::table('tb_banner')->orderBy('id', 'desc')->get();
        return view('index', compact('banner'));
    }
    public function jadwal() 
    {
        $jadwal = DB::table('tb_jadwal')->orderBy('id', 'desc')->first();
        return view('jadwal', compact('jadwal'));
    }
    public function tarif()
    {
        $tahun = date('Y');
        $data = DB::table('tb_bidang')
            ->join('sub_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
            ->leftjoin('tb_registrasi','tb_registrasi.bidang','=', DB::raw('sub_bidang.id AND YEAR(tb_registrasi.created_at) = '.$tahun.''))
            ->select('sub_bidang.parameter','sub_bidang.id','sub_bidang.kuota_1','sub_bidang.kuota_2', 'sub_bidang.id_bidang','sub_bidang.tarif',  'tb_bidang.bidang as Bidang', 
                    DB::raw('(CASE
                                WHEN sub_bidang.id != 10 THEN
                                    sub_bidang.kuota_1 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 1 THEN tb_registrasi.bidang END))
                                ELSE
                                    (SELECT MIN(sisa_kuota_1) FROM (
                                            SELECT
                                                SBD1.kuota_1 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 1 THEN tb_registrasi.bidang END))
                                                as sisa_kuota_1
                                            FROM
                                                `sub_bidang` AS SBD1
                                            LEFT JOIN tb_registrasi ON tb_registrasi.bidang = SBD1.id AND YEAR(tb_registrasi.created_at) = '.$tahun.'
                                            WHERE SBD1.id_bidang = 6
                                            AND SBD1.id <> 13
                                            GROUP BY SBD1.id
                                    ) as c )
                            END
                            ) as sisa_kuota_1,
                            (CASE
                                WHEN sub_bidang.id != 10 THEN
                                    sub_bidang.kuota_2 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 2 THEN tb_registrasi.bidang END))
                                ELSE
                                    (SELECT MIN(sisa_kuota_2) FROM (
                                            SELECT
                                                SBD1.kuota_2 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 2 THEN tb_registrasi.bidang END))
                                                as sisa_kuota_2
                                            FROM
                                                `sub_bidang` AS SBD1
                                            LEFT JOIN tb_registrasi ON tb_registrasi.bidang = SBD1.id AND YEAR(tb_registrasi.created_at) = '.$tahun.'
                                            WHERE SBD1.id_bidang = 6
                                            AND SBD1.id <> 13
                                            GROUP BY SBD1.id
                                    ) as c )
                            END
                            ) as sisa_kuota_2'))
            ->groupBy('sub_bidang.id')
            ->orderBy('tb_bidang.id', 'ASC')
            ->get();
        return view('home', compact('data'));
    }
    public function manual()
    {
        $juklak = DB::table('sub_bidang')
            ->join('juklak', 'sub_bidang.id', '=', 'juklak.id_bidang')
            ->select('juklak.*', 'sub_bidang.parameter as Bidang')
            ->get();
        $data = DB::table('manual_book')->orderBy('id', 'desc')->get();
        return view('manual_book', compact('data', 'juklak'));
    }
    public function penggunaan(){
        $this->middleware('auth');
        $user = Auth::user()->role;
        $data = DB::table('penggunaan')->where('role', $user)->get();
        return view('penggunaan', compact('data'));
    }
    public function sptjm()
    {
        $sptjm = DB::table('tb_sptjm')
            ->get();
        return view('sptjm', compact('sptjm'));
    }
    public function evaluasi()
    {
        $this->middleware('auth');
        $user = Auth::user()->role;
        $uss = Auth::id();
        // dd($user);
        $sertifikat = DB::table('tb_registrasi')
            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
            ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
            ->where('tb_registrasi.created_by', $uss)
            ->where('tb_registrasi.evaluasi', 'done')
            ->where('tb_registrasi.evaluasi2', 'done')
            ->select('tb_registrasi.id', 'perusahaan.nama_lab', 'sub_bidang.parameter')
            ->get();
        if ($user == 3) {
            $data = DB::table('evaluasi')
                ->join('tb_registrasi', 'evaluasi.id_register', '=', 'tb_registrasi.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->where('tb_registrasi.created_by', $uss)
                ->select('evaluasi.id', 'evaluasi.sertifikat', 'evaluasi.type', 'evaluasi.file', 'perusahaan.nama_lab', 'sub_bidang.parameter')
                ->paginate(10);
        }else{
            $data = DB::table('evaluasi')
                ->join('tb_registrasi', 'evaluasi.id_register', '=', 'tb_registrasi.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->select('evaluasi.id', 'evaluasi.sertifikat', 'evaluasi.type', 'evaluasi.file', 'perusahaan.nama_lab', 'sub_bidang.parameter')
                ->paginate(10);
        }
        return view('evaluasi', compact('user','data', 'sertifikat'));
    }

    public function sertifikat($id){
        $user = Auth::id();
        $data = DB::table('tb_registrasi')
            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
            ->leftjoin('evaluasi', 'tb_registrasi.id', '=', 'evaluasi.id_register')
            ->leftjoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
            ->where('tb_registrasi.created_by', $user)
            ->where('tb_registrasi.id', $id)
            ->where('tb_registrasi.evaluasi', 'done')
            ->where('tb_registrasi.evaluasi2', 'done')
            ->where('evaluasi.type', 'b')
            ->select('tb_registrasi.id', 'tb_registrasi.kode_lebpes', 'tb_registrasi.siklus', DB::raw('year(tb_registrasi.created_at) as tahun'), 'perusahaan.nama_lab', 'sub_bidang.parameter', 'tb_bidang.bidang', 'evaluasi.created_at')
            ->first();
            // dd($data);
        $pdf = PDF::loadview('sertifikat', compact('data'))
            ->setPaper('a4', 'landscape')
            ->setwarnings(false);
        return $pdf->stream('sertifikat.pdf');
        // return view('sertifikat');
    }

    public function sentEmail(\Illuminate\Http\Request $request)
    {
        $this->middleware('auth');
        $data = $request->all();
        $tahun = date('Y');
        $user = array();
        $users = DB::table('users')
                    ->join('tb_registrasi','tb_registrasi.created_by','=','users.id')
                    ->where('users.id_member', '!=', '0')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                    ->where(function($query) use ($data){
                        $query->where('tb_registrasi.siklus','=', $data['siklus'])
                            ->orwhere('tb_registrasi.siklus','=', '12');
                    })
                    ->groupBy('users.id')
                    ->select('users.email')
                    ->get();
        // dd($users);
        foreach($users as $usr) {
            $sent = Mail::send('email.email_blast', $data , function ($mail) use ($usr, $data)
            {
                $mail->from('sipamela.bblkjkt@gmail.com', 'PNPME BBLK JAKARTA ');
                $mail->to($usr->email);
                $mail->subject($data['sub']);
            });
        }
        return redirect('email-blast');
    }

    public function getKota($id){
        $data = DB::table('regencies')->select('id as Kode','name as Nama')->where('province_id',$id)->get();
        return response()->json(['Hasil'=>$data]);
    }
    public function getKecamatan($id){
        $data = DB::table('districts')->select('id as Kode','name as Nama')->where('regency_id',$id)->get();
        return response()->json(['Hasil'=>$data]);
    }

    public function getKelurahan($id){
        $data = DB::table('villages')->select('id as Kode','name as Nama')->where('district_id',$id)->get();
        return response()->json(['Hasil'=>$data]);
    }

    public function getReagenImun($id){
        $data = DB::table('tb_reagen_imunologi')->select('produsen as Produsen','metode as Metode')->where('id',$id)->get();
        return response()->json(['Hasil'=>$data]);
    }

    public function datasiklus($id){
        $tahun = date('Y');
        
        $data = DB::table('tb_bidang')
            ->join('sub_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
            ->leftjoin('tb_registrasi','tb_registrasi.bidang','=', DB::raw('sub_bidang.id AND YEAR(tb_registrasi.created_at) = 2019'))
            ->select('sub_bidang.*', 'tb_bidang.bidang', 
                    DB::raw('(CASE
                                WHEN sub_bidang.id != 10 THEN
                                    sub_bidang.kuota_1 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 1 THEN tb_registrasi.bidang END))
                                ELSE
                                    (SELECT MIN(sisa_kuota_1) FROM (
                                            SELECT
                                                SBD1.kuota_1 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 1 THEN tb_registrasi.bidang END))
                                                as sisa_kuota_1
                                            FROM
                                                `sub_bidang` AS SBD1
                                            LEFT JOIN tb_registrasi ON tb_registrasi.bidang = SBD1.id AND YEAR(tb_registrasi.created_at) = '.$tahun.'
                                            WHERE SBD1.id_bidang = 6
                                            AND SBD1.id <> 13
                                            GROUP BY SBD1.id
                                    ) as c )
                            END
                            ) as kuota_1,
                            (CASE
                                WHEN sub_bidang.id != 10 THEN
                                    sub_bidang.kuota_2 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 2 THEN tb_registrasi.bidang END))
                                ELSE
                                    (SELECT MIN(sisa_kuota_2) FROM (
                                            SELECT
                                                SBD1.kuota_2 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 2 THEN tb_registrasi.bidang END))
                                                as sisa_kuota_2
                                            FROM
                                                `sub_bidang` AS SBD1
                                            LEFT JOIN tb_registrasi ON tb_registrasi.bidang = SBD1.id AND YEAR(tb_registrasi.created_at) = '.$tahun.'
                                            WHERE SBD1.id_bidang = 6
                                            AND SBD1.id <> 13
                                            GROUP BY SBD1.id
                                    ) as c )
                            END
                            ) as kuota_2'))
            ->groupBy('sub_bidang.id')
            ->orderBy('tb_bidang.id','ASC')
            ->get();
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }
}
