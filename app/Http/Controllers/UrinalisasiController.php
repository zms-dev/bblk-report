<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;
use Carbon\Carbon;

use App\HpHeader;
use App\Skoringuri;
use App\Parameter;
use App\MetodePemeriksaan;
use App\TbHp;
use App\Rujukanurinalisa;
use App\TbReagenImunologi as ReagenImunologi;
use App\HpDetail;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\View;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\register as Register;
use App\TanggalEvaluasi;
use App\Rujukanurinalisametode;
use Redirect;
use Validator;
use Session;

class UrinalisasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {
        $reagen = DB::table('tb_reagen_imunologi')
                    ->where('kelompok','=','Urinalisa')
                    ->where('parameter_id',NULL)
                    ->get();
        // dd($reagen);
        $data = Parameter::where('kategori', 'urinalisa')->get();
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;

        return view('hasil_pemeriksaan/urinalisasi', compact('data', 'perusahaan', 'type', 'siklus','type', 'reagen','date'));
    }

    public function view(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $tanggal = date('d-m-Y');
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('tb_instrumen', 'hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('tb_hp', 'hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
            ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->select('parameter.*','hp_details.alat as Alat', 'tb_instrumen.instrumen', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.instrument_lain', 'hp_details.hasil_pemeriksaan as Hasil','hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'tb_reagen_imunologi.reagen', 'hp_details.reagen_lain','hp_details.reagen as Reagen', 'tb_hp.hp')
            ->where('parameter.kategori', 'urinalisa')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;

        // return view('cetak_hasil/urinalisasi', compact('data', 'perusahaan', 'datas', 'type'));
        $pdf = PDF::loadview('cetak_hasil/urinalisasi', compact('data', 'perusahaan','datas', 'type', 'siklus' ,'date','tanggal','register'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Urinalisa.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');

        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;
        $type = $input['type'];
        $validasi = HpHeader::where(['id_registrasi'=>$id , 'siklus'=>$siklus , 'type'=>$type])->get();

        $rules = array(
            'kode_peserta' => 'required',
            'tanggal_penerimaan' => 'required',
            'tanggal_pemeriksaan' => 'required',
            'kualitas' => 'required'
        );

        $messages = array(
            'kode_peserta.required' => 'Kode Peserta Wajib diisi',
            'tanggal_penerimaan.required' => 'Tanggal Penerimaan wajib diisi',
            'tanggal_pemeriksaan.required' => 'Tanggal Pemeriksaan wajib diisi',
            'kualitas.required' => 'Kualitas wajib dipliih'
        );
        if (count($validasi)>0) {
            Session::flash('message', 'Hasil Urinalisasi Sudah Ada!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
        }else{
        $saveHeader = new HpHeader;
        $saveHeader->kode_lab = $input['kode_peserta'];
        $saveHeader->kode_peserta = $perusahaanID;
        $saveHeader->tgl_pemeriksaan = date('Y-m-d', strtotime($input['tanggal_pemeriksaan']));
        $saveHeader->tgl_penerimaan = date('Y-m-d', strtotime($input['tanggal_penerimaan']));
        $saveHeader->kualitas_bahan = $input['kualitas'];
        $saveHeader->catatan = $input['catatan'];
        $saveHeader->kategori = 'urinalisa';
        $saveHeader->kode_bahan = $input['kode_bahan'];
        $saveHeader->id_registrasi = $id;
        $saveHeader->penanggung_jawab = $input['penanggung_jawab'];
        $saveHeader->type = $input['type'];
        $saveHeader->siklus = $siklus;
        $saveHeader->save();

        $saveHeaderID = $saveHeader->id;

        $i = 0;
        foreach ($input['parameter_id'] as $parameter_id) {
            $saveDetail = new HpDetail;
            $saveDetail->parameter_id = $input['parameter_id'][$i];
            $saveDetail->hp_header_id = $saveHeaderID;
            $saveDetail->alat = $input['alat'][$i];
            $saveDetail->kode_metode_pemeriksaan = $input['kode'][$i];
            $saveDetail->hasil_pemeriksaan = $input['hasil'][$i];
            $saveDetail->reagen = $input['reagen'][$i];
            $saveDetail->reagen_lain = $input['reagen_lain'][$i];
            $saveDetail->metode_lain = $input['metode_lain'][$i];
            $saveDetail->save();
            $i++;
        }

        $date_sekarang = date('Y-m-d H:i:s');
        DB::table('tanggal_input')->insert(['id_register' => $id, 'siklus' => $siklus, 'type' => $input['type'], 'input_date' => $date_sekarang]);
        if ($siklus == '1') {
            Register::where('id',$id)->update(['siklus_1'=>'done']);
            if ($input['type'] == 'a') {
                Register::where('id',$id)->update(['pemeriksaan'=>'done', 'status_data1'=>'1']);
            }else{
                Register::where('id',$id)->update(['pemeriksaan2'=>'done', 'status_data2'=>'1']);
            }
        }else{
            Register::where('id',$id)->update(['siklus_2'=>'done',]);
            if ($input['type'] == 'a') {
                Register::where('id',$id)->update(['rpr1'=>'done', 'status_datarpr1'=>'1']);
            }else{
                Register::where('id',$id)->update(['rpr2'=>'done', 'status_datarpr2'=>'1']);
            }
        }
        return redirect('hasil-pemeriksaan');
        }
    }


    public function edit(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
            ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp')
            ->where('parameter.kategori', 'urinalisa')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
            foreach ($data as $key => $val) {
                $metode = MetodePemeriksaan::where('parameter_id', $val->id)->get();
                $val->metode = $metode;
                $reagen = ReagenImunologi::where('parameter_id', $val->id)->get();
                $val->reagen = $reagen;
                $hasilPemeriksaan = TbHp::where('parameter_id', $val->id)->get();
                $val->hasilPemeriksaan = $hasilPemeriksaan;
            }
            // dd($data);
        $reagen = DB::table('tb_reagen_imunologi')
                    ->where('kelompok','=','Urinalisa')
                    ->where('parameter_id',NULL)
                    ->get();
        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();
            // dd($siklus);
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;

        return view('edit_hasil/urinalisasi', compact('data', 'perusahaan', 'datas', 'type','siklus','date','instrumen','reagen'));
    }


    public function update(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $type = $request->get('x');
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;

        // dd($input);
        $SaveHeader['tgl_penerimaan'] = $request->tanggal_penerimaan;
        $SaveHeader['tgl_pemeriksaan'] = $request->tanggal_pemeriksaan;
        $SaveHeader['kualitas_bahan'] = $request->kualitas;
        $SaveHeader['kode_bahan'] = $request->kode_bahan;
        $SaveHeader['catatan'] = $request->catatan;
        $SaveHeader['penanggung_jawab'] = $request->penanggung_jawab;

        HpHeader::where('id_registrasi',$id)->where('siklus', $siklus)->where('type', $type)->update($SaveHeader);
        // dd($input);
        $i = 0;
        foreach ($input['alat'] as $alat) {
            if($alat != ''){
                $SaveDetail['alat'] = $request->alat[$i];
                $SaveDetail['reagen'] = $request->reagen[$i];
                $SaveDetail['reagen_lain'] = $request->reagen_lain[$i];
                $SaveDetail['kode_metode_pemeriksaan'] = $request->kode[$i];
                $SaveDetail['metode_lain'] = $request->metode_lain[$i];
                $SaveDetail['hasil_pemeriksaan'] = $request->hasil[$i];
                HpDetail::where('id', $request->id_detail[$i])->update($SaveDetail);
            }
            $i++;
        }
        if ($request->simpan == "Kirim" || $request->simpan == "Validasi") {
            if ($siklus == '1') {
                Register::where('id',$id)->update(['siklus_1'=>'done']);
                if ($input['type'] == 'a') {
                    Register::where('id',$id)->update(['pemeriksaan'=>'done', 'status_data1'=>'2']);
                }else{
                    Register::where('id',$id)->update(['pemeriksaan2'=>'done', 'status_data2'=>'2']);
                }
            }else{
                Register::where('id',$id)->update(['siklus_2'=>'done']);
                if ($input['type'] == 'a') {
                    Register::where('id',$id)->update(['rpr1'=>'done', 'status_datarpr1'=>'2']);
                }else{
                    Register::where('id',$id)->update(['rpr2'=>'done', 'status_datarpr2'=>'2']);
                }
            }
            if ($request->simpan == "Validasi") {
                DB::table('validasi_input')->where('id_register', $id)->where('siklus', $siklus)->where('type', $input['type'])->delete();
                DB::table('validasi_input')->insert(
                    ['id_register' => $id, 'siklus' => $siklus, 'type' => $input['type'], 'status' => 'done']
                );
                return back();
            }else{
                return redirect('edit-hasil');
            }
        }else{
            return back();
        }
    }

    public function evaluasi(\Illuminate\Http\Request $request, $id){
        $input = $request->all();
        // dd($input);
        // dd($input);
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $tanggal = date('Y m d');
        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
            ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp')
            ->where('parameter.kategori', 'urinalisa')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
            foreach ($data as $key => $val) {
                $metode = Rujukanurinalisametode::where('parameter', $val->id)
                                                ->where('metode',$val->Kode)
                                                ->where('siklus', $siklus)
                                                ->where('tahun', $date)
                                                ->where('type', $type)
                                                ->get();
                $val->metode = $metode;
                $reagen = ReagenImunologi::where('parameter_id', $val->id)->get();
                $val->reagen = $reagen;
                $hasilPemeriksaan = TbHp::where('parameter_id', $val->id)->get();
                $val->hasilPemeriksaan = $hasilPemeriksaan;
                $target = Rujukanurinalisa::where('parameter', $val->id)
                                            ->where('siklus', $siklus)
                                            ->where('tahun', $date)
                                            ->where('type', $type)
                                            ->get();
                $val->target = $target;
            }
        // dd($data);
            $skoring = DB::table('tb_skoring_urinalisa')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $date)
                    // ->where('bahan', $request->bahan)
                    ->get();
            
            $skoringUp = DB::table('tb_skoring_urinalisa')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $date)
                    // ->where('bahan', $request->bahan)
                    ->get();

        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();


        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        // return $data;
        if (count($data[10]->target)) {
            return view('evaluasi/urinalisa/input', compact('data','simpan', 'perusahaan', 'datas', 'type','siklus','date','instrumen', 'tanggal', 'skoring','id', 'input','register','perusahaan'));
        }else{
            Session::flash('message', 'Data Rujukan Belum Lengkap!');
            Session::flash('alert-class', 'alert-danger');
            return redirect::back();
        }
    }

    public function printevaluasi(\Illuminate\Http\Request $request, $id){
       $input = $request->all();
        // dd($input);
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $tanggal = date('Y m d');
        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
            ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp')
            ->where('parameter.kategori', 'urinalisa')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
            foreach ($data as $key => $val) {
                $metode = Rujukanurinalisametode::where('parameter', $val->id)
                                                ->where('metode',$val->Kode)
                                                ->where('siklus', $siklus)
                                                ->where('tahun', $date)
                                                ->where('type', $type)
                                                ->get();
                $val->metode = $metode;
                $reagen = ReagenImunologi::where('parameter_id', $val->id)->get();
                $val->reagen = $reagen;
                $hasilPemeriksaan = TbHp::where('parameter_id', $val->id)->get();
                $val->hasilPemeriksaan = $hasilPemeriksaan;
                $target = Rujukanurinalisa::where('parameter', $val->id)
                                            ->where('siklus', $siklus)
                                            ->where('tahun', $date)
                                            ->where('type', $type)
                                            ->get();
                $val->target = $target;
            }

        // dd($data[10]->target);
        $skoring = DB::table('tb_skoring_urinalisa')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $date)
                    // ->where('bahan', $input['bahan'])
                    ->get();
        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();


        $skoring1 = DB::table('tb_skoring_urinalisa')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $date)
                    // ->where('bahan', $input['bahan'])
                    ->first();


        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $evaluasi = TanggalEvaluasi::where('form','=','Urinalisa')->where('siklus', $siklus)->where('tahun', $date)->first(); 

        if ($input['simpan'] == 'Print') {
            if (count($data[10]->target) > 0) {
            $pdf = PDF::loadview('evaluasi/urinalisa/print/evaluasi', compact('data' ,'perusahaan', 'datas', 'type','siklus','date','instrumen', 'tanggal', 'skoring','id','evaluasi','register','perusahaan'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Urinalisa.pdf');
            }else{
                 Session::flash('message', 'Urinalisa belum dievaluasi!');
                 Session::flash('alert-class', 'alert-danger');
                return back();
            }
        }else if($input['simpan'] == 'Update'){
            Skoringuri::where("id", $request->id)->update(["catatan" => $request->catatan, "status" => "1"]);
            return back();
        }else if($input['simpan'] == 'Batal Evaluasi'){
            Skoringuri::where("id", $request->id)->delete();
            return back();
        }

    }

    public function simpanskor(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $tanggal = date('Y m d');
        $input = $request->all();
        if ($input['simpan'] == 'Simpan') {
            $SaveData = new Skoringuri;
            $SaveData->id_registrasi = $id;
            $SaveData->skoring = $input['skoring'];
            $SaveData->skoringmetode = $input['skoringmetode'];
            $SaveData->bahan = $input['bahan'];
            $SaveData->siklus = $siklus;
            $SaveData->type = $type;
            $SaveData->tahun = $date;
            $SaveData->catatan = $input['catatan'];
            $SaveData->save();
        }else{
            $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
            $data = DB::table('parameter')
                ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
                ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
                ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
                ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
                ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
                ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
                ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp')
                ->where('parameter.kategori', 'urinalisa')
                ->where('hp_headers.id_registrasi', $id)
                ->where('hp_headers.type', $type)
                ->where('hp_headers.siklus', $siklus)
                ->get();
                foreach ($data as $key => $val) {
                    $metode = MetodePemeriksaan::where('parameter_id', $val->id)->get();
                    $val->metode = $metode;
                    $reagen = ReagenImunologi::where('parameter_id', $val->id)->get();
                    $val->reagen = $reagen;
                    $hasilPemeriksaan = TbHp::where('parameter_id', $val->id)->get();
                    $val->hasilPemeriksaan = $hasilPemeriksaan;
                    $target = Rujukanurinalisa::where('parameter', $val->id)
                                                ->where('siklus', $siklus)
                                                ->where('tahun', $date)
                                                ->get();
                    $val->target = $target;
                }
                // dd($data);
            $skoring = DB::table('tb_skoring_urinalisa')->where('id_registrasi', $id)->where('siklus', $siklus)->where('type', $type)->where('tahun', $date)->get();
            $datas = HpHeader::where('id_registrasi', $id)
                ->where('type', $type)
                ->first();

            $register = Register::find($id);
            $perusahaan = $register->perusahaan->nama_lab;

            return view('evaluasi/urinalisa/input', compact('data', 'perusahaan', 'datas', 'type','siklus','date','instrumen', 'tanggal', 'skoring'));

        }
        return redirect::back();
    }

    public function penilaianparameter(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        if ($type == "a") {
            $form = 'Negatif';
        }else{
            $form = 'Positif';
        }
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $reagen = Rujukanurinalisa::where('siklus', $siklus)
                                    ->where('tahun', $date)
                                    ->where('bahan', $form)
                                    ->count();
        // dd($form);
        $peserta = DB::table('tb_registrasi')->where('id', $id)->first();
        $parameter = DB::table('parameter')->where('kategori', '=', 'urinalisa')->get();
        // dd($parameter);
        foreach ($parameter as $key => $val) {
            if ($val->id > 10) {
                $masing = DB::table('tb_hp')
                            ->select('tb_hp.*',
                                DB::raw('(SELECT COUNT(*) from hp_details as HPD
                                        INNER JOIN `hp_headers` on `hp_headers`.`id` = `HPD`.`hp_header_id`
                                        INNER JOIN `tb_registrasi` on `tb_registrasi`.`id` = `hp_headers`.`id_registrasi`
                                            WHERE
                                                `HPD`.`hasil_pemeriksaan` = `tb_hp`.`id`
                                            AND
                                                `HPD`.`parameter_id` = tb_hp.parameter_id
                                            AND
                                                `hp_headers`.`type` = "'.$type.'"
                                            AND
                                                `hp_headers`.`siklus` = '.$siklus.'
                                            AND
                                                YEAR(tb_registrasi.created_at) = '.$date.'
                                  ) as Jumlah'))
                            ->where('tb_hp.parameter_id', '=', $val->id)
                            ->groupBy('tb_hp.id')
                            ->orderBy(DB::raw('FIELD(hp, "±","Negatif")'), 'desc')
                            ->get();

                $hasilna = DB::table('hp_details')
                            ->join('hp_headers', 'hp_details.hp_header_id','=','hp_headers.id')
                            ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                            ->join('tb_hp','hp_details.hasil_pemeriksaan','=','tb_hp.id')
                            ->where('hp_details.parameter_id', $val->id)
                            ->where('hp_headers.type', $type)
                            ->where('hp_headers.siklus', $siklus)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->where('tb_registrasi.id', $id)
                            ->select('tb_hp.hp')
                            ->first();


            }else{
                $masing = DB::table('hp_details')
                            ->join('hp_headers', 'hp_details.hp_header_id','=','hp_headers.id')
                            ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                            ->where('hp_details.parameter_id', $val->id)
                            ->where('hp_headers.type', $type)
                            ->where('hp_headers.siklus', $siklus)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->where(function($query) use ($val) {
                                if($val->id == '9') {
                                    $query->where('hp_details.hasil_pemeriksaan', '<=', '1.030' )
                                        ->where('hp_details.hasil_pemeriksaan', '>=', '1.000' )
                                        ->whereIn('hp_details.hasil_pemeriksaan', [1.000, 1.005, 1.010, 1.015, 1.020, 1.025, 1.030]);
                                }elseif ($val->id == '10') {
                                    $query->wherein('hp_details.hasil_pemeriksaan', [5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0]);
                                }
                             })
                            ->select('hp_details.hasil_pemeriksaan as hp', DB::raw('COUNT(hasil_pemeriksaan) as Jumlah'))
                            ->groupBy('hp_details.hasil_pemeriksaan')
                            ->get();
                $hasilna = DB::table('hp_details')
                            ->join('hp_headers', 'hp_details.hp_header_id','=','hp_headers.id')
                            ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                            ->where('hp_details.parameter_id', $val->id)
                            ->where('hp_headers.type', $type)
                            ->where('hp_headers.siklus', $siklus)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                            ->where('tb_registrasi.id', $id)
                            ->select('hp_details.hasil_pemeriksaan as hp')
                            ->first();
            }
            $target = Rujukanurinalisa::where('parameter', $val->id)
                                        ->where('siklus', $siklus)
                                        ->where('tahun', $date)
                                        ->where('bahan', $form)
                                        ->select('batas_bawah','batas_atas')
                                        ->first();
            $val->target = $target;
            $val->masing = $masing;
            $val->hasilna = $hasilna;
        }
        
        if ($reagen == '11') {
            return view('evaluasi.urinalisa.grafikparameter.grafik', compact('parameter', 'peserta', 'date','siklus', 'type'));
        }else{
            Session::flash('message', 'Data Rujukan Belum Lengkap!');
            Session::flash('alert-class', 'alert-danger');
            return redirect::back();
        }
        // dd($parameter);
    }

    public function penilaianreagen(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $reagen = DB::table('hp_details')
                    ->join('tb_reagen_imunologi','hp_details.reagen','=','tb_reagen_imunologi.id')
                    ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                    ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                    ->select('tb_reagen_imunologi.reagen','tb_reagen_imunologi.id', 'hp_headers.kode_lab', DB::raw('COUNT(DISTINCT tb_registrasi.id) as jumlah'))
                    ->groupBy('hp_details.reagen')
                    ->where('hp_headers.type', $type)
                    ->where('hp_headers.siklus', $siklus)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                    ->whereNotIn('hp_details.parameter_id', [19])
                    ->orderBy('tb_reagen_imunologi.reagen', 'asc')
                    ->get();
        $data = DB::table('hp_details')
                ->join('tb_reagen_imunologi','hp_details.reagen','=','tb_reagen_imunologi.id')
                ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                ->select('tb_reagen_imunologi.reagen','tb_reagen_imunologi.id', 'tb_registrasi.kode_lebpes')
                ->where('hp_headers.type', $type)
                ->where('hp_headers.siklus', $siklus)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->where('tb_registrasi.id', $id)
                ->whereNotIn('hp_details.parameter_id', [19])
                ->get();
        $kehamilan = DB::table('hp_details')
                    ->join('tb_reagen_imunologi','hp_details.reagen','=','tb_reagen_imunologi.id')
                    ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                    ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                    ->select('tb_reagen_imunologi.reagen','tb_reagen_imunologi.id', 'hp_headers.kode_lab', DB::raw('COUNT(DISTINCT tb_registrasi.id) as jumlah'))
                    ->groupBy('hp_details.reagen')
                    ->where('hp_headers.type', $type)
                    ->where('hp_headers.siklus', $siklus)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                    ->whereIn('hp_details.parameter_id', [19])
                    ->orderBy('tb_reagen_imunologi.reagen', 'asc')
                    ->get();
        $data2 = DB::table('hp_details')
                ->join('tb_reagen_imunologi','hp_details.reagen','=','tb_reagen_imunologi.id')
                ->join('hp_headers','hp_details.hp_header_id','=','hp_headers.id')
                ->join('tb_registrasi','hp_headers.id_registrasi','=','tb_registrasi.id')
                ->select('tb_reagen_imunologi.reagen','tb_reagen_imunologi.id', 'tb_registrasi.kode_lebpes')
                ->where('hp_headers.type', $type)
                ->where('hp_headers.siklus', $siklus)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->where('tb_registrasi.id', $id)
                ->whereIn('hp_details.parameter_id', [19])
                ->get();
                // dd($reagen);
        return view('evaluasi.urinalisa.grafikreagen.grafik', compact('reagen','data','kehamilan','data2','siklus','type','date'));
        // dd($parameter);
    }

    public function penilaianreagenkehamilan(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;
                // dd($reagen);
        return view('evaluasi.urinalisa.grafikreagenkehamilan.grafik', compact('reagen','data'));
        // dd($parameter);
    }

    public function perusahaan(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');

            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.kode_lebpes','tb_registrasi.id as idregistrasi')
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->where('tb_registrasi.bidang','=','3')
                ->where('tb_registrasi.status','>=','2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '=', 12)
                            ->orwhere('tb_registrasi.siklus', '=', $siklus);
                    })
                ->where(function($query){
                  if(Auth::user()->role != '5'){
                    $query->where('tb.registrasi.created_by',Auth::user()->id);
                  }
                })
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="urinalisa/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if($siklus == 1) {
                        if ($inputhasil->status_data1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }else{
                        if ($inputhasil->status_datarpr1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if($siklus == 1) {
                        if ($inputhasil->status_data1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }else{
                        if ($inputhasil->status_datarpr1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
        return view('evaluasi.urinalisa.rekapcetakpeserta.perusahaan');
    }

    public function dataperusahaan(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('sub_bidang.id', '=', '3')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
        return view('evaluasi.urinalisa.rekapcetakpeserta.dataperusahaan', compact('data','siklus'));
    }

}
