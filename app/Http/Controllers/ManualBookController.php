<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\manualbook as Book;
use Illuminate\Support\Facades\Redirect;

class ManualBookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = DB::table('manual_book')->get();
        return view('back/manual/index', compact('data'));
    }

    public function edit($id)
    {
        $data = DB::table('manual_book')->get();
        return view('back/manual/update', compact('data'));
    }

    public function in()
    {
        return view('back/manual/insert');
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data = Request::all();
        if (Request::hasFile('file'))
        {
            $dest = public_path('asset/backend/manual/');
            $name = Request::file('file')->getClientOriginalName();
            Request::file('file')->move($dest, $name);
            $data['file'] = $name;
        }
        if (Request::hasFile('image'))
        {
            $desti = public_path('asset/backend/manual/');
            $namei = Request::file('image')->getClientOriginalName();
            Request::file('image')->move($desti, $namei);
            $data['image'] = $namei;
        }
        $data['created_by'] = Auth::user()->id;
        Book::where('id',$id)->update($data);
        Session::flash('message', 'Manual Book Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/manual-book');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
       if (Request::hasFile('file'))
        {
            $dest = public_path('asset/backend/manual/');
            $name = Request::file('file')->getClientOriginalName();
            $ucp = Request::file('file')->move($dest, $name);
            // dd($ucp);
        }
        $data = Request::file('file');
        $data = Request::all();
        $data['file'] = $name;
        $data['created_by'] = Auth::user()->id;
        // dd($data);
        Book::create($data);
        Session::flash('message', 'Manual Book Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/manual-book');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Book::find($id)->delete();
        return redirect("admin/manual-book");
    }
}
