<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\User;
use App\daftar as Daftar;
use Illuminate\Support\Facades\Redirect;

class DatadiriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $id = Auth::user()->id;
        $data = DB::table('users')
                ->leftjoin('perusahaan', 'users.id', '=', 'perusahaan.created_by')
                ->leftjoin('badan_usaha', 'perusahaan.pemerintah', '=', 'badan_usaha.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                ->leftjoin('districts', 'perusahaan.kecamatan', '=', 'districts.id')
                ->leftjoin('villages', 'perusahaan.kelurahan', '=', 'villages.id')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab', 'badan_usaha.badan_usaha as Badan', 'badan_usaha.id as Idbadan', 'perusahaan.alamat', 'perusahaan.kode_pos', 'provinces.name as Provinsi', 'provinces.id as Idprovinsi', 'regencies.name as Kota', 'regencies.id as Idkota', 'districts.name as Kecamatan', 'districts.id as Idkecamatan', 'villages.name as Kelurahan', 'villages.id as Idkelurahan', 'perusahaan.telp as Telp', 'perusahaan.email as Email', 'perusahaan.penanggung_jawab', 'perusahaan.personal', 'perusahaan.no_hp', 'perusahaan.no_wa')
                ->where('users.id', $id)
                ->first();
        $provinsi = DB::table('provinces')->select('id','name')->get();
        $badan = DB::table('badan_usaha')->get();
        // dd($data);
        return view('edit_data', compact('data', 'badan', 'provinsi'));
    }

    public function update(\Illuminate\Http\Request $request)
    {
        $id = Auth::user()->id;
     
        $data['nama_lab'] = $request->nama_lab;
        $data['pemerintah'] = $request->pemerintah;
        $data['alamat'] = $request->alamat;
        $data['provinsi'] = $request->provinsi;
        $data['kota'] = $request->kota;
        $data['kecamatan'] = $request->kecamatan;
        $data['kelurahan'] = $request->kelurahan;
        $data['telp'] = $request->telp;
        $data['email'] = $request->email;
        $data['penanggung_jawab'] = $request->penanggung_jawab;
        $data['personal'] = $request->personal;
        $data['no_hp'] = $request->no_hp;
        $data['no_wa'] = $request->no_wa;
        $data['kode_pos'] = $request->kode_pos;


        $user['name'] = $request->name;
        $user['email'] = $request->email;

        $password = bcrypt($request->password);
        if ($request->password != "") {
            $user['password'] = $password;
        }

        Daftar::where('created_by',$id)->update($data);
        User::where('id',$id)->update($user);
        Session::flash('message', 'Data diri berhasil diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('edit-data');
    }
}
