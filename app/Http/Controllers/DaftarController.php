<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\register as Register;
use App\subbidang as Subbidang;
use App\daftar as Daftar;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Mail;

class DaftarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function getReagenImun($id){
        $data = DB::table('tb_reagen_imunologi')->select('produsen as Produsen','metode as Metode','spesifisitas','sensitivitas')->where('id',$id)->get();
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function status()
    {
        $user = Auth::user()->id;
        $date = date('Y');
        $daftar = DB::table('tb_registrasi')
                    ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                    ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                    ->where('tb_registrasi.created_by', $user)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $date)
                    ->select('tb_registrasi.*', 'sub_bidang.parameter', 'tb_bidang.bidang')
                    ->get();
        // dd($daftar);
        return view('status_pendaftaran', compact('daftar'));
    }

    public function index()
    {
        $user = Auth::user()->id;
        $date = date('Y');
        $perusahaan = DB::table('perusahaan')
                    ->where('created_by', $user)
                    ->get();
        if (!empty($perusahaan) && count($perusahaan) > 0) {
            $siklus = DB::table('perusahaan')
                        ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                        ->where('perusahaan.created_by', $user)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $date)
                        ->select('tb_registrasi.siklus')
                        ->groupBy('tb_registrasi.siklus')
                        ->get();
            $daftar = DB::table('tb_registrasi')
                        ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                        ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                        ->where('tb_registrasi.created_by', $user)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $date)
                        ->get();
            $provinsi = DB::table('provinces')->select('id','name')->get();
            $bidang = DB::table('sub_bidang')->get();
            $badan = DB::table('badan_usaha')->get();
            return view('daftar_old', compact('provinsi', 'bidang', 'badan', 'siklus','daftar'));   
        }else{
            $provinsi = DB::table('provinces')->select('id','name')->get();
            $bidang = DB::table('sub_bidang')->get();
            $badan = DB::table('badan_usaha')->get();
            return view('daftar', compact('provinsi', 'bidang', 'badan'));
        }
    }

    public function daftar(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        
        if ($input['pembayaran'] == 'transfer')
        {
            $validasi = Validator::make($input,[
                'file' => 'required|mimes:jpeg,pdf,png'
            ]);
        }else{
            $validasi = Validator::make($input,[
                'file1' => 'required|mimes:jpeg,pdf,png'
            ]);
        }

        $tahun = date('Y');
        $register = DB::table('tb_registrasi')
                    ->join('users', 'users.id','=','tb_registrasi.created_by')
                    ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                    ->whereNotNull('no_urut')
                    ->select('no_urut')
                    ->groupBy('no_urut')
                    ->orderBy('no_urut', 'desc')
                    ->first();
        if(count($register)){
            $cekno = $register->no_urut + 1;
            $no_urut = sprintf("%03s", $cekno);
        }else{
            $no_urut = '001';
        }

        $cekkuota = DB::table('tb_bidang')
            ->join('sub_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
            ->leftjoin('tb_registrasi','tb_registrasi.bidang','=', DB::raw('sub_bidang.id AND YEAR(tb_registrasi.created_at) = '.$tahun.''))
            ->select('sub_bidang.parameter','sub_bidang.id','sub_bidang.kuota_1','sub_bidang.kuota_2', 'sub_bidang.id_bidang','sub_bidang.tarif',  'tb_bidang.bidang as Bidang', 
                    DB::raw('(CASE
                                WHEN sub_bidang.id != 10 THEN
                                    sub_bidang.kuota_1 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 1 THEN tb_registrasi.bidang END))
                                ELSE
                                    (SELECT MIN(sisa_kuota_1) FROM (
                                            SELECT
                                                SBD1.kuota_1 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 1 THEN tb_registrasi.bidang END))
                                                as sisa_kuota_1
                                            FROM
                                                `sub_bidang` AS SBD1
                                            LEFT JOIN tb_registrasi ON tb_registrasi.bidang = SBD1.id AND YEAR(tb_registrasi.created_at) = '.$tahun.'
                                            WHERE SBD1.id_bidang = 6
                                            GROUP BY SBD1.id
                                    ) as c )
                            END
                            ) as sisa_kuota_1,
                            (CASE
                                WHEN sub_bidang.id != 10 THEN
                                    sub_bidang.kuota_2 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 2 THEN tb_registrasi.bidang END))
                                ELSE
                                    (SELECT MIN(sisa_kuota_2) FROM (
                                            SELECT
                                                SBD1.kuota_2 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 2 THEN tb_registrasi.bidang END))
                                                as sisa_kuota_2
                                            FROM
                                                `sub_bidang` AS SBD1
                                            LEFT JOIN tb_registrasi ON tb_registrasi.bidang = SBD1.id AND YEAR(tb_registrasi.created_at) = '.$tahun.'
                                            WHERE SBD1.id_bidang = 6
                                            GROUP BY SBD1.id
                                    ) as c )
                            END
                            ) as sisa_kuota_2'))
            ->wherein('sub_bidang.id', $input['bidang'])
            ->groupBy('sub_bidang.id')
            ->orderBy('sisa_kuota_1','asc')
            ->orderBy('sisa_kuota_2','asc')
            ->first();
            
        if ($input['siklus'] == 1) {
            $cekkuotasiklus = $cekkuota->sisa_kuota_1 < 1;
        }elseif($input['siklus'] == 2){
            $cekkuotasiklus = $cekkuota->sisa_kuota_2 < 1 ;
        }else{
            $cekkuotasiklus = $cekkuota->sisa_kuota_1 < 1 || $cekkuota->sisa_kuota_2 < 1 ;
        }
        if ($cekkuotasiklus) {
            Session::flash('message', 'Salah satu Bidang yang anda pilih Kuotanya sudah Habis!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return Redirect::back();
        }else{
            if ($validasi->fails()) {
                return redirect('daftar')->withInput()->withErrors($validasi);
            }
            $saveDaftar = new Daftar;
            $saveDaftar->nama_lab = $input['nama_lab'];
            $saveDaftar->pemerintah = $input['pemerintah'];
            $saveDaftar->alamat = $input['alamat'];
            $saveDaftar->provinsi = $input['provinsi'];
            $saveDaftar->kota = $input['kota'];
            $saveDaftar->kecamatan = $input['kecamatan'];
            $saveDaftar->kelurahan = $input['kelurahan'];
            $saveDaftar->telp = $input['telp'];
            $saveDaftar->email = $input['email'];
            $saveDaftar->penanggung_jawab = $input['penanggung_jawab'];
            $saveDaftar->personal = $input['personal'];
            $saveDaftar->no_hp = $input['no_hp'];
            $saveDaftar->kode_pos = $input['kode_pos'];
            $saveDaftar->created_by = Auth::user()->id;
            $saveDaftar->save();

            $saveDaftarID = $saveDaftar->id;

            $i = 0;
            $im = 1;
            if (Request::hasFile('file'))
            {
                $dest = public_path('asset/file/');
                $name = $input['nama_lab'].'-'.$tahun.'-'.$input['siklus'].'-'.Request::file('file')->getClientOriginalName();
                Request::file('file')->move($dest, $name);
            }
            if (Request::hasFile('file1'))
            {
                $dest = public_path('asset/file/');
                $name1 = $input['nama_lab'].'-'.$tahun.'-'.$input['siklus'].'-'.Request::file('file1')->getClientOriginalName();
                Request::file('file1')->move($dest, $name1);
            }
            $a = Auth::user()->id_member;
            $kode_pes = sprintf("%03s", $a);
            $date = date('y');
            foreach ($input['bidang'] as $alat) {
                $datas = $request->all();
                if($alat != ''){
                    if($alat == '10'){
                        $imun = DB::table('sub_bidang')->whereIn('id', [6,7,8,9])->get();
                        foreach ($imun as $key => $val) {
                            $saveRegister = new Register;
                            $saveRegister->perusahaan_id = $saveDaftar->id;
                            $saveRegister->bidang = $val->id;
                            $saveRegister->no_urut = $no_urut;
                            $saveRegister->kode_lebpes = $val->alias.'-'.$input['siklus'].'-'.$date.'-'.$no_urut;
                            if ($input['pembayaran'] == 'transfer')
                            {
                                $saveRegister->file = $name;
                                $saveRegister->status = '1';
                                $saveRegister->id_pembayaran = '1';
                            }else{
                                $saveRegister->sptjm = $input['sptjm'];
                                $saveRegister->file = $name1;
                                $saveRegister->id_pembayaran = '2';
                                $saveRegister->status = '2';
                            }
                            $saveRegister->siklus = $input['siklus'];
                            $saveRegister->created_by = Auth::user()->id;
                            $saveRegister->save();
                        }
                    }else{
                        $saveRegister = new Register;
                        $saveRegister->perusahaan_id = $saveDaftar->id;
                        $saveRegister->bidang = $alat;
                        $saveRegister->no_urut = $no_urut;
                        $saveRegister->kode_lebpes = $input['alias'][$alat - 1].'-'.$input['siklus'].'-'.$date.'-'.$no_urut;
                        if ($input['pembayaran'] == 'transfer')
                        {
                            $saveRegister->file = $name;
                            $saveRegister->status = '1';
                            $saveRegister->id_pembayaran = '1';
                        }else{
                            $saveRegister->sptjm = $input['sptjm'];
                            $saveRegister->file = $name1;
                            $saveRegister->id_pembayaran = '2';
                            $saveRegister->status = '2';
                        }
                        $saveRegister->siklus = $input['siklus'];
                        $saveRegister->created_by = Auth::user()->id;
                        $saveRegister->save();
                    }
                    $i++;
                }
            }
            $sub_bidang = DB::table('sub_bidang')->whereIn('id', $input['bidang'])->get();
            $user = DB::table('users')->where('role', 4)->first();
            $datu = [
                'labklinik' => $input['nama_lab'],
                'siklus' => $input['siklus'],
                'date' => date('Y'),
                'parameter' => $sub_bidang 
            ];
             
            Mail::send('email.pendaftaran', $datu, function ($mail) use ($input, $user)
            {
              $mail->from('sipamela.bblkjkt@gmail.com', 'BBLK Jakarta Daftar '. $input['nama_lab']);
              $mail->to($user->email);
              // $mail->to('tengkufirmansyah2@gmail.com');
              $mail->subject('Daftar PNPME');
            });

            Session::flash('message', 'Selamat, Anda telah berhasil melakukan Pendaftaran, Kami akan mengecek data yang telah anda input, Anda akan menerima email konfirmasi jika kami telah selesai melakukan verifikasi terhadap data yang anda kirimkan, Terimakasih'); 
            Session::flash('alert-class', 'alert-success'); 
            return redirect('index');
        }
    }
    
    public function daftar_old(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        
        if ($input['pembayaran'] == 'transfer')
        {
            $validasi = Validator::make($input,[
                'file' => 'required|mimes:jpeg,pdf,png'
            ]);
        }else{
            $validasi = Validator::make($input,[
                'file1' => 'required|mimes:jpeg,pdf,png'
            ]);
        }

        if ($validasi->fails()) {
            return redirect('daftar')->withInput()->withErrors($validasi);
        }

        $tahun = date('Y');
        $register = DB::table('tb_registrasi')
                    ->join('users', 'users.id','=','tb_registrasi.created_by')
                    ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                    ->whereNotNull('no_urut')
                    ->select('no_urut')
                    ->groupBy('no_urut')
                    ->orderBy('no_urut', 'desc')
                    ->first();
        if(count($register)){
            $cekno = $register->no_urut + 1;
            $no_urut = sprintf("%03s", $cekno);
        }else{
            $no_urut = '001';
        }

        $user = Auth::user()->id;
        $perusahaan = DB::table('perusahaan')->where('created_by', $user)->first();

        $cekkuota = DB::table('tb_bidang')
            ->join('sub_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
            ->leftjoin('tb_registrasi','tb_registrasi.bidang','=', DB::raw('sub_bidang.id AND YEAR(tb_registrasi.created_at) = '.$tahun.''))
            ->select('sub_bidang.parameter','sub_bidang.id','sub_bidang.kuota_1','sub_bidang.kuota_2', 'sub_bidang.id_bidang','sub_bidang.tarif',  'tb_bidang.bidang as Bidang', 
                    DB::raw('(CASE
                                WHEN sub_bidang.id != 10 THEN
                                    sub_bidang.kuota_1 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 1 THEN tb_registrasi.bidang END))
                                ELSE
                                    (SELECT MIN(sisa_kuota_1) FROM (
                                            SELECT
                                                SBD1.kuota_1 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 1 THEN tb_registrasi.bidang END))
                                                as sisa_kuota_1
                                            FROM
                                                `sub_bidang` AS SBD1
                                            LEFT JOIN tb_registrasi ON tb_registrasi.bidang = SBD1.id AND YEAR(tb_registrasi.created_at) = '.$tahun.'
                                            WHERE SBD1.id_bidang = 6
                                            GROUP BY SBD1.id
                                    ) as c )
                            END
                            ) as sisa_kuota_1,
                            (CASE
                                WHEN sub_bidang.id != 10 THEN
                                    sub_bidang.kuota_2 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 2 THEN tb_registrasi.bidang END))
                                ELSE
                                    (SELECT MIN(sisa_kuota_2) FROM (
                                            SELECT
                                                SBD1.kuota_2 - (COUNT(CASE WHEN tb_registrasi.siklus = 12 || tb_registrasi.siklus = 2 THEN tb_registrasi.bidang END))
                                                as sisa_kuota_2
                                            FROM
                                                `sub_bidang` AS SBD1
                                            LEFT JOIN tb_registrasi ON tb_registrasi.bidang = SBD1.id AND YEAR(tb_registrasi.created_at) = '.$tahun.'
                                            WHERE SBD1.id_bidang = 6
                                            GROUP BY SBD1.id
                                    ) as c )
                            END
                            ) as sisa_kuota_2'))
            ->wherein('sub_bidang.id', $input['bidang'])
            ->groupBy('sub_bidang.id')
            ->orderBy('sisa_kuota_1','asc')
            ->orderBy('sisa_kuota_2','asc')
            ->first();
        if ($input['siklus'] == 1) {
            $cekkuotasiklus = $cekkuota->sisa_kuota_1 < 1;
        }elseif($input['siklus'] == 2){
            $cekkuotasiklus = $cekkuota->sisa_kuota_2 < 1 ;
        }else{
            $cekkuotasiklus = $cekkuota->sisa_kuota_1 < 1 || $cekkuota->sisa_kuota_2 < 1 ;
        }
        if ($cekkuotasiklus) {
            Session::flash('message', 'Salah satu Bidang yang anda pilih Kuotanya sudah Habis!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return Redirect::back();
        }else{
            $i = 0;
            $im = 1;
            if (Request::hasFile('file'))
            {
                $dest = public_path('asset/file/');
                $name = $perusahaan->nama_lab.'-'.$tahun.'-'.$input['siklus'].'-'.Request::file('file')->getClientOriginalName();
                Request::file('file')->move($dest, $name);
            }
            if (Request::hasFile('file1'))
            {
                $dest = public_path('asset/file/');
                $name1 = $perusahaan->nama_lab.'-'.$tahun.'-'.$input['siklus'].'-'.Request::file('file1')->getClientOriginalName();
                Request::file('file1')->move($dest, $name1);
            }
            // dd($sub_bidang);
            $a = Auth::user()->id_member;
            $kode_pes = sprintf("%03s", $a);
            $date = date('y');
            foreach ($input['bidang'] as $alat) {
                $datas = $request->all();
                if($alat != ''){
                    if($alat == '10'){
                        $imun = DB::table('sub_bidang')->whereIn('id', [6,7,8,9])->get();
                        foreach ($imun as $key => $val) {
                            $saveRegister = new Register;
                            $saveRegister->perusahaan_id = $perusahaan->id;
                            $saveRegister->bidang = $val->id;
                            $saveRegister->no_urut = $no_urut;
                            $saveRegister->kode_lebpes = $val->alias.'-'.$input['siklus'].'-'.$date.'-'.$no_urut;
                            if ($input['pembayaran'] == 'transfer')
                            {
                                $saveRegister->file = $name;
                                $saveRegister->status = '1';
                                $saveRegister->id_pembayaran = '1';
                            }else{
                                $saveRegister->sptjm = $input['sptjm'];
                                $saveRegister->file = $name1;
                                $saveRegister->id_pembayaran = '2';
                                $saveRegister->status = '2';
                            }
                            $saveRegister->siklus = $input['siklus'];
                            $saveRegister->created_by = Auth::user()->id;
                            $saveRegister->save();
                        }
                    }else{
                        $saveRegister = new Register;
                        $saveRegister->perusahaan_id = $perusahaan->id;
                        $saveRegister->bidang = $alat;
                        $saveRegister->no_urut = $no_urut;
                        $saveRegister->kode_lebpes = $input['alias'][$alat - 1].'-'.$input['siklus'].'-'.$date.'-'.$no_urut;
                        if ($input['pembayaran'] == 'transfer')
                        {
                            $saveRegister->file = $name;
                            $saveRegister->status = '1';
                            $saveRegister->id_pembayaran = '1';
                        }else{
                            $saveRegister->sptjm = $input['sptjm'];
                            $saveRegister->file = $name1;
                            $saveRegister->id_pembayaran = '2';
                            $saveRegister->status = '2';
                        }
                        $saveRegister->siklus = $input['siklus'];
                        $saveRegister->created_by = Auth::user()->id;
                        $saveRegister->save();
                    }
                    $i++;
                }
            }
            $sub_bidang = DB::table('sub_bidang')->whereIn('id', $input['bidang'])->get();
            $user = DB::table('users')->where('role', 4)->first();
            $datu = [
                'labklinik' => $perusahaan->nama_lab,
                'siklus' => $input['siklus'],
                'date' => date('Y'),
                'parameter' => $sub_bidang 
            ];
             
            Mail::send('email.pendaftaran', $datu, function ($mail) use ($input, $user, $perusahaan)
            {
              $mail->from('sipamela.bblkjkt@gmail.com', 'BBLK Jakarta Daftar '.$perusahaan->nama_lab);
              $mail->to($user->email);
              // $mail->to('tengkufirmansyah2@gmail.com');
              $mail->subject('Daftar PNPME');
            });
            Session::flash('message', 'Selamat, Anda telah berhasil melakukan Pendaftaran, Kami akan mengecek data yang telah anda input, Anda akan menerima email konfirmasi jika kami telah selesai melakukan verifikasi terhadap data yang anda kirimkan, Terimakasih'); 
            Session::flash('alert-class', 'alert-success'); 
            return redirect('index');
        }
    }

}
