<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use App\Hide;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\banner as Banner;
use Illuminate\Support\Facades\Redirect;

class BannerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $banner = DB::table('tb_banner')->get();
        return view('back/banner/banner', compact('banner'));
    }

    public function edit($id)
    {
        $data = DB::table('tb_banner')->where('id', $id)->get();
        return view('back/banner/update', compact('data'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        if (Request::hasFile('image'))
        {
            $dest = public_path('asset/backend/banner/');
            $name = Request::file('image')->getClientOriginalName();
            Request::file('image')->move($dest, $name);
        }
        $data = Request::file('image');
        $data = Request::all();
        $data['image'] = $name;
        $data['created_by'] = Auth::user()->id;
        Banner::where('id',$id)->update($data);
        Session::flash('message', 'Banner Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/banner');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        if (Request::hasFile('image'))
        {
            $dest = public_path('asset/backend/banner/');
            $name = Request::file('image')->getClientOriginalName();
            Request::file('image')->move($dest, $name);
        }
        $data = Request::file('image');
        $data = Request::all();
        $data['image'] = $name;
        $data['created_by'] = Auth::user()->id;
        Banner::create($data);
        Session::flash('message', 'Banner Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/banner');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Banner::find($id)->delete();
        return redirect("admin/banner");
    }

    public function edithasil()
    {
        return view('back.edithasil.index');
    }
    public function edithasilna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        // dd($input);  
        if ($input['siklus'] == '1') {
            $proses = DB::table('tb_registrasi')
                    ->update(array('status_data1' => 2, 'status_data2' => 2));
        }else{
            $proses = DB::table('tb_registrasi')
                    ->update(array('status_datarpr1' => 2, 'status_datarpr2' => 2));
        }
        Session::flash('message', 'Data Edit Peserta Telah di Terupdate!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect::back();
    }

    public function menu()
    {
        return view('back.edithasil.menu');
    }
    public function menuna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $menu = Hide::where('menu', $input['menu'])->get();
        // dd($menu);
        if (count($menu)) {
            $proses = Hide::where('menu', $input['menu'])
                    ->update(['status'=> $input['status']]);
        }else{
            $proses = Hide::insert(
                            ['menu' => $input['menu'], 'status' => $input['status']]
                        );
        }
        Session::flash('message', 'Menu Web PNPME Berhasil di Ubah!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect::back();
    }
    
    public function siklus()
    {
        $menu = DB::table('tb_siklus')->first();
        return view('back.edithasil.siklus', compact('menu'));
    }
    public function siklusna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $menu = DB::table('tb_siklus')->get();
        // dd($menu);
        if (count($menu)) {
            $proses = DB::table('tb_siklus')->update(['siklus'=> $input['siklus'], 'tahun' => $input['tahun']]);
        }else{
            $proses = DB::table('tb_siklus')->insert(['siklus'=> $input['siklus'], 'tahun' => $input['tahun']]);
        }
        Session::flash('message', 'Siklus PNPME Berhasil di Ubah!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect::back();
    }
    
    public function tahun()
    {
        $menu = DB::table('tb_tahun_evaluasi')->first();
        return view('back.edithasil.tahun_evaluasi', compact('menu'));
    }
    public function tahunna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $menu = DB::table('tb_tahun_evaluasi')->get();
        // dd($menu);
        if (count($menu)) {
            $proses = DB::table('tb_tahun_evaluasi')->update(['siklus'=> $input['siklus'], 'tahun' => $input['tahun']]);
        }else{
            $proses = DB::table('tb_tahun_evaluasi')->insert(['siklus'=> $input['siklus'], 'tahun' => $input['tahun']]);
        }
        Session::flash('message', 'Tahun Evaluasi PNPME Berhasil di Ubah!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect::back();
    }
}
