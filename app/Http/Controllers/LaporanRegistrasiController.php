<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Excel;
use PDF;
class LaporanRegistrasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function peserta(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->join('provinces','perusahaan.provinsi','=','provinces.id')
                ->join('regencies','perusahaan.kota','=','regencies.id')
                ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->join('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.status','>=','2')
                ->select('tb_registrasi.*','perusahaan.nama_lab','perusahaan.alamat','perusahaan.telp','perusahaan.penanggung_jawab','perusahaan.personal','perusahaan.email','perusahaan.no_hp','provinces.name as Provinsi', 'regencies.name as Kota','sub_bidang.alias', 'perusahaan.pemerintah')
                ->get();
                // dd($data);
        return view('laporan.peserta.proses', compact('data', 'input'));
    }

    public function pesertaexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->join('provinces','perusahaan.provinsi','=','provinces.id')
                ->join('regencies','perusahaan.kota','=','regencies.id')
                ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->join('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.status','>=','2')
                ->select('tb_registrasi.*','perusahaan.nama_lab','perusahaan.alamat','perusahaan.telp','perusahaan.penanggung_jawab','perusahaan.personal','perusahaan.email','perusahaan.no_hp','provinces.name as Provinsi', 'regencies.name as Kota','sub_bidang.alias', 'perusahaan.pemerintah')
                ->get();
                // dd($data);
        // return view('laporan.peserta.proses', compact('data', 'input'));

        Excel::create('Peserta PME', function($excel) use ($data, $input) {
            $excel->sheet('PME', function($sheet) use ($data, $input) {
                $sheet->loadView('laporan.peserta.view', array('data'=>$data, 'input'=>$input) );
            });

        })->download('xls');
    }

    public function pesertaparameter()
    {
        $data = DB::table('sub_bidang')->where('id','!=','10')->get();
        return view('laporan.pesertaparameter.registrasi', compact('data'));
    }
    public function pesertaparameterp(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->join('provinces','perusahaan.provinsi','=','provinces.id')
                ->join('regencies','perusahaan.kota','=','regencies.id')
                ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->join('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.bidang','=', $input['parameter'])
                ->where('tb_registrasi.status','>=','2')
                ->where(function ($query) use ($input) {
                    if($input['siklus'] == '1'){
                        $query->where('tb_registrasi.siklus', '=', '1')
                              ->orWhere('tb_registrasi.siklus', '=', '12');
                    }else{
                        $query->where('tb_registrasi.siklus', '=', '2')
                              ->orWhere('tb_registrasi.siklus', '=', '12');
                    }
                })
                ->select('tb_registrasi.*','perusahaan.nama_lab','perusahaan.alamat','perusahaan.telp','perusahaan.penanggung_jawab','perusahaan.personal','perusahaan.email','perusahaan.no_hp','provinces.name as Provinsi', 'regencies.name as Kota','sub_bidang.alias', 'perusahaan.pemerintah')
                ->get();
                // dd($data);
        return view('laporan.pesertaparameter.proses', compact('data', 'input'));
    }

    public function pesertaparameterexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->join('provinces','perusahaan.provinsi','=','provinces.id')
                ->join('regencies','perusahaan.kota','=','regencies.id')
                ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.bidang','=', $input['parameter'])
                ->where('tb_registrasi.status','>=','2')
                ->where(function ($query) use ($input) {
                    if($input['siklus'] == '1'){
                        $query->where('tb_registrasi.siklus', '=', '1')
                              ->orWhere('tb_registrasi.siklus', '=', '12');
                    }else{
                        $query->where('tb_registrasi.siklus', '=', '2')
                              ->orWhere('tb_registrasi.siklus', '=', '12');
                    }
                })
                ->select('tb_registrasi.*','perusahaan.nama_lab','perusahaan.alamat','perusahaan.telp','perusahaan.penanggung_jawab','perusahaan.personal','perusahaan.email','perusahaan.no_hp','provinces.name as Provinsi', 'regencies.name as Kota','sub_bidang.alias')
                ->get();
                // dd($data);
        // return view('laporan.peserta.proses', compact('data', 'input'));

        Excel::create('Peserta PME per Parameter', function($excel) use ($data, $input) {
            $excel->sheet('PME', function($sheet) use ($data, $input) {
                $sheet->loadView('laporan.pesertaparameter.view', array('data'=>$data, 'input'=>$input) );
            });
        })->download('xls');
    }

    public function index(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $dataSiklus = "";
        $dataSiklus1 = "";
        if($input['siklus'] == "1"){
            $dataSiklus = "AND (tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12)";
            $dataSiklus1 = "(tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12)";
        }elseif($input['siklus'] == "2"){
            $dataSiklus = "AND (tb_registrasi.siklus = 2 OR tb_registrasi.siklus = 12)";
            $dataSiklus1 = "(tb_registrasi.siklus = 2 OR tb_registrasi.siklus = 12)";
        }elseif($input['siklus'] == "12"){
            $dataSiklus = "AND (tb_registrasi.siklus = 2 OR tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1)";
            $dataSiklus1 = "(tb_registrasi.siklus = 2 OR tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1)";
        }
        $data = DB::table('perusahaan')
        ->join('provinces','perusahaan.provinsi','=','provinces.id')
        ->join('regencies','perusahaan.kota','=','regencies.id')
        ->join('districts','perusahaan.kecamatan','=','districts.id')
        ->join('villages','perusahaan.kelurahan','=','villages.id')
        ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
        ->leftjoin('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
        ->select(
            'perusahaan.*',
            'badan_usaha.badan_usaha',
            "tb_registrasi.kode_lebpes",
            "provinces.name as province_name",
            "regencies.name as city_name",
            "districts.name as district_name",
            "villages.name as village_name",
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=1 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as hema_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=2 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as kk_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=3 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as urin_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=4 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as bta_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=5 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as tc_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=11 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as mal_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND (TR.bidang=6 OR TR.bidang=10) AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as ahiv_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND (TR.bidang=7 OR TR.bidang=10) AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as syph_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND (TR.bidang=8 OR TR.bidang=10) AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as hbsag_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND (TR.bidang=9 OR TR.bidang=10) AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as ahcv_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=12 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as kai_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=13 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as dar_count")
        )
        ->whereRaw($dataSiklus1)
        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
        ->groupBy('perusahaan.id')
        ->groupBy('tb_registrasi.siklus')
        ->havingRaw('hema_count > 0 OR kk_count > 0 OR urin_count > 0 OR bta_count > 0 OR tc_count > 0 OR mal_count > 0 OR ahiv_count > 0 OR syph_count > 0 OR hbsag_count > 0 OR ahcv_count > 0 OR kai_count > 0 OR dar_count > 0')
        ->get();
        return view('laporan.registrasi.proses', compact('data', 'input'));
    }

    // public function indexpdf(\Illuminate\Http\Request $request)
    // {
    //     $input = $request->all();

    //     $data = DB::table('tb_registrasi')
    //             ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
    //             ->join('users', 'perusahaan.created_by', '=', 'users.id')
    //             ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
    //             ->join('badan_usaha', 'perusahaan.pemerintah', '=', 'badan_usaha.id')
    //             ->where('siklus', $input['siklus'])
    //             ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //             //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
    //             ->select('tb_registrasi.perusahaan_id', 'perusahaan.alamat', 'perusahaan.nama_lab as Nama', 'perusahaan.personal', 'perusahaan.no_hp', 'badan_usaha.badan_usaha as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'users.email')
    //             ->groupBy('tb_registrasi.perusahaan_id')
    //             ->groupBy('perusahaan.alamat')
    //             ->groupBy('perusahaan.no_hp')
    //             ->groupBy('perusahaan.personal')
    //             ->groupBy('perusahaan.nama_lab')
    //             ->groupBy('badan_usaha.badan_usaha')
    //             ->groupBy('perusahaan.telp')
    //             ->groupBy('provinces.name')
    //             ->groupBy('users.email')
    //             ->get();
    //     foreach($data as $skey => $r)
    //     {
    //         $hematologi = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 1)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->h = $hematologi;
    //         $kimiaklinik = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 2)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->kk = $kimiaklinik;
    //         $urinalisa = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 3)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->uri = $urinalisa;
    //         $mbta = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 4)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->bta = $mbta;
    //         $mtc = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 5)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->tc = $mtc;
    //         $antihiv = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 6)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->ahiv = $antihiv;
    //         $syphilis = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 7)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->syph = $syphilis;
    //         $hbsag = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 8)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->hbsag = $hbsag;
    //         $antihcv = DB::table('tb_registrasi')
    //                         ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
    //                         ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
    //                         ->join('provinces','perusahaan.provinsi' ,'provinces.id')
    //                         ->where('sub_bidang.id','=', 9)
    //                         ->where('perusahaan.id', $r->perusahaan_id)
    //                         ->where('tb_registrasi.siklus', $input['siklus'])
    //                         ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
    //                         ->count();
    //         $r->ahcv = $antihcv;
    //     }
    //     $pdf = PDF::loadview('laporan.registrasi.proses', compact('data', 'input'))
    //         ->setPaper('a4', 'potrait')
    //         ->setwarnings(false);

    //     return $pdf->stream('Registrasi.pdf');
    // }

    public function indexexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $dataSiklus = "";
        $dataSiklus1 = "";
        if($input['siklus'] == "1"){
            $dataSiklus = "AND (tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12)";
            $dataSiklus1 = "(tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12)";
        }elseif($input['siklus'] == "2"){
            $dataSiklus = "AND (tb_registrasi.siklus = 2 OR tb_registrasi.siklus = 12)";
            $dataSiklus1 = "(tb_registrasi.siklus = 2 OR tb_registrasi.siklus = 12)";
        }elseif($input['siklus'] == "12"){
            $dataSiklus = "AND (tb_registrasi.siklus = 2 OR tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1)";
            $dataSiklus1 = "(tb_registrasi.siklus = 2 OR tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 1)";
        }
        $data = DB::table('perusahaan')
        ->join('provinces','perusahaan.provinsi','=','provinces.id')
        ->join('regencies','perusahaan.kota','=','regencies.id')
        ->join('districts','perusahaan.kecamatan','=','districts.id')
        ->join('villages','perusahaan.kelurahan','=','villages.id')
        ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
        ->leftjoin('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
        ->select(
            'perusahaan.*',
            'badan_usaha.badan_usaha',
            "tb_registrasi.kode_lebpes",
            "provinces.name as province_name",
            "regencies.name as city_name",
            "districts.name as district_name",
            "villages.name as village_name",
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=1 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as hema_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=2 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as kk_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=3 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as urin_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=4 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as bta_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=5 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as tc_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=11 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as mal_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND (TR.bidang=6 OR TR.bidang=10) AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as ahiv_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND (TR.bidang=7 OR TR.bidang=10) AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as syph_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND (TR.bidang=8 OR TR.bidang=10) AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as hbsag_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND (TR.bidang=9 OR TR.bidang=10) AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as ahcv_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=12 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as kai_count"),
            DB::raw("(SELECT COUNT(*) FROM tb_registrasi as TR WHERE TR.perusahaan_id = perusahaan.id AND TR.bidang=13 AND YEAR(TR.created_at) = ".$input['tahun']." AND siklus = tb_registrasi.siklus ) as dar_count")
        )
        ->whereRaw($dataSiklus1)
        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
        ->groupBy('perusahaan.id')
        ->groupBy('tb_registrasi.siklus')
        ->havingRaw('hema_count > 0 OR kk_count > 0 OR urin_count > 0 OR bta_count > 0 OR tc_count > 0 OR mal_count > 0 OR ahiv_count > 0 OR syph_count > 0 OR hbsag_count > 0 OR ahcv_count > 0 OR kai_count > 0 OR dar_count > 0')
        ->get();
        Excel::create('Laporan Registrasi', function($excel) use ($data, $input) {
            $excel->sheet('Registrasi', function($sheet) use ($data, $input) {
                $sheet->loadView('laporan.registrasi.view', array('data'=>$data, 'input'=>$input) );
            });

        })->download('xls');
    }


    public function uangmasuk(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        if ($input['pembayaran'] == '1') {
            $pembayaran = NULL;
        }else {
            $pembayaran = '-';
        }
        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->join('users', 'perusahaan.created_by', '=', 'users.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->join('badan_usaha', 'perusahaan.pemerintah', '=', 'badan_usaha.id')
                ->where('tb_registrasi.sptjm', $pembayaran)
                ->where('tb_registrasi.status', '>=', '2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('tb_registrasi.perusahaan_id', 'perusahaan.alamat', 'perusahaan.nama_lab as Nama', 'perusahaan.personal', 'perusahaan.no_hp', 'badan_usaha.badan_usaha as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'users.email')
                ->groupBy('tb_registrasi.perusahaan_id')
                ->groupBy('tb_registrasi.siklus')
                // ->groupBy('tb_registrasi.created_at')
                ->groupBy('perusahaan.nama_lab')
                ->get();

        foreach($data as $skey => $r)
        {
            $hematologi = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 1)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->h = $hematologi;
            $kimiaklinik = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 2)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->kk = $kimiaklinik;
            $urinalisa = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 3)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->uri = $urinalisa;
            $mbta = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 4)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->bta = $mbta;
            $mtc = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 5)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->tc = $mtc;
            $antihiv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 6)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->ahiv = $antihiv;
            $syphilis = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 7)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->syph = $syphilis;
            $hbsag = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 8)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->hbsag = $hbsag;
            $antihcv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 9)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->ahcv = $antihcv;
            $total = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->tot = $total;
        }
        return view('laporan.uangmasuk.proses', compact('data', 'input'));
    }

    public function uangmasukexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        if ($input['pembayaran'] == '1') {
            $pembayaran = NULL;
        }else {
            $pembayaran = '-';
        }
        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->join('users', 'perusahaan.created_by', '=', 'users.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->join('badan_usaha', 'perusahaan.pemerintah', '=', 'badan_usaha.id')
                ->where('tb_registrasi.sptjm', $pembayaran)
                ->where('tb_registrasi.status', '>=', '2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('tb_registrasi.perusahaan_id', 'perusahaan.alamat', 'perusahaan.nama_lab as Nama', 'perusahaan.personal', 'perusahaan.no_hp', 'badan_usaha.badan_usaha as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'users.email')
                ->groupBy('tb_registrasi.perusahaan_id')
                // ->groupBy('tb_registrasi.created_at')
                ->groupBy('perusahaan.nama_lab')
                ->get();

        foreach($data as $skey => $r)
        {
            $hematologi = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 1)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->h = $hematologi;
            $kimiaklinik = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 2)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->kk = $kimiaklinik;
            $urinalisa = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 3)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->uri = $urinalisa;
            $mbta = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 4)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->bta = $mbta;
            $mtc = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 5)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->tc = $mtc;
            $antihiv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 6)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->ahiv = $antihiv;
            $syphilis = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 7)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->syph = $syphilis;
            $hbsag = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 8)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->hbsag = $hbsag;
            $antihcv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 9)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->ahcv = $antihcv;
            $malaria = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 10)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->mal = $malaria;
            $kimiaair = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 11)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->kai = $kimiaair;
            $kimiaterbatas = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 12)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->kat = $kimiaterbatas;
            $bakteri = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 13)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->bac = $bakteri;
            $total = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])->select(
                                    DB::raw('sum(case 
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                            end) as tarif'))
                            ->get();
            $r->tot = $total;
        }
        Excel::create('Penerimaan Uang Masuk', function($excel) use ($data, $input) {
            $excel->sheet('Uang Masuk', function($sheet) use ($data, $input) {
                $sheet->loadView('laporan.uangmasuk.view', array('data'=>$data, 'input'=>$input) );
                $sheet->mergeCells('H1:J1');
                $sheet->mergeCells('K1:L1');
                $sheet->mergeCells('N1:Q1');
                $sheet->mergeCells('R1:R2');
                $sheet->cell('A1', function($cell) {
                    $cell->setValue('No'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('B1', function($cell) {
                    $cell->setValue('Tanggal'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('C1', function($cell) {
                    $cell->setValue('Nama Instansi'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('H2', function($cell) {
                    $cell->setValue('H (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('I2', function($cell) {
                    $cell->setValue('KK (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('J2', function($cell) {
                    $cell->setValue('Urinalisa (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('K2', function($cell) {
                    $cell->setValue('BTA (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('L2', function($cell) {
                    $cell->setValue('Tc (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('M2', function($cell) {
                    $cell->setValue('A HIV (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('N2', function($cell) {
                    $cell->setValue('Syph (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('O2', function($cell) {
                    $cell->setValue('HBsAg (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('P2', function($cell) {
                    $cell->setValue('A HCV (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
                $sheet->cell('Q2', function($cell) {
                    $cell->setValue('Paket (Rp)'); $cell->setAlignment('center'); $cell->setValignment('center'); $cell->setFontWeight('bold');
                });
            });

        })->download('xls');
    }

    public function uangmasukpdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->where('tb_registrasi.siklus', $input['siklus'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('tb_registrasi.perusahaan_id', 'perusahaan.nama_lab as Nama', 'tb_registrasi.created_at')
                ->groupBy('tb_registrasi.perusahaan_id')
                // ->groupBy('tb_registrasi.created_at')
                ->groupBy('perusahaan.nama_lab')
                ->get();

        foreach($data as $skey => $r)
        {
            $hematologi = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 1)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->h = $hematologi;
            $kimiaklinik = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 2)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->kk = $kimiaklinik;
            $urinalisa = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 3)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->uri = $urinalisa;
            $mbta = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 4)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->bta = $mbta;
            $mtc = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 5)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->tc = $mtc;
            $antihiv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 6)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->ahiv = $antihiv;
            $syphilis = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 7)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->syph = $syphilis;
            $hbsag = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 8)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->hbsag = $hbsag;
            $antihcv = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 9)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->ahcv = $antihcv;
            $paket = DB::table('tb_registrasi')
                            ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                            ->join('perusahaan','perusahaan.id' ,'tb_registrasi.perusahaan_id')
                            ->join('provinces','perusahaan.provinsi' ,'provinces.id')
                            ->where('sub_bidang.id','=', 10)
                            ->where('perusahaan.id', $r->perusahaan_id)
                            ->where('tb_registrasi.siklus', $input['siklus'])
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                            ->get();
            $r->paket = $paket;
        }
        $pdf = PDF::loadview('laporan.uangmasuk.proses', compact('data', 'input'))
            ->setPaper('a4', 'landscape')
            ->setwarnings(false);

        return $pdf->stream('Uang-masuk.pdf');
    }


    public function sampel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('tb_kirim')
                ->join('tb_registrasi', 'tb_kirim.id_registrasi', '=', 'tb_registrasi.id')
                ->leftjoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                ->where(DB::raw('YEAR(tb_kirim.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('tb_registrasi.*', 'perusahaan.nama_lab as Nama', 'perusahaan.pemerintah as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'sub_bidang.parameter as Bidang', 'sub_bidang.tarif as Tarif', 'sub_bidang.siklus as Siklus', 'regencies.name as Kota', 'perusahaan.alamat as Alamat')
                ->get();
        return view('laporan.sampel.proses', compact('data', 'input'));
    }

    public function sampelexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('tb_kirim')
                ->join('tb_registrasi', 'tb_kirim.id_registrasi', '=', 'tb_registrasi.id')
                ->leftjoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                ->where(DB::raw('YEAR(tb_kirim.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('tb_registrasi.*', 'perusahaan.nama_lab as Nama', 'perusahaan.pemerintah as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'sub_bidang.parameter as Bidang', 'sub_bidang.tarif as Tarif', 'sub_bidang.siklus as Siklus', 'regencies.name as Kota', 'perusahaan.alamat as Alamat')
                ->get();
        Excel::create('Penerimaan Sampel', function($excel) use ($data, $input) {
            $excel->sheet('Sampel', function($sheet) use ($data, $input) {
                $sheet->loadView('laporan.sampel.view', array('data'=>$data, 'input'=>$input) );
            });

        })->download('xls');
    }

    public function sampelpdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('tb_kirim')
                ->join('tb_registrasi', 'tb_kirim.id_registrasi', '=', 'tb_registrasi.id')
                ->leftjoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                ->where(DB::raw('YEAR(tb_kirim.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('tb_registrasi.*', 'perusahaan.nama_lab as Nama', 'perusahaan.pemerintah as Jenisbadan', 'perusahaan.telp as Telp', 'provinces.name as Provinsi', 'sub_bidang.parameter as Bidang', 'sub_bidang.tarif as Tarif', 'sub_bidang.siklus as Siklus', 'regencies.name as Kota', 'perusahaan.alamat as Alamat')
                ->get();

        $pdf = PDF::loadview('laporan.sampel.proses', compact('data', 'input'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Penerimaan Sampel.pdf');
    }

    public function evaluasi(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('evaluasi')
                ->join('tb_registrasi', 'evaluasi.id_register', '=', 'tb_registrasi.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->join('users', 'tb_registrasi.created_by', '=', 'users.id')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->where(DB::raw('YEAR(evaluasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('evaluasi.*', 'perusahaan.nama_lab', 'tb_registrasi.kode_lebpes', 'sub_bidang.parameter as Bidang')
                ->get();
        return view('laporan.evaluasi.proses', compact('data', 'input'));
    }

    public function evaluasiexcel(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('evaluasi')
                ->join('tb_registrasi', 'evaluasi.id_register', '=', 'tb_registrasi.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->join('users', 'tb_registrasi.created_by', '=', 'users.id')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->where(DB::raw('YEAR(evaluasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('evaluasi.*', 'perusahaan.nama_lab', 'tb_registrasi.kode_lebpes', 'sub_bidang.parameter as Bidang')
                ->get();

        Excel::create('Evaluasi', function($excel) use ($data, $input) {
            $excel->sheet('Evaluasi', function($sheet) use ($data, $input) {
                $sheet->loadView('laporan.evaluasi.view', array('data'=>$data, 'input'=>$input) );
            });

        })->download('xls');
    }

    public function evaluasipdf(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('evaluasi')
                ->join('tb_registrasi', 'evaluasi.id_register', '=', 'tb_registrasi.id')
                ->join('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                ->join('users', 'tb_registrasi.created_by', '=', 'users.id')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->where(DB::raw('YEAR(evaluasi.created_at)'), '=' , $input['tahun'])
                ->where('tb_registrasi.siklus', $input['siklus'])
                //->where('tb_registrasi.created_at' ,'LIKE' , $input['tahun'].'%%')
                ->select('evaluasi.*', 'perusahaan.nama_lab', 'tb_registrasi.kode_lebpes', 'sub_bidang.parameter as Bidang')
                ->get();
        $pdf = PDF::loadview('laporan.evaluasi.proses', compact('data', 'input'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Evaluasi.pdf');
    }


    public function rekappeserta(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('x');
        // dd($siklus);
        $data = DB::table('sub_bidang')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->select('sub_bidang.*', 'tb_bidang.bidang')
                ->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)
                                          ->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)
                                          ->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 2)
                                          ->orWhere('tb_registrasi.siklus', '=', 12)
                                          ->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->count();
            $r->jumlah = $jumlah;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekappesertaexcel(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $siklus = $request->siklus;
        $data = DB::table('sub_bidang')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->select('sub_bidang.*', 'tb_bidang.bidang')
                ->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)
                                          ->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)
                                          ->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 2)
                                          ->orWhere('tb_registrasi.siklus', '=', 12)
                                          ->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->count();
            $r->jumlah = $jumlah;
        }
        // dd($data);
        if ($request->export == "Export Excel") {
            Excel::create('Rekap Peserta Berdasarkan Parameter', function($excel) use ($data, $tahun) {
                $excel->sheet('Rekap', function($sheet) use ($data, $tahun) {
                    $sheet->loadView('laporan.rekap.excel.peserta_parameter', array('data'=>$data, 'tahun'=>$tahun) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('laporan.rekap.excel.peserta_parameter', compact('data', 'tahun'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Rekap Peserta Berdasarkan Parameter.pdf');
        }
    }

    public function monitoring1(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $data = DB::table('sub_bidang')
                ->join('tb_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
                ->select('sub_bidang.id','sub_bidang.alias', 'tb_bidang.bidang')
                ->wherein('sub_bidang.id', [1,2,3])
                ->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                $query->where('tb_registrasi.siklus','=','12')
                                      ->orwhere('tb_registrasi.siklus','=',$siklus);
                            })
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->jumlah = $jumlah;
            if ($siklus == "1") {
                $beluminput1 = DB::table('tb_registrasi')
                                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.pemeriksaan','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->beluminput1 = $beluminput1;
                $beluminput2 = DB::table('tb_registrasi')
                                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.pemeriksaan2','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->beluminput2 = $beluminput2;
                $input1 = DB::table('tb_registrasi')
                                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.status_data1','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->input1 = $input1;
                $input2 = DB::table('tb_registrasi')
                                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.status_data2','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->input2 = $input2;
                $sudahinput1 = DB::table('tb_registrasi')
                                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.status_data1','=', '2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->sudahinput1 = $sudahinput1;
                $sudahinput2 = DB::table('tb_registrasi')
                                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.status_data2','=', '2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->sudahinput2 = $sudahinput2;
            }else{
                $beluminput1 = DB::table('tb_registrasi')
                                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.rpr1','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->beluminput1 = $beluminput1;
                $beluminput2 = DB::table('tb_registrasi')
                                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.rpr2','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->beluminput2 = $beluminput2;
                $input1 = DB::table('tb_registrasi')
                                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.status_datarpr1','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->input1 = $input1;
                $input2 = DB::table('tb_registrasi')
                                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.status_datarpr2','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->input2 = $input2;
                $sudahinput1 = DB::table('tb_registrasi')
                                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.status_datarpr1','=', '2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->sudahinput1 = $sudahinput1;
                $sudahinput2 = DB::table('tb_registrasi')
                                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.status_datarpr2','=', '2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->sudahinput2 = $sudahinput2;
            }
            if(Auth::user()->penyelenggara == '3'){
                $sudahevaluasi1 = DB::table('tb_skoring_urinalisa')
                            ->where('type','=', 'a')
                            ->where('siklus','=', $siklus)
                            ->where('tahun', '=' , $id)
                            ->select(DB::raw('DISTINCT id_registrasi'))->get();
                $r->sudahevaluasi1 = count($sudahevaluasi1);

                $sudahevaluasi2 = DB::table('tb_skoring_urinalisa')
                            ->where('type','=', 'a')
                            ->where('siklus','=', $siklus)
                            ->where('tahun', '=' , $id)
                            ->select(DB::raw('DISTINCT id_registrasi'))->get();
                $r->sudahevaluasi2 = count($sudahevaluasi2);
            }else{
                $sudahevaluasi1 = DB::table('tb_zscore')
                                ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                                ->where('tb_zscore.form','=',strtolower($r->bidang))
                                ->where('tb_zscore.type','=', 'a')
                                ->where('tb_zscore.siklus','=', $siklus)
                                ->where('tb_zscore.tahun', '=' , $id)
                                ->select(DB::raw('DISTINCT id_registrasi'))->get();
                $r->sudahevaluasi1 = count($sudahevaluasi1);


                $sudahevaluasi2 = DB::table('tb_zscore')
                                ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_zscore.id_registrasi')
                                ->where('tb_zscore.form','=',strtolower($r->bidang))
                                ->where('tb_zscore.type','=', 'b')
                                ->where('tb_zscore.siklus','=', $siklus)
                                ->where('tb_zscore.tahun', '=' , $id)
                                ->select(DB::raw('DISTINCT id_registrasi'))->get();
                $r->sudahevaluasi2 = count($sudahevaluasi2);
            }

            $r->belumevaluasi1 = $r->sudahinput1 - $r->sudahevaluasi1;
            $r->belumevaluasi2 = $r->sudahinput2 - $r->sudahevaluasi2;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function monitoring2(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $data = DB::table('sub_bidang')->where('id', '=', Auth::user()->penyelenggara)->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->where(function ($query) use ($siklus) {
                                $query->where('tb_registrasi.siklus','=','12')
                                      ->orwhere('tb_registrasi.siklus','=',$siklus);
                            })
                            ->count();
            $r->jumlah = $jumlah;
            if ($siklus == "1") {
                $beluminput1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where('tb_registrasi.siklus_1','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->count();
                $r->beluminput1 = $beluminput1;
                $input1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where('tb_registrasi.status_data1','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->count();
                $r->input1 = $input1;
                $sudahinput1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where('tb_registrasi.status_data1','=', '2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->count();
                $r->sudahinput1 = $sudahinput1;
            }else{
                $beluminput1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where('tb_registrasi.siklus_2','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->count();
                $r->beluminput1 = $beluminput1;
                $input1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where('tb_registrasi.status_data2','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->count();
                $r->input1 = $input1;
                $sudahinput1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where('tb_registrasi.status_data2','=', '2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->count();
                $r->sudahinput1 = $sudahinput1;
            }
            $sudahevaluasi = DB::table('tb_kesimpulan_evaluasi')
                            ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $id)
                            ->where('siklus', $siklus)
                            ->where(function ($query){
                                if (Auth::user()->penyelenggara == 6) {
                                    $query->where('type', '=', 'Anti Hiv');
                                }elseif(Auth::user()->penyelenggara == 7){
                                    $query->where('type', '=', 'Syphilis');
                                }elseif(Auth::user()->penyelenggara == 8){
                                    $query->where('type', '=', 'HBsAg');
                                }elseif(Auth::user()->penyelenggara == 9){
                                    $query->where('type', '=', 'ANTI-HCV');
                                }
                            })
                            ->count();
            $r->sudahevaluasi = $sudahevaluasi;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function monitoring3(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $data = DB::table('sub_bidang')->where('id', '=', Auth::user()->penyelenggara)->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                $query->where('tb_registrasi.siklus','=','12')
                                      ->orwhere('tb_registrasi.siklus','=',$siklus);
                            })
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->jumlah = $jumlah;
            if ($siklus == "1") {
                $beluminput1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.pemeriksaan','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->beluminput1 = $beluminput1;
                $input1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.status_data1','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->input1 = $input1;
                $sudahinput1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.status_data1','=', '2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->sudahinput1 = $sudahinput1;
            }else{
                $beluminput1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.status_data2','=', NULL)
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->beluminput1 = $beluminput1;
                $input1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.status_data2','=', '1')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->input1 = $input1;
                $sudahinput1 = DB::table('tb_registrasi')
                                ->where('tb_registrasi.bidang','=',$r->id)
                                ->where('tb_registrasi.status','>=','2')
                                ->where(function ($query) use ($siklus) {
                                    $query->where('tb_registrasi.siklus','=','12')
                                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                                })
                                ->where('tb_registrasi.status_data2','=', '2')
                                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                                ->count();
                $r->sudahinput1 = $sudahinput1;
            }
            $sudahevaluasi = DB::table('tb_kesimpulan_evaluasi')
                            ->where(DB::raw('YEAR(tb_kesimpulan_evaluasi.created_at)'), '=' , $id)
                            ->where('siklus', $siklus)
                            ->where(function ($query){
                                if (Auth::user()->penyelenggara == 6) {
                                    $query->where('type', '=', 'Anti Hiv');
                                }elseif(Auth::user()->penyelenggara == 7){
                                    $query->where('type', '=', 'Syphilis');
                                }elseif(Auth::user()->penyelenggara == 8){
                                    $query->where('type', '=', 'HBsAg');
                                }elseif(Auth::user()->penyelenggara == 9){
                                    $query->where('type', '=', 'ANTI-HCV');
                                }
                            })
                            ->count();
            $r->sudahevaluasi = $sudahevaluasi;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekaptransaksi(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('x');
        $data = DB::table('sub_bidang')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->select('sub_bidang.*', 'tb_bidang.bidang')
                ->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->where(function ($query) use ($siklus) {
                                $query->where('tb_registrasi.siklus','=','12')
                                      ->orwhere('tb_registrasi.siklus','=',$siklus);
                            })
                            ->select(DB::raw('sum(case 
                                    when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                    when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                end) as jumlah_tarif'))
                            ->get();
            $r->jumlah = $jumlah;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekaptransaksiexcel(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $siklus = $request->siklus;
        $data = DB::table('sub_bidang')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->select('sub_bidang.*', 'tb_bidang.bidang')
                ->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->where(function ($query) use ($siklus) {
                                $query->where('tb_registrasi.siklus','=','12')
                                      ->orwhere('tb_registrasi.siklus','=',$siklus);
                            })
                            ->select(DB::raw('sum(case 
                                    when tb_registrasi.siklus != 12 && sub_bidang.siklus != 12 then sub_bidang.tarif / 2
                                    when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                end) as jumlah_tarif'))
                            ->get();
            $r->jumlah = $jumlah;
        }
        // dd($data);
        if ($request->export == "Export Excel") {
            Excel::create('Rekap Transaksi Berdasarkan Parameter', function($excel) use ($data, $tahun) {
                $excel->sheet('Rekap', function($sheet) use ($data, $tahun) {
                    $sheet->loadView('laporan.rekap.excel.transaksi_parameter', array('data'=>$data, 'tahun'=>$tahun) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('laporan.rekap.excel.transaksi_parameter', compact('data', 'tahun'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Rekap Transaksi Berdasarkan Parameter.pdf');
        }
    }

    public function grafiktransaksit(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::table('sub_bidang')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->select('sub_bidang.*', 'tb_bidang.bidang')
                ->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->first();
            $r->jumlah = $jumlah->jumlah_tarif;
        }

        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->jumlah;
            array_push($rows, $row);
        }

        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function grafiktransaksip(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::table('sub_bidang')
                ->join('tb_bidang','sub_bidang.id_bidang','=','tb_bidang.id')
                ->select('sub_bidang.*', 'tb_bidang.bidang')
                ->get();
        foreach($data as $skey => $r)
        {
            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->count();
            $r->peserta = $peserta;
        }

        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->peserta;
            array_push($rows, $row);
        }

        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function grafikmalaria(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::select("SELECT
                            	A.*,
                            	(SELECT count(kesimpulan) FROM tb_evaluasi_malaria WHERE kesimpulan = A.name) as y
                            FROM
                            	( SELECT kesimpulan as name FROM tb_evaluasi_malaria WHERE kesimpulan != '') A");

        return($data);
        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function grafiktc(\Illuminate\Http\Request $request, $id)
    {
        $tahun = $id;
        $siklus = $request->get('x');

        $sql = "SELECT
                    *, nilaina as name,
                    count(nilaina) AS y
                FROM
                    (
                        SELECT
                            tb_evaluasi_tc.*, (
                                CASE SUM(nilai)
                                WHEN '9.99' THEN
                                    '10'
                                ELSE
                                    SUM(nilai)
                                END
                            ) AS nilaina
                        FROM
                            `tb_evaluasi_tc`
                        INNER JOIN `tb_registrasi` ON `tb_registrasi`.`id` = `tb_evaluasi_tc`.`id_registrasi`
                        WHERE
                            `tb_evaluasi_tc`.`siklus` = '".$siklus."' AND tb_evaluasi_tc.tahun = '".$tahun."' 
                        GROUP BY
                            id_registrasi
                    ) tot
                GROUP BY
                    nilaina
                ORDER BY
                    count(nilaina) DESC
                "
                ;

        $data = DB::table(DB::raw("($sql) zone"))->select('*')->get();
        // dd($data);
        return($data);
        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function grafikpembayaran(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::table('pembayaran')->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('tb_registrasi.id_pembayaran','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->groupBy('tb_registrasi.id_pembayaran')
                            ->count(DB::raw('tb_registrasi.id_pembayaran'));
            $r->jumlah = $jumlah;
        }
        // dd($data);
        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->type;
            $row['y'] = $r->jumlah;
            array_push($rows, $row);
        }
        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function rekappesertaparameter(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('x');
        $data = DB::table('sub_bidang')->get();
        foreach($data as $skey => $r)
        {
            $blk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '1')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->blk = $blk;
            $bblk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->bblk = $bblk;
            $rspem = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '3')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->rspem = $rspem;
            $pus = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '4')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->pus = $pus;
            $labkes = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '5')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->labkes = $labkes;
            $rsswa = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '6')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->rsswa = $rsswa;
            $labkl = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '7')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->labkl = $labkl;
            $utdrs = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '8')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->utdrs = $utdrs;
            $utdpmi = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '9')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->utdpmi = $utdpmi;
            $rstni = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '10')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->count();
            $r->rstni = $rstni;
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekappesertaparameterexcel(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $siklus = $request->siklus;
        $data = DB::table('sub_bidang')->get();
        foreach($data as $skey => $r)
        {
            $blk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '1')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->blk = $blk;
            $bblk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->bblk = $bblk;
            $rspem = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '3')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->rspem = $rspem;
            $pus = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '4')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->pus = $pus;
            $labkes = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '5')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->labkes = $labkes;
            $rsswa = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '6')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->rsswa = $rsswa;
            $labkl = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '7')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->labkl = $labkl;
            $utdrs = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '8')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->utdrs = $utdrs;
            $utdpmi = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '9')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->utdpmi = $utdpmi;
            $rstni = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(function ($query) use ($siklus) {
                                if ($siklus == 1) {
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12);
                                }
                                if ($siklus == 2) {
                                    $query->where('tb_registrasi.siklus', '=', 2)->orWhere('tb_registrasi.siklus', '=', 12);
                                }else{
                                    $query->where('tb_registrasi.siklus', '=', 1)->orWhere('tb_registrasi.siklus', '=', 12)->orWhere('tb_registrasi.siklus', '=', 2);
                                }
                            })
                            ->where('perusahaan.pemerintah', '=', '10')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $request->tahun)
                            ->count();
            $r->rstni = $rstni;
        }
            // dd($data);
        if ($request->export == "Export Excel") {
            Excel::create('Rekapitulasi Peserta PME Berdasarkan Jenis Instansi dan Parameter', function($excel) use ($data, $tahun) {
                $excel->sheet('Rekap', function($sheet) use ($data, $tahun) {
                    $sheet->loadView('laporan.rekap.excel.peserta_instalasi', array('data'=>$data, 'tahun'=>$tahun) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('laporan.rekap.excel.peserta_instalasi', compact('data', 'tahun'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Rekapitulasi Peserta PME Berdasarkan Jenis Instansi dan Parameter.pdf');
        }
    }


    public function rekappesertaprovinsi($id, \Illuminate\Http\Request $request)
    {
        $siklus = $request->get('x');
        $sub_bidang = DB::table('sub_bidang')->where('id','!=',10)->get();
        if($siklus == 1){
            $siklus = "(tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12)";
        }elseif ($siklus == 2) {
            $siklus = "(tb_registrasi.siklus = 2 OR tb_registrasi.siklus = 12)";
        }else{
            $siklus = "(tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2)";
        }
        $subSql = "(
            SELECT COUNT(DISTINCT tb_registrasi.perusahaan_id ) FROM tb_registrasi 
            INNER JOIN perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
            WHERE perusahaan.provinsi = provinces.id
            AND 
               YEAR(tb_registrasi.created_at) = ".$id."
            AND 
                tb_registrasi.status >= 2
            AND
            ".$siklus."
        ) as COUNT_PESERTA,
        
        (
            SELECT COUNT(*) FROM tb_registrasi 
            INNER JOIN perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
            WHERE perusahaan.provinsi = provinces.id
            AND 
               YEAR(tb_registrasi.created_at) = ".$id."
            AND 
                tb_registrasi.status >= 2
            AND
            ".$siklus."
        ) as COUNT_PARAMETER,";
        $count = count($sub_bidang) -1;
        foreach($sub_bidang as $key => $r){
            $where = "tb_registrasi.bidang = ".$r->id;
            if($r->id >= 6){
                $where = "(tb_registrasi.bidang = ".$r->id." OR tb_registrasi.bidang = 10)";
            }
            $subSql .= "(
                SELECT COUNT(*) FROM tb_registrasi 
                INNER JOIN perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
                WHERE perusahaan.provinsi = provinces.id
                AND
                ".$where."
                AND
                ".$siklus."
                AND 
                   YEAR(tb_registrasi.created_at) = ".$id."
                AND 
                    tb_registrasi.status >= 2
            ) as COUNT_".$r->alias;
            if($count != $key){
                $subSql .= ", ";
            }
        }
        $data = DB::table('provinces')
        ->select(
            'provinces.name',
            DB::raw($subSql)    
        )
        ->get();
        
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekapinstansiprovinsi(\Illuminate\Http\Request $request, $id)
    {
        $data = DB::table('provinces')->get();
        $sub_bidang = DB::table('sub_bidang')->where('id','!=',10)->get();
        $badan = DB::table('badan_usaha')->get();
        $siklus = $request->get('x');
        if($siklus == 1){
            $siklus = "(tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12)";
        }elseif ($siklus == 2) {
            $siklus = "(tb_registrasi.siklus = 2 OR tb_registrasi.siklus = 12)";
        }else{
            $siklus = "(tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2)";
        }
        $subSql = "(
            SELECT COUNT(DISTINCT tb_registrasi.perusahaan_id ) FROM tb_registrasi 
            INNER JOIN perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
            WHERE perusahaan.provinsi = provinces.id
            AND 
               YEAR(tb_registrasi.created_at) = ".$id."
            AND 
                tb_registrasi.status >= 2
            AND
            ".$siklus."
        ) as COUNT_PESERTA,";
        $count = count($badan) -1;
        foreach($badan as $key => $r){
            $where = "perusahaan.pemerintah = ".$r->id;
            $subSql .= "(
                SELECT COUNT(DISTINCT tb_registrasi.perusahaan_id) FROM tb_registrasi 
                INNER JOIN perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
                WHERE perusahaan.provinsi = provinces.id
                AND
                ".$where."
                AND 
                   YEAR(tb_registrasi.created_at) = ".$id."
                AND 
                    tb_registrasi.status >= 2
                AND
                ".$siklus."
            ) as COUNT_".$r->id;
            if($count != $key){
                $subSql .= ", ";
            }
        }
        $data = DB::table('provinces')
        ->select(
            'provinces.name',
            DB::raw($subSql)    
        )
        ->get();
        
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekappesertaprovinsiexcel(\Illuminate\Http\Request $request)
    {
        $siklus = $request->siklus;
        $tahun = $request->tahun;
        $data = DB::table('provinces')->get();
        $sub_bidang = DB::table('sub_bidang')->where('id','!=',10)->get();
        if($siklus == 1){
            $siklus = "(tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12)";
        }elseif ($siklus == 2) {
            $siklus = "(tb_registrasi.siklus = 2 OR tb_registrasi.siklus = 12)";
        }else{
            $siklus = "(tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2)";
        }
        $subSql = "(
            SELECT COUNT(DISTINCT tb_registrasi.perusahaan_id ) FROM tb_registrasi 
            INNER JOIN perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
            WHERE perusahaan.provinsi = provinces.id
            AND 
               YEAR(tb_registrasi.created_at) = ".$tahun."
            AND 
                tb_registrasi.status >= 2
            AND
            ".$siklus."
        ) as COUNT_PESERTA,
        
        (
            SELECT COUNT(*) FROM tb_registrasi 
            INNER JOIN perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
            WHERE perusahaan.provinsi = provinces.id
            AND 
               YEAR(tb_registrasi.created_at) = ".$tahun."
            AND 
                tb_registrasi.status >= 2
            AND
            ".$siklus."
        ) as COUNT_PARAMETER,";
        $count = count($sub_bidang) -1;
        foreach($sub_bidang as $key => $r){
            $where = "tb_registrasi.bidang = ".$r->id;
            if($r->id >= 6){
                $where = "(tb_registrasi.bidang = ".$r->id." OR tb_registrasi.bidang = 10)";
            }
            $subSql .= "(
                SELECT COUNT(*) FROM tb_registrasi 
                INNER JOIN perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
                WHERE perusahaan.provinsi = provinces.id
                AND
                ".$where."
                AND 
                   YEAR(tb_registrasi.created_at) = ".$tahun."
                AND 
                    tb_registrasi.status >= 2
                AND
                ".$siklus."
            ) as COUNT_".$r->alias;
            if($count != $key){
                $subSql .= ", ";
            }
        }
        $data = DB::table('provinces')
        ->select(
            'provinces.name',
            DB::raw($subSql)    
        )
        ->get();
        // dd($request->export);
        if ($request->export == "Export Excel") {
            Excel::create('Rekap Peserta Berdasarkan Provinsi', function($excel) use ($data, $tahun,$request) {
                $excel->sheet('Rekap', function($sheet) use ($data, $tahun,$request) {
                    $sheet->loadView('laporan.rekap.excel.peserta_provinsi', array('data'=>$data, 'tahun'=>$tahun,$request) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('laporan.rekap.excel.peserta_provinsi', compact('data', 'tahun','request'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Rekap Peserta Berdasarkan Provinsi.pdf');
        }
    }

    public function rekapinstansiprovinsiexcel(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $data = DB::table('provinces')->get();
        $sub_bidang = DB::table('sub_bidang')->where('id','!=',10)->get();
        $badan = DB::table('badan_usaha')->get();
        $siklus = $request->siklus;
        if($siklus == 1){
            $siklus = "(tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12)";
        }elseif ($siklus == 2) {
            $siklus = "(tb_registrasi.siklus = 2 OR tb_registrasi.siklus = 12)";
        }else{
            $siklus = "(tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2)";
        }
        $subSql = "(
            SELECT COUNT(DISTINCT tb_registrasi.perusahaan_id ) FROM tb_registrasi 
            INNER JOIN perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
            WHERE perusahaan.provinsi = provinces.id
            AND 
               YEAR(tb_registrasi.created_at) = ".$tahun."
            AND 
                tb_registrasi.status >= 2
            AND
            ".$siklus."
        ) as COUNT_PESERTA,";
        $count = count($badan) -1;
        // dd($count);
        foreach($badan as $key => $r){
            $where = "perusahaan.pemerintah = ".$r->id;
            $subSql .= "(
                SELECT COUNT(DISTINCT tb_registrasi.perusahaan_id) FROM tb_registrasi 
                INNER JOIN perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
                WHERE perusahaan.provinsi = provinces.id
                AND
                ".$where."
                AND 
                   YEAR(tb_registrasi.created_at) = ".$tahun."
                AND 
                    tb_registrasi.status >= 2
                AND
                ".$siklus."
            ) as COUNT_".$r->id;
            if($count != $key){
                $subSql .= ", ";
            }
        }
        $data = DB::table('provinces')
        ->select(
            'provinces.name',
            DB::raw($subSql)    
        )
        ->get();
        if ($request->export == "Export Excel") {
            Excel::create('Rekap Peserta Berdasarkan Provinsi', function($excel) use ($data, $tahun,$request) {
                $excel->sheet('Rekap', function($sheet) use ($data, $tahun,$request) {
                    $sheet->loadView('laporan.rekap.excel.peserta_provinsi', array('data'=>$data, 'tahun'=>$tahun,$request) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('laporan.rekap.excel.peserta_provinsi', compact('data', 'tahun','request'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Rekap Peserta Berdasarkan Provinsi.pdf');
        }
    }

    public function rekapinstansi(\Illuminate\Http\Request $request, $id)
    {
        $bidang = $request->get('x');
        $data = DB::table('badan_usaha')
            ->select(
                'badan_usaha.badan_usaha',
                DB::raw('
                    (
                        SELECT COUNT(DISTINCT tb_registrasi.perusahaan_id) FROM tb_registrasi
                        INNER JOIN 
                        perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
                        WHERE
                        perusahaan.pemerintah = badan_usaha.id 
                        AND tb_registrasi.status >= 2
                        AND YEAR(tb_registrasi.created_at) = '.$id.'
                    ) as peserta
                '),
                DB::raw('
                (
                    SELECT IFNULL(SUM(tb_bayar.tarif),0) FROM tb_registrasi
                    INNER JOIN 
                    perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
                    INNER JOIN 
                    tb_bayar ON tb_bayar.id_registrasi = tb_registrasi.id
                    WHERE
                    perusahaan.pemerintah = badan_usaha.id 
                    AND tb_registrasi.status >= 2
                    AND YEAR(tb_registrasi.created_at) = '.$id.'
                    AND tb_registrasi.bidang = '.$bidang.'
                ) as jumlah
                ')
            );
        
        if (Auth::user()->badan_usaha == '9') {
            $data->where('badan_usaha.id','==',9);
        }else{
            $data->where('badan_usaha.id','!=',9);
        }
        $data = $data->get();
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekapinstansiexcel(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $bidang = $request->bidang;
        $data = DB::table('badan_usaha')
            ->select(
                'badan_usaha.badan_usaha',
                DB::raw('
                    (
                        SELECT COUNT(DISTINCT tb_registrasi.perusahaan_id) FROM tb_registrasi
                        INNER JOIN 
                        perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
                        WHERE
                        perusahaan.pemerintah = badan_usaha.id 
                        AND tb_registrasi.status >= 2
                        AND YEAR(tb_registrasi.created_at) = '.$tahun.'
                    ) as peserta
                '),
                DB::raw('
                (
                    SELECT IFNULL(SUM(tb_bayar.tarif),0) FROM tb_registrasi
                    INNER JOIN 
                    perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
                    INNER JOIN 
                    tb_bayar ON tb_bayar.id_registrasi = tb_registrasi.id
                    WHERE
                    perusahaan.pemerintah = badan_usaha.id 
                    AND tb_registrasi.status >= 2
                    AND YEAR(tb_registrasi.created_at) = '.$tahun.'
                    AND tb_registrasi.bidang = '.$bidang.'
                ) as jumlah
                ')
            );

        
        if (Auth::user()->badan_usaha == '9') {
            $data->where('badan_usaha.id','==',9);
        }else{
            $data->where('badan_usaha.id','!=',9);
        }
        $data = $data->get();
            // dd($data);
        if ($request->export == "Export Excel") {
            Excel::create('Rekapitulasi Peserta PME Berdasarkan Jenis Instansi', function($excel) use ($data, $tahun,$request) {
                $excel->sheet('Rekap', function($sheet) use ($data, $tahun,$request) {
                    $sheet->loadView('laporan.rekap.excel.peserta_instansi', array('data'=>$data, 'tahun'=>$tahun, 'request'=>$request) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('laporan.rekap.excel.peserta_instansi', compact('data', 'tahun','request'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Rekapitulasi Peserta PME Berdasarkan Jenis Instansi.pdf');
        }
    }

    public function grafikinstansip(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $penyelenggara = Auth::user()->penyelenggara;
        $data = DB::table('badan_usaha')->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($jumlah[0]->jumlah_tarif == NULL) {
                $r->jumlah = 0;
            }else{
                $r->jumlah = $jumlah[0]->jumlah_tarif;
            }

            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->count();
            $r->peserta = $peserta;
        }
        // dd($data);
        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->peserta;
            array_push($rows, $row);
        }

        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }
    // filter per penyelenggara
    public function grafikinstansifilter(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $penyelenggara = Auth::user()->penyelenggara;
        $subBidang = DB::table('sub_bidang')->find($penyelenggara);
        $data = DB::table('badan_usaha')->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang',$penyelenggara)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($jumlah[0]->jumlah_tarif == NULL) {
                $r->jumlah = 0;
            }else{
                $r->jumlah = $jumlah[0]->jumlah_tarif;
            }

            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('tb_registrasi.bidang',$penyelenggara)
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->count();
            $r->peserta = $peserta;
        }
        // dd($data);
        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->peserta;
            array_push($rows, $row);
        }

        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }


    public function grafikinstansij(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $data = DB::table('badan_usaha')->get();
        foreach($data as $skey => $r)
        {
            $jumlah = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($jumlah[0]->jumlah_tarif == NULL) {
                $r->jumlah = 0;
            }else{
                $r->jumlah = $jumlah[0]->jumlah_tarif;
            }

            $peserta = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('perusahaan.pemerintah','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                            ->count();
            $r->peserta = $peserta;
        }
        // dd($data);
        $rows = array();
        foreach ($data as $key => $r) {
            $row['name'] = $r->alias;
            $row['y'] = $r->jumlah;
            array_push($rows, $row);
        }

        return response ($rows);
        // return response()->json(['Hasil'=>$data]);
    }

    public function rekappesertatransaksi($id)
    {
        $data = DB::table('sub_bidang')->get();
        foreach($data as $skey => $r)
        {
            $blk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '1')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($blk[0]->jumlah_tarif == NULL) {
                $r->blk = 0;
            }else{
                $r->blk = $blk[0]->jumlah_tarif;
            }

            $bblk = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '2')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($bblk[0]->jumlah_tarif == NULL) {
                $r->bblk = 0;
            }else{
                $r->bblk = $bblk[0]->jumlah_tarif;
            }

            $rspem = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '3')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($rspem[0]->jumlah_tarif == NULL) {
                $r->rspem = 0;
            }else{
                $r->rspem = $rspem[0]->jumlah_tarif;
            }
            $pus = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '4')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($pus[0]->jumlah_tarif == NULL) {
                $r->pus = 0;
            }else{
                $r->pus = $pus[0]->jumlah_tarif;
            }
            $labkes = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '5')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($labkes[0]->jumlah_tarif == NULL) {
                $r->labkes = 0;
            }else{
                $r->labkes = $labkes[0]->jumlah_tarif;
            }
            $rsswa = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '6')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($rsswa[0]->jumlah_tarif == NULL) {
                $r->rsswa = 0;
            }else{
                $r->rsswa = $rsswa[0]->jumlah_tarif;
            }
            $labkl = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '7')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($labkl[0]->jumlah_tarif == NULL) {
                $r->labkl = 0;
            }else{
                $r->labkl = $labkl[0]->jumlah_tarif;
            }
            $utdrs = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '8')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($utdrs[0]->jumlah_tarif == NULL) {
                $r->utdrs = 0;
            }else{
                $r->utdrs = $utdrs[0]->jumlah_tarif;
            }
            $utdpmi = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '9')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($utdpmi[0]->jumlah_tarif == NULL) {
                $r->utdpmi = 0;
            }else{
                $r->utdpmi = $utdpmi[0]->jumlah_tarif;
            }
            $rstni = DB::table('tb_registrasi')
                            ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->leftjoin('badan_usaha','perusahaan.pemerintah','=','badan_usaha.id')
                            ->join('sub_bidang','tb_registrasi.bidang','=','sub_bidang.id')
                            ->where('tb_registrasi.bidang','=',$r->id)
                            ->where('tb_registrasi.status','>=','2')
                            ->where('perusahaan.pemerintah', '=', '10')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $id)
                            ->select(DB::raw('sum(sub_bidang.tarif) as jumlah_tarif'))
                            ->get();
            if ($rstni[0]->jumlah_tarif == NULL) {
                $r->rstni = 0;
            }else{
                $r->rstni = $rstni[0]->jumlah_tarif;
            }
        }
        // dd($data);
        return response()->json(['Hasil'=>$data]);
    }

    public function rekappesertatransaksiexcel(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $siklus = $request->siklus;
        if($siklus == 1){
            $siklus = "(tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12)";
        }elseif ($siklus == 2) {
            $siklus = "(tb_registrasi.siklus = 2 OR tb_registrasi.siklus = 12)";
        }else{
            $siklus = "(tb_registrasi.siklus = 1 OR tb_registrasi.siklus = 12 OR tb_registrasi.siklus = 2)";
        }
        $badan_usaha = DB::table('badan_usaha')->get();
        $sql = "";
        $count = count($badan_usaha) -1;
        foreach($badan_usaha as $key => $r){
            $sql .= "(
                SELECT IFNULL(SUM(tb_bayar.tarif),0) FROM tb_bayar
                INNER JOIN tb_registrasi ON tb_registrasi.id = tb_bayar.id_registrasi
                INNER JOIN perusahaan ON perusahaan.id = tb_registrasi.perusahaan_id
                WHERE tb_registrasi.bidang = sub_bidang.id
                AND perusahaan.pemerintah = ".$r->id."
                AND YEAR(tb_registrasi.created_at) = ".$tahun."
                AND ".$siklus."
            ) as sum_".$r->id;
            if($count != $key){
                $sql .= ", ";
            }
        }
        $data = DB::table('sub_bidang')
        ->select(
            'sub_bidang.alias',
            DB::raw($sql)
        )
        ->get();
        
            // dd($data);
        if ($request->export == "Export Excel") {
            Excel::create('Rekapitulasi Peserta PME Berdasarkan Jenis Instansi dan Parameter', function($excel) use ($data, $tahun,$request,$badan_usaha) {
                $excel->sheet('Rekap', function($sheet) use ($data, $tahun,$request,$badan_usaha) {
                    $sheet->loadView('laporan.rekap.excel.tarif_parameter', array('data'=>$data, 'tahun'=>$tahun, 'request'=>$request, 'badan_usaha'=>$badan_usaha) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('laporan.rekap.excel.tarif_parameter', compact('data', 'tahun','request','badan_usaha'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Rekapitulasi Peserta PME Berdasarkan Jenis Instansi dan Parameter.pdf');
        }
    }

    public function seluruh(){
      $bidang = DB::table('sub_bidang')->get();

      return view('laporan.rekap.seluruh_peserta', compact('bidang'));
    }

    public function seluruhcetak(\Illuminate\Http\Request $request){
      $tahun = $request->tahun;
      $bidangs = $request->bidang;
      $bidang = DB::table('sub_bidang')->where('id',$bidangs)->first();
      // return $bidang;
      //hematologi


      return view('laporan.rekap.excel.seluruh_peserta', compact('bidang','tahun','bidang'));
    }
}
