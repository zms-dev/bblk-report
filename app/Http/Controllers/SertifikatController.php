<?php

namespace App\Http\Controllers;
use DB;
use Request;
use Auth;
use Input;
use PDF;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\masterimunologi as Masterimunologi;
use App\bahanimunologi as Bahanimunologi;
use App\reagenimunologi as Reagenimunologi;
use App\hpimunologi as Hpimunologi;
use App\register as Register;
use App\strategi as Strategi;
use App\kesimpulan as Kesimpulan;
use App\CatatanImun;
use Redirect;
use Validator;
use Session;
use mikehaertl\wkhtmlto\Pdf as WKHTMLPDF;
use mikehaertl\wkhtmlto\Image;
use Illuminate\Support\Facades\Storage;
use App\SertfikatInsert;
use ZipArchive;

class SertifikatController extends Controller
{
    public function cetakall(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        
        // $q1 = DB::table('tb_registrasi')->where('perusahaan_id', $id)->first();
        // $q2 = new Carbon( $q1->created_at );
        // $tahun = $q2->year;
        $tahun = '2018';

        $bidang = DB::table('sub_bidang')->join('tb_bidang','tb_bidang.id','=','sub_bidang.id_bidang')->where('sub_bidang.id','=',$id)->first();
        $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
        ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
        ->where('tb_registrasi.bidang','=',$id)
        // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
        ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'),'tb_registrasi.perusahaan_id','sub_bidang.alias')
        ->groupBy('tb_registrasi.perusahaan_id')
        // ->limit(5)
        ->get();
        $name = $bidang->alias;
        $url = $bidang->link;
        // dd($data);
        Storage::deleteDirectory('tmp_sertifikat'.DIRECTORY_SEPARATOR.$name);
        if(file_exists(public_path($name.'.zip'))){
            unlink(public_path($name.'.zip'));
        }

        $make = Storage::makeDirectory('tmp_sertifikat'.DIRECTORY_SEPARATOR.$name);
        $zip = new ZipArchive;
        $zip->open(public_path($name.'.zip'), ZipArchive::CREATE);
        $zip->addEmptyDir($name);
        $zip->close();

        return response()->json([
            'data' => $data,
            'bidang' => $bidang,
            'y' => $request->query('y'),
            'x' => $request->query('x'),
            'tanggal' => $request->query('tanggal')
        ]);
    }

    public function cetakhbsag(\Illuminate\Http\Request $request, $id){
    	$type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->t;
        $nomor = $request->n;
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;
        
        $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=','Imunologi')->where('siklus', $type)->where('tahun',$tahun)->first();
        // dd($tahun);
        if ($request->query('download') == "true") {
            $url = url('hasil-pemeriksaan/anti-hiv/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&t='.$request->query('t').'&n='.$request->query('n'));
            $image = new WKHTMLPDF($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLPDF', '/usr/local/bin/wkhtmltopdf'),
                'orientation' => 'landscape',
                'page-size' => 'A4',
                'margin-top' => '0cm',
                'margin-right' => '0cm',
                'margin-bottom' => '0cm',
                'margin-left' => '0cm',
                'dpi' => '100',
            ]);

            if(!$image->send('Sertifikat Imunologi.pdf')){
                dd($image->getError());
            }
        }else{

            $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->whereIn('tb_registrasi.bidang',array(6,7,8,9))
                ->where('tb_registrasi.perusahaan_id',$data1->perusahaan_id)
                // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter SEPARATOR ", ") as jenis_form'))
                ->groupBy('tb_registrasi.perusahaan_id')
                ->get();
            // dd($data);
            // if (count($kesimpulan) > 0) {
            \Debugbar::disable();
            return view('evaluasi.imunologi.sertifikat.hbsag', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal','nomor','sertifikat'));
        }        
    }

    public function bta(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        
        $q1 = DB::table('tb_registrasi')->where('perusahaan_id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;

        $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
            ->whereIn('tb_registrasi.bidang',array(4,5,10))
            ->where('tb_registrasi.perusahaan_id',$id)
            // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
            ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'))
            ->groupBy('tb_registrasi.perusahaan_id')
            ->get();
        // dd($data);
        // if (count($kesimpulan) > 0) {
        $pdf = PDF::loadview('evaluasi.imunologi.sertifikat.mikrologi', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'))
            ->setPaper('a4', 'landscape')
            ->setwarnings(false);
        return $pdf->stream('Sertifikat Mikroskopis.pdf');
    }

     public function bac(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $nomor = $request->nomor;
        $bidang = $request->get('bidang');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;
        
        if ($bidang == 4) {
            $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=', 'Mikrobiologi BTA')->where('siklus', $type)->where('tahun',$tahun)->first();
        }else if($bidang == 5){
            $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=', 'Mikrobiologi TC')->where('siklus', $type)->where('tahun',$tahun)->first();
        }
        // dd($sertifikat);
        if ($request->query('download') == "true") {
            $url = url('hasil-pemeriksaan/bta/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal').'&nomor='.$request->query('nomor').'&bidang='.$request->query('bidang'));
            $image = new WKHTMLPDF($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLPDF', '/usr/local/bin/wkhtmltopdf'),
                'orientation' => 'landscape',
                'page-size' => 'A4',
                'margin-top' => '0cm',
                'margin-right' => '0cm',
                'margin-bottom' => '0cm',
                'margin-left' => '0cm',
                'dpi' => '100',
            ]);
            if(!$image->send('Sertifikat Mikrobiologi.pdf')){
                dd($image->getError()); 
            }
        }else{
            $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('tb_registrasi.bidang', $bidang)
                ->where('tb_registrasi.id', $id)
                // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter SEPARATOR ", ") as jenis_form'))
                ->groupBy('tb_registrasi.perusahaan_id')
                ->get();
            // dd($data);
            // if (count($kesimpulan) > 0) {
            \Debugbar::disable();
            return view('evaluasi.imunologi.sertifikat.identifikasi', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal','nomor','sertifikat'));
            // }
        }
    }

    public function hem(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $nomor = $request->nomor;
        
        $q1 = DB::table('tb_registrasi')->where('perusahaan_id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;

        $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=','Hematologi')->where('siklus', $type)->where('tahun',$tahun)->first();
        // dd($sertifikat);
        if ($request->query('download') == "true") {
            $url = url('hasil-pemeriksaan/hematologi/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal').'&nomor='.$request->query('nomor'));
            $image = new WKHTMLPDF($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLPDF', '/usr/local/bin/wkhtmltopdf'),
                'orientation' => 'landscape',
                'page-size' => 'A4',
                'margin-top' => '0cm',
                'margin-right' => '0cm',
                'margin-bottom' => '0cm',
                'margin-left' => '0cm',
                'dpi' => '100',
            ]);

            if(!$image->send('Sertifikat Hematologi.pdf')){
                dd($image->getError());
            }
        }else{
            $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->whereIn('tb_registrasi.bidang',array(1))
                ->where('tb_registrasi.perusahaan_id',$id)
                // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->select('tb_registrasi.kode_lebpes','tb_registrasi.id as idr','perusahaan.id as idp','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'))
                ->groupBy('tb_registrasi.perusahaan_id')
                ->get();
            // dd($data);
            // if (count($kesimpulan) > 0) {
            \Debugbar::disable();
            return view('evaluasi.imunologi.sertifikat.hem', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal', 'nomor','sertifikat'));
        }
    }

    public function kimiaklinik(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $nomor = $request->nomor;
        
        $q1 = DB::table('tb_registrasi')->where('perusahaan_id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;

        $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=','Kimiaklinik')->where('siklus', $type)->where('tahun',$tahun)->first();

        if ($request->query('download') == "true") {
            $url = url('hasil-pemeriksaan/kimia-klinik/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal').'&nomor='.$request->query('nomor'));
            $image = new Image($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLPDF', '/usr/local/bin/wkhtmltopdf'),
                'orientation' => 'landscape',
                'page-size' => 'A4',
                'margin-top' => '0cm',
                'margin-right' => '0cm',
                'margin-bottom' => '0cm',
                'margin-left' => '0cm',
                'dpi' => '100',
            ]);

            if(!$image->send('Sertifikat Kimiaklinik Klinik.pdf')){
                dd($image->getError());
            }
        }else{
            $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->whereIn('tb_registrasi.bidang',array(2))
                ->where('tb_registrasi.perusahaan_id',$id)
                // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'))
                ->groupBy('tb_registrasi.perusahaan_id')
                ->get();
            // dd($data);
            // if (count($kesimpulan) > 0) {
            \Debugbar::disable();
            return view('evaluasi.imunologi.sertifikat.kimiaklnik', compact('data', 'perusahaan','sertifikat', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal', 'nomor'));
        }
        // dd($data);
        // if (count($kesimpulan) > 0) {
    }

    public function uri(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $nomor = $request->nomor;

        $q1 = DB::table('tb_registrasi')->where('perusahaan_id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;

        $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=','Urinalisa')->where('siklus', $type)->where('tahun',$tahun)->first();
        // dd($sertifikat);
        if ($request->query('download') == "true") {
            $url = url('hasil-pemeriksaan/urinalisasi/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal').'&nomor='.$request->query('nomor'));
            $image = new Image($url);
             $image->setOptions([
                'binary' => env('PATH_WKHTMLPDF', '/usr/local/bin/wkhtmltopdf'),
                'orientation' => 'landscape',
                'page-size' => 'A4',
                'margin-top' => '0cm',
                'margin-right' => '0cm',
                'margin-bottom' => '0cm',
                'margin-left' => '0cm',
                'dpi' => '100',
            ]);

            if(!$image->send('Sertifikat Urinalisa.pdf')){
                dd($image->getError());
            }       
        }else{
            $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->whereIn('tb_registrasi.bidang',array(3))
                ->where('tb_registrasi.perusahaan_id',$id)
                // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'))
                ->groupBy('tb_registrasi.perusahaan_id')
                ->get();
            // dd($data);
            // if (count($kesimpulan) > 0) {
            \Debugbar::disable();
            return view('evaluasi.imunologi.sertifikat.urin', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal', 'sertifikat','nomor'));
        }
    }

    public function kimkes(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $q1 = DB::table('tb_registrasi')->where('perusahaan_id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;

        $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
            ->whereIn('tb_registrasi.bidang',array(11,12))
            ->where('tb_registrasi.perusahaan_id',$id)
            // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
            ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'))
            ->groupBy('tb_registrasi.perusahaan_id')
            ->get();
        // if (count($kesimpulan) > 0) {
        $pdf = PDF::loadview('evaluasi.imunologi.sertifikat.kimkes', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal'))
            ->setPaper('a4', 'landscape')
            ->setwarnings(false);
        return $pdf->stream('Sertifikat Urinalisa.pdf');
    }

    public function createzip(\Illuminate\Http\Request $request){
        $bidang = $request->bidang;
        $name = $bidang['alias'];
        $url = $bidang['link'];

        return response()->json([
            'url' => url($name.'.zip'),
        ]);
    }

    public function cetakoneby(\Illuminate\Http\Request $request){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $r      = $request->r;
        
        $q1 = DB::table('tb_registrasi')->where('perusahaan_id', $r)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;

        $bidang = $request->bidang;
        $name = $bidang['alias'];
        $url = $bidang['link'];
        $no_lab = str_replace("/","-",$r['kode_lebpes']);
        $url = url($url.'/sertifikat/print/'.$r['perusahaan_id'].'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal'));
        $image = new Image($url);
        $image->setOptions([
            'binary' => env('PATH_WKHTMLIMAGE', '/usr/local/bin/wkhtmltoimage')
        ]);

        if(!$image->saveAS(storage_path('app'.DIRECTORY_SEPARATOR.'tmp_sertifikat'.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR.$no_lab.'-sertifikat.png'))){
            return response()->json([
                'code' => 50,
            ]);
        } else{
            $zip = new ZipArchive;
            $zip->open(public_path($name.'.zip'));
            $zip->addFile(storage_path('app'.DIRECTORY_SEPARATOR.'tmp_sertifikat'.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR.$no_lab.'-sertifikat.png'),$name.'/'.$no_lab.'-sertifikat.png');
            $zip->close();
            return response()->json([
                'code' => 200,
            ]);
        }
    }
}
