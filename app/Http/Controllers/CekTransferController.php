<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\register as Register;
use App\bayar as Bayar;
use App\sptjm as SPTJM;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;

class CekTransferController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('tahun');
        $siklus = $request->get('siklus');
        $data = DB::table('sub_bidang')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab', 'perusahaan.no_hp', 'tb_registrasi.siklus', 'tb_registrasi.file','tb_registrasi.created_at',
                        DB::raw('group_concat(concat(tb_registrasi.id,"-",sub_bidang.tarif) SEPARATOR \'|\') as id'),
                        DB::raw('group_concat(sub_bidang.id, tb_registrasi.siklus SEPARATOR \'|\') as bidang'),
                        DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                        DB::raw('sum(case 
                                    when tb_registrasi.siklus != 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif /2
                                    when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                end) as jumlah_tarif')
                    )
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->leftJoin('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where(function($query) use ($siklus){
                    $query->where('tb_registrasi.siklus', '=', '12')
                        ->orwhere('tb_registrasi.siklus', '=', $siklus);
                })
                ->where('tb_registrasi.status', '1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
                ->groupBy('perusahaan.id')
                ->get();
        // dd($data);
        return view('cek_transfer', compact('data','tahun','siklus'));
    }

    public function pks()
    {
        $pks = DB::table('tb_pks')->get();
        // dd($data);
        return view('pks.datapks', compact('pks'));
    }

    public function cek_kwitansi(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('tahun');
        $siklus = $request->get('siklus');
        // dd($tahun);
        $data = DB::table('sub_bidang')
            ->select('users.name', 'users.email', 'perusahaan.nama_lab','perusahaan.id', 'tb_registrasi.siklus', 'tb_registrasi.file','tb_registrasi.created_at',
                    DB::raw('group_concat(concat(sub_bidang.tarif) SEPARATOR \'|\') as total'),
                    DB::raw('group_concat(sub_bidang.id, tb_registrasi.siklus SEPARATOR \'|\') as bidang'),
                    DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                    DB::raw('sum(case 
                                when tb_registrasi.siklus != 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif /2
                                when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                            end) as jumlah_tarif'),
                    DB::raw('YEAR(tb_registrasi.created_at) as tahun') 
                )
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
            ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
            ->join('users', 'perusahaan.created_by', '=', 'users.id')
            ->where(function($query) use ($siklus){
                $query->where('tb_registrasi.siklus', '=', '12')
                    ->orwhere('tb_registrasi.siklus', '=', $siklus);
            })
            ->where('tb_registrasi.status','>=', '2')
            ->where('tb_registrasi.sptjm','=', NULL)
            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $tahun)
            ->groupBy('perusahaan.id')
            ->groupBy('tb_registrasi.siklus')
            ->groupBy(DB::raw('YEAR(tb_registrasi.created_at)'))
            ->orderBy('tb_registrasi.id', 'asc')
            ->get();
        // dd($data);
        foreach ($data as  $val) {
            $datan = DB::table('tb_bayar')
                ->join('tb_registrasi','tb_registrasi.id','=','tb_bayar.id_registrasi')
                ->select('tb_bayar.*')
                ->where('tb_registrasi.siklus',$val->siklus)
                ->where('tb_registrasi.created_at',$val->created_at)
                ->where('tb_registrasi.perusahaan_id',$val->id)
                ->first();
            $val->datan = $datan;
        }
        // dd($data);
        return view('cek_kwitansi', compact('data','datan','tahun','siklus'));
    }

    public function cetak_kwitansi(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $input['siklus'];
        $tahun = $input['tahun'];
        // dd($input);
        // $datan = DB::table('tb_bayar')->join('tb_registrasi','tb_registrasi.id','=','tb_bayar.id_registrasi')->where('tb_registrasi.perusahaan_id',$id)->get();


        $data = DB::table('sub_bidang')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab', 'tb_registrasi.kode_lebpes', 'tb_registrasi.file','tb_registrasi.created_at as registrasi_tahun',
                    DB::raw("(select distinct `created_at` from tb_bayar where tb_registrasi.id = tb_bayar.id_registrasi) `created_at`"),
                    DB::raw('group_concat(concat(case 
                                    when tb_registrasi.siklus != 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif /2
                                    when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                end),\'-\', sub_bidang.id SEPARATOR \'|\') as total'),
                    DB::raw('group_concat(sub_bidang.id, tb_registrasi.siklus SEPARATOR \'|\') as bidang'),
                    DB::raw('group_concat(tb_registrasi.bidang SEPARATOR \'|\') as id_bidang'),
                    DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                    DB::raw('group_concat(tb_registrasi.siklus SEPARATOR \'|\') as siklus'),
                    DB::raw('sum(case 
                                    when tb_registrasi.siklus != 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif /2
                                    when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                end) as jumlah_tarif'),
                    DB::raw('YEAR(tb_registrasi.created_at) as tahun') 
                )  
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->leftJoin('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where('tb_registrasi.status','>=', '2')
                ->where('perusahaan.id', $id)
                ->where('sptjm', NULL)
                ->where('tb_registrasi.siklus', $siklus)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->groupBy('perusahaan.id')
                // ->groupBy('tb_registrasi.siklus')
                ->groupBy(DB::raw('YEAR(tb_registrasi.created_at)'))
                ->groupBy('tb_registrasi.siklus')
                ->first();
                //  $datan = DB::table('tb_bayar')
                // ->join('tb_registrasi','tb_registrasi.id','=','tb_bayar.id_registrasi')
                // ->select('tb_bayar.*')
                // ->where('tb_registrasi.perusahaan_id',$id)->get();
                // dd($datan);
        $cek = DB::table('tb_bayar')
                ->join('tb_registrasi','tb_registrasi.id','=','tb_bayar.id_registrasi')
                ->select('tb_bayar.*')
                ->where('tb_registrasi.siklus',$data->siklus)
                ->where('tb_registrasi.created_at',$data->registrasi_tahun)
                ->where('tb_registrasi.perusahaan_id',$id)
                ->get();
                // dd($cek);
        // return view('cetak_hasil/cetak_kwitansi', compact('data', 'input'));
                // dd($input);
        if ($input['cetak'] == 'Cetak') {
            $pdf = PDF::loadview('cetak_hasil/cetak_kwitansi', compact('data','input'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Cetak Kwitansi.pdf');
        }else{
            for ($i=0; $i < count($cek) ; $i++) { 
                $datan = DB::table('tb_bayar')
                ->join('tb_registrasi','tb_registrasi.id','=','tb_bayar.id_registrasi')
                ->select('tb_bayar.*')
                ->where('tb_registrasi.siklus',$data->siklus)
                ->where('tb_registrasi.created_at',$data->registrasi_tahun)
                ->where('tb_registrasi.perusahaan_id',$id)
                ->update(["tb_bayar.invoice"=>$request->invoice,"tb_bayar.tanggal"=>$request->tanggal]);
            }
            $pdf = PDF::loadview('cetak_hasil/cetak_kwitansi', compact('data','input'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Cetak Kwitansi.pdf');
        }
    }

    public function kuitansi_pks(\Illuminate\Http\Request $request, $id)
    {
        $date = $request->get('tahun');
        $total = DB::table('sub_bidang')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab', 'tb_registrasi.siklus', 'tb_registrasi.kode_lebpes', 'tb_bayar.created_at', 'tb_registrasi.file',
                    DB::raw('group_concat(concat(sub_bidang.tarif) SEPARATOR \'|\') as total'),
                    DB::raw('group_concat(tb_bidang.bidang SEPARATOR \'|\') as bidang'),
                    DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                    DB::raw('sum(case 
                                    when tb_registrasi.siklus != 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif /2
                                    when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                end) as jumlah_tarif')
                )  
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->leftJoin('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('tb_bayar', 'tb_registrasi.id', '=', 'tb_bayar.id_registrasi')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where('tb_registrasi.sptjm', '!=', NULL)
                ->where('tb_registrasi.pks','=',$id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->first();

        $data = DB::table('sub_bidang')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab', 'tb_registrasi.siklus', 'tb_registrasi.kode_lebpes', 'tb_bayar.created_at', 'tb_registrasi.file',
                    DB::raw('group_concat(concat(sub_bidang.tarif) SEPARATOR \'|\') as total'),
                    DB::raw('group_concat(tb_bidang.bidang SEPARATOR \'|\') as bidang'),
                    DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                    DB::raw('sum(case 
                                    when tb_registrasi.siklus != 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif /2
                                    when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                end) as jumlah_tarif')
                )  
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->leftJoin('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('tb_bayar', 'tb_registrasi.id', '=', 'tb_bayar.id_registrasi')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where('tb_registrasi.sptjm', '!=', NULL)
                ->where('tb_registrasi.pks','=', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->groupBy('perusahaan.id')
                ->get();
        // dd($data);
        
        // return view('cetak_hasil/kwitansi_pks', compact('data', 'total'));
        $pdf = PDF::loadview('cetak_hasil/kwitansi_pks', compact('data', 'total'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('Cetak Kwitansi.pdf');
    }

    public function proses(\Illuminate\Http\Request $request){

        $bidang = $request->bidang;
        $jumlah_dipilih = count($bidang);
        $user = DB::table('users')->where('role', 5)->get();
        $userna = count($user);
        // dd($request->all());
        for($x=0;$x<$jumlah_dipilih;$x++){
            $datas = $request->all();
            $exna = explode('|', $bidang[$x]);
            // dd($exna);
            $ids = "|";
            for ($u=0; $u < count($exna) ; $u++) { 
                $idTarif = explode('-', $exna[$u]);
                $data['status'] = '2';
                $bayar['tarif'] = $idTarif[1];
                $bayar['id_registrasi'] = $idTarif[0];
                $bayar['created_by'] = Auth::user()->id;
                Register::where('id', $exna[$u])->update($data);
                Bayar::create($bayar);
                $ids .= ",".$idTarif[0];
            }
            $ids = str_replace("|,", "", $ids);
            $ids = str_replace("", "", $ids);
            // $idas = "'".$ids."'";
            $email = DB::table('users')
                    ->leftjoin('tb_registrasi', 'users.id', '=', 'tb_registrasi.created_by')
                    ->leftjoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                    ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                    ->select('tb_registrasi.id','users.email', 'tb_registrasi.siklus', DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'), 'perusahaan.nama_lab', DB::raw('group_concat(tb_registrasi.kode_lebpes SEPARATOR \'|\') as kode_lebpes'))
                    // ->whereIn(DB::raw('' ($ids)))
                    ->whereRaw("tb_registrasi.id in ($ids)")
                    ->groupBy('users.id')
                    ->get();
            // dd($email);
            $datu = [
                    'labklinik' => $email[0]->nama_lab,
                    'siklus' => $email[0]->siklus,
                    'kode_lebpes' => $email[0]->kode_lebpes,
                    'date' => date('Y'),
                    'parameter' => $email[0]->parameter
                ];
            // dd($email);
            Mail::send('email.cek_transfer', $datu, function ($mail) use ($email)
            {
              $mail->from('sipamela.bblkjkt@gmail.com', 'BBLK Jakarta Transfer '.$email[0]->nama_lab);
              $mail->to($email[0]->email);
              $mail->subject('Pembayaran PNPME');
            });

            $penyelenggara = DB::table('users')->where('role','=','6')->get();
            foreach ($penyelenggara as $key => $val) {
                Mail::send('email.penyelenggara', $datu, function ($mail) use ($email, $val)
                {
                  $mail->from('sipamela.bblkjkt@gmail.com', 'BBLK Jakarta '.$email[0]->nama_lab);
                  $mail->to($val->email);
                    // $mail->to('tengkufirmansyah2@gmail.com');
                  $mail->subject('Pembayaran PNPME');
                });
            }
        }
           
        return redirect()->back();
    }

    public function pembayaran(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('tahun');
        $siklus = $request->get('siklus');
        $pks = DB::table('tb_pks')->get();
        $data = DB::table('sub_bidang')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab', 'tb_registrasi.siklus', 'tb_registrasi.file','tb_registrasi.created_at',
                        DB::raw('group_concat(concat(tb_registrasi.id,"-",sub_bidang.tarif) SEPARATOR \'|\') as id'),
                        DB::raw('group_concat(sub_bidang.id, tb_registrasi.siklus SEPARATOR \'|\') as bidang'),
                        DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                        DB::raw('sum(case 
                                    when tb_registrasi.siklus != 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif /2
                                    when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                end) as jumlah_tarif')
                    )
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->leftJoin('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where(function($query) use ($siklus){
                    $query->where('tb_registrasi.siklus', '=', '12')
                        ->orwhere('tb_registrasi.siklus', '=', $siklus);
                })
                ->where('tb_registrasi.sptjm', '!=', NULL)
                ->where('tb_registrasi.pks', '=', NULL)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->groupBy('perusahaan.id')
                ->get();
        return view('pks.pembayaran', compact('data','pks','tahun','siklus'));
    }

    public function prosesp(\Illuminate\Http\Request $request){

        $bidang = $request->bidang;
        $pks = $request->pks;
        $jumlah_dipilih = count($bidang);
        $user = DB::table('users')->where('role', 5)->get();
        $userna = count($user);
        // dd($request->all());
        for($x=0;$x<$jumlah_dipilih;$x++){
            $datas = $request->all();
            $exna = explode('|', $bidang[$x]);
            // dd($exna);
            $ids = "|";
            for ($u=0; $u < count($exna) ; $u++) { 
                $idTarif = explode('-', $exna[$u]);
                $data['pks'] = $pks;
                Register::where('id', $exna[$u])->update($data);
                $ids .= ",".$idTarif[0];
            }
        }

        return redirect()->back();
    }

    public function updatepembayaran(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('tahun');
        $siklus = $request->get('siklus');
        $data = DB::table('sub_bidang')
                ->select('users.name', 'users.email', 'perusahaan.nama_lab', 'tb_registrasi.siklus', 'tb_registrasi.file','tb_registrasi.created_at',
                        DB::raw('group_concat(concat(tb_registrasi.id,"-",sub_bidang.tarif) SEPARATOR \'|\') as id'),
                        DB::raw('group_concat(tb_bidang.bidang, tb_registrasi.siklus SEPARATOR \'|\') as bidang'),
                        DB::raw('group_concat(sub_bidang.parameter SEPARATOR \'|\') as parameter'),
                        DB::raw('sum(case 
                                    when tb_registrasi.siklus != 12 && (sub_bidang.siklus != 12 OR sub_bidang.siklus is NULL) then sub_bidang.tarif /2
                                    when tb_registrasi.siklus != 12 && sub_bidang.siklus = 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus != 12 then sub_bidang.tarif
                                    when tb_registrasi.siklus = 12 && sub_bidang.siklus = 12 then sub_bidang.tarif * 2
                                end) as jumlah_tarif')
                    )
                ->leftJoin('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->leftJoin('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->leftJoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftJoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->where(function($query) use ($siklus){
                    $query->where('tb_registrasi.siklus', '=', '12')
                        ->orwhere('tb_registrasi.siklus', '=', $siklus);
                })
                ->where('tb_registrasi.sptjm', '!=', NULL)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->groupBy('perusahaan.id')
                ->get();
        return view('update_pembayaran', compact('data', 'tahun','siklus'));
    }

    public function updateprosesp(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $bidang = $request->bidang;
        $file = $request->file;
        $jumlah_dipilih = count($bidang);
        $user = DB::table('users')->where('role', 5)->get();
        $userna = count($user);
        $tahun = date('Y');
        $key = array_keys($input['bidang']);
        // dd($input);
        // dd($key);
        foreach ($key as $val) {
            $exna = explode('|', $bidang[$val]);
            // dd($exna);
            if(!empty($input['file'][$val])){
                $dest = public_path('asset/file/');
                $name = $input['nama_lab'][$val].'-'.$tahun.'-'.$input['siklus'][$val].'-'.Request::file('file')[$val]->getClientOriginalName();
                Request::file('file')[$val]->move($dest, $name);
            }else{
                $name = NULL;
            }
            $ids = "|";
            for ($u=0; $u < count($exna) ; $u++) {
                $data['status'] = '1';
                $data['id_pembayaran'] = '1';
                $data['file'] = $name;
                $data['sptjm'] = NULL;
                Register::where('id', $exna[$u])->update($data);
            }
        }
        return redirect()->back();
    }
}
