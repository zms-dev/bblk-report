<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\masterimunologi as Masterimunologi;
use App\bahanimunologi as Bahanimunologi;
use App\reagenimunologi as Reagenimunologi;
use App\hpimunologi as Hpimunologi;
use App\register as Register;
use App\strategi as Strategi;
use App\kesimpulan as Kesimpulan;
use App\CatatanImun;
use Redirect;
use Validator;
use Session;


class RprController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {   
        $siklus = $request->get('y');
        $type = $request->get('x');
        $data = DB::table('parameter')->where('kategori', 'rpr-syphilis')->get();
        $reagen = DB::table('tb_reagen_imunologi')->where('kelompok', 'RPR')->get();

        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;
            
        return view('hasil_pemeriksaan/rpr-syp', compact('data', 'perusahaan', 'type','kodeperusahaan', 'reagen', 'siklus'));
    }

    public function view(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $date = date('Y');
        $data = DB::table('master_imunologi')
                ->join('bahan_imunologi', 'master_imunologi.id', '=', 'bahan_imunologi.id_master_imunologi')
                ->where('master_imunologi.jenis_form', 'rpr-syphilis')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->get();
        $data1 = DB::table('master_imunologi')
                ->join('reagen_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                ->leftjoin('tb_reagen_imunologi', 'reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                ->where('master_imunologi.jenis_form', 'rpr-syphilis')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->get();
        $data2 = DB::table('master_imunologi')
                ->join('hp_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                ->where('master_imunologi.jenis_form', 'rpr-syphilis')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->get();
        // dd($data);
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $kodeperusahaan = $register->kode_lebpes;

        // return view('cetak_hasil/anti-hcv', compact('data', 'perusahaan', 'data1', 'data2', 'kodeperusahaan'));
        $pdf = PDF::loadview('cetak_hasil/rpr-syp', compact('data', 'perusahaan', 'data1', 'data2', 'siklus', 'date', 'kodeperusahaan'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('RPR-Syphilis.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $input = $request->all();
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;
        $years = date('Y');

        $validasi = Masterimunologi::where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $years)->where('id_registrasi','=',$id)->where('siklus','=',$siklus)->where('master_imunologi.jenis_form','=','rpr-syphilis')->get();// dd($input);
        // dd($input);
        if (count($validasi)>0) {
            Session::flash('message', 'Hasil Imunologi Syphilis Sudah Ada!'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
        }else{
        $SaveMaster = new Masterimunologi;
        $SaveMaster->kode_peserta = $perusahaanID;
        $SaveMaster->petugas_pemeriksaan = $input['petugas_pemeriksaan'];
        $SaveMaster->keterangan = $input['keterangan'];
        $SaveMaster->kode_lab = $input['kode_peserta'];
        $SaveMaster->siklus = $siklus;
        $SaveMaster->hasil_pemeriksaan = '';
        $SaveMaster->jenis_form = 'rpr-syphilis';
        $SaveMaster->id_registrasi = $id;
        $SaveMaster->save();

        $SaveMasterId = $SaveMaster->id;
        $i = 0;

        foreach ($input['no_tabung'] as $tabung) {
            if($tabung != ''){
                $SaveBahan = new Bahanimunologi;
                $SaveBahan->tgl_diterima = $input['tgl_diterima'];
                $SaveBahan->tgl_diperiksa = $input['tgl_diperiksa'];
                $SaveBahan->no_tabung = $input['no_tabung'][$i];
                $SaveBahan->jenis = $input['jenis'][$i];
                $SaveBahan->lain = $input['lain'][$i];
                $SaveBahan->id_master_imunologi = $SaveMasterId;
                $SaveBahan->save();
            }
            $i++;
        }
        $i = 0;
        foreach ($input['nama_reagen'] as $reagen) {
            $SaveBahan = new Reagenimunologi;
            if (isset($input['metode'][$i])) {
                $SaveBahan->metode = $input['metode'][$i];
            }
            $SaveBahan->nama_reagen = $input['nama_reagen'][$i];
            $SaveBahan->reagen_lain = $input['reagen_lain'][$i];
            $SaveBahan->nama_produsen = $input['nama_produsen'][$i];
            $SaveBahan->nomor_lot = $input['nomor_lot'][$i];
            $SaveBahan->tgl_kadaluarsa = $input['tgl_kadaluarsa'][$i];
            $SaveBahan->id_master_imunologi = $SaveMasterId;
            $SaveBahan->save();
            $i++;
        }
        $i = 0;
        $not = 0;
        foreach ($input['kode_bahan_kontrol'] as $kode) {
            $not++;
            if($input['kode_bahan_kontrol'][$i] ){
                $SaveHp = new Hpimunologi;
                $SaveHp->tabung = $not;
                $SaveHp->kode_bahan_kontrol = $input['kode_bahan_kontrol'][$i];
                $SaveHp->interpretasi = $input['interpretasi1'][$i];
                $SaveHp->titer = $input['titer'][$i];
                $SaveHp->id_master_imunologi = $SaveMasterId;
                $SaveHp->save();
            }
            $i++;
        }
        
        if ($siklus == '1') {
            Register::where('id',$id)->update(['siklus_1'=>'done', 'rpr1'=>'done', 'status_datarpr1'=>'1']);
        }else{
            Register::where('id',$id)->update(['siklus_2'=>'done', 'rpr2'=>'done', 'status_datarpr2'=>'1']);
        }
        return redirect('hasil-pemeriksaan');
        }
    }

    public function edit(\Illuminate\Http\Request $request, $id){
        
        $siklus = $request->get('y');
        $data = DB::table('master_imunologi')
                ->join('bahan_imunologi', 'master_imunologi.id', '=', 'bahan_imunologi.id_master_imunologi')
                ->where('master_imunologi.jenis_form', 'rpr-syphilis')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->select('master_imunologi.*', 'bahan_imunologi.id as idimunologi', 'bahan_imunologi.lain', 'bahan_imunologi.id_master_imunologi', 'bahan_imunologi.no_tabung', 'bahan_imunologi.jenis', 'bahan_imunologi.tgl_diperiksa', 'bahan_imunologi.tgl_diterima')
                ->get();
        $data1 = DB::table('master_imunologi')
                ->join('reagen_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                ->leftjoin('tb_reagen_imunologi', 'reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                ->where('master_imunologi.jenis_form', 'rpr-syphilis')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->select('reagen_imunologi.*', 'tb_reagen_imunologi.id as Idreagen', 'tb_reagen_imunologi.reagen as namareagen')
                ->get();
        $data2 = DB::table('master_imunologi')
                ->join('hp_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                ->where('master_imunologi.jenis_form', 'rpr-syphilis')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->get();
        // dd($data);
        $reagen = DB::table('tb_reagen_imunologi')->where('kelompok', 'RPR')->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $kodeperusahaan = $register->kode_lebpes;

        return view('edit_hasil/rpr-syp', compact('data', 'perusahaan', 'data1', 'data2', 'siklus', 'kodeperusahaan','reagen'));
    }


    public function update(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $input = $request->all();
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;
        $MasterId = $request->idmaster;
        // dd($input);
        
        $SaveMaster['kode_peserta'] = $perusahaanID;
        $SaveMaster['kode_lab'] = $request->kode_peserta;
        $SaveMaster['petugas_pemeriksaan'] = $request->petugas_pemeriksaan;
        $SaveMaster['keterangan'] = $request->keterangan;
        $SaveMaster['siklus'] = $siklus;
        $SaveMaster['hasil_pemeriksaan'] = '';
        $SaveMaster['id_registrasi'] = $id;
        Masterimunologi::where('id_registrasi',$id)->where('id', $MasterId)->update($SaveMaster);
        
        $SaveMasterId = $MasterId;
        $i = 0;
        foreach ($input['no_tabung'] as $tabung) {
            if($tabung != ''){
                $SaveBahan['tgl_diterima'] = $request->tgl_diterima;
                $SaveBahan['tgl_diperiksa'] = $request->tgl_diperiksa;
                $SaveBahan['no_tabung'] = $request->no_tabung[$i];
                $SaveBahan['jenis'] = $request->jenis[$i];
                $SaveBahan['id_master_imunologi'] = $SaveMasterId;
                Bahanimunologi::where('id', $request->idbahan[$i])->update($SaveBahan);
            }
            $i++;
        }
        $i = 0;
        foreach ($input['nama_reagen'] as $reagen) {
            if (isset($input['metode'][$i])) {
                $SaveReagen['metode'] = $input['metode'][$i];
            }else{
                $SaveReagen['metode'] = NULL;
            }
            $SaveReagen['nama_reagen'] = $request->nama_reagen[$i];
            $SaveReagen['reagen_lain'] = $request->reagen_lain[$i];
            $SaveReagen['nama_produsen'] = $request->nama_produsen[$i];
            $SaveReagen['nomor_lot'] = $request->nomor_lot[$i];
            $SaveReagen['tgl_kadaluarsa'] = $request->tgl_kadaluarsa[$i];
            $SaveReagen['id_master_imunologi'] = $SaveMasterId;
            Reagenimunologi::where('id', $request->idreagen[$i])->update($SaveReagen);
            $i++;
        }
        $i = 0;
        $no = 0 ;
        foreach ($input['kode_bahan_kontrol'] as $kode) {
            $no++;
            if($input['kode_bahan_kontrol'][$i] ){
                $SaveHp['tabung'] = $no;
                $SaveHp['kode_bahan_kontrol'] = $request->kode_bahan_kontrol[$i];
                $SaveHp['interpretasi'] = $request->interpretasi1[$i];
                $SaveHp['titer'] = $request->titer[$i];
                $SaveHp['id_master_imunologi'] = $SaveMasterId;
                Hpimunologi::where('id', $request->idhp[$i])->update($SaveHp);
            }
            $i++;
        }
        if ($request->simpan == "Kirim") {
            if ($siklus == '1') {
                Register::where('id',$id)->update(['siklus_1'=>'done', 'rpr1'=>'done', 'status_datarpr1'=>'2']);
            }else{
                Register::where('id',$id)->update(['siklus_2'=>'done', 'rpr2'=>'done', 'status_datarpr2'=>'2']);
            }
        }
        return redirect('edit-hasil');
    }

    public function evaluasi(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $tahun = date('Y');
        $data2 = DB::table('master_imunologi')
                ->join('hp_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                ->join('tb_rujukan_imunologi', 'hp_imunologi.kode_bahan_kontrol', '=', 'tb_rujukan_imunologi.kode_bahan_uji')
                ->where('master_imunologi.jenis_form', 'rpr-syphilis')
                ->where('tb_rujukan_imunologi.parameter', 'rpr')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $type)
                ->where('tb_rujukan_imunologi.siklus', $type)
                ->where('tb_rujukan_imunologi.tahun', $tahun)
                ->get();
        $rujukan = DB::table('tb_rujukan_imunologi')
                ->where('siklus', $type)
                ->where('tahun', $tahun)
                ->where('parameter', 'rpr')
                ->orderBy('kode_bahan_uji', 'asc')
                ->get();
        // dd($rujukan);
        if (count($data2)) {
        // dd($data2);
            $reagen = DB::table('reagen_imunologi')
                        ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','tb_reagen_imunologi.id')
                        ->where('id_master_imunologi', $data2[0]->id_master_imunologi)
                        ->select('tb_reagen_imunologi.reagen','reagen_imunologi.metode','reagen_imunologi.reagen_lain')
                        ->get();
            $strategi = Strategi::where('id_master_imunologi', $data2[0]->id_master_imunologi)->where('siklus', $type)->get();
        }
        // dd($data2);
        $catatan = CatatanImun::where('id_registrasi', $id)->where('siklus', $type)->where('tahun', $tahun)->where('form','=','RPR')->first();
        $kesimpulan = Kesimpulan::where('id_registrasi', $id)->where('siklus', $type)->where('type','=','rpr-syphilis')->first();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $kodeperusahaan = $register->kode_lebpes;

        return view('evaluasi.imunologi.rpr.evaluasi', compact('perusahaan', 'data2', 'type','kodeperusahaan','kesimpulan', 'register','strategi', 'reagen', 'tahun', 'catatan', 'rujukan'));
    }

    public function insertevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('y');
        $input = $request->all();
        $date = date('Y');
        // dd($input);
        $i = 0;
        $a = 0;

        // foreach ($input['strategi'] as $strategi) {
        // $a++;
        //     if($strategi != ''){
        //         $SaveStrategi = new Strategi;
        //         $SaveStrategi->id_master_imunologi = $input['id_master_imunologi'];
        //         $SaveStrategi->nilai = $input['strategi'][$i];
        //         if ($input['strategi'][$i] == '0') {
        //             $SaveStrategi->kategori = 'Tidak Sesuai';
        //         }elseif ($input['strategi'][$i] == '5') {
        //             $SaveStrategi->kategori = 'Sesuai';
        //         }else{
        //             $SaveStrategi->kategori = '';
        //         }
        //         $SaveStrategi->kode_bahan = $input['kode_bahan'][$i];
        //         $SaveStrategi->tabung = $a;
        //         $SaveStrategi->siklus = $type;
        //         $SaveStrategi->save();
        //     }
        //     $i++;
        // }
        $SaveCatatan = new CatatanImun;
        $SaveCatatan->siklus = $type;
        $SaveCatatan->id_registrasi = $id;
        $SaveCatatan->tahun = $date;
        $SaveCatatan->catatan = $input['catatan'];
        $SaveCatatan->tanggal_ttd = $input['ttd'];
        $SaveCatatan->form = 'RPR';
        $SaveCatatan->save();

        $SaveKesimpulan = new Kesimpulan;
        $SaveKesimpulan->ketepatan = $input['ketepatan'];
        $SaveKesimpulan->type = 'rpr-syphilis';
        $SaveKesimpulan->id_master_imunologi = $input['id_master_imunologi'];
        $SaveKesimpulan->id_registrasi = $id;
        $SaveKesimpulan->siklus = $type;
        $SaveKesimpulan->save();

        return redirect('/hasil-pemeriksaan/rpr-syphilis/evaluasi/'.$id.'?y='.$type);
    }

    public function cetakevaluasi(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tahun = date('Y');
        $data2 = DB::table('master_imunologi')
                ->join('hp_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                ->join('tb_registrasi', 'master_imunologi.id_registrasi', '=', 'tb_registrasi.id')
                ->join('perusahaan','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->join('tb_rujukan_imunologi', 'hp_imunologi.kode_bahan_kontrol', '=', 'tb_rujukan_imunologi.kode_bahan_uji')
                ->where('master_imunologi.jenis_form', 'rpr-syphilis')
                ->where('tb_rujukan_imunologi.parameter', 'rpr')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $type)
                ->where('tb_rujukan_imunologi.siklus', $type)
                ->where('tb_rujukan_imunologi.tahun', $tahun)
                ->get();
        if (count($data2)) {
            $reagen = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','tb_reagen_imunologi.id')
                    ->where('id_master_imunologi', $data2[0]->id_master_imunologi)
                    ->select('tb_reagen_imunologi.reagen','reagen_imunologi.metode','reagen_imunologi.reagen_lain')
                    ->get();
        }
        $rujukan = DB::table('tb_rujukan_imunologi')
                ->where('siklus', $type)
                ->where('tahun', $tahun)
                ->where('parameter', 'rpr')
                ->orderBy('kode_bahan_uji', 'asc')
                ->get();
        // dd($data2);
        $kesimpulan = Kesimpulan::where('id_registrasi', $id)->where('siklus', $type)->where('type','=','rpr-syphilis')->first();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $kodeperusahaan = $register->kode_lebpes;
        $catatan = CatatanImun::where('id_registrasi', $id)->where('siklus', $type)->where('tahun', $tahun)->where('form','=','RPR')->first();
        // dd($catatan);

        // return view('evaluasi/anti-hiv', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi'));
        if (count($catatan) > 0) {
            $pdf = PDF::loadview('evaluasi.imunologi.rpr.print', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'ttd', 'catatan', 'rujukan'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
        }else{
            Session::flash('message', 'RPR belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
        return $pdf->stream('Evaluasi Syphilis RPR.pdf');
    }

    public function updatevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('y');
        $input = $request->all();
        // dd($input);
        $i = 0;
        $a = 0;
        // foreach ($input['strategi'] as $strategi) {
        // $a++;
        //     if($strategi != ''){
        //         $SaveStrategi['nilai'] = $request->strategi[$i];
        //         if ($input['strategi'][$i] == '0') {
        //             $SaveStrategi['kategori'] = 'Tidak Sesuai';
        //         }elseif ($input['strategi'][$i] == '5') {
        //             $SaveStrategi['kategori'] = 'Sesuai';
        //         }else{
        //             $SaveStrategi['kategori'] = '';
        //         }
        //         $SaveStrategi['tabung'] = $a;
        //         $SaveStrategi['kode_bahan'] = $request->kode_bahan_kontrol[$i];
        //         Strategi::where('id', $request->idevaluasi[$i])->update($SaveStrategi);
        //     }
        //     $i++;
        // }
        $SaveCatatan['catatan'] = $request->catatan;
        $SaveCatatan['tanggal_ttd'] = $request->ttd;
        CatatanImun::where('id_registrasi',$id)->update($SaveCatatan);
        
        $SaveKesimpulan['ketepatan'] = $request->ketepatan;
        Kesimpulan::where('id_registrasi',$id)->where('type','=','rpr-syphilis')->update($SaveKesimpulan);

        return redirect('/hasil-pemeriksaan/rpr-syphilis/evaluasi/'.$id.'?y='.$type);
    }
}