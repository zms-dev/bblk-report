<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\tbsptjm as Sptjm;
use Validator;
use Illuminate\Support\Facades\Redirect;

class SptjmController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $sptjm = DB::table('tb_sptjm')
            ->get();
        return view('back/sptjm/index', compact('sptjm'));
    }

    public function edit($id)
    {
        $data = DB::table('tb_sptjm')
            ->where('id',$id)
            ->get();
        return view('back/sptjm/update', compact('data'));
    }

    public function in()
    {
        return view('back/sptjm/insert');
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
       if (Request::hasFile('file'))
        {
            $dest = public_path('asset/backend/sptjm/');
            $name = Request::file('file')->getClientOriginalName();
            Request::file('file')->move($dest, $name);
        }
        $data = Request::file('file');
        $data = Request::all();
        $data['file'] = $name;
        Sptjm::where('id',$id)->update($data);
        Session::flash('message', 'Sptjm Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/sptjm');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        if (Request::hasFile('file'))
        {
            $dest = public_path('asset/backend/sptjm/');
            $name = Request::file('file')->getClientOriginalName();
            Request::file('file')->move($dest, $name);
        }
        $data = Request::file('file');
        $data = Request::all();
        $data['file'] = $name;
        Sptjm::create($data);
        Session::flash('message', 'Sptjm Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/sptjm');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Sptjm::find($id)->delete();
        return redirect("admin/sptjm");
    }
}
