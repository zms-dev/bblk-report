<?php

namespace App\Http\Controllers;
use DB;
use Carbon\Carbon;
use App\User;
use App\daftar as Daftar;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\SertfikatInsert;
class GrafikController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = DB::table('provinces')
        //         ->join('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
        //         ->select(DB::raw('provinces.name,count(DISTINCT perusahaan.id) AS Provinsi, (SELECT count(*) from perusahaan) as jumlah, provinces.alias as Alias'))
        //         ->groupBy('provinces.name')
        //         ->groupBy('provinces.Alias')
        //         ->get();
        // foreach ($data as $key => $r) {
        //     $r->percentage = $r->Provinsi / $r->jumlah * 100;
        // }
        // // dd($data);

        return view('dashboard.grafikwilayah.index');
    }

    public function wilayahg(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $siklus = $request->get('y');

        $data = DB::table('provinces')
                ->join('perusahaan', 'provinces.id', '=', 'perusahaan.provinsi')
                ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->select(DB::raw('provinces.name,count(DISTINCT perusahaan.id) AS Provinsi, (SELECT count(*) from perusahaan) as jumlah, provinces.alias as Alias'))
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('tb_registrasi.siklus', $siklus)
                ->groupBy('provinces.name')
                ->groupBy('provinces.Alias')
                ->get();
        $rows = array();
        foreach ($data as $key => $r) {
            $row['y'] = $r->Provinsi / $r->jumlah * 100;
            $row['bukan'] = $r->Provinsi;
            $row['name'] = $r->name;
            array_push($rows, $row);
        }

        return response ($rows);
    }

    public function badan(){
        return view('dashboard.grafikbadan.index');
    }
    public function badang(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $siklus = $request->get('y');

        $data = DB::table('badan_usaha')
                ->join('perusahaan', 'badan_usaha.id', '=', 'perusahaan.pemerintah')
                ->join('tb_registrasi', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->select(DB::raw('badan_usaha.badan_usaha,count(DISTINCT perusahaan.id) AS Badan, (SELECT count(*) from perusahaan) as jumlah, badan_usaha.alias as Alias'))
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('tb_registrasi.siklus', $siklus)
                ->groupBy('badan_usaha.badan_usaha')
                ->groupBy('badan_usaha.Alias')
                ->get();
        $rows = array();
        foreach ($data as $key => $r) {
            $row['y'] = $r->Badan / $r->jumlah * 100;
            $row['bukan'] = $r->Badan;
            $row['name'] = $r->Alias;
            array_push($rows, $row);
        }
        // dd($data);
        return response ($rows);
    }

    public function bidang()
    {
        return view('dashboard.grafikbidang.index');
    }

    public function bidangg(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $siklus = $request->get('y');
        $data = DB::table('sub_bidang')
                ->join('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->select(DB::raw('sub_bidang.alias,count(DISTINCT tb_registrasi.id) AS Bidang, (SELECT count(*) from tb_registrasi) as jumlah'))
                ->groupBy('sub_bidang.alias')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('tb_registrasi.siklus', $siklus)
                ->orderBy('sub_bidang.id', 'asc')
                ->get();
        $rows = array();
        foreach ($data as $key => $r) {
            $row['bukan'] = $r->Bidang;
            $row['y'] = $r->Bidang / $r->jumlah * 100;
            $row['name'] = $r->alias;
            array_push($rows, $row);
        }
        return response ($rows);
    }

    public function pembayaran()
    {
        return view('dashboard.grafikpembayaran.index', compact('data'));
    }
    public function typeg(\Illuminate\Http\Request $request)
    {
        $tahun = $request->get('x');
        $siklus = $request->get('y');

        $data = DB::table('pembayaran')
                ->join('tb_registrasi', 'pembayaran.id', '=', 'tb_registrasi.id_pembayaran')
                ->select(DB::raw('pembayaran.type,count(DISTINCT tb_registrasi.id) AS Type, (SELECT count(*) from tb_registrasi) as jumlah'))
                ->groupBy('pembayaran.type')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('tb_registrasi.siklus', $siklus)
                ->get();
        $rows = array();
        foreach ($data as $key => $r) {
            $row['y'] = $r->Type / $r->jumlah * 100;
            $row['bukan'] = $r->Type;
            $row['name'] = $r->type;
            array_push($rows, $row);
        }
        return response ($rows);
    }


    public function perusahaanhem(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','1')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="hematologi/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.hematologi.grafik.perusahaan');
    }

    public function perusahaanhemsemua(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, tb_registrasi.kode_lebpes, @rownum := @rownum +1 as rownum,tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('tb_registrasi.bidang','=','1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('tb_registrasi.status','>=', 2)
                ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '=', 12)
                            ->orwhere('tb_registrasi.siklus', '=', $siklus);
                    })
                ->get();

            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Proses
                                    </button>
                                </a>'
                        ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'a')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'b')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
        return view('evaluasi.hematologi.grafiksemua.perusahaan');
    }

    public function dataperusahaanhemsemua(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.grafiksemua.dataperusahaan', compact('data', 'siklus'));
    }

    public function perusahaankksemua(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes'), 'tb_registrasi.id as idregistrasi')
                    ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.status', '>=', '2')
                    ->where(function($query) use ($siklus)
                        {
                            $query->where('siklus', '=', '12')
                                ->orwhere('siklus', $siklus);
                        })
                    ->where('tb_registrasi.bidang','=','2')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                    ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('type', 'desc')
                                ->orderBy('id', 'asc')
                                ->first();
                    return "".'<a href="kimiaklinik/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Proses
                                            </button>
                                        </a>';
                ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'a')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'b')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
        return view('evaluasi.kimiaklinik.grafiksemua.perusahaan');
    }

    public function dataperusahaankksemua(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('sub_bidang.id', '=', '2')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafiksemua.dataperusahaan', compact('data','siklus'));
    }

    public function dataperusahaanhem($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.grafik.dataperusahaan', compact('data'));
    }

    public function dataperusahaanalathem($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.grafikalat.dataperusahaan', compact('data'));
    }

    public function dataperusahaanmetodehem($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.grafikmetode.dataperusahaan', compact('data'));
    }

    public function perusahaankimnilai(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes'), 'tb_registrasi.id as idregistrasi')
                    ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.status', '>=', '2')
                    ->where(function($query) use ($siklus)
                        {
                            $query->where('siklus', '=', '12')
                                ->orwhere('siklus', $siklus);
                        })
                    ->where('tb_registrasi.bidang','=','2')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                    ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('type', 'desc')
                                ->orderBy('id', 'asc')
                                ->first();
                    return "".'<a href="kimiaklinik/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Proses
                                            </button>
                                        </a>';
                ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'a')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'b')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
        return view('evaluasi.kimiaklinik.grafiknilai.perusahaan');
    }

    public function dataperusahaankimnilai(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->where('sub_bidang.id', '=', '2')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafiknilai.dataperusahaan', compact('data', 'siklus'));
    }

    public function perusahaankimnilaisertifikat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes'), 'tb_registrasi.id as idregistrasi')
                    ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.bidang','=','2') 
                    ->where(function ($query) use ($siklus) {
                        $query->where('tb_registrasi.siklus','=','12')
                              ->orwhere('tb_registrasi.siklus','=',$siklus);
                    })
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                    ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data)use($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=' , $siklus)
                                ->where('form', '=', 'kimia klinik')
                                ->orderBy('id', 'desc')
                                ->first();
                        if(!empty($evaluasi)){
                            if ($evaluasi->siklus == '1') {
                                if ($evaluasi->siklus == 'a') {
                                    return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                                    <button class="btn btn-primary btn-xs">
                                                       Proses
                                                    </button>
                                                </a>';
                                }else{
                                    return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                                    <button class="btn btn-info btn-xs">
                                                       Proses
                                                    </button>
                                                </a>';
                                }
                            }elseif($evaluasi->siklus == '2'){
                                if ($evaluasi->siklus == 'a') {
                                    return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                                    <button class="btn btn-info btn-xs">
                                                       Proses
                                                    </button>
                                                </a>';
                                }else{
                                    return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                                    <button class="btn btn-success btn-xs">
                                                       Proses
                                                    </button>
                                                </a>';
                                }
                            }else{
                                return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                                <button class="btn btn-primary btn-xs">
                                                   Proses
                                                </button>
                                            </a>';
                            }
                        }else{
                            return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                               Proses
                                            </button>
                                        </a>';
                        }
                        ;})

             ->addColumn('A', function($data)use($siklus,$tahun){
                        $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('tahun',$tahun)
                                    ->where('type', '=', 'a')
                                    ->where('form', '=', 'kimia klinik')
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if(count($evaluasi)){
                            if ($evaluasi->siklus == '1') {
                                return "".'Selesai';
                            }else{
                                return "".'Selesai';
                            }
                        }else{
                            return "".'-';
                        }
                ;})
            ->addColumn('B', function($data)use($siklus,$tahun){
                        $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('tahun',$tahun)
                                    ->where('type', '=', 'b')
                                    ->where('form', '=', 'kimia klinik')
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if(count($evaluasi)){
                            if ($evaluasi->siklus == '1') {
                                return "".'Selesai';
                            }else{
                                return "".'Selesai';
                            }
                        }else{
                            return "".'-';
                        }
                ;})
            ->make(true);
        }
         $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=','Kimiaklinik')->first();
         // dd($sertifikat);
        return view('evaluasi.kimiaklinik.grafiknilai.perusahaansertifikat',compact('sertifikat'));
    }
    public function evaluasiInsertSertifikat(\Illuminate\Http\Request $request)
    {
        $data = $request->all();
        $sertifikat = DB::table('tb_sertifikat')->where('siklus',$data['siklus'])->where('tahun',$data['tahun'])->where('parameter','=','Kimiaklinik')->get();

        if (count($sertifikat)) {
            $sertifikat = SertfikatInsert::where("tb_sertifikat.parameter", $request->parameter)
                                ->where('siklus', $data['siklus'])
                                ->where('tahun', $data['tahun'])
                                ->update([
                                    "tanggal_sertifikat" => $request->tanggal_sertifikat,
                                    "nomor_sertifikat" => $request->nomor_sertifikat
                                ]);
        }else{
            $b = new SertfikatInsert;
            $b->tanggal_sertifikat = $request->tanggal_sertifikat;
            $b->nomor_sertifikat =$request->nomor_sertifikat;
            $b->parameter = $request->parameter;
            $b->siklus = $request->siklus;
            $b->tahun = $request->tahun;
            $b->save();
        }
        // dd($data);
        return back();
    }
    public function dataperusahaankimnilaisertifikat(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id) 
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('sub_bidang.id', '=', '2')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=','Kimiaklinik')->first();
        return view('evaluasi.kimiaklinik.grafiknilai.dataperusahaansertifikat', compact('data','sertifikat','siklus','tahun'));
    }

    public function dataperusahaanhemnilaialat($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.grafiknilaialat.dataperusahaan', compact('data'));
    }

    public function dataperusahaankimnilaialat($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafiknilaialat.dataperusahaan', compact('data'));
    }

    public function dataperusahaankimnilaimetode($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafiknilaimetode.dataperusahaan', compact('data'));
    }

    public function dataperusahaanhemnilaimetode($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.grafiknilaimetode.dataperusahaan', compact('data'));
    }

    public function perusahaanhemnilai(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, tb_registrasi.kode_lebpes, @rownum := @rownum +1 as rownum,tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('tb_registrasi.bidang','=','1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('tb_registrasi.status','>=', 2)
                ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '=', 12)
                            ->orwhere('tb_registrasi.siklus', '=', $siklus);
                    })
                ->get();

            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Proses
                                    </button>
                                </a>'
                        ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'a')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'b')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
        return view('evaluasi.hematologi.grafiknilai.perusahaan');
    }

    public function dataperusahaanhemnilai(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.grafiknilai.dataperusahaan', compact('data', 'siklus'));
    }

    public function perusahaankimairnilai(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','11');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kimiaair/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaair.grafiknilai.perusahaan');
    }

    public function dataperusahaankimairnilai($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '11')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaair.grafiknilai.dataperusahaan', compact('data'));
    }

    public function perusahaankimairterbatasnilai(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','12');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kimiaairterbatas/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaairterbatas.grafiknilai.perusahaan');
    }

    public function dataperusahaankimairterbatasnilai($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '12')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaairterbatas.grafiknilai.dataperusahaan', compact('data'));
    }

    public function perusahaankim(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','2')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kimiaklinik/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaklinik.grafik.perusahaan');
    }

    public function dataperusahaankim($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafik.dataperusahaan', compact('data'));
    }

    public function dataperusahaankimalat($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafikalat.dataperusahaan', compact('data'));
    }

    public function dataperusahaankimmetode($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaklinik.grafikmetode.dataperusahaan', compact('data'));
    }

    public function perusahaankimair(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','11');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kimiaair/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaair.grafik.perusahaan');
    }

    public function dataperusahaankimair($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '11')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaair.grafik.dataperusahaan', compact('data'));
    }


    public function perusahaankimairterbatas(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','12');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kimiaairterbatas/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaairterbatas.grafik.perusahaan');
    }

    public function dataperusahaankimairterbatas($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '12')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaairterbatas.grafik.dataperusahaan', compact('data'));
    }

    public function zhemsemua(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','hematologi')->get();
        $alat = DB::table('tb_instrumen')->where('id_parameter','=','hematologi')->get();
        return view('evaluasi.hematologi.grafiksemua.zscore_parameter', compact('param','tahun','type','siklus', 'id', 'alat'));
    }

    public function gzhemsemua(\Illuminate\Http\Request $request, $id)
    {
      // return('kole');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;

        $type = $request->get('x');
        $siklus = $request->get('y');
        $alatmetode = DB::table('hp_headers')
                        ->join('hp_details', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                        ->join('parameter','parameter.id','=','hp_details.parameter_id')
                        ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                        ->where('hp_headers.id_registrasi', $id)
                        ->where('hp_headers.type', $type)
                        ->where('hp_headers.siklus', $siklus)
                        ->select('kode_metode_pemeriksaan as id_metode','hp_details.parameter_id as id','nama_parameter','metode_pemeriksaan')
                        ->get();
        // $alat = $alatmetode->alat;
        // $metode = $alatmetode->kode_metode_pemeriksaan;
        foreach ($alatmetode as $key => $val) {
        $sql ="SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` where not zscore = '-' AND form = 'hematologi' AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore < -1 and zscore >= -2, 1, 0)) as nilai1, '< -1 sd -2' as nilainya, '2' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION

            SELECT sum(if(zscore >= -1 and zscore < 0, 1, 0)) as nilai1, '>= -1 sd < 0' as nilainya, '3' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            
            SELECT sum(if(zscore = 0.0 , 1, 0)) as nilai1, '0.0' as nilainya, '4' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION

            SELECT sum(if(zscore > 0 and zscore <= 1, 1, 0)) as nilai1, '> 0 sd 1' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION

            SELECT sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '6' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '7' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '8' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'hematologi' AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '< -1 sd -2' as nilainya, 'c' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '>= -1 sd < 0' as nilainya, 'e' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore >= -1 AND zscore < 0
                UNION
                SELECT count(*) as nilai1, '0.0' as nilainya, 'f' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore = 0.0
                UNION
                SELECT count(*) as nilai1, '> 0 sd 1' as nilainya, 'g' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 0 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'h' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'i' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'j' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','hematologi')
                ->where('tahun','=',$tahun)
                ->orderBy('sort', 'asc')
                ->get();
        // dd($data);
        $val->data = $data;
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        $val->datap = $datap;
        // dd($datam);
          $metodena = DB::table('metode_pemeriksaan')->where('id', $id)->first();
          // dd($alat);
          $sql_metode ="
            SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` where not zscore = '-' AND form = 'hematologi' AND metode = ".$val->id_metode." AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$val->id_metode." AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore < -1 and zscore >= -2, 1, 0)) as nilai1, '< -1 sd -2' as nilainya, '2' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$val->id_metode." AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            
            SELECT sum(if(zscore >= -1 and zscore < 0, 1, 0)) as nilai1, '>= -1 sd < 0' as nilainya, '3' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$val->id_metode." AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            
            SELECT sum(if(zscore = 0.0, 1, 0)) as nilai1, '0.0' as nilainya, '4' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$val->id_metode." AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            
            SELECT sum(if(zscore > 0 and zscore <= 1, 1, 0)) as nilai1, '> 0 sd 1' as nilainya, '5' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$val->id_metode." AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION

            SELECT sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '6' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$val->id_metode." AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '7' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$val->id_metode." AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '8' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'hematologi' AND metode = ".$val->id_metode." AND tahun = '".$tahun."' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter";

          $sql2_metode =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -3
                  UNION
                  SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                  UNION
                  SELECT count(*) as nilai1, '< -1 sd -2' as nilainya, 'c' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -1 AND zscore >= -2
                  UNION
                  SELECT count(*) as nilai1, '>= -1 sd < 0' as nilainya, 'd' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore >= -1 AND zscore < 0
                  UNION
                  SELECT count(*) as nilai1, '> 0 sd 1' as nilainya, 'e' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 0 AND zscore <= 1
                  UNION
                  SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'f' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > -1 AND zscore <= 1
                  UNION
                  SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'g' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 1 AND zscore <= 2
                  UNION
                  SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'h' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 2 AND zscore <= 3
                  UNION
                  SELECT count(*) as nilai1, '> 3' as nilainya, 'i' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 3";
          $data_metode = DB::table(DB::raw("($sql_metode) zone"))
                  ->select('*')
                  ->where('form','=','hematologi')
                  ->where('tahun','=',$tahun)
                  ->orderBy('sort', 'asc')
                  ->get();
          $val->data_metode = $data_metode;
          $datap_metode = DB::table(DB::raw("($sql2_metode) zone"))
                  ->select('*')
                  ->orderBy('sort', 'asc')
                  ->get();
            $val->datap_metode = $datap_metode;
        }
        return view('evaluasi.hematologi.grafiksemua.grafik', compact('metodena','data','tahun','type','siklus','param','datap','datam', 'data_metode', 'datap_metode', 'datam_metode','paramna','alatmetode'));
    }

    public function zkksemua(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;

        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        $alat = DB::table('tb_instrumen')->where('id_parameter','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.grafiksemua.zscore_parameter', compact('param','tahun','type','siklus', 'id', 'alat'));
    }

    public function gzkksemua(\Illuminate\Http\Request $request, $id)
    {
      // return('kole');
        $input = $request->all();
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;

        $type = $request->get('x');
        $siklus = $request->get('y');
        $alatmetode = DB::table('hp_headers')
                        ->join('hp_details', 'hp_details.hp_header_id', '=', 'hp_headers.id')
                        ->join('parameter','parameter.id','=','hp_details.parameter_id')
                        ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                        ->where('hp_headers.id_registrasi', $id)
                        ->where('hp_headers.type', $type)
                        ->where('hp_headers.siklus', $siklus)
                        ->select('kode_metode_pemeriksaan as id_metode','hp_details.parameter_id as id','nama_parameter','metode_pemeriksaan')
                        ->get();
        // dd($alatmetode);
        // $alat = $alatmetode->alat;
        // $metode = $alatmetode->kode_metode_pemeriksaan;

        // $param = DB::table('parameter')->where('id',$parameter)->first();
        foreach ($alatmetode as $key => $val) {
        $sql ="SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` where not zscore = '-' AND form = 'kimia klinik' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'kimia klinik' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore < -1 and zscore >= -2, 1, 0)) as nilai1, '< -1 sd -2' as nilainya, '2' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'kimia klinik' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION

            SELECT sum(if(zscore >= -1 and zscore < 0, 1, 0)) as nilai1, '>= -1 sd < 0' as nilainya, '3' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'kimia klinik' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            
            SELECT sum(if(zscore = 0.0 , 1, 0)) as nilai1, '0.0' as nilainya, '4' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'kimia klinik' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION

            SELECT sum(if(zscore > 0 and zscore <= 1, 1, 0)) as nilai1, '> 0 sd 1' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'kimia klinik' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION

            SELECT sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '6' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'kimia klinik' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '7' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'kimia klinik' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '8' as sort, form, tahun, parameter, type, siklus, id_registrasi
            FROM `tb_zscore` WHERE not zscore = '-' AND form = 'kimia klinik' AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '< -1 sd -2' as nilainya, 'c' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '>= -1 sd < 0' as nilainya, 'e' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore >= -1 AND zscore < 0
                UNION
                SELECT count(*) as nilai1, '0.0' as nilainya, 'f' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore = 0.0
                UNION
                SELECT count(*) as nilai1, '> 0 sd 1' as nilainya, 'g' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 0 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'h' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'i' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'j' as sort from tb_zscore
                where parameter = '".$val->id."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','kimia klinik')
                ->where('tahun','=',$tahun)
                ->orderBy('sort', 'asc')
                ->get();
        $val->data = $data;
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        $val->datap = $datap;
        // dd($datam);
          // dd($alat);
          $sql_metode ="
            SELECT sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` where not zscore = '-' AND form = 'kimia klinik' AND metode = ".$val->id_metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'kimia klinik' AND metode = ".$val->id_metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore < -1 and zscore >= -2, 1, 0)) as nilai1, '< -1 sd -2' as nilainya, '2' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'kimia klinik' AND metode = ".$val->id_metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            
            SELECT sum(if(zscore >= -1 and zscore < 0, 1, 0)) as nilai1, '>= -1 sd < 0' as nilainya, '3' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'kimia klinik' AND metode = ".$val->id_metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            
            SELECT sum(if(zscore = 0.0, 1, 0)) as nilai1, '0.0' as nilainya, '4' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'kimia klinik' AND metode = ".$val->id_metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            
            SELECT sum(if(zscore > 0 and zscore <= 1, 1, 0)) as nilai1, '> 0 sd 1' as nilainya, '5' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'kimia klinik' AND metode = ".$val->id_metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION

            SELECT sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '6' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'kimia klinik' AND metode = ".$val->id_metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '7' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'kimia klinik' AND metode = ".$val->id_metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter UNION
            SELECT sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '8' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
            FROM `tb_zscore_metode` WHERE not zscore = '-' AND form = 'kimia klinik' AND metode = ".$val->id_metode." AND type = '".$type."' AND siklus = ".$siklus." AND parameter = ".$val->id." GROUP BY parameter";

          $sql2_metode =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -3
                  UNION
                  SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                  UNION
                  SELECT count(*) as nilai1, '< -1 sd -2' as nilainya, 'c' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -1 AND zscore >= -2
                  UNION
                  SELECT count(*) as nilai1, '>= -1 sd < 0' as nilainya, 'd' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore >= -1 AND zscore < 0
                  UNION
                  SELECT count(*) as nilai1, '> 0 sd 1' as nilainya, 'e' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 0 AND zscore <= 1
                  UNION
                  SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'f' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > -1 AND zscore <= 1
                  UNION
                  SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'g' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 1 AND zscore <= 2
                  UNION
                  SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'h' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 2 AND zscore <= 3
                  UNION
                  SELECT count(*) as nilai1, '> 3' as nilainya, 'i' as sort from tb_zscore_metode
                  where parameter = '".$val->id."' AND metode = '".$val->id_metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 3";

          $data_metode = DB::table(DB::raw("($sql_metode) zone"))
                  ->select('*')
                  ->where('form','=','kimia klinik')
                  ->where('tahun','=',$tahun)
                  ->orderBy('sort', 'asc')
                  ->get();
          $val->data_metode = $data_metode;
          $datap_metode = DB::table(DB::raw("($sql2_metode) zone"))
                  ->select('*')
                  ->orderBy('sort', 'asc')
                  ->get();
          $val->datap_metode=$datap_metode;
        }
        return view('evaluasi.kimiaklinik.grafiksemua.grafik', compact('metodena','data','tahun','type','siklus','param','datap','datam', 'data_metode', 'datap_metode', 'datam_metode','alatmetode'));
    }

    public function zhem(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','hematologi')->get();
        return view('evaluasi.hematologi.grafik.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function gzhem(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $parameter = $input['parameter'];
        $type = $input['type'];
        $siklus = $input['siklus'];

        $param = DB::table('parameter')->where('id',$parameter)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` where form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'hematologi' GROUP BY parameter";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";

        $median ="SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median < -2 and median >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median <= -1 AND median >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > -1 AND median <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 1 AND median <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 2 AND median <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_sd_median
                where parameter = '".$parameter."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 3";

        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','hematologi')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('type','=',$type)
                ->where('siklus','=',$siklus)
                ->orderBy('sort', 'asc')
                ->get();
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        $datam = DB::table(DB::raw("($median) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        // dd($datam);
        return view('evaluasi.hematologi.grafik.grafik', compact('data','tahun','type','siklus','param','datap','datam'));
    }

    public function alatzhem(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','hematologi')->get();
        $alat = DB::table('tb_instrumen')->where('id_parameter','=','hematologi')->get();
        // dd($alat);
        return view('evaluasi.hematologi.grafikalat.zscore_parameter', compact('alat','tahun','type','siklus', 'id', 'param'));
    }

    public function alatgzhem(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $alat = $input['alat'];
        $parameter = $input['parameter'];
        $type = $input['type'];
        $siklus = $input['siklus'];

        $param = DB::table('parameter')->where('id', $parameter)->first();
        $alatna = DB::table('tb_instrumen')->where('id', $alat)->first();
        // dd($alat);
        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` where form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'hematologi' GROUP BY parameter";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 3";

        $median ="SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median < -2 and median >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median <= -1 AND median >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > -1 AND median <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 1 AND median <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 2 AND median <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_sd_median_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 3";

        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','hematologi')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('alat','=',$alat)
                ->where('type','=',$type)
                ->where('siklus','=',$siklus)
                ->orderBy('sort', 'asc')
                ->get();
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        $datam = DB::table(DB::raw("($median) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        // dd($data);
        return view('evaluasi.hematologi.grafikalat.grafik', compact('data','tahun','type','siklus','param','datap','datam','alatna'));
    }

    public function getmetode($id){
        $data = DB::table('metode_pemeriksaan')->select('id as Kode','metode_pemeriksaan as Nama')->where('parameter_id', $id)->get();
        return response()->json(['Hasil'=>$data]);
    }

    public function getmetodesemua($id){
        $data_metode = DB::table('metode_pemeriksaan')->select('id as Kode','metode_pemeriksaan as Nama')->where('parameter_id', $id)->get();
        $data_alat = DB::table('tb_instrumen')->select('id as Kode','instrumen as Nama')->where('id_parameter', 'hematologi')->get();
        return response()->json(['Hasil'=>$data_metode, 'Data'=>$data_alat]);
    }

    public function metodezhem(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','hematologi')->get();
        // dd($alat);
        return view('evaluasi.hematologi.grafikmetode.zscore_parameter', compact('tahun','type','siklus', 'id', 'param'));
    }

    public function metodegzhem(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $metode = $input['metode'];
        $parameter = $input['parameter'];
        $type = $input['type'];
        $siklus = $input['siklus'];

        $param = DB::table('parameter')->where('id', $parameter)->first();
        $metodena = DB::table('metode_pemeriksaan')->where('id', $metode)->first();
        // dd($alat);
        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` where form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'hematologi' GROUP BY parameter
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'hematologi' GROUP BY parameter";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 3";

        $median ="SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median < -2 and median >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median <= -1 AND median >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > -1 AND median <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 1 AND median <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 2 AND median <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_sd_median_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'hematologi' AND siklus = '".$siklus."' AND tipe = '".$type."' AND tahun = '".$tahun."' AND median > 3";

        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','hematologi')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('metode','=',$metode)
                ->where('type','=',$type)
                ->where('siklus','=',$siklus)
                ->orderBy('sort', 'asc')
                ->get();
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        $datam = DB::table(DB::raw("($median) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        // dd($metode);
        return view('evaluasi.hematologi.grafikmetode.grafik', compact('data','tahun','type','siklus','param','datap','datam','metodena'));
    }

    public function nilaisama(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $data =  DB::table('tb_registrasi')
        ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
        ->where('tb_registrasi.id', $id)
        ->where('sub_bidang.id', '=', '2')
        ->where('status', 3)
        ->where(function($query)
            {
                $query->where('status_data1', 2)
                    ->orwhere('status_data2', 2);
            })
        ->where(function($query)
            {
                if ('pemeriksaan' != 'done') {
                    $query->orwhere('pemeriksaan2', 'done');
                }
                if ('pemeriksaan2' != 'done') {
                    $query->orwhere('pemeriksaan', 'done');
                }
            })
        ->first();
        $param = DB::table('parameter')->where('parameter.kategori','=','kimia klinik')
        ->join('hp_details','hp_details.parameter_id','=','parameter.id')
        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
        ->leftJoin('tb_sd_median',function($join) use($tahun,$siklus,$type){
            $join->on('tb_sd_median.parameter','=','parameter.id')
            ->where('tb_sd_median.tahun','=',$tahun)
            ->where('tb_sd_median.siklus','=',$siklus)
            ->where('tb_sd_median.form','=','kimia klinik')
            ->where('tb_sd_median.tipe','=',$type);
        })
        ->leftJoin('tb_ring_kk',function($join) use($tahun,$siklus,$type){
            $join->on('tb_ring_kk.parameter','=','parameter.id')
            ->where('tb_ring_kk.tahun','=',$tahun)
            ->where('tb_ring_kk.siklus','=',$siklus)
            ->where('tb_ring_kk.form','=','kimia klinik')
            ->where('tb_ring_kk.tipe','=',$type);
        })
        ->leftJoin('tb_zscore',function($join) use($tahun,$siklus,$type,$id){
            $join->on('tb_zscore.parameter','=','parameter.id')
            ->where('tb_zscore.tahun','=',$tahun)
            ->where('tb_zscore.id_registrasi','=',$id)
            ->where('tb_zscore.siklus','=',$siklus)
            ->where('tb_zscore.form','=','kimia klinik')
            ->where('tb_zscore.type','=',$type);
        })
        ->where('hp_headers.id_registrasi','=',$id)
        ->where('hp_headers.type','=',$type)
        ->whereRaw('hp_details.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")')
        ->select(
            'parameter.nama_parameter',
            'parameter.id as id_parameter',
            'hp_details.hasil_pemeriksaan',
            'tb_sd_median.sd',
            'tb_sd_median.median',
            'tb_ring_kk.batas_bawah',
            'tb_ring_kk.batas_atas',
            'tb_ring_kk.ring_1_bawah',
            'tb_ring_kk.ring_1_atas',
            'tb_ring_kk.ring_2_bawah',
            'tb_ring_kk.ring_2_atas',
            'tb_ring_kk.ring_3_bawah',
            'tb_ring_kk.ring_3_atas',
            'tb_ring_kk.ring_4_bawah',
            'tb_ring_kk.ring_4_atas',
            'tb_ring_kk.ring_5_bawah',
            'tb_ring_kk.ring_5_atas',
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.parameter_id = tb_ring_kk.parameter
                    AND
                HPD.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_bawah'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.parameter_id = tb_ring_kk.parameter
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_1_bawah AND tb_ring_kk.ring_1_atas
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_1'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.parameter_id = tb_ring_kk.parameter
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_2_bawah AND tb_ring_kk.ring_2_atas
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_2'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.parameter_id = tb_ring_kk.parameter
                    AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_3_bawah AND tb_ring_kk.ring_3_atas
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.parameter_id = tb_ring_kk.parameter
                    AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_4_bawah AND tb_ring_kk.ring_4_atas
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_4'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.parameter_id = tb_ring_kk.parameter
                    AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_5_bawah AND tb_ring_kk.ring_5_atas
                        AND
                    HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_5'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.parameter_id = tb_ring_kk.parameter
                    AND
                HPD.hasil_pemeriksaan > tb_ring_kk.batas_atas
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_atas'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 1
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara11'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_3'),
            'tb_zscore.zscore as zscore'
        )
        // ->where()
        ->get();
        return view('evaluasi.kimiaklinik.grafiknilai.zscore_parameter', compact('param','tahun','type','siklus', 'id','data'));
    }

    public function hemnilaisama(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','hematologi')->get();
        return view('evaluasi.hematologi.grafiknilai.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function ghemnilaisama(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $beluminput = DB::table('tb_registrasi')
                        ->where('tb_registrasi.bidang','=','1')
                        ->where('tb_registrasi.status','=','3')
                        ->where(function ($query) use ($siklus) {
                            $query->where('tb_registrasi.siklus','=','12')
                                  ->orwhere('tb_registrasi.siklus','=',$siklus);
                        })
                        ->where(function ($query) use ($siklus) {
                            if ($siklus == 1) {
                                $query->where('tb_registrasi.siklus_1','=', NULL);
                            }else{
                                $query->where('tb_registrasi.siklus_2','=', NULL);
                            }
                        })
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                        ->count();
        // dd($beluminput1);
        $datapes = DB::table('hp_headers')
                ->leftjoin('hp_details', 'hp_details.hp_header_id','=', 'hp_headers.id')
                ->leftjoin('metode_pemeriksaan', 'metode_pemeriksaan.id', '=', 'hp_details.kode_metode_pemeriksaan')
                ->leftjoin('parameter', 'parameter.id', '=', 'hp_details.parameter_id')
                ->where('hp_headers.id_registrasi', $id)
                ->where('hp_headers.type', $type)
                ->where('hp_headers.siklus', $siklus)
                ->select('hp_details.parameter_id as id','metode_pemeriksaan','kode_metode_pemeriksaan','nama_parameter', 'hp_details.id as iddetail')
                ->orderBy('parameter.bagian', 'asc')
                ->get();
        // dd($parameter);
        foreach ($datapes as $key => $value) {
            $data = DB::table('parameter')
                    ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                    ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                    ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                    ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                    ->where('parameter.id', $value->id)
                    ->where('hp_headers.siklus', $siklus)
                    ->where('hp_headers.type', $type)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                    ->select('hp_details.id', 'hp_details.hasil_pemeriksaan', DB::raw('count(hp_details.hasil_pemeriksaan) as jumlah'))
                    ->groupBy('hp_details.id')
                    ->get();
                foreach ($data as $key => $val) {
                    $peserta = DB::table('hp_details')
                            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                            ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_details.id', $val->id)
                            ->where('tb_registrasi.id', $id)
                            ->select('hp_details.hasil_pemeriksaan')
                            ->get();
                    $val->peserta = $peserta;
                }
            $value->data = $data;
            $batas = DB::table('tb_rentang_parameter')
                    ->where('id_parameter', $value->id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $tahun)
                    ->get();

            $value->batas = $batas;

            $datametode = DB::table('parameter')
                    ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                    ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                    ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                    ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                    ->where('metode_pemeriksaan.id', $value->kode_metode_pemeriksaan)
                    ->where('hp_headers.siklus', $siklus)
                    ->where('hp_headers.type', $type)
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                    ->select('hp_details.id', 'hp_details.hasil_pemeriksaan', DB::raw('count(hp_details.hasil_pemeriksaan) as jumlah'))
                    ->groupBy('hp_details.id')
                    ->get();
                foreach ($datametode as $key => $valu) {
                    $peserta = DB::table('hp_details')
                            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                            ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_details.id', $valu->id)
                            ->where('tb_registrasi.id', $id)
                            ->select('hp_details.hasil_pemeriksaan')
                            ->get();
                    $valu->peserta = $peserta;
                }

            $batasmetode = DB::table('tb_rentang_metode')
                    ->where('id_parameter', $value->id)
                    ->where('metode', $value->kode_metode_pemeriksaan)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $tahun)
                    ->get();
            $value->datametode = $datametode;
            $value->batasmetode = $batasmetode;
        }
        // dd($datapes);
        return view('evaluasi.hematologi.grafiknilai.grafik', compact('datapes','parameter','data','batas', 'datametode', 'batasmetode', 'beluminput'));
    }

    public function kknilaisama(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.hematologi.grafiknilai.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function gkknilaisama(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $datapes = DB::table('hp_headers')
                ->leftjoin('hp_details', 'hp_details.hp_header_id','=', 'hp_headers.id')
                ->leftjoin('metode_pemeriksaan', 'metode_pemeriksaan.id', '=', 'hp_details.kode_metode_pemeriksaan')
                ->leftjoin('parameter', 'parameter.id', '=', 'hp_details.parameter_id')
                ->where('hp_headers.id_registrasi', $id)
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_details.hasil_pemeriksaan','!=','-')
                ->where('hp_headers.type', $type)
                ->select('hp_details.parameter_id as id','metode_pemeriksaan','kode_metode_pemeriksaan','nama_parameter')
                ->orderBy('parameter.bagian', 'asc')
                ->get();
        // dd($parameter);
        // dd($datapes);
        foreach ($datapes as $key => $value) {
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('parameter.id', $value->id)
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->select('hp_details.id', 'hp_details.hasil_pemeriksaan', DB::raw('count(hp_details.hasil_pemeriksaan) as jumlah'))
                ->groupBy('hp_details.id')
                ->get();
            foreach ($data as $key => $val) {
                $peserta = DB::table('hp_details')
                        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('hp_details.id', $val->id)
                        ->where('tb_registrasi.id', $id)
                        ->select('hp_details.hasil_pemeriksaan')
                        ->get();
                $val->peserta = $peserta;
            }
        $value->data = $data;
        $datametode = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('metode_pemeriksaan.id',$value->kode_metode_pemeriksaan)
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->select('hp_details.id', 'hp_details.hasil_pemeriksaan', DB::raw('count(hp_details.hasil_pemeriksaan) as jumlah'))
                ->groupBy('hp_details.id')
                ->get();
            foreach ($datametode as $key => $valu) {
                $peserta = DB::table('hp_details')
                        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('hp_details.id', $valu->id)
                        ->where('tb_registrasi.id', $id)
                        ->select('hp_details.hasil_pemeriksaan')
                        ->get();
                $valu->peserta = $peserta;
            }

        $batas = DB::table('tb_rentang_parameter')
                ->where('id_parameter', $value->id)
                ->where('siklus', $siklus)
                ->where('type', $type)
                ->where('tahun', $tahun)
                ->get();

        $batasmetode = DB::table('tb_rentang_metode')
                ->where('id_parameter', $value->id)
                ->where('metode',$value->kode_metode_pemeriksaan)
                ->where('siklus', $siklus)
                ->where('type', $type)
                ->where('tahun', $tahun)
                ->get();
        $value->batas = $batas;
        $value->datametode = $datametode;
        $value->batasmetode = $batasmetode;
        }
        // dd($data);
        return view('evaluasi.kimiaklinik.grafiknilai.grafik', compact('datapes','parameter','data','batas', 'datametode', 'batasmetode'));
    }

    public function kimiaairnilaisama(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia air')->get();
        return view('evaluasi.kimiaair.grafiknilai.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function gkimiaairnilaisama(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('parameter.id', $input['parameter'])
                ->where('hp_headers.siklus', $input['siklus'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('hp_details.id', 'hp_details.hasil_pemeriksaan', DB::raw('count(hp_details.hasil_pemeriksaan) as jumlah'))
                ->groupBy('hp_details.id')
                ->get();
            foreach ($data as $key => $val) {
                $peserta = DB::table('hp_details')
                        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('hp_details.id', $val->id)
                        ->where('tb_registrasi.id', $id)
                        ->select('hp_details.hasil_pemeriksaan')
                        ->get();
                $val->peserta = $peserta;
            }
        $ring = DB::table('tb_ring')
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tahun', $input['tahun'])
                ->get();
        $median = DB::table('tb_sd_median')
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tahun', $input['tahun'])
                ->first();
        // dd($data);
        return view('evaluasi.kimiaair.grafiknilai.grafik', compact('data','parameter','datas','ring', 'median'));
    }

    public function kimiaairterbatasnilaisama(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia air terbatas')->get();
        return view('evaluasi.kimiaairterbatas.grafiknilai.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function gkimiaairterbatasnilaisama(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('parameter.id', $input['parameter'])
                ->where('hp_headers.siklus', $input['siklus'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('hp_details.id', 'hp_details.hasil_pemeriksaan', DB::raw('count(hp_details.hasil_pemeriksaan) as jumlah'))
                ->groupBy('hp_details.id')
                ->get();
            foreach ($data as $key => $val) {
                $peserta = DB::table('hp_details')
                        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('hp_details.id', $val->id)
                        ->where('tb_registrasi.id', $id)
                        ->select('hp_details.hasil_pemeriksaan')
                        ->get();
                $val->peserta = $peserta;
            }
        $ring = DB::table('tb_ring')
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tahun', $input['tahun'])
                ->get();
        $median = DB::table('tb_sd_median')
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tahun', $input['tahun'])
                ->first();
        // dd($data);
        return view('evaluasi.kimiaairterbatas.grafiknilai.grafik', compact('data','parameter','datas','ring', 'median'));
    }

    public function gnilaisama(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        $peserta = DB::table('hp_details')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_details.parameter_id', $input['parameter'])
                ->where('tb_registrasi.id', $id)
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['type'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('hp_details.hasil_pemeriksaan')
                ->first();
        $ring = DB::table('tb_ring_kk')
                ->select('tb_ring_kk.*',
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_bawah'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_1_bawah AND tb_ring_kk.ring_1_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_1'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_2_bawah AND tb_ring_kk.ring_2_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_2'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_3_bawah AND tb_ring_kk.ring_3_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_bawah'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_3'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_4_bawah AND tb_ring_kk.ring_4_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_4'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_5_bawah AND tb_ring_kk.ring_5_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_5'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_atas')

                )
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['type'])
                ->where('tahun', $input['tahun'])
                ->first();
        $median = DB::table('tb_sd_median')
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['type'])
                ->where('tahun', $input['tahun'])
                ->first();
        // dd($median);
        return view('evaluasi.kimiaklinik.grafiknilai.grafik', compact('peserta','parameter','datas','ring', 'median'));
    }

    public function nilaisamaalat(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $data =  DB::table('tb_registrasi')
        ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
        ->where('tb_registrasi.id', $id)
        ->where('sub_bidang.id', '=', '2')
        ->where('status', 3)
        ->where(function($query)
            {
                $query->where('status_data1', 2)
                    ->orwhere('status_data2', 2);
            })
        ->where(function($query)
            {
                if ('pemeriksaan' != 'done') {
                    $query->orwhere('pemeriksaan2', 'done');
                }
                if ('pemeriksaan2' != 'done') {
                    $query->orwhere('pemeriksaan', 'done');
                }
            })
        ->first();
        $param = DB::table('tb_instrumen')->where('parameter.kategori','=','kimia klinik')
        ->join('hp_details','hp_details.alat','=','tb_instrumen.id')
        ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
        ->leftJoin('tb_sd_median_alat',function($join) use($tahun,$siklus,$type){
            $join->on('tb_sd_median_alat.alat','=','tb_instrumen.id')
            ->where('tb_sd_median_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
            ->where('tb_sd_median_alat.tahun','=',$tahun)
            ->where('tb_sd_median_alat.siklus','=',$siklus)
            ->where('tb_sd_median_alat.form','=','kimia klinik')
            ->where('tb_sd_median_alat.tipe','=',$type);
        })
        ->leftJoin('tb_ring_kk_alat',function($join) use($tahun,$siklus,$type){
            $join->on('tb_ring_kk_alat.instrumen_id','=','tb_instrumen.id')
            ->where('tb_ring_kk_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
            ->where('tb_ring_kk_alat.tahun','=',$tahun)
            ->where('tb_ring_kk_alat.siklus','=',$siklus)
            ->where('tb_ring_kk_alat.form','=','kimia klinik')
            ->where('tb_ring_kk_alat.tipe','=',$type);
        })
        ->leftJoin('tb_zscore_alat',function($join) use($tahun,$siklus,$type,$id){
            $join->on('tb_zscore_alat.alat','=','tb_instrumen.id')
            ->where('tb_zscore_alat.parameter','=',DB::raw('tb_instrumen.id_parameter'))
            ->where('tb_zscore_alat.tahun','=',$tahun)
            ->where('tb_zscore_alat.id_registrasi','=',$id)
            ->where('tb_zscore_alat.siklus','=',$siklus)
            ->where('tb_zscore_alat.form','=','kimia klinik')
            ->where('tb_zscore_alat.type','=',$type);
        })
        ->where('hp_headers.id_registrasi','=',$id)
        ->where('hp_headers.type','=',$type)
        ->whereRaw('hp_details.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")')
        ->select(
            'parameter.nama_parameter',
            'parameter.id as id_parameter',
            'tb_instrumen.instrumen as nama_instrumen',
            'tb_instrumen.id as id_instrumen',
            'hp_details.hasil_pemeriksaan',
            'tb_sd_median_alat.sd',
            'tb_sd_median_alat.median',
            'tb_ring_kk_alat.batas_bawah',
            'tb_ring_kk_alat.batas_atas',
            'tb_ring_kk_alat.ring_1_bawah',
            'tb_ring_kk_alat.ring_1_atas',
            'tb_ring_kk_alat.ring_2_bawah',
            'tb_ring_kk_alat.ring_2_atas',
            'tb_ring_kk_alat.ring_3_bawah',
            'tb_ring_kk_alat.ring_3_atas',
            'tb_zscore_alat.zscore as zscore',
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan < tb_ring_kk_alat.batas_bawah
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-")
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_bawah'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_1_bawah AND tb_ring_kk_alat.ring_1_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_1'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_2_bawah AND tb_ring_kk_alat.ring_2_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_2'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_alat.ring_3_bawah AND tb_ring_kk_alat.ring_3_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.alat = tb_ring_kk_alat.instrumen_id
                    AND
                HPD.parameter_id = tb_ring_kk_alat.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan > tb_ring_kk_alat.batas_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_atas'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                        TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 1
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara11'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_alat as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.alat =  tb_instrumen.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_3')
        )
        // ->where()
        ->get();
        return view('evaluasi.kimiaklinik.grafiknilaialat.zscore_parameter', compact('param','tahun','type','siklus', 'id','data'));
    }

    public function gnilaisamaalat(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        $alat = DB::table('tb_instrumen')->where('id', $input['alat'])->first();
        $peserta = DB::table('hp_details')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_details.parameter_id', $input['parameter'])
                ->where('tb_instrumen.id', $input['alat'])
                ->where('tb_registrasi.id', $id)
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['type'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('hp_details.hasil_pemeriksaan')
                ->first();
        $ring = DB::table('tb_ring_kk')
                ->select('tb_ring_kk.*',
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_bawah'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_1_bawah AND tb_ring_kk.ring_1_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_1'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_2_bawah AND tb_ring_kk.ring_2_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_2'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_3_bawah AND tb_ring_kk.ring_3_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_bawah'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_3'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_4_bawah AND tb_ring_kk.ring_4_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_4'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_5_bawah AND tb_ring_kk.ring_5_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_5'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN tb_instrumen ON tb_instrumen.id = hp_details.alat
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        tb_instrumen.id = "'.$input['alat'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_atas')

                )
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['type'])
                ->where('tahun', $input['tahun'])
                ->first();

        $median = DB::table('tb_sd_median_alat')
                ->where('parameter', $input['parameter'])
                ->where('alat', $input['alat'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['type'])
                ->where('tahun', $input['tahun'])
                ->first();
        // dd($median);
        return view('evaluasi.kimiaklinik.grafiknilaialat.grafik', compact('peserta','parameter','datas','ring', 'median', 'alat'));
    }

    public function hemnilaisamaalat(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','hematologi')->get();
        $alat = DB::table('hp_headers')
                ->join('hp_details', 'hp_details.hp_header_id','=', 'hp_headers.id')
                ->join('tb_instrumen', 'tb_instrumen.id', '=', 'hp_details.alat')
                ->where('id_parameter','=','hematologi')
                ->orderBy('bagian', 'asc')
                ->groupBy('hp_details.alat')
                ->select('hp_details.alat', 'tb_instrumen.instrumen')
                ->get();
        return view('evaluasi.hematologi.grafiknilaialat.zscore_parameter', compact('param','tahun','type','siklus', 'id', 'alat'));
    }

    public function ghemnilaisamaalat(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        // $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        // $alat = DB::table('tb_instrumen')->where('id', $input['alat'])->first();
        $datapes = DB::table('hp_headers')
                ->join('hp_details', 'hp_details.hp_header_id','=', 'hp_headers.id')
                ->join('tb_instrumen', 'tb_instrumen.id', '=', 'hp_details.alat')
                ->leftjoin('parameter', 'parameter.id', '=', 'hp_details.parameter_id')
                ->where('id_parameter','=','hematologi')
                ->where('hp_headers.id_registrasi', $id)
                ->orderBy('parameter.bagian', 'asc')
                ->get();
        // dd($datapes);
        foreach ($datapes as $key => $val) {
            $data = DB::table('parameter')
                    ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                    ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                    ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                    ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                    ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                    ->where('parameter.id', $val->parameter_id)
                    ->where('tb_instrumen.id', $val->alat)
                    ->where('hp_headers.siklus', $input['siklus'])
                    ->where('hp_headers.type', $input['type'])
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                    ->select('hp_details.id', 'hp_details.hasil_pemeriksaan', DB::raw('count(hp_details.hasil_pemeriksaan) as jumlah'))
                    ->groupBy('hp_details.id')
                    ->get();
                foreach ($data as $key => $valu) {
                    $peserta = DB::table('hp_details')
                            ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                            ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                            ->where('hp_details.id', $valu->id)
                            ->where('tb_registrasi.id', $id)
                            ->select('hp_details.hasil_pemeriksaan')
                            ->get();
                    $valu->peserta = $peserta;
                }
            $val->data = $data;
            $ring = DB::table('tb_ring')
                    ->where('parameter', $val->parameter_id)
                    ->where('siklus', $input['siklus'])
                    ->where('tipe', $input['type'])
                    ->where('tahun', $input['tahun'])
                    ->get();
            $val->ring = $ring;
            $median = DB::table('tb_sd_median_alat')
                    ->where('parameter', $val->parameter_id)
                    ->where('alat', $val->alat)
                    ->where('siklus', $input['siklus'])
                    ->where('tipe', $input['type'])
                    ->where('tahun', $input['tahun'])
                    ->first();
            $val->median = $median;
        }
             // dd($datapes);
        return view('evaluasi.hematologi.grafiknilaialat.grafik', compact('datapes','parameter','datas','ring', 'median', 'alat'));
    }

    public function hemnilaisamametode(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','hematologi')->get();
        return view('evaluasi.hematologi.grafiknilaimetode.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function ghemnilaisamametode(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        // $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        // $metode = DB::table('metode_pemeriksaan')->where('id', $input['metode'])->first();

        $datapes = DB::table('hp_headers')
                ->join('hp_details', 'hp_details.hp_header_id','=', 'hp_headers.id')
                ->join('metode_pemeriksaan', 'metode_pemeriksaan.id', '=', 'hp_details.kode_metode_pemeriksaan')
                ->leftjoin('parameter', 'parameter.id', '=', 'hp_details.parameter_id')
                ->where('parameter.kategori','=','hematologi')
                ->where('hp_headers.id_registrasi', $id)
                ->orderBy('parameter.bagian', 'asc')
                ->get();
            foreach ($datapes as $key => $val) {
                $data = DB::table('parameter')
                        ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                        ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                        ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('parameter.id', $val->parameter_id)
                        ->where('metode_pemeriksaan.id', $val->kode_metode_pemeriksaan)
                        ->where('hp_headers.siklus', $input['siklus'])
                        ->where('hp_headers.type', $input['type'])
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                        ->select('hp_details.id', 'hp_details.hasil_pemeriksaan', DB::raw('count(hp_details.hasil_pemeriksaan) as jumlah'))
                        ->groupBy('hp_details.id')
                        ->get();
                    foreach ($data as $key => $valu) {
                        $peserta = DB::table('hp_details')
                                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                                ->where('hp_details.id', $valu->id)
                                ->where('tb_registrasi.id', $id)
                                ->select('hp_details.hasil_pemeriksaan')
                                ->get();
                        $valu->peserta = $peserta;
                    }
                $val->data = $data;
                $ring = DB::table('tb_ring')
                        ->where('parameter', $val->parameter_id)
                        ->where('siklus', $input['siklus'])
                        ->where('tipe', $input['type'])
                        ->where('tahun', $input['tahun'])
                        ->get();
                $val->ring = $ring;

                $median = DB::table('tb_sd_median_metode')
                        ->where('parameter', $val->parameter_id)
                        ->where('metode', $val->kode_metode_pemeriksaan)
                        ->where('siklus', $input['siklus'])
                        ->where('tipe', $input['type'])
                        ->where('tahun', $input['tahun'])
                        ->first();
                $val->median = $median;
            }
        // dd($median);
        return view('evaluasi.hematologi.grafiknilaimetode.grafik', compact('datapes','parameter','datas','ring', 'median', 'metode'));
    }

    public function nilaisamametode(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $data =  DB::table('tb_registrasi')
        ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
        ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
        ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
        ->where('tb_registrasi.id', $id)
        ->where('sub_bidang.id', '=', '2')
        ->where('status', 3)
        ->where(function($query)
            {
                $query->where('status_data1', 2)
                    ->orwhere('status_data2', 2);
            })
        ->where(function($query)
            {
                if ('pemeriksaan' != 'done') {
                    $query->orwhere('pemeriksaan2', 'done');
                }
                if ('pemeriksaan2' != 'done') {
                    $query->orwhere('pemeriksaan', 'done');
                }
            })
        ->first();
        $param = DB::table('metode_pemeriksaan')->where('parameter.kategori','=','kimia klinik')
        ->join('hp_details','hp_details.kode_metode_pemeriksaan','=','metode_pemeriksaan.id')
        ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
        ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
        ->leftJoin('tb_sd_median_metode',function($join) use($tahun,$siklus,$type){
            $join->on('tb_sd_median_metode.metode','=','metode_pemeriksaan.id')
            ->where('tb_sd_median_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
            ->where('tb_sd_median_metode.tahun','=',$tahun)
            ->where('tb_sd_median_metode.siklus','=',$siklus)
            ->where('tb_sd_median_metode.form','=','kimia klinik')
            ->where('tb_sd_median_metode.tipe','=',$type);
        })
        ->leftJoin('tb_ring_kk_metode',function($join) use($tahun,$siklus,$type){
            $join->on('tb_ring_kk_metode.metode_id','=','metode_pemeriksaan.id')
            ->where('tb_ring_kk_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
            ->where('tb_ring_kk_metode.tahun','=',$tahun)
            ->where('tb_ring_kk_metode.siklus','=',$siklus)
            ->where('tb_ring_kk_metode.form','=','kimia klinik')
            ->where('tb_ring_kk_metode.tipe','=',$type);
        })
        ->leftJoin('tb_zscore_metode',function($join) use($tahun,$siklus,$type,$id){
            $join->on('tb_zscore_metode.metode','=','metode_pemeriksaan.id')
            ->where('tb_zscore_metode.parameter','=',DB::raw('metode_pemeriksaan.parameter_id'))
            ->where('tb_zscore_metode.tahun','=',$tahun)
            ->where('tb_zscore_metode.id_registrasi','=',$id)
            ->where('tb_zscore_metode.siklus','=',$siklus)
            ->where('tb_zscore_metode.form','=','kimia klinik')
            ->where('tb_zscore_metode.type','=',$type);
        })
        ->where('hp_headers.id_registrasi','=',$id)
        ->where('hp_headers.type','=',$type)
        ->whereRaw('hp_details.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")')
        ->select(
            'parameter.nama_parameter',
            'parameter.id as id_parameter',
            'metode_pemeriksaan.metode_pemeriksaan as nama_metode',
            'metode_pemeriksaan.id as id_metode',
            'hp_details.hasil_pemeriksaan',
            'tb_sd_median_metode.sd',
            'tb_sd_median_metode.median',
            'tb_ring_kk_metode.batas_bawah',
            'tb_ring_kk_metode.batas_atas',
            'tb_ring_kk_metode.ring_1_bawah',
            'tb_ring_kk_metode.ring_1_atas',
            'tb_ring_kk_metode.ring_2_bawah',
            'tb_ring_kk_metode.ring_2_atas',
            'tb_ring_kk_metode.ring_3_bawah',
            'tb_ring_kk_metode.ring_3_atas',
            'tb_zscore_metode.zscore as zscore',
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan < tb_ring_kk_metode.batas_bawah
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-")
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_bawah'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_1_bawah AND tb_ring_kk_metode.ring_1_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_1'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_2_bawah AND tb_ring_kk_metode.ring_2_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_2'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                    HPD.hasil_pemeriksaan BETWEEN tb_ring_kk_metode.ring_3_bawah AND tb_ring_kk_metode.ring_3_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as ring_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM hp_details as HPD
                INNER JOIN hp_headers as HPH ON HPH.id = HPD.hp_header_id
                INNER JOIN tb_registrasi as TBR ON TBR.id  = HPH.id_registrasi
                WHERE HPD.kode_metode_pemeriksaan = tb_ring_kk_metode.metode_id
                    AND
                HPD.parameter_id = tb_ring_kk_metode.parameter
                    AND
                HPD.hasil_pemeriksaan NOT IN ("-","0.0","0","0.00",\'"_"\',\'"-"\',"_")
                    AND
                HPD.hasil_pemeriksaan > tb_ring_kk_metode.batas_atas
                    AND
                HPH.siklus = "'.$siklus.'"
                    AND
                HPH.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as out_atas'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_3'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) < -2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) >= -2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > -1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 1
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_kurang_antara11'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 1
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 2
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara12'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 2
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) <= 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_antara23'),
            DB::raw('IFNULL((
                SELECT COUNT(*) FROM tb_zscore_metode as TBZ
                INNER JOIN tb_registrasi as TBR ON TBR.id  = TBZ.id_registrasi
                WHERE TBZ.parameter =  parameter.id
                    AND
                TBZ.metode =  metode_pemeriksaan.id
                    AND
                CAST(TBZ.zscore AS decimal(11,2)) > 3
                    AND
                TBZ.zscore != "-"
                    AND
                TBZ.siklus = "'.$siklus.'"
                    AND
                TBZ.type = "'.$type.'"
                    AND
                YEAR(TBR.created_at) = "'.$tahun.'"
            ),0) as zscore_3')
        )
        // ->where()
        ->get();
        return view('evaluasi.kimiaklinik.grafiknilaimetode.zscore_parameter', compact('param','tahun','type','siklus', 'id','data'));
    }

    public function gnilaisamametode(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        $metode = DB::table('metode_pemeriksaan')->where('id', $input['metode'])->first();
        $peserta = DB::table('hp_details')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_details.parameter_id', $input['parameter'])
                ->where('metode_pemeriksaan.id', $input['metode'])
                ->where('tb_registrasi.id', $id)
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['type'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('hp_details.hasil_pemeriksaan')
                ->first();
        $ring = DB::table('tb_ring_kk')
                ->select('tb_ring_kk.*',
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_bawah'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_1_bawah AND tb_ring_kk.ring_1_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_1'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_2_bawah AND tb_ring_kk.ring_2_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_2'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_3_bawah AND tb_ring_kk.ring_3_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_bawah'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_3'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_4_bawah AND tb_ring_kk.ring_4_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_4'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                            hp_details.hasil_pemeriksaan BETWEEN tb_ring_kk.ring_5_bawah AND tb_ring_kk.ring_5_atas
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as ring_5'),
                    DB::raw('IFNULL((
                        SELECT COUNT(*) FROM hp_details
                        INNER JOIN hp_headers ON hp_headers.id = hp_details.hp_header_id
                        INNER JOIN metode_pemeriksaan ON metode_pemeriksaan.id = hp_details.kode_metode_pemeriksaan
                        INNER JOIN tb_registrasi ON tb_registrasi.id = hp_headers.id_registrasi
                        WHERE hp_details.parameter_id = tb_ring_kk.parameter
                            AND
                        hp_details.hasil_pemeriksaan < tb_ring_kk.batas_bawah
                            AND
                        hp_headers.siklus = "'.$input['siklus'].'"
                            AND
                        metode_pemeriksaan.id = "'.$input['metode'].'"
                            AND
                        hp_headers.type = "'.$input['type'].'"
                            AND
                        YEAR(tb_registrasi.created_at) = "'.$input['tahun'].'"
                    ),0) as out_atas')

                )
                ->where('parameter', $input['parameter'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['type'])
                ->where('tahun', $input['tahun'])
                ->first();
        $median = DB::table('tb_sd_median_metode')
                ->where('parameter', $input['parameter'])
                ->where('metode', $input['metode'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['type'])
                ->where('tahun', $input['tahun'])
                ->first();
        // dd($median);
        return view('evaluasi.kimiaklinik.grafiknilaimetode.grafik', compact('data','parameter','datas','ring', 'median', 'metode'));
    }

    public function zkim(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.grafik.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function gzkim(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $parameter = $input['parameter'];
        $type = $input['type'];
        $siklus = $input['siklus'];
        $param = DB::table('parameter')->where('id',$parameter)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` where form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia klinik' GROUP BY parameter, type";

        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','kimia klinik')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('type','=',$type)
                ->where('siklus','=',$siklus)
                ->get();
        // dd($type);
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        return view('evaluasi.kimiaklinik.grafik.grafik', compact('data','tahun','type','siklus','param','datap'));
    }

    public function alatzkim(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.grafikalat.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function alatgzkim(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $parameter = $input['parameter'];
        $alat = $input['alat'];
        $type = $input['type'];
        $siklus = $input['siklus'];
        $param = DB::table('parameter')->where('id',$parameter)->first();
        $alatna = DB::table('tb_instrumen')->where('id',$alat)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` where form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, alat, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_alat` WHERE form = 'kimia klinik' GROUP BY parameter, type";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore_alat
                where parameter = '".$parameter."' AND alat = '".$alat."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_alat.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','kimia klinik')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('alat','=',$alat)
                ->where('type','=',$type)
                ->where('siklus','=',$siklus)
                ->get();
        // dd($data);
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        return view('evaluasi.kimiaklinik.grafikalat.grafik', compact('data','tahun','type','siklus','param','datap', 'alatna'));
    }

    public function metodezkim(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.grafikmetode.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function metodegzkim(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $parameter = $input['parameter'];
        $metode = $input['metode'];
        $type = $input['type'];
        $siklus = $input['siklus'];
        $param = DB::table('parameter')->where('id',$parameter)->first();
        $metodena = DB::table('metode_pemeriksaan')->where('id',$metode)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` where form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'kimia klinik' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, metode, parameter, type, siklus, id_registrasi
                FROM `tb_zscore_metode` WHERE form = 'kimia klinik' GROUP BY parameter, type";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore_metode
                where parameter = '".$parameter."' AND metode = '".$metode."' AND form = 'kimia klinik' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore_metode.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','kimia klinik')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('metode','=',$metode)
                ->where('type','=',$type)
                ->where('siklus','=',$siklus)
                ->get();
        // dd($data);
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        return view('evaluasi.kimiaklinik.grafikmetode.grafik', compact('data','tahun','type','siklus','param','datap', 'metodena'));
    }

    public function zkimair(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia air')->get();
        return view('evaluasi.kimiaair.grafik.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function gzkimair(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $type = $request->get('x');
        $tahun = $input['tahun'];
        $parameter = $input['parameter'];
        $siklus = $input['siklus'];

        $param = DB::table('parameter')->where('id',$parameter)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` where form = 'kimia air' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '6' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air' GROUP BY parameter, type";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','kimia air')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('siklus','=',$siklus)
                ->get();
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        return view('evaluasi.kimiaair.grafik.grafik', compact('data','tahun','siklus','param','datap'));
    }

    public function zkimairterbatas(\Illuminate\Http\Request $request, $id)
    {
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $tahun = $q2->year;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $id = $id;
        $param = DB::table('parameter')->where('kategori','=','kimia air terbatas')->get();
        return view('evaluasi.kimiaairterbatas.grafik.zscore_parameter', compact('param','tahun','type','siklus', 'id'));
    }

    public function gzkimairterbatas(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $type = $request->get('x');
        $parameter = $input['parameter'];
        $siklus = $input['siklus'];

        $param = DB::table('parameter')->where('id',$parameter)->first();

        $sql ="SELECT
                    sum(if(zscore < -3, 1, 0)) as nilai1, '< -3' as nilainya, '0' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` where form = 'kimia air terbatas' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore < -2 and zscore >= -3, 1, 0)) as nilai1, '< -2 sd -3' as nilainya, '1' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore <= -1 and zscore >= -2, 1, 0)) as nilai1, '-1 sd -2' as nilainya, '2' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > -1 and zscore <= 1, 1, 0)) as nilai1, '> -1 sd 1' as nilainya, '3' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 1 and zscore <= 2, 1, 0)) as nilai1, '> 1 sd 2' as nilainya, '4' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 2 and zscore <= 3, 1, 0)) as nilai1, '> 2 sd 3' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' GROUP BY parameter, type
                UNION
                SELECT
                    sum(if(zscore > 3, 1, 0)) as nilai1, '> 3' as nilainya, '5' as sort, form, tahun, parameter, type, siklus, id_registrasi
                FROM `tb_zscore` WHERE form = 'kimia air terbatas' GROUP BY parameter, type";
        $sql2 =" SELECT count(*) as nilai1, '< -3' as nilainya, 'a' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore < -3
                UNION
                SELECT count(*) as nilai1, '< -2 sd -3' as nilainya, 'b' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND zscore < -2 and zscore >= -3
                UNION
                SELECT count(*) as nilai1, '-1 sd -2' as nilainya, 'c' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore <= -1 AND zscore >= -2
                UNION
                SELECT count(*) as nilai1, '> -1 sd 1' as nilainya, 'd' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > -1 AND zscore <= 1
                UNION
                SELECT count(*) as nilai1, '> 1 sd 2' as nilainya, 'e' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 1 AND zscore <= 2
                UNION
                SELECT count(*) as nilai1, '> 2 sd 3' as nilainya, 'f' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 2 AND zscore <= 3
                UNION
                SELECT count(*) as nilai1, '> 3' as nilainya, 'g' as sort from tb_zscore
                where parameter = '".$parameter."' AND form = 'kimia air terbatas' AND siklus = '".$siklus."' AND id_registrasi = '".$id."' AND type = '".$type."' AND tahun = '".$tahun."' AND tb_zscore.zscore > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->where('form','=','kimia air terbatas')
                ->where('tahun','=',$tahun)
                ->where('parameter','=',$parameter)
                ->where('siklus','=',$siklus)
                ->get();
        $datap = DB::table(DB::raw("($sql2) zone"))
                ->select('*')
                ->orderBy('sort', 'asc')
                ->get();
        return view('evaluasi.kimiaairterbatas.grafik.grafik', compact('data','tahun','siklus','param','datap'));
    }
}
