<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;
use Carbon\Carbon;

use App\HpHeader;
use App\Parameter;
use App\MetodePemeriksaan;
use App\HpDetail;
use App\ZScore;
use App\ZScoreAlat;
use App\ZScoreMetode;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\View;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\register as Register;
use Redirect;
use Validator;
use Session;
use App\Jobs\Evaluasi;
use App\JobsEvaluasi;
use App\TanggalEvaluasi;

class HematologiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {
        $data = Parameter::where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'hematologi')->orderBy('bagian', 'asc')->get();
        $type = $request->get('x');
        $siklus = $request->get('y');

        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;
        return view('hasil_pemeriksaan/hematologi', compact('data', 'perusahaan', 'type','siklus','date','instrumen','register'));
    }

    public function view(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');

        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $tanggal = date('d-m-Y');
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('tb_instrumen', 'hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->select('parameter.*','hp_details.alat as Alat', 'tb_instrumen.instrumen', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.instrument_lain', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan','hp_headers.kualitas_bahan')
            ->where('parameter.kategori', 'hematologi')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->orderBy('bagian', 'asc')
            ->get();
            
        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;

        // return view('cetak_hasil/hematologi', compact('data', 'perusahaan', 'datas', 'type'));
        $pdf = PDF::loadview('cetak_hasil/hematologi', compact('data', 'perusahaan','datas', 'type' ,'siklus','date','register','tanggal'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Hematologi.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;
        // $type = $request->get('type');
        $type = $input['type'];

        $validasi = HpHeader::where(['id_registrasi'=>$id , 'siklus'=>$siklus , 'type'=>$type])->get();

        $rules = array(
            'kode_peserta' => 'required',
            'tanggal_penerimaan' => 'required',
            'tanggal_pemeriksaan' => 'required',
            'kualitas' => 'required'
        );

        $messages = array(
            'kode_peserta.required' => 'Kode Peserta Wajib diisi',
            'tanggal_penerimaan.required' => 'Tanggal Penerimaan wajib diisi',
            'tanggal_pemeriksaan.required' => 'Tanggal Pemeriksaan wajib diisi',
            'kualitas.required' => 'Kualitas wajib dipliih'
        );
        if (count($validasi)>0) {
            Session::flash('message', 'Hasil Hematologi Sudah Ada!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
        }else{
            $saveHeader = new HpHeader;
            $saveHeader->kode_lab = $input['kode_peserta'];
            $saveHeader->kode_peserta = $perusahaanID;
            $saveHeader->tgl_pemeriksaan = date('Y-m-d', strtotime($input['tanggal_pemeriksaan']));
            $saveHeader->tgl_penerimaan = date('Y-m-d', strtotime($input['tanggal_penerimaan']));
            $saveHeader->kualitas_bahan = $input['kualitas'];
            $saveHeader->catatan = $input['catatan'];
            $saveHeader->kode_bahan = $input['kode_bahan'];
            $saveHeader->kategori = 'hematologi';
            $saveHeader->id_registrasi = $id;
            $saveHeader->penanggung_jawab = $input['penanggung_jawab'];
            $saveHeader->type = $input['type'];
            $saveHeader->siklus = $siklus;
            $saveHeader->save();

            $saveHeaderID = $saveHeader->id;
        $i = 0;
        foreach ($input['parameter_id'] as $parameter_id) {
            $saveDetail = new HpDetail;
            $saveDetail->parameter_id = $input['parameter_id'][$i];
            $saveDetail->hp_header_id = $saveHeaderID;
            $saveDetail->alat = $input['alat'][$i];
            $saveDetail->kode_metode_pemeriksaan = $input['kode'][$i];
            $saveDetail->instrument_lain = $input['instrument_lain'][$i];
            $saveDetail->hasil_pemeriksaan = $input['hasil'][$i];
            $saveDetail->metode_lain = $input['metode_lain'][$i];
            $saveDetail->save();
            $i++;
        }
        $date_sekarang = date('Y-m-d H:i:s');
        DB::table('tanggal_input')->insert(['id_register' => $id, 'siklus' => $siklus, 'type' => $input['type'], 'input_date' => $date_sekarang]);
        if ($siklus == '1') {
            Register::where('id',$id)->update(['siklus_1'=>'done']);
            if ($input['type'] == 'a') {
                Register::where('id',$id)->update(['pemeriksaan'=>'done', 'status_data1'=>'1']);
            }else{
                Register::where('id',$id)->update(['pemeriksaan2'=>'done', 'status_data2'=>'1']);
            }
        }else{
            Register::where('id',$id)->update(['siklus_2'=>'done']);
            if ($input['type'] == 'a') {
                Register::where('id',$id)->update(['rpr1'=>'done', 'status_datarpr1'=>'1']);
            }else{
                Register::where('id',$id)->update(['rpr2'=>'done', 'status_datarpr2'=>'1']);
            }
        }
        return redirect('hasil-pemeriksaan');
        }
    }

    public function edit(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );

        $date = $tahun->year;

        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'hematologi')->orderBy('bagian', 'asc')->get();
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail')
            ->where('parameter.kategori', 'hematologi')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->orderBy('bagian', 'asc')
            ->get();
            foreach ($data as $key => $val) {
                $metode = MetodePemeriksaan::where('parameter_id', $val->id)->get();
                $val->metode = $metode;
            }
            // dd($data);
        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;

        return view('edit_hasil/hematologi', compact('data', 'perusahaan', 'datas', 'type','siklus','date','instrumen'));
    }


    public function update(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $type = $request->get('x');
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;

        // dd($input);
        $SaveHeader['tgl_penerimaan'] = $request->tanggal_penerimaan;
        $SaveHeader['tgl_pemeriksaan'] = $request->tanggal_pemeriksaan;
        $SaveHeader['kualitas_bahan'] = $request->kualitas;
        $SaveHeader['kode_bahan'] = $request->kode_bahan;
        $SaveHeader['catatan'] = $request->catatan;
        $SaveHeader['penanggung_jawab'] = $request->penanggung_jawab;

        HpHeader::where('id_registrasi',$id)->where('siklus', $siklus)->where('type', $type)->update($SaveHeader);
        // dd($input);
        $i = 0;
        foreach ($input['alat'] as $alat) {
            if($alat != ''){
                $SaveDetail['alat'] = $request->alat[$i];
                $SaveDetail['kode_metode_pemeriksaan'] = $request->kode[$i];
                $SaveDetail['instrument_lain'] = $request->instrument_lain[$i];
                $SaveDetail['metode_lain'] = $request->metode_lain[$i];
                $SaveDetail['hasil_pemeriksaan'] = $request->hasil[$i];
                HpDetail::where('id', $request->id_detail[$i])->update($SaveDetail);
            }
            $i++;
        }
        if ($request->simpan == "Kirim" || $request->simpan == "Validasi") {
            if ($siklus == '1') {
                Register::where('id',$id)->update(['siklus_1'=>'done']);
                if ($input['type'] == 'a') {
                    Register::where('id',$id)->update(['pemeriksaan'=>'done', 'status_data1'=>'2']);
                }else{
                    Register::where('id',$id)->update(['pemeriksaan2'=>'done', 'status_data2'=>'2']);
                }
            }else{
                Register::where('id',$id)->update(['siklus_2'=>'done']);
                if ($input['type'] == 'a') {
                    Register::where('id',$id)->update(['rpr1'=>'done', 'status_datarpr1'=>'2']);
                }else{
                    Register::where('id',$id)->update(['rpr2'=>'done', 'status_datarpr2'=>'2']);
                }
            }
            if ($request->simpan == "Validasi") {
                DB::table('validasi_input')->where('id_register', $id)->where('siklus', $siklus)->where('type', $input['type'])->delete();
                DB::table('validasi_input')->insert(
                    ['id_register' => $id, 'siklus' => $siklus, 'type' => $input['type'], 'status' => 'done']
                );
                return back();
            }else{
                return redirect('edit-hasil');
            }
        }else{
            return back();
        }
    }

    public function evaluasi(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );

        $date = $tahun->year;

        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.alat', 'hp_details.hasil_pemeriksaan')
                ->orderBy('parameter.bagian', 'asc')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'hematologi')
                    ->get();
            $val->sd = $sd;
        }
        $zscore = DB::table('tb_zscore')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();
        //  dd($data);

        return view('evaluasi.hematologi.zscore.index', compact('data', 'input', 'date', 'type','zscore','id','siklus'));
    }

    public function autoSaveParameter(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $siklus = $input['siklus'];
        $type = $input['type'];
        $date = $input['date'];
        Session::flash('message', 'System sedang melakukan proses evaluasi.');
        Session::flash('alert-class', 'alert-info');
        $jobs = new JobsEvaluasi;
        $jobs->name = 'evaluasi-parameter';
        $jobs->description = 'Evaluasi Parameter';
        $jobs->status = 0;
        $jobs->isread = 0;
        $jobs->save();
        Evaluasi::dispatch($input,$jobs->name)->onConnection('redis');
        return redirect('evaluasi/hitung-zscore/hematologi');
    }


    public function autoSaveKimiaklinik(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $siklus = $input['siklus'];
        $type = $input['type'];
        $date = $input['date'];
        Session::flash('message', 'System sedang melakukan proses evaluasi.');
        Session::flash('alert-class', 'alert-info');
        $jobs = new JobsEvaluasi;
        $jobs->name = 'evaluasi-kimiaklinik';
        $jobs->description = 'Evaluasi Kimia Klinik';
        $jobs->status = 0;
        $jobs->isread = 0;
        $jobs->save();
        Evaluasi::dispatch($input,$jobs->name)->onConnection('redis');
        return redirect('evaluasi/hitung-zscore/kimiaklinik');
    }
    public function cetak(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );

        $date = $tahun->year;
        $tanggal = date('Y m d');


      if($type == 'a'){
        $urut = 'A';
      }else{
        $urut = 'B';
      }

      // dd($urut);
        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.instrument_lain', 'hp_details.alat', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                ->orderBy('parameter.bagian', 'asc')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'hematologi')
                    ->get();
            $val->sd = $sd;
        }
        $zscore = DB::table('tb_zscore')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();

        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();

        $evaluasi = TanggalEvaluasi::where('form','=','Hematologi')->first(); 
         // dd($data);
            $pdf = PDF::loadview('evaluasi/hematologi/zscore/print/cetak', compact('tanggal','data','datas', 'input','urut', 'date', 'type','zscore','id','siklus','evaluasi'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
             return $pdf->stream('Hematologi.pdf');
    }

    public function cetaksemua(\Illuminate\Http\Request $request, $id){
      $type = $request->get('x');
      $siklus = $request->get('y');
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );

        $date = $tahun->year;
      $tanggal = date('Y m d');

      if($type == 'a'){
        $urut = 'A';
      }else{
        $urut = 'B';
      }

      $data = DB::table('parameter')
              ->join('hp_details','hp_details.parameter_id','=','parameter.id')
              ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
              ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
              ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
              ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
              ->where('hp_headers.siklus', $siklus)
              ->where('hp_headers.type', $type)
              ->where('tb_registrasi.id', $id)
              ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
              ->select('hp_headers.kode_lab', 'hp_headers.type', 'parameter.nama_parameter', 'parameter.id as parameterID', 'tb_instrumen.id as alatID','tb_instrumen.instrumen', 'parameter.id as id_parameter', 'metode_pemeriksaan.id as metodeID',  'hp_details.instrument_lain', 'hp_details.alat', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
              ->orderBy('parameter.bagian', 'asc')
              ->get();
              // dd($data);
      foreach ($data as $key => $val) {
          $sd = DB::table('tb_sd_median')
                  ->where('parameter', $val->parameterID)
                  ->where('siklus', $siklus)
                  ->where('tahun', $date)
                  ->where('tipe', $type)
                  ->where('form', '=', 'hematologi')
                  ->get();
          $val->sd = $sd;

          $sd_alat = DB::table('tb_sd_median_alat')
                  ->where('parameter', $val->id_parameter)
                  ->where('alat', $val->alatID)
                  ->where('siklus', $siklus)
                  ->where('tahun', $date)
                  ->where('tipe', $type)
                  ->where('form', '=', 'hematologi')
                  ->get();
          $val->sd_alat = $sd_alat;

          $sd_metode = DB::table('tb_sd_median_metode')
                  ->where('metode', $val->metodeID)
                  ->where('siklus', $siklus)
                  ->where('tahun', $date)
                  ->where('tipe', $type)
                  ->where('form', '=', 'hematologi')
                  ->get();
          $val->sd_metode = $sd_metode;

      }

      $zscore_metode = DB::table('tb_zscore_metode')
              ->where('siklus', $siklus)
              ->where('tahun', $date)
              ->where('type', $type)
              ->where('id_registrasi', $id)
              ->get();

      $zscore_alat = DB::table('tb_zscore_alat')
                  ->where('siklus', $siklus)
                  ->where('tahun', $date)
                  ->where('type', $type)
                  ->where('id_registrasi', $id)
                  ->get();

      $zscore = DB::table('tb_zscore')
                  ->where('siklus', $siklus)
                  ->where('tahun', $date)
                  ->where('type', $type)
                  ->where('id_registrasi', $id)
                  ->get();

      $datas = HpHeader::where('id_registrasi', $id)
          ->where('type', $type)
          ->where('siklus', $siklus)
          ->first();

        $evaluasi = TanggalEvaluasi::where('form','=','Hematologi')->where('siklus', $siklus)->where('tahun', $date)->first(); 
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        // dd($evaluasi);
        if ($evaluasi != NULL) {
            if (count($zscore) > 0) {
                $pdf = PDF::loadview('evaluasi/hematologi/zscorecetak/print/cetak', compact('urut','tanggal','data','datas', 'input', 'date', 'type','zscore','id','siklus','zscore_alat','zscore_metode','evaluasi','perusahaan','register'))
                    ->setPaper('a4', 'landscape')
                    ->setwarnings(false);
                return $pdf->stream('Hematologi.pdf');
            }else{
                Session::flash('message', 'Hematologi belum dievaluasi!');
                Session::flash('alert-class', 'alert-danger');
                return back();
            }
        }else{
            Session::flash('message', 'Tanggal Cetak Evaluasi Hematologi belum di masukan!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }

    public function insertevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );

        $date = $tahun->year;
        $input = $request->all();

        $i = 0;
        foreach ($input['parameter'] as $alat) {
            $Save = new ZScore;
            $Save->id_registrasi = $id;
            $Save->parameter = $input['parameter'][$i];
            $Save->zscore = $input['zscore'][$i];
            $Save->type = $type;
            $Save->siklus = $siklus;
            $Save->tahun = $date;
            $Save->form = 'hematologi';
            $Save->save();
            $i++;
        }
        return redirect('evaluasi/hitung-zscore/hematologi');
    }

    public function evaluasimetode(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );

        $date = $tahun->year;
      
        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'metode_pemeriksaan.id', 'hp_details.instrument_lain', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                ->orderBy('parameter.bagian', 'asc')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median_metode')
                    ->where('metode', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'hematologi')
                    ->get();
            $val->sd = $sd;
        }
        $zscore = DB::table('tb_zscore_metode')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();
         // dd($data);
        return view('evaluasi.hematologi.zscoremetode.index', compact('data', 'input', 'date', 'type','zscore','id','siklus','type'));
    }

    public function cetakmetode(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');

        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;
        
        $tanggal = date('Y-m-d');
          if($type == 'a'){
            $urut = 'A';
          }else{
            $urut = 'B';
          }
        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'metode_pemeriksaan.id', 'hp_details.instrument_lain', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                ->orderBy('parameter.bagian', 'asc')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median_metode')
                    ->where('metode', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'hematologi')
                    ->get();
            $val->sd = $sd;
        }
        $zscore = DB::table('tb_zscore_metode')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();
        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();

         // dd($data);
            $evaluasi = TanggalEvaluasi::where('form','=','HematologiMetode')->first(); 
        $pdf = PDF::loadview('evaluasi/hematologi/zscoremetode/cetak', compact('urut','tanggal','data','datas', 'input', 'date', 'type','zscore','id','siklus','evaluasi'))
        ->setPaper('a4', 'potrait')
        ->setwarnings(false);
         return $pdf->stream('Hematologi.pdf');
         // dd($data);
        // return view('evaluasi.hematologi.zscoremetode.index', compact('data', 'input', 'date', 'type','zscore'));
    }

    public function insertevaluasimetode(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $input = $request->all();

        $i = 0;
        foreach ($input['metode'] as $metode) {
            $Save = new ZScoreMetode;
            $Save->id_registrasi = $id;
            $Save->parameter = $input['parameter'][$i];
            $Save->metode = $input['metode'][$i];
            $Save->zscore = $input['zscore'][$i];
            $Save->type = $type;
            $Save->siklus = $siklus;
            $Save->tahun = $date;
            $Save->form = 'hematologi';
            $Save->save();
            $i++;
        }
        return redirect('evaluasi/hitung-zscore-metode/hematologi');
    }

    public function evaluasialat(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'tb_instrumen.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                ->orderBy('parameter.bagian', 'asc')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median_alat')
                    ->where('parameter', $val->id_parameter)
                    ->where('alat', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'hematologi')
                    ->get();
            $val->sd = $sd;
        }
        $zscore = DB::table('tb_zscore_alat')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();
         // dd($data);
        return view('evaluasi.hematologi.zscorealat.index', compact('data', 'input', 'date', 'type','zscore','id','siklus','type'));
    }

    public function cetakalat(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $tanggal = date('Y-m-d');

        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'tb_instrumen.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                ->orderBy('parameter.bagian', 'asc')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median_alat')
                    ->where('parameter', $val->id_parameter)
                    ->where('alat', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'hematologi')
                    ->get();
            $val->sd = $sd;
        }
        $zscore = DB::table('tb_zscore_alat')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();

        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();

         // dd($data);
        $pdf = PDF::loadview('evaluasi/hematologi/zscorealat/cetak', compact('tanggal','data','datas', 'input', 'date', 'type','zscore','id','siklus'))
        ->setPaper('a4', 'potrait')
        ->setwarnings(false);
         return $pdf->stream('Hematologi.pdf');
         // dd($data);
        // return view('evaluasi.hematologi.zscorealat.cetak', compact('data', 'input', 'date', 'type','zscore','id','siklus','type'));
    }

    public function insertevaluasialat(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $input = $request->all();

        $i = 0;
        foreach ($input['alat'] as $alat) {
            $Save = new ZScoreAlat;
            $Save->id_registrasi = $id;
            $Save->parameter = $input['parameter'][$i];
            $Save->alat = $input['alat'][$i];
            $Save->zscore = $input['zscore'][$i];
            $Save->type = $type;
            $Save->siklus = $siklus;
            $Save->tahun = $date;
            $Save->form = 'hematologi';
            $Save->save();
            $i++;
        }
        return redirect('evaluasi/hitung-zscore-alat/hematologi');
    }

    public function autoSaveAlat(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $siklus = $input['siklus'];
        $type = $input['type'];
        $date = $input['date'];

        Session::flash('message', 'System sedang melakukan proses evaluasi.');
        Session::flash('alert-class', 'alert-info');
        $jobs = new JobsEvaluasi;
        $jobs->name = 'evaluasi-alat';
        $jobs->description = 'Evaluasi Alat';
        $jobs->status = 0;
        $jobs->isread = 0;
        $jobs->save();
        Evaluasi::dispatch($input,$jobs->name)->onConnection('redis');
        return redirect('evaluasi/hitung-zscore-alat/hematologi');
    }


    public function autoSaveMetode(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $siklus = $input['siklus'];
        $type = $input['type'];
        $date = $input['date'];

        Session::flash('message', 'System sedang melakukan proses evaluasi.');
        Session::flash('alert-class', 'alert-info');
        $jobs = new JobsEvaluasi;
        $jobs->name = 'evaluasi-metode';
        $jobs->description = 'Evaluasi Metode';
        $jobs->status = 0;
        $jobs->isread = 0;
        $jobs->save();
        Evaluasi::dispatch($input,$jobs->name)->onConnection('redis');
        return redirect('evaluasi/hitung-zscore-metode/hematologi');
    }

    public function perusahaan(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, tb_registrasi.kode_lebpes, @rownum := @rownum +1 as rownum,tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('tb_registrasi.bidang','=','1')
                ->where('tb_registrasi.status','>=', 2) 
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $tahun)
                ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '=', 12)
                            ->orwhere('tb_registrasi.siklus', '=', $siklus);
                    })
                ->get();

            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if($siklus == 1) {
                        if ($inputhasil->status_data1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }else{
                        if ($inputhasil->status_datarpr1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if($siklus == 1) {
                        if ($inputhasil->status_data1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }else{
                        if ($inputhasil->status_datarpr1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }

        return view('evaluasi.hematologi.rekapcetakpeserta.perusahaan');
    }

    public function dataperusahaan(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id) 
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
        return view('evaluasi.hematologi.rekapcetakpeserta.dataperusahaan', compact('data', 'siklus'));
    }


}