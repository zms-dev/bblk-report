<?php
namespace App\Http\Controllers;
use DB;
use Input;
use PDF;

use App\HpHeader;
use App\Parameter;
use App\MetodePemeriksaan;
use App\HpDetail;
use App\ZScore;
use App\ZScoreAlat;
use App\ZScoreMetode;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\register as Register;
use App\TbReagenImunologi as ReagenImunologi;
use App\TbHp;
use App\Rujukanurinalisa;
use Redirect;
use Validator;
use Session;
use App\User;
use App\SdMedian;
use App\SdMedianMetode;
use App\Ring;
use App\CatatanImun;
use App\SdMedianAlat;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Excel;
use App\statusbakteri;
use App\UploadLaporan;


class LapAkhirEvaluasiController extends Controller
{
    public function laporanakhirhematologi(\Illuminate\Http\Request $request,$id)
    {   
        $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, date(upload_lap_akhir.updated_at) as tanggal , @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')->where('upload_lap_akhir.parameter','=','1')->orderBy('id','desc')->first();
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang',1)
                ->first();
        // dd($data);
        
        $t = date('Y');

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_hema', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri HEMATOLOGI.pdf');
    }

    public function laporanakhirhematologipenutup(\Illuminate\Http\Request $request,$id)
    {   
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutuphema', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup Hema.pdf');
    }

    public function laporanakhirbacpenutup(\Illuminate\Http\Request $request,$id)
    {   
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupbac', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup BAC.pdf');
    }

    public function laporanakhirbtapenutup(\Illuminate\Http\Request $request,$id)
    {   
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupbta', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup BTA.pdf');
    }

    public function laporanakhirhbspenutup(\Illuminate\Http\Request $request,$id)
    {   
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutuphbs', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup HBS.pdf');
    }

    public function laporanakhirhcvpenutup(\Illuminate\Http\Request $request,$id)
    {   
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutuphcv', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup HCV.pdf');
    }

    public function laporanakhirhivpenutup(\Illuminate\Http\Request $request,$id)
    {   
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutuphiv', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup HIV.pdf');
    }

    public function laporanakhirkaipenutup(\Illuminate\Http\Request $request,$id)
    {   
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupkai', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup KAI.pdf');
    }

    public function laporanakhirkatpenutup(\Illuminate\Http\Request $request,$id)
    {   
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupkat', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup KAT.pdf');
    }

    public function laporanakhirkimiaklinikpenutup(\Illuminate\Http\Request $request,$id)
    {   
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupkimiaklinik', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup KK.pdf');
    }

    public function laporanakhirmalpenutup(\Illuminate\Http\Request $request,$id)
    {   
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupmal', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup MAL.pdf');
    }

    public function laporanakhirsifpenutup(\Illuminate\Http\Request $request,$id)
    {   
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupsif', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup SIF.pdf');
    }

    public function laporanakhirtccpenutup(\Illuminate\Http\Request $request,$id)
    {   
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutuptcc', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup TCC.pdf');
    }


    public function laporanakhiruripenutup(\Illuminate\Http\Request $request,$id)
    {   
        $t = Date('Y-m-d');
        $pdf = PDF::loadview('data_evaluasi/lap/penutupuri', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Penutup URI.pdf');
    }

    public function laporanakhirhematologidownload(\Illuminate\Http\Request $request)
    {

        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;

        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','1')
                ->where('upload_lap_akhir.tahun', $tanggal)
                ->where('upload_lap_akhir.siklus', $siklus);
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/hematologi');

    }

    public function laporanakhirbac(\Illuminate\Http\Request $request,$id)
    {
        $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, date(upload_lap_akhir.updated_at) as tanggal , @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','13')->orderBy('id','desc')->first();
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang',13)
                ->first();
        // dd($data);
                $t = date('Y');

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_bac', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri BAC.pdf');
    }
    public function laporanakhirbacdownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','13');
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/bac');

    }

    public function laporanakhirkimiaklinikdownload(\Illuminate\Http\Request $request)
    {
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;

        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','2')
                ->where('upload_lap_akhir.tahun', $tanggal)
                ->where('upload_lap_akhir.siklus', $siklus);
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/kimiaklinik');

    }


    public function laporanakhirbta(\Illuminate\Http\Request $request,$id)
    {
        $tanggal =Session::get('tanggal');
        $siklus =Session::get('siklus');
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang',4)
                ->first();

                $t = date('Y');

        
        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_bta', compact('data','datas','tanggal','siklus'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri BTA.pdf');
    }

    public function laporanakhirbtadownload(\Illuminate\Http\Request $request)
    {
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;

        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','4')
                ->where('upload_lap_akhir.tahun', $tanggal)
                ->where('upload_lap_akhir.siklus', $siklus);
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/bta');

    }

    public function laporanakhirhbs(\Illuminate\Http\Request $request,$id)
    {
        $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, date(upload_lap_akhir.updated_at) as tanggal , @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','8')->orderBy('id','desc')->first();
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang',8)
                ->first();
        $t = date('Y');

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_hbs', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri HBS.pdf');
    }

    public function laporanakhirhbsdownload(\Illuminate\Http\Request $request)
    {
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','8')
                ->where('upload_lap_akhir.tahun', $tanggal)
                ->where('upload_lap_akhir.siklus', $siklus);
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/hbs');

    }

    public function laporanakhirhcv(\Illuminate\Http\Request $request,$id)
    {
        $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, date(upload_lap_akhir.updated_at) as tanggal , @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','9')->orderBy('id','desc')->first();
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang',9)
                ->first();
        $t = date('Y');


        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_hcv', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri HCV.pdf');
    }

    public function laporanakhirhcvdownload(\Illuminate\Http\Request $request)
    {
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','9')
                ->where('upload_lap_akhir.tahun', $tanggal)
                ->where('upload_lap_akhir.siklus', $siklus);
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/hcv');

    }

    public function laporanakhirhiv(\Illuminate\Http\Request $request,$id)
    {
        $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, date(upload_lap_akhir.updated_at) as tanggal , @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','6')->orderBy('id','desc')->first();
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang',6)
                ->first();

        $t = date('Y');

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_hiv', compact('datas','data','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri HIV.pdf');
    }

    public function laporanakhirhivdownload(\Illuminate\Http\Request $request)
    {
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','6')
                ->where('upload_lap_akhir.tahun', $tanggal)
                ->where('upload_lap_akhir.siklus', $siklus);
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/hiv');

    }

    public function laporanakhirkai(\Illuminate\Http\Request $request,$id)
    {
        $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, date(upload_lap_akhir.updated_at) as tanggal , @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','11')->orderBy('id','desc')->first();
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang',11)
                ->first();
                $t = date('Y');


        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_kai', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri KAI.pdf');
    }

    public function laporanakhirkaidownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','11');
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/kai');

    }

    public function laporanakhirkat(\Illuminate\Http\Request $request,$id)
    {
        $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, date(upload_lap_akhir.updated_at) as tanggal , @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','12')->orderBy('id','desc')->first();
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang',12)
                ->first();
        $t = date('Y');
        
        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_kat', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri KAT.pdf');
    }

    public function laporanakhirkatdownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','12');
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/kat');

    }


    public function laporanakhirkimiaklinik(\Illuminate\Http\Request $request,$id)
    {
        $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, date(upload_lap_akhir.updated_at) as tanggal , @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','2')->orderBy('id','desc')->first();
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang',2)
                ->first();
                $t = date('Y');
        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_kimiaklinik', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri kimiaklinik.pdf');
    }

    public function laporanakhirmal(\Illuminate\Http\Request $request,$id)
    {
        $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, date(upload_lap_akhir.updated_at) as tanggal , @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','10')->orderBy('id','desc')->first();
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang',10)
                ->first();
        $t = date('Y');

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_mal', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri MAL.pdf');
    }

     public function laporanakhirmaldownload(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','10');
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/mal');

    }

    public function laporanakhirsif(\Illuminate\Http\Request $request,$id)
    {
        $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, date(upload_lap_akhir.updated_at) as tanggal , @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','7')->orderBy('id','desc')->first();
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang',7)
                ->first();
        $t = date('Y');

        
        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_sif', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri SIF.pdf');
    }

    public function laporanakhirsifdownload(\Illuminate\Http\Request $request)
    {
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','7')
                ->where('upload_lap_akhir.tahun', $tanggal)
                ->where('upload_lap_akhir.siklus', $siklus);
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/sif');

    }

    public function laporanakhirtcc(\Illuminate\Http\Request $request,$id)
    {
        $tanggal =Session::get('tanggal');
        $siklus =Session::get('siklus');
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang',5)
                ->first();
        $t = date('Y');


        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_tcc', compact('data','datas','tanggal','siklus'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri TCC.pdf');
    }

    public function laporanakhirtccdownload(\Illuminate\Http\Request $request)
    {
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','5')
                ->where('upload_lap_akhir.tahun', $tanggal)
                ->where('upload_lap_akhir.siklus', $siklus);
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/tcc');

    }

    public function laporanakhiruri(\Illuminate\Http\Request $request,$id)
    {
        $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, date(upload_lap_akhir.updated_at) as tanggal , @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','3')->orderBy('id','desc')->first();
        // dd($datas);
        $data =  DB::table('tb_registrasi')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang',3)
                ->first();        
        
        $t = date('Y');

        $pdf = PDF::loadview('data_evaluasi/lap/lap_akhir_uri', compact('data','datas','t'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Bakteri URI.pdf');
    }

    public function laporanakhiruridownload(\Illuminate\Http\Request $request)
    {
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias,sub_bidang.id as ids, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')
                ->where('upload_lap_akhir.parameter','=','3')
                ->where('upload_lap_akhir.tahun', $tanggal)
                ->where('upload_lap_akhir.siklus', $siklus);
            return Datatables::of($datas)
                ->addColumn('action', function($data){
                    return "".'<a href="../../../asset/laporan-akhir/'.$data->file.'" download="'.$data->file.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Download
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('data_evaluasi/uri');

    }


}
