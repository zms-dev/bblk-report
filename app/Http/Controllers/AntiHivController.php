<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\masterimunologi as Masterimunologi;
use App\bahanimunologi as Bahanimunologi;
use App\reagenimunologi as Reagenimunologi;
use App\hpimunologi as Hpimunologi;
use App\register as Register;
use App\saranevaluasi as SaranEvaluasi;
use App\strategi as Strategi;
use App\kesimpulan as Kesimpulan;
use App\CatatanImun;
use Redirect;
use Validator;
use Session;

class AntiHivController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $data = DB::table('parameter')->where('kategori', 'urinalisa')->get();
        $reagen = DB::table('tb_reagen_imunologi')
                    ->where('kelompok', 'Anti HIV')
                    ->where('produsen','!=', NULL)
                    ->orderBy(DB::raw('(`reagen` = "Lain - lain") ASC, `reagen`'))
                    ->get();
        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;
        $badan_usaha = $register->perusahaan->pemerintah;
        // dd($badan_usaha);

        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $date = $tahun1->year;

        if ($badan_usaha == '9') {
            return view('hasil_pemeriksaan/hiv-pmi', compact('data', 'perusahaan', 'reagen', 'siklus','date'));
        }else{
            return view('hasil_pemeriksaan/anti-hiv', compact('data', 'perusahaan', 'reagen', 'siklus','date'));
        }
    }

    public function view(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $data = DB::table('master_imunologi')
                ->join('bahan_imunologi', 'master_imunologi.id', '=', 'bahan_imunologi.id_master_imunologi')
                ->where('master_imunologi.jenis_form', 'Anti Hiv')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->get();
        $data1 = DB::table('master_imunologi')
                ->join('reagen_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                ->join('tb_reagen_imunologi', 'reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                ->where('master_imunologi.jenis_form', 'Anti Hiv')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->select('reagen_imunologi.*','tb_reagen_imunologi.reagen','tb_reagen_imunologi.metode as Metode')
                ->get();
        $data2 = DB::table('master_imunologi')
                ->join('hp_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                ->where('master_imunologi.jenis_form', 'Anti Hiv')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->get();
        // dd($data1);
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $badan_usaha = $register->perusahaan->pemerintah;
        if ($badan_usaha == '9') {
            // return view('cetak_hasil/anti-hiv', compact('data', 'perusahaan', 'data1', 'data2'));
            $pdf = PDF::loadview('cetak_hasil/hiv-pmi', compact('data', 'perusahaan', 'data1', 'data2', 'siklus', 'date'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
        }else{
            $pdf = PDF::loadview('cetak_hasil/anti-hiv', compact('data', 'perusahaan', 'data1', 'data2', 'siklus', 'date'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            // return view('cetak_hasil/hiv-pmi', compact('data', 'perusahaan', 'data1', 'data2'));
        }

        return $pdf->stream('Anti Hiv.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        dd($siklus);
        $type = $request->get('x');
        $input = $request->all();
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;
        $badan_usaha = $register->perusahaan->pemerintah;
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $years = $tahun->year;

        $validasi = Masterimunologi::where(DB::raw('YEAR(master_imunologi.created_at)'), '=' , $years)->where('id_registrasi','=',$id)->where('siklus','=',$siklus)->get();// dd($input);
        if (count($validasi)>0) {
            Session::flash('message', 'Hasil Anti HIV Sudah Ada!');
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
        }else{
        $SaveMaster = new Masterimunologi;
        $SaveMaster->kode_lab = $input['kode_peserta'];
        $SaveMaster->kode_peserta = $perusahaanID;
        $SaveMaster->petugas_pemeriksaan = $input['petugas_pemeriksaan'];
        $SaveMaster->keterangan = $input['keterangan'];
        $SaveMaster->hasil_pemeriksaan = 'ya';
        $SaveMaster->jenis_form = 'Anti Hiv';
        $SaveMaster->id_registrasi = $id;
        $SaveMaster->type = $type;
        $SaveMaster->siklus = $siklus;
        $SaveMaster->save();

        $SaveMasterId = $SaveMaster->id;
        $i = 0;
        foreach ($input['no_tabung'] as $tabung) {
            if($tabung != ''){
                $SaveBahan = new Bahanimunologi;
                $SaveBahan->tgl_diterima = $input['tgl_diterima'];
                $SaveBahan->tgl_diperiksa = $input['tgl_diperiksa'];
                $SaveBahan->no_tabung = $input['no_tabung'][$i];
                $SaveBahan->jenis = $input['jenis'][$i];
                $SaveBahan->id_master_imunologi = $SaveMasterId;
                $SaveBahan->save();
            }
            $i++;
        }
        $i = 0;
        foreach ($input['nama_reagen'] as $reagen) {
            $SaveBahan = new Reagenimunologi;
            if (isset($input['metode'][$i])) {
                $SaveBahan->metode = $input['metode'][$i];
            }
            $SaveBahan->nama_reagen = $input['nama_reagen'][$i];
            $SaveBahan->reagen_lain = $input['reagen_lain'][$i];
            $SaveBahan->nama_produsen = $input['nama_produsen'][$i];
            $SaveBahan->nomor_lot = $input['nomor_lot'][$i];
            $SaveBahan->tgl_kadaluarsa = $input['tgl_kadaluarsa'][$i];
            $SaveBahan->sensitivitas = $input['sensitivitas'][$i];
            $SaveBahan->spesifisitas = $input['spesifisitas'][$i];
            $SaveBahan->id_master_imunologi = $SaveMasterId;
            $SaveBahan->save();
            $i++;
        }
        $i = 0;
        if ($badan_usaha != '9') {
            foreach ($input['kode_bahan_kontrol'] as $kode) {
                if($input['kode_bahan_kontrol'][0] ){
                    $SaveHp = new Hpimunologi;
                    $SaveHp->tabung = "1";
                    $SaveHp->kode_bahan_kontrol = $input['kode_bahan_kontrol'][0];
                    $SaveHp->abs_od = $input['abs_od1'][$i];
                    $SaveHp->cut_off = $input['cut_off1'][$i];
                    $SaveHp->sco = $input['sco1'][$i];
                    $SaveHp->interpretasi = $input['interpretasi1'][$i];
                    $SaveHp->id_master_imunologi = $SaveMasterId;
                    $SaveHp->save();
                }
                if($input['kode_bahan_kontrol'][1]){
                    $SaveHp = new Hpimunologi;
                    $SaveHp->tabung = "2";
                    $SaveHp->kode_bahan_kontrol = $input['kode_bahan_kontrol'][1];
                    $SaveHp->abs_od = $input['abs_od2'][$i];
                    $SaveHp->cut_off = $input['cut_off2'][$i];
                    $SaveHp->sco = $input['sco2'][$i];
                    $SaveHp->interpretasi = $input['interpretasi2'][$i];
                    $SaveHp->id_master_imunologi = $SaveMasterId;
                    $SaveHp->save();
                }
                if($input['kode_bahan_kontrol'][2]){
                    $SaveHp = new Hpimunologi;
                    $SaveHp->tabung = "3";
                    $SaveHp->kode_bahan_kontrol = $input['kode_bahan_kontrol'][2];
                    $SaveHp->abs_od = $input['abs_od3'][$i];
                    $SaveHp->cut_off = $input['cut_off3'][$i];
                    $SaveHp->sco = $input['sco3'][$i];
                    $SaveHp->interpretasi = $input['interpretasi3'][$i];
                    $SaveHp->id_master_imunologi = $SaveMasterId;
                    $SaveHp->save();
                }
                $i++;
            }
        }else{
            foreach ($input['kode_bahan_kontrol'] as $kode) {
                if($input['kode_bahan_kontrol'][$i] ){
                    $SaveHp = new Hpimunologi;
                    $SaveHp->tabung = "1";
                    $SaveHp->kode_bahan_kontrol = $input['kode_bahan_kontrol'][$i];
                    $SaveHp->abs_od = $input['abs_od1'][$i];
                    $SaveHp->cut_off = $input['cut_off1'][$i];
                    $SaveHp->sco = $input['sco1'][$i];
                    $SaveHp->interpretasi = $input['interpretasi1'][$i];
                    $SaveHp->id_master_imunologi = $SaveMasterId;
                    $SaveHp->save();
                }
                $i++;
            }
        }
        
        $date_sekarang = date('Y-m-d H:i:s');
        DB::table('tanggal_input')->insert(['id_register' => $id, 'siklus' => $siklus, 'input_date' => $date_sekarang]);
        if ($siklus == '1') {
            Register::where('id',$id)->update(['siklus_1'=>'done', 'rpr1'=>'done', 'status_data1'=>'1']);
            Register::where('id',$id)->update(['pemeriksaan'=>'done']);
        }else{
            Register::where('id',$id)->update(['siklus_2'=>'done', 'rpr2'=>'done', 'status_data2'=>'1']);
            Register::where('id',$id)->update(['pemeriksaan2'=>'done']);
        }
        return redirect('hasil-pemeriksaan');
        }
    }

    public function edit(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $data = DB::table('master_imunologi')
                ->join('bahan_imunologi', 'master_imunologi.id', '=', 'bahan_imunologi.id_master_imunologi')
                ->where('master_imunologi.jenis_form', 'Anti Hiv')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->select('master_imunologi.*', 'bahan_imunologi.id as idimunologi',  'bahan_imunologi.lain', 'bahan_imunologi.id_master_imunologi', 'bahan_imunologi.no_tabung', 'bahan_imunologi.jenis', 'bahan_imunologi.tgl_diperiksa', 'bahan_imunologi.tgl_diterima')
                ->get();
        $data1 = DB::table('master_imunologi')
                ->join('reagen_imunologi', 'master_imunologi.id', '=', 'reagen_imunologi.id_master_imunologi')
                ->join('tb_reagen_imunologi', 'reagen_imunologi.nama_reagen','=','tb_reagen_imunologi.id')
                ->where('master_imunologi.jenis_form', 'Anti Hiv')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->select('reagen_imunologi.*', 'tb_reagen_imunologi.sensitivitas as Sensitivitas', 'tb_reagen_imunologi.spesifisitas as Spesifisitas', 'tb_reagen_imunologi.id as Idreagen', 'tb_reagen_imunologi.reagen as namareagen')
                ->get();
        $data2 = DB::table('master_imunologi')
                ->join('hp_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                ->where('master_imunologi.jenis_form', 'Anti Hiv')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $siklus)
                ->get();
        // dd($data1);
        $reagen = DB::table('tb_reagen_imunologi')
                    ->where('kelompok', 'Anti HIV')
                    ->where('produsen','!=', NULL)
                    ->orderBy(DB::raw('(`reagen` = "Lain - lain") ASC, `reagen`'))
                    ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $kodeperusahaan = $register->kode_lebpes;
        $badan_usaha = $register->perusahaan->pemerintah;
        // dd($data1);
        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $date = $q2->year;

        if ($badan_usaha == '9') {
            return view('edit_hasil/hiv-pmi', compact('data', 'perusahaan', 'data1', 'data2', 'siklus','kodeperusahaan', 'reagen', 'date'));
        }else{
            return view('edit_hasil/anti-hiv', compact('data', 'perusahaan', 'data1', 'data2', 'siklus','kodeperusahaan', 'reagen', 'date'));
        }
    }


    public function update(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $input = $request->all();
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;
        $badan_usaha = $register->perusahaan->pemerintah;
        $MasterId = $request->idmaster;
        // dd($input);
        $SaveMaster['kode_peserta'] = $perusahaanID;
        $SaveMaster['kode_lab'] = $request->kode_peserta;
        $SaveMaster['petugas_pemeriksaan'] = $request->petugas_pemeriksaan;
        $SaveMaster['siklus'] = $siklus;
        $SaveMaster['hasil_pemeriksaan'] = 'ya';
        $SaveMaster['jenis_form'] = 'Anti Hiv';
        $SaveMaster['id_registrasi'] = $id;
        $SaveMaster['keterangan'] = $request->keterangan;
        Masterimunologi::where('id_registrasi',$id)->where('id', $MasterId)->update($SaveMaster);

        $SaveMasterId = $MasterId;
        $i = 0;
        foreach ($input['no_tabung'] as $tabung) {
            if($tabung != ''){
                $SaveBahan['tgl_diterima'] = $request->tgl_diterima;
                $SaveBahan['tgl_diperiksa'] = $request->tgl_diperiksa;
                $SaveBahan['no_tabung'] = $request->no_tabung[$i];
                $SaveBahan['jenis'] = $request->jenis[$i];
                $SaveBahan['id_master_imunologi'] = $SaveMasterId;
                Bahanimunologi::where('id', $request->idbahan[$i])->update($SaveBahan);
            }
            $i++;
        }
        $i = 0;
        foreach ($input['nama_reagen'] as $reagen) {
            if (isset($input['metode'][$i])) {
                $SaveReagen['metode'] = $input['metode'][$i];
            }else{
                $SaveReagen['metode'] = NULL;
            }
            $SaveReagen['nama_reagen'] = $request->nama_reagen[$i];
            $SaveReagen['nama_produsen'] = $request->nama_produsen[$i];
            $SaveReagen['reagen_lain'] = $request->reagen_lain[$i];
            $SaveReagen['nomor_lot'] = $request->nomor_lot[$i];
            $SaveReagen['tgl_kadaluarsa'] = $request->tgl_kadaluarsa[$i];
            $SaveReagen['sensitivitas'] = $request->sensitivitas[$i];
            $SaveReagen['spesifisitas'] = $request->spesifisitas[$i];
            $SaveReagen['id_master_imunologi'] = $SaveMasterId;
            Reagenimunologi::where('id', $request->idreagen[$i])->update($SaveReagen);
            $i++;
        }
        $i = 0;
        $tabung1 = 0;
        $tabung2 = 3;
        $tabung3 = 6;
        $tabung4 = 9;
        $tabung5 = 12;
        if ($badan_usaha != '9') {
            foreach ($input['abs_od1'] as $kode) {
                if($input['kode_bahan_kontrol'][0] ){
                    $SaveHp['tabung'] = "1";
                    $SaveHp['kode_bahan_kontrol'] = $request->kode_bahan_kontrol[0];
                    $SaveHp['abs_od'] = $request->abs_od1[$tabung1];
                    $SaveHp['cut_off'] = $request->cut_off1[$tabung1];
                    $SaveHp['sco'] = $request->sco1[$tabung1];
                    $SaveHp['interpretasi'] = $request->interpretasi1[$tabung1];
                    $SaveHp['id_master_imunologi'] = $SaveMasterId;
                    Hpimunologi::where('id', $request->idhp[$tabung1])->update($SaveHp);
                }
                if($input['kode_bahan_kontrol'][1] ){
                    $SaveHp['tabung'] = "2";
                    $SaveHp['kode_bahan_kontrol'] = $request->kode_bahan_kontrol[1];
                    $SaveHp['abs_od'] = $request->abs_od2[$tabung1];
                    $SaveHp['cut_off'] = $request->cut_off2[$tabung1];
                    $SaveHp['sco'] = $request->sco2[$tabung1];
                    $SaveHp['interpretasi'] = $request->interpretasi2[$tabung1];
                    $SaveHp['id_master_imunologi'] = $SaveMasterId;
                    Hpimunologi::where('id', $request->idhp[$tabung2])->update($SaveHp);
                }
                if($input['kode_bahan_kontrol'][2] ){
                    $SaveHp['tabung'] = "3";
                    $SaveHp['kode_bahan_kontrol'] = $request->kode_bahan_kontrol[2];
                    $SaveHp['abs_od'] = $request->abs_od3[$tabung1];
                    $SaveHp['cut_off'] = $request->cut_off3[$tabung1];
                    $SaveHp['sco'] = $request->sco3[$tabung1];
                    $SaveHp['interpretasi'] = $request->interpretasi3[$tabung1];
                    $SaveHp['id_master_imunologi'] = $SaveMasterId;
                    Hpimunologi::where('id', $request->idhp[$tabung3])->update($SaveHp);
                }
                $tabung1++;
                $tabung2++;
                $tabung3++;
                $tabung4++;
                $tabung5++;
                $i++;
            }
        }else{
            foreach ($input['kode_bahan_kontrol'] as $kode) {
                if($input['kode_bahan_kontrol'][$i] ){
                    $SaveHp['tabung'] = "1";
                    $SaveHp['kode_bahan_kontrol'] = $request->kode_bahan_kontrol[$i];
                    $SaveHp['abs_od'] = $request->abs_od1[$i];
                    $SaveHp['cut_off'] = $request->cut_off1[$i];
                    $SaveHp['sco'] = $request->sco1[$i];
                    $SaveHp['interpretasi'] = $request->interpretasi1[$i];
                    $SaveHp['id_master_imunologi'] = $SaveMasterId;
                    Hpimunologi::where('id', $request->idhp[$i])->update($SaveHp);
                }
                $i++;
            }
        }
        if ($request->simpan == "Kirim" || $request->simpan == "Validasi") {
            if ($siklus == '1') {
                Register::where('id',$id)->update(['status_data1'=>'2']);
            }else{
                Register::where('id',$id)->update(['status_data2'=>'2']);
            }
            if ($request->simpan == "Validasi") {
                DB::table('validasi_input')->where('id_register', $id)->where('siklus', $siklus)->delete();
                DB::table('validasi_input')->insert(
                    ['id_register' => $id, 'siklus' => $siklus, 'status' => 'done']
                );
                return back();
            }else{
                return redirect('edit-hasil');
            }
        }else{
            return back();
        }
    }


    public function evaluasi(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        // dd($type);
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;
        $tahun = $tahun->year;

        $data2 = DB::table('master_imunologi')
                ->join('hp_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                ->where('master_imunologi.jenis_form', 'Anti Hiv')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $type)
                ->get();

        $rujukan = DB::table('tb_rujukan_imunologi')
                ->where('tb_rujukan_imunologi.tahun', '=' , $date)
                ->where('tb_rujukan_imunologi.siklus', $type)
                ->where('tb_rujukan_imunologi.parameter','Anti HIV')
                ->get();
        // dd($data2);
        $reagen = DB::table('reagen_imunologi')
                    ->join('tb_reagen_imunologi','reagen_imunologi.nama_reagen','tb_reagen_imunologi.id')
                    ->where('id_master_imunologi', $data2[0]->id_master_imunologi)
                    ->select('tb_reagen_imunologi.id','tb_reagen_imunologi.reagen','reagen_imunologi.metode','reagen_imunologi.reagen_lain', 'tb_reagen_imunologi.sensitivitas', 'tb_reagen_imunologi.spesifisitas', 'tb_reagen_imunologi.rscm', 'reagen_imunologi.tgl_kadaluarsa', 'reagen_imunologi.nomor_lot', 'reagen_imunologi.id as idreagen', 'reagen_imunologi.sensitivitas as sensi', 'reagen_imunologi.spesifisitas as spesi')
                    ->get();

        $strategi = Strategi::where('id_master_imunologi', $data2[0]->id_master_imunologi)->where('siklus', $type)->get();
        $evaluasi = SaranEvaluasi::where('id_registrasi', $id)->where('siklus', $type)->first();
        $catatan = CatatanImun::where('siklus', $type)->where('tahun', $date)->where('id_registrasi', $id)->first();
        $kesimpulan = Kesimpulan::where('id_registrasi', $id)->where('siklus', $type)->where('type','=','Anti Hiv')->first();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $kodeperusahaan = $register->kode_lebpes;

        $badan_usaha = $register->perusahaan->pemerintah;

        // dd($rujukan);

        if ($badan_usaha == '9') {
        return view('evaluasi.imunologi.hiv-pmi.evaluasi', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi','register','reagen','kesimpulan', 'date', 'catatan', 'rujukan','tahun'));
        }else{
        return view('evaluasi.imunologi.hiv.evaluasi', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi','register','reagen','kesimpulan', 'date', 'rujukan','tahun', 'catatan'));
        }
    }

    public function insertevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('y');
        $input = $request->all();
        $register = Register::find($id);
        $array = array_unique($input['kode_bahan_kontrol']);
        $kode_bahan = array_values($array);
        // dd($input);
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $badan_usaha = $register->perusahaan->pemerintah;
        // dd($badan_usaha);
        $chekdata = DB::table('tb_strategi_evaluasi')
                    ->where('id_master_imunologi', $input['id_master_imunologi'])
                    ->get();

        $i = 0;
        $a = 0;
        if(count($chekdata)){
        }else{
            foreach ($input['strategi'] as $strategi) {
            $a++;
                if($strategi != ''){
                    $SaveStrategi = new Strategi;
                    $SaveStrategi->id_master_imunologi = $input['id_master_imunologi'];
                    $SaveStrategi->nilai = $input['strategi'][$i];
                    if ($input['strategi'][$i] == '0') {
                        $SaveStrategi->kategori = 'Tidak Sesuai';
                    }elseif ($input['strategi'][$i] == '5') {
                        $SaveStrategi->kategori = 'Sesuai';
                    }else{
                        $SaveStrategi->kategori = '';
                    }
                    $SaveStrategi->tabung = $a;
                    $SaveStrategi->siklus = $type;
                    $SaveStrategi->save();
                }
                $i++;
            }
            $SaveMaster = new SaranEvaluasi;
            if (isset($input['nilai_1'])) {
                $SaveMaster->nilai_1 = $input['nilai_1'];
            }
            if (isset($input['nilai_2'])) {
                $SaveMaster->nilai_2 = $input['nilai_2'];
            }
            if (isset($input['nilai_3'])) {
                $SaveMaster->nilai_3 = $input['nilai_3'];
            }
            if (isset($input['nilai_4'])) {
                $SaveMaster->nilai_4 = $input['nilai_4'];
            }
            if (isset($input['nilai_5'])) {
                $SaveMaster->nilai_5 = $input['nilai_5'];
            }
            if (isset($input['nilai_6'])) {
                $SaveMaster->nilai_6 = $input['nilai_6'];
            }
            if (isset($input['nilai_7'])) {
                $SaveMaster->nilai_7 = $input['nilai_7'];
            }
            if (isset($input['nilai_8'])) {
                $SaveMaster->nilai_8 = $input['nilai_8'];
            }
            if (isset($input['nilai_9'])) {
                $SaveMaster->nilai_9 = $input['nilai_9'];
            }
            if (isset($input['nilai_10'])) {
                $SaveMaster->nilai_10 = $input['nilai_10'];
            }
            if (isset($input['nilai_11'])) {
                $SaveMaster->nilai_11 = $input['nilai_11'];
            }
            if (isset($input['nilai_12'])) {
                $SaveMaster->nilai_12 = $input['nilai_12'];
            }
            if (isset($input['nilai_113'])) {
                $SaveMaster->nilai_113 = $input['nilai_13'];
            }
            if (isset($input['nilai_14'])) {
                $SaveMaster->nilai_14 = $input['nilai_14'];
            }
            if (isset($input['nilai_15'])) {
                $SaveMaster->nilai_14 = $input['nilai_14'];
            }
            $SaveMaster->id_registrasi = $id;
            $SaveMaster->siklus = $type;
            $SaveMaster->save();

            $SaveCatatan = new CatatanImun;
            $SaveCatatan->siklus = $type;
            $SaveCatatan->id_registrasi = $id;
            $SaveCatatan->tahun = $date;
            $SaveCatatan->catatan = $input['catatan'];
            $SaveCatatan->save();
            $i = 0;
            if(isset($request->idreagen)){
                foreach ($request->idreagen as $tabung) {
                    if($tabung != ''){
                        $Savereagen['sensitivitas'] = $request->sensitivitas[$i];
                        $Savereagen['spesifisitas'] = $request->spesifisitas[$i];
                        Reagenimunologi::where('id',$request->idreagen[$i])->update($Savereagen);
                    }
                    $i++;
                }
            }
            $SaveKesimpulan = new Kesimpulan;
            $SaveKesimpulan->ketepatan = $input['ketepatan'];
            $SaveKesimpulan->kesesuaian = $input['kesesuaian'];
            $SaveKesimpulan->id_master_imunologi = $input['id_master_imunologi'];
            $SaveKesimpulan->type = 'Anti Hiv';
            $SaveKesimpulan->id_registrasi = $id;
            $SaveKesimpulan->siklus = $type;
            $SaveKesimpulan->save();
        }
        return redirect('/hasil-pemeriksaan/anti-hiv/evaluasi/'.$id.'?y='.$type);
    }

    public function cetakevaluasi(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $ttd = Date('Y m d');
        $date = date('d F Y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;

        $data2 = DB::table('master_imunologi')
                ->join('hp_imunologi', 'master_imunologi.id', '=', 'hp_imunologi.id_master_imunologi')
                ->where('master_imunologi.jenis_form', 'Anti Hiv')
                ->where('master_imunologi.id_registrasi', $id)
                ->where('master_imunologi.siklus', $type)
                ->get();

        $rujukan = DB::table('tb_rujukan_imunologi')
                ->where('tb_rujukan_imunologi.tahun', '=' , $tahun)
                ->where('tb_rujukan_imunologi.siklus', $type)
                ->where('tb_rujukan_imunologi.parameter','Anti HIV')
                ->get();

        $reagen = DB::table('reagen_imunologi')
                    ->leftjoin('tb_reagen_imunologi','reagen_imunologi.nama_reagen','tb_reagen_imunologi.id')
                    ->where('id_master_imunologi', $data2[0]->id_master_imunologi)
                    ->select('tb_reagen_imunologi.reagen','reagen_imunologi.metode','reagen_imunologi.reagen_lain', 'reagen_imunologi.nama_reagen', 'tb_reagen_imunologi.sensitivitas', 'tb_reagen_imunologi.spesifisitas', 'tb_reagen_imunologi.rscm', 'reagen_imunologi.sensitivitas as sensi', 'reagen_imunologi.spesifisitas as spesi','reagen_imunologi.nomor_lot','reagen_imunologi.tgl_kadaluarsa')
                    ->get();
        // dd($rujukan);
        $strategi = Strategi::where('id_master_imunologi', $data2[0]->id_master_imunologi)->where('siklus', $type)->get();
        $evaluasi = SaranEvaluasi::where('id_registrasi', $id)->where('siklus', $type)->first();
        $kesimpulan = Kesimpulan::where('id_registrasi', $id)->where('siklus', $type)->first();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $kodeperusahaan = $register->kode_lebpes;
        $catatan = CatatanImun::where('id_registrasi', $id)->where('siklus', $type)->where('tahun', $tahun)->first();

        $badan_usaha = $register->perusahaan->pemerintah;
        $tanggalevaluasi = DB::table('tanggal_evaluasi')->where('form', '=', 'Imunologi')->where('siklus', $type)->where('tahun', $tahun)->first();

        if ($kesimpulan != NULL) {
            if ($badan_usaha == '9') {
                $pdf = PDF::loadview('evaluasi.imunologi.hiv-pmi.print', compact('data','ttd', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'ttd', 'catatan','rujukan','tanggalevaluasi'))
                    ->setPaper('a4', 'potrait')
                    ->setwarnings(false);
                return $pdf->stream('Evaluasi Anti Hiv.pdf');
            }else{
                $pdf = PDF::loadview('evaluasi.imunologi.hiv.print', compact('data','ttd', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'ttd', 'catatan','rujukan','tanggalevaluasi'))
                    ->setPaper('a4', 'potrait')
                    ->setwarnings(false);
                return $pdf->stream('Evaluasi Anti Hiv.pdf');
            }
        }else{
            Session::flash('message', 'HIV masih dalam proses evaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
        // return view('evaluasi/anti-hiv', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi'));
    }

    public function updatevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('y');
        $input = $request->all();
        $kesimpulan = Kesimpulan::where('id_registrasi',$id)->get();
        $strategi = Strategi::where('id_master_imunologi', $input['id_master_imunologi'])->where('siklus', $type)->get();
        // dd($strategi);
        $array = array_unique($request->kode_bahan_kontrol);
        $kode_bahan = array_values($array);
        $register = Register::find($id);
        // dd($input);
        $badan_usaha = $register->perusahaan->pemerintah;
        $i = 0;
        $a = 0;
        if (count($strategi)) {
            foreach ($input['strategi'] as $strategi) {
            $a++;
                if($strategi != ''){
                    $SaveStrategi['nilai'] = $strategi;
                    if ($input['strategi'][$i] == '0') {
                        $SaveStrategi['kategori'] = 'Tidak Sesuai';
                    }elseif ($input['strategi'][$i] == '5') {
                        $SaveStrategi['kategori'] = 'Sesuai';
                    }else{
                        $SaveStrategi['kategori'] = '';
                    }
                    $SaveStrategi['tabung'] = $a;
                    Strategi::where('id', $request->idevaluasi[$i])->update($SaveStrategi);
                }
                $i++;
            }
        }else{
            foreach ($input['strategi'] as $strategi) {
            $a++;
                if($strategi != ''){
                    $SaveStrategi = new Strategi;
                    $SaveStrategi->id_master_imunologi = $input['id_master_imunologi'];
                    $SaveStrategi->nilai = $input['strategi'][$i];
                    if ($input['strategi'][$i] == '0') {
                        $SaveStrategi->kategori = 'Tidak Sesuai';
                    }elseif ($input['strategi'][$i] == '5') {
                        $SaveStrategi->kategori = 'Sesuai';
                    }else{
                        $SaveStrategi->kategori = '';
                    }
                    $SaveStrategi->tabung = $a;
                    $SaveStrategi->siklus = $type;
                    $SaveStrategi->save();
                }
                $i++;
            }
        }
        if (isset($request->nilai_1)) {
            $SaveMaster['nilai_1'] = $request->nilai_1;
        }else{
            $SaveMaster['nilai_1'] = 0;
        }
        if (isset($request->nilai_2)) {
            $SaveMaster['nilai_2'] = $request->nilai_2;
        }else{
            $SaveMaster['nilai_2'] = 0;
        }
        if (isset($request->nilai_3)) {
            $SaveMaster['nilai_3'] = $request->nilai_3;
        }else{
            $SaveMaster['nilai_3'] = 0;
        }
        if (isset($request->nilai_4)) {
            $SaveMaster['nilai_4'] = $request->nilai_4;
        }else{
            $SaveMaster['nilai_4'] = 0;
        }
        if (isset($request->nilai_5)) {
            $SaveMaster['nilai_5'] = $request->nilai_5;
        }else{
            $SaveMaster['nilai_5'] = 0;
        }
        if (isset($request->nilai_6)) {
            $SaveMaster['nilai_6'] = $request->nilai_6;
        }else{
            $SaveMaster['nilai_6'] = 0;
        }
        if (isset($request->nilai_7)) {
            $SaveMaster['nilai_7'] = $request->nilai_7;
        }else{
            $SaveMaster['nilai_7'] = 0;
        }
        if (isset($request->nilai_8)) {
            $SaveMaster['nilai_8'] = $request->nilai_8;
        }else{
            $SaveMaster['nilai_8'] = 0;
        }
        if (isset($request->nilai_9)) {
            $SaveMaster['nilai_9'] = $request->nilai_9;
        }else{
            $SaveMaster['nilai_9'] = 0;
        }
        if (isset($request->nilai_10)) {
            $SaveMaster['nilai_10'] = $request->nilai_10;
        }else{
            $SaveMaster['nilai_10'] = 0;
        }
        if (isset($request->nilai_11)) {
            $SaveMaster['nilai_11'] = $request->nilai_11;
        }else{
            $SaveMaster['nilai_11'] = 0;
        }
        if (isset($request->nilai_12)) {
            $SaveMaster['nilai_12'] = $request->nilai_12;
        }else{
            $SaveMaster['nilai_12'] = 0;
        }
        if (isset($request->nilai_13)) {
            $SaveMaster['nilai_13'] = $request->nilai_13;
        }else{
            $SaveMaster['nilai_13'] = 0;
        }
        if (isset($request->nilai_14)) {
            $SaveMaster['nilai_14'] = $request->nilai_14;
        }else{
            $SaveMaster['nilai_14'] = 0;
        }
        if (isset($request->nilai_15)) {
            $SaveMaster['nilai_15'] = $request->nilai_15;
        }else{
            $SaveMaster['nilai_15'] = 0;
        }
        SaranEvaluasi::where('id_registrasi',$id)->update($SaveMaster);

        $SaveCatatan['catatan'] = $request->catatan;
        $SaveCatatan['tanggal_ttd'] = $request->ttd;
        CatatanImun::where('id_registrasi',$id)->where('siklus', $type)->update($SaveCatatan);
        $i = 0;
        if(isset($request->idreagen)){
            foreach ($request->idreagen as $tabung) {
                if($tabung != ''){
                    $Savereagen['sensitivitas'] = $request->sensitivitas[$i];
                    $Savereagen['spesifisitas'] = $request->spesifisitas[$i];
                    Reagenimunologi::where('id',$request->idreagen[$i])->update($Savereagen);
                }
                $i++;
            }
        }
        // dd($request->spesifisitas);
        if(count($kesimpulan))
        {
            $SaveKesimpulan['ketepatan'] = $request->ketepatan;
            $SaveKesimpulan['kesesuaian'] = $request->kesesuaian;
            Kesimpulan::where('id_registrasi',$id)->update($SaveKesimpulan);
        }else{
            $SaveKesimpulan = new Kesimpulan;
            $SaveKesimpulan->ketepatan = $input['ketepatan'];
            $SaveKesimpulan->kesesuaian = $input['kesesuaian'];
            $SaveKesimpulan->id_registrasi = $id;
            $SaveKesimpulan->save();
        }

        return redirect('/hasil-pemeriksaan/anti-hiv/evaluasi/'.$id.'?y='.$type);
    }
}
