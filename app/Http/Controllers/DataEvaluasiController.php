<?php
namespace App\Http\Controllers;
use DB;
use Input;
use PDF;
use Carbon\Carbon;

use App\HpHeader;
use App\Parameter;
use App\MetodePemeriksaan;
use App\HpDetail;
use App\ZScore;
use App\ZScoreAlat;
use App\ZScoreMetode;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\register as Register;
use App\TbReagenImunologi as ReagenImunologi;
use App\TbHp;
use App\Rujukanurinalisa;
use Redirect;
use Validator;
use Session;
use App\User;
use App\SdMedian;
use App\SdMedianMetode;
use App\Ring;
use App\CatatanImun;
use App\SdMedianAlat;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use mikehaertl\wkhtmlto\Pdf as WKHTMLPDF;
use mikehaertl\wkhtmlto\Image;
use Illuminate\Support\Facades\Storage;
use Excel;
use App\Rujukanurinalisametode;
use App\SertfikatInsert;
use App\statusbakteri;
use App\TanggalEvaluasi;


class DataEvaluasiController extends Controller
{
    public function index(\Illuminate\Http\Request $request)
    {
        $tanggal = $request->tanggal;
        $siklus = $request->siklus;

        $request->session()->put('tanggal', $tanggal);
        $request->session()->put('siklus', $siklus);
        $dd = $request->session()->get('tanggal');
        $siklus = $request->session()->get('siklus');
        // return $dd;
        if ($tanggal == null) {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }else{
            $tanggal = $request->tanggal;
            $siklus = $request->siklus;
        }
        $tanggal = $dd;
        $siklus = $siklus;
        $setsiklus = DB::table('tb_siklus')->first();
        // dd($tanggal);
        $user = Auth::user()->id;
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', '>=' , 2)
                ->where(function ($query) use ($siklus){
                    if($siklus == 1){
                        $query->whereIn('tb_registrasi.siklus', ['1','12']);
                    }else{
                        $query->whereIn('tb_registrasi.siklus', ['2','12']);
                    }
                })
                // ->whereIn('sub_bidang.id',[4,5])
                ->get();
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        Session::put('data',$datas);
        Session::get('data');
    	return view('data_evaluasi/index', compact('datas', 'data2','tanggal','siklus','setsiklus','tahunevaluasi'));
    }

    

    public function hematologizscore(\Illuminate\Http\Request $request, $id)
    {
     	$type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $user = Auth::user()->id;
        

        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $setsiklus = DB::table('tb_siklus')->first();
        
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', '>=' , 2)
                ->where(function ($query) use ($siklus){
                    if($siklus == 1){
                        $query->whereIn('tb_registrasi.siklus', ['1','12']);
                    }else{
                        $query->whereIn('tb_registrasi.siklus', ['2','12']);
                    }
                })
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->orwhere('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('siklus_1', 'done')
                            ->orwhere('siklus_2', 'done');
                    })
                ->get();
        // dd($data);
       
         // dd($data);
        return view('data_evaluasi.hematologi', compact('data','dd','data1','id','tanggal','type','siklus','date','datas','setsiklus','tahunevaluasi'));
    }

    public function kimiaklinik(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $user = Auth::user()->id;
        

        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $setsiklus = DB::table('tb_siklus')->first();
        
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', '>=' , 2)
                ->where(function ($query) use ($siklus){
                    if($siklus == 1){
                        $query->whereIn('tb_registrasi.siklus', ['1','12']);
                    }else{
                        $query->whereIn('tb_registrasi.siklus', ['2','12']);
                    }
                })
                ->get();

        $data =  DB::table('tb_registrasi')
                    ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                    ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                    ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                    ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter','tb_registrasi.id as idregistrasi','sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                    ->where('tb_registrasi.created_by','=', Auth::user()->id)
                    ->where('sub_bidang.id', '=', '2')
                    ->where('status', 3)
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2)
                                ->orwhere('status_datarpr1', 2)
                                ->orwhere('status_datarpr2', 2);
                        })
                    ->where(function($query)
                        {
                            $query->orwhere('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('siklus_1', 'done')
                                ->orwhere('siklus_2', 'done');
                        })
                    ->get();

        // dd($data);
        return view('data_evaluasi.kimiaklinik', compact('skoring1','dd','tanggal','data','datas','id','type','siklus','setsiklus','tahunevaluasi'));
    }

     public function urisertifikat(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $nomor = $request->nomor;
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;
        
        $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=','Urinalisa')->where('siklus', $type)->where('tahun',$tahun)->first();
        // dd($sertifikat);
        if ($request->query('download') == "true") {
            $url = url('data-evaluasi/urinalisa/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal').'&nomor='.$request->query('nomor'));
            $image = new Image($url);
             $image->setOptions([
                'binary' => env('PATH_WKHTMLPDF', '/usr/local/bin/wkhtmltopdf'),
                'orientation' => 'landscape',
                'page-size' => 'A4',
                'margin-top' => '0cm',
                'margin-right' => '0cm',
                'margin-bottom' => '0cm',
                'margin-left' => '0cm',
                'dpi' => '100',
            ]);

            if(!$image->send('Sertifikat Urinalisa.pdf')){
                dd($image->getError());
            }       
        }else{
            $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->whereIn('tb_registrasi.bidang',array(3))
                ->where('tb_registrasi.id',$id)
                // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->select('tb_registrasi.kode_lebpes','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'))
                ->groupBy('tb_registrasi.perusahaan_id')
                ->get();
            // dd($data);
            // if (count($kesimpulan) > 0) {
            \Debugbar::disable();
            return view('evaluasi.imunologi.sertifikat.urin', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal', 'sertifikat','nomor'));
        }
    }

     public function kimiakliniksertifikat(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $nomor = $request->nomor;
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;
        $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=','Kimiaklinik')->where('siklus', $type)->where('tahun',$tahun)->first();

        if ($request->query('download') == "true") {
            $url = url('/sertifikat/kimia-klinik/data-evaluasi/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal').'&nomor='.$request->query('nomor'));
             $image = new WKHTMLPDF($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLPDF', '/usr/local/bin/wkhtmltopdf'),
                'orientation' => 'landscape',
                'page-size' => 'A4',
                'margin-top' => '0cm',
                'margin-right' => '0cm',
                'margin-bottom' => '0cm',
                'margin-left' => '0cm',
                'dpi' => '100',
            ]);

            if(!$image->send('Sertifikat Kimiaklinik.pdf')){
                dd($image->getError());
            }
        }else{
            $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->whereIn('tb_registrasi.bidang',array(2))
                ->where('tb_registrasi.id',$id)
                // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->select('tb_registrasi.kode_lebpes','tb_registrasi.id as idr','perusahaan.id as idp','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'))
                ->groupBy('tb_registrasi.perusahaan_id')
                ->get();
            // dd($data);
            // if (count($kesimpulan) > 0) {
            \Debugbar::disable();
            return view('evaluasi.imunologi.sertifikat.kimiaklnik', compact('data', 'perusahaan','sertifikat', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal', 'nomor'));
        }
        // dd($data);
        // if (count($kesimpulan) > 0) {
    }



     public function bta(\Illuminate\Http\Request $request, $id)
    {
       $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $user = Auth::user()->id;
        $setsiklus = DB::table('tb_siklus')->first();
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();

        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', '>=' , 2)
                ->where(function ($query) use ($siklus){
                    if($siklus == 1){
                        $query->whereIn('tb_registrasi.siklus', ['1','12']);
                    }else{
                        $query->whereIn('tb_registrasi.siklus', ['2','12']);
                    }
                })
                ->get();
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('sub_bidang.id', '=', '4')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
        
         // dd($data);
        return view('data_evaluasi.bta', compact('data','tanggal','dd','datas','id','type','siklus','setsiklus','tahunevaluasi'));
    }

     public function tcc(\Illuminate\Http\Request $request, $id)
    {
       $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $input = $request->all();
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $user = Auth::user()->id;
        $setsiklus = DB::table('tb_siklus')->first();
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', '>=' , 2)
                ->where(function ($query) use ($siklus){
                    if($siklus == 1){
                        $query->whereIn('tb_registrasi.siklus', ['1','12']);
                    }else{
                        $query->whereIn('tb_registrasi.siklus', ['2','12']);
                    }
                })
                ->get();
        
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('sub_bidang.id', '=', '5')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1','=', '2')
                            ->orwhere('status_data2','=', '2');
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
       
         // dd($data);
        return view('data_evaluasi.tcc', compact('data','tanggal','datas','id','type','siklus','setsiklus','tahunevaluasi'));
    }

     public function hiv(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $input = $request->all();
        

        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $setsiklus = DB::table('tb_siklus')->first();

        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', '>=' , 2)
                ->where(function ($query) use ($siklus){
                    if($siklus == 1){
                        $query->whereIn('tb_registrasi.siklus', ['1','12']);
                    }else{
                        $query->whereIn('tb_registrasi.siklus', ['2','12']);
                    }
                })
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('tb_registrasi.bidang','=', '6')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
       
         // dd($data);
        return view('data_evaluasi.hiv', compact('data','tanggal','datas','id','type','siklus','setsiklus','tahunevaluasi'));
    }

     public function sif(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $input = $request->all();
        
        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $setsiklus = DB::table('tb_siklus')->first();
        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', '>=' , 2)
                ->where(function ($query) use ($siklus){
                    if($siklus == 1){
                        $query->whereIn('tb_registrasi.siklus', ['1','12']);
                    }else{
                        $query->whereIn('tb_registrasi.siklus', ['2','12']);
                    }
                })
                ->get();
              
        $data =  DB::table('tb_registrasi')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.bidang','=','7')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
                // dd($data);
        $type = $request->get('y');
        $tahun= date('Y'); 
        // $catatan = CatatanImun::where('id_registrasi', $id)->where('siklus', $type)->where('tahun', $tahun)->get();
        // dd($catatan)->first();
        return view('data_evaluasi.sif', compact('data','tanggal','datas','id','type','siklus','setsiklus','tahunevaluasi'));
    }

     public function hbs(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $user = Auth::user()->id;
        $input = $request->all();
        

        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $setsiklus = DB::table('tb_siklus')->first();

        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', '>=' , 2)
                ->where(function ($query) use ($siklus){
                    if($siklus == 1){
                        $query->whereIn('tb_registrasi.siklus', ['1','12']);
                    }else{
                        $query->whereIn('tb_registrasi.siklus', ['2','12']);
                    }
                })
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
        // dd($data);
        return view('data_evaluasi.hbs', compact('datas','tanggal','data','id','type','siklus','setsiklus','tahunevaluasi'));
    }

     public function hcv(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $input = $request->all();
        

        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $setsiklus = DB::table('tb_siklus')->first();

        $user = Auth::user()->id;
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();
        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', '>=' , 2)
                ->where(function ($query) use ($siklus){
                    if($siklus == 1){
                        $query->whereIn('tb_registrasi.siklus', ['1','12']);
                    }else{
                        $query->whereIn('tb_registrasi.siklus', ['2','12']);
                    }
                })
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.bidang','=','9')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        // dd($data);
        return view('data_evaluasi.hcv', compact('data','id','type','tanggal','siklus','datas','setsiklus','tahunevaluasi'));
    }

     public function mal(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $input = $request->all();
        

        $dd = $request->tanggal;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
        } else {
            $dd = $request->session()->get('tanggal');
        }
        $tanggal = $dd;

        $user = Auth::user()->id;
        $datas=  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('sub_bidang.id', '=', '10')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
         // dd($data);
        return view('data_evaluasi.mal', compact('data','id','type','siklus','datas','tanggal'));
    }

     public function bac(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $input = $request->all();

        $dd = $request->tanggal;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
        } else {
            $dd = $request->session()->get('tanggal');
        }
        $tanggal = $dd;


        $user = Auth::user()->id;
        $datas=  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
            $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('sub_bidang.id', '=', '13')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();

            $data1 =  DB::table('tb_registrasi')
            ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
            ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
            ->where('tb_registrasi.created_by','=', Auth::user()->id)
            ->where('sub_bidang.id', '=', '13')
            ->where('status', 3)
            ->where(function($query)
                {
                    $query->where('status_data1', 2)
                        ->orwhere('status_data2', 2)
                        ->orwhere('status_datarpr1', 2)
                        ->orwhere('status_datarpr2', 2);
                })
            ->where(function($query)
                {
                    $query->where('pemeriksaan2', '=' , 'done')
                            ->orwhere('pemeriksaan', '=' , 'done')
                            ->orwhere('siklus_1', '=' , 'done')
                            ->orwhere('siklus_2', '=' , 'done');
                })
            ->get();

         // dd($data);
        return view('data_evaluasi.bac', compact('data','datas','dd','id','type','siklus','datas','data1','tanggal'));
    }
     public function uri(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $user = Auth::user()->id;
        

        $dd = $request->tanggal;
        $siklus = $request->siklus;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
            $request->session()->put('siklus', $siklus);
        } else {
            $dd = $request->session()->get('tanggal');
            $siklus = $request->session()->get('siklus');
        }
        $tanggal = $dd;
        $setsiklus = DB::table('tb_siklus')->first();
        $tahunevaluasi = DB::table('tb_tahun_evaluasi')->first();

        $user = Auth::user()->id;

        $datas =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias',DB::raw('YEAR(tb_registrasi.created_at) as tgl'))
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', '>=' , 2)
                ->where(function ($query) use ($siklus){
                    if($siklus == 1){
                        $query->whereIn('tb_registrasi.siklus', ['1','12']);
                    }else{
                        $query->whereIn('tb_registrasi.siklus', ['2','12']);
                    }
                })
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tanggal)
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        
         // dd($data);
        return view('data_evaluasi.uri', compact('data','datas','tanggal','id','type','siklus','setsiklus','tahunevaluasi'));
    }

    public function printevaluasiuri(\Illuminate\Http\Request $request, $id){
       $input = $request->all();
        // dd($input);
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $date = $tahun1->year;

        $tanggal = date('Y m d');
        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
            ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp')
            ->where('parameter.kategori', 'urinalisa')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
            foreach ($data as $key => $val) {
                $metode = Rujukanurinalisametode::where('parameter', $val->id)
                                                ->where('metode',$val->Kode)
                                                ->where('siklus', $siklus)
                                                ->where('tahun', $date)
                                                ->where('type', $type)
                                                ->get();
                $val->metode = $metode;
                $reagen = ReagenImunologi::where('parameter_id', $val->id)->get();
                $val->reagen = $reagen;
                $hasilPemeriksaan = TbHp::where('parameter_id', $val->id)->get();
                $val->hasilPemeriksaan = $hasilPemeriksaan;
                $target = Rujukanurinalisa::where('parameter', $val->id)
                                            ->where('siklus', $siklus)
                                            ->where('tahun', $date)
                                            ->where('type', $type)
                                            ->get();
                $val->target = $target;
            }

        // dd($data[10]->target);
        $skoring = DB::table('tb_skoring_urinalisa')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $date)
                    // ->where('bahan', $input['bahan'])
                    ->get();
        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();


        $skoring1 = DB::table('tb_skoring_urinalisa')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $date)
                    // ->where('bahan', $input['bahan'])
                    ->first();


        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $evaluasi = TanggalEvaluasi::where('form','=','Urinalisa')->where('siklus', $siklus)->where('tahun', $date)->first(); 

        if (count($skoring) > 0) {
        $pdf = PDF::loadview('evaluasi/urinalisa/print/evaluasi', compact('data' ,'perusahaan', 'datas', 'type','siklus','date','instrumen', 'tanggal', 'skoring','id','evaluasi','register','perusahaan'))
        ->setPaper('a4', 'potrait')
        ->setwarnings(false);
        return $pdf->stream('Evaluasi Urinalisa.pdf');
        }else{
             Session::flash('message', 'Urinalisa belum dievaluasi!');
             Session::flash('alert-class', 'alert-danger');
            return back();
        }
        
    }


     public function cetakhemserti(\Illuminate\Http\Request $request, $id){
        $type = $request->get('y');
        $date = date('d F Y');
        $tanggal = $request->tanggal;
        $nomor = $request->nomor;
                
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;

        $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=','Hematologi')->where('siklus', $type)->where('tahun',$tahun)->first();
        // dd($sertifikat);
        if ($request->query('download') == "true") {
            $url = url('data-evaluasi/hematologi/sertifikat/print/'.$id.'?x='.$request->query('x').'&y='.$request->query('y').'&tanggal='.$request->query('tanggal').'&nomor='.$request->query('nomor'));
            $image = new WKHTMLPDF($url);
            $image->setOptions([
                'binary' => env('PATH_WKHTMLPDF', '/usr/local/bin/wkhtmltopdf'),
                'orientation' => 'landscape',
                'page-size' => 'A4',
                'margin-top' => '0cm',
                'margin-right' => '0cm',
                'margin-bottom' => '0cm',
                'margin-left' => '0cm',
                'dpi' => '100',
            ]);

            if(!$image->send('Sertifikat Hematologi.pdf')){
                dd($image->getError());
            }
        }else{
            $data = Register::join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->whereIn('tb_registrasi.bidang',array(1))
                ->where('tb_registrasi.id',$id)
                // ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->select('tb_registrasi.kode_lebpes','tb_registrasi.id as idr','perusahaan.id as idp','perusahaan.nama_lab',DB::raw('GROUP_CONCAT(sub_bidang.parameter) as jenis_form'))
                ->groupBy('tb_registrasi.perusahaan_id')
                ->get();
            // dd($data);
            // if (count($kesimpulan) > 0) {
            \Debugbar::disable();
            return view('evaluasi.imunologi.sertifikat.hem', compact('data', 'perusahaan', 'data1', 'data2', 'type','kodeperusahaan','evaluasi', 'strategi', 'reagen','date','kesimpulan','tahun', 'catatan','tanggal', 'nomor','sertifikat'));
        }
    }

     public function evaluasi(\Illuminate\Http\Request $request, $id){
        $input = $request->all();
        $user = Auth::user()->id;
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');

        $dd = $request->tanggal;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
        } else {
            $dd = $request->session()->get('tanggal');
        }
        $tanggal = $dd;

        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
        $datas1=  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();

        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
            ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp')
            ->where('parameter.kategori', 'urinalisa')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
            foreach ($data as $key => $val) {
                $metode = MetodePemeriksaan::where('parameter_id', $val->id)->get();
                $val->metode = $metode;
                $reagen = ReagenImunologi::where('parameter_id', $val->id)->get();
                $val->reagen = $reagen;
                $hasilPemeriksaan = TbHp::where('parameter_id', $val->id)->get();
                $val->hasilPemeriksaan = $hasilPemeriksaan;
                $target = Rujukanurinalisa::where('parameter', $val->id)
                                            ->where('siklus', $siklus)
                                            ->where('tahun', $date)
                                            ->where('bahan', $input['bahan'])
                                            ->get();
                $val->target = $target;
            }

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        if (count($data[10]->target)) {
            return view('data_evaluasi/urinput', compact('data','dd','simpan', 'perusahaan', 'datas1', 'type','siklus','date','instrumen', 'tanggal', 'skoring','id', 'input','tanggal'));
        }else{
            Session::flash('message', 'Data Rujukan Belum Lengkap!');
            Session::flash('alert-class', 'alert-danger');
            return redirect::back();
        }
    }

     public function evaluasi1(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->y;
        $years = date('Y');
        $register = Register::find($id);
        $user = Auth::user()->id;
        
        $dd = $request->tanggal;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
        } else {
            $dd = $request->session()->get('tanggal');
        }
        $tanggal = $dd;


        $datas=  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $dd)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('sub_bidang.id', '=', '13')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();

            $data1 =  DB::table('tb_registrasi')
            ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
            ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
            ->where('tb_registrasi.created_by','=', Auth::user()->id)
            ->where('sub_bidang.id', '=', '13')
            ->where('status', 3)
            ->where(function($query)
                {
                    $query->where('status_data1', 2)
                        ->orwhere('status_data2', 2)
                        ->orwhere('status_datarpr1', 2)
                        ->orwhere('status_datarpr2', 2);
                })
            ->where(function($query)
                {
                    $query->where('pemeriksaan2', '=' , 'done')
                            ->orwhere('pemeriksaan', '=' , 'done')
                            ->orwhere('siklus_1', '=' , 'done')
                            ->orwhere('siklus_2', '=' , 'done');
                })
            ->get();
        // dd($data);
        // dd($data);
        return view('data_evaluasi.bacinput', compact('data','tanggal','data1','datas','id','siklus'));
    }

    public function printevaluasi(\Illuminate\Http\Request $request, $id){
       $input = $request->all();
        // dd($input);
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $tanggal = date('Y m d');
        $instrumen = DB::table('tb_instrumen')->where('id_parameter', 'urinalisa')->get();
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->leftjoin('tb_reagen_imunologi', 'hp_details.reagen', '=', 'tb_reagen_imunologi.id')
            ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('tb_hp','hp_details.hasil_pemeriksaan', '=', 'tb_hp.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail','hp_details.reagen as hp_reagen','tb_reagen_imunologi.reagen as imu_reagen','hp_details.reagen_lain', 'tb_hp.hp')
            ->where('parameter.kategori', 'urinalisa')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
            foreach ($data as $key => $val) {
                $metode = MetodePemeriksaan::where('parameter_id', $val->id)->get();
                $val->metode = $metode;
                $reagen = ReagenImunologi::where('parameter_id', $val->id)->get();
                $val->reagen = $reagen;
                $hasilPemeriksaan = TbHp::where('parameter_id', $val->id)->get();
                $val->hasilPemeriksaan = $hasilPemeriksaan;
                $target = Rujukanurinalisa::where('parameter', $val->id)
                                            ->where('siklus', $siklus)
                                            ->where('tahun', $date)
                                            ->where('bahan', $input['bahan'])
                                            ->get();
                $val->target = $target;
            }

        // dd($data[10]->target);
        $skoring = DB::table('tb_skoring_urinalisa')
                    ->where('id_registrasi', $id)
                    ->where('siklus', $siklus)
                    ->where('type', $type)
                    ->where('tahun', $date)
                    ->where('bahan', $input['bahan'])
                    ->get();
        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();


        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        
        if ($input['simpan'] == 'Print') {
            if (count($data[10]->target)) {
            $pdf = PDF::loadview('evaluasi/urinalisa/print/evaluasi', compact('data' ,'perusahaan', 'datas', 'type','siklus','date','instrumen', 'tanggal', 'skoring','id'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
            return $pdf->stream('Evaluasi Urinalisa.pdf');
            }else{
                return redirect::back();
            }
        }else if($input['simpan'] == 'Update'){
            Skoringuri::where("id", $request->id)->update(["catatan" => $request->catatan, "status" => "1"]);
            return back();
        }

    }

    public function kepekaan(\Illuminate\Http\Request $request, $id){
        $siklus = $request->y;
        $years = date('Y');

        $register = Register::find($id);
        $user = Auth::user()->id;


        $dd = $request->tanggal;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
        } else {
            $dd = $request->session()->get('tanggal');
        }
        $tanggal = $dd;

        $datas=  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('sub_bidang.id', '=', '13')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();

            $data1 =  DB::table('tb_registrasi')
            ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
            ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
            ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
            ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
            ->where('tb_registrasi.created_by','=', Auth::user()->id)
            ->where('sub_bidang.id', '=', '13')
            ->where('status', 3)
            ->where(function($query)
                {
                    $query->where('status_data1', 2)
                        ->orwhere('status_data2', 2)
                        ->orwhere('status_datarpr1', 2)
                        ->orwhere('status_datarpr2', 2);
                })
            ->where(function($query)
                {
                    $query->where('pemeriksaan2', '=' , 'done')
                            ->orwhere('pemeriksaan', '=' , 'done')
                            ->orwhere('siklus_1', '=' , 'done')
                            ->orwhere('siklus_2', '=' , 'done');
                })
            ->get();

        return view('data_evaluasi.bacinput', compact('data','tanggal','data1','datas','id','siklus'));
    }


  
    public function kat(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $input = $request->all();
        $user = Auth::user()->id;
        
        $dd = $request->tanggal;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
        } else {
            $dd = $request->session()->get('tanggal');
        }
        $tanggal = $dd;

        $datas=  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('sub_bidang.id', '=', '12')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();       
         // dd($data);
        return view('data_evaluasi.kat', compact('data','tanggal','id','type','siklus','datas'));
    }

    public function kai(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $input = $request->all();
        

        $dd = $request->tanggal;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
        } else {
            $dd = $request->session()->get('tanggal');
        }
        $tanggal = $dd;

        $user = Auth::user()->id;
        $datas=  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();

        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('users', 'users.id', '=', 'tb_registrasi.created_by')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by','=', Auth::user()->id)
                ->where('sub_bidang.id', '=', '11')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
         // dd($data);
        return view('data_evaluasi.kai', compact('data','id','type','siklus','datas','tanggal'));
    }

    public function evaluasi_cacing(\Illuminate\Http\Request $request, $id)
    {
        $input =$request->all();
        $siklus = $request->get('y');
        $tahun = date('Y');
        $user = Auth::user()->id;
        
        $dd = $request->tanggal;
        if (!empty($dd)) {
            $request->session()->put('tanggal', $dd);
        } else {
            $dd = $request->session()->get('tanggal');
        }
        $tanggal = $dd;

        $datas1=  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','sub_bidang.alias')
                ->where('tb_registrasi.created_by', $user)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=', $dd)
                // ->where('tb_registrasi.bidang', 1)
                ->groupBy('tb_registrasi.bidang')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        // dd($evaluasi);
        return view('data_evaluasi.tccinput', compact('data','datas1','tanggal','user','id','siklus', 'perusahaan','type','rujuk','evaluasi','nilaievaluasi'));
    }
}
