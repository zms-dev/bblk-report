<?php
namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\telurcacing as Telurcacing;
use App\daftar as Daftar;
use App\EvaluasiTc;
use App\register as Register;
use Redirect;
use Validator;
use Session;
use App\sarantc;

class TelurCacingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        return view('hasil_pemeriksaan/telur_cacing', compact('data', 'perusahaan', 'siklus','date'));
    }

    public function view(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        // $data = DB::table('parameter')
        //     ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
        //     ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
        //     ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
        //     ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil')
        //     ->where('parameter.kategori', 'hematologi')
        //     ->where('hp_headers.id_registrasi', $id)
        //     ->get();
        // $datas = HpHeader::where('id_registrasi', $id)
        //     ->first();
        $data = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->where('siklus', $siklus)
                ->first();
        $datas = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->where('siklus', $siklus)
                ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        
        // return view('cetak_hasil/telur-cacing', compact('data', 'perusahaan', 'datas', 'type'));
        $pdf = PDF::loadview('cetak_hasil/telur-cacing', compact('data', 'perusahaan','datas', 'type','date','siklus'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Telur Cacing.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    { 
        $siklus = $request->get('y');
        $kode_botol = $request->kode_botol; 
        $reagen = $request->reagen; 
        $reagen_lain = $request->reagen_lain; 
        $hasil = $request->hasil;
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $years = $tahun->year;

        $input = $request->all();
        // dd($input);
        $validasi = Telurcacing::where(DB::raw('YEAR(tb_telurcacing.created_at)'), '=' , $years)->where('id_registrasi','=',$id)->where('siklus','=',$siklus)->get();
       
        $i = 0;
        if (count($validasi)>0) {
            Session::flash('message', 'Hasil Telur Cacing Sudah Ada!'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
            }else{
            foreach ($input['kode_botol'] as $kode) {
                if($kode != ''){
                    $data = new Telurcacing;
                    $data['kode_botol'] = $input['kode_botol'][$i];
                    $data['tgl_penerimaan'] = $request->tgl_penerimaan;
                    $data['tgl_pemeriksaan'] = $request->tgl_pemeriksaan;
                    $data['reagen'] = $reagen[$i];
                    $data['kondisi'] = $request->kondisi; 
                    $data['reagen_lain'] = $reagen_lain[$i];
                    $data['hasil'] = htmlentities($hasil[$i]);  
                    $data['kode_peserta'] = $request->kode_peserta;
                    $data['nama_pemeriksa'] = $request->nama_pemeriksa;
                    $data['nomor_hp'] = $request->nomor_hp;
                    $data['catatan'] = $request->catatan;
                    $data['penanggung_jawab'] = $request->penanggung_jawab;
                    $data['created_by'] = Auth::user()->id; 
                    $data['id_registrasi'] = $id;
                    $data['siklus'] = $siklus;
                    $data->save();
                    }
                $i++;
            }   
            

        $date_sekarang = date('Y-m-d H:i:s');
        DB::table('tanggal_input')->insert(['id_register' => $id, 'siklus' => $siklus, 'input_date' => $date_sekarang]);
        if ($siklus == '1') {
            Register::where('id',$id)->update(['siklus_1'=>'done', 'rpr1'=>'done', 'status_data1'=>'1']);
            Register::where('id',$id)->update(['pemeriksaan'=>'done']);
        }else{
            Register::where('id',$id)->update(['siklus_2'=>'done', 'rpr2'=>'done', 'status_data2'=>'1']);
            Register::where('id',$id)->update(['pemeriksaan2'=>'done']);
        }
        return redirect('hasil-pemeriksaan');
        }
    }

    public function edit(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $date = $tahun1->year;

        $data = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->first();
        $datas = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        
        return view('edit_hasil/telur_cacing', compact('data', 'perusahaan', 'datas', 'siklus','date'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $register = Register::find($id);

        // dd($input);
        $i = 0;
        foreach ($input['kode_botol'] as $kode_botol) {
            if($kode_botol != ''){
                $Savedata['tgl_pemeriksaan'] = $request->tgl_pemeriksaan;
                $Savedata['tgl_penerimaan'] = $request->tgl_penerimaan;
                $Savedata['catatan'] = $request->catatan;
                $Savedata['penanggung_jawab'] = $request->penanggung_jawab;
                $Savedata['nama_pemeriksa'] = $request->nama_pemeriksa;
                $Savedata['nomor_hp'] = $request->nomor_hp;
                $Savedata['kode_botol'] = $request->kode_botol[$i];
                $Savedata['reagen'] = $request->reagen[$i];
                $Savedata['reagen_lain'] = $request->reagen_lain[$i];
                $Savedata['hasil'] = $request->hasil[$i];
                $Savedata['kondisi'] = $request->kondisi;
                Telurcacing::where('id', $request->idtc[$i])->update($Savedata);
            }
            $i++;
        }
        // dd($input);
        if ($request->simpan == "Kirim" || $request->simpan == "Validasi") {
            if ($siklus == '1') {
                Register::where('id',$id)->update(['status_data1'=>'2']);
            }else{
                Register::where('id',$id)->update(['status_data2'=>'2']);
            }
            if ($request->simpan == "Validasi") {
                DB::table('validasi_input')->where('id_register', $id)->where('siklus', $siklus)->delete();
                DB::table('validasi_input')->insert(
                    ['id_register' => $id, 'siklus' => $siklus, 'status' => 'done']
                );
                return back();
            }else{
                return redirect('edit-hasil');
            }
        }else{
            return back();
        }
    }

    public function evaluasi(\Illuminate\Http\Request $request, $id)
    {
        $input =$request->all();
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;

        $data = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();
        $datas = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->get();
        $rujuk = DB::table('tb_rujukan_tc')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun)
                ->orderBy('kode_sediaan', 'asc')
                ->get();
        
        $evaluasi = DB::table('tb_evaluasi_tc')->where('id_registrasi',$id)->where('siklus', $siklus)->get();
        $evaluasisaran = DB::table('tb_evaluasi_tc')->where('id_registrasi',$id)->where('siklus', $siklus)->where('tahun',$tahun)->first();
        // dd($evaluasi);
        $nilaievaluasi = DB::table('tb_evaluasi_tc')->where('id_registrasi',$id)->where('siklus', $siklus)->select(DB::raw('SUM(nilai) as nilai'))->first();
       
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;

        $salah = DB::table('tb_saran_tc')->where('id_registrasi',$id)->where('tahun','=', $tahun)->where('siklus','=',$siklus)->first();

        return view('evaluasi.tc.penilaian.evaluasi.index', compact('data','evaluasisaran','id','salah','siklus', 'saran','perusahaan', 'datas', 'type','rujuk','evaluasi','nilaievaluasi','register'));
    }

    public function insertevaluasi(\Illuminate\Http\Request $request, $id)
    { 
        $input = $request->all();
        // dd($input);
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;

        $i = 0;
        if ($input['simpan'] == 'Simpan') {
            $Savedatakomen = new sarantc;
            $Savedatakomen->id_registrasi = $id;
            $Savedatakomen->siklus = $siklus;
            $Savedatakomen->tahun = $tahun;
            $Savedatakomen->saran = $input['saran'];
            if (isset($input['salah_spesies'])) {
                $Savedatakomen->salah_spesies = $input['salah_spesies'];
            }else{
                $Savedatakomen->salah_spesies = 0;
            }
            if (isset($input['negatif_palsu'])) {  
                $Savedatakomen->negatif_palsu = $input['negatif_palsu'];
            }else{
                $Savedatakomen->negatif_palsu = 0;
            }
            if (isset($input['spesies_kurang'])) {  
                $Savedatakomen->spesies_kurang = $input['spesies_kurang'];
            }else{
                $Savedatakomen->spesies_kurang = 0;
            }
            if (isset($input['positif_palsu'])) {  
                $Savedatakomen->positif_palsu = $input['positif_palsu'];
            }else{
                $Savedatakomen->positif_palsu = 0;
            }
            // $Savedatakomen->tanggal_ttd = $input['ttd'];
            $Savedatakomen->save();
            
            foreach ($input['kode_sediaan'] as $kode) {
                if($kode != ''){
                    $Savedata = new EvaluasiTc;
                    $Savedata->id_registrasi = $id;
                    $Savedata->kode_sediaan = $input['kode_sediaan'][$i];
                    $Savedata->nilai = $input['nilai'][$i];
                    $Savedata->siklus = $siklus;
                    $Savedata->tahun = $tahun;
                    $Savedata->save();
                }
                $i++;
            }
            
        }
        return back();
    }
     
    public function printevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');

        $input =$request->all();
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;

        $tanggal = Date('Y m d');
        $data = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();

        $datas = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->get();
        $rujuk = DB::table('tb_rujukan_tc')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun)
                ->get();
        $evaluasi = DB::table('tb_evaluasi_tc')->where('id_registrasi',$id)->where('siklus', $siklus)->get();
        $salah = DB::table('tb_saran_tc')->where('id_registrasi',$id)->where('tahun','=', $tahun)->where('siklus','=',$siklus)->first();
        $evaluasisaran = DB::table('tb_evaluasi_tc')->where('id_registrasi',$id)->where('siklus', $siklus)->where('tahun',$tahun)->first();
        
        $nilaievaluasi = DB::table('tb_evaluasi_tc')->where('id_registrasi',$id)->where('siklus', $siklus)->select(DB::raw('SUM(nilai) as nilai'))->first();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $i = 0;
        $tanggalevaluasi = DB::table('tanggal_evaluasi')->where('form', '=', 'Mikrobiologi TC')->where('siklus', $siklus)->where('tahun', $tahun)->first();
        // dd($evaluasi);
        // return view('evaluasi/tc/penilaian/evaluasi/print', compact('data','salah','evaluasisaran','tanggal','evaluasi','rujuk','nilaievaluasi','id','siklus','tanggal','tahun','register','perusahaan','datas','tanggalevaluasi'));
        if (count($evaluasi) > 0) {
        $pdf = PDF::loadview('evaluasi/tc/penilaian/evaluasi/print', compact('data','salah','evaluasisaran','tanggal','evaluasi','rujuk','nilaievaluasi','id','siklus','tanggal','tahun','register','perusahaan','datas','tanggalevaluasi'));
        $pdf->setPaper('a4','potrait');
        return $pdf->stream('Telur Cacing.pdf');
        }else{
            Session::flash('message', 'TC belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }

    public function printdataevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');

        $input =$request->all();
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;

        $tanggal = Date('Y m d');
        $data = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();

        $datas = DB::table('tb_telurcacing')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->get();
        $rujuk = DB::table('tb_rujukan_tc')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun)
                ->get();
        $evaluasi = DB::table('tb_evaluasi_tc')->where('id_registrasi',$id)->where('siklus', $siklus)->get();
        $salah = DB::table('tb_saran_tc')->where('id_registrasi',$id)->where('tahun','=', $tahun)->where('siklus','=',$siklus)->first();
        $evaluasisaran = DB::table('tb_evaluasi_tc')->where('id_registrasi',$id)->where('siklus', $siklus)->where('tahun',$tahun)->first();
        
        $nilaievaluasi = DB::table('tb_evaluasi_tc')->where('id_registrasi',$id)->where('siklus', $siklus)->select(DB::raw('SUM(nilai) as nilai'))->first();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $i = 0;
        $tanggalevaluasi = DB::table('tanggal_evaluasi')->where('form', '=', 'Mikrobiologi TC')->where('siklus', $siklus)->where('tahun', $tahun)->first();
        // dd($evaluasi);
        if (count($evaluasi) > 0) {
        $pdf = PDF::loadview('evaluasi/tc/penilaian/evaluasi/print', compact('data','salah','evaluasisaran','tanggal','evaluasi','rujuk','nilaievaluasi','id','siklus','tanggal','tahun','register','perusahaan','datas','tanggalevaluasi'));
        $pdf->setPaper('a4','potrait');
        return $pdf->stream('Telur Cacing.pdf');
        }else{
            Session::flash('message', 'TC belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }

    public function updateevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $input =$request->all();
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;

        $i = 0; 
        if($input['method'] == 'Update'){
            $status = sarantc::where("tb_saran_tc.id_registrasi", $id)
            ->where('tb_saran_tc.tahun','=', $tahun)
            ->where('tb_saran_tc.siklus','=', $siklus)
            ->update(["salah_spesies" => $request->salah_spesies,"negatif_palsu" => $request->negatif_palsu,"spesies_kurang" => $request->spesies_kurang,"positif_palsu" => $request->positif_palsu,"saran" => $request->saran]);
            foreach ($input['kode_sediaan'] as $kode) {
                    if($kode != ''){
                        $Savedata['siklus'] = $siklus;
                        $Savedata['kode_sediaan'] = $request->kode_sediaan[$i];
                        $Savedata['nilai'] = $request->nilai[$i];
                        EvaluasiTc::where('id', $request->id[$i])->update($Savedata);
                    }
                    $i++;
                }
            return back();
        }  
    }
}


     