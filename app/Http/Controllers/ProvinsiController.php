<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\provinsi as Provinsi;
use Illuminate\Support\Facades\Redirect;

class ProvinsiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = DB::table('provinces')->get();
        return view('back/provinsi/index', compact('data'));
    }

    public function edit($id)
    {
        $data = DB::table('provinces')->where('id', $id)->get();
        return view('back/provinsi/update', compact('data'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data['name'] = $request->name;
        Provinsi::where('id',$id)->update($data);
        Session::flash('message', 'Data Provinsi Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/provinsi');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Provinsi::create($data);
        Session::flash('message', 'Data Provinsi Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/provinsi');
    }

    public function delete($id){
        Session::flash('message', 'Data Provinsi Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Provinsi::find($id)->delete();
        return redirect("admin/provinsi");
    }
}
