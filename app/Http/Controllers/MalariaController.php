<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\malaria as Malaria;
use App\EvaluasiMalaria;
use App\daftar as Daftar;
use App\register as Register;
use Redirect;
use Validator;
use Session;

class MalariaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;

        $siklus = $request->get('y');
        $data = DB::table('parameter')->where('kategori', 'malaria')->get();

        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;
        
        return view('hasil_pemeriksaan/malaria', compact('data', 'perusahaan','siklus','tahun'));
    }

    public function view(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $data = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->first();
        $datas = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        
        // $date = $data->created_at->year;
        // return view('cetak_hasil/telur-cacing', compact('data', 'perusahaan', 'datas', 'type'));
        $pdf = PDF::loadview('cetak_hasil/malaria', compact('data', 'perusahaan','datas', 'type','siklus','date'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Malaria.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    { 
        $input = $request->all();
        $siklus = $request->get('y');
        // $input = implode(", ",$input['stadium']);
        $i = 1;
                // dd($input); 
          $years = date('Y');

        $validasi = Malaria::where(DB::raw('YEAR(tb_malaria.created_at)'), '=' , $years)->where('id_registrasi','=',$id)->where('siklus','=',$siklus)->get();// dd($input);
         if (count($validasi)>0) {
            Session::flash('message', 'Hasil Malaria Sudah Ada!'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
        }else{  
        foreach ($input['kode'] as $kode) {
            if($kode != ''){

                $Savedata = new Malaria;
                $Savedata->kode = $input['kode'][$i];
                if(!empty($input['hasil'][$i])){
                    $Savedata->hasil = $input['hasil'][$i];    
                }

                if(!empty($input['spesies'][$i])){
                    $Savedata->spesies = $input['spesies'][$i];    
                }

                $stadium = "";
                if(!empty($input['stadium'][$i])){
                    $stadium = implode(", ",$input['stadium'][$i]);     
                }

                $Savedata->stadium = $stadium;
                $Savedata->kondisi = $input['kondisi'];
                $Savedata->kode_peserta = $input['kode_peserta'];
                $Savedata->nomor_hp = $input['nomor_hp'];
                $Savedata->nama_pemeriksa = $input['nama_pemeriksa'];
                $Savedata->parasit = $input['parasit'][$i];    
                $Savedata->penanggung_jawab = $input['penanggung_jawab'];
                $Savedata->catatan = $input['catatan'];
                $Savedata->tgl_penerimaan = $input['tgl_penerimaan'];
                $Savedata->tgl_pemeriksaan = $input['tgl_pemeriksaan'];
                $Savedata->created_by = Auth::user()->id;
                $Savedata->id_registrasi = $id;
                $Savedata->siklus = $siklus;
                $Savedata->save();
            }
            $i++;
        }
    
        $date_sekarang = date('Y-m-d H:i:s');
        DB::table('tanggal_input')->insert(['id_register' => $id, 'siklus' => $siklus, 'input_date' => $date_sekarang]);
        if ($siklus == '1') {
            Register::where('id',$id)->update(['siklus_1'=>'done', 'rpr1'=>'done', 'status_data1'=>'1']);
            Register::where('id',$id)->update(['pemeriksaan'=>'done']);
        }else{
            Register::where('id',$id)->update(['siklus_2'=>'done', 'rpr2'=>'done', 'status_data2'=>'1']);
            Register::where('id',$id)->update(['pemeriksaan2'=>'done']);
        }
        return redirect('hasil-pemeriksaan');
        }
    }

    public function edit(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $data = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->first();
        $datas = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        
        return view('edit_hasil/malaria', compact('data', 'perusahaan', 'datas', 'siklus'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $register = Register::find($id);

        $i = 1;
        foreach ($input['kode'] as $kode) {
            $Savedata = [];
            if($kode != ''){
                if(!empty($input['hasil'][$i])){
                    $Savedata['hasil'] = $request->hasil[$i];    
                }

                if(!empty($input['spesies'][$i])){
                    $Savedata['spesies'] = $request->spesies[$i];    
                }

                if(!empty($input['parasit'][$i])){
                    $Savedata['parasit'] = $request->parasit[$i];    
                }

                $stadium = "";
                if(!empty($input['stadium'][$i])){
                    $stadium = implode(",",$request->stadium[$i]);     
                }

                $Savedata['siklus'] = $siklus;
                $Savedata['tgl_pemeriksaan'] = $request->tgl_pemeriksaan;
                $Savedata['tgl_penerimaan'] = $request->tgl_penerimaan;
                $Savedata['catatan'] = $request->catatan;
                $Savedata['penanggung_jawab'] = $request->penanggung_jawab;
                $Savedata['nama_pemeriksa'] = $request->nama_pemeriksa;
                $Savedata['nomor_hp'] = $request->nomor_hp;
                $Savedata['stadium'] = $stadium;
                $Savedata['kode'] = $request->kode[$i];
                $Savedata['kondisi'] = $request->kondisi;
                $wakwaw[$i] = $Savedata; 
                Malaria::where('id', $request->idmal[$i])->update($Savedata);
            }
            $i++;
        }
        if ($request->simpan == "Kirim" || $request->simpan == "Validasi") {
            if ($siklus == '1') {
                Register::where('id',$id)->update(['status_data1'=>'2']);
            }else{
                Register::where('id',$id)->update(['status_data2'=>'2']);
            }
            if ($request->simpan == "Validasi") {
                DB::table('validasi_input')->where('id_register', $id)->where('siklus', $siklus)->delete();
                DB::table('validasi_input')->insert(
                    ['id_register' => $id, 'siklus' => $siklus, 'status' => 'done']
                );
                return back();
            }else{
                return redirect('edit-hasil');
            }
        }else{
            return redirect('edit-hasil');
        }
    }

    public function evaluasi(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $tahun = date('Y');
        $data = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();
        $datas = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->get();
        $rujuk = DB::table('tb_rujukan_malaria')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun)
                ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $evaluasi = DB::table('tb_evaluasi_malaria')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->first();
        // $date = $data->created_at->year;
        return view('evaluasi.malaria.penilaian.evaluasi.index', compact('data', 'perusahaan', 'datas', 'type', 'rujuk', 'evaluasi', 'id', 'siklus'));
    }

    public function cetakevaluasi(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $tahun = date('Y');
        $tanggal = \Carbon\Carbon::now();
        $data = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();
        $datas = DB::table('tb_malaria')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->get();
        $rujuk = DB::table('tb_rujukan_malaria')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun)
                ->get();
        $register = Register::find($id)->where('bidang','=','10')->first();
        $perusahaan = $register->perusahaan->nama_lab;
        $evaluasi = DB::table('tb_evaluasi_malaria')
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->where('tb_evaluasi_malaria.tahun','=',$tahun)
                    ->first();
        // $date = $data->created_at->year;
        // return view('evaluasi.malaria.penilaian.evaluasi.cetak', compact('data', 'perusahaan', 'datas', 'type', 'rujuk', 'evaluasi', 'siklus', 'tahun', 'tanggal'));
        if (count($evaluasi) > 0) {
        $pdf = PDF::loadView('evaluasi.malaria.penilaian.evaluasi.cetak',compact('data', 'perusahaan', 'datas', 'type', 'rujuk', 'evaluasi', 'siklus', 'tahun', 'tanggal', 'register'));
            $pdf->setPaper('a4','potrait');
            return $pdf->stream('Malaria.pdf');
        }else{
            Session::flash('message', 'Malaria belum dievaluasi'); 
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }
    public function insertevaluasi(\Illuminate\Http\Request $request, $id)
    { 
        $input = $request->all();
        $siklus = $request->get('y');
        $tahun = date('Y');
        // dd($input);
        if ($input['simpan'] == 'Simpan') {
            $Savedata = new EvaluasiMalaria;
            $Savedata->kesalahan = $input['kesalahan'];
            $Savedata->kesimpulan = $input['kesimpulan'];
            $Savedata->tindakan = $input['tindakan'];
            $Savedata->total = $input['total'];
            $Savedata->id_registrasi = $id;
            $Savedata->siklus = $siklus;
            $Savedata->tahun = $tahun;
            $Savedata->save();
        }else {
            $Savedata['kesalahan'] = $request->kesalahan;
            $Savedata['tindakan'] = $request->tindakan;
            EvaluasiMalaria::where('id_registrasi', $id)->where('siklus', $siklus)->update($Savedata);
        }

        return redirect::back();
    }
}
