<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;

class LaporanCetakHasilController extends Controller
{
    public function index(\Illuminate\Http\Request $request){
        $bidang = DB::table('sub_bidang')->where('id', '>', '3')->get();
        if($request->ajax()){
            $datas = DB::table('tb_registrasi')
            ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
            ->select(DB::raw('tb_registrasi.*,perusahaan.nama_lab,sub_bidang.link'));
            $datas->where('bidang','=',$request->query('sub_bidang_id'));
            $datas->where(DB::raw('YEAR(tb_registrasi.created_at)'),'=',$request->query('tahun'));
            $siklus = $request->query('siklus');
                $datas->where(function($q)use($siklus){
                    if($siklus == 1){
                        $q->orWhere('pemeriksaan','=','done')
                        ->orWhere('pemeriksaan2','=','done');
                    }else{
                        $q->orWhere('rpr1','=','done')
                        ->orWhere('rpr2','=','done');
                    }
                    
                });
            return Datatables::of($datas)
            ->addColumn('action', function($data)use($request){
                $action = "";
                    if($request->query('siklus') == 1){
                        if($data->pemeriksaan == "done"){
                            $action .= "<a href='".url($data->link."/print/".$data->id)."?x=a&y=1' target='_BLANK' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-print'></i>]<div style='display:block'>A</div></a>";
                        }
                        if($data->pemeriksaan2 == "done"){
                            $action .= "<a href='".url($data->link."/print/".$data->id)."?x=b&y=1'  target='_BLANK' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-print'></i>]<div style='display:block'>B</div></a>";
                        }
                    }else{
                        if($data->rpr1 == "done"){
                            $action .= "<a href='".url($data->link."/print/".$data->id)."?x=a&y=2' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-print'></i>]<div style='display:block'>A</div></a>";
                        }
                        if($data->rpr2 == "done"){
                            $action .= "<a href='".url($data->link."/print/".$data->id)."?x=b&y=2' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-print'></i>]<div style='display:block'>B</div></a>";
                        }
                    }
                    
                  
                return $action
            ;})
            ->make(true);
        }
        return view('laporan.cetak-hasil.index',compact('bidang','request'));
    }

    public function edit(\Illuminate\Http\Request $request){
        $bidang = DB::table('sub_bidang')->get();
        if($request->ajax()){
            $siklus = $request->query('siklus');
            $bidangna = $request->query('sub_bidang_id');

            $datas = DB::table('tb_registrasi')
            ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
            ->leftJoin('tanggal_input', function($leftJoin)use($siklus)
            {
                $leftJoin->on('tb_registrasi.id', '=', 'tanggal_input.id_register');
                $leftJoin->on(DB::raw('tanggal_input.siklus'), DB::raw('='),DB::raw("'".$siklus."'"));
            })
            ->groupBy('tb_registrasi.id')
            ->orderBy('tanggal_input.input_date', 'asc')
            ->select(DB::raw('tb_registrasi.*,perusahaan.nama_lab,sub_bidang.link'));
            $datas->where('bidang','=',$request->query('sub_bidang_id'));
            $datas->where(DB::raw('YEAR(tb_registrasi.created_at)'),'=',$request->query('tahun'));
            if($bidangna <= 3){
                $datas->where(function($q)use($siklus){
                    if($siklus == 1){
                        $q->orWhere('pemeriksaan','=','done')
                        ->orWhere('pemeriksaan2','=','done');
                    }else{
                        $q->orWhere('rpr1','=','done')
                        ->orWhere('rpr2','=','done');
                    }
                });
            }else{
                if($siklus == 1){
                    $datas->where('pemeriksaan','=','done');
                }else{
                    $datas->where('pemeriksaan2','=','done');
                }
            }
            return Datatables::of($datas)
            ->addColumn('action', function($data)use($request){
                $action = "";
                    if($request->query('siklus') == 1){
                        if($data->bidang <= 3){
                            if($data->pemeriksaan == "done"){
                                $status = DB::table('validasi_input')
                                            ->where('id_register', $data->id)
                                            ->where('siklus', $request->query('siklus'))
                                            ->where('type', 'a')->first();
                                if ($status != NULL) {
                                    $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=a&y=1' target='_BLANK' style='text-align:center;display:inline-block;width:30px;color:#34a449;'>[<i class='fa fa-edit'></i>]<div style='display:block'>A</div></a>";
                                }else{
                                    $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=a&y=1' target='_BLANK' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-edit'></i>]<div style='display:block'>A</div></a>";
                                }
                            }
                            if($data->pemeriksaan2 == "done"){
                                $status = DB::table('validasi_input')
                                            ->where('id_register', $data->id)
                                            ->where('siklus', $request->query('siklus'))
                                            ->where('type', 'b')->first();
                                if ($status != NULL) {
                                    $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=b&y=1'  target='_BLANK' style='text-align:center;display:inline-block;width:30px;color:#34a449'>[<i class='fa fa-edit'></i>]<div style='display:block'>B</div></a>";
                                }else{
                                    $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=b&y=1'  target='_BLANK' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-edit'></i>]<div style='display:block'>B</div></a>";
                                }
                            }
                        }else{
                            if($data->pemeriksaan == "done"){
                                $status = DB::table('validasi_input')
                                            ->where('id_register', $data->id)
                                            ->where('siklus', $request->query('siklus'))->first();
                                if ($status != NULL) {
                                    $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=a&y=1' target='_BLANK' style='text-align:center;display:inline-block;width:30px;color:#34a449'>[<i class='fa fa-edit'></i>]<div style='display:block'></div></a>";
                                }else{
                                    $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=a&y=1' target='_BLANK' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-edit'></i>]<div style='display:block'></div></a>";
                                }
                            }
                        }
                    }else{
                        if($data->bidang <= 3){
                            if($data->rpr1 == "done"){
                                $status = DB::table('validasi_input')
                                            ->where('id_register', $data->id)
                                            ->where('siklus', $request->query('siklus'))
                                            ->where('type', 'a')->first();
                                if ($status != NULL) {
                                    $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=a&y=2' style='text-align:center;display:inline-block;width:30px;color:#34a449'>[<i class='fa fa-edit'></i>]<div style='display:block'>A</div></a>";
                                }else{
                                    $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=a&y=2' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-edit'></i>]<div style='display:block'>A</div></a>";
                                }
                            }
                            if($data->rpr2 == "done"){
                                $status = DB::table('validasi_input')
                                            ->where('id_register', $data->id)
                                            ->where('siklus', $request->query('siklus'))
                                            ->where('type', 'b')->first();
                                if ($status != NULL) {
                                    $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=b&y=2' style='text-align:center;display:inline-block;width:30px;color:#34a449'>[<i class='fa fa-edit'></i>]<div style='display:block'>B</div></a>";
                                }else{
                                    $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=b&y=2' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-edit'></i>]<div style='display:block'>B</div></a>";
                                }
                            }
                        }else{
                            if($data->pemeriksaan2 == "done"){
                                $status = DB::table('validasi_input')
                                            ->where('id_register', $data->id)
                                            ->where('siklus', $request->query('siklus'))->first();
                                if ($status != NULL) {
                                    $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=a&y=2' target='_BLANK' style='text-align:center;display:inline-block;width:30px;color:#34a449'>[<i class='fa fa-edit'></i>]<div style='display:block'></div></a>";
                                }else{
                                    $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=a&y=2' target='_BLANK' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-edit'></i>]<div style='display:block'></div></a>";
                                }
                            }
                        }
                    }
                return $action
            ;})
            ->make(true);
        }
        return view('laporan.edit-hasil.index',compact('bidang','request'));
    }

    public function editcetak(\Illuminate\Http\Request $request){
        $bidang = DB::table('sub_bidang')->get();
        if($request->ajax()){
            $datas = DB::table('tb_registrasi')
            ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
            ->join('sub_bidang','sub_bidang.id','=','tb_registrasi.bidang')
            ->select(DB::raw('tb_registrasi.*,perusahaan.nama_lab,sub_bidang.link'));
            $datas->where('bidang','=',$request->query('sub_bidang_id'));
            $datas->where(DB::raw('YEAR(tb_registrasi.created_at)'),'=',$request->query('tahun'));
            $siklus = $request->query('siklus');
            $bidangna = $request->query('sub_bidang_id');
            if($bidangna <= 3){
                $datas->where(function($q)use($siklus){
                    if($siklus == 1){
                        $q->orWhere('status_data1','=','2')
                        ->orWhere('status_data2','=','2');
                    }else{
                        $q->orWhere('status_datarpr1','=','2')
                        ->orWhere('status_datarpr2','=','2');
                    }
                });
            }else{
                if($siklus == 1){
                    $datas->where('status_data1','=','2');
                }else{
                    $datas->where('status_data2','=','2');
                }
            }
            return Datatables::of($datas)
            ->addColumn('action', function($data)use($request){
                $action = "";
                    if($request->query('siklus') == 1){
                        if($data->bidang <= 3){
                            if($data->status_data1 == "2"){
                                $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=a&y=1' target='_BLANK' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-edit'></i>]<div style='display:block'>A</div></a>";
                            }
                            if($data->status_data1 == "2"){
                                $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=b&y=1'  target='_BLANK' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-edit'></i>]<div style='display:block'>B</div></a>";
                            }
                        }else{
                            if($data->status_data1 == "2"){
                                $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=a&y=1' target='_BLANK' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-edit'></i>]<div style='display:block'></div></a>";
                            }
                        }
                    }else{
                        if($data->bidang <= 3){
                            if($data->status_datarpr1 == "2"){
                                $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=a&y=2' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-edit'></i>]<div style='display:block'>A</div></a>";
                            }
                            if($data->status_datarpr2 == "2"){
                                $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=b&y=2' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-edit'></i>]<div style='display:block'>B</div></a>";
                            }
                        }else{
                            if($data->status_data2 == "2"){
                                $action .= "<a href='".url($data->link."/edit/".$data->id)."?x=a&y=2' target='_BLANK' style='text-align:center;display:inline-block;width:30px;'>[<i class='fa fa-edit'></i>]<div style='display:block'></div></a>";
                            }
                        }
                    }
                return $action
            ;})
            ->make(true);
        }
        return view('laporan.edit-hasil.cek',compact('bidang','request'));
    }
}
