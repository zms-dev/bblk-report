<?php
namespace App\Http\Controllers;

use Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;
use Session;
use PDF;
use App\UploadLaporan;
use Validator;

use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;


class UploadLaporanAkhirController extends Controller
{
	public function index(\Illuminate\Http\Request $request){
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = UploadLaporan::select( DB::raw('upload_lap_akhir.*, sub_bidang.alias, @rownum := @rownum +1 as rownum'))->join('sub_bidang', 'sub_bidang.id','=', 'upload_lap_akhir.parameter')->where('sub_bidang.id','=',Auth::user()->penyelenggara)->orderBy('id','ASC')->get();
            return Datatables::of($datas)

            ->addColumn('action', function($data){
                return "".'
                        <select name="forma" onchange="location = this.value;" class="form-control">
                            <option></option>
                            <option value="upload-laporan-akhir/edit'.'/'.$data->id.'">Edit</option>
                            <option value="upload-laporan-akhir/download'.'/'.$data->id.'">Download</option>
                            <option value="upload-laporan-akhir/delete'.'/'.$data->id.'">Hapus</option>
                        </select>'
                        ;})
            ->make(true);
        }
        return view('evaluasi/imunologi/spl/index');
    }

   

    public function upload()
    {
        $parameter = DB::table('sub_bidang')->where('sub_bidang.id','=',Auth::user()->penyelenggara)->get();
        return view('evaluasi/imunologi/spl/upload', compact('parameter'));
    }

    public function edit($id)
    {
        // $data = UploadLaporan::find('id')->get();
        // dd($data);
         $data = DB::table('upload_lap_akhir')
                ->leftjoin('sub_bidang', 'sub_bidang.id', '=', 'upload_lap_akhir.parameter')
                ->select('upload_lap_akhir.*' ,'sub_bidang.alias as alias','sub_bidang.id as ids')
                ->where('upload_lap_akhir.id', $id)
                ->get();
                // dd($data);
        $parameter = DB::table('sub_bidang')->where('sub_bidang.id','=',Auth::user()->penyelenggara)->get();

        // dd($data);
     return view('evaluasi/imunologi/spl/update', compact('parameter','id','data'));   
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data = UploadLaporan::find($id);
        $input = $request->all();
        $validasi = Validator::make($input,[
                'file' => 'required|mimes:pdf|max:20000'
            ]);
        if ($validasi->fails()) {
            return redirect('evaluasi/upload-laporan-akhir/edit'.'/'.$id)->withInput()->withErrors($validasi);
        }

        $file= Input::file('file');
        if (!empty($file)) {
            if (Request::hasFile('file'))
                {
                    $dest = public_path('asset/laporan-akhir/');
                    $name = Request::file('file')->getClientOriginalName();
                    Request::file('file')->move($dest, $name);
                }
        }
        $data->tahun = $request['tahun'];
        $data->file = $name;
        $data->siklus = $request['siklus'];
        $data->tipe = $request['tipe'];
        $data->parameter = $request['parameter'];
        $data->save();
        return redirect('/evaluasi/upload-laporan-akhir/');

    }

    public function delete($id)
    {
        $parameter = UploadLaporan::find($id);
        $parameter->delete();
        return back();
    }

    public function insert(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $bagian = $request->get('isi');
        $bagian = $request->get('penutup');
        // dd($bagian);

        $validasi = Validator::make($input,[
                'file' => 'required|mimes:pdf|max:20000'
            ]);
        if ($validasi->fails()) {
            return redirect('evaluasi/upload-laporan-akhir/upload')->withInput()->withErrors($validasi);
        }
        if (Request::hasFile('file'))
        {
            $dest = public_path('asset/laporan-akhir/');
            $name = Request::file('file')->getClientOriginalName();
            Request::file('file')->move($dest, $name);
        }
        $Upload = new UploadLaporan;
        $Upload->file = $name;
        $Upload->tahun = $input['tahun'];
        $Upload->siklus = $input['siklus'];
        $Upload->tipe = $input['tipe'];
        $Upload->parameter = $input['parameter'];
        $Upload->save();

        if ($bagian == 'penutup') {
            return Redirect('evaluasi/upload-laporan-akhir/penutup');
        }elseif ($bagian == "isi") {
            return Redirect('evaluasi/upload-laporan-akhir/isi');
        }else{
            return Redirect('evaluasi/upload-laporan-akhir');
        }

        return Redirect('evaluasi/upload-laporan-akhir');
    }


}
