<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\penggunaan as Penggunaan;
use Illuminate\Support\Facades\Redirect;

class PenggunaanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = DB::table('penggunaan')->get();
        return view('back/penggunaan/index', compact('data'));
    }

    public function edit($id)
    {
        $data = DB::table('penggunaan')->where('id', $id)->get();
        return view('back/penggunaan/update', compact('data'));
    }

    public function in()
    {
        return view('back/penggunaan/insert');
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data = Request::all();
        $this->validate($request, [
            'video' => 'mimes:mp4,mkv,avi',
        ]);
        dd();
        if (Request::hasFile('video'))
        {
            $dest = public_path('asset/backend/penggunaan/');
            $name = Request::file('video')->getClientOriginalName();
            Request::file('video')->move($dest, $name);
            $data['video'] = $name;
        }
        Penggunaan::where('id',$id)->update($data);
        Session::flash('message', 'Tutorial Penggunaan Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/penggunaan');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        $validator = Validator::make($request->all(), [
            'video' => 'mimetypes:video/avi,video/mpeg,video/quicktime'
        ]);

        if ($validator->fails()) {
            Session::flash('message', 'Extensi file tidak sesuai!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return back();
        }

        if (Request::hasFile('video'))
        {
            $dest = public_path('asset/backend/penggunaan/');
            $name = Request::file('video')->getClientOriginalName();
            $ucp = Request::file('video')->move($dest, $name);
            // dd($ucp);
        }
        $data = Request::file('video');
        $data = Request::all();
        $data['video'] = $name;

        Penggunaan::create($data);
        Session::flash('message', 'Tutorial Penggunaan Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/penggunaan');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Penggunaan::find($id)->delete();
        return redirect("admin/penggunaan");
    }
}
