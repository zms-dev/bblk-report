<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;
use File;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\juklak as Juklak;
use Illuminate\Support\Facades\Redirect;

class JuklakController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $juklak = DB::table('sub_bidang')
            ->join('juklak', 'sub_bidang.id', '=', 'juklak.id_bidang')
            ->select('juklak.*', 'sub_bidang.parameter as Bidang')
            ->get();
        return view('back/juklak/index', compact('juklak'));
    }

    public function edit($id)
    {
        $bidang = DB::table('sub_bidang')->get();
        $data = DB::table('sub_bidang')
            ->join('juklak', 'sub_bidang.id', '=', 'juklak.id_bidang')
            ->select('juklak.*', 'sub_bidang.parameter as Bidang', 'sub_bidang.id as Id')
            ->where('juklak.id',$id)
            ->get();
        return view('back/juklak/update', compact('data','bidang'));
    }

    public function in()
    {
        $bidang = DB::table('sub_bidang')->get();
        return view('back/juklak/insert', compact('bidang'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
       if (Request::hasFile('file'))
        {
            $dest = public_path('asset/backend/juklak/');
            $name = Request::file('file')->getClientOriginalName();
            Request::file('file')->move($dest, $name);
        }
        $data = Request::file('file');
        $data = Request::all();
        $data['file'] = $name;
        $data['created_by'] = Auth::user()->id;
        Juklak::where('id',$id)->update($data);
        Session::flash('message', 'Juklak Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/juklak');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        if (File::exists('asset/backend/juklak') === false) {
            File::makeDirectory('asset/backend/juklak', 0775, true);
            public_path('asset/backend/juklak/', 0775, true);
        }
        if (Request::hasFile('file'))
        {
            $dest = public_path('asset/backend/juklak/');
            $name = Request::file('file')->getClientOriginalName();
            Request::file('file')->move($dest, $name);
        }
        $data = Request::file('file');
        $data = Request::all();
        $data['file'] = $name;
        $data['created_by'] = Auth::user()->id;
        Juklak::create($data);
        Session::flash('message', 'Juklak Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/juklak');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Juklak::find($id)->delete();
        return redirect("admin/juklak");
    }
}
