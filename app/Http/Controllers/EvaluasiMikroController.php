<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use App\daftar as Daftar;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\register as Register;
use Excel;
use PDF;
use App\SertfikatInsert;
use App\TanggalEvaluasi;
class EvaluasiMikroController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function malariagrafik()
    {
        return view('evaluasi.malaria.grafik.rekap.index');
    }
    public function malariagrafikna(\Illuminate\Http\Request $request)
    {
        $tahun = date('Y');
        $siklus = $request->siklus;

        $badan = DB::table('tb_registrasi')
                ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->leftjoin('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                ->where('tb_registrasi.bidang','=', '10')
                ->where(function ($query) use ($siklus) {
                    $query->where('tb_registrasi.siklus','=','12')
                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                })
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->select('badan_usaha.badan_usaha', DB::raw('count(badan_usaha.badan_usaha) as jumlah'))
                ->groupBy('badan_usaha.id')
                ->get();
        // dd($badan);
        return view('evaluasi.malaria.grafik.rekap.grafik', compact('badan', 'tahun', 'siklus'));
    }

    public function malariagrafikkesimpulan()
    {
        return view('evaluasi.malaria.grafik.kesimpulan.index');
    }
    public function malariagrafikkesimpulanna(\Illuminate\Http\Request $request)
    {
        $tahun = $request->tahun;
        $siklus = $request->siklus;

        $data = DB::table('tb_evaluasi_malaria')
                ->select('kesimpulan', DB::raw('COUNT(kesimpulan) as jumlah'))
                ->where('siklus', $siklus)
                ->where('tahun', $tahun)
                ->groupBy('kesimpulan')
                ->get();
        // dd($badan);
        return view('evaluasi.malaria.grafik.kesimpulan.grafik', compact('data', 'tahun', 'siklus'));
    }

    public function malariapeserta()
    {
        return view('evaluasi.malaria.rekaphasilpeserta.index');
    }
    public function malariapesertana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        if($input['siklus'] == '1')
        {
            $data = DB::table('tb_registrasi')
                ->where('bidang','=','10')
                ->where('status', 3)
                ->where('status_data1', 2)
                ->where('pemeriksaan', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->get();
        }else{
            $data = DB::table('tb_registrasi')
                ->where('bidang','=','10')
                ->where('status', 3)
                ->where('status_data2', 2)
                ->where('pemeriksaan2', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->get();
        }
        foreach ($data as $key => $val) {
            $bta = DB::table('tb_malaria')
                    ->where('tb_malaria.id_registrasi', $val->id)
                    ->where('tb_malaria.siklus', $input['siklus'])
                    ->where(DB::raw('YEAR(tb_malaria.created_at)'), '=' , $input['tahun'])
                    ->get();
            $val->hasil = $bta;

            $total = DB::table('tb_evaluasi_malaria')
                        ->where('id_registrasi', $val->id)
                        ->where('siklus', $input['siklus'])
                        ->where('tahun', $input['tahun'])
                        ->groupBy('id_registrasi')
                        ->sum('total');
            $val->total = $total;
        }
        // dd($data);
        Excel::create('Rekap Hasil Seluruh Peserta Malaria', function($excel) use ($data, $input) {
            $excel->sheet('Data', function($sheet) use ($data, $input) {
                $sheet->loadView('evaluasi.malaria.rekaphasilpeserta.view', array('data'=>$data,'input'=>$input) );
            });
        })->download('xls');
        // return view('evaluasi.bta.rekaphasilpeserta.view', compact('data', 'input'));
    }

    public function btagrafik()
    {
        return view('evaluasi.bta.grafik.rekap.index');
    }
    public function btagrafikna(\Illuminate\Http\Request $request)
    {
        $tahun = date('Y');
        $siklus = $request->siklus;

        $badan = DB::table('tb_registrasi')
                ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->leftjoin('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                ->where('tb_registrasi.bidang','=', '4')
                ->where(function ($query) use ($siklus) {
                    $query->where('tb_registrasi.siklus','=','12')
                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                })
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->select('badan_usaha.badan_usaha', DB::raw('count(badan_usaha.badan_usaha) as jumlah'))
                ->groupBy('badan_usaha.id')
                ->get();
        // dd($badan);
        return view('evaluasi.bta.grafik.rekap.grafik', compact('badan', 'tahun', 'siklus'));
    }

    public function rekapInputPeserta(){
      return view('evaluasi.bta.rekapinputpeserta.index');
    }

    public function rekapInputPesertaView(\Illuminate\Http\Request $request){
      $input = $request->all();
      $datas = DB::table('perusahaan')
                ->leftjoin('tb_registrasi','tb_registrasi.perusahaan_id','perusahaan.id')
                ->where('tb_registrasi.bidang', '4')
                ->select('perusahaan.nama_lab', 'tb_registrasi.kode_lebpes','tb_registrasi.id')
                ->orderBy('tb_registrasi.id','ASC')
                ->get();
      foreach ($datas as $key => $value) {
          $tb = DB::table('tb_bta')
                  ->where('id_registrasi',$value->id)
                  ->limit('10')
                  ->get();

                  // foreach ($tb as $key => $val) {
                  //     $tba = $val->kode;
                  // //   $isitb = $val->kode;
                  // //   // $value->kole[$val->id] = $tb[$val->id];
                  //     $val->kole = $tba;
                  // }
                  // dd($tb);
          $value->tb = $tb;
      }
      // dd($datas);
      Excel::create('Rekap Input Peserta BTA', function($excel) use ($datas, $input) {
          $excel->sheet('BTA', function($sheet) use ($datas, $input) {
              $sheet->loadView('evaluasi.bta.rekapinputpeserta.view', array('datas'=>$datas,'input'=>$input) );
          });
      })->download('xls');

      // return view('evaluasi.bta.rekapinputpeserta.view', compact('input','datas'));
    }

    public function btapeserta()
    {
        return view('evaluasi.bta.rekaphasilpeserta.index');
    }
    public function btapesertana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        if($input['siklus'] == '1')
        {
            $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('bidang','=','4')
                ->where('status', 3)
                ->where('status_data1', 2)
                ->where('pemeriksaan', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('tb_registrasi.*','perusahaan.nama_lab')
                ->get();
        }else{
            $data = DB::table('tb_registrasi')
                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('bidang','=','4')
                ->where('status', 3)
                ->where('status_data2', 2)
                ->where('pemeriksaan2', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('tb_registrasi.*','perusahaan.nama_lab')
                ->get();
        }
        foreach ($data as $key => $val) {
            $bta = DB::table('tb_bta')
                    ->leftJoin('tb_evaluasi_bta', function($leftJoin) use ($val, $input)
                        {
                            $leftJoin->on('tb_evaluasi_bta.kode_sediaan', '=', 'tb_bta.kode')
                                ->on('tb_evaluasi_bta.id_registrasi', '=', DB::raw("'".$val->id."'") )
                                ->on('tb_evaluasi_bta.siklus', '=', DB::raw("'".$input['siklus']."'") );
                        })
                    ->where('tb_bta.id_registrasi', $val->id)
                    ->where('tb_bta.siklus', $input['siklus'])
                    ->where(DB::raw('YEAR(tb_bta.created_at)'), '=' , $input['tahun'])
                    ->get();
            // dd($bta);
            $val->hasil = $bta;

            $status = DB::table('tb_evaluasi_bta_status')
                        ->where('id_registrasi', $val->id)
                        ->where('siklus', $input['siklus'])
                        ->where('tahun', $input['tahun'])
                        ->get();
            $val->status = $status;

            $total = DB::table('tb_evaluasi_bta')
                        ->where('id_registrasi', $val->id)
                        ->where('siklus', $input['siklus'])
                        ->where('tahun', $input['tahun'])
                        ->groupBy('id_registrasi')
                        ->sum('nilai');
            $val->total = $total;
        }
        // dd($data);
        Excel::create('Rekap Hasil Seluruh Peserta BTA Siklus '. $input['siklus'] .' Tahun '.$input['tahun'] , function($excel) use ($data, $input) {
            $excel->sheet('BTA', function($sheet) use ($data, $input) {
                $sheet->loadView('evaluasi.bta.rekaphasilpeserta.view', array('data'=>$data,'input'=>$input) );
                //     foreach ($data as $key => $val) {
                // $sheet->fromArray(array(
                //         array('data1', 'data2')
                // ));
                //     }
            });
        })->download('xls');
        // return view('evaluasi.bta.rekaphasilpeserta.view', compact('data', 'input'));
    }

    public function tcpeserta()
    {
        return view('evaluasi.tc.rekaphasilpeserta.index');
    }
    public function tcpesertana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        if($input['siklus'] == '1')
        {
            $data = DB::table('tb_registrasi')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('bidang','=','5')
                ->where('status', 3)
                ->where('status_data1', 2)
                ->where('pemeriksaan', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('tb_registrasi.*','perusahaan.nama_lab')
                ->get();
        }else{
            $data = DB::table('tb_registrasi')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('bidang','=','5')
                ->where('status', 3)
                ->where('status_data2', 2)
                ->where('pemeriksaan2', 'done')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('tb_registrasi.*','perusahaan.nama_lab')
                ->get();
        }
        foreach ($data as $key => $val) {
            $bta = DB::table('tb_evaluasi_tc')
                    ->where('tb_evaluasi_tc.id_registrasi', $val->id)
                    ->where('tb_evaluasi_tc.siklus', $input['siklus'])
                    ->where('tb_evaluasi_tc.tahun', $input['tahun'])
                    ->get();
            $val->hasil = $bta;

            $tc = DB::table('tb_telurcacing')
                    ->where('tb_telurcacing.id_registrasi', $val->id)
                    ->where('tb_telurcacing.siklus', $input['siklus'])
                    ->where(DB::raw('YEAR(tb_telurcacing.created_at)'), '=' , $input['tahun'])
                    ->get();
            $val->input = $tc;

            $total = DB::table('tb_evaluasi_tc')
                        ->where('id_registrasi', $val->id)
                        ->where('siklus', $input['siklus'])
                        ->where('tahun', $input['tahun'])
                        ->groupBy('id_registrasi')
                        ->sum('nilai');
            $val->total = $total;
        }
        $rujukan = DB::table('tb_rujukan_tc')->where('siklus', $input['siklus'])->where('tahun', $input['tahun'])->get();
        // dd($data[0]);
        // return view('evaluasi.tc.rekaphasilpeserta.view', compact('data', 'input','rujukan'));
        Excel::create('Rekap Hasil Seluruh Peserta TC Siklus '.$input['siklus'].' Tahun '.$input['tahun'], function($excel) use ($data, $input, $rujukan) {
            $excel->sheet('TC', function($sheet) use ($data, $input, $rujukan) {
                $sheet->loadView('evaluasi.tc.rekaphasilpeserta.view', array('data'=>$data,'rujukan'=>$rujukan,'input'=>$input) );
            });
        })->download('xls');
    }

    public function grafikPesertaProvinsi(){
      return view('evaluasi.bta.grafik.provinsi-peserta.index');
    }

    public function grafikPesertaProvinsiView(\Illuminate\Http\Request $request){
      $input = $request->all();
      $tahun = date('Y');
      // dd($input);
      $provinsi = DB::table('provinces')->get();
      // return($provinsi);
      foreach ($provinsi as $key => $value) {
        $peserta = DB::table('perusahaan')
            ->join('tb_registrasi','tb_registrasi.perusahaan_id','perusahaan.id')
            ->where('perusahaan.provinsi',$value->id)
            ->where('tb_registrasi.bidang', '4')
            ->where(function($query) use ($input) {
                $query->where('siklus', '=', $input['siklus'])
                    ->orwhere('siklus', '=', 12);
             })
            ->count();
          $value->peserta = $peserta;
      }
      // dd($provinsi);
      // return($provinsi);

      return view('evaluasi.bta.grafik.provinsi-peserta.grafik', compact('provinsi','tahun','input'));
    }

    public function grafikkesimpulanpeserta()
    {
        return view('evaluasi.bta.grafik.kesimpulan.index');
    }
    public function grafikkesimpulanpesertana(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $tahun = date('Y');
        $sql = "SELECT
                    COUNT(total) jumlah, status, total
                FROM
                    (
                        SELECT
                            `tb_evaluasi_bta_status`.`status`,
                            SUM(nilai) AS total

                        FROM
                            `tb_evaluasi_bta`
                        INNER JOIN `tb_evaluasi_bta_status` ON `tb_evaluasi_bta_status`.`id_registrasi` = `tb_evaluasi_bta`.`id_registrasi`
                        WHERE
                            `tb_evaluasi_bta`.`tahun` = '".$tahun."'
                        AND `tb_evaluasi_bta`.`siklus` = '".$input['siklus']."'
                        AND `tb_evaluasi_bta_status`.`tahun` = '".$tahun."'
                        AND `tb_evaluasi_bta_status`.`siklus` = '".$input['siklus']."'
                        GROUP BY
                            `tb_evaluasi_bta`.`id_registrasi`,
                            `tb_evaluasi_bta`.`tahun`,
                            `tb_evaluasi_bta`.`siklus`
                    )
                AS A
                GROUP BY
                    status,total
                    ";

        $data = DB::table(DB::raw("($sql) zone"))
                ->select('*')
                ->orderBy('total', 'desc')
                ->get();
        $arrList = [
            100 => 0,
            95 => 0,
            90 => 0,
            85 => 0,
            80 => 0,
            75 => 0,
            70 => 0
        ];
        $arrList2 = [
            100 => 0,
            95 => 0,
            90 => 0,
            85 => 0,
            80 => 0,
            75 => 0,
            70 => 0
        ];
        foreach ($data as $key => $val) {
            foreach ($arrList as $key1 => $val1) {
                if($val->total == $key1){
                    if($val->status == 'Lulus'){
                        $arrList[$key1] = $val->jumlah;
                    }else{
                        $arrList2[$key1] = $val->jumlah;
                    }
                }
            }
        }

        return view('evaluasi.bta.grafik.kesimpulan.grafik', compact('data', 'input', 'tahun','arrList','arrList2'));
    }

    public function grafikjawabanpeserta()
    {
        return view('evaluasi.bta.grafik.jawabanpeserta.index');
    }
    public function grafikjawabanpesertana(\Illuminate\Http\Request $request)
    {
        DB::statement(DB::raw('set @row_num=0')); 
        DB::statement(DB::raw('set @cst=null')); 
        $input = $request->all();
        $arrNilai = [ 
                     0  => 0,
                     1  => 0,
                     2  => 0,
                     3  => 0,
                     4  => 0,
                     5  => 0,
                     6  => 0,
                     7  => 0,
                     8  => 0,
                     9  => 0,
                     10  => 0
        ];
        $arrData = [
             'Benar' => $arrNilai, 
             'KH' => $arrNilai, 
             'PPR' => $arrNilai, 
             'NPR' => $arrNilai, 
             'PPT' => $arrNilai, 
             'NPT' => $arrNilai
        ];
        // dd($input);
        $tahun = date('Y');
        $kode = (int)$input['kode_sediaan'];
        $data = DB::table(DB::raw('tb_evaluasi_bta A'))
                ->join('tb_registrasi', 'tb_registrasi.id','=', 'A.id_registrasi')
                ->select(DB::raw('
                                    *,
                                    (CASE 
                                        WHEN @cst = A.id_registrasi THEN @row_num:=@row_num+1
                                        ELSE @row_num:=1
                                    END) as row_num,
                                @cst:=A.id_registrasi as cst'))
                ->where('A.siklus', $input['siklus'])
                ->get();
        // dd($data);
        // $data = DB::table('tb_bta')
        //         ->join('tb_evaluasi_bta', 'tb_evaluasi_bta.key(array)ode_sediaan', '=', 'tb_bta.kode')
        //         ->where(DB::raw('YEAR(tb_bta.created_at)'), '=' , $input['tahun'])
        //         ->where(DB::raw('substr(kode, 10)'), '=' , $kode)
        //         ->select('tb_bta.hasil', 'tb_evaluasi_bta.keterangan' , DB::raw('SUBSTR(kode, 10) AS kode_sediaan, COUNT(substring(kode, 10)) as jumlah'))
        //         ->groupBy('tb_bta.hasil')
        //         ->get();
        // dd($data);
        foreach ($data as $key => $value) {
            if(isset($arrData[$value->keterangan][$value->row_num])){
                $arrData[$value->keterangan][$value->row_num] = $arrData[$value->keterangan][$value->row_num]+=1;
            }else{
                $arrData[$value->keterangan][$value->row_num] = 0;
            }
        }
        // dd($arrData);
        return view('evaluasi.bta.grafik.jawabanpeserta.grafik', compact('data', 'input', 'arrData'));
    }

    public function tcgrafik()
    {
        return view('evaluasi.tc.grafik.rekap.index');
    }
    public function tcgrafikna(\Illuminate\Http\Request $request)
    {
        $tahun = date('Y');
        $siklus = $request->siklus;

        $badan = DB::table('tb_registrasi')
                ->leftjoin('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->leftjoin('badan_usaha','badan_usaha.id','=','perusahaan.pemerintah')
                ->where('tb_registrasi.bidang','=', '5')
                ->where(function ($query) use ($siklus) {
                    $query->where('tb_registrasi.siklus','=','12')
                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                })
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                ->select('badan_usaha.badan_usaha', DB::raw('count(badan_usaha.badan_usaha) as jumlah'))
                ->groupBy('badan_usaha.id')
                ->get();
        // dd($badan);
        return view('evaluasi.tc.grafik.rekap.grafik', compact('badan', 'tahun', 'siklus'));
    }

    public function perusahaanmalaria(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','10')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="penilaian/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.malaria.penilaian.perusahaan');
    }

    public function dataperusahaanmalaria($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '10')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();

        return view('evaluasi.malaria.penilaian.dataperusahaan', compact('data'));
    }

    public function perusahaanbta(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.id as idregistrasi,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where(function ($query) use ($siklus) {
                    $query->where('tb_registrasi.siklus','=','12')
                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                })
                ->where('tb_registrasi.bidang','=','4')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->orderBy('tb_registrasi.id', 'asc')
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_evaluasi_bta_status')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    return "".'<a href="penilaian/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Proses
                                    </button>
                                </a>';
            ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_evaluasi_bta_status')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia'])
            ->make(true);
        }
        // $evaluasi = TanggalEvaluasi::where('form','=','Mikrobiologi BTA')->first(); 
        return view('evaluasi.bta.penilaian.perusahaan');
    }

    public function inserttanggal(\Illuminate\Http\Request $request)
    {
        $data = $request->all();
        $evaluasi = DB::table('tanggal_evaluasi')->where('form', $request->form)->where('siklus', $request->siklus)->where('tahun', $request->tahun)->get();
        // dd($evaluasi);
        if (!count($evaluasi)) {
            $Save = new TanggalEvaluasi;
            $Save->tanggal = $request->tanggal;
            $Save->form = $request->form;
            $Save->siklus = $request->siklus;
            $Save->tahun = $request->tahun;
            $Save->save();
        }else{
            $evaluasi = TanggalEvaluasi::where("form", $request->form)->where('siklus', $request->siklus)->where('tahun', $request->tahun)
                        ->update(["tanggal" => $request->tanggal]);
        }
        // dd($data);
        return back();
    }

    public function perusahaanbtacetak(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.id as idregistrasi,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','4')
                ->where(function ($query) use ($siklus) {
                    $query->where('tb_registrasi.siklus','=','12')
                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                })
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_evaluasi_bta_status')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    return "".'<a href="cetakan/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Proses
                                    </button>
                                </a>';
            ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_evaluasi_bta_status')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia'])
            ->make(true);
        }

        return view('evaluasi.bta.penilaian.perusahaancetak');
    }

    public function perusahaantccetak(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes, tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','5')
                ->where(function ($query) use ($siklus) {
                    $query->where('tb_registrasi.siklus','=','12')
                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                })
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_evaluasi_bta_status')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    return "".'<a href="cetakan/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Proses
                                    </button>
                                </a>';
            ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_evaluasi_bta_status')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia'])
            ->make(true);
        }
        return view('evaluasi.tc.penilaian.perusahaancetak');
    }


    public function perusahaanbtasertifikat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.id as idregistrasi,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=', Auth::user()->penyelenggara)
                ->where(function ($query) use ($siklus) {
                    $query->where('tb_registrasi.siklus','=','12')
                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                })
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->get();
            return Datatables::of($datas)
           ->addColumn('action', function($data) use ($siklus,$tahun){
                    if (Auth::user()->penyelenggara == 5) {
                    $evaluasi = DB::table('tb_evaluasi_tc')
                        ->where('id_registrasi', $data->idregistrasi)
                        ->where('siklus', $siklus)
                        ->where('tahun',$tahun)
                        ->orderBy('id', 'desc')->first();
                    }else{
                        $evaluasi = DB::table('tb_evaluasi_bta_status')
                        ->where('id_registrasi', $data->idregistrasi)
                        ->where('siklus', $siklus)
                        ->where('tahun',$tahun)
                        ->orderBy('id', 'desc')->first();
                    }
                    if(count($evaluasi)){
                        if ($evaluasi->siklus == '1') {
                            return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Proses
                                            </button>
                                        </a>';
                        }elseif($evaluasi->siklus == '2'){
                            return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Proses
                                            </button>
                                        </a>';
                        }
                    }else{
                        return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Proses
                                        </button>
                                    </a>';
                    }
            ;})
            ->make(true);
        }
        $bidang = Auth::user()->penyelenggara;
        $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=', Auth::user()->name)->first();
        return view('evaluasi.bta.penilaian.perusahaansertifikat', compact('bidang','sertifikat'));
    }

    public function evaluasiInsertSertifikatbta(\Illuminate\Http\Request $request)
    {
        $data = $request->all();
        $sertifikat = DB::table('tb_sertifikat')->where('siklus',$data['siklus'])->where('tahun',$data['tahun'])->where('parameter', $data['parameter'])->get();

        if (count($sertifikat)) {
            $sertifikat = SertfikatInsert::where("tb_sertifikat.parameter", $request->parameter)
                                ->where('siklus', $data['siklus'])
                                ->where('tahun', $data['tahun'])
                                ->update([
                                    "tanggal_sertifikat" => $request->tanggal_sertifikat,
                                    "nomor_sertifikat" => $request->nomor_sertifikat
                                ]);
        }else{
            $b = new SertfikatInsert;
            $b->tanggal_sertifikat = $request->tanggal_sertifikat;
            $b->nomor_sertifikat =$request->nomor_sertifikat;
            $b->parameter = $request->parameter;
            $b->siklus = $request->siklus;
            $b->tahun = $request->tahun;
            $b->save();
        }
        // dd($data);
        return back();
    }

    public function dataperusahaanbta(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '4')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
        return view('evaluasi.bta.penilaian.dataperusahaan', compact('data','siklus','tahun'));
    }

    public function dataperusahaanbtacetak(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('sub_bidang.id', '=', '4')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
        // dd($data);
        return view('evaluasi.bta.penilaian.dataperusahaancetak', compact('data','siklus','tahun'));
    }

    public function dataperusahaantccetak(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('sub_bidang.id', '=', '5')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
        // dd($data);
        return view('evaluasi.tc.penilaian.dataperusahaancetak', compact('data','siklus','tahun'));
    }

     public function cetak(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $tanggal = \Carbon\Carbon::now();
        $tahun = date('Y');
        $data = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();

        $datas = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->get();
        // dd($datas);
        $kode = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();
        $rujuk = DB::table('tb_rujukan_bta')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun)
                ->get();
        $register = Register::find($id);
        $evaluasi = DB::table('tb_evaluasi_bta')->where('id_registrasi',$id)->get();
        // dd($evaluasi);
        $perusahaan = $register->perusahaan->nama_lab;
        $status = DB::table('tb_evaluasi_bta_status')
                ->where('siklus', $siklus)
                ->where('tahun', $tahun)
                ->where('id_registrasi', $id)
                ->first();
        $skor = DB::table('tb_evaluasi_bta')
                ->where('id_registrasi',$id)
                ->groupBy('id_registrasi')
                ->sum('nilai');
        // dd($skor);
        if ($rujuk != NULL) {
            if (count($evaluasi)>0) {
            $pdf = PDF::loadView('evaluasi.bta.penilaian.evaluasi.cetak',compact('data','kode', 'perusahaan', 'datas', 'type','rujuk','evaluasi', 'status','siklus','tahun','tanggal','skor'));
            $pdf->setPaper('a4','potrait');
            return $pdf->stream('EvaluasiBta'.rand(2,32012).'.pdf');
            }else{
                 Session::flash('message', 'BTA belum dievaluasi!');
                Session::flash('alert-class', 'alert-danger');
                return back();
            }

        }else{
            return back();
        }
    }

    public function dataperusahaanbtasertifikat(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->wherein('sub_bidang.id', [4,5])
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
        return view('evaluasi.bta.penilaian.dataperusahaansertifikat', compact('data','tahun','siklus'));
    }

    public function perusahaantc(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes, tb_registrasi.id as idregistrasi'))
                ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('tb_registrasi.bidang','=','5')
                ->where(function ($query) use ($siklus) {
                    $query->where('tb_registrasi.siklus','=','12')
                          ->orwhere('tb_registrasi.siklus','=',$siklus);
                })
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_evaluasi_tc')
                                ->where('siklus', $siklus)
                                ->where('id_registrasi', $data->idregistrasi)
                                ->orderBy('id', 'desc')
                                ->first();
                    if($evaluasi != NULL){
                        return "".'<a href="penilaian/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            Proses
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="penilaian/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Proses
                                        </button>
                                    </a>';
                    }
            ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_evaluasi_tc')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia'])
            ->make(true);
        }
        // $evaluasi = TanggalEvaluasi::where('form','=','Mikrobiologi TC')->first(); 
        return view('evaluasi.tc.penilaian.perusahaan');
    }

    public function dataperusahaantc(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang', DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('sub_bidang.id', '=', '5')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1','=', '2')
                            ->orwhere('status_data2','=', '2');
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
        return view('evaluasi.tc.penilaian.dataperusahaan', compact('data','siklus','tahun'));
    }
}
