<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\daftar as Daftar;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use App\SertfikatInsert;
use App\TanggalEvaluasi;
class EvaluasiimunController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function skoring()
    {
        return view('evaluasi.urinalisa.grafikskoring.index');
    }
    public function grafikskoring(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $tahun = $input['tahun'];
        $type = $input['type'];
        $siklus = $input['siklus'];

        $sql =" SELECT count(*) as nilai1, '<= 1,00' as nilainya, 'a' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus."' AND type = '".$type."' AND tahun = '".$tahun."' AND skoring <= 1
                UNION
                SELECT count(*) as nilai1, '> 1,00 - 2,00' as nilainya, 'b' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus."' AND type = '".$type."' AND tahun = '".$tahun."' AND skoring > 1 and skoring <= 2
                UNION
                SELECT count(*) as nilai1, '> 2,00 - 3,00' as nilainya, 'c' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus."' AND type = '".$type."' AND tahun = '".$tahun."' AND skoring > 2 AND skoring <= 3
                UNION
                SELECT count(*) as nilai1, '> 3,00' as nilainya, 'd' as sort from tb_skoring_urinalisa
                where siklus = '".$siklus."' AND type = '".$type."' AND tahun = '".$tahun."' AND skoring > 3";
        $data = DB::table(DB::raw("($sql) zone"))
                ->orderBy('sort', 'desc')
                ->get();
        return view('evaluasi.urinalisa.grafikskoring.grafik', compact('data','tahun','type','siklus'));
    }

    public function perusahaan(\Illuminate\Http\Request $request)
    {
        // ->where('tb_registrasi.status_data1','=','2')
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.bidang','tb_registrasi.kode_lebpes as kode_lebpesan', 'tb_registrasi.id as idregistrasi')
                        ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                        ->where(function($query) use ($siklus){
                            $query->where('tb_registrasi.siklus', '=', '12')
                                ->orwhere('tb_registrasi.siklus', '=', $siklus);
                        })
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                        ->groupBy('perusahaan.id')
                        ->get();
            return Datatables::of($datas)
                ->addColumn('action', function($data) use ($siklus, $tahun){
                    if(Auth::user()->penyelenggara != '7'){
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if($evaluasi != NULL){
                            return "".'<a href="imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';
                        }else{
                            return "".'<a href="imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';
                        }
                    }else{
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->orderBy('id', 'desc')
                                    ->count('id_registrasi');
                        if ($evaluasi == '2') {
                            return "".'<a href="imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';
                        }else{
                            return "".'<a href="imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';
                        }
                    }
                ;})
                ->addColumn('posisi', function($data) use ($siklus){
                        $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                    ->where('siklus', $siklus)
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('type', '=', 'rpr-syphilis')
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if($evaluasi != NULL){
                            return "1";
                        }else{
                            return "0";
                        }
                ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_kesimpulan_evaluasi')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where(DB::raw('YEAR(created_at)'), '=', $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where(function($query)
                                    {
                                        if(Auth::user()->penyelenggara == '7'){
                                            $query->where('type', '=', 'Syphilis');
                                        }
                                    })
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_data2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_kesimpulan_evaluasi')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where(DB::raw('YEAR(created_at)'), '=', $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where(function($query)
                                    {
                                        if(Auth::user()->penyelenggara == '7'){
                                            $query->where('type', '=', 'rpr-syphilis');
                                        }
                                    })
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr2 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('print', function($data) use ($siklus, $tahun){
                    return "".'<a href="cetak-imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib', 'print' =>  'print'])
            ->make(true);
        }
        // $evaluasi = TanggalEvaluasi::where('form','=','Imunologi')->first(); 
        return view('evaluasi.imunologi.perusahaan');
    }
    public function inserttanggal(\Illuminate\Http\Request $request)
    {
        $data = $request->all();
        $evaluasi = DB::table('tanggal_evaluasi')
                    ->where('form', $request->form)
                    ->where('siklus', $request->siklus)
                    ->where('tahun', $request->tahun)
                    ->get();
        // dd($evaluasi);
        if (!count($evaluasi)) {
            $Save = new TanggalEvaluasi;
            $Save->tanggal = $request->tanggal;
            $Save->form = $request->form;
            $Save->siklus = $request->siklus;
            $Save->tahun = $request->tahun;
            $Save->save();
        }else{
            $evaluasi = TanggalEvaluasi::where("form", $request->form)
                        ->where("siklus", $request->siklus)
                        ->where("tahun", $request->tahun)
                        ->update([
                            "tanggal" => $request->tanggal
                        ]);
        }
        // dd($data);
        return back();
    }
    public function dataperusahaan(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        // dd($siklus);
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('status', 3)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->where(function($query){
                    if (Auth::user()->badan_usaha == '9') {
                        $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                    }
                })
                ->where(function($query) use ($siklus){
                        $query->where('tb_registrasi.siklus', '=', '12')
                            ->orwhere('tb_registrasi.siklus', $siklus);
                })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('rpr1', 'done')
                                ->orwhere('rpr2', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
        // dd($data);
        // dd($data);
        return view('evaluasi.imunologi.dataperusahaan', compact('data','siklus','tahun'));
    }

    public function perusahaancetak(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.bidang','tb_registrasi.kode_lebpes as kode_lebpesan','tb_registrasi.id as idregistrasi')
                        ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                        ->where(function ($query) use ($siklus) {
                            $query->where('tb_registrasi.siklus','=','12')
                                  ->orwhere('tb_registrasi.siklus','=',$siklus);
                        })
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                        ->groupBy('perusahaan.id')
                        ->get();
           return Datatables::of($datas)
                ->addColumn('action', function($data) use ($tahun,$siklus) {
                    $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($evaluasi)){
                        if ($evaluasi->siklus == '1') {
                            return "".'<a href="cetak-imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';
                        }elseif($evaluasi->siklus == '2'){
                            return "".'<a href="cetak-imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';
                        }else{
                            return "".'<a href="cetak-imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';
                        }
                    }else{
                        return "".'<a href="cetak-imunologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Evaluasi
                                        </button>
                                    </a>';
                    }
                ;})
            ->make(true);
        }
        return view('evaluasi.imunologi.cetak.perusahaan');
    }

    public function perusahaancetaksertifikat(\Illuminate\Http\Request $request)
    {
        // ->where('tb_registrasi.status_data1','=','2')
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.bidang', 'tb_registrasi.id as idregistrasi','tb_registrasi.kode_lebpes as kode_lebpesan')
                        ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                        ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                        ->where(function ($query) use ($siklus) {
                            $query->where('tb_registrasi.siklus','=','12')
                                  ->orwhere('tb_registrasi.siklus','=',$siklus);
                        })
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                        ->groupBy('perusahaan.id')
                        ->get();
            
            return Datatables::of($datas)
               ->addColumn('action', function($data) use ($tahun,$siklus) {
                    $evaluasi = DB::table('tb_kesimpulan_evaluasi')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    if(count($evaluasi)){
                        if ($evaluasi->siklus == '1') {
                            return "".'<a href="cetak-sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';
                        }elseif($evaluasi->siklus == '2'){
                            return "".'<a href="cetak-sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';
                        }else{
                            return "".'<a href="cetak-sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';
                        }
                    }else{
                        return "".'<a href="cetak-sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Evaluasi
                                        </button>
                                    </a>';
                    }
                ;})
            ->make(true);
        }
        $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=','Imunologi')->first();
        // dd($sertifikat);
        return view('evaluasi.imunologi.cetak.perusahaansertifikat',compact('sertifikat'));
    }

    public function evaluasiInsertSertifikat(\Illuminate\Http\Request $request)
    {
        $data = $request->all();
        $sertifikat = DB::table('tb_sertifikat')->where('siklus',$data['siklus'])->where('tahun',$data['tahun'])->where('parameter','=', $data['parameter'])->get();

        if (count($sertifikat)) {
            $sertifikat = SertfikatInsert::where("tb_sertifikat.parameter", $request->parameter)
                                ->where('siklus', $data['siklus'])
                                ->where('tahun', $data['tahun'])
                                ->update([
                                    "tanggal_sertifikat" => $request->tanggal_sertifikat,
                                    "nomor_sertifikat" => $request->nomor_sertifikat
                                ]);
        }else{
            $b = new SertfikatInsert;
            $b->tanggal_sertifikat = $request->tanggal_sertifikat;
            $b->nomor_sertifikat =$request->nomor_sertifikat;
            $b->parameter = $request->parameter;
            $b->siklus = $request->siklus;
            $b->tahun = $request->tahun;
            $b->save();
        }
        // dd($data);
        return back();
    }


    public function dataperusahaancetak(\Illuminate\Http\Request $request,$id)
    {
         $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('status', 3)
                ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                // ->where(function($query){
                //     if (Auth::user()->badan_usaha == '9') {
                //         $query->where('perusahaan.pemerintah',Auth::user()->badan_usaha);
                //     }
                // })
                ->where(function($query) use ($siklus){
                        $query->where('tb_registrasi.siklus', '=', '12')
                            ->orwhere('tb_registrasi.siklus', $siklus);
                })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('rpr1', 'done')
                                ->orwhere('rpr2', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
        // dd($data);

        return view('evaluasi.imunologi.cetak.dataperusahaan', compact('data','siklus','tahun'));
    }
     public function dataperusahaancetaksertifikat(\Illuminate\Http\Request $request,$id)
    {
         $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang','perusahaan.pemerintah',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where(function($query)
                    {
                        $query->where('sub_bidang.id_bidang', '=', '6')
                                ->orwhere('sub_bidang.id_bidang', '=', '7')
                                ->orwhere('sub_bidang.id_bidang', '=', '8')
                                ->orwhere('sub_bidang.id_bidang', '=', '9');
                    })
                ->where('status', 3)
                ->where(function($query) use ($siklus){
                        $query->where('tb_registrasi.siklus', '=', '12')
                            ->orwhere('tb_registrasi.siklus', $siklus);
                })
                ->where('tb_registrasi.bidang','=',Auth::user()->penyelenggara)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('rpr1', 'done')
                                ->orwhere('rpr2', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
        // dd($data);
        return view('evaluasi.imunologi.cetak.dataperusahaansertifikat', compact('data','siklus','tahun'));
    }
    
    public function urinalisasertifikat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.kode_lebpes','tb_registrasi.id as idregistrasi')
                            ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                            ->where('tb_registrasi.bidang','=','3')
                            ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                            ->where(function($query){
                              if(Auth::user()->role != '5'){
                                $query->where('tb.registrasi.created_by',Auth::user()->id);
                              }
                            })
                            ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->get();
                    if(count($evaluasi) == '1' || count($evaluasi) == '2'){
                        return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Proses
                                            </button>
                                        </a>';
                    }elseif (count($evaluasi) == '3' || count($evaluasi) == '4') {
                        return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Proses
                                            </button>
                                        </a>';
                    }
                       return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Proses
                                            </button>
                                        </a>';
                            ;})
            ->addColumn('A', function($data)use($tahun,$siklus){
                        $evaluasi = DB::table('tb_skoring_urinalisa')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('tahun',$tahun)
                                    ->where('type', '=', 'a')
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if(count($evaluasi)){
                            if ($evaluasi->siklus == '1') {
                                return "".'Selesai';
                            }else{
                                return "".'Selesai';
                            }
                        }else{
                            return "".'-';
                        }
                ;})
            ->addColumn('B', function($data)use($tahun,$siklus){
                        $evaluasi = DB::table('tb_skoring_urinalisa')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('tahun',$tahun)
                                    ->where('type', '=', 'b')
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if(count($evaluasi)){
                            if ($evaluasi->siklus == '1') {
                                return "".'Selesai';
                            }else{
                                return "".'Selesai';
                            }
                        }else{
                            return "".'-';
                        }
                ;})
            ->make(true);
        }
         $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=','Urinalisa')->first();
        return view('evaluasi.urinalisa.perusahaansertifikat',compact('sertifikat'));
    }

    public function dataurinalisasertifikat(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang',db::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done')
                            ->orwhere('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->get();
                // dd($data);
        return view('evaluasi.urinalisa.dataperusahaansertifikat', compact('data','siklus','tahun'));
    }

    public function urinalisa(\Illuminate\Http\Request $request)
    {
      // return Auth::user();
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');

            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.kode_lebpes','tb_registrasi.id as idregistrasi')
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->where('tb_registrasi.bidang','=','3')
                ->where('tb_registrasi.status','>=','2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '=', 12)
                            ->orwhere('tb_registrasi.siklus', '=', $siklus);
                    })
                ->where(function($query){
                  if(Auth::user()->role != '5'){
                    $query->where('tb_registrasi.created_by',Auth::user()->id);
                  }
                })
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                                ->get();
                    return "".'<a href="urinalisa/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Proses
                                        </button>
                                    </a>';
                ;})

            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'a')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'b')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
        // $evaluasi = TanggalEvaluasi::where('form','=','Urinalisa')->first(); 
        return view('evaluasi.urinalisa.perusahaan');
    }

    public function dataurinalisa(\Illuminate\Http\Request $request,$id)
    {

        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done')
                            ->orwhere('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->get();
                // dd($data);
        return view('evaluasi.urinalisa.dataperusahaan', compact('data','siklus','tahun'));
    }

    public function parameter(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');

            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.kode_lebpes','tb_registrasi.id as idregistrasi')
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->where('tb_registrasi.bidang','=','3')
                ->where('tb_registrasi.status','>=','2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '=', 12)
                            ->orwhere('tb_registrasi.siklus', '=', $siklus);
                    })
                ->where(function($query){
                  if(Auth::user()->role != '5'){
                    $query->where('tb.registrasi.created_by',Auth::user()->id);
                  }
                })
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                                ->get();
                    if(count($evaluasi) == '2'){
                        return "".'<a href="parameter-urinalisa/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            View
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="parameter-urinalisa/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            View
                                        </button>
                                    </a>';
                    }
                    ;})
            
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'a')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'b')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
        return view('evaluasi.urinalisa.grafikparameter.perusahaan');
    }

    public function dataparameter(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('rpr1', 'done')
                                ->orwhere('rpr2', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
        
        // dd($data);
        return view('evaluasi.urinalisa.grafikparameter.dataperusahaan', compact('data', 'siklus', 'tahun'));
    }

    public function reagen(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');

            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'),'perusahaan.*','tb_registrasi.kode_lebpes','tb_registrasi.id as idregistrasi')
                ->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')
                ->where('tb_registrasi.bidang','=','3')
                ->where('tb_registrasi.status','>=','2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '=', 12)
                            ->orwhere('tb_registrasi.siklus', '=', $siklus);
                    })
                ->where(function($query){
                  if(Auth::user()->role != '5'){
                    $query->where('tb.registrasi.created_by',Auth::user()->id);
                  }
                })
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                                ->get();
                    if(count($evaluasi) == '2'){
                        return "".'<a href="reagen-urinalisa/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-info btn-xs">
                                            View
                                        </button>
                                    </a>';
                    }else{
                        return "".'<a href="reagen-urinalisa/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            View
                                        </button>
                                    </a>';
                    }
                    ;})
            
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'a')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = DB::table('tb_skoring_urinalisa')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('type', '=', 'b')
                                ->where('siklus', $siklus)
                                ->orderBy('id', 'desc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
        return view('evaluasi.urinalisa.grafikreagen.perusahaan');
    }

    public function datareagen(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->orwhere('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('rpr1', 'done')
                                ->orwhere('rpr2', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
        // dd($data);
        return view('evaluasi.urinalisa.grafikreagen.dataperusahaan', compact('data', 'siklus', 'tahun'));
    }

    public function reagenkehamilan(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','tb_registrasi.perusahaan_id','=','perusahaan.id')->where('tb_registrasi.bidang','=','3');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="reagen-kehamilan-urinalisa/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        View
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.urinalisa.grafikreagenkehamilan.perusahaan');
    }

    public function datareagenkehamilan($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->orwhere('pemeriksaan2', 'done')
                                ->orwhere('pemeriksaan', 'done')
                                ->orwhere('rpr1', 'done')
                                ->orwhere('rpr2', 'done')
                                ->orwhere('status_data1', 2)
                                ->orwhere('status_data2', 2);
                    })
                ->get();
        // dd($data);
        return view('evaluasi.urinalisa.grafikreagenkehamilan.dataperusahaan', compact('data'));
    }
}
