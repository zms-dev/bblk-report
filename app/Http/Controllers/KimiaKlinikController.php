<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;
use Carbon\Carbon;

use App\Parameter;
use App\MetodePemeriksaan;
use App\HpHeader;
use App\ZScore;
use App\ZScoreAlat;
use App\ZScoreMetode;
use App\HpDetail;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\View;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\register as Register;
use App\Instrumen;
use App\TanggalEvaluasi;

use Redirect;
use Validator;
use Session;

class KimiaKlinikController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {
        $data = Parameter::where('kategori', 'kimia klinik')->get();
        // dd($data);
        $type = $request->get('x');
        $siklus = $request->get('y');

        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;
        
        return view('hasil_pemeriksaan/kimia-klinik', compact('data', 'perusahaan','kode', 'type', 'siklus','date','instrumen'));
    }

    public function view(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $tanggal = date('d-m-Y');
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('tb_instrumen', 'hp_details.alat', '=', 'tb_instrumen.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->select('parameter.*', 'tb_instrumen.instrumen', 'hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.instrument_lain', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan')
            ->where('parameter.kategori', 'kimia klinik')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
        $datas = HpHeader::where('id_registrasi', $id)
                ->where('type', $type)
                ->where('siklus', $siklus)
                ->first();

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        
        // return view('cetak_hasil/kimia-klinik', compact('data', 'perusahaan', 'datas', 'type'));
        $pdf = PDF::loadview('cetak_hasil/kimia-klinik', compact('data', 'perusahaan','datas', 'type','siklus','date','register','tanggal'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Kimia-Klinik.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;
        $type = $request->get('type');
        $type = $input['type'];

        $validasi = HpHeader::where(['id_registrasi'=>$id , 'siklus'=>$siklus , 'type'=>$type])->get();

        $rules = array(
            'kode_peserta' => 'required',
            'tanggal_penerimaan' => 'required',
            'tanggal_pemeriksaan' => 'required'
        );

        $messages = array(
            'kode_peserta.required' => 'Kode Peserta Wajib diisi',
            'tanggal_penerimaan.required' => 'Tanggal Penerimaan wajib diisi',
            'tanggal_pemeriksaan.required' => 'Tanggal Pemeriksaan wajib diisi'
        );
        if (count($validasi)>0) {
            Session::flash('message', 'Hasil Kimia Klinik Sudah Ada!'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
        }else{
        $saveHeader = new HpHeader;
        $saveHeader->kode_lab = $input['kode_peserta'];
        $saveHeader->kode_bahan = $input['kode_bahan'];
        $saveHeader->kode_peserta = $perusahaanID;
        $saveHeader->tgl_pemeriksaan = date('Y-m-d', strtotime($input['tanggal_pemeriksaan']));
        $saveHeader->tgl_penerimaan = date('Y-m-d', strtotime($input['tanggal_penerimaan']));
        $saveHeader->kualitas_bahan = $input['kualitas'];
        $saveHeader->catatan = $input['catatan'];
        $saveHeader->kategori = 'kimia klinik';
        $saveHeader->id_registrasi = $id;
        $saveHeader->penanggung_jawab = $input['penanggung_jawab'];
        $saveHeader->type = $input['type'];
        $saveHeader->siklus = $siklus;
        $saveHeader->save();

        $saveHeaderID = $saveHeader->id;

        $i = 0;
        foreach ($input['parameter_id'] as $parameter_id) {
            $saveDetail = new HpDetail;
            $saveDetail->parameter_id = $input['parameter_id'][$i];
            $saveDetail->hp_header_id = $saveHeaderID;
            $saveDetail->alat = $input['alat'][$i];
            $saveDetail->kode_metode_pemeriksaan = $input['kode'][$i];
            $saveDetail->instrument_lain = $input['instrument_lain'][$i];
            if ($input['hasil'][$i] == 'NaN') {
                $saveDetail->hasil_pemeriksaan = "-";
            }else{
                $saveDetail->hasil_pemeriksaan = $input['hasil'][$i];
            }
            $saveDetail->metode_lain = $input['metode_lain'][$i];
            $saveDetail->save();
            $i++;
        }
        $date_sekarang = date('Y-m-d H:i:s');
        DB::table('tanggal_input')->insert(['id_register' => $id, 'siklus' => $siklus, 'type' => $input['type'], 'input_date' => $date_sekarang]);
        if ($siklus == '1') {
            Register::where('id',$id)->update(['siklus_1'=>'done']);
            if ($input['type'] == 'a') {
                Register::where('id',$id)->update(['pemeriksaan'=>'done', 'status_data1'=>'1']);
            }else{
                Register::where('id',$id)->update(['pemeriksaan2'=>'done', 'status_data2'=>'1']);
            }
        }else{
            Register::where('id',$id)->update(['siklus_2'=>'done']);
            if ($input['type'] == 'a') {
                Register::where('id',$id)->update(['rpr1'=>'done', 'status_datarpr1'=>'1']);
            }else{
                Register::where('id',$id)->update(['rpr2'=>'done', 'status_datarpr2'=>'1']);
            }
        }
        return redirect('hasil-pemeriksaan');
        }  
    }

    public function edit(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $instrumen = DB::table('tb_instrumen')->get();
        $data = DB::table('parameter')
            ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
            ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
            ->leftjoin('metode_pemeriksaan', 'hp_details.kode_metode_pemeriksaan', '=', 'metode_pemeriksaan.id')
            ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
            ->leftjoin('tb_instrumen','hp_details.alat', '=', 'tb_instrumen.id')
            ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil', 'hp_details.metode_lain', 'metode_pemeriksaan.kode', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.alat','tb_instrumen.id as idinstrumen', 'tb_instrumen.instrumen as Instrumen','hp_details.instrument_lain', 'hp_details.id as id_detail')
            ->where('parameter.kategori', 'kimia klinik')
            ->where('hp_headers.id_registrasi', $id)
            ->where('hp_headers.type', $type)
            ->where('hp_headers.siklus', $siklus)
            ->get();
            foreach ($data as $key => $val) {
                $metode = MetodePemeriksaan::where('parameter_id', $val->id)->get();
                $val->metode = $metode;
                $instrumen = Instrumen::where('id_parameter', $val->id)->get();
                $val->instrumen = $instrumen;
            }
            // dd($data);
        $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();
            // dd($datas);

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        
        return view('edit_hasil/kimia-klinik', compact('data', 'perusahaan', 'datas', 'type','siklus','date','instrumen'));
    }


    public function update(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $type = $request->get('x');
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;

        // dd($input);
        $SaveHeader['tgl_penerimaan'] = $request->tanggal_penerimaan;
        $SaveHeader['tgl_pemeriksaan'] = $request->tanggal_pemeriksaan;
        $SaveHeader['kualitas_bahan'] = $request->kualitas;
        $SaveHeader['kode_bahan'] = $request->kode_bahan;
        $SaveHeader['catatan'] = $request->catatan;
        $SaveHeader['penanggung_jawab'] = $request->penanggung_jawab;

        HpHeader::where('id_registrasi',$id)->where('siklus', $siklus)->where('type', $type)->update($SaveHeader);
        // dd($input);
        $i = 0;
        foreach ($input['alat'] as $alat) {
            if($alat != ''){
                $SaveDetail['alat'] = $request->alat[$i];
                $SaveDetail['kode_metode_pemeriksaan'] = $request->kode[$i];
                $SaveDetail['instrument_lain'] = $request->instrument_lain[$i];
                $SaveDetail['metode_lain'] = $request->metode_lain[$i];
                $SaveDetail['hasil_pemeriksaan'] = $request->hasil[$i];
                HpDetail::where('id', $request->id_detail[$i])->update($SaveDetail);
                }
                $i++;
        }
        if ($request->simpan == "Kirim" || $request->simpan == "Validasi") {
            if ($siklus == '1') {
                Register::where('id',$id)->update(['siklus_1'=>'done']);
                if ($input['type'] == 'a') {
                    Register::where('id',$id)->update(['pemeriksaan'=>'done', 'status_data1'=>'2']);
                }else{
                    Register::where('id',$id)->update(['pemeriksaan2'=>'done', 'status_data2'=>'2']);
                }
            }else{
                Register::where('id',$id)->update(['siklus_2'=>'done']);
                if ($input['type'] == 'a') {
                    Register::where('id',$id)->update(['rpr1'=>'done', 'status_datarpr1'=>'2']);
                }else{
                    Register::where('id',$id)->update(['rpr2'=>'done', 'status_datarpr2'=>'2']);
                }
            }
            if ($request->simpan == "Validasi") {
                DB::table('validasi_input')->where('id_register', $id)->where('siklus', $siklus)->where('type', $input['type'])->delete();
                DB::table('validasi_input')->insert(
                    ['id_register' => $id, 'siklus' => $siklus, 'type' => $input['type'], 'status' => 'done']
                );
                return back();
            }else{
                return redirect('edit-hasil');
            }
        }else{
            return back();
        }
    }


    public function evaluasi(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->leftjoin('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('tb_instrumen.id as idalat', 'metode_pemeriksaan.id as idmetode',  'hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.instrument_lain', 'hp_details.alat as Instrumen', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'kimia klinik')
                    ->first();
            $val->sd = $sd;
            // $sdalat = DB::table('tb_sd_median_alat')
            //         ->where('alat', $val->idalat)
            //         ->where('siklus', $siklus)
            //         ->where('tahun', $date)
            //         ->where('tipe', $type)
            //         ->where('form', '=', 'kimia klinik')
            //         ->first();
            // $val->sdalat = $sdalat;
            $sdmetode = DB::table('tb_sd_median_metode')
                    ->where('metode', $val->idmetode)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'kimia klinik')
                    ->first();
            $val->sdmetode = $sdmetode;
        }
        $zscoremetode = DB::table('tb_zscore_metode')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();
        $zscore = DB::table('tb_zscore')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();
        // $zscorealat = DB::table('tb_zscore_alat')
        //             ->where('siklus', $siklus)
        //             ->where('tahun', $date)
        //             ->where('type', $type)
        //             ->where('id_registrasi', $id)
        //             ->get();
         // dd($zscoremetode);
        return view('evaluasi.kimiaklinik.zscore.index', compact('data', 'input', 'date', 'type','zscore', 'zscoremetode','id','siklus'));

    }

    public function cetak(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;
       

        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->leftjoin('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('tb_instrumen.id as idalat', 'metode_pemeriksaan.id as idmetode','tb_registrasi.id as ids',  'hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.instrument_lain', 'hp_details.alat as Instrumen', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                ->get();
        // dd($data);
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'kimia klinik')
                    ->first();
            $val->sd = $sd;
            // $sdalat = DB::table('tb_sd_median_alat')
            //         ->where('alat', $val->idalat)
            //         ->where('siklus', $siklus)
            //         ->where('tahun', $date)
            //         ->where('tipe', $type)
            //         ->where('form', '=', 'kimia klinik')
            //         ->first();
            // $val->sdalat = $sdalat;
            $sdmetode = DB::table('tb_sd_median_metode')
                    ->where('metode', $val->idmetode)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'kimia klinik')
                    ->first();
            $val->sdmetode = $sdmetode;
        }
         $datas = HpHeader::where('id_registrasi', $id)
            ->where('type', $type)
            ->where('siklus', $siklus)
            ->first();
        $tanggal = Date('Y m d');

        $zscoremetode = DB::table('tb_zscore_metode')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();
        
        $zscore = DB::table('tb_zscore')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        $evaluasi = TanggalEvaluasi::where('form','=','Kimiaklinik')->where('siklus', $siklus)->where('tahun', $date)->first(); 
        // $zscorealat = DB::table('tb_zscore_alat')
        //             ->where('siklus', $siklus)
        //             ->where('tahun', $date)
        //             ->where('type', $type)
        //             ->where('id_registrasi', $id)
        //             ->get();
        if (count($zscore) > 0) {
            $pdf = PDF::loadview('evaluasi/kimiaklinik/zscore/print/cetak', compact('data','tanggal', 'datas','input', 'date', 'type','zscore', 'zscorealat', 'zscoremetode','id','siklus','evaluasi','register','perusahaan'))
                ->setPaper('a4', 'landscape')
                ->setwarnings(false);
                 return $pdf->stream('KimiaKlinik.pdf');
        }else{
            Session::flash('message', 'Data Kimia klinik Belum Dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return redirect::back();
        }
    }

    public function insertevaluasi(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $input = $request->all();
        if($input['simpan'] != "Batal Evaluasi"){
            $i = 0;
            foreach ($input['parameter'] as $alat) {
                if(!empty($input['zscoreid'][$i])){
                    $Save = ZScore::find($input['zscoreid'][$i]);
                }else{
                    $Save = new ZScore;
                }
                
                $Save->id_registrasi = $id;
                $Save->parameter = $input['parameter'][$i];
                $Save->zscore = $input['zscore'][$i];
                $Save->type = $type;    
                $Save->siklus = $siklus;
                $Save->tahun = $date;
                $Save->form = 'kimia klinik';
                $Save->save();


                // if(!empty($input['zscorealatid'][$i])){
                //     $SaveAlat = ZScoreAlat::find($input['zscorealatid'][$i]);
                // }else{
                //     $SaveAlat = new ZScoreAlat;
                // }
                
                // $SaveAlat->id_registrasi = $id;
                // $SaveAlat->parameter = $input['parameter'][$i];
                // $SaveAlat->alat = $input['alat'][$i];
                // $SaveAlat->zscore = $input['zscorealat'][$i];
                // $SaveAlat->type = $type;
                // $SaveAlat->siklus = $siklus;
                // $SaveAlat->tahun = $date;
                // $SaveAlat->form = 'kimia klinik';
                // $SaveAlat->save();

                if(!empty($input['zscoremetodeid'][$i])){
                    $Save = ZScoreMetode::find($input['zscoremetodeid'][$i]);
                }else{
                    $Save = new ZScoreMetode;
                }

                $Save->id_registrasi = $id;
                $Save->parameter = $input['parameter'][$i];
                $Save->metode = $input['metode'][$i];
                $Save->zscore = $input['zscoremetode'][$i];
                $Save->type = $type;
                $Save->siklus = $siklus;
                $Save->tahun = $date;
                $Save->form = 'kimia klinik';
                $Save->save();
                $i++;
            }
        }else{
            ZScore::where('id_registrasi','=',$id)->where('form','=','kimia klinik')->delete();
            ZScoreMetode::where('id_registrasi','=',$id)->where('form','=','kimia klinik')->delete();
        }
        
        return redirect('hasil-pemeriksaan/kimia-klinik/evaluasi/'.$id.'?x='.$type.'&y='.$siklus);
    }


    public function evaluasialat(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'tb_instrumen.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median_alat')
                    ->where('alat', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'kimia klinik')
                    ->get();
            $val->sd = $sd;
        }
        $zscore = DB::table('tb_zscore_alat')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();
         // dd($data);
        return view('evaluasi.kimiaklinik.zscorealat.index', compact('data', 'input', 'date', 'type','zscore'));
    }

    public function insertevaluasialat(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $input = $request->all();

        $i = 0;
        foreach ($input['alat'] as $alat) {
            $Save = new ZScoreAlat;
            $Save->id_registrasi = $id;
            $Save->parameter = $input['parameter'][$i];
            $Save->alat = $input['alat'][$i];
            $Save->zscore = $input['zscore'][$i];
            $Save->type = $type;
            $Save->siklus = $siklus;
            $Save->tahun = $date;
            $Save->form = 'kimia klinik';
            $Save->save();
            $i++;
        }
        return redirect('evaluasi/hitung-zscore-alat/kimiaklinik');
    }

    public function evaluasimetode(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', $type)
                ->where('tb_registrasi.id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id as id_parameter', 'metode_pemeriksaan.id', 'hp_details.instrument_lain', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                ->get();
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median_metode')
                    ->where('metode', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('tipe', $type)
                    ->where('form', '=', 'kimia klinik')
                    ->get();
            $val->sd = $sd;
        }
        $zscore = DB::table('tb_zscore_metode')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('type', $type)
                    ->where('id_registrasi', $id)
                    ->get();
         // dd($data);
        return view('evaluasi.kimiaklinik.zscoremetode.index', compact('data', 'input', 'date', 'type','zscore'));
    }

    public function insertevaluasimetode(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;
        
        $input = $request->all();

        $i = 0;
        foreach ($input['metode'] as $metode) {
            $Save = new ZScoreMetode;
            $Save->id_registrasi = $id;
            $Save->parameter = $input['parameter'][$i];
            $Save->metode = $input['metode'][$i];
            $Save->zscore = $input['zscore'][$i];
            $Save->type = $type;
            $Save->siklus = $siklus;
            $Save->tahun = $date;
            $Save->form = 'kimia klinik';
            $Save->save();
            $i++;
        }
        return redirect('evaluasi/hitung-zscore-metode/kimiaklinik');
    }


    public function perusahaan(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes'), 'tb_registrasi.id as idregistrasi')
                    ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.status', '>=', '2')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                    ->where(function($query) use ($siklus)
                        {
                            $query->where('siklus', '=', '12')
                                ->orwhere('siklus', $siklus);
                        })
                    ->where('tb_registrasi.bidang','=','2')
                    ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="kimiaklinik/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Cetak
                                    </button>
                                </a>'
                        ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if($siklus == 1) {
                        if ($inputhasil->status_data1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }else{
                        if ($inputhasil->status_datarpr1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if($siklus == 1) {
                        if ($inputhasil->status_data1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }else{
                        if ($inputhasil->status_datarpr1 == '2' ) {
                            return '<div class="status-success">Kirim Hasil</div>';
                        }else{
                            return '<div class="status-danger">Kirim Hasil</div>';
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
        return view('evaluasi.kimiaklinik.rekapcetakpeserta.perusahaan');
    }

    public function dataperusahaan(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('sub_bidang.id', '=', '2')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2);
                    })
                ->where(function($query)
                    {
                        if ('pemeriksaan' != 'done') {
                            $query->orwhere('pemeriksaan2', 'done');
                        }
                        if ('pemeriksaan2' != 'done') {
                            $query->orwhere('pemeriksaan', 'done');
                        }
                    })
                ->get();
        return view('evaluasi.kimiaklinik.rekapcetakpeserta.dataperusahaan', compact('data','siklus'));
    }

    public function Testtest()
    {
        $siklus = '1';
        $type = 'a';
        $date = '2018';

        $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','2')->get();
            foreach($datas as $r){
                $r->sub_bidang =  DB::table('tb_registrasi')
                    ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                    ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                    ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                    ->where('tb_registrasi.perusahaan_id', $r->id)
                    ->where('sub_bidang.id', '=', '2')
                    ->where('status', 3)
                    ->where(function($query)
                        {
                            $query->where('status_data1', 2)
                                ->orwhere('status_data2', 2);
                        })
                    ->where(function($query)
                        {
                            if ('pemeriksaan' != 'done') {
                                $query->orwhere('pemeriksaan2', 'done');
                            }
                            if ('pemeriksaan2' != 'done') {
                                $query->orwhere('pemeriksaan', 'done');
                            }
                        })
                    ->get();
                foreach($r->sub_bidang as $sb){
                    $zscore = DB::table('tb_zscore')
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->where('tahun', $date)
                            ->where('form', '=', 'kimia klinik')
                            ->where('id_registrasi', $sb->id);
                    $zscore->delete();
                    $zscoremetode = DB::table('tb_zscore_metode')
                            ->where('siklus', $siklus)
                            ->where('type', $type)
                            ->where('tahun', $date)
                            ->where('form', '=', 'kimia klinik')
                            ->where('id_registrasi', $sb->id);
                    $zscoremetode->delete();
                    $dataSave = [];
                    $dataParamA = DB::table('parameter')
                        ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                        ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                        ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                        ->leftjoin('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                        ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                        ->where('hp_headers.siklus', $siklus)
                        ->where('hp_headers.type', $type)
                        ->where('tb_registrasi.id', $sb->id)
                        ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                        ->select('tb_instrumen.id as idalat', 'metode_pemeriksaan.id as idmetode',  'hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.instrument_lain', 'hp_details.alat as Instrumen', 'tb_instrumen.instrumen', 'metode_pemeriksaan.metode_pemeriksaan', 'hp_details.metode_lain', 'hp_details.hasil_pemeriksaan')
                        ->get();
                    foreach ($dataParamA as $key => $valParam) {
                        $sd = DB::table('tb_sd_median')
                                ->where('parameter', $valParam->id)
                                ->where('siklus', $siklus)
                                ->where('tahun', $date)
                                ->where('tipe', $type)
                                ->where('form', '=', 'kimia klinik')
                                ->get();
                        $valParam->sd = $sd;

                        $sdmetode = DB::table('tb_sd_median_metode')
                                ->where('metode', $valParam->idmetode)
                                ->where('siklus', $siklus)
                                ->where('tahun', $date)
                                ->where('tipe', $type)
                                ->where('form', '=', 'kimia klinik')
                                ->first();
                        $valParam->sdmetode = $sdmetode;

                        $dataSave['parameter_id'][$key] = $valParam->id;
                        $dataSave['metode_id'][$key] = $valParam->idmetode;
                        $valParam->hasil_pemeriksaan = str_replace(",",".",$valParam->hasil_pemeriksaan);
                        $valParam->hasil_pemeriksaan = ($valParam->hasil_pemeriksaan == "-" ? NULL : str_replace(",",".",$valParam->hasil_pemeriksaan));

                        $labelHasil = $valParam->hasil_pemeriksaan;
                        try{
                            $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan  *1;
                        }catch(\Exception $e){
                            $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                        }
                        // dd($valParam->sd[0]->median);
                        if($valParam->hasil_pemeriksaan == "-" || $valParam->hasil_pemeriksaan == null || empty($valParam->hasil_pemeriksaan) || is_string($valParam->hasil_pemeriksaan)){
                            if($valParam->hasil_pemeriksaan == '0' || $valParam->hasil_pemeriksaan == '0.0' || $valParam->hasil_pemeriksaan == '0.00' || is_string($valParam->hasil_pemeriksaan)){
                                $valParam->hasil_pemeriksaan = $valParam->hasil_pemeriksaan;
                            }else{
                                $valParam->hasil_pemeriksaan = NULL;
                            }
                            $dataSave['zscore'][$key] = "-";
                            $dataSave['zscore_metode'][$key] = "-";
                        }else{
                            $target_parameter = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->median) ? $valParam->sd[0]->median : '-') : '-');
                            $sd_parameter = (!empty($valParam->sd) ? (!empty($valParam->sd[0]->sd) ? $valParam->sd[0]->sd : '-') : '-');
                            // dd($sd_parameter);
                            $dataSave['zscore'][$key] = "-";
                            if(($target_parameter != '-' && $sd_parameter != '-')){
                                $dataSave['zscore'][$key] = number_format(($valParam->hasil_pemeriksaan - $valParam->sd[0]->median) / $valParam->sd[0]->sd,2);
                            }

                            $target_metode = (!empty($valParam->sdmetode) ? (!empty($valParam->sdmetode[0]->median) ? $valParam->sdmetode[0]->median : '-') : '-');
                            $sd_metode = (!empty($valParam->sdmetode) ? (!empty($valParam->sdmetode[0]->sd) ? $valParam->sdmetode[0]->sd : '-') : '-');
                            $dataSave['zscore_metode'][$key] = '-';
                            if(($target_metode != '-' && $sd_metode != '-')){
                                $dataSave['zscore_metode'][$key] = number_format(($valParam->hasil_pemeriksaan - $valParam->sdmetode[0]->median) / $valParam->sdmetode[0]->sd,2);
                            }
                        }
                        // dd($dataSave['zscore'][$key]);
                    }
                    $i = 0;
                    if(!empty($dataSave)){
                        foreach ($dataSave['parameter_id'] as $alat) {
                            $Save = new ZScore;
                            $Save->id_registrasi = $sb->id;
                            $Save->parameter = $dataSave['parameter_id'][$i];
                            $Save->zscore = $dataSave['zscore'][$i];
                            $Save->type = $type;
                            $Save->siklus = $siklus;
                            $Save->tahun = $date;
                            $Save->form = 'kimia klinik';
                            $Save->save();

                            $Save = new ZScoreMetode;
                            $Save->id_registrasi = $sb->id;
                            $Save->parameter = $dataSave['parameter_id'][$i];
                            $Save->metode = $dataSave['metode_id'][$i];
                            $Save->zscore = $dataSave['zscore_metode'][$i];
                            $Save->type = $type;
                            $Save->siklus = $siklus;
                            $Save->tahun = $date;
                            $Save->form = 'kimia klinik';
                            $Save->save();
                            $i++;
                        }
                    }
                }
            }
    }
}