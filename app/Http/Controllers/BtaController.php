<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\bta as Bta;
use App\saranbta;
use App\EvaluasiBta;
use App\daftar as Daftar;
use App\register as Register;
use App\EvaluasiBTAStatus;
use Redirect;
use Validator;
use Session;

class BtaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('y');
        $data = DB::table('parameter')->where('kategori', 'hematologi')->get();

        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;
        // dd($tahun);
        return view('hasil_pemeriksaan/bta', compact('data', 'perusahaan','siklus', 'tahun'));
    }

    public function view(\Illuminate\Http\Request $request, $id){
        // $data = DB::table('parameter')
        //     ->leftjoin('hp_details', 'parameter.id', '=', 'hp_details.parameter_id')
        //     ->leftjoin('hp_headers', 'hp_details.hp_header_id', '=', 'hp_headers.id')
        //     ->leftjoin('tb_registrasi', 'hp_headers.id_registrasi', '=', 'tb_registrasi.id')
        //     ->select('parameter.*','hp_details.alat as Alat', 'hp_details.kode_metode_pemeriksaan as Kode', 'hp_details.hasil_pemeriksaan as Hasil')
        //     ->where('parameter.kategori', 'hematologi')
        //     ->where('hp_headers.id_registrasi', $id)
        //     ->get();
        // $datas = HpHeader::where('id_registrasi', $id)
        //     ->first();
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $data = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->first();
        $datas = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        
        // return view('cetak_hasil/telur-cacing', compact('data', 'perusahaan', 'datas', 'type'));
        if (count($data) > 0) {
         $pdf = PDF::loadview('cetak_hasil/bta', compact('data', 'perusahaan','datas', 'siklus', 'date'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        }else{
            Session::flash('message', 'BTA Belum dievaluasi!'); 
            Session::flash('alert-class', 'alert-danger');
            return back();
        }

        return $pdf->stream('BTA.pdf');
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    { 
        $input = $request->all();
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $years = $tahun->year;

        // dd($input);
        $validasi = Bta::where(DB::raw('YEAR(tb_bta.created_at)'), '=' , $years)->where('id_registrasi','=',$id)->where('siklus','=',$siklus)->get();

        $i = 0;
          if (count($validasi)>0) {
            Session::flash('message', 'Hasil BTA Sudah Ada!'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
        }else{
        foreach ($input['kode'] as $kode) {
            if($kode != ''){
              
                $Savedata = new Bta;
                $Savedata->kode = $input['kode'][$i];
                $Savedata->kondisi = $input['kondisi'];
                $Savedata->hasil = htmlentities($input['hasil'][$i]);
                $Savedata->kode_peserta = $input['kode_peserta'];
                $Savedata->nomor_hp = $input['nomor_hp'];
                $Savedata->kuman = $input['kuman'][$i];
                $Savedata->nama_pemeriksa = $input['nama_pemeriksa'];
                $Savedata->penanggung_jawab = $input['penanggung_jawab'];
                $Savedata->catatan = $input['catatan'];
                $Savedata->tgl_penerimaan = $input['tgl_penerimaan'];
                $Savedata->tgl_pemeriksaan = $input['tgl_pemeriksaan'];
                $Savedata->created_by = Auth::user()->id;
                $Savedata->id_registrasi = $id;
                $Savedata->siklus = $siklus;
                $Savedata->save();
                }
            $i++;
        }
        // dd($input);

        $date_sekarang = date('Y-m-d H:i:s');
        DB::table('tanggal_input')->insert(['id_register' => $id, 'siklus' => $siklus, 'input_date' => $date_sekarang]);
        if ($siklus == '1') {
            Register::where('id',$id)->update(['siklus_1'=>'done', 'rpr1'=>'done', 'status_data1'=>'1']);
            Register::where('id',$id)->update(['pemeriksaan'=>'done']);
        }else{
            Register::where('id',$id)->update(['siklus_2'=>'done', 'rpr2'=>'done', 'status_data2'=>'1']);
            Register::where('id',$id)->update(['pemeriksaan2'=>'done']);
        }
        return redirect('hasil-pemeriksaan');
        }
    }

    public function edit(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $data = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->first();
        $datas = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->get();
        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;

        $q1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $q2 = new Carbon( $q1->created_at );
        $date = $q2->year;
        
        return view('edit_hasil/bta', compact('data', 'perusahaan', 'datas', 'siklus', 'date'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $register = Register::find($id);

        // dd($input);
        $i = 0;
        foreach ($input['kode'] as $kode) {
            if($kode != ''){
                $Savedata['siklus'] = $siklus;
                $Savedata['tgl_pemeriksaan'] = $request->tgl_pemeriksaan;
                $Savedata['tgl_penerimaan'] = $request->tgl_penerimaan;
                $Savedata['catatan'] = $request->catatan;
                $Savedata['penanggung_jawab'] = $request->penanggung_jawab;
                $Savedata['nama_pemeriksa'] = $request->nama_pemeriksa;
                $Savedata['nomor_hp'] = $request->nomor_hp;
                $Savedata['hasil'] = $request->hasil[$i];
                $Savedata['kode'] = $request->kode[$i];
                $Savedata['kondisi'] = $request->kondisi;
                $Savedata['kuman'] = $request->kuman[$i];
                Bta::where('id', $request->idbta[$i])->update($Savedata);
            }
            $i++;
        }
        if ($request->simpan == "Kirim" || $request->simpan == "Validasi") {
            if ($siklus == '1') {
                Register::where('id',$id)->update(['status_data1'=>'2']);
            }else{
                Register::where('id',$id)->update(['status_data2'=>'2']);
            }
            if ($request->simpan == "Validasi") {
                DB::table('validasi_input')->where('id_register', $id)->where('siklus', $siklus)->delete();
                DB::table('validasi_input')->insert(
                    ['id_register' => $id, 'siklus' => $siklus, 'status' => 'done']
                );
                return back();
            }else{
                return redirect('edit-hasil');
            }
        }else{
            return back();
        }
    }

    public function evaluasi(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun = new Carbon( $data1->created_at );
        $date = $tahun->year;

        $data = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->first();
        $datas = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->get();
        $rujuk = DB::table('tb_rujukan_bta')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $date)
                ->orderBy('kode_bahan_uji', 'asc')
                ->get();
        $kode = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $date)
                ->first();
        $register = Register::find($id);
        $evaluasi = DB::table('tb_evaluasi_bta')->where('siklus',$siklus)->where('id_registrasi',$id)->get();
        $perusahaan = $register->perusahaan->nama_lab;
        $status = DB::table('tb_evaluasi_bta_status')
                ->where('siklus', $siklus)
                ->where('tahun', $date)
                ->where('id_registrasi', $id)
                ->first();
        $skor = DB::table('tb_evaluasi_bta')
                ->where('id_registrasi',$id)
                ->where('siklus',$siklus)
                ->groupBy('id_registrasi')
                ->sum('nilai');
        $saran = DB::table('tb_saran_bta')->where('id_registrasi',$id)->where('tahun','=', $date)->where('siklus','=',$siklus)->first();
        // dd($evaluasi);
        if ($rujuk != NULL) {
            return view('evaluasi.bta.penilaian.evaluasi.index', compact('data','saran','kode','siklus','kode_peserta','perusahaan', 'datas', 'type','rujuk','evaluasi', 'status','id','skor'));
        }else{
            return back();
        }
    }

    public function cetak(\Illuminate\Http\Request $request, $id){
        $siklus = $request->get('y');
        $tanggal = date('d-m-Y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;

        $data = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();
        
        $saran = DB::table('tb_saran_bta')->where('id_registrasi',$id)->where('tahun','=', $tahun)->where('siklus','=',$siklus)->first();
        $datas = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->get();
        // dd($datas);
        $kode = DB::table('tb_bta')
                ->where('id_registrasi', $id)
                ->where('siklus', $siklus)
                ->where(DB::raw('YEAR(created_at)'), '=' , $tahun)
                ->first();
        $rujuk = DB::table('tb_rujukan_bta')
                ->where('siklus', $siklus)
                ->where('tahun', '=' , $tahun)
                ->orderBy('kode_bahan_uji', 'asc')
                ->get();
        $register = Register::find($id);
        $evaluasi = DB::table('tb_evaluasi_bta')->where('siklus',$siklus)->where('id_registrasi',$id)->get();
        // dd($evaluasi);
        $perusahaan = $register->perusahaan->nama_lab;
        $status = DB::table('tb_evaluasi_bta_status')
                ->where('siklus', $siklus)
                ->where('tahun', $tahun)
                ->where('id_registrasi', $id)
                ->first();
        $skor = DB::table('tb_evaluasi_bta')
                ->where('id_registrasi',$id)
                ->where('siklus',$siklus)
                ->groupBy('id_registrasi')
                ->sum('nilai');
        $tanggalevaluasi = DB::table('tanggal_evaluasi')->where('form', '=', 'Mikrobiologi BTA')->where('siklus', $siklus)->where('tahun', $tahun)->first();

        // dd($saran);
        if ($rujuk != NULL) {
            if (count($evaluasi)>0) {
            $pdf = PDF::loadView('evaluasi.bta.penilaian.evaluasi.cetak',compact('data','kode','saran', 'perusahaan', 'datas', 'type','rujuk','evaluasi', 'status','siklus','tahun','tanggal','skor', 'tanggalevaluasi'));
            $pdf->setPaper('a4','potrait');
            return $pdf->stream('EvaluasiBta'.rand(2,32012).'.pdf');
            }else{
                 Session::flash('message', 'BTA belum dievaluasi!');
                Session::flash('alert-class', 'alert-danger');
                return back();
            }

        }else{
            return back();
        }
    }

    public function insertevaluasi(\Illuminate\Http\Request $request, $id)
    { 
        $input = $request->all();
        $siklus = $request->get('y');
        
        $data1 = DB::table('tb_registrasi')->where('id', $id)->first();
        $tahun1 = new Carbon( $data1->created_at );
        $tahun = $tahun1->year;
        // dd($input);
        $i = 0;
        if ($input['simpan'] == 'Simpan') {
            foreach ($input['kode_sediaan'] as $kode) {
                if($kode != ''){
                    $Savedata = new EvaluasiBta;
                    $Savedata->id_registrasi = $id;
                    $Savedata->kode_sediaan = $input['kode_sediaan'][$i];
                    $Savedata->keterangan = $input['nilai'][$i];
                    if ($input['nilai'][$i] == 'Benar') {
                        $Savedata->nilai = '10';
                    }else if($input['nilai'][$i] == 'KH'){
                        $Savedata->nilai = '5';
                    }else if($input['nilai'][$i] == 'PPR'){
                        $Savedata->nilai = '5';
                    }else if($input['nilai'][$i] == 'NPR'){
                        $Savedata->nilai = '5';
                    }else if($input['nilai'][$i] == 'PPT'){
                        $Savedata->nilai = '0';
                    }else if($input['nilai'][$i] == 'NPT'){
                        $Savedata->nilai = '0';
                    }
                    $Savedata->siklus = $siklus;
                    $Savedata->tahun = $tahun;
                    $Savedata->save();
                }
                $i++;
            }
            $Savedatakomen = new saranbta;
            $Savedatakomen->id_registrasi = $id;
            $Savedatakomen->siklus = $siklus;
            $Savedatakomen->tahun = $tahun;
            if (isset($input['kh'])) {
                $Savedatakomen->kh = $input['kh'];
            }else{
                $Savedatakomen->kh = 0;
            }
            if (isset($input['npr'])) {  
                $Savedatakomen->npr = $input['npr'];
            }else{
                $Savedatakomen->npr = 0;
            }
            if (isset($input['npt'])) {
                $Savedatakomen->npt = $input['npt'];
            }else{
                $Savedatakomen->npt = 0;
            }
            if (isset($input['ppr'])) {  
                $Savedatakomen->ppr = $input['ppr'];
            }else{
                $Savedatakomen->ppr = 0;
            }
            if (isset($input['ppt'])) {  
                $Savedatakomen->ppt = $input['ppt'];
            }else{
                $Savedatakomen->ppt = 0;
            }
            // $Savedatakomen->tanggal_ttd = $input->ttd;
            $Savedatakomen->save();

            $EvaluasiStatus = new EvaluasiBTAStatus;
            $EvaluasiStatus->id_registrasi = $id;
            $EvaluasiStatus->status = $input['status'];
            $EvaluasiStatus->siklus = $siklus;
            $EvaluasiStatus->tahun = $tahun;
            $EvaluasiStatus->save();
        }else{
            foreach ($input['kode_sediaan'] as $kode) {
                if($kode != ''){
                    $Savedata['siklus'] = $siklus;
                    $Savedata['kode_sediaan'] = $request->kode_sediaan[$i];
                    $Savedata['keterangan'] = $request->nilai[$i];
                    if ($input['nilai'][$i] == 'Benar') {
                        $Savedata['nilai'] = '10';
                    }else if($input['nilai'][$i] == 'KH'){
                        $Savedata['nilai'] = '5';
                    }else if($input['nilai'][$i] == 'PPR'){
                        $Savedata['nilai'] = '5';
                    }else if($input['nilai'][$i] == 'NPR'){
                        $Savedata['nilai'] = '5';
                    }else if($input['nilai'][$i] == 'PPT'){
                        $Savedata['nilai'] = '0';
                    }else if($input['nilai'][$i] == 'NPT'){
                        $Savedata['nilai'] = '0';
                    }
                    EvaluasiBta::where('id', $request->id[$i])->update($Savedata);
                }
                $i++;
            }   
            $status = saranbta::where("tb_saran_bta.id_registrasi", $id)
            ->where('tb_saran_bta.tahun','=', $tahun)
            ->where('tb_saran_bta.siklus','=', $siklus)
            ->update(["kh" => $request->kh,"npr" => $request->npr,"npt" => $request->npt,"ppr" => $request->ppr,"ppt" => $request->ppt,"tanggal_ttd" => $request->ttd]);
            $EvaluasiStatus['status'] = $request->status;
            EvaluasiBTAStatus::where('id_registrasi', $id)->where('siklus', $siklus)->where('tahun', $tahun)->update($EvaluasiStatus);
        }
        // dd($Savedata);
        return back();
    }
}