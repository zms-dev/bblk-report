<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\subbidang as Subbidang;
use Illuminate\Support\Facades\Redirect;

class SubbidangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $subbidang = DB::table('tb_bidang')
            ->join('sub_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
            ->select('sub_bidang.*', 'tb_bidang.bidang as Bidang')
            ->get();
        return view('back/subbidang/index', compact('subbidang'));
    }

    public function edit($id)
    {
        $bidang = DB::table('tb_bidang')->get();
        $data = DB::table('tb_bidang')
            ->join('sub_bidang', 'tb_bidang.id', '=', 'sub_bidang.id_bidang')
            ->select('sub_bidang.*', 'tb_bidang.bidang as Bidang', 'tb_bidang.id as Id')
            ->where('sub_bidang.id',$id)
            ->get();
        return view('back/subbidang/update', compact('data','bidang'));
    }

    public function in()
    {
        $bidang = DB::table('tb_bidang')->get();
        return view('back/subbidang/insert', compact('bidang'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data = Request::all();
        $data['created_by'] = Auth::user()->id;
        Subbidang::where('id',$id)->update($data);
        Session::flash('message', 'Master Bidang Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
        // dd($data);
        return redirect('admin/parameter');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        $data['created_by'] = Auth::user()->id;
        Subbidang::create($data);
        Session::flash('message', 'Parameter Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/parameter');
    }

    public function delete($id){
        Session::flash('message', 'Data Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Subbidang::find($id)->delete();
        return redirect("admin/parameter");
    }
}
