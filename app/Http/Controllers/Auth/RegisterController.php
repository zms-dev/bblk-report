<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\daftar as Daftar;
use App\register as Register;
use App\subbidang as Subbidang;

Use DB;
use Request;
use Session;
use Redirect;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/sessionregister';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        if ($data['pembayaran'] == 'transfer')
        {
            $validasi = Validator::make($data,[
                'file' => 'required|mimes:jpeg,pdf,png'
            ]);
        }else{
            $validasi = Validator::make($data,[
                'file1' => 'required|mimes:jpeg,pdf,png'
            ]);
        }
        $tahun = date('Y');
        $register = DB::table('tb_registrasi')
                    ->join('users', 'users.id','=','tb_registrasi.created_by')
                    ->where(DB::raw('Year(tb_registrasi.created_at)'), $tahun)
                    ->whereNotNull('no_urut')
                    ->select(DB::raw('CAST(no_urut AS UNSIGNED) as no_urut'))
                    ->groupBy('no_urut')
                    ->orderBy('no_urut', 'desc')
                    ->first();
        if($register != NULL){
            $cekno = $register->no_urut + 1;
            $no_urut = $cekno;
        }else{
            $no_urut = 1;
        }

        if ($validasi->fails()) {
            return redirect('register')->withInput()->withErrors($validasi);
        }
        $userid = DB::table('users')->orderBy('id_member', 'desc')->first();
        if($userid != NULL){
            if (Request::hasFile('file'))
            {
                $dest = public_path('asset/file/');
                $name = $data['name'].'-'.$tahun.'-'.$data['siklus'].'-'.Request::file('file')->getClientOriginalName();
                Request::file('file')->move($dest, $name);
            }
            if (Request::hasFile('file1'))
            {
                $dest = public_path('asset/file/');
                $name1 = $data['name'].'-'.$tahun.'-'.$data['siklus'].'-'.Request::file('file1')->getClientOriginalName();
                Request::file('file1')->move($dest, $name1);
            }
            // dd($name);
            $id_member = $userid->id_member + 1;


            $user = new User;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->role = '3';
            $user->id_member = $id_member;
            $user->password = bcrypt($data['password']);
            $user->save();

            $saveDaftar = new Daftar;
            $saveDaftar->nama_lab = $data['name'];
            $saveDaftar->pemerintah = $data['pemerintah'];
            $saveDaftar->alamat = $data['alamat'];
            $saveDaftar->provinsi = $data['provinsi'];
            $saveDaftar->kota = $data['kota'];
            $saveDaftar->kecamatan = $data['kecamatan'];
            $saveDaftar->kelurahan = $data['kelurahan'];
            $saveDaftar->telp = $data['telp'];
            $saveDaftar->email = $data['email'];
            $saveDaftar->penanggung_jawab = $data['penanggung_jawab'];
            $saveDaftar->personal = $data['personal'];
            $saveDaftar->no_hp = $data['no_hp'];
            $saveDaftar->kode_pos = $data['kode_pos'];
            $saveDaftar->created_by = $user->id;
            $saveDaftar->save();

            $saveDaftarID = $saveDaftar->id;

            $i = 0;
            $im = 1;
            $a = $user->id_member;
            $kode_pes = sprintf("%03s", $no_urut);
            $date = date('y');
            foreach ($data['bidang'] as $alat) {
                $datas = $data;
                if($alat != ''){
                    if($alat == '10'){
                        $imun = DB::table('sub_bidang')->whereIn('id', [6,7,8,9])->get();
                        foreach ($imun as $key => $val) {
                            $saveRegister = new Register;
                            $saveRegister->perusahaan_id = $saveDaftar->id;
                            $saveRegister->bidang = $val->id;
                            $saveRegister->no_urut = $no_urut;
                            $saveRegister->kode_lebpes = $val->alias.'-'.$data['siklus'].'-'.$date.'-'.$kode_pes;
                            if ($data['pembayaran'] == 'transfer')
                            {
                                $saveRegister->file = $name;
                                $saveRegister->status = '1';
                                $saveRegister->id_pembayaran = '1';
                            }else{
                                $saveRegister->sptjm = $data['sptjm'];
                                $saveRegister->file = $name1;
                                $saveRegister->id_pembayaran = '2';
                                $saveRegister->status = '2';
                            }
                            $saveRegister->siklus = $data['siklus'];
                            $saveRegister->created_by = $user->id;
                            $saveRegister->save();
                        }
                    }else{
                        $saveRegister = new Register;
                        $saveRegister->perusahaan_id = $saveDaftar->id;
                        $saveRegister->bidang = $alat;
                        $saveRegister->no_urut = $no_urut;
                        $saveRegister->kode_lebpes = $data['alias'][$alat - 1].'-'.$data['siklus'].'-'.$date.'-'.$kode_pes;
                        if ($data['pembayaran'] == 'transfer')
                        {
                            $saveRegister->file = $name;
                            $saveRegister->status = '1';
                            $saveRegister->id_pembayaran = '1';
                        }else{
                            $saveRegister->sptjm = $data['sptjm'];
                            $saveRegister->file = $name1;
                            $saveRegister->id_pembayaran = '2';
                            $saveRegister->status = '2';
                        }
                        $saveRegister->siklus = $data['siklus'];
                        $saveRegister->created_by = $user->id;
                        $saveRegister->save();
                    }
                    $i++;
                }
            }
            $sub_bidang = DB::table('sub_bidang')->whereIn('id', $data['bidang'])->get();
            $userna = DB::table('users')->where('role', 4)->first();
            $datu = [
                'labklinik' => $data['name'],
                'siklus' => $data['siklus'],
                'date' => date('Y'),
                'parameter' => $sub_bidang 
            ];
             
            Mail::send('email.pendaftaran', $datu, function ($mail) use ($data, $userna)
            {
              $mail->from('sipamela.bblkjkt@gmail.com', 'BBLK Jakarta Daftar '. $data['name']);
              $mail->to($userna->email);
              // $mail->to('tengkufirmansyah2@gmail.com');
              $mail->subject('Daftar PNPME');
            });

            Session::flash('message', 'Selamat, Anda telah berhasil melakukan Pendaftaran, Kami akan mengecek data yang telah anda input, Anda akan menerima email konfirmasi jika kami telah selesai melakukan verifikasi terhadap data yang anda kirimkan, Terimakasih'); 
            Session::flash('alert-class', 'alert-success'); 
        
        return $user;

        }else{
        return User::create([
            'name' => $data['name'],
            'role' => '3',
            'email' => $data['email'],
            'id_member' => '0',
            'password' => bcrypt($data['password']),
        ]);
        }
    }
}
