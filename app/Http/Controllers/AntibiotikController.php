<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\kimiaklinik as Kimiaklinik;
use App\daftar as Daftar;
use App\Dataantibiotik as Dataantibiotik;
use App\Datapekaantibiotik as Datapekaantibiotik;
use App\register as Register;
use App\Fermentasinegatif as Fermentasinegatif;
use App\Fermentasipositif as Fermentasipositif;
use App\Kepekaan as Kepekaan;
use Session;

use Redirect;
use Validator;

class AntibiotikController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(\Illuminate\Http\Request $request, $id)
    {
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');


        $lembar = DB::table('data_antibiotik')
                    ->where(DB::raw('YEAR(created_at)'), $date)
                    ->where('siklus', $siklus)
                    ->where('id_registrasi', $id)
                    ->orderBy('id', 'desc')
                    ->get();
        // dd($lembar);
        $fermentasinegatif = DB::table('bakteri_fermentasi')->where('gram', 'negatif')->get();
        $fermentasipositif = DB::table('bakteri_fermentasi')->where('gram', 'positif')->get();
        $antibiotik = DB::table('antibiotik')->get();

        $register = Register::find($id);
        $perusahaan = $register->kode_lebpes;
        
        return view('hasil_pemeriksaan/antibiotik', compact('data', 'perusahaan','kode', 'type', 'siklus','date', 'fermentasinegatif', 'fermentasipositif', 'antibiotik','lembar'));
    }

    public function view(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        $date = date('Y');
        $lembar = $request->get('lembar');
        if(empty($lembar)){
            $lembar = 1;
        }

        $data = DB::table('data_antibiotik')->where('id_registrasi', $id)->where('siklus', $siklus)->where('lembar', '=', $lembar)->get();

        foreach ($data as $key => $val) {
            $fermentasinegatif = DB::table('tb_fermentasi_negatif')
                                ->leftjoin('bakteri_fermentasi', 'tb_fermentasi_negatif.id_fermentasi', '=', 'bakteri_fermentasi.id')
                                ->join('data_antibiotik', 'tb_fermentasi_negatif.id_antibiotik', '=', 'data_antibiotik.id')
                                ->where('data_antibiotik.id', $val->id)
                                ->get();
            $val->fermentasinegatif = $fermentasinegatif;

            $fermentasipositif = DB::table('tb_fermentasi_positif')
                                ->leftjoin('bakteri_fermentasi', 'tb_fermentasi_positif.id_fermentasi', '=', 'bakteri_fermentasi.id')
                                ->join('data_antibiotik', 'tb_fermentasi_positif.id_antibiotik', '=', 'data_antibiotik.id')
                                ->where('data_antibiotik.id', $val->id)
                                ->get();
            $val->fermentasipositif = $fermentasipositif;

            $kepekaan = DB::table('tb_kepekaan')
                                ->leftjoin('antibiotik', 'tb_kepekaan.id_jenis_antibiotik', '=', 'antibiotik.id')
                                ->join('data_antibiotik', 'tb_kepekaan.id_antibiotik', '=', 'data_antibiotik.id')
                                ->where('data_antibiotik.id', $val->id)
                                ->get();
            $val->kepekaan = $kepekaan;
        }
        $peka = DB::table('data_peka_antibiotik')->where('id_registrasi', $id)->where('siklus', $siklus)->where('lembar', '=', $lembar)->first();
        // dd($peka);
        // dd($data);

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        
        if (count($data) > 0) {
        $pdf = PDF::loadview('cetak_hasil/antibiotik', compact('data', 'perusahaan','peka', 'type','siklus','date'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('Kimia-Klinik.pdf');
        }else{
            Session::flash('message', 'Bakteri belum dievaluasi!');
            Session::flash('alert-class', 'alert-danger');
            return back();
        }
        
    }

    public function insert(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $siklus = $request->get('y');
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;

        $media_digunakan = implode(", ",$input['media_digunakan']);
        $vitek = implode(", ",$input['vitek']);
        $api = implode(", ",$input['api']);
        $api2 = implode(", ",$input['api2']);
        $bd_phoenik = implode(", ",$input['bd_phoenik']);
        $standart = implode(", ",$input['standart']);
        $metode = implode(", ",$input['metode']);
        $years = date('Y');

        $siklus = $input['siklus'];
        $lembar = $input['lembar'];

        $validasi = Datapekaantibiotik::where('id_registrasi','=',$id)->where('siklus','=',$siklus)->where('lembar','=','3')->first();// dd($input);
        // dd($media_digunakan);
        if (count($validasi)>0) {
            Session::flash('message', 'Hasil Identifikasi Bakteri dan Uji Kepekaan Antibiotik Sudah Ada!'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect('hasil-pemeriksaan');
        }else{

        $savePeka = new Datapekaantibiotik;
        $savePeka->id_registrasi = $id;
        $savePeka->hasil_identifikasi = $input['hasil_identifikasi'];
        $savePeka->standart = $standart;
        $savePeka->metode = $metode;
        $savePeka->siklus = $input['siklus'];
        $savePeka->lembar = $input['lembar'];
        $savePeka->save();

        $saveMaster = new Dataantibiotik;
        $saveMaster->kode_lab = $input['kode_peserta'];
        $saveMaster->id_registrasi = $id;
        $saveMaster->kode_lab = $input['kode_peserta'];
        $saveMaster->nama_instansi = $input['nama_instansi'];
        $saveMaster->alamat = $input['alamat'];
        $saveMaster->kondisi = $input['kondisi'];
        $saveMaster->tgl_diterima = $input['tgl_diterima'];
        $saveMaster->nama_pemeriksa = $input['nama_pemeriksa'];
        $saveMaster->no_hp = $input['no_hp'];
        $saveMaster->kd_bahan = $input['kode_bahan'];
        $saveMaster->siklus = $input['siklus'];
        $saveMaster->jenis_bahan = $input['jenis_bahan'];
        $saveMaster->tgl_pelaksanaan = $input['tgl_pelaksanaan'];
        $saveMaster->metode_konvensional = $input['metode_konvensional'];
        $saveMaster->metode_otomatis = $input['metode_otomatis'];
        $saveMaster->metode_lainnya = $input['metode_lainnya'];
        $saveMaster->test_metode_identifikasi = $input['metodetext1'];
        $saveMaster->media_digunakan = $media_digunakan;
        $saveMaster->buatan_sendiri = $input['media_buatan'];           
        $saveMaster->media_komersial = $input['media_komersial'];  
        $saveMaster->fermentasilainn = $input['fermentasilainn'];  
        $saveMaster->fermentasilainp = $input['fermentasilainp'];
        $saveMaster->hasil_kultur = $input['hasil_kultur'];
        $saveMaster->spesies_kultur = $input['spesies_kultur'];
        $saveMaster->pewarnaan_gram_p = $input['pewarnaan_gram_p'];
        $saveMaster->pewarnaan_gram_n = $input['pewarnaan_gram_n'];
        $saveMaster->pewarnaan_gram_y = $input['pewarnaan_gram_y'];
        $saveMaster->hemolisa = $input['hemolisa'];
        $saveMaster->lembar = $input['lembar'];
        $saveMaster->faktorxv = $input['faktorxv'];
        $saveMaster->kebutuhan_oksigen = $input['kebutuhan_oksigen'];
        $saveMaster->vitek = $vitek;
        $saveMaster->api = $api;
        $saveMaster->api2 = $api2;
        $saveMaster->bd_phoenik = $bd_phoenik;
        $saveMaster->penanggung_jawab = $input['penanggung_jawab'];
        $saveMaster->spesies_auto = $input['spesies_auto'];
        $saveMaster->mrsa = $input['mrsa'];
        $saveMaster->esbl = $input['esbl'];
        $saveMaster->save();


        $saveHeaderID = $saveMaster->id;

        $i = 0;
        foreach ($input['fermentasinegatif'] as $alat) {
            if($alat != ''){
                $saveFermentasinegatif = new Fermentasinegatif;
                $saveFermentasinegatif->id_antibiotik = $saveHeaderID;
                $saveFermentasinegatif->id_fermentasi = $input['id_fermentasi'][$i];
                $saveFermentasinegatif->status = $input['fermentasinegatif'][$i];
                $saveFermentasinegatif->save();
            }
            $i++;
        }
        $i = 0;
        foreach ($input['fermentasipositif'] as $alat) {
            if($alat != ''){
                $saveFermentasinegatif = new Fermentasipositif;
                $saveFermentasinegatif->id_antibiotik = $saveHeaderID;
                $saveFermentasinegatif->id_fermentasi = $input['id_fermentasi_positif'][$i];
                $saveFermentasinegatif->status = $input['fermentasipositif'][$i];
                $saveFermentasinegatif->save();
            }
            $i++;
        }
        $i = 0;
        foreach ($input['id_jenis_antibiotik'] as $alat) {
            if($alat != ''){
                $saveKepekaan = new Kepekaan;
                $saveKepekaan->id_antibiotik = $saveHeaderID;
                $saveKepekaan->id_jenis_antibiotik = $input['id_jenis_antibiotik'][$i];
                $saveKepekaan->disk = $input['disk'][$i];
                $saveKepekaan->hasil1 = $input['hasil1'][$i];
                $saveKepekaan->mic = $input['mic'][$i];
                $saveKepekaan->hasil2 = $input['hasil2'][$i];
                $saveKepekaan->kesimpulan = $input['kesimpulan'][$i];
                $saveKepekaan->lain_lain = $input['lain_lain'][$i];
                $saveKepekaan->save();
            }
            $i++;
        }
        if ($input['lembar'] == '3') {
            if ($siklus == '1') {
                Register::where('id',$id)->update(['siklus_1'=>'done', 'rpr1'=>'done', 'status_data1'=>'1']);
                Register::where('id',$id)->update(['pemeriksaan'=>'done']);
            }else{
                Register::where('id',$id)->update(['siklus_2'=>'done', 'rpr2'=>'done', 'status_data2'=>'1']);
                Register::where('id',$id)->update(['pemeriksaan2'=>'done']);
            }
        }

        return redirect('hasil-pemeriksaan');
        }
    }

    public function edit(\Illuminate\Http\Request $request, $id){
        $type = $request->get('x');
        $siklus = $request->get('y');
        $lembar = $request->get('lembar');
        if(empty($lembar)){
            $lembar = 1;
        }
        $date = date('Y');

        $data = DB::table('data_antibiotik')->where('id_registrasi', $id)->where('siklus', $siklus)->where('lembar', '=', $lembar)->get();
        foreach ($data as $key => $val) {
            $fermentasinegatif = DB::table('tb_fermentasi_negatif')
                                ->leftjoin('bakteri_fermentasi', 'tb_fermentasi_negatif.id_fermentasi', '=', 'bakteri_fermentasi.id')
                                ->join('data_antibiotik', 'tb_fermentasi_negatif.id_antibiotik', '=', 'data_antibiotik.id')
                                ->where('data_antibiotik.id', $val->id)
                                ->get();
            $val->fermentasinegatif = $fermentasinegatif;

            $fermentasipositif = DB::table('tb_fermentasi_positif')
                                ->leftjoin('bakteri_fermentasi', 'tb_fermentasi_positif.id_fermentasi', '=', 'bakteri_fermentasi.id')
                                ->join('data_antibiotik', 'tb_fermentasi_positif.id_antibiotik', '=', 'data_antibiotik.id')
                                ->where('data_antibiotik.id', $val->id)
                                ->get();
            $val->fermentasipositif = $fermentasipositif;

            $kepekaan = DB::table('tb_kepekaan')
                                ->leftjoin('antibiotik', 'tb_kepekaan.id_jenis_antibiotik', '=', 'antibiotik.id')
                                ->join('data_antibiotik', 'tb_kepekaan.id_antibiotik', '=', 'data_antibiotik.id')
                                ->where('data_antibiotik.id', $val->id)
                                ->select('tb_kepekaan.*', 'antibiotik.antibiotik')
                                ->get();
            $val->kepekaan = $kepekaan;
        }
        $peka = DB::table('data_peka_antibiotik')->where('id_registrasi', $id)->where('siklus', $siklus)->where('lembar', '=', $lembar)->first();
        // dd($data[0]->kepekaan);

        $register = Register::find($id);
        $perusahaan = $register->perusahaan->nama_lab;
        
        return view('edit_hasil/antibiotik', compact('data', 'perusahaan','peka', 'type','siklus','date'));
    }

    public function update(\Illuminate\Http\Request $request,$id){
        $input = $request->all();
        $siklus = $request->get('y');
        $register = Register::find($id);
        $perusahaanID = $register->perusahaan->id;
        // dd($input);

        $media_digunakan = implode(", ",$input['media_digunakan']);
        $vitek = implode(", ",$input['vitek']);
        $api = implode(", ",$input['api']);
        $api2 = implode(", ",$input['api2']);
        $bd_phoenik = implode(", ",$input['bd_phoenik']);
        $standart = implode(", ",$input['standart']);
        $metode = implode(", ",$input['metode']);

        // dd($media_digunakan);
        $savePeka = Datapekaantibiotik::find($input['id_peka']);
        $savePeka->id_registrasi = $id;
        $savePeka->hasil_identifikasi = $input['hasil_identifikasi'];
        $savePeka->standart = $standart;
        $savePeka->metode = $metode;
        $savePeka->siklus = $input['siklus'];
        $savePeka->lembar = $input['lembar'];
        $savePeka->save();

        $saveMaster = Dataantibiotik::find($input['id_bakteri']);
        $saveMaster->kode_lab = $input['kode_peserta'];
        $saveMaster->id_registrasi = $id;
        $saveMaster->kode_lab = $input['kode_peserta'];
        $saveMaster->nama_instansi = $input['nama_instansi'];
        $saveMaster->alamat = $input['alamat'];
        $saveMaster->kondisi = $input['kondisi'];
        $saveMaster->tgl_diterima = $input['tgl_diterima'];
        $saveMaster->nama_pemeriksa = $input['nama_pemeriksa'];
        $saveMaster->no_hp = $input['no_hp'];
        $saveMaster->kd_bahan = $input['kode_bahan'];
        $saveMaster->siklus = $input['siklus'];
        $saveMaster->jenis_bahan = $input['jenis_bahan'];
        $saveMaster->tgl_pelaksanaan = $input['tgl_pelaksanaan'];
        $saveMaster->metode_konvensional = $input['metode_konvensional'];
        $saveMaster->metode_otomatis = $input['metode_otomatis'];
        $saveMaster->metode_lainnya = $input['metode_lainnya'];
        $saveMaster->test_metode_identifikasi = $input['metodetext1'];
        $saveMaster->media_digunakan = $media_digunakan;
        $saveMaster->buatan_sendiri = $input['media_buatan'];           
        $saveMaster->media_komersial = $input['media_komersial'];  
        $saveMaster->fermentasilainn = $input['fermentasilainn'];  
        $saveMaster->fermentasilainp = $input['fermentasilainp'];
        $saveMaster->hasil_kultur = $input['hasil_kultur'];
        $saveMaster->spesies_kultur = $input['spesies_kultur'];
        $saveMaster->pewarnaan_gram_p = $input['pewarnaan_gram_p'];
        $saveMaster->pewarnaan_gram_n = $input['pewarnaan_gram_n'];
        $saveMaster->pewarnaan_gram_y = $input['pewarnaan_gram_y'];
        $saveMaster->hemolisa = $input['hemolisa'];
        $saveMaster->lembar = $input['lembar'];
        $saveMaster->faktorxv = $input['faktorxv'];
        $saveMaster->kebutuhan_oksigen = $input['kebutuhan_oksigen'];
        $saveMaster->vitek = $vitek;
        $saveMaster->api = $api;
        $saveMaster->api2 = $api2;
        $saveMaster->bd_phoenik = $bd_phoenik;
        $saveMaster->penanggung_jawab = $input['penanggung_jawab'];
        $saveMaster->spesies_auto = $input['spesies_auto'];
        $saveMaster->mrsa = $input['mrsa'];
        $saveMaster->esbl = $input['esbl'];
        $saveMaster->save();


        $saveHeaderID = $saveMaster->id;

        $i = 0;
        Fermentasinegatif::where('id_antibiotik','=',$saveHeaderID)->delete();
        foreach ($input['fermentasinegatif'] as $alat) {
            if($alat != ''){
                $saveFermentasinegatif = new Fermentasinegatif;
                $saveFermentasinegatif->id_antibiotik = $saveHeaderID;
                $saveFermentasinegatif->id_fermentasi = $input['id_fermentasi'][$i];
                $saveFermentasinegatif->status = $input['fermentasinegatif'][$i];
                $saveFermentasinegatif->save();
            }
            $i++;
        }
        $i = 0;
        Fermentasipositif::where('id_antibiotik','=',$saveHeaderID)->delete();
        foreach ($input['fermentasipositif'] as $alat) {
            if($alat != ''){
                $saveFermentasinegatif = new Fermentasipositif;
                $saveFermentasinegatif->id_antibiotik = $saveHeaderID;
                $saveFermentasinegatif->id_fermentasi = $input['id_fermentasi_positif'][$i];
                $saveFermentasinegatif->status = $input['fermentasipositif'][$i];
                $saveFermentasinegatif->save();
            }
            $i++;
        }
        $i = 0;
        Kepekaan::where('id_antibiotik','=',$saveHeaderID)->delete();
        foreach ($input['id_jenis_antibiotik'] as $alat) {
            if($alat != ''){
                $saveKepekaan = new Kepekaan;
                $saveKepekaan->id_antibiotik = $saveHeaderID;
                $saveKepekaan->id_jenis_antibiotik = $input['id_jenis_antibiotik'][$i];
                $saveKepekaan->disk = $input['disk'][$i];
                $saveKepekaan->hasil1 = $input['hasil1'][$i];
                $saveKepekaan->mic = $input['mic'][$i];
                $saveKepekaan->hasil2 = $input['hasil2'][$i];
                $saveKepekaan->kesimpulan = $input['kesimpulan'][$i];
                $saveKepekaan->lain_lain = $input['lain_lain'][$i];
                $saveKepekaan->save();
            }
            $i++;
        }

        if ($request->simpan == "Kirim") {
            if ($input['lembar'] == '3') {
                if ($siklus == '1') {
                    Register::where('id',$id)->update(['siklus_1'=>'done', 'rpr1'=>'done', 'status_data1'=>'2']);
                    Register::where('id',$id)->update(['pemeriksaan'=>'done']);
                }else{
                    Register::where('id',$id)->update(['siklus_2'=>'done', 'rpr2'=>'done', 'status_data2'=>'2']);
                    Register::where('id',$id)->update(['pemeriksaan2'=>'done']);
                }
                return redirect('edit-hasil');
            }
            else{
                $next = $input['lembar'] + 1;
                return redirect('hasil-pemeriksaan/antibiotik/edit/'.$id.'?y='.$siklus.'&lembar='.$next);
            }
        }else{
            if ($input['lembar'] == '3') {
                return redirect('edit-hasil');
            }else{
                $next = $input['lembar'] + 1;
                return redirect('hasil-pemeriksaan/antibiotik/edit/'.$id.'?y='.$siklus.'&lembar='.$next);
            }
        }
    }
}
