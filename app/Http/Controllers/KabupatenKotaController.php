<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\kota as Kota;
use Illuminate\Support\Facades\Redirect;

class KabupatenKotaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = DB::table('regencies')
                ->join('provinces', 'regencies.province_id', '=', 'provinces.id')
                ->select('regencies.*', 'provinces.name as Name')
                ->get();
        return view('back/kota/index', compact('data'));
    }

    public function in()
    {
        $data = DB::table('provinces')->get();
        return view('back/kota/insert', compact('data'));
    }

    public function edit($id)
    {
        $data = DB::table('regencies')
                ->join('provinces', 'regencies.province_id', '=', 'provinces.id')
                ->select('regencies.*', 'provinces.name as Name', 'provinces.id as Id')
                ->where('regencies.id', $id)
                ->get();
        $provinsi = DB::table('provinces')->get();
        return view('back/kota/update', compact('data','provinsi'));
    }

    public function update(\Illuminate\Http\Request $request, $id)
    {
        $data['name'] = $request->name;
        Kota::where('id',$id)->update($data);
        Session::flash('message', 'Data Kota Berhasil Diubah!'); 
        Session::flash('alert-class', 'alert-success'); 
  
        return redirect('admin/kabupaten-kota');
    }    

    public function insert(\Illuminate\Http\Request $request)
    {
        $data = Request::all();
        Kota::create($data);
        Session::flash('message', 'Data Kabupaten / Kota Berhasil Ditambahkan!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/kabupaten-kota');
    }

    public function delete($id){
        Session::flash('message', 'Data Kota Telah Dihapus!'); 
        Session::flash('alert-class', 'alert-warning'); 
        Kota::find($id)->delete();
        return redirect("admin/kabupaten-kota");
    }
}
