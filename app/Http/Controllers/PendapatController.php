<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;
use Session;

use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\pendapat as Pendapat;
use App\responden as Responden;
use App\saran as Saran;
use App\TandaTerima;
use Redirect;
use Validator;
use Excel;

class PendapatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {   
        $tahun = date('Y');
        $user = Auth::user()->id;
        $pendapat = DB::table('tb_pendapat_responden')->where('tahun', $tahun)->get();
        $perusahaan = DB::table('perusahaan')->where('created_by', $user)->first();
        return view('pendapat/pendapat', compact('perusahaan', 'tahun','pendapat'));
    }

    public function saran()
    {   
        $tahun = date('Y');
        $user = Auth::user()->id;
        $pendapat = DB::table('tb_saran')->where('tahun', $tahun)->get();
        $perusahaan = DB::table('perusahaan')->where('created_by', $user)->first();
        // dd($perusahaan);
        return view('pendapat/saran', compact('perusahaan', 'tahun','pendapat'));
    }

    public function insertsaran(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $tahun = date('Y');
        $user = Auth::user()->id;
        $perusahaan = DB::table('perusahaan')->where('created_by', $user)->first();
        // dd($input);
        $SaveSaran = new Saran;
        $SaveSaran->id_users = $user;
        $SaveSaran->parameter = $input['parameter'];
        $SaveSaran->saran = $input['saran'];    
        $SaveSaran->tahun = $tahun;
        $SaveSaran->siklus = $input['siklus'];    
        $SaveSaran->save();

        Session::flash('message', 'Saran berhasil terkirim!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('index');
    }

    public function insert(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $tahun = date('Y');
        $user = Auth::user()->id;
        $perusahaan = DB::table('perusahaan')->where('created_by', $user)->first();
        // dd($input);
        $SaveMaster = new Responden;
        $SaveMaster->id_users = $user;
        $SaveMaster->siklus = $input['siklus'];
        $SaveMaster->tahun = $tahun;
        $SaveMaster->save();

        $SaveMasterId = $SaveMaster->id;

        $SavePendapat = new Pendapat;
        $SavePendapat->id_responden = $SaveMasterId;
        $SavePendapat->no_1 = $input['no_1'];
        $SavePendapat->no_2 = $input['no_2'];
        $SavePendapat->no_3 = $input['no_3'];
        $SavePendapat->no_4 = $input['no_4'];
        $SavePendapat->no_5 = $input['no_5'];
        $SavePendapat->no_6 = $input['no_6'];
        $SavePendapat->no_7 = $input['no_7'];
        $SavePendapat->no_8 = $input['no_8'];
        $SavePendapat->no_9 = $input['no_9'];
        $SavePendapat->no_10 = $input['no_10'];
        $SavePendapat->no_11 = $input['no_11'];
        $SavePendapat->no_12 = $input['no_12'];
        $SavePendapat->no_13 = $input['no_13'];
        $SavePendapat->no_14 = $input['no_14'];
        $SavePendapat->no_15 = $input['no_15'];
        $SavePendapat->no_16 = $input['no_16'];
        $SavePendapat->no_17 = $input['no_17'];
        $SavePendapat->no_18 = $input['no_18'];
        $SavePendapat->no_19 = $input['no_19'];
        $SavePendapat->no_20 = $input['no_20'];
        $SavePendapat->save();

        Session::flash('message', 'Pendapat Responden berhasil terkirim!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('index');
    }

    public function viewtanda(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = TandaTerima::select( DB::raw('@rownum := @rownum +1 as rownum, tb_tanda_terima.*,perusahaan.nama_lab,perusahaan.alamat,tb_registrasi.created_by'))
                    ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_tanda_terima.id_registrasi')
                    ->join('users', 'tb_registrasi.created_by','=','users.id')
                    ->join('perusahaan','users.id','=','perusahaan.created_by')
                    ->groupBy('tb_registrasi.perusahaan_id');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="tanda-terima/'.$data->created_by.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        View
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('laporan.tanda-terima.laporan.index');
    }

    public function tandapeserta($id)
    {
        $data = DB::table('tb_tanda_terima')
                ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_tanda_terima.id_registrasi')
                ->groupBy('tb_registrasi.created_by', 'tb_tanda_terima.siklus', 'tb_tanda_terima.tanggal')
                ->where('tb_registrasi.created_by', $id)
                ->select('tb_tanda_terima.*')
                ->get();
        return view('laporan.tanda-terima.laporan.proses', compact('data'));
    }

    public function lihattanda($id)
    {
        $datas = DB::table('tb_tanda_terima')
                ->join('tb_registrasi', 'tb_registrasi.id', '=', 'tb_tanda_terima.id_registrasi')
                ->groupBy('tb_registrasi.created_by', 'tb_tanda_terima.siklus', 'tb_tanda_terima.tanggal')
                ->where('tb_tanda_terima.id', $id)
                ->select('tb_tanda_terima.*', 'tb_registrasi.created_by', 'tb_registrasi.perusahaan_id')
                ->first();
        // dd($datas);
        $perusahaan = DB::table('perusahaan')->where('created_by', $datas->created_by)->first();
        $data = DB::table('tb_registrasi')
                    ->join('sub_bidang', 'tb_registrasi.bidang','=','sub_bidang.id')
                    ->where('perusahaan_id', $datas->perusahaan_id)
                    ->where(function ($query) use ($datas){
                        if($datas->siklus == 1){
                            $query->whereIn('tb_registrasi.siklus', ['1','12']);
                        }else{
                            $query->whereIn('tb_registrasi.siklus', ['2','12']);
                        }
                    })
                    ->select('tb_registrasi.*', 'sub_bidang.parameter')
                    ->get();
        // dd()
        foreach ($data as $key => $val) {
            $tanda = DB::table('tb_tanda_terima')
                        ->where('id_registrasi', $val->id)
                        ->where('siklus', $datas->siklus)
                        ->where('tanggal', '=', $datas->tanggal)
                        ->first();
            $val->penerima = $tanda;
        }
        return view('laporan.tanda-terima.laporan.view', compact('data', 'datas', 'perusahaan'));
    }

    public function viewsaran(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Saran::select( DB::raw('@rownum := @rownum +1 as rownum, tb_saran.*,perusahaan.nama_lab,perusahaan.alamat'))
                    ->join('users', 'tb_saran.id_users','=','users.id')
                    ->join('perusahaan','users.id','=','perusahaan.created_by');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="jawab/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Jawab
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('pendapat.index');
    }

    public function jawabsaran($id)
    {   
        $tahun = date('Y');
        $pendapat = DB::table('tb_saran')->where('id', $id)->first();
        $perusahaan = DB::table('perusahaan')->where('created_by', $pendapat->id_users)->first();
        // dd($pendapat);
        return view('pendapat/jawab', compact('perusahaan', 'tahun','pendapat'));
    }

    public function injawabsaran(\Illuminate\Http\Request $request, $id)
    {
        $input = $request->all();
        $tahun = date('Y');
        $user = Auth::user()->id;
        $perusahaan = DB::table('perusahaan')->where('created_by', $user)->first();
        // dd($input);
        $SaveSaran['jawaban'] = $request->jawaban;
        Saran::where('id',$id)->update($SaveSaran);

        Session::flash('message', 'Jawaban Saran berhasil terkirim!'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('index');
    }

    public function jawabansaran(\Illuminate\Http\Request $request)
    {
        $user = Auth::user()->id;
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0")); 
            $datas = Saran::select( DB::raw('@rownum := @rownum +1 as rownum, tb_saran.*,perusahaan.nama_lab,perusahaan.alamat'))
                    ->join('users', 'tb_saran.id_users','=','users.id')
                    ->join('perusahaan','users.id','=','perusahaan.created_by')
                    ->where('users.id',$user);
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="keluh-saran/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Lihat
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('pendapat.datauser');
    }

    public function datajawab(\Illuminate\Http\Request $request, $id)
    {
        $tahun = date('Y');
        $pendapat = DB::table('tb_saran')->where('id', $id)->first();
        $perusahaan = DB::table('perusahaan')->where('created_by', $pendapat->id_users)->first();
        // dd($perusahaan);
        return view('pendapat/datajawab', compact('perusahaan', 'tahun','pendapat'));
    }

    public function rekaptanda()
    {
        return view('laporan.tanda-terima.rekap.index');
    }

    public function printrekaptanda(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('perusahaan')
                ->join('tb_registrasi', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->join('tb_tanda_terima', 'tb_tanda_terima.id_registrasi', '=', 'tb_registrasi.id')
                ->where('tb_tanda_terima.siklus', $input['siklus'])
                ->whereYear('tb_tanda_terima.tanggal', $input['tahun'])
                ->groupBy('perusahaan.id')
                ->select('perusahaan.*', 'tb_tanda_terima.penerima', 'tb_tanda_terima.tanggal')
                ->get();
        // dd($data);
        foreach ($data as $key => $val) {
            $hem = DB::table('tb_registrasi')
                        ->join('sub_bidang', 'tb_registrasi.bidang','=','sub_bidang.id')
                        ->join('tb_tanda_terima', 'tb_tanda_terima.id_registrasi', 'tb_registrasi.id')
                        ->where('perusahaan_id', $val->id)
                        ->where('tb_registrasi.bidang', '=', '1')
                        ->where('tb_tanda_terima.siklus', $input['siklus'])
                        ->whereYear('tb_tanda_terima.tanggal', $input['tahun'])
                        ->select('tb_registrasi.*', 'sub_bidang.parameter', 'tb_tanda_terima.jumlah', 'tb_tanda_terima.kondisi', 'tb_tanda_terima.keterangan')
                        ->orderBy('tb_registrasi.bidang', 'asc')
                        ->first();
            $val->hem = $hem;
            $kk = DB::table('tb_registrasi')
                        ->join('sub_bidang', 'tb_registrasi.bidang','=','sub_bidang.id')
                        ->join('tb_tanda_terima', 'tb_tanda_terima.id_registrasi', 'tb_registrasi.id')
                        ->where('perusahaan_id', $val->id)
                        ->where('tb_registrasi.bidang', '=', '2')
                        ->where('tb_tanda_terima.siklus', $input['siklus'])
                        ->whereYear('tb_tanda_terima.tanggal', $input['tahun'])
                        ->select('tb_registrasi.*', 'sub_bidang.parameter', 'tb_tanda_terima.jumlah', 'tb_tanda_terima.kondisi', 'tb_tanda_terima.keterangan')
                        ->orderBy('tb_registrasi.bidang', 'asc')
                        ->first();
            $val->kk = $kk;
            $uri = DB::table('tb_registrasi')
                        ->join('sub_bidang', 'tb_registrasi.bidang','=','sub_bidang.id')
                        ->join('tb_tanda_terima', 'tb_tanda_terima.id_registrasi', 'tb_registrasi.id')
                        ->where('perusahaan_id', $val->id)
                        ->where('tb_registrasi.bidang', '=', '3')
                        ->where('tb_tanda_terima.siklus', $input['siklus'])
                        ->whereYear('tb_tanda_terima.tanggal', $input['tahun'])
                        ->select('tb_registrasi.*', 'sub_bidang.parameter', 'tb_tanda_terima.jumlah', 'tb_tanda_terima.kondisi', 'tb_tanda_terima.keterangan')
                        ->orderBy('tb_registrasi.bidang', 'asc')
                        ->first();
            $val->uri = $uri;
            $bta = DB::table('tb_registrasi')
                        ->join('sub_bidang', 'tb_registrasi.bidang','=','sub_bidang.id')
                        ->join('tb_tanda_terima', 'tb_tanda_terima.id_registrasi', 'tb_registrasi.id')
                        ->where('perusahaan_id', $val->id)
                        ->where('tb_registrasi.bidang', '=', '4')
                        ->where('tb_tanda_terima.siklus', $input['siklus'])
                        ->whereYear('tb_tanda_terima.tanggal', $input['tahun'])
                        ->select('tb_registrasi.*', 'sub_bidang.parameter', 'tb_tanda_terima.jumlah', 'tb_tanda_terima.kondisi', 'tb_tanda_terima.keterangan')
                        ->orderBy('tb_registrasi.bidang', 'asc')
                        ->first();
            $val->bta = $bta;
            $tc = DB::table('tb_registrasi')
                        ->join('sub_bidang', 'tb_registrasi.bidang','=','sub_bidang.id')
                        ->join('tb_tanda_terima', 'tb_tanda_terima.id_registrasi', 'tb_registrasi.id')
                        ->where('perusahaan_id', $val->id)
                        ->where('tb_registrasi.bidang', '=', '5')
                        ->where('tb_tanda_terima.siklus', $input['siklus'])
                        ->whereYear('tb_tanda_terima.tanggal', $input['tahun'])
                        ->select('tb_registrasi.*', 'sub_bidang.parameter', 'tb_tanda_terima.jumlah', 'tb_tanda_terima.kondisi', 'tb_tanda_terima.keterangan')
                        ->orderBy('tb_registrasi.bidang', 'asc')
                        ->first();
            $val->tc = $tc;
            $hiv = DB::table('tb_registrasi')
                        ->join('sub_bidang', 'tb_registrasi.bidang','=','sub_bidang.id')
                        ->join('tb_tanda_terima', 'tb_tanda_terima.id_registrasi', 'tb_registrasi.id')
                        ->where('perusahaan_id', $val->id)
                        ->where('tb_registrasi.bidang', '=', '6')
                        ->where('tb_tanda_terima.siklus', $input['siklus'])
                        ->whereYear('tb_tanda_terima.tanggal', $input['tahun'])
                        ->select('tb_registrasi.*', 'sub_bidang.parameter', 'tb_tanda_terima.jumlah', 'tb_tanda_terima.kondisi', 'tb_tanda_terima.keterangan')
                        ->orderBy('tb_registrasi.bidang', 'asc')
                        ->first();
            $val->hiv = $hiv;
            $syp = DB::table('tb_registrasi')
                        ->join('sub_bidang', 'tb_registrasi.bidang','=','sub_bidang.id')
                        ->join('tb_tanda_terima', 'tb_tanda_terima.id_registrasi', 'tb_registrasi.id')
                        ->where('perusahaan_id', $val->id)
                        ->where('tb_registrasi.bidang', '=', '7')
                        ->where('tb_tanda_terima.siklus', $input['siklus'])
                        ->whereYear('tb_tanda_terima.tanggal', $input['tahun'])
                        ->select('tb_registrasi.*', 'sub_bidang.parameter', 'tb_tanda_terima.jumlah', 'tb_tanda_terima.kondisi', 'tb_tanda_terima.keterangan')
                        ->orderBy('tb_registrasi.bidang', 'asc')
                        ->first();
            $val->syp = $syp;
            $hbs = DB::table('tb_registrasi')
                        ->join('sub_bidang', 'tb_registrasi.bidang','=','sub_bidang.id')
                        ->join('tb_tanda_terima', 'tb_tanda_terima.id_registrasi', 'tb_registrasi.id')
                        ->where('perusahaan_id', $val->id)
                        ->where('tb_registrasi.bidang', '=', '8')
                        ->where('tb_tanda_terima.siklus', $input['siklus'])
                        ->whereYear('tb_tanda_terima.tanggal', $input['tahun'])
                        ->select('tb_registrasi.*', 'sub_bidang.parameter', 'tb_tanda_terima.jumlah', 'tb_tanda_terima.kondisi', 'tb_tanda_terima.keterangan')
                        ->orderBy('tb_registrasi.bidang', 'asc')
                        ->first();
            $val->hbs = $hbs;
            $hcv = DB::table('tb_registrasi')
                        ->join('sub_bidang', 'tb_registrasi.bidang','=','sub_bidang.id')
                        ->join('tb_tanda_terima', 'tb_tanda_terima.id_registrasi', 'tb_registrasi.id')
                        ->where('perusahaan_id', $val->id)
                        ->where('tb_registrasi.bidang', '=', '9')
                        ->where('tb_tanda_terima.siklus', $input['siklus'])
                        ->whereYear('tb_tanda_terima.tanggal', $input['tahun'])
                        ->select('tb_registrasi.*', 'sub_bidang.parameter', 'tb_tanda_terima.jumlah', 'tb_tanda_terima.kondisi', 'tb_tanda_terima.keterangan')
                        ->orderBy('tb_registrasi.bidang', 'asc')
                        ->first();
            $val->hcv = $hcv;
        }
        // dd($data);
        Excel::create('Rekap Data Peserta Tanda Terima Bahan Kontrol', function($excel) use ($data, $input) {
            $excel->sheet('Data', function($sheet) use ($data, $input) {
                $sheet->loadView('laporan.tanda-terima.rekap.view', array('data'=>$data, 'input'=>$input) );
            });
        })->download('xls');
        // return view('laporan.tanda-terima.rekap.view', compact('data', 'input'));
    }
}