<?php

namespace App\Http\Controllers;

use DB;
use Request;
use Auth;
use Input;
use PDF;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\register as Register;
use App\kirimbahan as Kirim;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;

class KirimBahanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user()->penyelenggara;
        $data = DB::table('sub_bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->join('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                ->leftjoin('districts', 'perusahaan.kecamatan', 'districts.id')
                ->leftjoin('villages', 'perusahaan.kelurahan', 'villages.id')
                ->leftjoin('users', 'perusahaan.created_by', '=', 'users.id')
                ->select('perusahaan.*','tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.tarif as Tarif', 'tb_bidang.bidang as Bidang', 'users.name as Name', 'users.email', 'provinces.name as Provinsi', 'regencies.name as Kota', 'districts.name as Kecamatan', 'villages.name as Kelurahan')
                ->where('tb_registrasi.status', '2')
                ->orderBy('Name', 'asc')
                ->paginate(20);
        return view('kirim_bahan', compact('data'));
    }
    public function proses(\Illuminate\Http\Request $request){

        $bidang = $request->bidang;
        $jumlah_dipilih = count($bidang);
        $date = date('Y-m-d');
        // $datas1 = $request->get('jenis_barang');
        // $datas2 = $request->get('jumlah');
        // dd($bidang);
        for($x=0;$x<$jumlah_dipilih;$x++){
            $data['status'] = '3';
            $i= 0;
            foreach ($bidang as $key => $valb) {
                $kirim['id_registrasi'] = $valb;
                $kirim['created_by'] = Auth::user()->id;
                Register::where('id', $valb)->update($data);
                Kirim::create($kirim);
                $i++;
            }

            $email = DB::table('users')
                    ->leftjoin('tb_registrasi', 'users.id', '=', 'tb_registrasi.created_by')
                    ->leftjoin('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                    ->leftjoin('sub_bidang', 'tb_registrasi.bidang', '=', 'sub_bidang.id')
                    ->select('users.email', 'tb_registrasi.siklus', 'sub_bidang.parameter as parameter', 'perusahaan.nama_lab', 'tb_registrasi.kode_lebpes')
                    ->whereIn('tb_registrasi.id', ($bidang))
                    ->get();
                    // dd($email); 
            $datu = [
                    'labklinik' => $email[0]->nama_lab,
                    'kode_lebpes' => $email[0]->kode_lebpes,
                    'date' => date('Y'),
                    'tanggal' => $date,
                    'parameter' => $email
                    ];
            Mail::send('email.kirim_bahan', $datu, function ($mail) use ($email)
            {
              $mail->from('sipamela.bblkjkt@gmail.com', 'BBLK Jakarta Bahan Uji '.$email[0]->nama_lab);
              // $mail->to('tengkufirmansyah2@gmail.com');
              $mail->to($email[0]->email);
              $mail->subject('Kirim Bahan PNPME');
            });
             
        return redirect('kirim-bahan');
            $detail = DB::table('sub_bidang')
                    ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                    ->join('tb_registrasi', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                    ->join('perusahaan', 'tb_registrasi.perusahaan_id', '=', 'perusahaan.id')
                    ->leftjoin('provinces', 'perusahaan.provinsi', '=', 'provinces.id')
                    ->leftjoin('regencies', 'perusahaan.kota', '=', 'regencies.id')
                    ->leftjoin('districts', 'perusahaan.kecamatan', 'districts.id')
                    ->leftjoin('villages', 'perusahaan.kelurahan', 'villages.id')
                    ->leftjoin('users', 'perusahaan.created_by', '=', 'users.id')
                    ->select('perusahaan.*','tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.tarif as Tarif', 'tb_bidang.bidang as Bidang', 'users.name as Name', 'users.email', 'provinces.name as Provinsi', 'regencies.name as Kota', 'districts.name as Kecamatan', 'villages.name as Kelurahan')
                    ->where('tb_registrasi.id', $bidang[$x])
                    ->first();

        $pdf = PDF::loadview('cetak_hasil/kirim_bahan', compact('detail', 'datas1', 'datas2', 'date'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);

        return $pdf->stream('Kuitansi Kirim Bahan.pdf');
        }
    }
}
