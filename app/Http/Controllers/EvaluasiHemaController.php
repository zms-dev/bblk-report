<?php

namespace App\Http\Controllers;
use DB;
use Session;
use App\User;
use App\SdMedian;
use App\SdMedianMetode;
use App\Ring;
use App\BatasParameter;
use App\BatasMetode;
use App\RingKK;
use App\RingKKAlat;
use App\RingKKMetode;
use App\SdMedianAlat;
use App\daftar as Daftar;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\register as Register;
use Excel;
use PDF;
use App\JobsEvaluasi;
use App\SertfikatInsert;
use App\TanggalEvaluasi;

class EvaluasiHemaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

     public function hemazscorelaporan()
     {
         return view('evaluasi.hematologi.laporanzscorehasilpeserta.index');
     }

     public function hemazscorelaporanna(\Illuminate\Http\Request $request)
     {
         $input = $request->all();
         $parameterna = DB::table('parameter')
                         ->where('kategori', 'hematologi')
                         ->orderBy('bagian', 'asc')
                         ->get();
         $data = DB::table('hp_headers')
                 ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                 ->where('tb_registrasi.bidang', '=', '1')
                 ->where('hp_headers.siklus', $input['siklus'])
                 ->where('hp_headers.type', $input['tipe'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                 ->select('hp_headers.kode_lab','hp_headers.id','hp_headers.id_registrasi', 'tb_registrasi.updated_at')
                 ->get();
         foreach ($data as $key => $val) {
             $parameter = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
             foreach ($parameter as $key => $par) {
                 $detail = DB::table('tb_zscore')
                             ->where('id_registrasi', $val->id_registrasi)
                             ->where('parameter', $par->id)
                             ->where('type',$input['tipe'])
                             ->where('siklus', $input['siklus'])
                             ->limit('1')
                             ->get();
                 $par->detail = $detail;
             }
             $val->parameter = $parameter;
         }
         // dd($val);

         // dd($data[0]);
         // return view('evaluasi.hematologi.laporanhasilpeserta.view', compact('data', 'parameterna'));
         Excel::create('Z-Score Peserta Per Parameter Hematologi Siklus '.$input['siklus'].' Tahun '.$input['tahun'], function($excel) use ($data, $parameterna) {
             $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                 $sheet->loadView('evaluasi.hematologi.laporanzscorehasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
             });
         })->download('xls');
     }

     public function airzscorelaporan()
     {
         return view('evaluasi.kimiaair.laporanzscorehasilpeserta.index');
     }

     public function airzscorelaporanna(\Illuminate\Http\Request $request)
     {
         $input = $request->all();
         $parameterna = DB::table('parameter')
                         ->where('kategori', 'kimia air')
                         ->orderBy('bagian', 'asc')
                         ->get();
         $data = DB::table('hp_headers')
                 ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                 ->where('tb_registrasi.bidang', '=', '11')
                 ->where('hp_headers.siklus', $input['siklus'])
                 ->select('hp_headers.kode_lab','hp_headers.id','hp_headers.id_registrasi', 'tb_registrasi.updated_at')
                 ->get();
         foreach ($data as $key => $val) {
             $parameter = DB::table('parameter')->where('kategori', 'kimia air')->orderBy('bagian', 'asc')->get();
             foreach ($parameter as $key => $par) {
                 $detail = DB::table('tb_zscore')
                             ->where('id_registrasi', $val->id_registrasi)
                             ->where('parameter', $par->id)
                             ->limit('1')
                             ->get();
                 $par->detail = $detail;
             }
             $val->parameter = $parameter;
         }
         // dd($val);

         // dd($data);
         // return view('evaluasi.hematologi.laporanhasilpeserta.view', compact('data', 'parameterna'));
         Excel::create('Z-Score Peserta Per Parameter Kimia Air', function($excel) use ($data, $parameterna) {
             $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                 $sheet->loadView('evaluasi.kimiaair.laporanzscorehasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
             });
         })->download('xls');
     }

     public function airtzscorelaporan()
     {
         return view('evaluasi.kimiaairterbatas.laporanzscorehasilpeserta.index');
     }

     public function airtzscorelaporannilai()
     {
         return view('evaluasi.kimiaairterbatas.laporanzscorehasilpeserta.nilai');
     }

    public function airtzscorelaporannilaina(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $type = $request->get('x');
        $siklus = $request->siklus;
        $date = date('Y');

        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', 'a')
                ->where('parameter.kategori','=','kimia air terbatas')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.tanggal_mulai', 'hp_details.tanggal_selesai', 'hp_details.alat', 'hp_details.kode_metode_pemeriksaan', 'hp_details.hasil_pemeriksaan')
                ->get();
        // dd($data);
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('form', '=', 'kimia air terbatas')
                    ->get();
            $val->sd = $sd;
        }

        $zscore = DB::table('tb_zscore')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('form','=','kimia air terbatas')
                    ->get();

        
        return view('evaluasi.kimiaairterbatas.laporanzscorehasilpeserta.graf', compact('data','sub','zscore','zscore1'));
    }

    public function airtzscorelaporannilaiair()
     {
         return view('evaluasi.kimiaair.laporanzscorehasilpeserta.nilaiair');
     }

    public function airtzscorelaporannilainaair(\Illuminate\Http\Request $request)
    {   
         $type = $request->get('x');
        $siklus = $request->siklus;
        $date = date('Y');

        $input = $request->all();
        $data = DB::table('parameter')
                ->join('hp_details','hp_details.parameter_id','=','parameter.id')
                ->join('hp_headers','hp_headers.id','=','hp_details.hp_header_id')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('hp_headers.siklus', $siklus)
                ->where('hp_headers.type', 'a')
                ->where('parameter.kategori','=','kimia air')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $date)
                ->select('hp_headers.kode_lab', 'parameter.nama_parameter', 'parameter.id', 'hp_details.tanggal_mulai', 'hp_details.tanggal_selesai', 'hp_details.alat', 'hp_details.kode_metode_pemeriksaan', 'hp_details.hasil_pemeriksaan')
                ->get();
        // dd($data);
        foreach ($data as $key => $val) {
            $sd = DB::table('tb_sd_median')
                    ->where('parameter', $val->id)
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('form', '=', 'kimia air')
                    ->get();
            $val->sd = $sd;
        }
        $zscore = DB::table('tb_zscore')
                    ->where('siklus', $siklus)
                    ->where('tahun', $date)
                    ->where('form','=','kimia air')
                    ->get();
         // dd($zscore);
        
        return view('evaluasi.kimiaair.laporanzscorehasilpeserta.grafair', compact('data','sub','zscore','zscore1'));
    }




     public function airtzscorelaporanna(\Illuminate\Http\Request $request)
     {
         $input = $request->all();
         $parameterna = DB::table('parameter')
                         ->where('kategori', 'kimia air terbatas')
                         ->orderBy('bagian', 'asc')
                         ->get();
         $data = DB::table('hp_headers')
                 ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                 ->where('tb_registrasi.bidang', '=', '12')
                 ->where('hp_headers.siklus', $input['siklus'])
                 ->select('hp_headers.kode_lab','hp_headers.id','hp_headers.id_registrasi', 'tb_registrasi.updated_at')
                 ->get();
         foreach ($data as $key => $val) {
             $parameter = DB::table('parameter')->where('kategori', 'kimia air terbatas')->orderBy('bagian', 'asc')->get();
             foreach ($parameter as $key => $par) {
                 $detail = DB::table('tb_zscore')
                             ->where('id_registrasi', $val->id_registrasi)
                             ->where('parameter', $par->id)
                             ->limit('1')
                             ->get();
                 $par->detail = $detail;
             }
             $val->parameter = $parameter;
         }
         // dd($val);

         // dd($data);
         // return view('evaluasi.hematologi.laporanhasilpeserta.view', compact('data', 'parameterna'));
         Excel::create('Z-Score Peserta Per Parameter Kimia Air Terbatas', function($excel) use ($data, $parameterna) {
             $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                 $sheet->loadView('evaluasi.kimiaairterbatas.laporanzscorehasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
             });
         })->download('xls');
     }

     public function hemazscorealatlaporan()
     {
         return view('evaluasi.hematologi.laporanzscorehasilpesertaalat.index');
     }

     public function hemazscorealatlaporanna(\Illuminate\Http\Request $request)
     {
         $input = $request->all();
         $parameterna = DB::table('parameter')
                         ->where('kategori', 'hematologi')
                         ->orderBy('bagian', 'asc')
                         ->get();
         $data = DB::table('hp_headers')
                 ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                 ->where('tb_registrasi.bidang', '=', '1')
                 ->where('hp_headers.siklus', $input['siklus'])
                 ->where('hp_headers.type', $input['tipe'])
                 ->select('hp_headers.kode_lab','hp_headers.id','hp_headers.id_registrasi', 'tb_registrasi.updated_at')
                 ->get();
         foreach ($data as $key => $val) {
             $parameter = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
             foreach ($parameter as $key => $par) {
                 $detail = DB::table('tb_zscore_alat')
                             ->where('id_registrasi', $val->id_registrasi)
                             ->where('parameter', $par->id)
                             ->where('type', $input['tipe'])
                             ->where('siklus', $input['siklus'])
                             ->get();
                 $par->detail = $detail;
             }
             $val->parameter = $parameter;
         }
         // dd($val);

         // dd($data[0]);
         // return view('evaluasi.hematologi.laporanhasilpeserta.view', compact('data', 'parameterna'));
         Excel::create('Z-Score Peserta Per Alat Hematologi', function($excel) use ($data, $parameterna) {
             $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                 $sheet->loadView('evaluasi.hematologi.laporanzscorehasilpesertaalat.view', array('data'=>$data, 'parameterna'=>$parameterna) );
             });
         })->download('xls');
     }

     public function hemazscoremetodelaporan()
     {
         return view('evaluasi.hematologi.laporanzscorehasilpesertametode.index');
     }

     public function hemazscoremetodelaporanna(\Illuminate\Http\Request $request)
     {
         $input = $request->all();
         $parameterna = DB::table('parameter')
                         ->where('kategori', 'hematologi')
                         ->orderBy('bagian', 'asc')
                         ->get();
         $data = DB::table('hp_headers')
                 ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                 ->where('tb_registrasi.bidang', '=', '1')
                 ->where('hp_headers.siklus', $input['siklus'])
                 ->where('hp_headers.type', $input['tipe'])
                 ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'] )
                 ->select('hp_headers.kode_lab','hp_headers.id','hp_headers.id_registrasi', 'tb_registrasi.updated_at')
                 ->get();
         foreach ($data as $key => $val) {
             $parameter = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
             foreach ($parameter as $key => $par) {
                 $detail = DB::table('tb_zscore_metode')
                             ->where('id_registrasi', $val->id_registrasi)
                             ->where('parameter', $par->id)
                             ->where('type', $input['tipe'])
                             ->where('siklus', $input['siklus'])
                             ->get();
                 $par->detail = $detail;
             }
             $val->parameter = $parameter;
         }
         // dd($val);

         // dd($data[0]);
         // return view('evaluasi.hematologi.laporanhasilpeserta.view', compact('data', 'parameterna'));
         Excel::create('Z-Score Peserta Per Alat Hematologi Siklus '.$input['siklus'].' Tahun '.$input['tahun'], function($excel) use ($data, $parameterna) {
             $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                 $sheet->loadView('evaluasi.hematologi.laporanzscorehasilpesertametode.view', array('data'=>$data, 'parameterna'=>$parameterna) );
             });
         })->download('xls');
     }



    public function hemalaporan()
    {
        return view('evaluasi.hematologi.laporanhasilpeserta.index');
    }

    public function hemalaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        // dd($input);
        $parameterna = DB::table('parameter')
                        ->where('kategori', 'hematologi')
                        ->orderBy('bagian', 'asc')
                        ->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('tb_registrasi.bidang', '=', '1')
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['tipe'])
                ->where(function($query){
                    if (Auth::user()->role != '5') {
                        $query->where('tb_registrasi.created_by',Auth::user()->id);
                    }
                })
                ->select('hp_headers.*', 'tb_registrasi.updated_at')
                ->get();
                // dd($data);
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data[0]);
        // return view('evaluasi.hematologi.laporanhasilpeserta.view', compact('data', 'parameterna'));
        Excel::create('Laporan Hasil Peserta Per Parameter Hematologi', function($excel) use ($data, $parameterna) {
            $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.hematologi.laporanhasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
            // $excel->sheet('Data', function($sheet)  use ($data, $parameterna){

            //     $sheet->cell('A1', function($cell) {
            //         $cell->setValue('Kode Lab');
            //     });

            //     $alphas = range('A', 'Z');
            //     $a = 1;
            //     foreach($parameterna as $val){
            //         $sheet->cell($alphas[$a].'1', function($cell) use ($val) {
            //             $cell->setValue(strip_tags($val->nama_parameter));
            //         });
            //         $a++;
            //     }

            //     $startRow = 2;
            //     foreach($data as $val){
            //         $sheet->cell('A'.$startRow, function($cell) use ($val) {
            //             $cell->setValue($val->kode_lab);
            //         });
            //         $a = 1;
            //         foreach($val->parameter as $par){
            //             foreach($par->detail as $hasil){
            //                 $sheet->cell($alphas[$a].$startRow, function($cell) use ($hasil) {
            //                     $cell->setValue($hasil->hasil_pemeriksaan);
            //                 });
            //                 if($par->catatan == "Hasil pemeriksaan menggunakan 1 (satu) desimal"){
            //                     $format = '0.0';
            //                 }elseif($par->catatan == "Hasil pemeriksaan tanpa desimal"){
            //                     $format = '0';
            //                 }
            //                 $sheet->setColumnFormat(array(
            //                     $alphas[$a].$startRow => $format
            //                 ));
            //                 $a++;
            //             }

            //         }
            //         $startRow++;
            //     }
            //     $highestRow = $startRow - 1;
            //     $startRow = 2;

            // });
        })->download('xls');
    }

    public function hemaalatlaporan()
    {
        return view('evaluasi.hematologi.laporanalat.index');
    }

    public function hemaalatlaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('tb_registrasi.bidang', '=', '1')
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['tipe'])
                ->select('hp_headers.*', 'tb_registrasi.updated_at')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        Excel::create('Laporan Hasil Peserta Alat / Instrument Hematologi', function($excel) use ($data, $parameterna) {
            $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.hematologi.laporanalat.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function hemametodelaporan()
    {
        return view('evaluasi.hematologi.laporanmetode.index');
    }

    public function hemametodelaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->join('perusahaan', 'perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('tb_registrasi.bidang', '=', '1')
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['tipe'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('hp_headers.*', 'tb_registrasi.updated_at', 'perusahaan.nama_lab')
                ->orderBy('tb_registrasi.id', 'asc')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'hematologi')->orderBy('bagian', 'asc')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                            ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                            ->where('hp_details.hp_header_id', $val->id)
                            ->where('hp_details.parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data[0]);
        Excel::create('Laporan Hasil Peserta Hematologi', function($excel) use ($data, $parameterna) {
            $excel->sheet('Laporan', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.hematologi.laporanmetode.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function hemasdmedian()
    {
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->orderBy('bagian', 'asc')->get();
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'hematologi')
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.hematologi.sdmedian.index', compact('parameter','sd'));
    }

    public function hemasdmedianedit($id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->orderBy('bagian', 'asc')->get();
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'hematologi')
            ->where('tb_sd_median.id', $id)
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->first();
            // dd($sd);
        return view('evaluasi.hematologi.sdmedian.view', compact('parameter','sd'));
    }

    public function hemasdmedianupdate(\Illuminate\Http\Request $request, $id)
    {
        $Update['parameter'] = $request->parameter;
        $Update['siklus'] = $request->siklus;
        $Update['tahun'] = $request->tahun;
        $Update['tipe'] = $request->tipe;
        $Update['sd'] = $request->sd;
        $Update['median'] = $request->median;

        SdMedian::where('id',$id)->update($Update);
        // dd($input);
        Session::flash('message', 'Edit SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median/hematologi');
    }

    public function hemasdmediandelet($id){
        Session::flash('message', 'Hapus Data SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        SdMedian::find($id)->delete();
        return redirect('evaluasi/input-sd-median/hematologi');
    }

    public function hemasdmedianna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $Savedata = new SdMedian;
        $Savedata->parameter = $input['parameter'];
        $Savedata->siklus = $input['siklus'];
        $Savedata->tahun = $input['tahun'];
        $Savedata->tipe = $input['tipe'];
        $Savedata->sd = $input['sd'];
        $Savedata->median = $input['median'];
        $Savedata->form = 'hematologi';
        $Savedata->save();

        Session::flash('message', 'Input SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-sd-median/hematologi');
    }

    public function hemringedit(\Illuminate\Http\Request $request, $id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->get();
        $data = DB::table('tb_rentang_parameter')
                ->join('parameter','parameter.id','=','tb_rentang_parameter.id_parameter')
                ->where('parameter.kategori','=','hematologi')
                ->select('tb_rentang_parameter.*', 'parameter.nama_parameter')
                ->where('tb_rentang_parameter.id', $id)
                ->first();
        $batas = DB::table('tb_rentang_parameter')
                ->join('parameter','parameter.id','=','tb_rentang_parameter.id_parameter')
                ->where('parameter.kategori','=','hematologi')
                ->select('tb_rentang_parameter.*', 'parameter.nama_parameter')
                ->where('id_parameter', $data->id_parameter)
                ->where('siklus', $data->siklus)
                ->where('tahun', $data->tahun)
                ->where('type', $data->type)
                ->get();
        return view('evaluasi.hematologi.ring.view', compact('batas', 'parameter','data'));
    }

    public function hemringmetodeedit(\Illuminate\Http\Request $request, $id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->get();
        $data = DB::table('tb_rentang_metode')
                ->join('parameter','parameter.id','=','tb_rentang_metode.id_parameter')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_rentang_metode.metode')
                ->where('parameter.kategori','=','hematologi')
                ->select('tb_rentang_metode.*', 'parameter.nama_parameter', 'metode_pemeriksaan.metode_pemeriksaan')
                ->where('tb_rentang_metode.id', $id)
                ->first();
        $batas = DB::table('tb_rentang_metode')
                ->join('parameter','parameter.id','=','tb_rentang_metode.id_parameter')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_rentang_metode.metode')
                ->where('parameter.kategori','=','hematologi')
                ->select('tb_rentang_metode.*', 'parameter.nama_parameter', 'metode_pemeriksaan.metode_pemeriksaan')
                ->where('id_parameter', $data->id_parameter)
                ->where('metode', $data->metode)
                ->where('siklus', $data->siklus)
                ->where('type', $data->type)
                ->where('tahun', $data->tahun)
                ->get();
        return view('evaluasi.hematologi.ringmetode.view', compact('batas', 'parameter', 'data'));
    }

    public function kimiaringmetodeedit(\Illuminate\Http\Request $request, $id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        $data = DB::table('tb_rentang_metode')
                ->join('parameter','parameter.id','=','tb_rentang_metode.id_parameter')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_rentang_metode.metode')
                ->where('parameter.kategori','=','kimia klinik')
                ->select('tb_rentang_metode.*', 'parameter.nama_parameter', 'metode_pemeriksaan.metode_pemeriksaan')
                ->where('tb_rentang_metode.id', $id)
                ->first();
        $batas = DB::table('tb_rentang_metode')
                ->join('parameter','parameter.id','=','tb_rentang_metode.id_parameter')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_rentang_metode.metode')
                ->where('parameter.kategori','=','kimia klinik')
                ->select('tb_rentang_metode.*', 'parameter.nama_parameter', 'metode_pemeriksaan.metode_pemeriksaan')
                ->where('id_parameter', $data->id_parameter)
                ->where('metode', $data->metode)
                ->where('siklus', $data->siklus)
                ->where('type', $data->type)
                ->where('tahun', $data->tahun)
                ->get();
        return view('evaluasi.kimiaklinik.ringmetode.view', compact('batas', 'parameter', 'data'));
    }

    public function hemringmetodeeditna(\Illuminate\Http\Request $request, $id)
    {
        $i = 0 ;
        foreach ($request->batas_bawah as $batas_bawah) {
            $Update['id_parameter'] = $request->id_parameter;
            $Update['metode'] = $request->metode;
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['type'] = $request->type;
            $Update['batas_bawah'] = $request->batas_bawah[$i];
            $Update['batas_atas'] = $request->batas_atas[$i];
            BatasMetode::where('id',$request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit Rentang telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-ring-metode/hematologi');
    }

    public function kimiaringmetodeeditna(\Illuminate\Http\Request $request, $id)
    {
        $i = 0 ;
        foreach ($request->batas_bawah as $batas_bawah) {
            $Update['id_parameter'] = $request->id_parameter;
            $Update['metode'] = $request->metode;
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['type'] = $request->type;
            $Update['batas_bawah'] = $request->batas_bawah[$i];
            $Update['batas_atas'] = $request->batas_atas[$i];
            BatasMetode::where('id',$request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit Rentang telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-ring-metode/kimiaklinik');
    }

    public function hemringeditna(\Illuminate\Http\Request $request, $id)
    {
        $i = 0 ;
        foreach ($request->batas_bawah as $batas_bawah) {
            $Update['id_parameter'] = $request->id_parameter;
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['type'] = $request->type;
            $Update['batas_bawah'] = $request->batas_bawah[$i];
            $Update['batas_atas'] = $request->batas_atas[$i];
            BatasParameter::where('id', $request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit Rentang telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-ring/hematologi');
    }

    public function hemringdelet($id){
        $data = DB::table('tb_rentang_parameter')
                ->join('parameter','parameter.id','=','tb_rentang_parameter.id_parameter')
                ->where('parameter.kategori','=','hematologi')
                ->select('tb_rentang_parameter.*', 'parameter.nama_parameter')
                ->where('tb_rentang_parameter.id', $id)
                ->first();
        BatasParameter::where('id_parameter', $data->id_parameter)->where('siklus', $data->siklus)->where('tahun', $data->tahun)->where('type', $data->type)->delete();
        Session::flash('message', 'Hapus Ring telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        return redirect('evaluasi/input-ring/hematologi');
    }

    public function hemringmetodedelet($id){
        $data = DB::table('tb_rentang_metode')
                ->join('parameter','parameter.id','=','tb_rentang_metode.id_parameter')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_rentang_metode.metode')
                ->where('parameter.kategori','=','hematologi')
                ->select('tb_rentang_metode.*', 'parameter.nama_parameter', 'metode_pemeriksaan.metode_pemeriksaan')
                ->where('tb_rentang_metode.id', $id)
                ->first();
        // dd($data);
        BatasMetode::where('id_parameter', $data->id_parameter)
                    ->where('metode', $data->metode)
                    ->where('siklus', $data->siklus)
                    ->where('type', $data->type)
                    ->where('tahun', $data->tahun)
                    ->delete();

        Session::flash('message', 'Hapus Ring telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        return redirect('evaluasi/input-ring-metode/hematologi');
    }

    public function kimiaringmetodedelet($id){
        $data = DB::table('tb_rentang_metode')
                ->join('parameter','parameter.id','=','tb_rentang_metode.id_parameter')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_rentang_metode.metode')
                ->where('parameter.kategori','=','kimia klinik')
                ->select('tb_rentang_metode.*', 'parameter.nama_parameter', 'metode_pemeriksaan.metode_pemeriksaan')
                ->where('tb_rentang_metode.id', $id)
                ->first();
        // dd($data);
        BatasMetode::where('id_parameter', $data->id_parameter)
                    ->where('metode', $data->metode)
                    ->where('siklus', $data->siklus)
                    ->where('type', $data->type)
                    ->where('tahun', $data->tahun)
                    ->delete();

        Session::flash('message', 'Hapus Ring telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        return redirect('evaluasi/input-ring-metode/kimiaklinik');
    }

    public function hemring()
    {
        $batas = DB::table('tb_rentang_parameter')
                ->join('parameter','parameter.id','=','tb_rentang_parameter.id_parameter')
                ->where('parameter.kategori','=','hematologi')
                ->select('tb_rentang_parameter.*', 'parameter.nama_parameter')
                ->groupBy('id_parameter', 'tahun', 'siklus', 'type')
                ->orderBy('id','desc')
                ->paginate(10);
            // dd($batas);
        return view('evaluasi.hematologi.ring.index', compact('batas'));
    }

    public function hemringinsert()
    {
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->get();
        return view('evaluasi.hematologi.ring.insert', compact('parameter'));
    }

    public function hemringna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        // dd($input);
        $i = 0 ;
        foreach ($input['batas_bawah'] as $batas_bawah) {
            $Savedata = new BatasParameter;
            $Savedata->id_parameter = $input['id_parameter'];
            $Savedata->siklus = $input['siklus'];
            $Savedata->tahun = $input['tahun'];
            $Savedata->type = $input['type'];
            $Savedata->batas_bawah = $input['batas_bawah'][$i];
            $Savedata->batas_atas = $input['batas_atas'][$i];
            $Savedata->save();
            $i++;
        }

        Session::flash('message', 'Input Rentang telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-ring/hematologi');
    }

    public function hemringmetode()
    {
        $batas = DB::table('tb_rentang_metode')
                ->join('parameter','parameter.id','=','tb_rentang_metode.id_parameter')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_rentang_metode.metode')
                ->where('parameter.kategori','=','hematologi')
                ->select('tb_rentang_metode.*', 'parameter.nama_parameter', 'metode_pemeriksaan.metode_pemeriksaan')
                ->groupBy('id_parameter', 'metode','siklus','tahun','type')
                ->paginate(10);
            // dd($sd);
        return view('evaluasi.hematologi.ringmetode.index', compact('batas'));
    }

    public function hemringmetodei()
    {
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->get();
        return view('evaluasi.hematologi.ringmetode.insert', compact('parameter'));
    }

    public function hemringmetodena(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $i = 0 ;
        foreach ($input['batas_bawah'] as $batas_bawah) {
            $Savedata = new BatasMetode;
            $Savedata->id_parameter = $input['id_parameter'];
            $Savedata->metode = $input['metode'];
            $Savedata->siklus = $input['siklus'];
            $Savedata->tahun = $input['tahun'];
            $Savedata->type = $input['type'];
            $Savedata->batas_bawah = $input['batas_bawah'][$i];
            $Savedata->batas_atas = $input['batas_atas'][$i];
            $Savedata->save();
            $i++;
        }

        Session::flash('message', 'Input Rentang telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-ring-metode/hematologi');
    }

    public function hemasdmedianalat()
    {
        $alat = DB::table('tb_instrumen')->where('id_parameter','=','hematologi')->get();
        $parameter = DB::table('parameter')->where('kategori', '=','hematologi')->get();
        $sd = DB::table('tb_sd_median_alat')
            ->join('tb_instrumen','tb_instrumen.id','=','tb_sd_median_alat.alat')
            ->leftjoin('parameter','parameter.id','=','tb_sd_median_alat.parameter')
            ->where('form', '=', 'hematologi')
            ->orderBy('tb_sd_median_alat.id', 'desc')
            ->select('tb_sd_median_alat.*', 'tb_instrumen.instrumen', 'parameter.nama_parameter')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.hematologi.sdmedianalat.index', compact('alat','sd', 'parameter'));
    }

    public function hemasdmedianalatna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $Savedata = new SdMedianAlat;
        $Savedata->parameter = $input['parameter'];
        $Savedata->alat = $input['alat'];
        $Savedata->siklus = $input['siklus'];
        $Savedata->tahun = $input['tahun'];
        $Savedata->tipe = $input['tipe'];
        $Savedata->sd = $input['sd'];
        $Savedata->median = $input['median'];
        $Savedata->form = 'hematologi';
        $Savedata->save();

        Session::flash('message', 'Input SD dan Median Alat / Instrument telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-sd-median-alat/hematologi');
    }

    public function hemasdmedianalatedit($id)
    {
        $alat = DB::table('tb_instrumen')->where('id_parameter','=','hematologi')->get();
        $parameter = DB::table('parameter')->where('kategori','=','hematologi')->get();
        $sd = DB::table('tb_sd_median_alat')
            ->join('tb_instrumen','tb_instrumen.id','=','tb_sd_median_alat.alat')
            ->leftjoin('parameter','parameter.id','=','tb_sd_median_alat.parameter')
            ->where('form', '=', 'hematologi')
            ->where('tb_sd_median_alat.id', $id)
            ->orderBy('tb_sd_median_alat.id', 'desc')
            ->select('tb_sd_median_alat.*', 'tb_instrumen.instrumen', 'parameter.nama_parameter')
            ->first();
            // dd($sd);
        return view('evaluasi.hematologi.sdmedianalat.view', compact('parameter','sd','alat'));
    }

    public function hemasdmedianalatupdate(\Illuminate\Http\Request $request, $id)
    {
        $Update['parameter'] = $request->parameter;
        $Update['alat'] = $request->alat;
        $Update['siklus'] = $request->siklus;
        $Update['tahun'] = $request->tahun;
        $Update['tipe'] = $request->tipe;
        $Update['sd'] = $request->sd;
        $Update['median'] = $request->median;

        SdMedianAlat::where('id',$id)->update($Update);
        // dd($input);
        Session::flash('message', 'Edit SD dan Median Alat / Instrument telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-alat/hematologi');
    }

    public function hemasdmedianalatdelet($id){
        Session::flash('message', 'Hapus Data SD dan Median Alat / Instrument telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        SdMedianAlat::find($id)->delete();
        return redirect('evaluasi/input-sd-median-alat/hematologi');
    }

    public function perusahaanhem(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes,tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('status', '>=', 2)  
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where(function($query) use ($siklus)
                    {
                        $query->where('siklus', '=', '12')
                            ->orwhere('siklus', $siklus);
                    })
                ->where('tb_registrasi.bidang','=','1')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Proses
                                    </button>
                                </a>'
                        ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'a')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'b')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
        $jobPending = JobsEvaluasi::where('name','=','evaluasi-parameter')->where('status','=',0)->first();
        $showAllertPending = false;
        $showAllertSuccess = false;
        $showForm = true;
        if(!empty($jobPending)){
            $showAllertPending = true;
            $showAllertSuccess = false;
            $showForm = false;
        }else{
            $jobSuccess = JobsEvaluasi::where('name','=','evaluasi-parameter')->where('status','=',1)->where('isread','=',0)->first();
            if(!empty($jobSuccess)){
                $showAllertPending = false;
                $showAllertSuccess = true;
                $showForm = true;
                JobsEvaluasi::where('name','=','evaluasi-parameter')->where('status','=',1)->where('isread','=',0)->update(['isread'=>1]);
            }else{
                $showForm = true;
            }
        }
        // $evaluasi = TanggalEvaluasi::where('form','=','Hematologi')->first(); 
        return view('evaluasi.hematologi.zscore.perusahaan',compact('showAllertPending','showAllertSuccess','showForm'));
    }

     public function inserttanggal(\Illuminate\Http\Request $request)
    {
        $data = $request->all();
        $evaluasi = DB::table('tanggal_evaluasi')
                    ->where('form', $request->form)
                    ->where('siklus', $request->siklus)
                    ->where('tahun', $request->tahun)
                    ->get();
        // dd($evaluasi);
        if (!count($evaluasi)) {
            $Save = new TanggalEvaluasi;
            $Save->tanggal = $request->tanggal;
            $Save->form = $request->form;
            $Save->siklus = $request->siklus;
            $Save->tahun = $request->tahun;
            $Save->save();
        }else{
            $evaluasi = TanggalEvaluasi::where("form", $request->form)
                        ->where("siklus", $request->siklus)
                        ->where("tahun", $request->tahun)
                        ->update([
                            "tanggal" => $request->tanggal
                        ]);
        }
        // dd($data);
        return back();
    }

    

    public function dataperusahaanhem(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id) 
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.zscore.dataperusahaan', compact('data','tahun','siklus'));
    }

    public function perusahaanhemcetak(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, tb_registrasi.kode_lebpes, @rownum := @rownum +1 as rownum,tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('tb_registrasi.bidang','=','1')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('tb_registrasi.status','>=', 2)
                ->where(function($query) use ($siklus)
                    {
                        $query->where('tb_registrasi.siklus', '=', 12)
                            ->orwhere('tb_registrasi.siklus', '=', $siklus);
                    })
                ->get();

            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Proses
                                    </button>
                                </a>'
                        ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'a')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'b')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
            // $evaluasi = TanggalEvaluasi::where('form','=','Hematologi')->first(); 
        return view('evaluasi.hematologi.zscorecetak.perusahaan');
    }

    public function perusahaanhemcetaksertifikat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, tb_registrasi.kode_lebpes, @rownum := @rownum +1 as rownum,tb_registrasi.id as idregistrasi'))
                ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('tb_registrasi.bidang','=','1')
                ->where('tb_registrasi.status','>=','2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where(function($query) use ($siklus)
                    {
                        $query->where('siklus', '=', '12')
                            ->orwhere('siklus', $siklus);
                    })
                ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data)use($tahun,$siklus){

                     $evaluasi = DB::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('siklus', $siklus)
                                ->where('tahun',$tahun)
                                ->where('form', '=', 'hematologi')
                                ->orderBy('id', 'desc')
                                ->first();
                    if($evaluasi != NULL){
                        if ($evaluasi->siklus == '1') {
                            if ($evaluasi->siklus == 'a') {
                                return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';
                            }else{
                                return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';  
                            }
                        }elseif($evaluasi->siklus == '2'){
                            if ($evaluasi->siklus == 'a') {
                                return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                                <button class="btn btn-info btn-xs">
                                                    Evaluasi
                                                </button>
                                            </a>';
                            }else{
                                return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';
                            }
                        }else{
                            return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Evaluasi
                                            </button>
                                        </a>';
                        }
                    }else{
                        return "".'<a href="sertifikat/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                            Evaluasi
                                        </button>
                                    </a>';
                    }
                     ;})
            ->addColumn('A', function($data)use($tahun,$siklus){
                        $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('tahun',$tahun)
                                    ->where('type', '=', 'a')
                                    ->where('form', '=', 'hematologi')
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if($evaluasi != NULL){
                            if ($evaluasi->siklus == '1') {
                                return "".'Selesai';
                            }else{
                                return "".'Selesai';
                            }
                        }else{
                            return "".'-';
                        }
                ;})
            ->addColumn('B', function($data)use($tahun,$siklus){
                        $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('siklus', $siklus)
                                    ->where('tahun',$tahun)
                                    ->where('type', '=', 'b')
                                    ->where('form', '=', 'hematologi')
                                    ->orderBy('id', 'desc')
                                    ->first();
                        if($evaluasi != NULL){
                            if ($evaluasi->siklus == '1') {
                                return "".'Selesai';
                            }else{
                                return "".'Selesai';
                            }
                        }else{
                            return "".'-';
                        }
                ;})
            ->make(true);
        }
        $sertifikat = SertfikatInsert::where('tb_sertifikat.parameter','=','Hematologi')->first();
        return view('evaluasi.hematologi.zscorecetak.perusahaansertifikat',compact('sertifikat'));
    }

    public function evaluasiInsertSertifikatna(\Illuminate\Http\Request $request)
    {
        $data = $request->all();
        $sertifikat = DB::table('tb_sertifikat')->where('siklus',$data['siklus'])->where('tahun',$data['tahun'])->where('parameter','=','Hematologi')->get();

        if (count($sertifikat)) {
            $sertifikat = SertfikatInsert::where("tb_sertifikat.parameter", $request->parameter)
                                ->where('siklus', $data['siklus'])
                                ->where('tahun', $data['tahun'])
                                ->update([
                                    "tanggal_sertifikat" => $request->tanggal_sertifikat,
                                    "nomor_sertifikat" => $request->nomor_sertifikat
                                ]);
        }else{
            $b = new SertfikatInsert;
            $b->tanggal_sertifikat = $request->tanggal_sertifikat;
            $b->nomor_sertifikat =$request->nomor_sertifikat;
            $b->parameter = $request->parameter;
            $b->siklus = $request->siklus;
            $b->tahun = $request->tahun;
            $b->save();
        }
        // dd($data);
        return back();
    }

    public function dataperusahaanhemcetak(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.zscorecetak.dataperusahaan', compact('data','siklus','tahun'));
    }

     public function dataperusahaanhemcetaksertifikat(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.zscorecetak.dataperusahaansertifikat', compact('data','tahun','siklus'));
    }

    public function perusahaanhemnilai(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','1');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="hematologi/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.hematologi.grafiknilai.perusahaan');
    }

    public function dataperusahaanhemnilai($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.grafiknilai.dataperusahaan', compact('data'));
    }

    public function perusahaanhemalat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','1')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="hematologi/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        $jobPending = JobsEvaluasi::where('name','=','evaluasi-alat')->where('status','=',0)->first();
        $showAllertPending = false;
        $showAllertSuccess = false;
        $showForm = true;
        if(!empty($jobPending)){
            $showAllertPending = true;
            $showAllertSuccess = false;
            $showForm = false;
        }else{
            $jobSuccess = JobsEvaluasi::where('name','=','evaluasi-alat')->where('status','=',1)->where('isread','=',0)->first();
            if(!empty($jobSuccess)){
                $showAllertPending = false;
                $showAllertSuccess = true;
                $showForm = true;
                JobsEvaluasi::where('name','=','evaluasi-alat')->where('status','=',1)->where('isread','=',0)->update(['isread'=>1]);
            }else{
                $showForm = true;
            }
        }
        return view('evaluasi.hematologi.zscorealat.perusahaan',compact('showAllertPending','showAllertSuccess','showForm'));
    }

    public function dataperusahaanhemalat($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.zscorealat.dataperusahaan', compact('data'));
    }


    public function perusahaankimiaalat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','1');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kimiaklinik/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaklinik.zscorealat.perusahaan');
    }

    public function dataperusahaankimiaalat($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaklinik.zscorealat.dataperusahaan', compact('data'));
    }

    public function perusahaankimiametode(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','2');
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    return "".'<a href="kimiaklinik/'.$data->id.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Evaluasi
                                    </button>
                                </a>'
                        ;})
            ->make(true);
        }
        return view('evaluasi.kimiaklinik.zscoremetode.perusahaan');
    }

    public function dataperusahaankimiametode($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '2')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaklinik.zscoremetode.dataperusahaan', compact('data'));
    }

    public function perusahaanhemametode(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.id as idregistrasi, tb_registrasi.kode_lebpes as kode_lebpes'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->where('status', '>=', '2')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where(function($query) use ($siklus)
                    {
                        $query->where('siklus', '=', '12')
                            ->orwhere('siklus', $siklus);
                    })
                ->where('tb_registrasi.bidang','=','1')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    return "".'<a href="hematologi/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                    <button class="btn btn-primary btn-xs">
                                        Proses
                                    </button>
                                </a>'
                        ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'a')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'b')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
        $jobPending = JobsEvaluasi::where('name','=','evaluasi-metode')->where('status','=',0)->first();
        $showAllertPending = false;
        $showAllertSuccess = false;
        $showForm = true;
        if(!empty($jobPending)){
            $showAllertPending = true;
            $showAllertSuccess = false;
            $showForm = false;
        }else{
            $jobSuccess = JobsEvaluasi::where('name','=','evaluasi-metode')->where('status','=',1)->where('isread','=',0)->first();
            if(!empty($jobSuccess)){
                $showAllertPending = false;
                $showAllertSuccess = true;
                $showForm = true;
                JobsEvaluasi::where('name','=','evaluasi-metode')->where('status','=',1)->where('isread','=',0)->update(['isread'=>1]);
            }else{
                $showForm = true;
            }
        }
        // $evaluasi = TanggalEvaluasi::where('form','=','Hematologi')->first();
        return view('evaluasi.hematologi.zscoremetode.perusahaan',compact('showAllertPending','showAllertSuccess','showForm'));
    }

    public function dataperusahaanhemametode(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('sub_bidang.id', '=', '1')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.hematologi.zscoremetode.dataperusahaan', compact('data','siklus','tahun'));
    }

    public function kimiakliniklaporan()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.laporanhasilpeserta.index', compact('parameter'));
    }

    public function kimiakliniklaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('tb_registrasi.bidang', '=', '2')
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['tipe'])
                ->select('hp_headers.*', 'tb_registrasi.updated_at')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        // return view('evaluasi.kimiaklinik.laporanhasilpeserta.view', compact('data','parameterna'));
        Excel::create('Laporan Hasil Peserta Per Parameter Kimia Klinik', function($excel) use ($data, $parameterna) {
            $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.kimiaklinik.laporanhasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function kimiaklinikrekapitulasi()
    {
        $parameter = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
        return view('evaluasi.kimiaklinik.rekapitulasihasil.index', compact('parameter'));
    }

    public function kimiaklinikrekapitulasina(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $jumlahpesertaa = DB::table('tb_zscore')
                        ->where('type', $input['tipe'])
                        ->where('form', '=', 'kimia klinik')
                        ->where('parameter', $input['parameter'])
                        ->where('siklus', $input['siklus'])
                        ->where('tahun', $input['tahun'])
                        ->count();

        $jumlahalata = DB::table('tb_zscore_alat')
                        ->where('type', $input['tipe'])
                        ->where('form', '=', 'kimia klinik')
                        ->where('parameter', $input['parameter'])
                        ->where('siklus', $input['siklus'])
                        ->where('tahun', $input['tahun'])
                        ->count();

        $jumlahmetodea = DB::table('tb_zscore_metode')
                        ->where('type', $input['tipe'])
                        ->where('form', '=', 'kimia klinik')
                        ->where('parameter', $input['parameter'])
                        ->where('siklus', $input['siklus'])
                        ->where('tahun', $input['tahun'])
                        ->count();

        $dataa = "SELECT t.jumlah AS score, count(*) AS jumlah FROM
                    (SELECT CASE
                        WHEN zscore = '-' THEN 'Tidak di Analisis'
                        WHEN zscore <= 2 AND zscore >= - 2 THEN 'Memuaskan'
                        WHEN zscore > 2 AND zscore <= 3 OR zscore < - 2 AND zscore >= - 3 THEN 'Peringatan'
                        WHEN zscore > 3 OR zscore < 3 THEN 'Tidak Memuaskan'
                        END AS jumlah 
                        FROM tb_zscore where form = 'kimia klinik' AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." AND tahun = ".$input['tahun']." AND parameter = ".$input['parameter']."
                    ) t GROUP BY score";

        $dataalata = "SELECT t.jumlah AS score, count(*) AS jumlah FROM
                    (SELECT CASE
                        WHEN zscore = '-' THEN 'Tidak di Analisis'
                        WHEN zscore <= 2 AND zscore >= - 2 THEN 'Memuaskan'
                        WHEN zscore > 2 AND zscore <= 3 OR zscore < - 2 AND zscore >= - 3 THEN 'Peringatan'
                        WHEN zscore > 3 OR zscore < 3 THEN 'Tidak Memuaskan'
                        END AS jumlah 
                        FROM tb_zscore_alat where form = 'kimia klinik' AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." AND tahun = ".$input['tahun']." AND parameter = ".$input['parameter']."
                    ) t GROUP BY score";

        $datametodea = "SELECT t.jumlah AS score, count(*) AS jumlah FROM
                    (SELECT CASE
                        WHEN zscore = '-' THEN 'Tidak di Analisis'
                        WHEN zscore <= 2 AND zscore >= - 2 THEN 'Memuaskan'
                        WHEN zscore > 2 AND zscore <= 3 OR zscore < - 2 AND zscore >= - 3 THEN 'Peringatan'
                        WHEN zscore > 3 OR zscore < 3 THEN 'Tidak Memuaskan'
                        END AS jumlah 
                        FROM tb_zscore_metode where form = 'kimia klinik' AND type = '".$input['tipe']."' AND siklus = ".$input['siklus']." AND tahun = ".$input['tahun']." AND parameter = ".$input['parameter']."
                    ) t GROUP BY score";

        $datapesertaa = DB::table(DB::raw("($dataa) t"))->select('*')->get();
        $dataalata = DB::table(DB::raw("($dataalata) t"))->select('*')->get();
        $datametodea = DB::table(DB::raw("($datametodea) t"))->select('*')->get();
        // dd($datametodea);
        // return view('evaluasi.kimiaklinik.rekapitulasihasil.view', compact('jumlahpesertaa', 'datapesertaa', 'jumlahalata', 'dataalata', 'datametodea', 'jumlahmetodea'));

        Excel::create('Rekapitulasi Hasil PNPME per Parameter Kimia Klinik', function($excel) use ($jumlahpesertaa, $datapesertaa, $jumlahalata, $dataalata, $datametodea, $jumlahmetodea) {
            $excel->sheet('Wilayah', function($sheet) use ($jumlahpesertaa, $datapesertaa, $jumlahalata, $dataalata, $datametodea, $jumlahmetodea) {
                $sheet->loadView('evaluasi.kimiaklinik.rekapitulasihasil.view', array('jumlahpesertaa'=>$jumlahpesertaa, 'datapesertaa'=>$datapesertaa, 'jumlahalata'=>$jumlahalata, 'dataalata'=>$dataalata, 'datametodea'=>$datametodea, 'jumlahmetodea'=>$jumlahmetodea) );
            });
        })->download('xls');
    }

    public function hematologirekapitulasi()
    {
        $parameter = DB::table('parameter')->where('kategori', 'hematologi')->get();
        return view('evaluasi.hematologi.rekapitulasihasil.index', compact('parameter'));
    }

    public function hematologirekapitulasina(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameter = DB::table('parameter')->where('id', $input['parameter'])->first();
        $dataa = "SELECT
                    (
                        SELECT 
                        count(*)
                        FROM `hp_details` 
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='".$input['tipe']."'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='".$input['tipe']."' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id
                        WHERE
                            hp_details.parameter_id = ".$input['parameter']."
                    ) AS JUMLAH_PESERTA,
                    (
                        SELECT 
                            count(*)
                        FROM `hp_details` 
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='".$input['tipe']."'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='".$input['tipe']."' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id
                        WHERE
                            hp_details.parameter_id = ".$input['parameter']." AND hasil_pemeriksaan IN ('_','-','-','_') AND tb_zscore.zscore = '-'
                    ) AS TIDAK_DI_ANAL,
                    (
                        SELECT 
                        count(*)
                        FROM `hp_details` 
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='".$input['tipe']."'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='".$input['tipe']."' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id
                        WHERE
                            hp_details.parameter_id = ".$input['parameter']." AND hasil_pemeriksaan NOT IN  ('_','-','-','_') AND tb_zscore.zscore = '-'
                    ) AS TIDAK_DI_EVALUASI,
                    (
                        SELECT 
                        count(*)
                        FROM `hp_details` 
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='".$input['tipe']."'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='".$input['tipe']."' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                        WHERE
                            hp_details.parameter_id = ".$input['parameter']." AND hasil_pemeriksaan NOT IN  ('_','-','-','_') 
                            AND 
                            (
                                (tb_zscore.zscore > 2 AND tb_zscore.zscore <= 3) 
                                OR 
                                (tb_zscore.zscore >= -3 AND tb_zscore.zscore < -2)
                            ) 
                    ) AS PERINGATAN,
                    (
                        SELECT 
                        count(*)
                        FROM `hp_details` 
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='".$input['tipe']."'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='".$input['tipe']."' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                        WHERE
                            hp_details.parameter_id = ".$input['parameter']." 
                            AND 
                            hasil_pemeriksaan NOT IN  ('_','-','-','_') 
                            AND (
                                    (tb_zscore.zscore >= -2 AND tb_zscore.zscore <= 2)
                                ) 
                    ) AS MEMUASKAN,
                    (
                        SELECT 
                        count(*)
                        FROM `hp_details` 
                            INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='".$input['tipe']."'
                            INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                            INNER JOIN tb_zscore ON tb_zscore.parameter = hp_details.parameter_id AND tb_zscore.siklus = ".$input['siklus']." AND tb_zscore.tahun = ".$input['tahun']." AND tb_zscore.type='".$input['tipe']."' AND tb_zscore.form = '".$input['form']."' AND tb_zscore.id_registrasi = tb_registrasi.id AND tb_zscore.zscore != '-'
                        WHERE
                            hp_details.parameter_id = ".$input['parameter']." AND hasil_pemeriksaan NOT IN  ('_','-','-','_') 
                            AND 
                            (
                                (tb_zscore.zscore < -3 OR tb_zscore.zscore > 3)
                            ) 
                    ) AS TIDAKS_MEMUASKAN
                ";
        $datametodea = "SELECT
                            (
                                SELECT 
                                count(*)
                                FROM `hp_details` 
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='".$input['tipe']."'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='".$input['tipe']."' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id
                                WHERE
                                    hp_details.parameter_id = ".$input['parameter']."
                            ) AS JUMLAH_PESERTA_METODE,
                            (
                                SELECT 
                                    count(*)
                                FROM `hp_details` 
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='".$input['tipe']."'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='".$input['tipe']."' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id
                                WHERE
                                    hp_details.parameter_id = ".$input['parameter']." AND hasil_pemeriksaan IN ('_','-','_') AND tb_zscore_metode.zscore = '-'
                            ) AS TIDAK_DI_ANAL_METODE,
                            (
                                SELECT 
                                count(*)
                                FROM `hp_details` 
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='".$input['tipe']."'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='".$input['tipe']."' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id
                                WHERE
                                    hp_details.parameter_id = ".$input['parameter']." AND hasil_pemeriksaan NOT IN ('_','-','_') AND tb_zscore_metode.zscore = '-'
                            ) AS TIDAK_DI_EVALUASI_METODE,
                            (
                                SELECT 
                                count(*)
                                FROM `hp_details` 
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='".$input['tipe']."'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='".$input['tipe']."' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$input['parameter']." AND hasil_pemeriksaan NOT IN ('_','-','_') 
                                    AND 
                                    (
                                        (tb_zscore_metode.zscore > 2 AND tb_zscore_metode.zscore <= 3) 
                                        OR 
                                        (tb_zscore_metode.zscore >= -3 AND tb_zscore_metode.zscore < -2)
                                    ) 
                            ) AS PERINGATAN_METODE,
                            (
                                SELECT 
                                count(*)
                                FROM `hp_details` 
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='".$input['tipe']."'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='".$input['tipe']."' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$input['parameter']." 
                                    AND 
                                    hasil_pemeriksaan NOT IN ('_','-','_') 
                                    AND (
                                            (tb_zscore_metode.zscore >= -2 AND tb_zscore_metode.zscore <= 2)
                                        ) 
                            ) AS MEMUASKAN_METODE,
                            (
                                SELECT 
                                count(*)
                                FROM `hp_details` 
                                    INNER JOIN hp_headers ON hp_details.hp_header_id = hp_headers.id AND hp_headers.kategori='".$input['form']."' AND hp_headers.siklus=".$input['siklus']." AND hp_headers.type='".$input['tipe']."'
                                    INNER JOIN tb_registrasi ON hp_headers.id_registrasi = tb_registrasi.id
                                    INNER JOIN tb_zscore_metode ON tb_zscore_metode.parameter = hp_details.parameter_id AND tb_zscore_metode.siklus = ".$input['siklus']." AND tb_zscore_metode.tahun = ".$input['tahun']." AND tb_zscore_metode.type='".$input['tipe']."' AND tb_zscore_metode.form = '".$input['form']."' AND tb_zscore_metode.id_registrasi = tb_registrasi.id AND tb_zscore_metode.zscore != '-'
                                WHERE
                                    hp_details.parameter_id = ".$input['parameter']." AND hasil_pemeriksaan NOT IN ('_','-','_') 
                                    AND 
                                    (
                                        (tb_zscore_metode.zscore < -3 OR tb_zscore_metode.zscore > 3)
                                    ) 
                            ) AS TIDAKS_MEMUASKAN_METODE
                ";

        $datapesertaa = DB::table(DB::raw("($dataa) t"))->select('*')->first();
        $datametodea = DB::table(DB::raw("($datametodea) t"))->select('*')->first();
        // dd($dataalata);
        // return view('evaluasi.hematologi.rekapitulasihasil.view', compact('jumlahpesertaa', 'datapesertaa', 'jumlahalata', 'dataalata', 'datametodea', 'jumlahmetodea'));

        Excel::create('Rekapitulasi Hasil PNPME per Parameter '.$input['form'], function($excel) use ($datapesertaa, $datametodea, $input, $parameter) {
            $excel->sheet('Wilayah', function($sheet) use ($datapesertaa, $datametodea, $input, $parameter) {
                $sheet->loadView('evaluasi.hematologi.rekapitulasihasil.view', array('datapesertaa'=>$datapesertaa,'datametodea'=>$datametodea,'input'=>$input,'parameter'=>$parameter) );
            });
        })->download('xls');
    }

    public function kimiakliniklaporanpelaksanaan()
    {
        $propinsi = DB::table('provinces')->get();
        return view('evaluasi.kimiaklinik.laporanpelaksanaan.index', compact('propinsi'));
    }

    public function kimiakliniklaporanpelaksanaanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->join('perusahaan','perusahaan.id','=','tb_registrasi.perusahaan_id')
                ->leftjoin('provinces','provinces.id','=','perusahaan.provinsi')
                ->where('tb_registrasi.bidang', '=', '2')
                ->where('hp_headers.siklus', $input['siklus'])
                ->select('hp_headers.*', 'tb_registrasi.updated_at','provinces.name','perusahaan.provinsi', 'tb_registrasi.id as Id')
                ->groupBy('hp_headers.id_registrasi')
                ->orderBy('hp_headers.kode_lab', 'asc')
                ->get();
        // dd($data);
        foreach ($data as $key => $val) {
            $tipe =  DB::table('hp_headers')
                    ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                    ->where('tb_registrasi.bidang', '=', '2')
                    ->where('hp_headers.siklus', $input['siklus'])
                    ->where('hp_headers.type', 'a')
                    ->where('hp_headers.id_registrasi', $val->Id)
                    ->select('hp_headers.*', 'tb_registrasi.updated_at')
                    ->first();
            $val->tipe_1 = $tipe;
            $tipe =  DB::table('hp_headers')
            ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
            ->where('tb_registrasi.bidang', '=', '2')
            ->where('hp_headers.siklus', $input['siklus'])
            ->where('hp_headers.type', 'b')
            ->where('hp_headers.id_registrasi', $val->Id)
            ->select('hp_headers.*', 'tb_registrasi.updated_at')
            ->first();
            $val->tipe_2 = $tipe;
        }
        // dd($data);
        if ($input['proses'] == 'Proses') {
            // return view('evaluasi.kimiaklinik.laporanpelaksanaan.view', compact('data','parameterna'));
            Excel::create('LAPORAN PELAKSANAAN PNPME Kimia Klinik', function($excel) use ($data, $parameterna) {
                $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                    $sheet->loadView('evaluasi.kimiaklinik.laporanpelaksanaan.view', array('data'=>$data, 'parameterna'=>$parameterna) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('evaluasi.kimiaklinik.laporanpelaksanaan.print', compact('data', 'input', 'parameterna'))
                ->setPaper('a4', 'lanscape')
                ->setwarnings(false);
            return $pdf->stream('Laporan Pelaksanaan PME Kimia Klinik.pdf');
        }
    }

    public function kimiaklinikalatlaporan()
    {
        return view('evaluasi.kimiaklinik.laporanalat.index');
    }

    public function kimiaklinikalatlaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('tb_registrasi.bidang', '=', '2')
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['tipe'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('hp_headers.*', 'tb_registrasi.updated_at')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        // return view('evaluasi.kimiaklinik.laporanalat.view', compact('parameterna', 'data'));
        Excel::create('Laporan Hasil Peserta Alat / Instrument Kimia Klinik', function($excel) use ($data, $parameterna) {
            $excel->sheet('Laporan', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.kimiaklinik.laporanalat.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function kimiaringedit(\Illuminate\Http\Request $request, $id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        $data = DB::table('tb_rentang_parameter')
                ->join('parameter','parameter.id','=','tb_rentang_parameter.id_parameter')
                ->where('parameter.kategori','=','kimia klinik')
                ->select('tb_rentang_parameter.*', 'parameter.nama_parameter')
                ->where('tb_rentang_parameter.id', $id)
                ->first();
        $batas = DB::table('tb_rentang_parameter')
                ->join('parameter','parameter.id','=','tb_rentang_parameter.id_parameter')
                ->where('parameter.kategori','=','kimia klinik')
                ->select('tb_rentang_parameter.*', 'parameter.nama_parameter')
                ->where('id_parameter', $data->id_parameter)
                ->where('siklus', $data->siklus)
                ->where('tahun', $data->tahun)
                ->where('type', $data->type)
                ->get();
        return view('evaluasi.kimiaklinik.ring.view', compact('batas', 'parameter','data'));
    }

    public function kimiaringeditna(\Illuminate\Http\Request $request, $id)
    {
        $i = 0 ;
        foreach ($request->batas_bawah as $batas_bawah) {
            $Update['id_parameter'] = $request->id_parameter;
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['type'] = $request->type;
            $Update['batas_bawah'] = $request->batas_bawah[$i];
            $Update['batas_atas'] = $request->batas_atas[$i];
            BatasParameter::where('id', $request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit Rentang telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-ring/kimiaklinik');
    }

    public function kimiaringdelet($id){
        $data = DB::table('tb_rentang_parameter')
                ->join('parameter','parameter.id','=','tb_rentang_parameter.id_parameter')
                ->where('parameter.kategori','=','kimia klinik')
                ->select('tb_rentang_parameter.*', 'parameter.nama_parameter')
                ->where('tb_rentang_parameter.id', $id)
                ->first();
        BatasParameter::where('id_parameter', $data->id_parameter)->where('siklus', $data->siklus)->where('tahun', $data->tahun)->where('type', $data->type)->delete();
        Session::flash('message', 'Hapus Ring telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        return redirect('evaluasi/input-ring/kimiaklinik');
    }

    public function kimiaringdeletalat(\Illuminate\Http\Request $request){
        Session::flash('message', 'Hapus Ring telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        $tahun = $request->query('tahun');
        $tipe = $request->query('tipe');
        $siklus = $request->query('siklus');

        RingKKAlat::where('tahun','=',$tahun)->where('siklus','=',$siklus)->where('tipe','=',$tipe)->delete();
        return redirect('evaluasi/input-ring/kimiaklinik-alat');
    }

    public function kimiaring()
    {
        $batas = DB::table('tb_rentang_parameter')
                ->join('parameter','parameter.id','=','tb_rentang_parameter.id_parameter')
                ->where('parameter.kategori','=','kimia klinik')
                ->select('tb_rentang_parameter.*', 'parameter.nama_parameter')
                ->groupBy('id_parameter', 'tahun', 'siklus', 'type')
                ->orderBy('id','desc')
                ->paginate(10);
            // dd($batas);
        return view('evaluasi.kimiaklinik.ring.index', compact('batas'));
    }

    public function kimiaringalat()
    {
        $sd = DB::table('tb_ring_kk_alat')
            ->where('form', '=', 'kimia klinik')
            ->groupBy('tb_ring_kk_alat.tahun','tb_ring_kk_alat.siklus','tb_ring_kk_alat.tipe')
            ->select('tb_ring_kk_alat.tahun','tb_ring_kk_alat.siklus','tb_ring_kk_alat.tipe')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaklinik.ringalat.index', compact('sd'));
    }

    public function kimiaringmetode()
    {
        $batas = DB::table('tb_rentang_metode')
                ->join('parameter','parameter.id','=','tb_rentang_metode.id_parameter')
                ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_rentang_metode.metode')
                ->where('parameter.kategori','=','kimia klinik')
                ->select('tb_rentang_metode.*', 'parameter.nama_parameter', 'metode_pemeriksaan.metode_pemeriksaan')
                ->groupBy('id_parameter', 'metode', 'tahun', 'siklus', 'type')
                ->paginate(10);
            // dd($sd);
        return view('evaluasi.kimiaklinik.ringmetode.index', compact('batas'));
    }

    public function kimiaringna(\Illuminate\Http\Request $request)
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.ring.insert', compact('parameter'));
    }

    public function kimiaringnaalat(\Illuminate\Http\Request $request)
    {
        $tahun = $request->query('tahun');
        $tipe = $request->query('tipe');
        $siklus = $request->query('siklus');

        $sd = DB::table('tb_instrumen')
            ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
            ->leftJoin('tb_ring_kk_alat', function ($join) use($tipe,$siklus,$tahun) {
                $join->on('tb_instrumen.id', '=', 'tb_ring_kk_alat.instrumen_id')
                     ->where('tb_ring_kk_alat.tipe', '=', $tipe)
                     ->where('tb_ring_kk_alat.tahun', '=', $tahun)
                     ->where('tb_ring_kk_alat.siklus', '=', $siklus)
                     ->where('tb_ring_kk_alat.form', '=', 'kimia klinik');
            })
            ->having('total_data','>=',8)
            ->where('parameter.kategori','=','kimia klinik')
            ->where('tb_instrumen.instrumen','!=','Alat Lain :')
            ->select('parameter.nama_parameter','parameter.id as id_parameter','tb_ring_kk_alat.*','tb_instrumen.instrumen as nama_instrumen','tb_instrumen.id as id_instrumen', DB::raw("(
                SELECT COUNT(*) from hp_details as HPD
                INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                WHERE
                HPD.alat = tb_instrumen.id
                AND
                `hp_headers`.`type` = '".$tipe."'
                AND
                `hp_headers`.`siklus` = '".$siklus."'
                AND
                YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."

            ) as total_data"))
            ->get();
        return view('evaluasi.kimiaklinik.ringalat.insert', compact('sd','tahun','siklus','tipe'));
    }

    public function kimiaringnametode(\Illuminate\Http\Request $request)
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.ringmetode.insert', compact('parameter'));
    }

    public function kimiaringnasave(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        // dd($input);
        $i = 0 ;
        foreach ($input['batas_bawah'] as $batas_bawah) {
            $Savedata = new BatasParameter;
            $Savedata->id_parameter = $input['id_parameter'];
            $Savedata->siklus = $input['siklus'];
            $Savedata->tahun = $input['tahun'];
            $Savedata->type = $input['type'];
            $Savedata->batas_bawah = $input['batas_bawah'][$i];
            $Savedata->batas_atas = $input['batas_atas'][$i];
            $Savedata->save();
            $i++;
        }

        Session::flash('message', 'Input Rentang telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-ring/kimiaklinik');
    }

    public function kimiaringnasavealat(\Illuminate\Http\Request $request)
    {
        $input = $request->input();
        foreach($input['data'] as $key => $r){
            if(!empty($r['id_tb_ring'])){
                $save = RingKKAlat::find($r['id_tb_ring']);
            }else{
                $save = new RingKKAlat;
            }
            unset($r['id_tb_ring']);
            $save->tahun = $input['tahun'];
            $save->siklus = $input['siklus'];
            $save->tipe = $input['tipe'];
            $save->form = 'kimia klinik';
            foreach($r as $ak => $val){
                $save->$ak = $val;
            }
            $save->save();
        }
        Session::flash('message', 'Input range telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-ring/kimiaklinik-alat');
    }

    public function kimiaringnasavemetode(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $i = 0 ;
        foreach ($input['batas_bawah'] as $batas_bawah) {
            $Savedata = new BatasMetode;
            $Savedata->id_parameter = $input['id_parameter'];
            $Savedata->metode = $input['metode'];
            $Savedata->siklus = $input['siklus'];
            $Savedata->tahun = $input['tahun'];
            $Savedata->type = $input['type'];
            $Savedata->batas_bawah = $input['batas_bawah'][$i];
            $Savedata->batas_atas = $input['batas_atas'][$i];
            $Savedata->save();
            $i++;
        }

        Session::flash('message', 'Input Rentang telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-ring-metode/kimiaklinik');
    }

    public function kimiaklinikjumlahkelompok()
    {
        return view('evaluasi.kimiaklinik.jumlahkelompok.index');
    }

    public function kimiaklinikjumlahkelompokna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $alat = DB::table('hp_details')
                ->leftjoin('parameter','parameter.id','=','hp_details.parameter_id')
                ->join('tb_instrumen', 'tb_instrumen.id', '=', 'hp_details.alat')
                ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                ->leftjoin('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->select('tb_instrumen.instrumen', 'parameter.nama_parameter', DB::raw('COUNT(alat) as jumlah'))
                ->groupBy('hp_details.alat')
                ->where('parameter.kategori', '=', 'kimia klinik')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.type', '=', $input['tipe'])
                ->where('hp_headers.siklus', '=', $input['siklus'])
                ->get();

        $alatlain = DB::table('hp_details')
                ->leftjoin('parameter','parameter.id','=','hp_details.parameter_id')
                ->join('tb_instrumen', 'tb_instrumen.id', '=', 'hp_details.alat')
                ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                ->leftjoin('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->select('hp_details.instrument_lain', DB::raw('COUNT(alat) as jumlah'))
                ->groupBy('hp_details.instrument_lain')
                ->where('parameter.kategori', '=', 'kimia klinik')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.type', '=', $input['tipe'])
                ->where('hp_headers.siklus', '=', $input['siklus'])
                ->where('tb_instrumen.instrumen', '=', 'Alat Lain :')
                ->get();

        $metode = DB::table('hp_details')
                ->leftjoin('parameter','parameter.id','=','hp_details.parameter_id')
                ->join('metode_pemeriksaan', 'metode_pemeriksaan.id', '=', 'hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                ->leftjoin('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->groupBy('hp_details.kode_metode_pemeriksaan')
                ->select('metode_pemeriksaan.metode_pemeriksaan', 'parameter.nama_parameter', DB::raw('COUNT(alat) as jmlah'))
                ->where('parameter.kategori', '=', 'kimia klinik')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.type', '=', $input['tipe'])
                ->where('hp_headers.siklus', '=', $input['siklus'])
                ->orderBy('parameter.id', 'asc')
                ->get();

        $metodelain = DB::table('hp_details')
                ->leftjoin('parameter','parameter.id','=','hp_details.parameter_id')
                ->join('metode_pemeriksaan', 'metode_pemeriksaan.id', '=', 'hp_details.kode_metode_pemeriksaan')
                ->join('hp_headers', 'hp_headers.id', '=', 'hp_details.hp_header_id')
                ->leftjoin('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->groupBy('hp_details.kode_metode_pemeriksaan', 'hp_details.metode_lain', 'parameter.nama_parameter')
                ->select('metode_pemeriksaan.metode_pemeriksaan', 'parameter.nama_parameter', 'hp_details.metode_lain', DB::raw('COUNT(alat) as jmlah'))
                ->where('parameter.kategori', '=', 'kimia klinik')
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->where('hp_headers.type', '=', $input['tipe'])
                ->where('hp_headers.siklus', '=', $input['siklus'])
                ->where('metode_pemeriksaan.metode_pemeriksaan', '=', 'Metode lain :')
                ->get();

        // dd($alatlain);
        if ($input['proses'] == 'Proses') {
            Excel::create('Laporan Jumlah Kelompok Peserta Kimia Klinik', function($excel) use ($metode, $metodelain, $alat, $alatlain, $input) {
                $excel->sheet('Alat', function($sheet) use ($alat, $input) {
                    $sheet->loadView('evaluasi.kimiaklinik.jumlahkelompok.viewalat', array('alat'=>$alat, 'input'=>$input) );
                });
                $excel->sheet('Metode', function($sheet) use ($metode, $input) {
                    $sheet->loadView('evaluasi.kimiaklinik.jumlahkelompok.viewmetode', array('metode'=>$metode, 'input'=>$input) );
                });
                $excel->sheet('Metode Lain', function($sheet) use ($metodelain, $input) {
                    $sheet->loadView('evaluasi.kimiaklinik.jumlahkelompok.viewmetodelain', array('metodelain'=>$metodelain, 'input'=>$input) );
                });
                $excel->sheet('Alat Lain', function($sheet) use ($alatlain, $input) {
                    $sheet->loadView('evaluasi.kimiaklinik.jumlahkelompok.viewalatlain', array('alatlain'=>$alatlain, 'input'=>$input) );
                });
            })->download('xls');
        }else{
            $pdf = PDF::loadview('evaluasi.kimiaklinik.jumlahkelompok.cetak', compact('alat', 'input', 'metode','metodelain','alatlain'))
                ->setPaper('a4', 'potrait')
                ->setwarnings(false);
            return $pdf->stream('Laporan Jumlah Kelompok Peserta Kimia Klinik.pdf');
        }
    }

    public function kimiaklinikmetodelaporan()
    {
        return view('evaluasi.kimiaklinik.laporanmetode.index');
    }

    public function kimiaklinikmetodelaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->join('perusahaan', 'perusahaan.id', '=', 'tb_registrasi.perusahaan_id')
                ->where('tb_registrasi.bidang', '=', '2')
                ->where('hp_headers.siklus', $input['siklus'])
                ->where('hp_headers.type', $input['tipe'])
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $input['tahun'])
                ->select('hp_headers.*', 'tb_registrasi.updated_at', 'perusahaan.nama_lab')
                ->orderBy('tb_registrasi.id', 'asc')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'kimia klinik')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->leftjoin('metode_pemeriksaan','metode_pemeriksaan.id','=','hp_details.kode_metode_pemeriksaan')
                            ->leftjoin('tb_instrumen','tb_instrumen.id','=','hp_details.alat')
                            ->where('hp_details.hp_header_id', $val->id)
                            ->where('hp_details.parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        Excel::create('Laporan Hasil Peserta Kimia Klinik', function($excel) use ($data, $parameterna) {
            $excel->sheet('Laporan', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.kimiaklinik.laporanmetode.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function kimiakliniksdmedian()
    {
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia klinik')
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->groupBy('tb_sd_median.tahun')
            ->groupBy('tb_sd_median.siklus')
            ->groupBy('tb_sd_median.tipe')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaklinik.sdmedian.index', compact('parameter','sd'));
    }

    public function kimiakliniksdmedianinsert()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        return view('evaluasi.kimiaklinik.sdmedian.view', compact('parameter','sd'));
    }

    public function kimiakliniksdmedianna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        $data = DB::table('tb_sd_median')
                ->where('form', '=', 'kimia klinik')
                ->where('tb_sd_median.tahun', $input['tahun'])
                ->where('tb_sd_median.siklus', $input['siklus'])
                ->where('tb_sd_median.tipe', $input['tipe'])
                ->count();
        // dd($data);
        if ($data == 0) {
            // dd($input);
            $i = 0;
            foreach ($input['parameter'] as $parameter) {
                $Savedata = new SdMedian;
                $Savedata->siklus = $input['siklus'];
                $Savedata->tahun = $input['tahun'];
                $Savedata->tipe = $input['tipe'];
                $Savedata->parameter = $input['parameter'][$i];
                $Savedata->sd = $input['sd'][$i];
                $Savedata->median = $input['median'][$i];
                $Savedata->form = 'kimia klinik';
                $Savedata->save();
                $i++;
            }

            Session::flash('message', 'Input SD dan Median telah berhasil!');
            Session::flash('alert-class', 'alert-success');
            return redirect('evaluasi/input-sd-median/kimiaklinik');
        }else{
            Session::flash('message', 'Input SD dan Median Gagal!');
            Session::flash('alert-class', 'alert-danger');
            // dd($Savedata);
            return redirect('evaluasi/input-sd-median/kimiaklinik');
        }
    }

    public function kimiakliniksdmedianprint($id)
    {
        $edit = DB::table('tb_sd_median')->where('tb_sd_median.id', $id)->first();
        // dd($edit);
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia klinik')
            ->where('tb_sd_median.tahun', $edit->tahun)
            ->where('tb_sd_median.siklus', $edit->siklus)
            ->where('tb_sd_median.tipe', $edit->tipe)
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->get();
        // dd($sd);
        $pdf = PDF::loadview('evaluasi.kimiaklinik.sdmedian.print', compact('edit', 'sd'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('SD dan Median per Parameter.pdf');
    }

    public function kimiakliniksdmedianedit($id)
    {
        $edit = DB::table('tb_sd_median')->where('tb_sd_median.id', $id)->first();
        // dd($edit);
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia klinik')
            ->where('tb_sd_median.tahun', $edit->tahun)
            ->where('tb_sd_median.siklus', $edit->siklus)
            ->where('tb_sd_median.tipe', $edit->tipe)
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->get();
        // dd($sd);
        return view('evaluasi.kimiaklinik.sdmedian.edit', compact('edit','sd'));
    }

    public function kimiakliniksdmedianupdate(\Illuminate\Http\Request $request)
    {
        $i = 0;
        foreach ($request->parameter as $parameter) {
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['tipe'] = $request->tipe;
            $Update['parameter'] = $request->parameter[$i];
            $Update['sd'] = $request->sd[$i];
            $Update['median'] = $request->median[$i];

            SdMedian::where('id',$request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median/kimiaklinik');
    }

    public function kimiakliniksdmediandelet($id){
        $edit = DB::table('tb_sd_median')->where('tb_sd_median.id', $id)->first();

        DB::table('tb_sd_median')->where('tahun', $edit->tahun)->where('siklus', $edit->siklus)->where('tipe', $edit->tipe)->where('form', '=', 'kimia klinik')->delete();

        Session::flash('message', 'Hapus Data SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-warning');

        return redirect('evaluasi/input-sd-median/kimiaklinik');
    }

    public function kimiakliniksdmedianalat()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia klinik')->get();
        $sd = DB::table('tb_sd_median_alat')
            ->where('form', '=', 'kimia klinik')
            ->orderBy('tb_sd_median_alat.id', 'desc')
            ->select('tb_sd_median_alat.*')
            ->groupBy('tahun')
            ->groupBy('tipe')
            ->groupBy('siklus')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaklinik.sdmedianalat.index', compact('parameter','sd'));
    }

    public function kimiakliniksdmedianalatinsert(\Illuminate\Http\Request $request)
    {
        $siklus = $request->siklus;
        $tahun = $request->tahun;
        $tipe = $request->tipe;
        $alat = DB::table('tb_instrumen')
                ->select('tb_instrumen.id', 'instrumen', 'id_parameter','nama_parameter','tb_sd_median_alat.sd',DB::raw('tb_sd_median_alat.id id_sd_med'),'tb_sd_median_alat.median', DB::raw("(
                            SELECT COUNT(*) from hp_details as HPD
                            INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                            WHERE
                            HPD.alat = tb_instrumen.id
                            AND
                            `hp_headers`.`type` = '".$tipe."'
                            AND
                            `hp_headers`.`siklus` = '".$siklus."'
                            AND
                            YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."

                        ) as total_data"))
                ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
                ->leftJoin('tb_sd_median_alat', function ($join) use($tipe,$siklus,$tahun) {
                    $join->on('tb_instrumen.id', '=', 'tb_sd_median_alat.alat')
                         ->where('tb_sd_median_alat.tipe', '=', $tipe)
                         ->where('tb_sd_median_alat.tahun', '=', $tahun)
                         ->where('tb_sd_median_alat.siklus', '=', $siklus)
                         ->where('tb_sd_median_alat.form', '=', 'kimia klinik');
                })
                ->where('parameter.kategori','=','kimia klinik')
                ->where('instrumen','!=','Alat Lain :')
                ->havingRaw('total_data >= 8')
                ->orderBy('parameter.id', 'asc')
                ->orderBy('instrumen', 'asc')
                ->get();
            // dd($alat);
        return view('evaluasi.kimiaklinik.sdmedianalat.view', compact('alat', 'siklus', 'tahun', 'tipe'));
    }

    public function kimiakliniksdmedianalatna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $i = 0;
        foreach ($input['alat'] as $alat) {
            if(!empty($input['id_sd_med'][$i])){
                $Savedata = SdMedianAlat::find($input['id_sd_med'][$i]);
            }else{
                $Savedata = new SdMedianAlat;
            }

            $Savedata->parameter = $input['parameter'][$i];
            $Savedata->siklus = $input['siklus'];
            $Savedata->tahun = $input['tahun'];
            $Savedata->tipe = $input['tipe'];
            $Savedata->alat = $input['alat'][$i];
            $Savedata->sd = $input['sd'][$i];
            $Savedata->median = $input['median'][$i];
            $Savedata->form = 'kimia klinik';
            $Savedata->save();

            $i++;
        }
        Session::flash('message', 'Input SD dan Median Alat / Instrument telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-sd-median-alat/kimiaklinik');
    }

    public function kimiakliniksdmedianalatprint(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->siklus;
        $tahun = $request->tahun;
        $tipe = $request->tipe;
        $alat = DB::table('tb_instrumen')
                ->select('tb_instrumen.id', 'instrumen', 'id_parameter','nama_parameter','tb_sd_median_alat.sd',DB::raw('tb_sd_median_alat.id id_sd_med'),'tb_sd_median_alat.median', DB::raw("(
                            SELECT COUNT(*) from hp_details as HPD
                            INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                            WHERE
                            HPD.alat = tb_instrumen.id
                            AND
                            `hp_headers`.`type` = '".$tipe."'
                            AND
                            `hp_headers`.`siklus` = '".$siklus."'
                            AND
                            YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."

                        ) as total_data"))
                ->join('parameter','parameter.id','=','tb_instrumen.id_parameter')
                ->leftJoin('tb_sd_median_alat', function ($join) use($tipe,$siklus,$tahun) {
                    $join->on('tb_instrumen.id', '=', 'tb_sd_median_alat.alat')
                         ->where('tb_sd_median_alat.tipe', '=', $tipe)
                         ->where('tb_sd_median_alat.tahun', '=', $tahun)
                         ->where('tb_sd_median_alat.siklus', '=', $siklus)
                         ->where('tb_sd_median_alat.form', '=', 'kimia klinik');
                })
                ->where('parameter.kategori','=','kimia klinik')
                ->where('instrumen','!=','Alat Lain :')
                ->havingRaw('total_data >= 8')
                ->orderBy('parameter.id', 'asc')
                ->orderBy('instrumen', 'asc')
                ->get();
            // dd($sd);
        $pdf = PDF::loadview('evaluasi.kimiaklinik.sdmedianalat.print', compact('request','alat'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('SD dan Median per Parameter.pdf');
    }

    public function kimiakliniksdmedianalatedit($id)
    {
        $edit = DB::table('tb_sd_median_alat')->where('id', $id)->first();
        // dd($edit);
        $sd = DB::table('tb_sd_median_alat')
            ->join('tb_instrumen','tb_instrumen.id','=','tb_sd_median_alat.alat')
            ->where('form', '=', 'kimia klinik')
            ->where('parameter', $edit->parameter)
            ->where('tahun', $edit->tahun)
            ->where('siklus', $edit->siklus)
            ->where('tipe', $edit->tipe)
            ->select('tb_sd_median_alat.*', 'tb_instrumen.instrumen')
            ->get();
            // dd($sd);
        return view('evaluasi.kimiaklinik.sdmedianalat.edit', compact('edit','sd'));
    }

    public function kimiakliniksdmedianalatupdate(\Illuminate\Http\Request $request, $id)
    {
        $i = 0;
        foreach ($request->alat as $alat) {
            $Update['parameter'] = $request->parameter;
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['tipe'] = $request->tipe;
            $Update['alat'] = $request->alat[$i];
            $Update['sd'] = $request->sd[$i];
            $Update['median'] = $request->median[$i];
            SdMedianAlat::where('id',$request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit SD dan Median Alat / Instrument telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-alat/kimiaklinik');
    }

    public function kimiakliniksdmedianalatdelet(\Illuminate\Http\Request $request,$id){
        $siklus = $request->siklus;
        $tahun = $request->tahun;
        $tipe = $request->tipe;

        DB::table('tb_sd_median_alat')
        ->where('tahun', $request->tahun)
        ->where('siklus', $request->siklus)
        ->where('tipe', $request->tipe)
        ->where('form', '=', 'kimia klinik')
        ->delete();

        Session::flash('message', 'Hapus Data SD dan Median Alat / Instrument telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-alat/kimiaklinik');
    }

    public function kimiakliniksdmedianmetode(\Illuminate\Http\Request $request)
    {
        $sd = DB::table('tb_sd_median_metode')
            ->where('form', '=', 'kimia klinik')
            ->orderBy('tb_sd_median_metode.id', 'desc')
            ->select('tb_sd_median_metode.*')
            ->groupBy('tahun')
            ->groupBy('tipe')
            ->groupBy('siklus')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaklinik.sdmedianmetode.index', compact('sd'));
    }

    public function kimiakliniksdmedianmetodeinsert(\Illuminate\Http\Request $request)
    {
        $siklus = $request->siklus;
        $tahun = $request->tahun;
        $tipe = $request->tipe;

        $metode = DB::table('metode_pemeriksaan')
                ->select('metode_pemeriksaan.id', 'metode_pemeriksaan.metode_pemeriksaan', 'parameter_id','nama_parameter','tb_sd_median_metode.sd',DB::raw('tb_sd_median_metode.id id_sd_med'),'tb_sd_median_metode.median',
                DB::raw("(
                        SELECT COUNT(*) from hp_details as HPD
                        INNER JOIN `hp_headers` ON `hp_headers`.`id` = `HPD`.`hp_header_id`
                        WHERE
                        HPD.kode_metode_pemeriksaan = metode_pemeriksaan.id
                        AND
                        `hp_headers`.`type` = '".$tipe."'
                        AND
                        `hp_headers`.`siklus` = '".$siklus."'
                        AND
                        YEAR(`hp_headers`.`tgl_pemeriksaan`) = ".$tahun."

                    ) as total_data")
                )
                ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
                ->leftJoin('tb_sd_median_metode', function ($join) use($tipe,$siklus,$tahun) {
                    $join->on('metode_pemeriksaan.id', '=', 'tb_sd_median_metode.metode')
                         ->where('tb_sd_median_metode.tipe', '=', $tipe)
                         ->where('tb_sd_median_metode.tahun', '=', $tahun)
                         ->where('tb_sd_median_metode.siklus', '=', $siklus)
                         ->where('tb_sd_median_metode.form', '=', 'kimia klinik');
                })
                ->where('parameter.kategori','=','kimia klinik')
                ->where('metode_pemeriksaan.metode_pemeriksaan','!=','Metode lain :')
                ->having('total_data','>=',8)
                ->orderBy('parameter.id', 'asc')
                ->orderBy('metode_pemeriksaan.metode_pemeriksaan', 'asc')
                ->get();

        // $metode = db::table('parameter')
        //         ->join('metode_pemeriksaan', 'metode_pemeriksaan.parameter_id','=','parameter.id')
        //         ->where('kategori', '=', 'kimia klinik')
        //         ->select('metode_pemeriksaan.*','parameter.nama_parameter')
        //         ->get();
            // dd($sd);
        return view('evaluasi.kimiaklinik.sdmedianmetode.view', compact('metode', 'siklus', 'tahun', 'tipe'));
    }

    public function kimiakliniksdmedianmetodena(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_sd_median_metode')
                ->where('form', '=', 'kimia klinik')
                ->where('tahun', $input['tahun'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['tipe'])
                ->where('parameter', $input['parameter'])
                ->count();
        $i = 0;
        foreach ($input['metode'] as $metode) {
            if(!empty($input['id_sd_med'][$i])){
                $Savedata = SdMedianMetode::find($input['id_sd_med'][$i]);
            }else{
                $Savedata = new SdMedianMetode;
            }
            $Savedata->siklus = $input['siklus'];
            $Savedata->tahun = $input['tahun'];
            $Savedata->tipe = $input['tipe'];
            $Savedata->parameter = $input['parameter'][$i];
            $Savedata->metode = $input['metode'][$i];
            $Savedata->sd = $input['sd'][$i];
            $Savedata->median = $input['median'][$i];
            $Savedata->form = 'kimia klinik';
            $Savedata->save();

            $i++;
        }
        Session::flash('message', 'Input SD dan Median Metode telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-sd-median-metode/kimiaklinik');

    }

    public function kimiakliniksdmedianmetodeprint(\Illuminate\Http\Request $request,$id)
    {
        $siklus = $request->siklus;
        $tahun = $request->tahun;
        $tipe = $request->tipe;

        $sd = DB::table('metode_pemeriksaan')
                ->select('metode_pemeriksaan.id', 'metode_pemeriksaan.metode_pemeriksaan', 'parameter_id','nama_parameter','tb_sd_median_metode.sd',DB::raw('tb_sd_median_metode.id id_sd_med'),'tb_sd_median_metode.median'
                )
                ->join('parameter','parameter.id','=','metode_pemeriksaan.parameter_id')
                ->leftJoin('tb_sd_median_metode', function ($join) use($tipe,$siklus,$tahun) {
                    $join->on('metode_pemeriksaan.id', '=', 'tb_sd_median_metode.metode')
                         ->where('tb_sd_median_metode.tipe', '=', $tipe)
                         ->where('tb_sd_median_metode.tahun', '=', $tahun)
                         ->where('tb_sd_median_metode.siklus', '=', $siklus)
                         ->where('tb_sd_median_metode.form', '=', 'kimia klinik');
                })
                ->where('parameter.kategori','=','kimia klinik')
                ->where('metode_pemeriksaan.metode_pemeriksaan','!=','Metode lain :')
                ->orderBy('parameter.id', 'asc')
                ->orderBy('metode_pemeriksaan.metode_pemeriksaan', 'asc')
                ->get();
        $pdf = PDF::loadview('evaluasi.kimiaklinik.sdmedianmetode.print', compact('request', 'sd'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('SD dan Median per Metode.pdf');
    }

    public function kimiakliniksdmedianmetodeedit($id)
    {
        $edit = DB::table('tb_sd_median_metode')->where('id', $id)->first();
        // dd($edit);
        $sd = DB::table('tb_sd_median_metode')
            ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_sd_median_metode.metode')
            ->join('parameter','parameter.id','=','tb_sd_median_metode.parameter')
            ->where('form', '=', 'kimia klinik')
            ->where('tahun', $edit->tahun)
            ->where('siklus', $edit->siklus)
            ->where('tipe', $edit->tipe)
            ->select('tb_sd_median_metode.*', 'metode_pemeriksaan.metode_pemeriksaan', 'parameter.nama_parameter')
            ->get();
            // dd($sd);
        return view('evaluasi.kimiaklinik.sdmedianmetode.edit', compact('edit','sd'));
    }

    public function kimiakliniksdmedianmetodeupdate(\Illuminate\Http\Request $request, $id)
    {
        $i = 0;
        foreach ($request->metode as $metode) {
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['tipe'] = $request->tipe;
            $Update['parameter'] = $request->parameter[$i];
            $Update['metode'] = $request->metode[$i];
            $Update['sd'] = $request->sd[$i];
            $Update['median'] = $request->median[$i];
            SdMedianMetode::where('id',$request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit SD dan Median Metode telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-metode/kimiaklinik');
    }

    public function kimiakliniksdmedianmetodedelet($id){

        $edit = DB::table('tb_sd_median_metode')->where('tb_sd_median_metode.id', $id)->first();

        DB::table('tb_sd_median_metode')
        ->where('tahun', $edit->tahun)
        ->where('siklus', $edit->siklus)
        ->where('tipe', $edit->tipe)
        ->where('form', '=', 'kimia klinik')
        ->delete();

        Session::flash('message', 'Hapus Data SD dan Median Metode telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-metode/kimiaklinik');
    }

    public function hematologisdmedianmetode()
    {
        $sd = DB::table('tb_sd_median_metode')
            ->join('parameter','parameter.id','=','tb_sd_median_metode.parameter')
            ->where('form', '=', 'hematologi')
            ->orderBy('tb_sd_median_metode.id', 'desc')
            ->select('tb_sd_median_metode.*', 'parameter.nama_parameter')
            ->groupBy('tahun')
            ->groupBy('tipe')
            ->groupBy('siklus')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.hematologi.sdmedianmetode.index', compact('sd'));
    }

    public function hematologisdmedianmetodeinsert(\Illuminate\Http\Request $request)
    {
        $metode = db::table('parameter')
                ->join('metode_pemeriksaan', 'metode_pemeriksaan.parameter_id','=','parameter.id')
                ->where('kategori', '=', 'hematologi')
                ->select('metode_pemeriksaan.*','parameter.nama_parameter')
                ->get();
            // dd($sd);
        return view('evaluasi.hematologi.sdmedianmetode.view', compact('metode'));
    }

    public function hematologisdmedianmetodena(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $data = DB::table('tb_sd_median_metode')
                ->where('form', '=', 'hematologi')
                ->where('tahun', $input['tahun'])
                ->where('siklus', $input['siklus'])
                ->where('tipe', $input['tipe'])
                ->where('parameter', $input['parameter'])
                ->count();
        if ($data == 0) {
            $i = 0;
            foreach ($input['metode'] as $metode) {
                $Savedata = new SdMedianMetode;
                $Savedata->siklus = $input['siklus'];
                $Savedata->tahun = $input['tahun'];
                $Savedata->tipe = $input['tipe'];
                $Savedata->parameter = $input['parameter'][$i];
                $Savedata->metode = $input['metode'][$i];
                $Savedata->sd = $input['sd'][$i];
                $Savedata->median = $input['median'][$i];
                $Savedata->form = 'hematologi';
                $Savedata->save();

                $i++;
            }
            Session::flash('message', 'Input SD dan Median Metode telah berhasil!');
            Session::flash('alert-class', 'alert-success');
            // dd($Savedata);
            return redirect('evaluasi/input-sd-median-metode/hematologi');
        }else{
            Session::flash('message', 'Input SD dan Median Metode Gagal!');
            Session::flash('alert-class', 'alert-danger');
            // dd($Savedata);
            return redirect('evaluasi/input-sd-median-metode/hematologi');
        }
    }

    public function hematologisdmedianmetodeprint($id)
    {
        $edit = DB::table('tb_sd_median_metode')->where('id', $id)->first();
        // dd($edit);
        $sd = DB::table('tb_sd_median_metode')
            ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_sd_median_metode.metode')
            ->join('parameter','parameter.id','=','tb_sd_median_metode.parameter')
            ->where('form', '=', 'hematologi')
            ->where('tahun', $edit->tahun)
            ->where('siklus', $edit->siklus)
            ->where('tipe', $edit->tipe)
            ->select('tb_sd_median_metode.*', 'metode_pemeriksaan.metode_pemeriksaan', 'parameter.nama_parameter')
            ->get();
            // dd($sd);
        $pdf = PDF::loadview('evaluasi.hematologi.sdmedianmetode.print', compact('edit', 'sd'))
            ->setPaper('a4', 'potrait')
            ->setwarnings(false);
        return $pdf->stream('SD dan Median per Metode.pdf');
    }

    public function hematologisdmedianmetodeedit($id)
    {
        $edit = DB::table('tb_sd_median_metode')->where('id', $id)->first();
        // dd($edit);
        $sd = DB::table('tb_sd_median_metode')
            ->join('metode_pemeriksaan','metode_pemeriksaan.id','=','tb_sd_median_metode.metode')
            ->join('parameter','parameter.id','=','tb_sd_median_metode.parameter')
            ->where('form', '=', 'hematologi')
            ->where('tahun', $edit->tahun)
            ->where('siklus', $edit->siklus)
            ->where('tipe', $edit->tipe)
            ->select('tb_sd_median_metode.*', 'metode_pemeriksaan.metode_pemeriksaan', 'parameter.nama_parameter')
            ->get();
            // dd($sd);
        return view('evaluasi.hematologi.sdmedianmetode.edit', compact('edit','sd'));
    }

    public function hematologisdmedianmetodeupdate(\Illuminate\Http\Request $request, $id)
    {
        $i = 0;
        foreach ($request->metode as $metode) {
            $Update['siklus'] = $request->siklus;
            $Update['tahun'] = $request->tahun;
            $Update['tipe'] = $request->tipe;
            $Update['parameter'] = $request->parameter[$i];
            $Update['metode'] = $request->metode[$i];
            $Update['sd'] = $request->sd[$i];
            $Update['median'] = $request->median[$i];
            SdMedianMetode::where('id',$request->id[$i])->update($Update);
            $i++;
        }
        // dd($input);
        Session::flash('message', 'Edit SD dan Median Metode telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-metode/hematologi');
    }

    public function hematologisdmedianmetodedelet($id){

        $edit = DB::table('tb_sd_median_metode')->where('tb_sd_median_metode.id', $id)->first();

        DB::table('tb_sd_median_metode')
        ->where('tahun', $edit->tahun)
        ->where('siklus', $edit->siklus)
        ->where('tipe', $edit->tipe)
        ->where('form', '=', 'hematologi')
        ->delete();

        Session::flash('message', 'Hapus Data SD dan Median Metode telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median-metode/hematologi');
    }

    public function perusahaankimiaklinik(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            $siklus = $request->get('siklus');
            $tahun = $request->get('amp;tahun');
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum, tb_registrasi.kode_lebpes'), 'tb_registrasi.id as idregistrasi')
                    ->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')
                    ->where('tb_registrasi.status', '>=', '2')
                    ->where(function($query) use ($siklus)
                        {
                            $query->where('siklus', '=', '12')
                                ->orwhere('siklus', $siklus);
                        })
                    ->where('tb_registrasi.bidang','=','2')
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun)
                    ->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data) use ($siklus, $tahun){
                    $evaluasi = db::table('tb_zscore')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->orderBy('type', 'desc')
                                ->orderBy('id', 'asc')
                                ->first();
                    return "".'<a href="kimiaklinik/'.$data->id.'?siklus='.$siklus.'&tahun='.$tahun.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Proses
                                            </button>
                                        </a>';
                ;})
            ->addColumn('posisia', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'a')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->addColumn('posisib', function($data) use ($siklus,$tahun){
                    $evaluasi = db::table('tb_zscore_metode')
                                ->where('id_registrasi', $data->idregistrasi)
                                ->where('tahun', '=' , $tahun)
                                ->where('siklus', '=', $siklus)
                                ->where('type', '=', 'b')
                                ->orderBy('id', 'asc')
                                ->first();
                    $inputhasil = DB::table('tb_registrasi')
                                ->where('id', $data->idregistrasi)
                                ->first();
                    if(!empty($evaluasi)){
                        return '<div class="status-success">Kirim Hasil</div><div class="status-success">Evaluasi</div>';
                    }else{
                        if($siklus == 1) {
                            if ($inputhasil->status_data1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }else{
                            if ($inputhasil->status_datarpr1 == '2' ) {
                                return '<div class="status-success">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }else{
                                return '<div class="status-danger">Kirim Hasil</div><div class="status-danger">Evaluasi</div>';
                            }
                        }
                    }
                ;})
            ->rawColumns(['action' => 'action', 'posisia' =>  'posisia', 'posisib' =>  'posisib'])
            ->make(true);
        }
        $jobPending = JobsEvaluasi::where('name','=','evaluasi-kimiaklinik')->where('status','=',0)->first();
        $showAllertPending = false;
        $showAllertSuccess = false;
        $showForm = true;
        if(!empty($jobPending)){
            $showAllertPending = true;
            $showAllertSuccess = false;
            $showForm = false;
        }else{
            $jobSuccess = JobsEvaluasi::where('name','=','evaluasi-kimiaklinik')->where('status','=',1)->where('isread','=',0)->first();
            if(!empty($jobSuccess)){
                $showAllertPending = false;
                $showAllertSuccess = true;
                $showForm = true;
                JobsEvaluasi::where('name','=','evaluasi-kimiaklinik')->where('status','=',1)->where('isread','=',0)->update(['isread'=>1]);
            }else{
                $showForm = true;
            }
        }
        // $evaluasi = TanggalEvaluasi::where('form','=','Kimiaklinik')->first(); 
        return view('evaluasi.kimiaklinik.zscore.perusahaan',compact('showAllertPending','showAllertSuccess','showForm'));
    }

    public function dataperusahaankimiaklinik(\Illuminate\Http\Request $request, $id)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang',DB::raw('YEAR(tb_registrasi.created_at) as tanggalna'))
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where(DB::raw('YEAR(tb_registrasi.created_at)'), '=' , $tahun )
                ->where('sub_bidang.id', '=', '2')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        // dd($data);
        return view('evaluasi.kimiaklinik.zscore.dataperusahaan', compact('data','siklus','tahun'));
    }


    public function kimiaairlaporan()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air')->get();
        return view('evaluasi.kimiaair.laporanhasilpeserta.index', compact('parameter'));
    }

    public function kimiaairlaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'kimia air')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('tb_registrasi.bidang', '=', '11')
                ->where('hp_headers.siklus', $input['siklus'])
                ->select('hp_headers.*')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'kimia air')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        Excel::create('Laporan Hasil Peserta Per Parameter Kimia Air', function($excel) use ($data, $parameterna) {
            $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.kimiaair.laporanhasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function kimiaairsdmedian()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air')->get();
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia air')
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaair.sdmedian.index', compact('parameter','sd'));
    }

    public function kimiaairsdmedianna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $Savedata = new SdMedian;
        $Savedata->parameter = $input['parameter'];
        $Savedata->siklus = $input['siklus'];
        $Savedata->tahun = $input['tahun'];
        $Savedata->sd = $input['sd'];
        $Savedata->median = $input['median'];
        $Savedata->form = 'kimia air';
        $Savedata->save();

        Session::flash('message', 'Input SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-sd-median/kimiaair');
    }

    public function kimiaairsdmedianedit($id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air')->get();
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia air')
            ->where('tb_sd_median.id', $id)
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->first();
            // dd($sd);
        return view('evaluasi.kimiaair.sdmedian.view', compact('parameter','sd'));
    }

    public function kimiaairsdmedianupdate(\Illuminate\Http\Request $request, $id)
    {
        $Update['parameter'] = $request->parameter;
        $Update['siklus'] = $request->siklus;
        $Update['tahun'] = $request->tahun;
        $Update['tipe'] = $request->tipe;
        $Update['sd'] = $request->sd;
        $Update['median'] = $request->median;

        SdMedian::where('id',$id)->update($Update);
        // dd($input);
        Session::flash('message', 'Edit SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median/kimiaair');
    }

    public function kimiaairsdmediandelet($id){
        Session::flash('message', 'Hapus Data SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        SdMedian::find($id)->delete();
        return redirect('evaluasi/input-sd-median/kimiaair');
    }

    public function perusahaankimiaair(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes,tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','11')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('form','=','kimia air')
                                    ->orderBy('id', 'desc')
                                    ->first();
                    if(count($evaluasi)){
                        if ($evaluasi->siklus == '1') {
                            return "".'<a href="kimiaair/'.$data->id.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                evaluasi
                                            </button>
                                        </a>';
                        }elseif($evaluasi->siklus == '2'){
                            return "".'<a href="kimiaair/'.$data->id.'" style="float: left;">
                                            <button class="btn btn-success btn-xs">
                                                evaluasi
                                            </button>
                                        </a>';
                        }else{
                            return "".'<a href="kimiaair/'.$data->id.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                evaluasi
                                            </button>
                                        </a>';
                        }
                    }else{
                        return "".'<a href="kimiaair/'.$data->id.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                                evaluasi
                                        </button>
                                    </a>';
                    }
                ;})
            ->make(true);
        }
        return view('evaluasi.kimiaair.zscore.perusahaan');
    }

    public function dataperusahaankimiaair($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '11')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        
        return view('evaluasi.kimiaair.zscore.dataperusahaan', compact('data'));
    }

    public function perusahaankimiaairsertifikat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes,tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','11')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('form','=','kimia air')
                                    ->orderBy('id', 'desc')
                                    ->first();
                    if(count($evaluasi)){
                        if ($evaluasi->siklus == '1') {
                            return "".'<a href="kimiaair/'.$data->id.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                cetak
                                            </button>
                                        </a>';
                        }elseif($evaluasi->siklus == '2'){
                            return "".'<a href="kimiaair/'.$data->id.'" style="float: left;">
                                            <button class="btn btn-success btn-xs">
                                                cetak
                                            </button>
                                        </a>';
                        }else{
                            return "".'<a href="kimiaair/'.$data->id.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                cetak
                                            </button>
                                        </a>';
                        }
                    }else{
                        return "".'<a href="kimiaair/'.$data->id.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                                cetak
                                        </button>
                                    </a>';
                    }
                ;})
            ->make(true);
        }
        return view('evaluasi.kimiaair.zscore.perusahaansertifikat');
    }

    public function dataperusahaankimiaairsertifikat($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '11')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaair.zscore.dataperusahaansertifikat', compact('data'));
    }

    public function kimiaairterbataslaporan()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air terbatas')->get();
        return view('evaluasi.kimiaairterbatas.laporanhasilpeserta.index', compact('parameter'));
    }

    public function kimiaairterbataslaporanna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $input = $request->all();
        $parameterna = DB::table('parameter')->where('kategori', 'kimia air terbatas')->get();
        $data = DB::table('hp_headers')
                ->join('tb_registrasi','tb_registrasi.id','=','hp_headers.id_registrasi')
                ->where('tb_registrasi.bidang', '=', '12')
                ->where('hp_headers.siklus', $input['siklus'])
                ->select('hp_headers.*')
                ->get();
        foreach ($data as $key => $val) {
            $parameter = DB::table('parameter')->where('kategori', 'kimia air terbatas')->get();
            foreach ($parameter as $key => $par) {
                $detail = DB::table('hp_details')
                            ->where('hp_header_id', $val->id)
                            ->where('parameter_id', $par->id)
                            ->get();
                $par->detail = $detail;
            }
            $val->parameter = $parameter;
        }
        // dd($data);
        Excel::create('Laporan Hasil Peserta Per Parameter Kimia Air Terbatas', function($excel) use ($data, $parameterna) {
            $excel->sheet('Wilayah', function($sheet) use ($data, $parameterna) {
                $sheet->loadView('evaluasi.kimiaairterbatas.laporanhasilpeserta.view', array('data'=>$data, 'parameterna'=>$parameterna) );
            });
        })->download('xls');
    }

    public function kimiaairterbatassdmedian()
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air terbatas')->get();
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia air terbatas')
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->paginate(8);
            // dd($sd);
        return view('evaluasi.kimiaairterbatas.sdmedian.index', compact('parameter','sd'));
    }

    public function kimiaairterbatassdmedianna(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        $Savedata = new SdMedian;
        $Savedata->parameter = $input['parameter'];
        $Savedata->siklus = $input['siklus'];
        $Savedata->tahun = $input['tahun'];
        $Savedata->sd = $input['sd'];
        $Savedata->median = $input['median'];
        $Savedata->form = 'kimia air terbatas';
        $Savedata->save();

        Session::flash('message', 'Input SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        // dd($Savedata);
        return redirect('evaluasi/input-sd-median/kimiaairterbatas');
    }

    public function kimiaairterbatassdmedianedit($id)
    {
        $parameter = DB::table('parameter')->where('kategori','=','kimia air terbatas')->get();
        $sd = DB::table('tb_sd_median')
            ->join('parameter','parameter.id','=','tb_sd_median.parameter')
            ->where('form', '=', 'kimia air terbatas')
            ->where('tb_sd_median.id', $id)
            ->orderBy('tb_sd_median.id', 'desc')
            ->select('tb_sd_median.*', 'parameter.nama_parameter')
            ->first();
            // dd($sd);
        return view('evaluasi.kimiaairterbatas.sdmedian.view', compact('parameter','sd'));
    }

    public function kimiaairterbatassdmedianupdate(\Illuminate\Http\Request $request, $id)
    {
        $Update['parameter'] = $request->parameter;
        $Update['siklus'] = $request->siklus;
        $Update['tahun'] = $request->tahun;
        $Update['tipe'] = $request->tipe;
        $Update['sd'] = $request->sd;
        $Update['median'] = $request->median;

        SdMedian::where('id',$id)->update($Update);
        // dd($input);
        Session::flash('message', 'Edit SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-success');
        return redirect('evaluasi/input-sd-median/kimiaairterbatas');
    }

    public function kimiaairterbatassdmediandelet($id){
        Session::flash('message', 'Hapus Data SD dan Median telah berhasil!');
        Session::flash('alert-class', 'alert-warning');
        SdMedian::find($id)->delete();
        return redirect('evaluasi/input-sd-median/kimiaairterbatas');
    }

    public function perusahaankimiaairterbatas(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes,tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','12')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('form','=','kimia air terbatas')
                                    ->orderBy('id', 'desc')
                                    ->first();
                    if(count($evaluasi)){
                        if ($evaluasi->siklus == '1') {
                            return "".'<a href="kimiaairterbatas/'.$data->id.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                evaluasi
                                            </button>
                                        </a>';
                        }elseif($evaluasi->siklus == '2'){
                            return "".'<a href="kimiaairterbatas/'.$data->id.'" style="float: left;">
                                            <button class="btn btn-success btn-xs">
                                                evaluasi
                                            </button>
                                        </a>';
                        }else{
                            return "".'<a href="kimiaairterbatas/'.$data->id.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                evaluasi
                                            </button>
                                        </a>';
                        }
                    }else{
                        return "".'<a href="kimiaairterbatas/'.$data->id.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                                evaluasi
                                        </button>
                                    </a>';
                    }
                ;})
            ->make(true);
        }
        return view('evaluasi.kimiaairterbatas.zscore.perusahaan');
    }

    public function dataperusahaankimiaairterbatas($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '12')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaairterbatas.zscore.dataperusahaan', compact('data'));
    }

    public function perusahaankimiaairterbatassertifikat(\Illuminate\Http\Request $request)
    {
        if($request->ajax()){
            DB::statement(DB::raw("set @rownum=0"));
            $datas = Daftar::select( DB::raw('perusahaan.*, @rownum := @rownum +1 as rownum,tb_registrasi.kode_lebpes,tb_registrasi.id as idregistrasi'))->join('tb_registrasi','perusahaan.id','=','tb_registrasi.perusahaan_id')->where('tb_registrasi.bidang','=','12')->get();
            return Datatables::of($datas)
            ->addColumn('action', function($data){
                    $evaluasi = DB::table('tb_zscore')
                                    ->where('id_registrasi', $data->idregistrasi)
                                    ->where('form','=','kimia air terbatas')
                                    ->orderBy('id', 'desc')
                                    ->first();
                    if(count($evaluasi)){
                        if ($evaluasi->siklus == '1') {
                            return "".'<a href="kimiaairterbatas/'.$data->id.'" style="float: left;">
                                            <button class="btn btn-info btn-xs">
                                                Cetak
                                            </button>
                                        </a>';
                        }elseif($evaluasi->siklus == '2'){
                            return "".'<a href="kimiaairterbatas/'.$data->id.'" style="float: left;">
                                            <button class="btn btn-success btn-xs">
                                                Cetak
                                            </button>
                                        </a>';
                        }else{
                            return "".'<a href="kimiaairterbatas/'.$data->id.'" style="float: left;">
                                            <button class="btn btn-primary btn-xs">
                                                Cetak
                                            </button>
                                        </a>';
                        }
                    }else{
                        return "".'<a href="kimiaairterbatas/'.$data->id.'" style="float: left;">
                                        <button class="btn btn-primary btn-xs">
                                                Cetak
                                        </button>
                                    </a>';
                    }
                ;})
            ->make(true);
        }
        return view('evaluasi.kimiaairterbatas.zscore.perusahaansertifikat');
    }

    public function dataperusahaankimiaairterbatassertifikat($id)
    {
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.perusahaan_id', $id)
                ->where('sub_bidang.id', '=', '12')
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', 'done')
                            ->orwhere('pemeriksaan', 'done')
                            ->orwhere('rpr1', 'done')
                            ->orwhere('rpr2', 'done');
                    })
                ->get();
        return view('evaluasi.kimiaairterbatas.zscore.dataperusahaansertifikat', compact('data'));
    }
}
