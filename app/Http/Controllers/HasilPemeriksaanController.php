<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use App\CatatanImun;
use App\TandaTerima;

class HasilPemeriksaanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahun = date('Y');
        $user = Auth::user()->id;
        $siklus = DB::table('tb_siklus')->first();
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->whereYear('tb_registrasi.created_at', $tahun)
                ->where('status', '>=', '2')
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', NULL)
                        ->orWhere('pemeriksaan', NULL)
                        ->orWhere('siklus_1',NULL)
                        ->orWhere('siklus_2',NULL)
                        ->orWhere('rpr1',NULL)
                        ->orWhere('rpr2',NULL);
                    })
                ->get();
                // dd($siklus);
        return view('hasil_pemeriksaan', compact('data','siklus'));
    }

    public function editdata()
    {
        $user = Auth::user()->id;
        $siklus = DB::table('tb_siklus')->first();
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where('status', '>=', '2')
                ->where(function($query)
                    {
                        $query->where('status_data1', 1)
                            ->orwhere('status_data2', 1)
                            ->orwhere('status_datarpr1', 1)
                            ->orwhere('status_datarpr2', 1);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
                // dd($data);
        return view('edit_hasil', compact('data','siklus'));
    }

    public function cetak()
    {
        $user = Auth::user()->id;
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where('status', '>=', '2')
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        // dd($data);
        $data2 =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 1)
                            ->orwhere('status_data2', 1)
                            ->orwhere('status_datarpr1', 1)
                            ->orwhere('status_datarpr2', 1);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        return view('cetak_hasil', compact('data', 'data2'));
    }

    public function hasilevaluasi()
    {
        $user = Auth::user()->id;
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 2)
                            ->orwhere('status_data2', 2)
                            ->orwhere('status_datarpr1', 2)
                            ->orwhere('status_datarpr2', 2);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        // dd($data);
        $data2 =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang','sub_bidang.id as SubBidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 1)
                            ->orwhere('status_data2', 1)
                            ->orwhere('status_datarpr1', 1)
                            ->orwhere('status_datarpr2', 1);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        return view('hasil_evaluasi', compact('data', 'data2'));
    }
    public function cetaksementara()
    {
        $user = Auth::user()->id;
        $data =  DB::table('tb_registrasi')
                ->join('sub_bidang', 'sub_bidang.id', '=', 'tb_registrasi.bidang')
                ->join('tb_bidang', 'sub_bidang.id_bidang', '=', 'tb_bidang.id')
                ->select('tb_registrasi.*', 'sub_bidang.parameter as Parameter', 'sub_bidang.id_bidang', 'sub_bidang.link as Link', 'tb_bidang.bidang as Bidang')
                ->where('tb_registrasi.created_by', $user)
                ->where('status', 3)
                ->where(function($query)
                    {
                        $query->where('status_data1', 1)
                            ->orwhere('status_data2', 1)
                            ->orwhere('status_datarpr1', 1)
                            ->orwhere('status_datarpr2', 1);
                    })
                ->where(function($query)
                    {
                        $query->where('pemeriksaan2', '=' , 'done')
                                ->orwhere('pemeriksaan', '=' , 'done')
                                ->orwhere('siklus_1', '=' , 'done')
                                ->orwhere('siklus_2', '=' , 'done');
                    })
                ->get();
        // dd($data);
        return view('cetak_sementara', compact('data'));
    }

    public function TandaTerima()
    {
        return view('laporan.tanda-terima.proses');
    }

    public function TandaTerimaP(\Illuminate\Http\Request $request)
    {
        $siklus = $request->get('siklus');
        $tahun = $request->get('tahun');
        // dd($siklus);
        $perusahaan = DB::table('perusahaan')->where('created_by', Auth::user()->id)->first();

        $sudah = DB::table('tb_registrasi')
                    ->join('sub_bidang', 'tb_registrasi.bidang','=','sub_bidang.id')
                    ->join('tb_tanda_terima', 'tb_tanda_terima.id_registrasi', '=', 'tb_registrasi.id')
                    ->where('perusahaan_id', $perusahaan->id)
                    ->where(function ($query) use ($siklus, $tahun){
                        if($siklus == 1){
                            $query->whereIn('tb_registrasi.siklus', ['1','12']);
                        }else{
                            $query->whereIn('tb_registrasi.siklus', ['2','12']);
                        }
                    })
                    ->where('tb_tanda_terima.siklus', $siklus)
                    ->whereYear('tb_tanda_terima.tanggal', '=', $tahun)
                    ->select('tb_registrasi.*', 'sub_bidang.parameter')
                    ->get();
        $hsudah = count($sudah);
        $data = DB::table('tb_registrasi')
                    ->join('sub_bidang', 'tb_registrasi.bidang','=','sub_bidang.id')
                    ->where('perusahaan_id', $perusahaan->id)
                    ->where(function ($query) use ($siklus, $tahun){
                        if($siklus == 1){
                            $query->whereIn('tb_registrasi.siklus', ['1','12']);
                        }else{
                            $query->whereIn('tb_registrasi.siklus', ['2','12']);
                        }
                    })
                    ->where(DB::raw('YEAR(tb_registrasi.created_at)'), $tahun)
                    ->select('tb_registrasi.*', 'sub_bidang.parameter')
                    ->get();
        $hdata = count($data);
        foreach ($data as $key => $val) {
            $tanda = DB::table('tb_tanda_terima')
                        ->where('id_registrasi', $val->id)
                        ->where('siklus', $siklus)
                        ->whereYear('tanggal', '=', $tahun)
                        ->first();
            $val->penerima = $tanda;
        }
        // dd($data);
        return view('laporan.tanda-terima.index', compact('perusahaan', 'data', 'siklus', 'tahun','sudah','hsudah','hdata'));
    }

    public function InsertTerima(\Illuminate\Http\Request $request)
    {
        $siklus = $request->get('siklus');
        $input = $request->all();
        // dd($input);
        $no=0;
        $perusahaan = DB::table('perusahaan')->where('created_by', Auth::user()->id)->first();
        if(isset($input['id_registrasi'])){
            foreach ($input['id_registrasi'] as $key => $val) {
                if ($input['jumlah'][$no] != '' || $input['jumlah'][$no] != NULL) {
                    $Save = new TandaTerima;
                    $Save->tanggal = $input['tanggal'];
                    $Save->siklus = $siklus;
                    $Save->penerima = $input['penerima'];
                    $Save->id_registrasi = $input['id_registrasi'][$no];
                    $Save->jumlah = $input['jumlah'][$no];
                    $Save->keterangan = $input['keterangan'][$no];
                    $Save->kondisi = $input['kondisi'][$no];
                    $no++;
                    $Save->save();
                }
            }
        }
        Session::flash('message', 'Tanda terima Barang Sudah di Kirim'); 
        Session::flash('alert-class', 'alert-success'); 
        return Redirect('tanda-terima-bahan');
    }
}