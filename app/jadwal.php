<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class jadwal extends Model{
    protected $table = 'tb_jadwal';
    protected $fillable = ['judul','isi']; 
}