<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rujukanurinalisa extends Model{
    protected $table = 'tb_rujukan_urinalisa';
    protected $fillable = ['*'];
    public $timestamps = false;
}