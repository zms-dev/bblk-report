<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model{
    protected $table = 'tb_banner';
    protected $fillable = ['judul','teaser','image','_token','created_by']; 
}