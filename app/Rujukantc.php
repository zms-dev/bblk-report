<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rujukantc extends Model{
    protected $table = 'tb_rujukan_tc';
    protected $fillable = ['kode_sediaan','rujukan','form','siklus','tahun'];
    public $timestamps = false;
}