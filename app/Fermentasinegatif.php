<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fermentasinegatif extends Model{
    protected $table = 'tb_fermentasi_negatif';

    protected $fillable = ['*'];
    public $timestamps = false;
}