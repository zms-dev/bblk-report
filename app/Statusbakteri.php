<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Statusbakteri extends Model{
    protected $table = 'tb_status_bakteri';
        public $timestamps = false;
    protected $fillable = ['*'];

    // protected $table = 'tb_cobaan';
    // protected $fillable = ['id_registrasi','siklus','kode_peserta','tgl_penerimaan','tgl_pemeriksaan','kualitas','kolesterol','trigliserida','jenis','catatan','penanggung_jawab','created_by'];
}