<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SertfikatInsert extends Model
{
	protected $table = 'tb_sertifikat';
    protected $fillable = ['*'];
    public $timestamps = false;	
}
