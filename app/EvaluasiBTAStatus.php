<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EvaluasiBTAStatus extends Model{
    protected $table = 'tb_evaluasi_bta_status';

    protected $fillable = ['*'];
    public $timestamps = false;
}