<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadLaporan extends Model
{
    protected $table = 'upload_lap_akhir';
    protected $fillable = ['*'];
}
