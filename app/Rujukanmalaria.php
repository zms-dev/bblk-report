<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rujukanmalaria extends Model{
    protected $table = 'tb_rujukan_malaria';
    protected $fillable = ['kode_bahan_uji','rujukan','spesies','stadium','parasit','siklus','tahun'];
    public $timestamps = false;
}