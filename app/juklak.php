<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Juklak extends Model{
    protected $table = 'juklak';
    protected $fillable = ['id_bidang','file','created_by']; 
}