<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subbidang extends Model{
    protected $table = 'sub_bidang';
    protected $fillable = ['id_bidang','parameter','tarif','link','siklus','_token','created_by','kuota_1','kuota_2']; 
}