<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Daftar extends Model{
    protected $table = 'perusahaan';
    protected $fillable = ['nama_lab','pemerintah','swasta','alamat','provinsi','kota','kecamatan','kelurahan','telp','penanggung_jawab','personal','no_hp','created_by', 'id_pembayaran', 'email','no_wa','no_urut'];
}