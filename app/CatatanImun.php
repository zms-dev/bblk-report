<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatatanImun extends Model{
    protected $table = 'catatan_evaluasi_imunologi';

    protected $fillable = ['*'];
    public $timestamps = false;
}