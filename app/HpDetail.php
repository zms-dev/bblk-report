<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HpDetail extends Model
{
    protected $fillable = ['hp_detail_id', 'parameter_id', 'alat', 'kode_metode_pemeriksaan', 'hasil_pemeriksaan','metode_lain','catatan','instrument_lain','reagen','reagen_lain'];

    public $timestamps = false;
}
