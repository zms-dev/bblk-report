<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class kimiaklinik extends Model{
    protected $table = 'hp_kimiaklinik';
    protected $fillable = ['id_registrasi','siklus','kode_peserta','tgl_penerimaan','tgl_pemeriksaan','kualitas','kolesterol','trigliserida','bilirubin','got','gpt','protein','albumin','fosfatase','gamma','ureum','kreatinin','asamurat','glukosa','jenis','catatan','penanggung_jawab','created_by'];


    // protected $table = 'tb_cobaan';
    // protected $fillable = ['id_registrasi','siklus','kode_peserta','tgl_penerimaan','tgl_pemeriksaan','kualitas','kolesterol','trigliserida','jenis','catatan','penanggung_jawab','created_by'];
}