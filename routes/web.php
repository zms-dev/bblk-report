<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('index');
});
Route::get('/sessionregister', function () {
            Session::flash('message', 'Selamat, Anda telah berhasil melakukan Pendaftaran, Kami akan mengecek data yang telah anda input, Anda akan menerima email konfirmasi jika kami telah selesai melakukan verifikasi terhadap data yang anda kirimkan, Terimakasih'); 
            Session::flash('alert-class', 'alert-success'); 
    return redirect('index');
});

Route::get('/home', function () {
    return redirect('index');
});

Route::get('/email-blast', function () {
    return view('email_blast');
});

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::post('/email-blast', 'HomeController@sentEmail');

Route::get('login', [
  'as' => 'login',
  'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('login', [
  'as' => '',
  'uses' => 'Auth\LoginController@login'
]);
Route::post('logout', [
  'as' => 'logout',
  'uses' => 'Auth\LoginController@logout'
]);

// Password Reset Routes...
Route::post('password/email', [
  'as' => 'password.email',
  'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
]);
Route::get('password/reset', [
  'as' => 'password.request',
  'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
]);
Route::post('password/reset', [
  'as' => 'password.update',
  'uses' => 'Auth\ResetPasswordController@reset'
]);
Route::get('password/reset/{token}', [
  'as' => 'password.reset',
  'uses' => 'Auth\ResetPasswordController@showResetForm'
]);

// Registration Routes...
Route::post('register', [
  'as' => '',
  'uses' => 'Auth\RegisterController@register'
]);

Route::get('/index', 'HomeController@index');
Route::get('/download-sptjm', 'HomeController@sptjm');
Route::get('/juklak', 'HomeController@juklak');
Route::get('/download-manual-book', 'HomeController@manual');
Route::get('/download-penggunaan', 'HomeController@penggunaan');
Route::get('/download_evaluasi', 'HomeController@evaluasi');
Route::get('/cetak-sertifikat/{id}', 'HomeController@sertifikat');
Route::get('/tarif', 'HomeController@tarif');
Route::get('/jadwal', 'HomeController@jadwal');

$menu = DB::table('tb_hide_menu')->where('menu', '=', 'Daftar PNPME')->first();
if ( $menu->status == 'muncul' )
{
	Route::get('register', [
	  'as' => 'register',
	  'uses' => 'Auth\RegisterController@showRegistrationForm'
	]);
	Route::get('/daftar', 'DaftarController@index');
}else{
	Route::get('/daftar', function () {return redirect('index');});
	Route::get('/register', function () {return redirect('index');});
}

Route::post('/daftar_old', 'DaftarController@daftar_old');
Route::post('/daftar', 'DaftarController@daftar');

Route::get('/status-pendaftaran', 'DaftarController@status');
Route::get('/hasil-pemeriksaan', 'HasilPemeriksaanController@index');
Route::get('/cetak-hasil', 'HasilPemeriksaanController@cetak');
Route::get('/hasil-evaluasi', 'HasilPemeriksaanController@hasilevaluasi');
Route::get('/cetak-sementara', 'HasilPemeriksaanController@cetaksementara');
Route::get('/edit-hasil', 'HasilPemeriksaanController@editdata');

Route::get('/tanda-terima-bahan', 'HasilPemeriksaanController@TandaTerima');
Route::get('/tanda-terima-bahan/proses', 'HasilPemeriksaanController@TandaTerimaP');
Route::post('/tanda-terima-bahan/proses', 'HasilPemeriksaanController@InsertTerima');

Route::get('/laporan/tanda-terima', 'PendapatController@viewtanda');
Route::get('/laporan/tanda-terima/{id}', 'PendapatController@tandapeserta');
Route::get('/laporan/tanda-terima/view/{id}', 'PendapatController@lihattanda');

Route::get('/laporan/rekap-tanda-terima', 'PendapatController@rekaptanda');
Route::post('/laporan/rekap-tanda-terima', 'PendapatController@printrekaptanda');

Route::get('/cek_transfer', 'CekTransferController@index');
Route::post('/cek_transfer', 'CekTransferController@proses');
Route::get('/cek-kwitansi', 'CekTransferController@cek_kwitansi');
Route::post('/cetak-kwitansi/{id}', 'CekTransferController@cetak_kwitansi');
Route::get('/kuitansi-pks', 'CekTransferController@pks');
Route::get('/kuitansi-pks/{id}', 'CekTransferController@kuitansi_pks');

Route::get('/data-evaluasi', 'DataEvaluasiController@index');
Route::post('/data-evaluasi','DataEvaluasiController@index');

Route::get('/hasil-pemeriksaan/hematologi/data-evaluasi/{id}','DataEvaluasiController@hematologizscore');
Route::post('/hasil-pemeriksaan/hematologi/data-evaluasi/{id}','DataEvaluasiController@hematologizscore');
Route::get('data-evaluasi/hematologi/sertifikat/print/{id}', 'DataEvaluasiController@cetakhemserti');

Route::get('/hasil-pemeriksaan/kimia-klinik/data-evaluasi/{id}','DataEvaluasiController@kimiaklinik');
Route::post('/hasil-pemeriksaan/kimia-klinik/data-evaluasi/{id}','DataEvaluasiController@kimiaklinik');
Route::get('/sertifikat/kimia-klinik/data-evaluasi/{id}','DataEvaluasiController@kimiakliniksertifikat');


Route::get('/hasil-pemeriksaan/urinalisasi/data-evaluasi/{id}','DataEvaluasiController@uri');
Route::get('data-evaluasi/urinalisa/sertifikat/print/{id}','DataEvaluasiController@urisertifikat');
Route::post('/hasil-pemeriksaan/urinalisasi/data-evaluasi/{id}','DataEvaluasiController@uri');
// Route::get('/urinalisasi/data-evaluasi/{id}', 'DataEvaluasiController@evaluasi');
// Route::post('/urinalisasi/data-evaluasi/{id}', 'DataEvaluasiController@evaluasi');
// // 'UrinalisasiController@printevaluasi'
Route::get('/urinalisasi/data-evaluasi/print/{id}', 'DataEvaluasiController@printevaluasiuri');
Route::get('urinalisasi/data-evaluasi/penilaian-parameter/{id}', 'UrinalisasiController@penilaianparameter');
Route::get('urinalisasi/data-evaluasi/penilaian-reagen/{id}', 'UrinalisasiController@penilaianreagen');



Route::get('kimia-klinik/data-evaluasi/{id}', 'KimiaKlinikController@cetak');
Route::get('kimia-klinik/data-evaluasi/grafik-zscore/{id}', 'GrafikController@zkim');
Route::post('kimia-klinik/data-evaluasi/grafik-zscore/{id}', 'GrafikController@gzkim');

Route::get('kimia-klinik/data-evaluasi/grafik-zscore-alat/{id}', 'GrafikController@alatzkim');
Route::post('kimia-klinik/data-evaluasi/grafik-zscore-alat/{id}', 'GrafikController@alatgzkim');

Route::get('kimia-klinik/data-evaluasi/grafik-zscore-metode/{id}', 'GrafikController@metodezkim');
Route::post('kimia-klinik/data-evaluasi/grafik-zscore-metode/{id}', 'GrafikController@metodegzkim');

// Route::get('/hasil-pemeriksaan/kimia-klinik/nilai-sama/{id}', 'GrafikController@nilaisama');
// Route::post('/hasil-pemeriksaan/kimia-klinik/nilai-sama/{id}', 'GrafikController@gnilaisama');

Route::get('/kimia-klinik/data-evaluasi/nilai-sama/{id}', 'GrafikController@nilaisama');
Route::post('/kimia-klinik/data-evaluasi/nilai-sama/{id}', 'GrafikController@gnilaisama');

Route::get('/kimia-klinik/data-evaluasi/nilai-sama-alat/{id}', 'GrafikController@nilaisamaalat');
Route::post('/kimia-klinik/data-evaluasi/nilai-sama-alat/{id}', 'GrafikController@gnilaisamaalat');

Route::get('/kimia-klinik/data-evaluasi/nilai-sama-metode/{id}', 'GrafikController@nilaisamametode');
Route::post('/kimia-klinik/data-evaluasi/nilai-sama-metode/{id}', 'GrafikController@gnilaisamametode');

Route::get('/hasil-pemeriksaan/bta/data-evaluasi/{id}','DataEvaluasiController@bta');
Route::post('/hasil-pemeriksaan/bta/data-evaluasi/{id}','DataEvaluasiController@bta');
Route::get('bta/data-evaluasi/penilaian/{id}', 'BtaController@cetak');

Route::get('malaria/data-evaluasi/penilaian/{id}', 'MalariaController@cetakevaluasi');

Route::get('/hasil-pemeriksaan/telur-cacing/data-evaluasi/{id}','DataEvaluasiController@tcc');
Route::post('/hasil-pemeriksaan/telur-cacing/data-evaluasi/{id}','DataEvaluasiController@tcc');

Route::get('/telur-cacing/data-evaluasi/{id}', 'DataEvaluasiController@evaluasi_cacing');
Route::post('/telur-cacing/data-evaluasi/{id}', 'DataEvaluasiController@evaluasi_cacing');
Route::get('/telur-cacing/data-evaluasi/print/{id}','TelurCacingController@printevaluasi');

Route::get('/hasil-pemeriksaan/anti-hiv/data-evaluasi/{id}','DataEvaluasiController@hiv');
Route::post('/hasil-pemeriksaan/anti-hiv/data-evaluasi/{id}','DataEvaluasiController@hiv');
Route::get('anti-hiv/data-evaluasi/print/{id}', 'AntiHivController@cetakevaluasi');
Route::get('/hasil-pemeriksaan/syphilis/data-evaluasi/{id}','DataEvaluasiController@sif');
Route::post('/hasil-pemeriksaan/syphilis/data-evaluasi/{id}','DataEvaluasiController@sif');

Route::get('/syphilis/data-evaluasi/print/{id}', 'SyphilisController@cetakevaluasi');
Route::get('/syphilis/data-evaluasi/rpr/print/{id}', 'RprController@cetakevaluasi');

Route::get('/hasil-pemeriksaan/hbsag/data-evaluasi/{id}','DataEvaluasiController@hbs');
Route::post('/hasil-pemeriksaan/hbsag/data-evaluasi/{id}','DataEvaluasiController@hbs');
Route::get('/hbsag/data-evaluasi/print/{id}', 'HbsagController@cetakevaluasi');

Route::get('/hasil-pemeriksaan/anti-hcv/data-evaluasi/{id}','DataEvaluasiController@hcv');
Route::post('/hasil-pemeriksaan/anti-hcv/data-evaluasi/{id}','DataEvaluasiController@hcv');
Route::get('/anti-hcv/data-evaluasi/print/{id}', 'AntihcvController@cetakevaluasi');

Route::get('/hasil-pemeriksaan/malaria/data-evaluasi/{id}','DataEvaluasiController@mal');
Route::post('/hasil-pemeriksaan/malaria/data-evaluasi/{id}','DataEvaluasiController@mal');

Route::get('/hasil-pemeriksaan/antibiotik/data-evaluasi/{id}','DataEvaluasiController@bac');
Route::post('/hasil-pemeriksaan/antibiotik/data-evaluasi/{id}','DataEvaluasiController@bac');
Route::get('/antibiotik/data-evaluasi/print/{id}', 'AntibiotikController@view');
Route::get('/antibiotik/data-evaluasi/uji-kepekaan-antibiotik/print/{id}', 'EvaluasiBakteriController@cetakimonologi');

Route::get('/antibiotik/data-evaluasi/penilaian/{id}', 'DataEvaluasiController@evaluasi1');
Route::post('/antibiotik/data-evaluasi/penilaian/{id}', 'DataEvaluasiController@evaluasi1');
Route::post('/antibiotik/data-evaluasi/penilaian/print/{id}','EvaluasiBakteriController@evaluasiPrint');

Route::get('/antibiotik/data-evaluasiuji/peka/{id}', 'DataEvaluasiController@kepekaan');
Route::post('/antibiotik/data-evaluasiuji/print/{id}','EvaluasiBakteriController@evaluasiujiPrint');

Route::get('/hasil-pemeriksaan/kimia-air-terbatas/data-evaluasi/{id}','DataEvaluasiController@kat');
Route::post('/hasil-pemeriksaan/kimia-air-terbatas/data-evaluasi/{id}','DataEvaluasiController@kat');
Route::get('/kimia-air-terbatas/data-evaluasi/print/{id}', 'KimiaAirTerbatasController@cetakevaluasi');
Route::get('/kimia-air-terbatas/data-evaluasi/grafik-zscorepeserta/{id}', 'GrafikController@zkimairterbatas');
Route::post('/kimia-air-terbatas/data-evaluasi/grafik-zscorepeserta/{id}', 'GrafikController@gzkimairterbatas');

Route::get('/hasil-pemeriksaan/kimia-air/data-evaluasi/{id}','DataEvaluasiController@kai');
Route::post('/hasil-pemeriksaan/kimia-air/data-evaluasi/{id}','DataEvaluasiController@kai');

Route::get('/kimia-air/data-evaluasi/print/{id}', 'KimiaAirController@cetakevaluasi');
Route::get('/kimia-air/data-evaluasi/grafik-zscore/{id}', 'GrafikController@zkimair');
Route::post('/kimia-air/data-evaluasi/grafik-zscore/{id}', 'GrafikController@gzkimair');
Route::get('/kimia-air/data-evaluasi/parameter-nilai-sama/{id}', 'GrafikController@kimiaairnilaisama');
Route::post('/kimia-air/data-evaluasi/parameter-nilai-sama/{id}', 'GrafikController@gkimiaairnilaisama');

Route::get('/kimia-air/data-evaluasi/kat/nilai-sama/{id}', 'GrafikController@kimiaairterbatasnilaisama');
Route::post('/kimia-air/data-evaluasi/kat/nilai-sama/{id}', 'GrafikController@gkimiaairterbatasnilaisama');


Route::get('evaluasi/input-ring/kimiaair', 'KimiaAirController@kimring');
Route::post('evaluasi/input-ring/kimiaair', 'KimiaAirController@kimringna');
Route::get('evaluasi/input-ring/kimiaair/edit/{id}', 'KimiaAirController@kimringedit');
Route::post('evaluasi/input-ring/kimiaair/edit/{id}', 'KimiaAirController@kimringeditna');
Route::get('evaluasi/input-ring/kimiaair/delete/{id}', 'KimiaAirController@kimringdelet');

Route::get('evaluasi/input-ring/kimiaairterbatas', 'KimiaAirTerbatasController@kimring');
Route::post('evaluasi/input-ring/kimiaairterbatas', 'KimiaAirTerbatasController@kimringna');
Route::get('evaluasi/input-ring/kimiaairterbatas/edit/{id}', 'KimiaAirTerbatasController@kimringedit');
Route::post('evaluasi/input-ring/kimiaairterbatas/edit/{id}', 'KimiaAirTerbatasController@kimringeditna');
Route::get('evaluasi/input-ring/kimiaairterbatas/delete/{id}', 'KimiaAirTerbatasController@kimringdelet');


Route::get('/data-evaluasi/hematologi/print/{id}', 'HematologiController@cetaksemua');
Route::get('/data-evaluasi/hematologialat/print/{id}', 'DataEvaluasiController@cetakzscorealat');
Route::get('/data-evaluasi/hematologimetode/print/{id}', 'DataEvaluasiController@cetakzscoremetode');

Route::get('/data-evaluasi/grafik-zscore/{id}', 'DataEvaluasiController@grafikzscore');
Route::post('/data-evaluasi/grafik-zscore/{id}', 'DataEvaluasiController@grafikzscorepost');

Route::get('/data-evaluasi/grafik-zscore-alat/{id}', 'DataEvaluasiController@grafikzscorealat');
Route::post('/data-evaluasi/grafik-zscore-alat/{id}', 'DataEvaluasiController@grafikzscorepostalat');

Route::get('/data-evaluasi/grafik-zscore-semua/{id}', 'GrafikController@gzhemsemua');
Route::post('/data-evaluasi/grafik-zscore-semua/{id}', 'GrafikController@gzhemsemua');

Route::get('/data-evaluasi/grafik-zscore-nilai-sama/{id}', 'GrafikController@ghemnilaisama');
Route::post('/data-evaluasi/grafik-zscore-nilai-sama/{id}', 'GrafikController@ghemnilaisama');

Route::get('/laporan-akhir-hematologi/data-evaluasi/{id}','LapAkhirEvaluasiController@laporanakhirhematologi');
Route::get('/data-evaluasi/data-laporan-akhir/', 'LapAkhirEvaluasiController@laporanakhirhematologidownload');

Route::get('/laporan-akhir-hematologi/data-evaluasi/penutup/{id}','LapAkhirEvaluasiController@laporanakhirhematologipenutup');
Route::get('/laporan-akhir-bac/data-evaluasi/penutup/{id}','LapAkhirEvaluasiController@laporanakhirbacpenutup');
Route::get('/laporan-akhir-bta/data-evaluasi/penutup/{id}','LapAkhirEvaluasiController@laporanakhirbtapenutup');
Route::get('/laporan-akhir-hbs/data-evaluasi/penutup/{id}','LapAkhirEvaluasiController@laporanakhirhbspenutup');
Route::get('/laporan-akhir-hcv/data-evaluasi/penutup/{id}','LapAkhirEvaluasiController@laporanakhirhcvpenutup');
Route::get('/laporan-akhir-hiv/data-evaluasi/penutup/{id}','LapAkhirEvaluasiController@laporanakhirhivpenutup');
Route::get('/laporan-akhir-kai/data-evaluasi/penutup/{id}','LapAkhirEvaluasiController@laporanakhirkaipenutup');
Route::get('/laporan-akhir-kat/data-evaluasi/penutup/{id}','LapAkhirEvaluasiController@laporanakhirkatpenutup');
Route::get('/laporan-akhir-kimiaklinik/data-evaluasi/penutup/{id}','LapAkhirEvaluasiController@laporanakhirkimiaklinikpenutup');
Route::get('/laporan-akhir-mal/data-evaluasi/penutup/{id}','LapAkhirEvaluasiController@laporanakhirmalpenutup');
Route::get('/laporan-akhir-sif/data-evaluasi/penutup/{id}','LapAkhirEvaluasiController@laporanakhirsifpenutup');
Route::get('/laporan-akhir-tcc/data-evaluasi/penutup/{id}','LapAkhirEvaluasiController@laporanakhirtccpenutup');
Route::get('/laporan-akhir-uri/data-evaluasi/penutup/{id}','LapAkhirEvaluasiController@laporanakhiruripenutup');



Route::get('/laporan-akhir-bac/data-evaluasi/{id}','LapAkhirEvaluasiController@laporanakhirbac');
Route::get('/data-evaluasi/data-laporan-akhir/bac', 'LapAkhirEvaluasiController@laporanakhirbacdownload');


Route::get('/laporan-akhir-bta/data-evaluasi/{id}','LapAkhirEvaluasiController@laporanakhirbta');
Route::get('/data-evaluasi/data-laporan-akhir/bta', 'LapAkhirEvaluasiController@laporanakhirbtadownload');


Route::get('/laporan-akhir-hbs/data-evaluasi/{id}','LapAkhirEvaluasiController@laporanakhirhbs');
Route::get('/data-evaluasi/data-laporan-akhir/hbs', 'LapAkhirEvaluasiController@laporanakhirhbsdownload');


Route::get('/laporan-akhir-hcv/data-evaluasi/{id}','LapAkhirEvaluasiController@laporanakhirhcv');
Route::get('/data-evaluasi/data-laporan-akhir/hcv', 'LapAkhirEvaluasiController@laporanakhirhcvdownload');

Route::get('/laporan-akhir-hiv/data-evaluasi/{id}','LapAkhirEvaluasiController@laporanakhirhiv');
Route::get('/data-evaluasi/data-laporan-akhir/hiv', 'LapAkhirEvaluasiController@laporanakhirhivdownload');


Route::get('/laporan-akhir-kai/data-evaluasi/{id}','LapAkhirEvaluasiController@laporanakhirkai');
Route::get('/data-evaluasi/data-laporan-akhir/kai', 'LapAkhirEvaluasiController@laporanakhirkaidownload');

Route::get('/laporan-akhir-kat/data-evaluasi/{id}','LapAkhirEvaluasiController@laporanakhirkat');
Route::get('/data-evaluasi/data-laporan-akhir/kat', 'LapAkhirEvaluasiController@laporanakhirkatdownload');

Route::get('/laporan-akhir-kimiaklinik/data-evaluasi/{id}','LapAkhirEvaluasiController@laporanakhirkimiaklinik');
Route::get('/data-evaluasi/data-laporan-akhir/kk', 'LapAkhirEvaluasiController@laporanakhirkimiaklinikdownload');

Route::get('/laporan-akhir-mal/data-evaluasi/{id}','LapAkhirEvaluasiController@laporanakhirmal');
Route::get('/data-evaluasi/data-laporan-akhir/mal', 'LapAkhirEvaluasiController@laporanakhirmaldownload');

Route::get('/laporan-akhir-sif/data-evaluasi/{id}','LapAkhirEvaluasiController@laporanakhirsif');
Route::get('/data-evaluasi/data-laporan-akhir/sif', 'LapAkhirEvaluasiController@laporanakhirsifdownload');

Route::get('/laporan-akhir-tcc/data-evaluasi/{id}','LapAkhirEvaluasiController@laporanakhirtcc');
Route::get('/data-evaluasi/data-laporan-akhir/tcc', 'LapAkhirEvaluasiController@laporanakhirtccdownload');


Route::get('/laporan-akhir-uri/data-evaluasi/{id}','LapAkhirEvaluasiController@laporanakhiruri');
Route::get('/data-evaluasi/data-laporan-akhir/uri', 'LapAkhirEvaluasiController@laporanakhiruridownload');


Route::get('/edit-data', 'DatadiriController@index');
Route::post('/edit-data', 'DatadiriController@update');

Route::get('/sptjm', 'CekTransferController@pembayaran');
Route::post('/sptjm', 'CekTransferController@prosesp');

Route::get('/update-pembayaran', 'CekTransferController@updatepembayaran');
Route::post('/update-pembayaran', 'CekTransferController@updateprosesp');

Route::get('/kirim-bahan', 'KirimBahanController@index');
Route::post('/kirim-bahan', 'KirimBahanController@proses');
Route::get('/kirim-bahan/surat-pengantar', function(){return view("/cetak_hasil/kirim_bahan");});

/*----Provinsi----*/
Route::get('/admin/provinsi', 'ProvinsiController@index');
Route::get('/admin/provinsi/edit/{id}', 'ProvinsiController@edit');
Route::get('/admin/provinsi/delete/{id}', 'ProvinsiController@delete');
Route::get('/admin/provinsi/insert', function(){return view("/back/provinsi/insert");});
Route::post('/admin/provinsi/insert', 'ProvinsiController@insert');
Route::post('/admin/provinsi/edit/{id}', 'ProvinsiController@update');

/*----Kota----*/
Route::get('/admin/kabupaten-kota', 'KabupatenKotaController@index');
Route::get('/admin/kabupaten-kota/edit/{id}', 'KabupatenKotaController@edit');
Route::get('/admin/kabupaten-kota/delete/{id}', 'KabupatenKotaController@delete');
Route::get('/admin/kabupaten-kota/insert', 'KabupatenKotaController@in');
Route::post('/admin/kabupaten-kota/insert', 'KabupatenKotaController@insert');
Route::post('/admin/kabupaten-kota/edit/{id}', 'KabupatenKotaController@update');

/*----Kecamatan----*/
Route::get('/admin/kecamatan', 'KecamatanController@index');
Route::get('/admin/kecamatan/edit/{id}', 'KecamatanController@edit');
Route::get('/admin/kecamatan/delete/{id}', 'KecamatanController@delete');
Route::get('/admin/kecamatan/insert', 'KecamatanController@in');
Route::post('/admin/kecamatan/insert', 'KecamatanController@insert');
Route::post('/admin/kecamatan/edit/{id}', 'KecamatanController@update');

/*----Kelurahan----*/
Route::get('/admin/kelurahan', 'KelurahanController@index');
Route::get('/admin/kelurahan/edit/{id}', 'KelurahanController@edit');
Route::get('/admin/kelurahan/delete/{id}', 'KelurahanController@delete');
Route::get('/admin/kelurahan/insert', 'KelurahanController@in');
Route::post('/admin/kelurahan/insert', 'KelurahanController@insert');
Route::post('/admin/kelurahan/edit/{id}', 'KelurahanController@update');

// Edit Hasil
Route::get('/admin/edit-hasil', 'BannerController@edithasil');
Route::post('/admin/edit-hasil', 'BannerController@edithasilna');

Route::get('/admin/menu', 'BannerController@menu');
Route::post('/admin/menu', 'BannerController@menuna');

Route::get('/admin/siklus', 'BannerController@siklus');
Route::post('/admin/siklus', 'BannerController@siklusna');

Route::get('/admin/tahun-evaluasi', 'BannerController@tahun');
Route::post('/admin/tahun-evaluasi', 'BannerController@tahunna');
/*----Banner----*/
Route::get('/admin/banner', 'BannerController@index');
Route::get('/admin/banner/edit/{id}', 'BannerController@edit');
Route::get('/admin/banner/delete/{id}', 'BannerController@delete');
Route::get('/admin/banner/insert', function(){return view("/back/banner/insert");});
Route::post('/admin/banner/insert', 'BannerController@insert');
Route::post('/admin/banner/edit/{id}', 'BannerController@update');

/*----Bidang----*/
Route::get('/admin/bidang', 'BidangController@index');
Route::get('/admin/bidang/edit/{id}', 'BidangController@edit');
Route::get('/admin/bidang/delete/{id}', 'BidangController@delete');
Route::get('/admin/bidang/insert', function(){return view("/back/bidang/insert");});
Route::post('/admin/bidang/insert', 'BidangController@insert');
Route::post('/admin/bidang/edit/{id}', 'BidangController@update');

/*----PKS----*/
Route::get('/admin/pks', 'PksController@index');
Route::get('/admin/pks/edit/{id}', 'PksController@edit');
Route::get('/admin/pks/delete/{id}', 'PksController@delete');
Route::get('/admin/pks/insert', function(){return view("/back/pks/insert");});
Route::post('/admin/pks/insert', 'PksController@insert');
Route::post('/admin/pks/edit/{id}', 'PksController@update');

/*----Parameter/Sub bidang----*/
Route::get('/admin/parameter', 'SubbidangController@index');
Route::get('/admin/parameter/edit/{id}', 'SubbidangController@edit');
Route::get('/admin/parameter/delete/{id}', 'SubbidangController@delete');
Route::get('/admin/parameter/insert', 'SubbidangController@in');
Route::post('/admin/parameter/insert', 'SubbidangController@insert');
Route::post('/admin/parameter/edit/{id}', 'SubbidangController@update');

/*----Juklak----*/
Route::get('/admin/juklak', 'JuklakController@index');
Route::get('/admin/juklak/edit/{id}', 'JuklakController@edit');
Route::get('/admin/juklak/delete/{id}', 'JuklakController@delete');
Route::get('/admin/juklak/insert', 'JuklakController@in');
Route::post('/admin/juklak/insert', 'JuklakController@insert');
Route::post('/admin/juklak/edit/{id}', 'JuklakController@update');

/*----Sptjm----*/
Route::get('/admin/sptjm', 'SptjmController@index');
Route::get('/admin/sptjm/edit/{id}', 'SptjmController@edit');
Route::get('/admin/sptjm/delete/{id}', 'SptjmController@delete');
Route::get('/admin/sptjm/insert', 'SptjmController@in');
Route::post('/admin/sptjm/insert', 'SptjmController@insert');
Route::post('/admin/sptjm/edit/{id}', 'SptjmController@update');

/*----Manual Book----*/
Route::get('/admin/manual-book', 'ManualBookController@index');
Route::get('/admin/manual-book/edit/{id}', 'ManualBookController@edit');
Route::get('/admin/manual-book/delete/{id}', 'ManualBookController@delete');
Route::get('/admin/manual-book/insert', 'ManualBookController@in');
Route::post('/admin/manual-book/insert', 'ManualBookController@insert');
Route::post('/admin/manual-book/edit/{id}', 'ManualBookController@update');

/*----Jadwal----*/
Route::get('/admin/jadwal', 'JadwalController@index');
Route::get('/admin/jadwal/edit/{id}', 'JadwalController@edit');
Route::get('/admin/jadwal/delete/{id}', 'JadwalController@delete');
Route::get('/admin/jadwal/insert', 'JadwalController@in');
Route::post('/admin/jadwal/insert', 'JadwalController@insert');
Route::post('/admin/jadwal/edit/{id}', 'JadwalController@update');

/*----Manual Book----*/
Route::get('/admin/penggunaan', 'PenggunaanController@index');
Route::get('/admin/penggunaan/edit/{id}', 'PenggunaanController@edit');
Route::get('/admin/penggunaan/delete/{id}', 'PenggunaanController@delete');
Route::get('/admin/penggunaan/insert', 'PenggunaanController@in');
Route::post('/admin/penggunaan/insert', 'PenggunaanController@insert');
Route::post('/admin/penggunaan/edit/{id}', 'PenggunaanController@update');

/*----Evaluasi----*/
Route::get('/admin/upload-evaluasi', 'UploadEvaluasiController@index');
Route::get('/admin/upload-evaluasi/register', 'UploadEvaluasiController@register');
Route::get('/admin/upload-evaluasi/edit/{id}', 'UploadEvaluasiController@edit');
Route::get('/admin/upload-evaluasi/delete/{id}', 'UploadEvaluasiController@delete');
Route::get('/admin/upload-evaluasi/insert/{id}', 'UploadEvaluasiController@in');
Route::post('/admin/upload-evaluasi/insert/{id}', 'UploadEvaluasiController@insert');
Route::post('/admin/upload-evaluasi/edit/{id}', 'UploadEvaluasiController@update');

/*----Hak Akses----*/
Route::get('/admin/hak-akses', 'HakAksesController@index');
Route::get('/admin/hak-akses/edit/{id}', 'HakAksesController@edit');
Route::get('/admin/hak-akses/delete/{id}', 'HakAksesController@delete');
Route::post('/admin/hak-akses/edit/{id}', 'HakAksesController@update');
Route::get('/admin/hak-akses/insert', 'HakAksesController@in');
Route::post('/admin/hak-akses/insert', 'HakAksesController@insert');

/*----Perusahaan----*/
Route::get('/admin/data-perusahaan', 'DataPerusahaanController@index');
Route::get('/admin/data-perusahaan/edit/{id}', 'DataPerusahaanController@edit');
Route::get('/admin/data-perusahaan/tanda-terima/edit/{id}', 'DataPerusahaanController@editt');
Route::get('/admin/data-perusahaan/hapus/{id}', 'DataPerusahaanController@destroy');
Route::get('/admin/data-perusahaan/tanda-terima/hapus/{id}', 'DataPerusahaanController@hapustanda');
Route::get('/admin/data-perusahaan/delete/{id}', 'DataPerusahaanController@hapus');
Route::get('/admin/data-perusahaan/edit-data/{id}', 'DataPerusahaanController@updatedata');
// Route::get('/admin/data-perusahaan/insert', 'HakAksesController@in');
// Route::post('/admin/data-perusahaan/insert', 'HakAksesController@insert');


Route::get('/getkota/{id}', 'HomeController@getKota');
Route::get('/getkecamatan/{id}', 'HomeController@getKecamatan');
Route::get('/getkelurahan/{id}', 'HomeController@getKelurahan');
Route::get('/datasiklus/{id}', 'HomeController@datasiklus');

/*----hasil pemeriksaan----*/
Route::get('/hasil-pemeriksaan/antibiotik/{id}', 'AntibiotikController@index');
Route::post('/hasil-pemeriksaan/antibiotik/{id}', 'AntibiotikController@insert');
Route::get('/hasil-pemeriksaan/antibiotik/print/{id}', 'AntibiotikController@view');
Route::get('/hasil-pemeriksaan/antibiotik/edit/{id}', 'AntibiotikController@edit');
Route::post('/hasil-pemeriksaan/antibiotik/edit/{id}', 'AntibiotikController@update');

Route::get('/hasil-pemeriksaan/kimia-air/{id}', 'KimiaAirController@index');
Route::post('/hasil-pemeriksaan/kimia-air/{id}', 'KimiaAirController@insert');
Route::get('/hasil-pemeriksaan/kimia-air/edit/{id}', 'KimiaAirController@edit');
Route::post('/hasil-pemeriksaan/kimia-air/edit/{id}', 'KimiaAirController@update');
Route::get('/hasil-pemeriksaan/kimia-air/print/{id}', 'KimiaAirController@view');
Route::get('/hasil-pemeriksaan/kimia-air/evaluasi/{id}', 'KimiaAirController@evaluasi');
Route::post('/hasil-pemeriksaan/kimia-air/evaluasi/{id}', 'KimiaAirController@insertevaluasi');
Route::post('/hasil-pemeriksaan/kimia-air/evaluasi/print/{id}', 'KimiaAirController@cetakevaluasi');

Route::get('/hasil-pemeriksaan/kimia-air/grafik-zscore/{id}', 'GrafikController@zkimair');
Route::post('/hasil-pemeriksaan/kimia-air/grafik-zscore/{id}', 'GrafikController@gzkimair');

Route::get('/hasil-pemeriksaan/kimia-air-terbatas/{id}', 'KimiaAirTerbatasController@index');
Route::post('/hasil-pemeriksaan/kimia-air-terbatas/{id}', 'KimiaAirTerbatasController@insert');
Route::get('/hasil-pemeriksaan/kimia-air-terbatas/print/{id}', 'KimiaAirTerbatasController@view');
Route::get('/hasil-pemeriksaan/kimia-air-terbatas/edit/{id}', 'KimiaAirTerbatasController@edit');
Route::post('/hasil-pemeriksaan/kimia-air-terbatas/edit/{id}', 'KimiaAirTerbatasController@update');
Route::get('/hasil-pemeriksaan/kimia-air-terbatas/evaluasi/{id}', 'KimiaAirTerbatasController@evaluasi');
Route::post('/hasil-pemeriksaan/kimia-air-terbatas/evaluasi/{id}', 'KimiaAirTerbatasController@insertevaluasi');
Route::post('/hasil-pemeriksaan/kimia-air-terbatas/evaluasi/print/{id}', 'KimiaAirTerbatasController@cetakevaluasi');

Route::get('/hasil-pemeriksaan/kimia-air-terbatas/grafik-zscore/{id}', 'GrafikController@zkimairterbatas');
Route::post('/hasil-pemeriksaan/kimia-air-terbatas/grafik-zscore/{id}', 'GrafikController@gzkimairterbatas');


Route::get('/hasil-pemeriksaan/urinalisasi/{id}', 'UrinalisasiController@index');
Route::get('/hasil-pemeriksaan/urinalisasi/print/{id}', 'UrinalisasiController@view');
Route::post('/hasil-pemeriksaan/urinalisasi/{id}', 'UrinalisasiController@insert');
Route::get('/hasil-pemeriksaan/urinalisasi/edit/{id}', 'UrinalisasiController@edit');
Route::post('/hasil-pemeriksaan/urinalisasi/edit/{id}', 'UrinalisasiController@update');

Route::get('/hasil-pemeriksaan/urinalisasi/evaluasi/{id}', 'UrinalisasiController@evaluasi');
Route::post('/hasil-pemeriksaan/urinalisasi/evaluasi/print/{id}', 'UrinalisasiController@printevaluasi');
Route::post('/hasil-pemeriksaan/urinalisasi/evaluasi/{id}', 'UrinalisasiController@simpanskor');
Route::post('/hasil-pemeriksaan/urinalisasi/evaluasi/update/{id}', 'UrinalisasiController@updatevaluasi');

Route::get('/hasil-pemeriksaan/urinalisasi/penilaian-parameter/{id}', 'UrinalisasiController@penilaianparameter');
Route::get('/hasil-pemeriksaan/urinalisasi/penilaian-reagen/{id}', 'UrinalisasiController@penilaianreagen');
Route::get('/hasil-pemeriksaan/urinalisasi/penilaian-reagen-kehamilan/{id}', 'UrinalisasiController@penilaianreagenkehamilan');

Route::get('/hasil-pemeriksaan/kimia-klinik/{id}', 'KimiaKlinikController@index');
Route::post('/hasil-pemeriksaan/kimia-klinik/{id}', 'KimiaKlinikController@insert');
Route::get('/hasil-pemeriksaan/kimia-klinik/print/{id}', 'KimiaKlinikController@view');
Route::get('/hasil-pemeriksaan/kimia-klinik/edit/{id}', 'KimiaKlinikController@edit');
Route::post('/hasil-pemeriksaan/kimia-klinik/edit/{id}', 'KimiaKlinikController@update');
Route::get('/hasil-pemeriksaan/kimia-klinik/evaluasi/{id}', 'KimiaKlinikController@evaluasi');
Route::post('/hasil-pemeriksaan/kimia-klinik/evaluasi/{id}', 'KimiaKlinikController@insertevaluasi');
Route::get('/hasil-pemeriksaan/kimia-klinik/evaluasi/print/{id}', 'KimiaKlinikController@cetak');
Route::post('/hasil-pemeriksaan/kimiaklinik/evaluasi-kimiaklinik/auto-save/data', 'HematologiController@autoSaveKimiaklinik');

Route::get('/hasil-pemeriksaan/kimia-klinik0/test', 'KimiaKlinikController@Testtest');

Route::get('/hasil-pemeriksaan/kimia-klinik/evaluasi-alat/{id}', 'KimiaKlinikController@evaluasialat');
Route::post('/hasil-pemeriksaan/kimia-klinik/evaluasi-alat/{id}', 'KimiaKlinikController@insertevaluasialat');
Route::get('/hasil-pemeriksaan/kimia-klinik/evaluasi-metode/{id}', 'KimiaKlinikController@evaluasimetode');
Route::post('/hasil-pemeriksaan/kimia-klinik/evaluasi-metode/{id}', 'KimiaKlinikController@insertevaluasimetode');

Route::get('/evaluasi/rekap-hasil-peserta/kimiaklinik', 'KimiaKlinikController@perusahaan');
Route::get('/evaluasi/rekap-hasil-peserta/kimiaklinik/{id}', 'KimiaKlinikController@dataperusahaan');

Route::get('/evaluasi/rekap-hasil-peserta/hematologi', 'HematologiController@perusahaan');
Route::get('/evaluasi/rekap-hasil-peserta/hematologi/{id}', 'HematologiController@dataperusahaan');

Route::get('/evaluasi/rekap-hasil-peserta/urinalisa', 'UrinalisasiController@perusahaan');
Route::get('/evaluasi/rekap-hasil-peserta/urinalisa/{id}', 'UrinalisasiController@dataperusahaan');

Route::get('/hasil-pemeriksaan/kimia-klinik/grafik-zscore/{id}', 'GrafikController@zkim');
Route::post('/hasil-pemeriksaan/kimia-klinik/grafik-zscore/{id}', 'GrafikController@gzkim');

Route::get('/hasil-pemeriksaan/kimia-klinik/grafik-zscore-alat/{id}', 'GrafikController@alatzkim');
Route::post('/hasil-pemeriksaan/kimia-klinik/grafik-zscore-alat/{id}', 'GrafikController@alatgzkim');

Route::get('/hasil-pemeriksaan/kimia-klinik/grafik-zscore-metode/{id}', 'GrafikController@metodezkim');
Route::post('/hasil-pemeriksaan/kimia-klinik/grafik-zscore-metode/{id}', 'GrafikController@metodegzkim');

Route::get('grafik/hitung-zscore/kimiaklinik', 'GrafikController@zkim');
Route::get('grafik/gzkim', 'GrafikController@gzkim');

Route::get('grafik/hitung-zscore-alat/kimiaklinik', 'GrafikController@alatzkim');
Route::get('grafik-alat/gzkim', 'GrafikController@alatgzkim');

Route::get('grafik/hitung-zscore-metode/kimiaklinik', 'GrafikController@metodezkim');
Route::get('grafik-metode/gzkim', 'GrafikController@metodegzkim');

Route::get('/getmetode/{id}', 'GrafikController@getmetode');
Route::get('/getmetodesemua/{id}', 'GrafikController@getmetodesemua');
Route::get('/hasil-pemeriksaan/hematologi/grafik-zscore-metode/{id}', 'GrafikController@metodezhem');
Route::post('/hasil-pemeriksaan/hematologi/grafik-zscore-metode/{id}', 'GrafikController@metodegzhem');

Route::get('grafik/hitung-zscore-metode/hematologi', 'GrafikController@perusahaanhem');
Route::get('grafik/hitung-zscore-metode/hematologi/{id}', 'GrafikController@dataperusahaanmetodehem');

Route::get('/hasil-pemeriksaan/hematologi/{id}', 'HematologiController@index');
Route::post('/hasil-pemeriksaan/hematologi/{id}', 'HematologiController@insert');
Route::get('/hasil-pemeriksaan/hematologi/print/{id}', 'HematologiController@view');
Route::get('/hasil-pemeriksaan/hematologi/edit/{id}', 'HematologiController@edit');
Route::post('/hasil-pemeriksaan/hematologi/edit/{id}', 'HematologiController@update');
Route::get('/hasil-pemeriksaan/hematologi/evaluasi/{id}', 'HematologiController@evaluasi');
Route::post('/hasil-pemeriksaan/hematologi/evaluasi/{id}', 'HematologiController@insertevaluasi');
Route::post('/hasil-pemeriksaan/hematologi/evaluasi-parameter/auto-save/data', 'HematologiController@autoSaveParameter');

Route::post('/hasil-pemeriksaan/hematologi/evaluasi-alat/auto-save/data', 'HematologiController@autoSaveAlat');
Route::post('/hasil-pemeriksaan/hematologi/evaluasi-metode/auto-save/data', 'HematologiController@autoSaveMetode');

// Route::post('/hasil-pemeriksaan/urinalisasi/evaluasi/print/{id}', 'UrinalisasiController@printevaluasi');
Route::post('/hasil-pemeriksaan/hematologi/evaluasi/print/{id}', 'HematologiController@cetak');
Route::get('/hasil-pemeriksaan/hematologi/evaluasi/cetak/{id}', 'HematologiController@cetaksemua');

Route::get('/hasil-pemeriksaan/hematologi/evaluasi-alat/{id}', 'HematologiController@evaluasialat');
Route::post('/hasil-pemeriksaan/hematologi/evaluasi-alat/{id}', 'HematologiController@insertevaluasialat');
Route::post('/hasil-pemeriksaan/hematologi/evaluasi-alat/print/{id}', 'HematologiController@cetakalat');

Route::get('/hasil-pemeriksaan/hematologi/evaluasi-metode/{id}', 'HematologiController@evaluasimetode');
Route::post('/hasil-pemeriksaan/hematologi/evaluasi-metode/{id}', 'HematologiController@insertevaluasimetode');
Route::post('/hasil-pemeriksaan/hematologi/evaluasi-metode/print/{id}', 'HematologiController@cetakmetode');

Route::get('/hasil-pemeriksaan/hematologi/grafik-zscore/{id}', 'GrafikController@zhem');
Route::post('/hasil-pemeriksaan/hematologi/grafik-zscore/{id}', 'GrafikController@gzhem');

Route::get('/hasil-pemeriksaan/hematologi/grafik-zscore-semua/{id}', 'GrafikController@zhemsemua');
Route::post('/hasil-pemeriksaan/hematologi/grafik-zscore-semua/{id}', 'GrafikController@gzhemsemua');

Route::get('/hasil-pemeriksaan/kimia-klinik/grafik-zscore-semua/{id}', 'GrafikController@gzkksemua');
Route::post('/hasil-pemeriksaan/kimia-klinik/grafik-zscore-semua/{id}', 'GrafikController@gzkksemua');

Route::get('/kimia-klinik/data-evaluasi/nilai-sama/{id}', 'GrafikController@zkksemua');
Route::post('/kimia-klinik/data-evaluasi/nilai-sama/{id}', 'GrafikController@gzkksemua');

Route::get('/hasil-pemeriksaan/hematologi/grafik-zscore-alat/{id}', 'GrafikController@alatzhem');
Route::post('/hasil-pemeriksaan/hematologi/grafik-zscore-alat/{id}', 'GrafikController@alatgzhem');

Route::get('/hasil-pemeriksaan/hematologi/nilai-sama/{id}', 'GrafikController@hemnilaisama');
Route::post('/hasil-pemeriksaan/hematologi/nilai-sama/{id}', 'GrafikController@ghemnilaisama');

Route::get('/hasil-pemeriksaan/hematologi/nilai-sama-alat/{id}', 'GrafikController@hemnilaisamaalat');
Route::post('/hasil-pemeriksaan/hematologi/nilai-sama-alat/{id}', 'GrafikController@ghemnilaisamaalat');

Route::get('/hasil-pemeriksaan/hematologi/nilai-sama-metode/{id}', 'GrafikController@hemnilaisamametode');
Route::post('/hasil-pemeriksaan/hematologi/nilai-sama-metode/{id}', 'GrafikController@ghemnilaisamametode');

Route::get('/hasil-pemeriksaan/kimia-air/nilai-sama/{id}', 'GrafikController@kimiaairnilaisama');
Route::post('/hasil-pemeriksaan/kimia-air/nilai-sama/{id}', 'GrafikController@gkimiaairnilaisama');

Route::get('/hasil-pemeriksaan/kimia-air-terbatas/nilai-sama/{id}', 'GrafikController@kimiaairterbatasnilaisama');
Route::post('/hasil-pemeriksaan/kimia-air-terbatas/nilai-sama/{id}', 'GrafikController@gkimiaairterbatasnilaisama');

Route::get('/hasil-pemeriksaan/kimia-klinik/nilai-sama/{id}', 'GrafikController@gkknilaisama');
Route::post('/hasil-pemeriksaan/kimia-klinik/nilai-sama/{id}', 'GrafikController@gkknilaisama');

Route::get('/hasil-pemeriksaan/kimia-klinik/nilai-sama-alat/{id}', 'GrafikController@nilaisamaalat');
Route::post('/hasil-pemeriksaan/kimia-klinik/nilai-sama-alat/{id}', 'GrafikController@gnilaisamaalat');

Route::get('/hasil-pemeriksaan/kimia-klinik/nilai-sama-metode/{id}', 'GrafikController@nilaisamametode');
Route::post('/hasil-pemeriksaan/kimia-klinik/nilai-sama-metode/{id}', 'GrafikController@gnilaisamametode');

Route::get('/hasil-pemeriksaan/malaria/{id}', 'MalariaController@index');
Route::get('/hasil-pemeriksaan/malaria/print/{id}', 'MalariaController@view');
Route::post('/hasil-pemeriksaan/malaria/{id}', 'MalariaController@insert');
Route::get('/hasil-pemeriksaan/malaria/edit/{id}', 'MalariaController@edit');
Route::post('/hasil-pemeriksaan/malaria/edit/{id}', 'MalariaController@update');

Route::get('/hasil-pemeriksaan/malaria/evaluasi/{id}', 'MalariaController@evaluasi');
Route::post('/hasil-pemeriksaan/malaria/evaluasi/{id}', 'MalariaController@insertevaluasi');
Route::get('/hasil-pemeriksaan/malaria/evaluasi/cetak/{id}', 'MalariaController@cetakevaluasi');

Route::get('/hasil-pemeriksaan/telur-cacing/{id}', 'TelurCacingController@index');
Route::post('/hasil-pemeriksaan/telur-cacing/{id}', 'TelurCacingController@insert');
Route::get('/hasil-pemeriksaan/telur-cacing/print/{id}', 'TelurCacingController@view');
Route::get('/hasil-pemeriksaan/telur-cacing/edit/{id}', 'TelurCacingController@edit');
Route::post('/hasil-pemeriksaan/telur-cacing/edit/{id}', 'TelurCacingController@update');
Route::get('/hasil-pemeriksaan/telur-cacing/evaluasi/{id}', 'TelurCacingController@evaluasi');
Route::post('/hasil-pemeriksaan/telur-cacing/evaluasi/{id}', 'TelurCacingController@insertevaluasi');
Route::post('/hasil-pemeriksaan/telur-cacing/evaluasi/update/{id}', 'TelurCacingController@updateevaluasi');
Route::get('hasil-pemeriksaan/telur-cacing/evaluasi/print/{id}','TelurCacingController@printevaluasi');


Route::get('/hasil-pemeriksaan/bta/{id}', 'BtaController@index');
Route::post('/hasil-pemeriksaan/bta/{id}', 'BtaController@insert');
Route::get('/hasil-pemeriksaan/bta/print/{id}', 'BtaController@view');
Route::get('/hasil-pemeriksaan/bta/edit/{id}', 'BtaController@edit');
Route::post('/hasil-pemeriksaan/bta/edit/{id}', 'BtaController@update');
Route::get('/hasil-pemeriksaan/bta/evaluasi/{id}', 'BtaController@evaluasi');
Route::post('/hasil-pemeriksaan/bta/evaluasi/{id}', 'BtaController@insertevaluasi');
Route::get('/hasil-pemeriksaan/bta/evaluasi/cetak/{id}', 'BtaController@cetak');
Route::get('/hasil-pemeriksaan/bta/hasil-evaluasi/{id}', 'BtaController@cetak');


Route::get('/hasil-pemeriksaan/anti-hiv/{id}', 'AntiHivController@index');
Route::post('/hasil-pemeriksaan/anti-hiv/{id}', 'AntiHivController@insert');
Route::get('/hasil-pemeriksaan/anti-hiv/print/{id}', 'AntiHivController@view');
Route::get('/hasil-pemeriksaan/anti-hiv/edit/{id}', 'AntiHivController@edit');
Route::post('/hasil-pemeriksaan/anti-hiv/edit/{id}', 'AntiHivController@update');
Route::get('/hasil-pemeriksaan/anti-hiv/evaluasi/{id}', 'AntiHivController@evaluasi');
Route::get('/hasil-pemeriksaan/anti-hiv/evaluasi/print/{id}', 'AntiHivController@cetakevaluasi');
Route::post('/hasil-pemeriksaan/anti-hiv/evaluasi/{id}', 'AntiHivController@insertevaluasi');
Route::post('/hasil-pemeriksaan/anti-hiv/evaluasi/update/{id}', 'AntiHivController@updatevaluasi');
Route::get('/hasil-pemeriksaan/anti-hiv/hasil-evaluasi/{id}', 'AntiHivController@cetakevaluasi');

Route::get('/hasil-pemeriksaan/syphilis/{id}', 'SyphilisController@index');
Route::post('/hasil-pemeriksaan/syphilis/{id}', 'SyphilisController@insert');
Route::get('/hasil-pemeriksaan/syphilis/print/{id}', 'SyphilisController@view');
Route::get('/hasil-pemeriksaan/syphilis/edit/{id}', 'SyphilisController@edit');
Route::post('/hasil-pemeriksaan/syphilis/edit/{id}', 'SyphilisController@update');
Route::get('/hasil-pemeriksaan/syphilis/evaluasi/{id}', 'SyphilisController@evaluasi');
Route::get('/hasil-pemeriksaan/syphilis/evaluasi/print/{id}', 'SyphilisController@cetakevaluasi');
Route::post('/hasil-pemeriksaan/syphilis/evaluasi/update/{id}', 'SyphilisController@updatevaluasi');
Route::post('/hasil-pemeriksaan/syphilis/evaluasi/{id}', 'SyphilisController@insertevaluasi');
Route::get('/hasil-pemeriksaan/syphilis/hasil-evaluasi/{id}', 'SyphilisController@cetakevaluasi');


Route::get('/evaluasi/upload-laporan-akhir', 'UploadLaporanAkhirController@index');
Route::get('/evaluasi/upload-laporan-akhir/pendahuluan', 'UploadLaporanAkhirController@pendahuluan');
Route::get('/evaluasi/upload-laporan-akhir/isi', 'UploadLaporanAkhirController@isi');
Route::get('/evaluasi/upload-laporan-akhir/penutup', 'UploadLaporanAkhirController@penutup');


Route::get('/evaluasi/upload-laporan-akhir/upload', 'UploadLaporanAkhirController@upload');
Route::post('/evaluasi/upload-laporan-akhir/upload', 'UploadLaporanAkhirController@insert');
Route::get('/evaluasi/upload-laporan-akhir/edit/{id}', 'UploadLaporanAkhirController@edit');
Route::post('/evaluasi/upload-laporan-akhir/update/{id}', 'UploadLaporanAkhirController@update');
Route::get('/evaluasi/upload-laporan-akhir/delete/{id}', 'UploadLaporanAkhirController@delete');


Route::post('/hasil-pemeriksaan/syphilis/hasil-evaluasi/{id}', 'SyphilisController@cetakevaluasi');
Route::get('/hasil-pemeriksaan/syphilis/hasil-evaluasi/{id}', 'SyphilisController@cetakevaluasi');
Route::post('/hasil-pemeriksaan/syphilis/hasil-evaluasi/{id}', 'SyphilisController@cetakevaluasi');



Route::get('/hasil-pemeriksaan/rpr-syphilis/{id}', 'RprController@index');
Route::post('/hasil-pemeriksaan/rpr-syphilis/{id}', 'RprController@insert');
Route::get('/hasil-pemeriksaan/rpr-syphilis/print/{id}', 'RprController@view');
Route::get('/hasil-pemeriksaan/rpr-syphilis/edit/{id}', 'RprController@edit');
Route::post('/hasil-pemeriksaan/rpr-syphilis/edit/{id}', 'RprController@update');
Route::get('/hasil-pemeriksaan/rpr-syphilis/evaluasi/{id}', 'RprController@evaluasi');
Route::get('/hasil-pemeriksaan/rpr-syphilis/evaluasi/print/{id}', 'RprController@cetakevaluasi');
Route::post('/hasil-pemeriksaan/rpr-syphilis/evaluasi/update/{id}', 'RprController@updatevaluasi');
Route::post('/hasil-pemeriksaan/rpr-syphilis/evaluasi/{id}', 'RprController@insertevaluasi');
Route::get('/hasil-pemeriksaan/rpr-syphilis/hasil-evaluasi/{id}', 'RprController@cetakevaluasi');

Route::get('/hasil-pemeriksaan/hbsag/{id}', 'HbsagController@index');
Route::post('/hasil-pemeriksaan/hbsag/{id}', 'HbsagController@insert');
Route::get('/hasil-pemeriksaan/hbsag/print/{id}', 'HbsagController@view');
Route::get('/hasil-pemeriksaan/hbsag/edit/{id}', 'HbsagController@edit');
Route::post('/hasil-pemeriksaan/hbsag/edit/{id}', 'HbsagController@update');
Route::get('/hasil-pemeriksaan/hbsag/evaluasi/{id}', 'HbsagController@evaluasi');
Route::get('/hasil-pemeriksaan/hbsag/evaluasi/print/{id}', 'HbsagController@cetakevaluasi');
Route::post('/hasil-pemeriksaan/hbsag/evaluasi/update/{id}', 'HbsagController@updatevaluasi');
Route::post('/hasil-pemeriksaan/hbsag/evaluasi/{id}', 'HbsagController@insertevaluasi');

Route::get('/hasil-pemeriksaan/anti-hcv/{id}', 'AntihcvController@index');
Route::post('/hasil-pemeriksaan/anti-hcv/{id}', 'AntihcvController@insert');
Route::get('/hasil-pemeriksaan/anti-hcv/print/{id}', 'AntihcvController@view');
Route::get('/hasil-pemeriksaan/anti-hcv/edit/{id}', 'AntihcvController@edit');
Route::post('/hasil-pemeriksaan/anti-hcv/edit/{id}', 'AntihcvController@update');
Route::get('/hasil-pemeriksaan/anti-hcv/evaluasi/{id}', 'AntihcvController@evaluasi');
Route::get('/hasil-pemeriksaan/anti-hcv/evaluasi/print/{id}', 'AntihcvController@cetakevaluasi');
Route::post('/hasil-pemeriksaan/anti-hcv/evaluasi/update/{id}', 'AntihcvController@updatevaluasi');
Route::post('/hasil-pemeriksaan/anti-hcv/evaluasi/{id}', 'AntihcvController@insertevaluasi');

Route::get('/hasil-pemeriksaan/imunologi/print/{id}', 'AntihcvController@view4');

//LAPORAN//

Route::get('/laporan-peserta', function () {return view('laporan.peserta.registrasi');});
Route::post('/laporan-peserta/proses', 'LaporanRegistrasiController@peserta');
Route::post('/laporan-peserta/proses/excel', 'LaporanRegistrasiController@pesertaexcel');

Route::get('/laporan-peserta-parameter', 'LaporanRegistrasiController@pesertaparameter');
Route::post('/laporan-peserta-parameter/proses', 'LaporanRegistrasiController@pesertaparameterp');
Route::post('/laporan-peserta-parameter/proses/excel', 'LaporanRegistrasiController@pesertaparameterexcel');

Route::get('/laporan-registrasi', function () {return view('laporan.registrasi.registrasi');});
Route::post('/laporan-registrasi/proses', 'LaporanRegistrasiController@index');
Route::post('/laporan-registrasi/proses/excel', 'LaporanRegistrasiController@indexexcel');
Route::post('/laporan-registrasi/proses/pdf', 'LaporanRegistrasiController@indexpdf');

Route::get('/laporan-uang-masuk', function () {return view('laporan.uangmasuk.uang-masuk');});
Route::post('/laporan-uang-masuk/proses', 'LaporanRegistrasiController@uangmasuk');
Route::post('/laporan-uang-masuk/proses/excel', 'LaporanRegistrasiController@uangmasukexcel');
Route::post('/laporan-uang-masuk/proses/pdf', 'LaporanRegistrasiController@uangmasukpdf');

Route::get('/laporan-penerimaan-sampel', function () {return view('laporan.sampel.sampel');});
Route::post('/laporan-penerimaan-sampel/proses', 'LaporanRegistrasiController@sampel');
Route::post('/laporan-penerimaan-sampel/proses/excel', 'LaporanRegistrasiController@sampelexcel');
Route::post('/laporan-penerimaan-sampel/proses/pdf', 'LaporanRegistrasiController@sampelpdf');

Route::get('/laporan-evaluasi', function () {return view('laporan.evaluasi.evaluasi');});
Route::post('/laporan-evaluasi/proses', 'LaporanRegistrasiController@evaluasi');
Route::post('/laporan-evaluasi/proses/excel', 'LaporanRegistrasiController@evaluasiexcel');
Route::post('/laporan-evaluasi/proses/pdf', 'LaporanRegistrasiController@evaluasipdf');

////////////////////////EVALUASI IMUNOLOGI////////////////////////////////

Route::get('/evaluasi/rekap-peserta-instansi', 'EvaluasiImunologiController@pesertainstansi');
Route::post('/evaluasi/rekap-peserta-instansi', 'EvaluasiImunologiController@rekapinstansi');

Route::get('/evaluasi/rekap-evaluasi-peserta', 'EvaluasiImunologiController@rekapevaluasi');
Route::post('/evaluasi/rekap-evaluasi-peserta', 'EvaluasiImunologiController@rekapevaluasina');

Route::get('/evaluasi/rekap-persen-instansi', 'EvaluasiImunologiController@perseninstansi');
Route::post('/evaluasi/rekap-persen-instansi', 'EvaluasiImunologiController@rekapperseninstansi');

Route::get('/evaluasi/laporan-hasil-peserta', 'EvaluasiImunologiController@laporanhasil');
Route::post('/evaluasi/laporan-hasil-peserta', 'EvaluasiImunologiController@laporanhasilna');

Route::get('/evaluasi/rekap-peserta-belum-evaluasi', 'EvaluasiImunologiController@rekapevaluasibelum');
Route::post('/evaluasi/rekap-peserta-belum-evaluasi', 'EvaluasiImunologiController@rekappesertabelum');

Route::get('/evaluasi/rekap-reagen', 'EvaluasiImunologiController@rekapreagen');
Route::post('/evaluasi/rekap-reagen', 'EvaluasiImunologiController@rekapreagenna');

Route::get('/evaluasi/rekap-reagen-persen', 'EvaluasiImunologiController@rekapreagenn');
Route::post('/evaluasi/rekap-reagen-persen', 'EvaluasiImunologiController@rekapreagenpersen');

/////////////////////////////////Grafik////////////////////////////////////////
Route::get('/evaluasi/grafik-peserta-kirim-hasil', 'EvaluasiImunologiController@grafikinstansi');
Route::post('/evaluasi/grafik-peserta-kirim-hasil', 'EvaluasiImunologiController@grafikinstansina');

Route::get('/evaluasi/grafik-rujukan-instansi', 'EvaluasiImunologiController@rujukaninstansi');
Route::post('/evaluasi/grafik-rujukan-instansi', 'EvaluasiImunologiController@rujukaninstansina');

Route::get('/evaluasi/grafik-rujukan-bahan-uji', 'EvaluasiImunologiController@rujukanbahanuji');
Route::post('/evaluasi/grafik-rujukan-bahan-uji', 'EvaluasiImunologiController@rujukanbahanujina');

Route::get('/evaluasi/grafik-rujukan-reagen', 'EvaluasiImunologiController@rujukanreagen');
Route::post('/evaluasi/grafik-rujukan-reagen', 'EvaluasiImunologiController@rujukanreagenna');

Route::get('/evaluasi/grafik-nilai-peserta', 'EvaluasiImunologiController@nilaipeserta');
Route::post('/evaluasi/grafik-nilai-peserta', 'EvaluasiImunologiController@nilaipesertana');

Route::get('/evaluasi/grafik-tahap-pemeriksaan', 'EvaluasiImunologiController@tahappemeriksaan');
Route::post('/evaluasi/grafik-tahap-pemeriksaan', 'EvaluasiImunologiController@tahappemeriksaanna');

Route::get('/evaluasi/grafik-tahap-strategi', 'EvaluasiImunologiController@tahapstrategi');
Route::post('/evaluasi/grafik-tahap-strategi', 'EvaluasiImunologiController@tahapstrategina');

Route::get('/evaluasi/grafik-jenis-reagen', 'EvaluasiImunologiController@jenisreagen');
Route::post('/evaluasi/grafik-jenis-reagen', 'EvaluasiImunologiController@jenisreagenna');

Route::get('/evaluasi/grafik-urutan-reagen', 'EvaluasiImunologiController@urutanreagen');
Route::post('/evaluasi/grafik-urutan-reagen', 'EvaluasiImunologiController@urutanreagenna');

Route::get('/evaluasi/grafik-ketepatan-dan-kesesuaian', 'EvaluasiImunologiController@ketepatankesesuaian');
Route::post('/evaluasi/grafik-ketepatan-dan-kesesuaian', 'EvaluasiImunologiController@ketepatankesesuaianna');

////////////////////////END EVALUASI IMUNOLOGI////////////////////////////////

////////////////////////EVALUASI URINALISA////////////////////////////////

Route::get('/evaluasi/urinalisa/rekap-peserta', 'EvaluasiUrinalisaController@rekappeserta');
Route::post('/evaluasi/urinalisa/rekap-peserta', 'EvaluasiUrinalisaController@rekappesertana');

/////////////////////////////////Grafik///////////////////////////////////////


////////////////////////EVALUASI Malaria////////////////////////////////

Route::get('evaluasi/malaria/rekap-grafik', 'EvaluasiMikroController@malariagrafik');
Route::post('evaluasi/malaria/rekap-grafik', 'EvaluasiMikroController@malariagrafikna');

Route::get('evaluasi/malaria/penilaian', 'EvaluasiMikroController@perusahaanmalaria');
Route::get('evaluasi/malaria/penilaian/{id}', 'EvaluasiMikroController@dataperusahaanmalaria');

////////////////////////END EVALUASI Malaria////////////////////////////////

////////////////////////EVALUASI BTA////////////////////////////////

Route::get('evaluasi/bta/rekap-grafik', 'EvaluasiMikroController@btagrafik');
Route::post('evaluasi/bta/rekap-grafik', 'EvaluasiMikroController@btagrafikna');

Route::get('evaluasi/bta/rekap-peserta', 'EvaluasiMikroController@btapeserta');
Route::post('evaluasi/bta/rekap-peserta', 'EvaluasiMikroController@btapesertana');

Route::get('evaluasi/bta/grafik-jawaban-peserta', 'EvaluasiMikroController@grafikjawabanpeserta');
Route::post('evaluasi/bta/grafik-jawaban-peserta', 'EvaluasiMikroController@grafikjawabanpesertana');

Route::get('evaluasi/bta/grafik-kesimpulan-peserta', 'EvaluasiMikroController@grafikkesimpulanpeserta');
Route::post('evaluasi/bta/grafik-kesimpulan-peserta', 'EvaluasiMikroController@grafikkesimpulanpesertana');

Route::get('evaluasi/bta/grafik-provinsi-peserta', 'EvaluasiMikroController@grafikPesertaProvinsi');
Route::post('evaluasi/bta/grafik-provinsi-peserta', 'EvaluasiMikroController@grafikPesertaProvinsiView');

Route::get('evaluasi/bta/penilaian', 'EvaluasiMikroController@perusahaanbta');
Route::post('evaluasi/bta/penilaian', 'EvaluasiMikroController@inserttanggal');
Route::get('evaluasi/bta/penilaian/{id}', 'EvaluasiMikroController@dataperusahaanbta');

Route::get('evaluasi/rekap-input-peserta', 'EvaluasiMikroController@rekapInputPeserta');
Route::post('evaluasi/rekap-input-peserta', 'EvaluasiMikroController@rekapInputPesertaView');

Route::get('evaluasi/bta/cetakan', 'EvaluasiMikroController@perusahaanbtacetak');
Route::get('evaluasi/bta/cetakan/{id}', 'EvaluasiMikroController@dataperusahaanbtacetak');
Route::get('hasil-pemeriksaan/bta/evaluasi/print/{id}', 'EvaluasiMikroController@cetak');


Route::get('evaluasi-mikro/sertifikat', 'EvaluasiMikroController@perusahaanbtasertifikat');
Route::post('evaluasi-mikro/sertifikat', 'EvaluasiMikroController@evaluasiInsertSertifikatbta');
Route::get('evaluasi-mikro/sertifikat/{id}', 'EvaluasiMikroController@dataperusahaanbtasertifikat');
Route::get('hasil-pemeriksaan/bta/sertifikat/print/{id}', 'SertifikatController@bac');
Route::get('hasil-pemeriksaan/urinalisa/sertifikat/print/{id}', 'SertifikatController@uri');
Route::get('hasil-pemeriksaan/telur-cacing/sertifikat/print/{id}', 'SertifikatController@bac');
Route::get('hasil-pemeriksaan/malaria/sertifikat/print/{id}', 'SertifikatController@bac');


Route::get('sertifikat/cetak-all/{id}', 'SertifikatController@cetakall');
Route::post('sertifikat/create-zip', 'SertifikatController@createzip');
Route::post('sertifikat/cetak-spect', 'SertifikatController@cetakoneby');
////////////////////////END EVALUASI BTA////////////////////////////////

////////////////////////EVALUASI TC////////////////////////////////

Route::get('evaluasi/telur-cacing/rekap-peserta', 'EvaluasiMikroController@tcpeserta');
Route::post('evaluasi/telur-cacing/rekap-peserta', 'EvaluasiMikroController@tcpesertana');

Route::get('evaluasi/telur-cacing/rekap-grafik', 'EvaluasiMikroController@tcgrafik');
Route::post('evaluasi/telur-cacing/rekap-grafik', 'EvaluasiMikroController@tcgrafikna');

Route::get('evaluasi/telur-cacing/cetakan', 'EvaluasiMikroController@perusahaantccetak');
Route::get('evaluasi/telur-cacing/cetakan/{id}', 'EvaluasiMikroController@dataperusahaantccetak');

Route::get('evaluasi/telur-cacing/penilaian', 'EvaluasiMikroController@perusahaantc');
Route::post('evaluasi/telur-cacing/penilaian', 'EvaluasiMikroController@inserttanggal');
Route::get('evaluasi/telur-cacing/penilaian/{id}', 'EvaluasiMikroController@dataperusahaantc');

////////////////////////END EVALUASI TC////////////////////////////////


Route::get('evaluasi/rekap-zscore-parameter/kimiaair', 'EvaluasiHemaController@airzscorelaporan');
Route::post('evaluasi/rekap-zscore-parameter/kimiaair', 'EvaluasiHemaController@airzscorelaporanna');

Route::get('evaluasi/rekap-zscore-parameter/kimiaairterbatas', 'EvaluasiHemaController@airtzscorelaporan');
Route::post('evaluasi/rekap-zscore-parameter/kimiaairterbatas', 'EvaluasiHemaController@airtzscorelaporanna');


Route::get('evaluasi/rekap-nilai-peserta/kimiaairterbatas', 'EvaluasiHemaController@airtzscorelaporannilai');
Route::post('evaluasi/rekap-nilai-peserta/kimiaairterbatas', 'EvaluasiHemaController@airtzscorelaporannilaina');

Route::get('evaluasi/rekap-nilai-peserta/kimiaair', 'EvaluasiHemaController@airtzscorelaporannilaiair');
Route::post('evaluasi/rekap-nilai-peserta/kimiaair', 'EvaluasiHemaController@airtzscorelaporannilainaair');
////////////////////////EVALUASI Hematologi////////////////////////////////

Route::get('evaluasi/rekap-zscore-parameter/hematologi', 'EvaluasiHemaController@hemazscorelaporan');
Route::post('evaluasi/rekap-zscore-parameter/hematologi', 'EvaluasiHemaController@hemazscorelaporanna');

Route::get('evaluasi/rekap-zscore-parameter-alat/hematologi', 'EvaluasiHemaController@hemazscorealatlaporan');
Route::post('evaluasi/rekap-zscore-parameter-alat/hematologi', 'EvaluasiHemaController@hemazscorealatlaporanna');

Route::get('evaluasi/rekap-zscore-parameter-metode/hematologi', 'EvaluasiHemaController@hemazscoremetodelaporan');
Route::post('evaluasi/rekap-zscore-parameter-metode/hematologi', 'EvaluasiHemaController@hemazscoremetodelaporanna');

Route::get('evaluasi/laporan-peserta-parameter/hematologi', 'EvaluasiHemaController@hemalaporan');
Route::post('evaluasi/laporan-peserta-parameter/hematologi', 'EvaluasiHemaController@hemalaporanna');
Route::get('evaluasi/laporan-peserta-alat/hematologi', 'EvaluasiHemaController@hemaalatlaporan');
Route::post('evaluasi/laporan-peserta-alat/hematologi', 'EvaluasiHemaController@hemaalatlaporanna');
Route::get('evaluasi/laporan-peserta-metode/hematologi', 'EvaluasiHemaController@hemametodelaporan');
Route::post('evaluasi/laporan-peserta-metode/hematologi', 'EvaluasiHemaController@hemametodelaporanna');


Route::get('evaluasi/input-sd-median-alat/hematologi', 'EvaluasiHemaController@hemasdmedianalat');
Route::post('evaluasi/input-sd-median-alat/hematologi', 'EvaluasiHemaController@hemasdmedianalatna');
Route::get('evaluasi/input-sd-median-alat/hematologi/edit/{id}', 'EvaluasiHemaController@hemasdmedianalatedit');
Route::post('evaluasi/input-sd-median-alat/hematologi/edit/{id}', 'EvaluasiHemaController@hemasdmedianalatupdate');
Route::get('evaluasi/input-sd-median-alat/hematologi/delete/{id}', 'EvaluasiHemaController@hemasdmedianalatdelet');

Route::get('evaluasi/input-sd-median/hematologi', 'EvaluasiHemaController@hemasdmedian');
Route::get('evaluasi/input-sd-median/hematologi/edit/{id}', 'EvaluasiHemaController@hemasdmedianedit');
Route::post('evaluasi/input-sd-median/hematologi/edit/{id}', 'EvaluasiHemaController@hemasdmedianupdate');
Route::post('evaluasi/input-sd-median/hematologi', 'EvaluasiHemaController@hemasdmedianna');
Route::get('evaluasi/input-sd-median/hematologi/delete/{id}', 'EvaluasiHemaController@hemasdmediandelet');

Route::get('evaluasi/input-sd-median-metode/hematologi', 'EvaluasiHemaController@hematologisdmedianmetode');
Route::get('evaluasi/input-sd-median-metode/hematologi/insert', 'EvaluasiHemaController@hematologisdmedianmetodeinsert');
Route::post('evaluasi/input-sd-median-metode/hematologi/insert', 'EvaluasiHemaController@hematologisdmedianmetodena');
Route::get('evaluasi/input-sd-median-metode/hematologi/edit/{id}', 'EvaluasiHemaController@hematologisdmedianmetodeedit');
Route::post('evaluasi/input-sd-median-metode/hematologi/edit/{id}', 'EvaluasiHemaController@hematologisdmedianmetodeupdate');
Route::get('evaluasi/input-sd-median-metode/hematologi/delete/{id}', 'EvaluasiHemaController@hematologisdmedianmetodedelet');
Route::get('evaluasi/input-sd-median-metode/hematologi/print/{id}', 'EvaluasiHemaController@hematologisdmedianmetodeprint');

Route::get('evaluasi/hitung-zscore/hematologi', 'EvaluasiHemaController@perusahaanhem');
Route::post('evaluasi/hitung-zscore/hematologi', 'EvaluasiHemaController@inserttanggal');
Route::get('evaluasi/hitung-zscore/hematologi/{id}', 'EvaluasiHemaController@dataperusahaanhem');

Route::get('evaluasi/hitung-zscore-cetak/hematologi', 'EvaluasiHemaController@perusahaanhemcetak');
Route::post('evaluasi/hitung-zscore-cetak/hematologi', 'EvaluasiHemaController@inserttanggal');
Route::get('evaluasi/hitung-zscore-cetak/hematologi/{id}', 'EvaluasiHemaController@dataperusahaanhemcetak');

Route::get('hematologi/sertifikat/', 'EvaluasiHemaController@perusahaanhemcetaksertifikat');
Route::get('hematologi/sertifikat/{id}', 'EvaluasiHemaController@dataperusahaanhemcetaksertifikat');
Route::get('hasil-pemeriksaan/hematologi/sertifikat/print/{id}', 'SertifikatController@hem');

Route::get('evaluasi/nilai-sama/hematologi', 'EvaluasiHemaController@perusahaanhemnilai');
Route::get('evaluasi/nilai-sama/hematologi/{id}', 'EvaluasiHemaController@dataperusahaanhemnilai');

Route::get('evaluasi/hitung-zscore-alat/hematologi', 'EvaluasiHemaController@perusahaanhemalat');
Route::get('evaluasi/hitung-zscore-alat/hematologi/{id}', 'EvaluasiHemaController@dataperusahaanhemalat');

Route::get('evaluasi/hitung-zscore-metode/hematologi', 'EvaluasiHemaController@perusahaanhemametode');
Route::post('evaluasi/hitung-zscore-metode/hematologi', 'EvaluasiHemaController@inserttanggal');
Route::get('evaluasi/hitung-zscore-metode/hematologi/{id}', 'EvaluasiHemaController@dataperusahaanhemametode');

Route::get('evaluasi/input-ring/hematologi', 'EvaluasiHemaController@hemring');
Route::get('evaluasi/input-ring/hematologi/insert', 'EvaluasiHemaController@hemringinsert');
Route::post('evaluasi/input-ring/hematologi/insert', 'EvaluasiHemaController@hemringna');
Route::get('evaluasi/input-ring/hematologi/edit/{id}', 'EvaluasiHemaController@hemringedit');
Route::post('evaluasi/input-ring/hematologi/edit/{id}', 'EvaluasiHemaController@hemringeditna');
Route::get('evaluasi/input-ring/hematologi/delete/{id}', 'EvaluasiHemaController@hemringdelet');

Route::get('evaluasi/input-ring-metode/hematologi', 'EvaluasiHemaController@hemringmetode');
Route::get('evaluasi/input-ring-metode/hematologi/insert', 'EvaluasiHemaController@hemringmetodei');
Route::post('evaluasi/input-ring-metode/hematologi/insert', 'EvaluasiHemaController@hemringmetodena');
Route::get('evaluasi/input-ring-metode/hematologi/edit/{id}', 'EvaluasiHemaController@hemringmetodeedit');
Route::post('evaluasi/input-ring-metode/hematologi/edit/{id}', 'EvaluasiHemaController@hemringmetodeeditna');
Route::get('evaluasi/input-ring-metode/hematologi/delete/{id}', 'EvaluasiHemaController@hemringmetodedelet');

Route::get('evaluasi/rekapitulasi-hasil/hematologi', 'EvaluasiHemaController@hematologirekapitulasi');
Route::post('evaluasi/rekapitulasi-hasil/hematologi', 'EvaluasiHemaController@hematologirekapitulasina');
////////////////////////END EVALUASI Hematologi////////////////////////////////

////////////////////////EVALUASI KimiaKlinik////////////////////////////////

Route::get('evaluasi/laporan-peserta-parameter/kimiaklinik', 'EvaluasiHemaController@kimiakliniklaporan');
Route::post('evaluasi/laporan-peserta-parameter/kimiaklinik', 'EvaluasiHemaController@kimiakliniklaporanna');

Route::get('evaluasi/laporan-peserta-alat/kimiaklinik', 'EvaluasiHemaController@kimiaklinikalatlaporan');
Route::post('evaluasi/laporan-peserta-alat/kimiaklinik', 'EvaluasiHemaController@kimiaklinikalatlaporanna');

Route::get('evaluasi/laporan-peserta-metode/kimiaklinik', 'EvaluasiHemaController@kimiaklinikmetodelaporan');
Route::post('evaluasi/laporan-peserta-metode/kimiaklinik', 'EvaluasiHemaController@kimiaklinikmetodelaporanna');

Route::get('evaluasi/laporan-pelaksanaan/kimiaklinik', 'EvaluasiHemaController@kimiakliniklaporanpelaksanaan');
Route::post('evaluasi/laporan-pelaksanaan/kimiaklinik', 'EvaluasiHemaController@kimiakliniklaporanpelaksanaanna');

Route::get('evaluasi/rekapitulasi-hasil/kimiaklinik', 'EvaluasiHemaController@kimiaklinikrekapitulasi');
Route::post('evaluasi/rekapitulasi-hasil/kimiaklinik', 'EvaluasiHemaController@hematologirekapitulasina');

Route::get('evaluasi/laporan-jumlah-kelompok/kimiaklinik', 'EvaluasiHemaController@kimiaklinikjumlahkelompok');
Route::post('evaluasi/laporan-jumlah-kelompok/kimiaklinik', 'EvaluasiHemaController@kimiaklinikjumlahkelompokna');

Route::get('evaluasi/hitung-zscore/kimiaklinik', 'EvaluasiHemaController@perusahaankimiaklinik');
Route::post('evaluasi/hitung-zscore/kimiaklinik', 'EvaluasiHemaController@inserttanggal');
Route::get('evaluasi/hitung-zscore/kimiaklinik/{id}', 'EvaluasiHemaController@dataperusahaankimiaklinik');
Route::get('evaluasi/hitung-zscore-alat/kimiaklinik', 'EvaluasiHemaController@perusahaankimiaalat');
Route::get('evaluasi/hitung-zscore-alat/kimiaklinik/{id}', 'EvaluasiHemaController@dataperusahaankimiaalat');
Route::get('evaluasi/hitung-zscore-metode/kimiaklinik', 'EvaluasiHemaController@perusahaankimiametode');
Route::get('evaluasi/hitung-zscore-metode/kimiaklinik/{id}', 'EvaluasiHemaController@dataperusahaankimiametode');

Route::get('evaluasi/input-sd-median-metode/kimiaklinik', 'EvaluasiHemaController@kimiakliniksdmedianmetode');
Route::get('evaluasi/input-sd-median-metode/kimiaklinik/insert', 'EvaluasiHemaController@kimiakliniksdmedianmetodeinsert');
Route::post('evaluasi/input-sd-median-metode/kimiaklinik/insert', 'EvaluasiHemaController@kimiakliniksdmedianmetodena');
Route::get('evaluasi/input-sd-median-metode/kimiaklinik/edit/{id}', 'EvaluasiHemaController@kimiakliniksdmedianmetodeedit');
Route::post('evaluasi/input-sd-median-metode/kimiaklinik/edit/{id}', 'EvaluasiHemaController@kimiakliniksdmedianmetodeupdate');
Route::get('evaluasi/input-sd-median-metode/kimiaklinik/delete/{id}', 'EvaluasiHemaController@kimiakliniksdmedianmetodedelet');
Route::get('evaluasi/input-sd-median-metode/kimiaklinik/print/{id}', 'EvaluasiHemaController@kimiakliniksdmedianmetodeprint');

Route::get('evaluasi/input-sd-median/kimiaklinik', 'EvaluasiHemaController@kimiakliniksdmedian');
Route::get('evaluasi/input-sd-median/kimiaklinik/insert', 'EvaluasiHemaController@kimiakliniksdmedianinsert');
Route::post('evaluasi/input-sd-median/kimiaklinik/insert', 'EvaluasiHemaController@kimiakliniksdmedianna');
Route::get('evaluasi/input-sd-median/kimiaklinik/edit/{id}', 'EvaluasiHemaController@kimiakliniksdmedianedit');
Route::post('evaluasi/input-sd-median/kimiaklinik/edit/{id}', 'EvaluasiHemaController@kimiakliniksdmedianupdate');
Route::get('evaluasi/input-sd-median/kimiaklinik/delete/{id}', 'EvaluasiHemaController@kimiakliniksdmediandelet');
Route::get('evaluasi/input-sd-median/kimiaklinik/print/{id}', 'EvaluasiHemaController@kimiakliniksdmedianprint');

Route::get('evaluasi/input-sd-median-alat/kimiaklinik', 'EvaluasiHemaController@kimiakliniksdmedianalat');
Route::get('evaluasi/input-sd-median-alat/kimiaklinik/insert', 'EvaluasiHemaController@kimiakliniksdmedianalatinsert');
Route::post('evaluasi/input-sd-median-alat/kimiaklinik/insert', 'EvaluasiHemaController@kimiakliniksdmedianalatna');
Route::get('evaluasi/input-sd-median-alat/kimiaklinik/edit/{id}', 'EvaluasiHemaController@kimiakliniksdmedianalatedit');
Route::post('evaluasi/input-sd-median-alat/kimiaklinik/edit/{id}', 'EvaluasiHemaController@kimiakliniksdmedianalatupdate');
Route::get('evaluasi/input-sd-median-alat/kimiaklinik/delete/{id}', 'EvaluasiHemaController@kimiakliniksdmedianalatdelet');
Route::get('evaluasi/input-sd-median-alat/kimiaklinik/print/{id}', 'EvaluasiHemaController@kimiakliniksdmedianalatprint');

Route::get('evaluasi/input-ring/kimiaklinik', 'EvaluasiHemaController@kimiaring');
Route::get('evaluasi/input-ring/kimiaklinik/insert', 'EvaluasiHemaController@kimiaringna');
Route::post('evaluasi/input-ring/kimiaklinik/insert', 'EvaluasiHemaController@kimiaringnasave');
Route::get('evaluasi/input-ring/kimiaklinik/edit/{id}', 'EvaluasiHemaController@kimiaringedit');
Route::post('evaluasi/input-ring/kimiaklinik/edit/{id}', 'EvaluasiHemaController@kimiaringeditna');
Route::get('evaluasi/input-ring/kimiaklinik/delete/{id}', 'EvaluasiHemaController@kimiaringdelet');

Route::get('evaluasi/input-ring/kimiaklinik-alat', 'EvaluasiHemaController@kimiaringalat');
Route::get('evaluasi/input-ring/kimiaklinik-alat/insert', 'EvaluasiHemaController@kimiaringnaalat');
Route::post('evaluasi/input-ring/kimiaklinik-alat/insert', 'EvaluasiHemaController@kimiaringnasavealat');
Route::get('evaluasi/input-ring/kimiaklinik-alat/delete', 'EvaluasiHemaController@kimiaringdeletalat');

Route::get('evaluasi/input-ring/kimiaklinik-metode', 'EvaluasiHemaController@kimiaringmetode');
Route::get('evaluasi/input-ring/kimiaklinik-metode/insert', 'EvaluasiHemaController@kimiaringnametode');
Route::post('evaluasi/input-ring/kimiaklinik-metode/insert', 'EvaluasiHemaController@kimiaringnasavemetode');
Route::get('evaluasi/input-ring/kimiaklinik-metode/delete', 'EvaluasiHemaController@kimiaringdeletmetode');

Route::get('evaluasi/input-ring-metode/kimiaklinik', 'EvaluasiHemaController@kimiaringmetode');
Route::get('evaluasi/input-ring-metode/kimiaklinik/insert', 'EvaluasiHemaController@kimiaringnametode');
Route::post('evaluasi/input-ring-metode/kimiaklinik/insert', 'EvaluasiHemaController@kimiaringnasavemetode');
Route::get('evaluasi/input-ring-metode/kimiaklinik/edit/{id}', 'EvaluasiHemaController@kimiaringmetodeedit');
Route::post('evaluasi/input-ring-metode/kimiaklinik/edit/{id}', 'EvaluasiHemaController@kimiaringmetodeeditna');
Route::get('evaluasi/input-ring-metode/kimiaklinik/delete/{id}', 'EvaluasiHemaController@kimiaringmetodedelet');
////////////////////////END EVALUASI KimiaKlinik////////////////////////////////

////////////////////////EVALUASI KimiaAir////////////////////////////////

Route::get('evaluasi/laporan-peserta-parameter/kimiaair', 'EvaluasiHemaController@kimiaairlaporan');
Route::post('evaluasi/laporan-peserta-parameter/kimiaair', 'EvaluasiHemaController@kimiaairlaporanna');

Route::get('evaluasi/hitung-zscore/kimiaair', 'EvaluasiHemaController@perusahaankimiaair');
Route::get('evaluasi/hitung-zscore/kimiaair/{id}', 'EvaluasiHemaController@dataperusahaankimiaair');

Route::get('/kimkes/sertifikat', 'EvaluasiHemaController@perusahaankimiaairsertifikat');
Route::get('/kimkes/kimiaair/{id}', 'EvaluasiHemaController@dataperusahaankimiaairsertifikat');
Route::get('hasil-pemeriksaan/kimia-air/sertifikat/print/{id}/','SertifikatController@kimkes');
// Route::get('/hasil-pemeriksaan/kimia-air/sertifikat/print/{id}/', 'SertifikatController
// 	@kimkes');


Route::get('evaluasi/input-sd-median/kimiaair', 'EvaluasiHemaController@kimiaairsdmedian');
Route::post('evaluasi/input-sd-median/kimiaair', 'EvaluasiHemaController@kimiaairsdmedianna');
Route::get('evaluasi/input-sd-median/kimiaair/edit/{id}', 'EvaluasiHemaController@kimiaairsdmedianedit');
Route::post('evaluasi/input-sd-median/kimiaair/edit/{id}', 'EvaluasiHemaController@kimiaairsdmedianupdate');
Route::get('evaluasi/input-sd-median/kimiaair/delete/{id}', 'EvaluasiHemaController@kimiaairsdmediandelet');

////////////////////////END EVALUASI KimiaAir////////////////////////////////

////////////////////////EVALUASI KimiaAirTerbatas////////////////////////////////

Route::get('evaluasi/laporan-peserta-parameter/kimiaairterbatas', 'EvaluasiHemaController@kimiaairterbataslaporan');
Route::post('evaluasi/laporan-peserta-parameter/kimiaairterbatas', 'EvaluasiHemaController@kimiaairterbataslaporanna');

Route::get('evaluasi/hitung-zscore/kimiaairterbatas', 'EvaluasiHemaController@perusahaankimiaairterbatas');
Route::get('evaluasi/hitung-zscore/kimiaairterbatas/{id}', 'EvaluasiHemaController@dataperusahaankimiaairterbatas');

Route::get('kimkest/sertifikat', 'EvaluasiHemaController@perusahaankimiaairterbatassertifikat');
Route::get('kimkest/kimiaairterbatas/{id}', 'EvaluasiHemaController@dataperusahaankimiaairterbatassertifikat');
Route::get('hasil-pemeriksaan/kimia-air-terbatas/sertifikat/print/{id}', 'SertifikatController@kimkes');


Route::get('evaluasi/input-sd-median/kimiaairterbatas', 'EvaluasiHemaController@kimiaairterbatassdmedian');
Route::post('evaluasi/input-sd-median/kimiaairterbatas', 'EvaluasiHemaController@kimiaairterbatassdmedianna');
Route::get('evaluasi/input-sd-median/kimiaairterbatas/edit/{id}', 'EvaluasiHemaController@kimiaairterbatassdmedianedit');
Route::post('evaluasi/input-sd-median/kimiaairterbatas/edit/{id}', 'EvaluasiHemaController@kimiaairterbatassdmedianupdate');
Route::get('evaluasi/input-sd-median/kimiaairterbatas/delete/{id}', 'EvaluasiHemaController@kimiaairterbatassdmediandelet');

////////////////////////END EVALUASI KimiaAirTerbatas////////////////////////////////

Route::get('/rekap-jumlah-peserta-parameter', function () {return view('laporan.rekap.peserta_parameter');});
Route::post('/rekap-jumlah-peserta-parameter', 'LaporanRegistrasiController@rekappesertaexcel');
Route::get('/dataparameter/{id}', 'LaporanRegistrasiController@rekappeserta');


Route::get('/monitoring-simultan', function () {return view('laporan.rekap.monitoring_simultan');});
Route::get('/monitoring-laporan-hasil',
	function () {
		if (Auth::user()->penyelenggara <= '3') {
			return view('laporan.rekap.monitoring_laporan_hasil_kk');
		}elseif (Auth::user()->penyelenggara >= '6') {
			return view('laporan.rekap.monitoring_laporan_hasil_tprpr');
		}else{
			return view('laporan.rekap.monitoring_laporan_hasil1');
		}
		// elseif (Auth::user()->penyelenggara <= '3') {
		// 	return view('laporan.rekap.monitoring_laporan_hasil');
		// }
	});
Route::get('/monitoringsimultan1/{id}', 'LaporanRegistrasiController@monitoring1');
Route::get('/monitoringsimultan2/{id}', 'LaporanRegistrasiController@monitoring1');
Route::get('/monitoringsimultan3/{id}', 'LaporanRegistrasiController@monitoring1');
Route::get('/monitoringsimultan4/{id}', 'LaporanRegistrasiController@monitoring2');
Route::get('/monitoringsimultan5/{id}', 'LaporanRegistrasiController@monitoring2');
Route::get('/monitoringsimultan6/{id}', 'LaporanRegistrasiController@monitoring2');
Route::get('/monitoringsimultan7/{id}', 'LaporanRegistrasiController@monitoring3');
Route::get('/monitoringsimultan8/{id}', 'LaporanRegistrasiController@monitoring2');
Route::get('/monitoringsimultan9/{id}', 'LaporanRegistrasiController@monitoring2');
Route::get('/monitoringsimultan10/{id}', 'LaporanRegistrasiController@monitoring2');
Route::get('/monitoringsimultan11/{id}', 'LaporanRegistrasiController@monitoring2');
Route::get('/monitoringsimultan12/{id}', 'LaporanRegistrasiController@monitoring2');
Route::get('/monitoringsimultan13/{id}', 'LaporanRegistrasiController@monitoring2');


Route::get('/rekap-transaksi-parameter', function () {return view('laporan.rekap.transaksi_parameter');});
Route::post('/rekap-transaksi-parameter', 'LaporanRegistrasiController@rekaptransaksiexcel');
Route::get('/datatransaksi/{id}', 'LaporanRegistrasiController@rekaptransaksi');

Route::get('/rekap-peserta-parameter', function () {return view('laporan.rekap.peserta_instalasi');});
Route::post('/rekap-peserta-parameter', 'LaporanRegistrasiController@rekappesertaparameterexcel');
Route::get('/datapesertaparameter/{id}', 'LaporanRegistrasiController@rekappesertaparameter');


Route::get('/laporan-cetak-hasil', 'LaporanCetakHasilController@index');
Route::get('/laporan-edit-hasil', 'LaporanCetakHasilController@edit');
Route::get('/laporan-cek-hasil', 'LaporanCetakHasilController@editcetak');

Route::get('/rekap-peserta-provinsi', function () {return view('laporan.rekap.peserta_provinsi');});
Route::post('/rekap-peserta-provinsi', 'LaporanRegistrasiController@rekappesertaprovinsiexcel');
Route::get('/datapesertaprovinsi/{id}', 'LaporanRegistrasiController@rekappesertaprovinsi');

Route::get('/rekap-instansi-provinsi', function () {return view('laporan.rekap.instansi_provinsi');});
Route::post('/rekap-instansi-provinsi', 'LaporanRegistrasiController@rekapinstansiprovinsiexcel');
Route::get('/datainstansiprovinsi/{id}', 'LaporanRegistrasiController@rekapinstansiprovinsi');

Route::get('/rekap-peserta-bidang', 'LaporanRegistrasiController@seluruh');
Route::post('/rekap-peserta-bidang', 'LaporanRegistrasiController@seluruhcetak');

Route::get('/rekap-peserta-instansi', function () {
    $subBidang = DB::table('sub_bidang')->get();
	return view('laporan.rekap.peserta_instansi',compact('subBidang'));
});
Route::post('/rekap-peserta-instansi', 'LaporanRegistrasiController@rekapinstansiexcel');
Route::get('/datainstansi/{id}', 'LaporanRegistrasiController@rekapinstansi');

Route::get('/rekap-transaksi-peserta-parameter', function () {return view('laporan.rekap.tarif_parameter');});
Route::post('/rekap-transaksi-peserta-parameter', 'LaporanRegistrasiController@rekappesertatransaksiexcel');
Route::get('/datapesertatransaksi/{id}', 'LaporanRegistrasiController@rekappesertatransaksi');


Route::get('grafik/rekap-peserta-instansi', function () {
	$penyelenggara = Auth::user()->penyelenggara;
    $subBidang = DB::table('sub_bidang')->find($penyelenggara);
	return view('laporan.grafik.peserta_instansi',compact('subBidang'));
});
Route::get('grafik/datainstansip', 'LaporanRegistrasiController@grafikinstansip');
Route::get('grafik/datainstansifilter', 'LaporanRegistrasiController@grafikinstansifilter');
Route::get('grafik/datainstansij', 'LaporanRegistrasiController@grafikinstansij');

Route::get('grafik/grafik-malaria', function () {return view('laporan.grafik.malaria');});
Route::get('grafik/grafik-tc', function () {return view('laporan.grafik.tc');});

Route::get('grafik/rekap-transaksi-parameter', function () {return view('laporan.grafik.transaksi_parameter');});
Route::get('grafik/datatransaksit', 'LaporanRegistrasiController@grafiktransaksit');
Route::get('grafik/datatransaksip', 'LaporanRegistrasiController@grafiktransaksip');
Route::get('grafik/datamalaria', 'LaporanRegistrasiController@grafikmalaria');
Route::get('grafik/datatc/{id}', 'LaporanRegistrasiController@grafiktc');

Route::get('grafik/rekap-peserta-pembayaran', function () {return view('laporan.grafik.pembayaran');});
Route::get('grafik/datapembayaran', 'LaporanRegistrasiController@grafikpembayaran');

//Z-score Grafik//

Route::get('grafik/hitung-zscore/hematologi', 'GrafikController@perusahaanhem');
Route::get('grafik/hitung-zscore/hematologi/{id}', 'GrafikController@dataperusahaanhem');

Route::get('grafik/hitung-zscore-semua/hematologi', 'GrafikController@perusahaanhemsemua');
Route::get('grafik/hitung-zscore-semua/hematologi/{id}', 'GrafikController@dataperusahaanhemsemua');

Route::get('grafik/hitung-zscore-semua/kimiaklinik', 'GrafikController@perusahaankksemua');
Route::get('grafik/hitung-zscore-semua/kimiaklinik/{id}', 'GrafikController@dataperusahaankksemua');

Route::get('grafik/hitung-zscore-alat/hematologi', 'GrafikController@perusahaanhem');
Route::get('grafik/hitung-zscore-alat/hematologi/{id}', 'GrafikController@dataperusahaanalathem');

Route::get('grafik/nilai-sama/hematologi', 'GrafikController@perusahaanhemnilai');
Route::get('grafik/nilai-sama/hematologi/{id}', 'GrafikController@dataperusahaanhemnilai');

Route::get('grafik/nilai-sama/kimiaair', 'GrafikController@perusahaankimairnilai');
Route::get('grafik/nilai-sama/kimiaair/{id}', 'GrafikController@dataperusahaankimairnilai');

Route::get('grafik/nilai-sama/kimiaairterbatas', 'GrafikController@perusahaankimairterbatasnilai');
Route::get('grafik/nilai-sama/kimiaairterbatas/{id}', 'GrafikController@dataperusahaankimairterbatasnilai');

Route::get('grafik/nilai-sama-alat/hematologi', 'GrafikController@perusahaanhemnilai');
Route::get('grafik/nilai-sama-alat/hematologi/{id}', 'GrafikController@dataperusahaanhemnilaialat');

Route::get('grafik/nilai-sama-metode/hematologi', 'GrafikController@perusahaanhemnilai');
Route::get('grafik/nilai-sama-metode/hematologi/{id}', 'GrafikController@dataperusahaanhemnilaimetode');

Route::get('grafik/nilai-sama/kimiaklinik', 'GrafikController@perusahaankimnilai');
Route::get('grafik/nilai-sama/kimiaklinik/{id}', 'GrafikController@dataperusahaankimnilai');

Route::get('kimia-klinik/data-evaluasi/nilai-sama-alat/{id}', 'GrafikController@kknilaisama');
Route::post('kimia-klinik/data-evaluasi/nilai-sama-alat/{id}', 'GrafikController@gkknilaisama');

Route::get('kimiaklinik/sertifikat', 'GrafikController@perusahaankimnilaisertifikat');
Route::post('kimiaklinik/sertifikat', 'GrafikController@evaluasiInsertSertifikat');

Route::get('kimiaklinik/sertifikat/{id}', 'GrafikController@dataperusahaankimnilaisertifikat');

Route::get('hasil-pemeriksaan/kimia-klinik/sertifikat/print/{id}', 'SertifikatController@kimiaklinik');

Route::get('grafik/nilai-sama-alat/kimiaklinik', 'GrafikController@perusahaankimnilai');
Route::get('grafik/nilai-sama-alat/kimiaklinik/{id}', 'GrafikController@dataperusahaankimnilaialat');

Route::get('grafik/nilai-sama-metode/kimiaklinik', 'GrafikController@perusahaankimnilai');
Route::get('grafik/nilai-sama-metode/kimiaklinik/{id}', 'GrafikController@dataperusahaankimnilaimetode');

Route::get('grafik/hitung-zscore/kimiaklinik', 'GrafikController@perusahaankim');
Route::get('grafik/hitung-zscore/kimiaklinik/{id}', 'GrafikController@dataperusahaankim');

Route::get('grafik/hitung-zscore-alat/kimiaklinik', 'GrafikController@perusahaankim');
Route::get('grafik/hitung-zscore-alat/kimiaklinik/{id}', 'GrafikController@dataperusahaankimalat');

Route::get('grafik/hitung-zscore-metode/kimiaklinik', 'GrafikController@perusahaankim');
Route::get('grafik/hitung-zscore-metode/kimiaklinik/{id}', 'GrafikController@dataperusahaankimmetode');

Route::get('grafik/hitung-zscore/kimiaair', 'GrafikController@perusahaankimair');
Route::get('grafik/hitung-zscore/kimiaair/{id}', 'GrafikController@dataperusahaankimair');

Route::get('grafik/hitung-zscore/kimiaairterbatas', 'GrafikController@perusahaankimairterbatas');
Route::get('grafik/hitung-zscore/kimiaairterbatas/{id}', 'GrafikController@dataperusahaankimairterbatas');

//dashboard//

Route::get('/dashboard/pnpme', 'DashboardController@pnpme');
Route::post('/dashboard/pnpme/view', 'DashboardController@pnpmep');
Route::post('/dashboard/pnpme/excel', 'DashboardController@pnpmee');
Route::post('/dashboard/pnpme/pdf', 'DashboardController@pnpmepdf');
Route::get('/getparameter/{id}', 'DashboardController@getparameter');

// Route::get('/dashboard/wilayah', 'DashboardController@wilayah');
Route::get('/dashboard/wilayah', function () {return view('dashboard.wilayah.index');});
Route::post('/dashboard/wilayah/excel', 'DashboardController@wilayahexcel');
Route::post('/dashboard/wilayah/pdf', 'DashboardController@wilayahpdf');
Route::get('/wilayah/{id}', 'DashboardController@wilayah');

Route::get('/dashboard/badan-usaha', function () {return view('dashboard.badanusaha.index');});
Route::post('/dashboard/badanusaha/excel', 'DashboardController@badanusahaexcel');
Route::post('/dashboard/badanusaha/pdf', 'DashboardController@badanusahapdf');
Route::get('/badanusaha/{id}', 'DashboardController@badanusaha');

Route::get('/dashboard/badan-usaha-provinsi', 'DashboardController@badanusahana');
Route::post('/dashboard/badanusahaprovinsi/excel', 'DashboardController@badanusahaexcelprovinsi');
Route::post('/dashboard/badanusahaprovinsi/pdf', 'DashboardController@badanusahapdfprovinsi');
Route::get('/badanusahaprovinsi', 'DashboardController@badanusahaprovinsi');

Route::get('/dashboard/bidang-pengujian', function () {return view('dashboard.bidang.index');});
Route::post('/dashboard/bidang/excel', 'DashboardController@bidangexcel');
Route::post('/dashboard/bidang/pdf', 'DashboardController@bidangpdf');
Route::get('/bidang/{id}', 'DashboardController@bidang');

Route::get('/dashboard/pembayaran', function () {return view('dashboard.pembayaran.index');});
Route::post('/dashboard/pembayaran/excel', 'DashboardController@pembayaranexcel');
Route::post('/dashboard/pembayaran/pdf', 'DashboardController@pembayaranpdf');
Route::get('/pembayaran/{id}', 'DashboardController@pembayaran');

Route::get('/dashboard/omset', function () {return view('dashboard.omset.index');});
Route::post('/dashboard/omset/excel', 'DashboardController@omsetexcel');
Route::post('/dashboard/omset/pdf', 'DashboardController@omsetpdf');
Route::get('/omset/{id}', 'DashboardController@omset');

Route::get('/dashboard/grafik/wilayah', 'GrafikController@index');
Route::get('/dashboard/grafik/badan-usaha', 'GrafikController@badan');
Route::get('/dashboard/grafik/bidang-pengujian', 'GrafikController@bidang');
Route::get('/dashboard/grafik/type-pembayaran', 'GrafikController@pembayaran');

Route::get('/grafik/wilayah', 'GrafikController@wilayahg');
Route::get('/grafik/badan-usaha', 'GrafikController@badang');
Route::get('/grafik/bidang-pengujian', 'GrafikController@bidangg');
Route::get('/grafik/type-pembayaran', 'GrafikController@typeg');

Route::get('/pendapat/pendapat-responden', 'PendapatController@index');
Route::post('/pendapat/pendapat-responden', 'PendapatController@insert');
Route::get('/pendapat/keluh-saran', 'PendapatController@saran');
Route::post('/pendapat/keluh-saran', 'PendapatController@insertsaran');
Route::get('/pendapat/keluh-saran/jawab', 'PendapatController@viewsaran');
Route::get('/pendapat/keluh-saran/jawab/{id}', 'PendapatController@jawabsaran');
Route::post('/pendapat/keluh-saran/jawab/{id}', 'PendapatController@injawabsaran');

Route::get('/pendapat/jawab/keluh-saran', 'PendapatController@jawabansaran');
Route::get('/pendapat/jawab/keluh-saran/{id}', 'PendapatController@datajawab');

Route::get('/evaluasi/hematologi/rekap-hasil', function () {return view('evaluasi.hematologi.parameter');});
Route::get('/evaluasi/imunologi/anti-hiv/evaluasi-peserta', function () {return view('evaluasi.imunologi.hiv.evaluasi');});
Route::get('/evaluasi/imunologi/anti-hcv/evaluasi-peserta', function () {return view('evaluasi.imunologi.hcv.evaluasi');});

//---------------------Rujukan------------------------//

Route::get('/admin/rujukan-bta', 'RujukanController@indexbta');
Route::get('/admin/rujukan-bta/edit/{id}', 'RujukanController@editbta');
Route::get('/admin/rujukan-bta/delete/{id}', 'RujukanController@deletebta');
Route::get('/admin/rujukan-bta/insert', function(){return view("/back/rujukan/bta/insert");});
Route::post('/admin/rujukan-bta/insert', 'RujukanController@insertbta');
Route::post('/admin/rujukan-bta/edit/{id}', 'RujukanController@updatebta');

Route::get('/admin/rujukan-malaria', 'RujukanController@indexmalaria');
Route::get('/admin/rujukan-malaria/edit/{id}', 'RujukanController@editmalaria');
Route::get('/admin/rujukan-malaria/delete/{id}', 'RujukanController@deletemalaria');

Route::get('/admin/rujukan-malaria/insert', function(){
	return view("/back/rujukan/malaria/insert");
});

Route::post('/admin/rujukan-malaria/insert', 'RujukanController@insertmalaria');
Route::post('/admin/rujukan-malaria/edit/{id}', 'RujukanController@updatemalaria');

Route::get('/admin/rujukan-urinalisa', 'RujukanController@indexurinalisa');
Route::get('/admin/rujukan-urinalisa-metode', 'RujukanController@indexurinalisametode');
Route::get('/admin/rujukan-urinalisa/edit/{id}', 'RujukanController@editurinalisa');
Route::get('/admin/rujukan-urinalisa/editmetode/{id}', 'RujukanController@editurinalisametode');
Route::get('/admin/rujukan-urinalisa/delete/{id}', 'RujukanController@deleteurinalisa');
Route::get('/admin/rujukan-urinalisa/deletemetode/{id}', 'RujukanController@deleteurinalisametode');
Route::get('/admin/rujukan-urinalisa/insert', 'RujukanController@inserturinalisa');
Route::get('/admin/rujukan-urinalisa/insertmetode', 'RujukanController@insertmetode');

Route::get('/admin/rujukan-urinalisa/bj-ph', 'RujukanController@inserturinalisa2');
Route::post('/admin/rujukan-urinalisa/bj-ph', 'RujukanController@inserturinalisana2');
Route::post('/admin/rujukan-urinalisa/insert', 'RujukanController@inserturinalisana');
Route::post('/admin/rujukan-urinalisa/insertmetode', 'RujukanController@inserturinalisanametode');
Route::post('/admin/rujukan-urinalisa/edit/{id}', 'RujukanController@updateurinalisa');
Route::post('/admin/rujukan-urinalisa/editmetode/{id}', 'RujukanController@updateurinalisametode');

Route::get('/admin/rujukan-hematologi', 'RujukanController@indexhematologi');
Route::get('/admin/rujukan-hematologi/edit/{id}', 'RujukanController@edithematologi');
Route::get('/admin/rujukan-hematologi/delete/{id}', 'RujukanController@deletehematologi');
Route::get('/admin/rujukan-hematologi/insert', function(){return view("/back/rujukan/hematologi/insert");});
Route::post('/admin/rujukan-hematologi/insert', 'RujukanController@inserthematologi');
Route::post('/admin/rujukan-hematologi/edit/{id}', 'RujukanController@updatehematologi');

Route::get('/admin/rujukan-kimiaklinik', 'RujukanController@indexkimiaklinik');
Route::get('/admin/rujukan-kimiaklinik/edit/{id}', 'RujukanController@editkimiaklinik');
Route::get('/admin/rujukan-kimiaklinik/delete/{id}', 'RujukanController@deletekimiaklinik');
Route::get('/admin/rujukan-kimiaklinik/insert', function(){return view("/back/rujukan/kimiaklinik/insert");});
Route::post('/admin/rujukan-kimiaklinik/insert', 'RujukanController@insertkimiaklinik');
Route::post('/admin/rujukan-kimiaklinik/edit/{id}', 'RujukanController@updatekimiaklinik');

Route::get('/admin/rujukan-kimiakesehatan', 'RujukanController@indexkimiakesehatan');
Route::get('/admin/rujukan-kimiakesehatan/edit/{id}', 'RujukanController@editkimiakesehatan');
Route::get('/admin/rujukan-kimiakesehatan/delete/{id}', 'RujukanController@deletekimiakesehatan');
Route::get('/admin/rujukan-kimiakesehatan/insert', function(){return view("/back/rujukan/kimiakesehatan/insert");});
Route::post('/admin/rujukan-kimiakesehatan/insert', 'RujukanController@insertkimiakesehatan');
Route::post('/admin/rujukan-kimiakesehatan/edit/{id}', 'RujukanController@updatekimiakesehatan');

Route::get('/admin/rujukan-tc', 'RujukanController@indextc');
Route::get('/admin/rujukan-tc/edit/{id}', 'RujukanController@edittc');
Route::get('/admin/rujukan-tc/delete/{id}', 'RujukanController@deletetc');
Route::get('/admin/rujukan-tc/insert', function(){return view("/back/rujukan/tc/insert");});
Route::post('/admin/rujukan-tc/insert', 'RujukanController@inserttc');
Route::post('/admin/rujukan-tc/edit/{id}', 'RujukanController@updatetc');

Route::get('/admin/rujukan-imunologi', 'RujukanController@indeximunologi');
Route::get('/admin/rujukan-imunologi/edit/{id}', 'RujukanController@editimunologi');
Route::get('/admin/rujukan-imunologi/delete/{id}', 'RujukanController@deleteimunologi');
Route::get('/admin/rujukan-imunologi/insert', function(){return view("/back/rujukan/imunologi/insert");});
Route::post('/admin/rujukan-imunologi/insert', 'RujukanController@insertimunologi');
Route::post('/admin/rujukan-imunologi/edit/{id}', 'RujukanController@updateimunologi');

//------------ Evaluasi Imunologi ----------------//

Route::get('/evaluasi/imunologi', 'EvaluasiimunController@perusahaan');
Route::post('/evaluasi/imunologi', 'EvaluasiimunController@inserttanggal');
Route::get('/evaluasi/imunologi/{id}', 'EvaluasiimunController@dataperusahaan');

Route::get('/evaluasi/cetak-imunologi', 'EvaluasiimunController@perusahaancetak');
Route::get('/evaluasi/cetak-imunologi/{id}', 'EvaluasiimunController@dataperusahaancetak');

Route::get('/evaluasi/sertifikat', 'EvaluasiimunController@perusahaancetaksertifikat');
Route::get('/evaluasi/cetak-sertifikat/{id}', 'EvaluasiimunController@dataperusahaancetaksertifikat');
Route::post('/evaluasi/sertifikat', 'EvaluasiimunController@evaluasiInsertSertifikat');
Route::post('/hematologi/sertifikat', 'EvaluasiHemaController@evaluasiInsertSertifikatna');

Route::get('/hasil-pemeriksaan/hbsag/sertifikat/print/{id}', 'SertifikatController@cetakhbsag');
Route::get('/hasil-pemeriksaan/rpr-syphilis/sertifikat/print/{id}', 'SertifikatController@cetakhbsag');
Route::get('/hasil-pemeriksaan/syphilis/sertifikat/print/{id}', 'SertifikatController@cetakhbsag');
Route::get('/hasil-pemeriksaan/anti-hcv/sertifikat/print/{id}', 'SertifikatController@cetakhbsag');
Route::get('/hasil-pemeriksaan/anti-hiv/sertifikat/print/{id}', 'SertifikatController@cetakhbsag');


Route::get('/evaluasi/urinalisa', 'EvaluasiimunController@urinalisa');
Route::get('/evaluasi/urinalisa/{id}', 'EvaluasiimunController@dataurinalisa');

Route::get('/evaluasi/urinalisasi', 'EvaluasiimunController@urinalisa');
Route::post('/evaluasi/urinalisasi', 'EvaluasiHemaController@inserttanggal');
Route::get('/evaluasi/urinalisasi/{id}', 'EvaluasiimunController@dataurinalisa');

Route::get('/urinalisa/sertifikat', 'EvaluasiimunController@urinalisasertifikat');
Route::post('/urinalisa/sertifikat', 'EvaluasiimunController@evaluasiInsertSertifikat');

Route::get('/urinalisa/sertifikat/{id}', 'EvaluasiimunController@dataurinalisasertifikat');
Route::get('/hasil-pemeriksaan/urinalisasi/sertifikat/print/{id}', 'SertifikatController@uri');


Route::get('/penilaian/parameter-urinalisa', 'EvaluasiimunController@parameter');
Route::get('/penilaian/parameter-urinalisa/{id}', 'EvaluasiimunController@dataparameter');

Route::get('/penilaian/reagen-urinalisa', 'EvaluasiimunController@reagen');
Route::get('/penilaian/reagen-urinalisa/{id}', 'EvaluasiimunController@datareagen');
Route::get('/penilaian/reagen-kehamilan-urinalisa', 'EvaluasiimunController@reagenkehamilan');
Route::get('/penilaian/reagen-kehamilan-urinalisa/{id}', 'EvaluasiimunController@datareagenkehamilan');

Route::get('/penilaian/skoring-urinalisa', 'EvaluasiimunController@skoring');
Route::post('/penilaian/skoring-urinalisa', 'EvaluasiimunController@grafikskoring');

Route::get('/getalat/{id}',
	function($id){
        $data = DB::table('tb_instrumen')->select('id as Kode','instrumen as Nama')->where('id_parameter', $id)->get();
        return response()->json(['Hasil'=>$data]);
	}
);
Route::get('/getmetode/{id}',
	function($id){
        $data = DB::table('metode_pemeriksaan')->select('id as Kode','metode_pemeriksaan as Nama')->where('parameter_id', $id)->get();
        return response()->json(['Hasil'=>$data]);
	}
);
Route::get('/getreagen/{id}',
	function($id){
		if($id == '19'){
        	$data = DB::table('tb_reagen_imunologi')->select('id as Kode','reagen as Nama')->where('parameter_id', $id)->get();
    	}else{
        	$data = DB::table('tb_reagen_imunologi')->select('id as Kode','reagen as Nama')->where('kelompok', '=','Urinalisa')->where('parameter_id', NULL)->get();
    	}
        return response()->json(['Hasil'=>$data]);
	}
);

Route::get('/getreagenimun/{id}', 'DaftarController@getReagenImun');

Route::get('/evaluasi/input-rujukan/bakteri', 'EvaluasiBakteriController@indexbakteri');
Route::get('/evaluasi/input-rujukan/bakteri/insert/', 'EvaluasiBakteriController@insertBakteri');
Route::get('/evaluasi/input-rujukan/bakteri/edit/{id}', 'EvaluasiBakteriController@editBakteri');
Route::get('/evaluasi/input-rujukan/bakteri/delete/{id}', 'EvaluasiBakteriController@deleteBakteri');
Route::post('/evaluasi/input-rujukan/bakteri/insert', 'EvaluasiBakteriController@insertBakteri2');
Route::post('/evaluasi/input-rujukan/bakteri/edit/{id}', 'EvaluasiBakteriController@updateBakteri');

Route::get('evaluasi/bakteri/penilaian', 'EvaluasiBakteriController@perusahaanbakteri');
Route::get('evaluasi/bakteri/evaluasi_uji_kepekaan_antibiotik', 'EvaluasiBakteriController@perusahaanbakteri1');

Route::get('bac/sertifikat', 'EvaluasiBakteriController@perusahaanbakterisertifikat');
Route::get('bac/sertifikat/{id}', 'EvaluasiBakteriController@dataperusahaanbakteri1sertifikat');
Route::get('hasil-pemeriksaan/antibiotik/sertifikat/print/{id}','SertifikatController@bac');


Route::get('evaluasi/bakteri/penilaian_uji_kepekaan_antibiotik/{id}', 'EvaluasiBakteriController@dataperusahaanbakteri1');
Route::get('evaluasi/bakteri/penilaian/{id}', 'EvaluasiBakteriController@dataperusahaanbakteri');
Route::get('hasil-pemeriksaan/antibiotik/evaluasi/{id}', 'EvaluasiBakteriController@evaluasi');

Route::post('hasil-pemeriksaan/antibiotik/evaluasi/print/{id}','EvaluasiBakteriController@evaluasiPrint');
Route::get('hasil-pemeriksaan/antibiotik/evaluasiuji/{id}', 'EvaluasiBakteriController@kepekaan');
Route::post('hasil-pemeriksaan/antibiotik/evaluasiuji/print/{id}','EvaluasiBakteriController@evaluasiujiPrint');

Route::get('/evaluasi/rekap-hasil-peserta/bakteri', 'EvaluasiBakteriController@perusahaan');
Route::get('/evaluasi/rekap-hasil-peserta/bakteri/{id}', 'EvaluasiBakteriController@dataperusahaan');

Route::post('hasil-pemeriksaan/antibiotik/evaluasi/{id}', 'EvaluasiBakteriController@evaluasiInsert');
Route::post('hasil-pemeriksaan/antibiotik/evaluasiuji/{id}', 'EvaluasiBakteriController@evaluasiInsertUji');

Route::get('evaluasi/bakteri/penilaian-uji-kepekaan-antibiotik', 'EvaluasiBakteriController@penilaian');
Route::get('evaluasi/bakteri/antibiotik/{id}', 'EvaluasiBakteriController@penilaiandetailantibiotik');
Route::get('/evaluasi/antibiotik-lembar/{id}', 'EvaluasiBakteriController@lembar');
Route::post('/evaluasi/antibiotik-lembar/{id}', 'EvaluasiBakteriController@insertAntibiotik');

Route::get('evaluasi/bakteri/grafik-keterangan-peserta', 'EvaluasiBakteriController@grafikketeranganpeserta');
Route::post('evaluasi/bakteri/grafik-keterangan-peserta', 'EvaluasiBakteriController@grafikketeranganpesertanya');
Route::get('evaluasi/bakteri/grafik-keterangan-peserta/print/{id}', 'EvaluasiBakteriController@cetakimonologi');
